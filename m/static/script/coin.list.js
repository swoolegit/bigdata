function initLoading() {
    $('a.on-more').hide();
    $('div.mine-message').hide();
}

function getCoinList(more) {//1:更多，2:充值
    loadingShow();
    $('a.on-more').html("正在获取数据中..");
    $.ajax({
        url: '/user/coin_search',
        type: 'POST',
        dataType: 'html',
        data: {
            'type' : orderType
        },
        timeout: 30000,
        success: function (data) {
            $('ul#bet_list li.loading').remove();
            if(data=="")
            {
            	$('div.mine-message').show();
            	$('ul#bet_list').html('');
            }else
            {
	            $('div.mine-message').hide();
				$('ul#bet_list').html(data);
				//$('ul#bet_list').append(txtHtml);            	
            }
            loadingHide();
            // loaded(); //解决iscroll自带 bug (bugID=1858)
        }
    });
}
$(function () {
	$('#group_list li a').live("click",function(event) {
        $('#group_list a').removeClass('on');
        //$('div.beet-tips').hide();
        $(this).addClass('on');
        $('span#order_type').text($(this).text());
        orderType = $(this).data('id');
        switch(orderType)
        {
        	case 0:
        		$('#play_list').html(window.play_list[0]);
        	break;
        	case 1:
        		$('#play_list').html(window.play_list[1]);
        	break;
        	case 2:
        		$('#play_list').html(window.play_list[2]);
        	break;
        	case 3:
        		$('#play_list').html(window.play_list[2]);
        	break;
        	case 99:
        		$('div.beet-tips').hide();
		        orderType = '';
		        getCoinList(2);
        		return;
        	break;
        }
    });
	$('#play_list li a').live("click",function(event) {
        $('#play_list a').removeClass('on');
        $('div.beet-tips').hide();
        $(this).addClass('on');
        $('span#order_type').text($(this).text());
        orderType = $(this).data('id');
        initLoading();
        getCoinList(2);
    });
    $('div.ui-bett-refresh').live("click",function() {
    	orderType = $('#play_list a.on').data('id');
        getCoinList(2);
    });

});
