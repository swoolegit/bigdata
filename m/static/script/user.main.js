function initLoading() {
    $('a.on-more').hide();
    $('div.mine-message').hide();
}

function showMessage(id) {
    loadingShow();
    $('a.on-more').html("正在获取数据中..");
    url="/user/message_receive_content?id="+id;
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: {
            'id' : id
        },
        timeout: 30000,
        success: function (data) {
            $('ul#bet_list li.loading').remove();
            if(data!="")
            {
		        $('.bet_list').hide();
		        $('.bet_revokeok').hide();
		        $('.bet_detail').show();
		        $('.bet_detail.scorllmain-content').html(data);
		        $('.fr.c-red.'+id).html('');
            }
            loadingHide();
            // loaded(); //解决iscroll自带 bug (bugID=1858)
        }
    });
}

function getMainList(more) {//1:更多，2:充值
    loadingShow();
    $('a.on-more').html("正在获取数据中..");
    url="";
    if($('#page_type').val()=="recharge")
    {
    	url="/user/recharge_search";
    }
    if($('#page_type').val()=="cash")
    {
    	url="/user/cash_search";
    }
    if($('#page_type').val()=="message")
    {
    	url="/user/message_receive_search";
    }
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: {
            'state' : orderType
        },
        timeout: 30000,
        success: function (data) {
            $('ul#bet_list li.loading').remove();
            if(data=="")
            {
            	$('div.mine-message').show();
            	$('ul#bet_list').html('');
            }else
            {
	            $('div.mine-message').hide();
				$('ul#bet_list').html(data);
				//$('ul#bet_list').append(txtHtml);            	
            }
            loadingHide();
            // loaded(); //解决iscroll自带 bug (bugID=1858)
        }
    });
}
$(function () {
	$('#group_list li a').live("click",function(event) {
		$('div.beet-tips').hide();
        $('#group_list a').removeClass('on');
        //$('div.beet-tips').hide();
        $(this).addClass('on');
        $('span#order_type').text($(this).text());
        orderType = $(this).data('id');
        initLoading();
        getMainList(2);
    });
    $('div.ui-bett-refresh').live("click",function() {
    	orderType = $('#group_list li a.on').data('id');
        getMainList(2);
    });
});
