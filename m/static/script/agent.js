$(function() {
	window.lottery = { 
		timer_last: [], // 事件ID列表
		timer_lobbyT: [], // 事件ID列表
		timer_lobbyKT: [], // 事件ID列表
		timer_lobby_count:[], // 事件ID列表
		timer_lobby_fresh:[], // 事件ID列表
		timer: {}, // 事件ID列表
		switcher: {}, // 开关列表
		play_Pl: {},
		play_title:'',
		sub_title:'',
	},	 
	// loadpage: 页面加载
	window.loadpage = {
		loading: null,
		init: function() {
			var dom_loading = $('#loading-page');
			loadpage.loading = {
				self: dom_loading,
				warp: dom_loading.find('.dialogue-warp'),
				error: dom_loading.find('.error'),
				container: $('#container_warp'),
			};
		},
		before: function(callback, text) {
			loadpage.loading.warp.text(text||'正在努力加载中，请稍后...');
			loadpage.loading.self.fadeIn(150,callback);
		},
		close: function(callback) {
			loadpage.loading.self.fadeOut(150,function() {
				if (callback) callback();
			});
		},
		call: function(error, success) {
			var $this = $(this);
			loadpage.close(function() {
				if (error) {
					$.error(error);
				} else {
					var container;
					$('a.on[target=ajax],#home').removeClass('on');
					if ($this.attr('container')) {
						container = $($this.attr('container'));
					} else {
						container = loadpage.loading.container;
					}
					var top = $this.attr('top') || true;
					if (top !== 'false') {
						$('body').animate({scrollTop:0}, 200, function() {
							container.fadeOut(150,function() {
								$(this).html(success).fadeIn(150);
							});
						});
					} else {
						container.fadeOut(150,function() {
							$(this).html(success).fadeIn(150);
						});
					}
				}
			});
		},
	};
	loadpage.init();
	// form_submit: 表单提交
	window.form_submit = {
		before: function(callback) {
			loadpage.before(callback, '加载中...');
		},
		call: function(error, success) {
			var $this = $(this);
			loadpage.close(function() {
				if (error) {
					$.error(error);
				} else {
					var container = $('.order-center');
					var top = $this.attr('top') || true;
					if (top !== 'false') {
						$('body').animate({scrollTop:0}, 500, function() {
							container.fadeOut(150,function() {
								$('.order-center').html(success).fadeIn();
							});
						});
					} else {
						container.fadeOut(150,function() {
							$('.order-center').html(success).fadeIn();
						});
					}
				}
			});
		},
	};
});