$(function() {
	// 玩法分类切换
	$('#group_list li a').live('click', lottery.group_tab);
	// 玩法切换
	$('#play_list li a').live('click', lottery.play_tab);
    $(window).bind("load",function(){
	    $('#beet-tips').live("click",function(event){
	    	if($(this).css('display')!='none'){
	    		event.stopPropagation();
	    	}
	    });
	    $(document).on("click",function() {
	        if($('.beet-tips').is(':visible')) {
	           $('.beet-tips').toggle();
	        }else {
                $('.beet-tips').css('display','none');
            }
	        if($('.beet-rig').is(':visible')) {
	           $('.beet-rig').toggle();
	        }else {
                $('.beet-rig').css('display','none');
            }
	    });
		$('#wrapper_1').bind('touchend', function () {
		            if($('.beet-rig').is(':visible')) {
		                $('.beet-rig').toggle();
		            }
		            else {
		                $('.beet-rig').css('display','none');
		            }
		            if($('.beet-tips').is(':visible')) {
		                $('.beet-tips').toggle();
		            }
		            else {
		                $('.beet-tips').css('display','none');
		            }
		});
		$('.bett-foot').bind('touchend', function () {
		            if($('.beet-rig').is(':visible')) {
		                $('.beet-rig').toggle();
		            }
		            else {
		                $('.beet-rig').css('display','none');
		            }
		            if($('.beet-tips').is(':visible')) {
		                $('.beet-tips').toggle();
		            }
		            else {
		                $('.beet-tips').css('display','none');
		            }
		});
    });
	// 位数一键全选
	$('#digit_select_all').live('click', function() {
		$('#wei-shu :checkbox').attr('checked', true);
	});
	// 文本框投注号码清空
	$('#clear_num_func').live('click', function() {
		$('#textarea-code').val('');
		lottery.calc_amount();
	});
	// 选号按钮点击事件
	$('#num-select input.code').live('click', function() {
		var call = $(this).attr('action');
		if (call && $.isFunction(call = lottery.select_funcs[call])) {
			call.call(this, $(this).parent());
		} else {
			if ($(this).is('.checked')) {
				$(this).removeClass('checked');
				if(game.type_type==11) //幸運28處理
				{
					if($('#num-select .pp').attr('length')=="3" && $('.code.checked').length<3) //特碼包三處理
					{
						$('.code').not('.checked').attr('disabled', false);
					}
				}	
			} else {

				if(lottery.play_Pl.value)
				{
					if (parseInt(lottery.play_Pl.value.maxBalls) > 0 && $(this).closest('.ch_numball').find('.checked').length + 1 > lottery.play_Pl.value.maxBalls) {
						$.error('最多只能选择<span class="btn btn-red">'+lottery.play_Pl.value.maxBalls+'</span>个号码');
					} else {
	
						$(this).addClass('checked');
					}
				}else
				{
					$(this).addClass('checked');
				}
				if(game.type_type==11) //幸運28處理
				{
					if($('#num-select .pp').attr('length')=="3" && $('.code.checked').length>=3) //特碼包三處理
					{
						$('.code').not('.checked').attr('disabled', true);
					}
				}
			}
		}
		// 重新计算总预投注数和金额
		lottery.prepare_bets();
	});
	// 选择追号投注
	$('#bets-cart li').live('click', function() {
		$(this).addClass('choosed').siblings('li.choosed').removeClass('choosed');
	});


	//	$('li').live('click', function() {
	//	alert(1);
//		$(this).addClass('choosed').siblings('li.choosed').removeClass('choosed');
	//});
	// 操作快速选号按钮点击事件
	$('#num-select input.action').live('click', function() {
		var call = $(this).attr('action');
		var pp = $(this).parent().parent().parent();
		$(this).addClass('on').siblings('.action').removeClass('on');
		if (call && $.isFunction(call = lottery.select_funcs[call])) {
			call.call(this, pp);
		} else if ($(this).is('.all')) { // 全: 全部选中
			$('input.code', pp).addClass('checked');
		} else if ($(this).is('.large')) { // 大: 选中5到9
			$('input.code.max', pp).addClass('checked');
			$('input.code.min', pp).removeClass('checked');
		} else if ($(this).is('.small')) { // 小: 选中0到4
			$('input.code.min', pp).addClass('checked');
			$('input.code.max', pp).removeClass('checked');
		} else if ($(this).is('.odd')) { // 单: 选中单数
			$('input.code.d', pp).addClass('checked');
			$('input.code.s', pp).removeClass('checked');
		} else if ($(this).is('.even')) { // 双: 选中双数
			$('input.code.s', pp).addClass('checked');
			$('input.code.d', pp).removeClass('checked');
		} else if ($(this).is('.none')) { // 清: 全不选
			$('input.code', pp).removeClass('checked');
		}
		var _this = this;
		var checkBalls = function (){
			if (parseInt(lottery.play_Pl.value.maxBalls) == 0) return false;
			var balls = $(_this).closest('.pp').find('.ch_numball').find('.checked'),i;
			if (balls.length  > lottery.play_Pl.value.maxBalls) {
				$.error('最多只能选择<span class="btn btn-red">'+lottery.play_Pl.value.maxBalls+'</span>个号码');
				i = 0;
				$.each(balls,function (){
					if (++i > lottery.play_Pl.value.maxBalls){
						$(this).removeClass('checked');
					}
				});
			}
		};
		checkBalls();
		lottery.prepare_bets();
	});
	// 点击查看选号
	$('#bets-cart td.code-list').live('click', function() {
		var data = $(this).parent().data('code');
		lottery.display_code_list(data);
	});
	// 预投注号码移除
	$('#bets-cart .del').live('click', function() {
		var $this = $(this);
		$('#bets-cart').fadeOut(0,function() {
			$this.closest('li').remove();
			$(this).fadeIn(0,function() {
				$('#zhuiHao').val(0);
				lottery.calc_amount();
			});
		});
	});
	// 胆拖模式: 选项卡切换
	$('#num-select .dantuo :radio').live('click', function() {
		var dom = $(this).closest('.dantuo');
		if (this.value) {
			dom.next().fadeOut(function() {
				$(this).next().fadeIn();
			});
		} else {
			dom.next().next().fadeOut(function() {
				$(this).prev().fadeIn();
			});
		}
	});
	// 胆拖模式: 胆码与拖码校验
	$('#num-select .dmtm :input.code').live('click', function(event) {
		var $this = $(this);
		var $dom = $this.closest('.dmtm');
		if ($('.code.checked[value=' + this.value +']', $dom).not(this).length == 1) {
			$this.removeClass('checked');
			$.error('选择胆码不能与拖码相同');
			return false;
		}
	});
	// 快3: 二同号单选处理
	$('#num-select .zhixu115 :input.code').live('click', function(event) {
		var $this = $(this);
		if (!$this.is('.checked')) return false;
		var $dom = $this.closest('.zhixu115');
		$('.code.checked[value=' + this.value +']', $dom).removeClass('checked');
		$this.addClass('checked');
	});
	// 录入式投注录入框键盘事件
	var prepare_add_code = false;
	$('#textarea-code')
	.live('keypress', function(event) {
		event.keyCode = event.keyCode||event.charCode;
		return !!(
			// 按Ctrl、Alt、Shift时有效
			event.ctrlKey || event.altKey || event.shiftKey
			// 回车键有效
			|| event.keyCode == 13
			// 退格键有效
			|| event.keyCode == 8
			// 空格键有效
			|| event.keyCode == 32
			// ;
			|| event.keyCode == 59
			// ,
			|| event.keyCode == 44
			// 数字键有效
			|| (event.keyCode >= 48 && event.keyCode <= 57)
		);
	})
	.live('keyup', lottery.prepare_bets)
	.live('change', function() {
		var str = $(this).val();
		if (/[^\d\s\r\n\,\;]+/.test(str)) {
			$.error('投注号码仅能由非数字、空格、换行组成');
			$(this).val('');
		}
	});
	// 模式切换
	$('#play-mod .danwei').live('click', lottery.mod_tab);
	// 变更投注倍数事件处理
	$('#beishu-value').live('change', lottery.prepare_bets);
	$('#beishu-warp .sur').live('click', function() {
		var dom = $('#beishu-value');
		var new_val = parseInt(dom.val()) - 1;
		if(new_val < 1) new_val = 1;
		dom.val(new_val);
		lottery.prepare_bets();
	});
	$('#beishu-warp .add').live('click', function() {
		var dom = $('#beishu-value');
		var new_val = parseInt(dom.val()) + 1;
		dom.val(new_val);
		lottery.prepare_bets();
	});
	// 追号处理
	//$('#btnZhuiHao').live('click', lottery.game_zhui_hao);
	var maxBeiShu = 100000;
	// 追号期数选择
	var zhuihao_data_func = function() {
		var $zhuihao = $('.zhuihao_box td input[type=checkbox]:checked');
		var num = $zhuihao.length;
		var amount = $('#zhuihao_amount').val();
		var total = (amount * num).toFixed(2);
		var beiShuAmount = 0;

		$.each($('.zhuihao_box td input[type=checkbox]:checked'), function (){
			var _this = $(this).parent().parent();
			beiShuAmount += parseFloat(_this.find('.beiShu-amount').html());
		});
		$('#zhuihao_num').text(num);
		$('#zhuihao_total').text(beiShuAmount.toFixed(2));
	};
	
	var zhuihaoChangeAmount = function (_this, betZjAmount, bonusProp, actionNum){
		var _this = $(_this).parent().parent();
		var beiShu = parseInt(_this.find('.beiShu').val()) || 1;
		if (beiShu > maxBeiShu) beiShu = maxBeiShu;
		_this.find('.beiShu').val(beiShu);
		var beiShuMode = parseFloat($('#beiShu-mode').val()) || 1;
		var amount = beiShu * beiShuMode * actionNum;
		var prompt;
		prompt ='';
		_this.find('input[type=checkbox]').attr('disabled',false);
		if ((beiShu * bonusProp * beiShuMode / 2) > parseInt(betZjAmount)) {
			_this.find('input[type=checkbox]').attr('disabled',true).attr('checked',false);
			prompt = '<span class="t btn btn-red">上限</span>';
		}

		_this.find('.beiShu-amount').html( amount.toFixed(2));
		_this.find('.bonusProp-amount').html( (beiShu * bonusProp * beiShuMode / 2).toFixed(2) );
		_this.find('.prompt').html( prompt  );
		
	};
	//倍投確定
	//$(document).on('click', '#beiTou-button', function (){
	var count_zhuihao=function(){
		var $setting = $('#fandian-value').data();
		//begin 不使用選號藍  by robert
		//var $code = $('#bets-cart li.choosed').data('code');
		var $code = $('#bets-cart li.code').data('code');
		//end 不使用選號藍  by robert
//		bonusProp
		
		var $beiTouStart = $('#beiTou-start');
		var $beiTouCycle = $('#beiTou-cycle');
		var $beiTouBeiShut = $('#beiTou-beiShu');
		
		var start = parseInt($beiTouStart.val());
		if (!start) {
			$beiTouStart.val(1);
			start = 1;
		}
		var cycle = parseInt($beiTouCycle.val());
		if (!cycle) {
			$beiTouCycle.val(1);
			cycle = 1;
		}
		var beiShu = parseInt($beiTouBeiShut.val());
		if (!beiShu) {
			$beiTouBeiShut.val(1);
			beiShu = 1;
		}

		var i=0;
		$.each($('.zhuihao_box input.beiShu'),function (){
			if (i%cycle == 0 && i > 0) {
				start = start * beiShu;
			}
			if (start >= maxBeiShu) start = maxBeiShu;
			this.value = start;
			i++;
			zhuihaoChangeAmount(this, $setting.betZjAmount, $code.bonusProp, $code.actionNum);
		});
		zhuihao_data_func();

	};
	//換倍數
	$(document).on('change','.dialogue-body input.beiShu', function (){
		var $setting = $('#fandian-value').data();
		var $code = $('#play-work-data .code.choosed').data('code');

		this.value = parseInt(this.value) || 1;		
		zhuihaoChangeAmount(this, $setting.betZjAmount, $code.bonusProp, $code.actionNum);
		zhuihao_data_func();
	});

	$('.zhuihao_box .form-control2').live('change', function() {
		lottery.zhuihao_load($('#beiTou-tcycle').val());
	});

	$('.zhuihao_box .form-control').live('change', function() {
		count_zhuihao();
	});

	$('.zhuihao_box td.choose_all').live('click', function() {
		$('.zhuihao_box td input:not(:disabled)').attr('checked', 'checked');
		zhuihao_data_func();
	});
	$('.zhuihao_box td input').live('click', zhuihao_data_func);
	// 玩法说明
	$('#game-play .play-info .showeg').live('mouseover',function() {
		var action = $(this).attr('action');
		var ps = $(this).position();
		$('#' + action).siblings('.play-eg').hide();
		$('#' + action).css({top: ps.top + 22, left: ps.left - 7}).fadeIn();
		
	});
	$('#game-play .play-info .showeg').live('mouseout',function() {
		$('#game-play .play-info .play-eg').hide();
	});


	// end	
});