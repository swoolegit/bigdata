
function showDetail(key) {
    var detail = $('a.'+key).data('detail');
    if (detail == undefined || detail == null || detail == '') {
        return;
    }
    /*
    var reg = /close=([0-9]*)/;
    var arr = detail.match(reg);
    if ($('a.'+key).data('needrevoke') == 0 || arr[1] != 0) {//已关盘或已开奖
        location.href = '/mine/betDetail.html?'+detail;
        showStep('list');
        $('div.beet-tips').hide();
        return;
    }
    */
    var posText = '';
    var gameid = '';
    var gname = '';
    var pname = '';
    var subid = '';
    var period = '';
    var opennum = '';
    var winamount = '';
    var codeArr = '';
    var reward = '';
    var win = '';
    var close = '';
    var key = '';
    var status = '';
    var detailArr = detail.split('&');
    for (var i = 0; i < detailArr.length; i++) {
        var tmpArr = detailArr[i].split('=');
        if (tmpArr[0] == 'reward') {
            tmpArr[1] = (tmpArr[1]*100).toFixed(1)+'%';
        }
        $('span#bet_'+tmpArr[0]).text(tmpArr[1]);
        switch (tmpArr[0]) {
            case 'pos':
                posText = tmpArr[1];
                break;
            case 'gid':
                gameid = tmpArr[1];
                break;
            case 'subid':
                subid = tmpArr[1];
                break;
            case 'code':
                codeArr = tmpArr[1];
                break;
            case 'period':
                period = tmpArr[1];
                break;
            case 'gname':
                gname = tmpArr[1];
                break;
            case 'pname':
                pname = tmpArr[1];
                break;
            case 'opennum':
                opennum = tmpArr[1];
                break;
            case 'winamount':
                winamount = tmpArr[1];
                break;
            case 'win':
                win = tmpArr[1];
                break;
            case 'close':
                close = tmpArr[1];
                break;
            case 'key':
                key = tmpArr[1];
                break;
            case 'status':
                status = tmpArr[1];
                break;
            default:
            // ...
        }
    }
    //显示投注号码
    //开奖号码
    var showOpen = '';
    if(opennum != undefined && opennum != null && opennum != '') {
        var openArr = opennum.split(',');
        	if(gameid=='65')
        	{
        		var color_ball="";
        		/*
        		if($.inArray( openArr[3], window.ball28.color_red )>-1)
        		{
        			color_ball="class='ball28_red'";
        		}
        		if($.inArray( openArr[3], window.ball28.color_green )>-1)
        		{
        			color_ball="class='ball28_green'";
        		}
        		if($.inArray( openArr[3], window.ball28.color_blue )>-1)
        		{
        			color_ball="class='ball28_blue'";
        		}
        		*/
        		showOpen = '<span>'+openArr[0]+'</span> + <span>'+openArr[1]+'</span> + <span>'+openArr[2]+'</span> = <span '+color_ball+' style="width:120px;">'+openArr[3]+'</span>';
        	}else
        	{
	           for (var tmp in openArr) {
	                showOpen += '<span>'+openArr[tmp]+'</span>';
	           }        		
        	}
    } else {
        showOpen = '未开奖';
    }
    $('div#open_num').html(showOpen);
    //已中奖
    $('div#win_amt span').text('已中奖，赢'+winamount+'元');
    if (/[0-9\.]+/.test(winamount) && winamount > 0) {
        $('div#win_amt').show();
    } else {
        $('div#win_amt').hide();
    }
    $('img#game_logo').attr('src',resUrl+'/'+gameid+'.png');
    $('span.f120').text(gname);
    $('span.f80').text('第'+period+'期');
	codeArr='<li><div>选码：'+codeArr+'</div></li>';
    $('ul#code').html(codeArr);
    //取消、返回
    if ($('a.'+key).data('needrevoke') == 0  || close != 0) {
        $('button.order-btn').data('key','');
        $('button.order-btn').data('gid',gameid);
        $('button.order-btn').text('再来一注');
    } else {
        $('button.order-btn').data('key',key);
        $('button.order-btn').text('取消投注');
    }
    //状态
    if (opennum == '' ) {
        status = '未开奖';
    } else if (win == 1) {
        status = '中奖';
    } else if (win == 2) {
        status = '已撤单';
    } else {
        status = '未中奖';
    }
    $('span#bet_status').text(status);
    $('span#bet_close').text((close == '' || close == 0) ? '否' : '是');
}

function doConfirmOk() {
    var key = $('#revokeKey').val();
    loadingShow();
    $.ajax({
        url: '/bet/remove_single',
        type: 'GET',
        dataType: 'json',
        data: {
            'id' : key
        },
        timeout: 30000,
        success: function (data) {
            loadingHide();
            //$('#revokeKey').val('');
            if (data.Result == false) {
                msgAlert(data.Desc);
                //reLogin(data.Desc);
                return;
            }
            $('span#win_state').text('手动退码');
            //改页面参数
            console.log($('a.'+key+' div.c-gary span').html());
            $('a.'+key).data('needrevoke','0');
            $('a.'+key).data('detail',$('a.'+key).data('detail').replace(/status=[0-9]*/g,'status=2').replace(/win=[0-9]*/g,'win=2'));
            $('a.'+key+' div.c-gary span').text('已取消');
            showStep('revokeok');
        }
    });
}

function initLoading() {
    $('a.on-more').hide();
    $('div.mine-message').hide();
}

function getBetList(more) {//1:更多，2:充值
    loadingShow();
    $('a.on-more').html("正在获取数据中..");
    $.ajax({
        url: '/bet/log_search',
        type: 'POST',
        dataType: 'html',
        data: {
            'state' : orderType
        },
        timeout: 30000,
        success: function (data) {
            $('ul#bet_list li.loading').remove();
            if(data=="")
            {
            	$('div.mine-message').show();
            	$('ul#bet_list').html('');
            }else
            {
	            $('div.mine-message').hide();
				$('ul#bet_list').html(data);
				//$('ul#bet_list').append(txtHtml);            	
            }
            loadingHide();
            // loaded(); //解决iscroll自带 bug (bugID=1858)
        }
    });
}

$(function () {
    $('button.order-btn').click(function() {
        var key = $(this).data('key');
        if (key != undefined && key != null && key != '') {
            $('#revokeKey').val(key);
            msgConfirm('是否确认要撤单？');
        } else {//非撤单，显示详情
        	var gameid = $(this).data('gid');
            if (gameid != undefined && gameid != null && gameid != '') {
				var url = '/game/index?id='+gameid;
				location.href = url;
            }
        }
    });

    $('div.beet-tips a').click(function() {
        $('div.beet-tips a').removeClass('on');
        $('div.beet-tips').hide();
        $(this).addClass('on');
        $('span#order_type').text($(this).text());
        orderType = $(this).data('type');
        initLoading();
        getBetList(2);
    });
    $('div.ui-bett-refresh').live("click",function() {
    	orderType = $('div.beet-tips a.on').data('type');
		getBetList(2);
    });

    //getBetList();
});
