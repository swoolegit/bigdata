<?php
// [天合寶]支付接口
class pay_tianhebao
{

    //real use!
    private $partner = '1565'; //商戶號
    private $merchantKey = '7053a66c5b924fed8896c50e42e085b0'; //密鑰
/*
0000	交易成功
1001	商户号未提交
1002	订单号未提交
1003	MD5摘要未提交
1004	查询商户信息异常,请稍后再试
1005	无商户信息
1006	MD5验证失败
1007	查询订单错,请稍后再试
1008	未找到订单信息
1009	返回信息数字签名失败
1010	返回信息MD5摘要失败
1011	当前订单状态不允许退款
1012	订单数据异常，请联系移联
1013	查询订单支付记录错,请稍后再试
1014	未找到订单的支付信息，请联系移联
1015	记录退款申请失败，请稍候再试
1016	记录退款交易失败，请稍候再试
1017	更新订单退款金额失败，请稍候再试
1020	更新银行支付记录状态失败，请稍候再试
1021	记录流水失败，请稍候再试
1030	退款金额未提交
1031	发送时间未提交
1032	退款金额过大，订单可退金额不足
1033	查询退款申请异常
1036	商户退款流水号未提交
1037	查询退款数据异常


*/

    // 银行代码

    private $banks = array(

		'1' => '992', // 支付寶 v
        '19' => '1004', // 微信/财付通 v
		'3' => '985', // 广东发展银行v
        '4' => '982',  // 华夏银行v
        '5' => '981', // 交通银行 v
        '6' => '978', // 平安银行v
        '7' => '977', // 上海浦东发展银行v
        '9' => '971', // 中国邮政储蓄银行v
        '10' => '986', // 中国光大银行v
		'11' => '967', // 中国工商银行 v
        '12' => '965', // 中国建设银行 v
        '13' => '980', // 中国民生银行v
        '14' => '964', // 中国农业银行 v
        '15' => '963', // 中国银行 v
        '16' => '970', // 招商银行v
        '17' => '962', // 中信银行v
		'8' => '972', // 兴业银行 v
	);


    /**
     * @name 支付方法
     * @param int bankid 银行ID
     * @param int amount 充值金额
     * @param string orderid 订单ID
     * @param string url_callback 回调地址
     * @param string url_return 充值完成后返回地址
     */
    public function pay($bankid, $amount, $orderid, $url_callback, $url_return)
    {

		//$product = ['QQ币', '会员充值', '移动充值', '联通充值', '电信充值', '话费', '水电费'];

		if(!empty($_SERVER['HTTP_CLIENT_IP'])){
			$userIP = $_SERVER['HTTP_CLIENT_IP'];
		}else if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$userIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
			$userIP = $_SERVER['REMOTE_ADDR'];
		}
		
		$product = ['Member Recharge', 'Mobile Recharge'];
		shuffle($product);
		$parameter = array(
			//'version'	=> 'v1',
            'parter'	=> $this->partner,
			'type'	=> $this->banks[$bankid],
			//'type'	=> 1006,
            'value'	=> sprintf("%.2f", $amount),
            'orderid'	=> $orderid,
            'callbackurl'	=> $url_callback,

        );

		$sign = '';
		foreach ($parameter as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$sign .= $this->merchantKey;

		$parameter['hrefbackurl'] = $url_return;
		$parameter['payerIp'] = $userIP;
		$parameter['attach'] = $product[0];

		$parameter['sign'] = md5($sign);

        core::logger($parameter);
		$middleUrl = MIDDLE_URL.(str_replace('pay_','',__CLASS__)).'.php';

		//支付寶或微信
//		if($bankid == 1 || $bankid == 19) {

//			$response = core::lib('middle')->onlinePay($parameter,$middleUrl);
//			core::logger(['response' => trim($response)]);


//			$json = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', trim($response)), true );
//			return $json;
//		}
//		else {
			//使用中轉
			core::lib('middle')->bankPay($parameter,$middleUrl);
			exit;
			$html = '';
			$html .= '<html>';
			$html .= '<head>';
			$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
			$html .= '</head>';
			$html .= '<body onLoad="document.pay.submit();">';
			$html .= '正在跳转 ...';

			//$html .= '<form name="pay" method="GET" action="https://pay.newpaypay.com/servlet/InitBankLogoServlet">';
			$html .= '<form name="pay" method="GET" action="https://pay.newpaypay.com/center/proxy/partner/v1/pay.jsp">';
			foreach ($parameter as $key => $val) {
				$html .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />';
			}
			$html .= '<script type="text/javascript">document.pay.submit();</script>';
			$html .= '</form>';
			$html .= '</body>';
			$html .= '</html>';
			return $html;
//		}
    }


	//反查訂單，以防假單注入
	function checkOrder($orderid) {

		$parameter = array(
            'orderid'	=> $orderid,
            'parter'	=> $this->partner,
        );
		$sign = '';
		foreach ($parameter as $key => $val) {
			if ($sign) $sign .= '&';
			$sign .= $key.'='.$val;
		}
		$sign .= $this->merchantKey;

		$parameter['sign'] = md5($sign);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "http://pay.zhongyishentu.com/search.aspx");
		curl_setopt($curl, CURLOPT_POST, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query( $parameter ));
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		$response = curl_exec($curl);
		curl_close($curl);
		parse_str($response,$output);
		return $output;
		/*
		I：未支付，默认值(Init)
商户订单号	orderid	Y	请求的商户订单号
订单结果	opstate	Y	3：请求参数无效
2：签名错误
1：商户订单号无效
0：支付成功 
其他：用户还未完成支付或者支付失败
订单金额	ovalue	Y	订单实际金额，单位元
MD5签名	sign	-	32位小写MD5签名值，GB2312编码

		*/
		//$json = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true );
		//return $json;
	}

    #	取得返回串中的所有参数
    function verifyCallbackRequest()
    {
/*
商户订单号	orderid	Y	上行过程中商户系统传入的orderid。
订单结果	opstate	Y	0：支付成功
-1 请求参数无效
-2 签名错误
订单金额	ovalue	Y	订单实际支付金额，单位元
MD5签名	sign	-	32位小写MD5签名值，GB2312编码
天合宝订单号	sysorderid	N	此次订单过程中天合宝接口系统内的订单Id
天合宝订单时间	completiontime	N	此次订单过程中天合宝接口系统内的订单结束时间。格式为
年/月/日 时：分：秒，如2010/04/05 21:50:58
备注信息	attach	N	备注信息，上行中attach原样返回
订单结果说明	msg	N	订单结果说明
orderid={}&opstate={}&ovalue={}key
*/

		$verifyString = "orderid={$_GET['orderid']}&";
		$verifyString .= "opstate={$_GET['opstate']}&";
		$verifyString .= "ovalue={$_GET['ovalue']}";
		$verifyString .= $this->merchantKey;
        return	md5($verifyString) === $_GET['sign'];
    }


    /**
     * @name 回调方法
     */
    public function callback($returnDeviceType = 'pc')
    {

		$log = [
			'GET' => json_encode($_GET),
			'POST' => json_encode($_POST),
		];

        core::logger($log);

		if (!$this->verifyCallbackRequest()) {
            die('認證錯誤');
		}

		//付款成功
        if ($_GET['opstate'] === '0') {

			//反查訂單
			$orderId = intval($_GET['orderid']);
			$result = $this->checkOrder($orderId);
			if($result['opstate'] !== '0' || $result['orderid'] !== $_GET['orderid'] ||  $result['ovalue'] !== $_GET['ovalue']) {
				die('非法请求');
			}

			$sign = "orderid={$result['orderid']}&opstate={$result['opstate']}&ovalue={$result['ovalue']}".$this->merchantKey;
			if (md5($sign) != $result['sign']) {
				core::logger(array(
					'ALARM' => '反查訂單完成，回傳MD5簽名錯誤',
					'sign' => $sign,
					'result' => json_encode($result)
				));
				die('非法请求2');
			}
			$amount = floatval($result['ovalue']);
            $orderReturn = '';

			require_once(SYSTEM . "/core/pay.core.php");
            $pay = new pay();
            $pay->call($amount, $orderId, $orderReturn);

            $pay_call = array(
               'PAY_CORE' => 'pay.core.php-->call',
               'amount' => $amount,
               'orderId' => 'orderId='.$orderId,
               'orderReturn' => $orderReturn,
            );
            core::logger($pay_call);
			return;

        } else {
            echo '充值失败';
            return;
        }
    }

}
