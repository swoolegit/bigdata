<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="stylesheet" href="/static/theme/default/css/index.css" type="text/css">
    <link href="/static/theme/default/css/m.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/static/theme/default/css/user.css" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        body.login-bg{padding-top:97px;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>
<title>Error</title>
</head>
<body>
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">    
                <div class="bett-tit-noborder">
                    大数据
                </div>
            </div>
        </h1>
        <div class=" header-icon">
            <a class="header-logout" href="/user/logout"></a>
        </div>
    </div>
</div>

<div id="error" style="width: 98%; border: 1px solid #C2C2C2;border-radius: 4px; text-align: center;margin: auto;margin-top: 60px;">
	<div class="head" style="border-bottom: 1px solid #eee;padding: 12px;font-size: 19px;">
		<div class="name">200 . Session Error</div>
	</div>
	<div class="body">
		<div class="container error" style="padding: 12px;font-size: 16px;">
			<div class="errmsg"><?php echo $msg;?></div>
		</div>
	</div>
</div>

<div class="btn-wrap">
	<button type="button" class="more-btn" onclick="reLogin()">重新登录</button>
</div>
</body>
<script>
function reLogin() {
	window.top.location.href = "<?php echo $login_url ?>";
}
</script>
</html>
