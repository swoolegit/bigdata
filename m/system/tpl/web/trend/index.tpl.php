<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link href="/static/theme/<?=THEME;?>/css/m.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link href="/static/theme/<?=THEME;?>/css/line.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
	<script language="javascript" type="text/javascript" src="/static/script/jquery.1.7.2.min.js"></script>
	<script language="javascript" type="text/javascript" src="/static/script/line/line.js"></script>
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>

    <title>首页</title>
</head><script type="text/javascript" id="useragent-switcher">
navigator.__defineGetter__("userAgent", function() {return "Mozilla/5.0 (Android 4.4; Mobile; rv:18.0) Gecko/18.0 Firefox/18.0"})</script>
<body>
<body >
<div class="header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl()">reveal</button>
        </div>
        <h1 class="ui-betting-title">
            <div class="bett-top-box">
                    <div class="bett-play">玩法</div>
                    <div class="bett-tit"><span id="msg_type">基本走势</span><!--<i class="bett-attr"></i>--></div>
            </div>
        </h1>
        <div class="ui-bett-right trend-icon">
            <a class="bett-heads" href="javascript:;" ></a>
        </div>
    </div>
</div>

<div id="wrapper_1" class="scorllmain-content scorllmain-Beet nobottom_bar" style="padding-top: 44px; padding-bottom: 44px;">
<div class="sub_ScorllCont" id="trend_list" >
<div id="trend-pos" class="trend-tit">
<?php
        switch($type)
        {
        	case 1:
        		$html='<span data-pos="1" class="on">万</span><span data-pos="2">千</span><span data-pos="3">百</span><span data-pos="4">十</span><span data-pos="5">个</span>';
        		echo $html;
        	break;
        	case 3:
        		$html='<span data-pos="1" class="on">百</span><span data-pos="2">十</span><span data-pos="3">个</span>';
        		echo $html;
        	break;
        	case 6:
        		$html='<span data-pos="1" class="on">一位</span><span data-pos="2">二位</span><span data-pos="3">三位</span><span data-pos="4">四位</span><span data-pos="5">五位</span><span data-pos="6">六位</span><span data-pos="7">七位</span><span data-pos="8">八位</span><span data-pos="9">九位</span><span data-pos="10">十位</span>';
        		echo $html;
        	break;
        	case 9:
        		$html='<span data-pos="1" class="on">百</span><span data-pos="2">十</span><span data-pos="3">个</span>';
        		echo $html;
        	break;
        }
?>
</div>
<div id="container" class="trend-content" style="margin-bottom: 80px;">
<table id="trend-content" width="100%" cellpadding="0" cellspacing="0" border="0" >
      <tbody><tr id="title" class="trend-titbg">
			<th><strong>期号</strong></th>
<?php
switch($type)
{
	case 1:
		echo '
			<th  align="center"><strong>0</strong></th>
			<th  align="center"><strong>1</strong></th>
			<th  align="center"><strong>2</strong></th>
			<th  align="center"><strong>3</strong></th>
			<th  align="center"><strong>4</strong></th>
			<th  align="center"><strong>5</strong></th>
			<th  align="center"><strong>6</strong></th>
			<th  align="center"><strong>7</strong></th>
			<th  align="center"><strong>8</strong></th>
			<th  align="center"><strong>9</strong></th>
			';
	break;
	case 3:
		echo '
			<th  align="center"><strong>0</strong></th>
			<th  align="center"><strong>1</strong></th>
			<th  align="center"><strong>2</strong></th>
			<th  align="center"><strong>3</strong></th>
			<th  align="center"><strong>4</strong></th>
			<th  align="center"><strong>5</strong></th>
			<th  align="center"><strong>6</strong></th>
			<th  align="center"><strong>7</strong></th>
			<th  align="center"><strong>8</strong></th>
			<th  align="center"><strong>9</strong></th>
			';
	break;
	case 6:
		echo '
			<th  align="center"><strong>1</strong></th>
			<th  align="center"><strong>2</strong></th>
			<th  align="center"><strong>3</strong></th>
			<th  align="center"><strong>4</strong></th>
			<th  align="center"><strong>5</strong></th>
			<th  align="center"><strong>6</strong></th>
			<th  align="center"><strong>7</strong></th>
			<th  align="center"><strong>8</strong></th>
			<th  align="center"><strong>9</strong></th>
			<th  align="center"><strong>10</strong></th>
			';
	break;
	case 9:
		echo '
			<th  align="center"><strong>1</strong></th>
			<th  align="center"><strong>2</strong></th>
			<th  align="center"><strong>3</strong></th>
			<th  align="center"><strong>4</strong></th>
			<th  align="center"><strong>5</strong></th>
			<th  align="center"><strong>6</strong></th>
			';
	break;

}
	
?>

            </tr>
<?php
			$number_start=0;
			$number_end=9;
			switch($type)
			{
				case 6:
					$number_start=1;
					$number_end=10;
				break;
				case 9:
					$number_start=1;
					$number_end=6;
				break;
			
			}
			$five=array();
			for($i=0;$i<=10;$i++) //万位
			{
				$five['LW'.$i]=0;
				$five['SW'.$i]=0;
				$five['ZW'.$i]=0;
				$five['MW'.$i]=0;
				$five['MLCW'.$i]=0;
				$five['LCW'.$i]=0;
			}
			if($data) foreach($data as $k=>$var)
			{
  				$dArry=explode(",",$var['data']);
				$number=$dArry[intval($trendPos)-1];				
				if($k%2==0)
				{
					echo '<tr >';
				}else
				{
					echo '<tr class="trend-bg">';
					
				}
				echo '<td>'.$var['number'].'</td>';
				
				for($i=$number_start;$i<=$number_end;$i++) //万位
				{
					if($i==intval($number)){
						echo '<td class="charball" align="center"><div class="cur">'.$number.'</div></td>';
						$five['LW'.$i]=0;  //遗漏
						if($five['SW'.$i]){$five['SW'.$i]++;}else{$five['SW'.$i]=1;} //出现总次数
						if($five['LCW'.$i]){$five['LCW'.$i]++;}else{$five['LCW'.$i]=1;} //最大连出值
					}else{
						if($five['LW'.$i]){$five['LW'.$i]++;}else{$five['LW'.$i]=1;}
						echo '<td  align="center"><div >'.$five['LW'.$i].'</div></td>';
						$five['LCW'.$i]=0;
					}
					//遗漏总计
					if($five['ZW'.$i]){$five['ZW'.$i]+=$five['LW'.$i];}else{$five['ZW'.$i]=$five['LW'.$i];}
					//最大遗漏值
					if($five['MW'.$i]<$five['LW'.$i]){$five['MW'.$i]=$five['LW'.$i];}
					//最大连出值
					if($five['MLCW'.$i]<$five['LCW'.$i]){$five['MLCW'.$i]=$five['LCW'.$i];}

				}

				echo '</tr>';
			}
?>
    <tr class="trend-titbg">
    <th >总次数</th>
    <?php
    $var='W';
	for($i=$number_start;$i<=$number_end;$i++)
	{
		if(isset($five['S'.$var.$i])){
			$five['D'.$var.$i]=$five['S'.$var.$i];
		}else{
			$five['D'.$var.$i]=0;
		}
		echo '<th align="center">'.$five['D'.$var.$i].'</th>';
	}
	?>
    </tr>
    <tr>
    <td nowrap="">平均遗漏</td>
    <?php
    $pgs=30;
	for($i=$number_start;$i<=$number_end;$i++)
	{
		$five['P'.$var.$i]=intval($pgs/($five['D'.$var.$i]+1));
		echo '<td align="center">'.$five['P'.$var.$i].'</td>';
	}
	?>
    </tr>
    <tr>
    <td nowrap>最大遗漏</td>
    <?php
		for($i=$number_start;$i<=$number_end;$i++)
		{
			if(isset($five['M'.$var.$i])){
				$five['Max'.$var.$i]=$five['M'.$var.$i];
			}else{
				$five['Max'.$var.$i]=0;
			}
			echo '<td align="center">'.$five['Max'.$var.$i].'</td>';
		}
	?>
    </tr>
</tbody></table>
</div>
</div>
</div>

<!-- 走势图彩种tips -->
<div class="trend-tips" hidden="" style="display: none;">
    <div class="trtip-tit"><i class="tr-icon"></i>选择彩种</div>
        <ul>
<?php
$lottery_type=array();
foreach($type_data as $k=>$var)
{
	$class="";
	if($var['id']==$type_id)
	{
		$class="trend-on";
	}
	echo '<li class="'.$class.'" data-gid="'.$var['id'].'" >'.$var['shortName'].'</li>';
	$lottery_type[]=$var['id'].":".$var['type'];	
}
?>                  
    </ul>
</div>
<?php require(TPL.'/index_foot.tpl.php');?>
<div id="tip_bg" class="tips-bg" style="display: none;"></div>
<script type="text/javascript">window.onerror=function(){return true;}</script>
<script language="javascript">
fw.onReady(function(){
	Chart.init();
	DrawLine.bind("trend-content","has_line");
	DrawLine.color('#BFA917');
	DrawLine.add(1,1,10,1);
	DrawLine.draw(Chart.ini.default_has_line);
	if($("#trend-content").width()>$('body').width())
	{
	   $('body').width($("#trend-content").width() + "px");
	}
	$("#container").height($("#trend-content").height() + "px");
});
</script>
<script>
    trendGid = <?=$type_id?>;
    trendCount = 20;
    trendPos = 1;
    noStr = '0|1|2|3|4|5|6|7|8|9';
<?php
	echo "var lottery_type={".implode(',', $lottery_type)."};"
?>

    function getTrendData() {
        if (trendGid == null || trendGid == undefined || trendGid == '') {
            trendGid = 1;
        }
        /*		
		var url = '/trend/trend_list?trendGid=' + trendGid + 'trendPos=' + trendPos;
		$.load(url, '#container', {
			show: null,
			callback: function() {
				//$('#group_list li a.on').removeClass('on');
				//$this.addClass('on');
			},
		});
		*/
        $.ajax({
            url: '/trend/trend_list',
            type: 'GET',
            dataType: 'html',
            data: {
                'trendGid' : trendGid,
                'trendPos' : trendPos
            },
            timeout: 30000,
            success: function (data) {
            	$('#container').html(data);
            }
        });	
    };

    $('.bett-top').click(function(event){
        event.stopPropagation();
    });
    $('.trend-icon').click(function(){
        $('.trend-tips').show();
        $('.tips-bg').show();
    });

    $('.tips-bg').click(function(){
        $('.trend-tips').hide();
        $('.tips-bg').hide();
    })
    $('div.trend-tips li').click(function(){
        $('div.trend-tips li').removeClass('trend-on');
        $(this).addClass('trend-on');
        $('.trend-tips').hide();
        $('.tips-bg').hide();
        trendGid = $(this).data('gid');
        trendPos = 1;
        //需要切换菜单，位置信息选项
        
        switch(lottery_type[trendGid])
        {
        	case 1:
        		html='<span data-pos="1" class="on">万</span><span data-pos="2">千</span><span data-pos="3">百</span><span data-pos="4">十</span><span data-pos="5">个</span>';
        		$('#trend-pos').html(html);
        	break;
        	case 3:
        		html='<span data-pos="1" class="on">百</span><span data-pos="2">十</span><span data-pos="3">个</span>';
        		$('#trend-pos').html(html);
        	break;
        	case 6:
        		html='<span data-pos="1" class="on">一位</span><span data-pos="2">二位</span><span data-pos="3">三位</span><span data-pos="4">四位</span><span data-pos="5">五位</span><span data-pos="6">六位</span><span data-pos="7">七位</span><span data-pos="8">八位</span><span data-pos="9">九位</span><span data-pos="10">十位</span>';
        		$('#trend-pos').html(html);
        	break;
        	case 9:
        		html='<span data-pos="1" class="on">百</span><span data-pos="2">十</span><span data-pos="3">个</span>';
        		$('#trend-pos').html(html);
        	break;
        }
        getTrendData();
    });
    
    $('div.trend-tit span').live("click",function () {
        //event.preventDefault();
        $('div.trend-tit span').removeClass('on');
        $(this).addClass('on');
        trendPos = $(this).data('pos');
        getTrendData();
    });
    
function goUrl() {
    var url = '/game/index?id='+trendGid;
    location.href = url;
}	
</script>
</body>
</html>