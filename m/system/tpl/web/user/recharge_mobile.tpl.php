<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/common.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/function.js?v=<?php echo $this->version;?>"></script>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/lobby.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
	<link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>
    <title>充值</title>
</head>
<body style="background: #252944;">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl(1)">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">
                <div class="bett-tit-noborder">
					充值
                </div>
            </div>
        </h1>
        <div class=" header-icon">

        </div>
    </div>
</div>

<?php
$total=0;
$width='';
	$style = '';
if ($this->config['cashFlowQQpay'] != 'close') $total++;
if ($this->config['cashFlowBank'] != 'close') $total++;
if ($this->config['cashFlowQuickpay'] != 'close') $total++;
if ($this->config['cashFlowWechat'] != 'close') $total++;
if ($this->config['cashFlowAlipay'] != 'close') $total++;
if ($this->config['cashFlowJDpay'] != 'close') $total++;
if ($total >= 6) {
	$width = '20';
	$style = 'padding-top: 44px;';
}
else {
	$width = (100/$total);
	$style = 'padding-top: 0px;';

}
?>

<style>
.recharge_item {width:<?=$width;?>%;}
</style>

<div style="padding-top: 44px;" >
	<div style="width: 100%">
<?php if ($this->config['cashFlowQQWAPpay'] != 'close') { ?>
		
		<a href="javascript:;" style="white-space: nowrap; text-overflow: ellipsis;"><div class="recharge_item" data-type="8">QQ手机</div></a>
<?php } ?>
<?php /*if ($this->config['cashFlowQQpay'] != 'close') { ?>
		<a href="javascript:;" style="white-space: nowrap; text-overflow: ellipsis;"><div class="recharge_item recharge_active" data-type="4">QQ钱包</div></a>
<?php }*/ ?>
<?php if ($this->config['cashFlowBank'] != 'close') { ?>
		<a href="javascript:;" style="white-space: nowrap; text-overflow: ellipsis;"><div class="recharge_item" data-type="3">银行支付</div></a>
<?php } ?>
<?php if ($this->config['cashFlowQuickpay'] != 'close') { ?>
		<a href="javascript:;" style="white-space: nowrap; text-overflow: ellipsis;"><div class="recharge_item" data-type="5" >快捷支付</div></a>
<?php } ?>

<?php if ($this->config['cashFlowWechatWAPpay'] != 'close') { ?>
		<a href="javascript:;" style="white-space: nowrap; text-overflow: ellipsis;"><div class="recharge_item" data-type="7">微信手机</div></a>
<?php } ?>
<?php /*if ($this->config['cashFlowWechat'] != 'close') { ?>
		<a href="javascript:;" style="white-space: nowrap; text-overflow: ellipsis;"><div class="recharge_item" data-type="1">微信</div></a>
<?php } */?>
<?php if ($this->config['cashFlowAlipay'] != 'close') { ?>
		<a href="javascript:;" style="white-space: nowrap; text-overflow: ellipsis;"><div class="recharge_item" data-type="2">支付宝</div></a>
<?php } ?>
<?php if ($this->config['cashFlowJDpay'] != 'close') { ?>
		<a href="javascript:;" style="white-space: nowrap; text-overflow: ellipsis;"><div class="recharge_item" data-type="6">京东</div></a>
<?php } ?>



	</div>
</div>

<div id="recharge-box"  style="<?=$style?>" >
<!-- begin 微信 -->
	<div class="recharge-tabs hide">
		<!--
		<form action="/user/pay" target="ajax" func="form_submit" container="#qrCodesWepay">
		-->
		<?php if ($this->config['cashFlowWechat'] == 'niufu'|| $this->config['cashFlowWechat'] == 'aifu') { ?>
		<form action="/user/pay" method="post" target="ajax" func="form_submit" container="#qrCodesWepay">
		<?php } else { ?>
		<form action="/user/pay" method="post" target="_blank" func="form_submit">
		<?php } ?>

			<input type="hidden" name="bankid" value="24">
				<div class="Tips">
					<p>尊敬的会员您好.</p>
					<p>微信充值时间为:全天24小时.</p>
					<p class="red">微信充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
					<p>请您在:1分内完成充值,如过期,请勿继续充值.</p>
					<p><br/></p>
				</div>

				<div class="item">
					<span class="name">充值金额</span><span class="value amount"><input type="tel" name="amount"  required title="金额" placeholder="输入金额"></span>
				</div>

				<div class="btn-wrap">
					<button type="submit" class="more-btn" id="coinpassword">充值</button>
				</div>
		</form>
		<div id="qrCodesWepay" class="QrcodeArea"></div>
	</div>
<!-- end 微信 -->

<!-- begin 支付宝 -->
	<div class="recharge-tabs hide">
	<!--
		<form action="/user/pay" target="ajax" func="form_submit" container="#qrCodesAlipay">
	-->
	<?php if ($this->config['cashFlowAlipay'] == 'niufu' || $this->config['cashFlowAlipay'] == 'aifu') { ?>
	<form action="/user/pay" method="post" target="ajax" func="form_submit" container="#qrCodesAlipay">
	<?php } else { ?>
	<form action="/user/pay" method="post" target="_blank" func="form_submit">
	<?php } ?>
	        <input type="hidden" name="bankid" value="1">
				<div class="Tips">
					<p>尊敬的会员您好.</p>
					<p>支付宝充值时间为:全天24小时.</p>
					<p class="red">支付宝充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
					<p>请您在:1分内完成充值,如过期,请勿继续充值.</p>
					<p><br/></p>
				</div>

				<div class="item">
					<span class="name">充值金额</span><span class="value amount"><input type="tel" name="amount"  required title="金额" placeholder="输入金额"></span>
				</div>

				<div class="btn-wrap">
					<button type="submit" class="more-btn" id="coinpassword">充值</button>
				</div>
		</form>
		<div id="qrCodesAlipay" class="QrcodeArea"></div>
	</div>
<!-- end 支付宝 -->

<!-- begin 银行 -->
	<div class="recharge-tabs hide">
		<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
		<input type="hidden" id="bank-id" name="bankid" value="<?=$bank_default['id'];?>">
				<div class="Tips">
					<p>尊敬的会员您好.</p>
					<p>请选择下列银行其一再按充值</p>
					<p class="red">银行支付充值金额为:￥<?php echo $this->config['rechargeMin']; ?> - ￥<?php echo $this->config['rechargeMax']; ?>元.</p>
					<p><br/></p>
				</div>

				<div class="item">
					<span class="name">充值金额</span><span class="value amount"><input type="tel" name="amount" required title="金额" placeholder="输入金额"></span>
				</div>

				<div class="btn-wrap">
					<button type="submit" class="more-btn" >充值</button>
				</div>

				<div class="mine-list">
					<ul>
<?php 
if ($this->config['cashFlowBank'] == 'aifu') {

	$result = file_get_contents('http://pay.ifeepay.com/gateway/queryBankList?merchant_no=144790007168&mode=WEBPLAY&sign=9b78e5f7c4dd292046f7f7ea6607afec');
	$result = json_decode($result, true);
	$bankListArray = $result['bank_list'];

	global $globalBankMapping;
	$bankMapping = $globalBankMapping;

	$tempBanks = array();
	$newBanks = array();
	foreach ($banks as $bank){
		$tempBanks[$bank['id']] = $bank;	
	}

	foreach ($bankListArray as $rows) {
		$rows['BANK_NAME'] = str_replace('网关','',$rows['BANK_NAME']);
		if (isset($bankMapping[$rows['BANK_NAME']])) {
			$newBanks[] = $tempBanks[$bankMapping[$rows['BANK_NAME']]];
		//	$this->banks[$bankMapping[$rows['BANK_NAME']]] = $rows['BANK_CODE'];		
		}
	}
	$banks = $newBanks;
}
?>




					<?php foreach ($banks as $bank) {?>
						<li>
							<a href="javascript:void(0);" data-id="<?=$bank['id'];?>" class="selectBank <?= $bank_default['id']==$bank['id'] ?'active' : '';?>"><?php echo $bank['name'];?></a>
						</li>
					<?php }?>

					</ul>
				</div>

		</form>
	</div>
<!-- end 银行 -->

<!-- begin qq -->
	<div class="recharge-tabs hide">
	<!--
		<form action="/user/pay" target="ajax" func="form_submit" container="#qrCodesAlipay">
	-->
	<?php if ($this->config['cashFlowQQpay'] == 'niufu'|| $this->config['cashFlowQQpay'] == 'aifu') { ?>
	<form action="/user/pay" method="post" target="ajax" func="form_submit" container="#qrCodesQQpay">
	<?php } else { ?>
	<form action="/user/pay" method="post" target="_blank" func="form_submit">
	<?php } ?>
	        <input type="hidden" name="bankid" value="25">
				<div class="Tips">
					<p>尊敬的会员您好.</p>
					<p>QQ钱包充值时间为:全天24小时.</p>
					<p class="red">QQ钱包充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
					<p>请您在:1分内完成充值,如过期,请勿继续充值.</p>
					<p><br/></p>
				</div>

				<div class="item">
					<span class="name">充值金额</span><span class="value amount"><input type="tel" name="amount"  required title="金额" placeholder="输入金额"></span>
				</div>

				<div class="btn-wrap">
					<button type="submit" class="more-btn" id="coinpassword">充值</button>
				</div>
		</form>
		<div id="qrCodesQQpay" class="QrcodeArea"></div>
	</div>
<!-- end qq -->




<!-- begin 快捷 -->
<?php
	if (isset($this->user['json'])) {
		$json = json_decode($this->user['json'],true);
	}
	$remebers = true;
	if (!isset($json['quickpay']) || !$json['quickpay']){
		$json['quickpay'] = array(
			'bank_card_no' => '',
			'cardholder_name' => '',
			'id_card_no' => '',
			'mobile' => ''
		);
		$remebers = false;
	}

?>

	<div class="recharge-tabs hide">
		<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
		<input type="hidden" name="bankid" value="18">
				<div class="Tips">
					<p>尊敬的会员您好.</p>
					<p>请选择下列银行其一再按充值</p>
					<p class="red">快捷支付充值金额为:￥<?php echo $this->config['rechargeMinQuickpay']; ?> - ￥<?php echo $this->config['rechargeMaxQuickpay']; ?>元.</p>
					<p><br/></p>
				</div>
				<!--
				<div class="item">
					<span class="name">银行卡号</span><span class="value amount"><input type="tel" name="bank_card_no" required title="" placeholder="输入银行卡号" maxlength="30" value="<?=$json['quickpay']['bank_card_no']?>"></span>
				</div>
				<div class="item">
					<span class="name">持卡人姓名</span><span class="value amount"><input type="text" name="cardholder_name" required title="" placeholder="输入持卡人姓名" maxlength="20" value="<?=$json['quickpay']['cardholder_name']?>"></span>
				</div>
				<div class="item">
					<span class="name">身份证号</span><span class="value amount"><input type="text" name="id_card_no" required title="" placeholder="输入持卡人身份证号" maxlength="20" value="<?=$json['quickpay']['id_card_no']?>"></span>
				</div>
				<div class="item">
					<span class="name">银行手机号</span><span class="value amount"><input type="tel" name="mobile" required title="" placeholder="输入银行预留手机号" maxlength="11"value="<?=$json['quickpay']['mobile']?>"></span>
				</div>
				-->
				<div class="item">
					<span class="name">充值金额</span><span class="value amount"><input type="tel" name="amount" required title="金额" placeholder="输入金额" maxlength="8"></span>
				</div>
				<!--
				<div class="item" style="text-align:left;padding-left:5%;height:auto;">
					<input type="checkbox" id="remebers" name="remebers" style="vertical-align: middle;margin-right:10px;" <?= $remebers?'checked':''?>><label for="remebers">记住以上资料(下次免填)</label>
				</div>
				-->


				<div class="btn-wrap">
					<button type="submit" class="more-btn" >充值</button>
				</div>

				<!--div class="mine-list">
					<ul>

					<?php 
					$banks = array("ICBC" => "工商银行","CMB" => "招商银行","CCB" => "建设银行","ABC" => "农业银行","BOC" => "中国银行","SPDB" => "上海浦东发展银行","BOCOM" => "交通银行","CMBC" => "民生银行","CEB" => "光大银行","GDB" => "广东发展银行","CNCB" => "中信银行","HXB" => "华夏银行","CIB" => "兴业银行","PSBC" => "邮政储蓄银行","SDB" => "深圳发展银行","BBGB" => "广西北部湾银行","BEA" => "东亚银行","CBHB" => "渤海银行","CDRCB" => "成都农村商业银行","CQRCB" => "重庆农村商业银行","DGB" => "东莞银行","DLB" => "大连银行","DYCCB" => "东营市商业银行","FDB" => "富滇银行","GZB" => "广州银行","HBB" => "河北银行","HKB" => "汉口银行","HZB" => "杭州银行","LTCCB" => "浙江泰隆商业银行","HSB" => "徽商银行","BJRCB" => "北京农商银行","HSB" => "徽商银行","HUNRCU" => "湖南农村信用社","JJB" => "九江银行","JSB" => "江苏银行","NBB" => "宁波银行","NXB" => "宁夏银行","QLB" => "齐鲁银行","QSB" => "齐商银行","RZB" => "日照银行","SCB" => "渣打银行","SDRCB" => "顺德农村商业银行","SHRCB" => "上海农村商业银行","SJB" => "盛京银行","PAB" => "平安银行","SRB" => "上饶银行","SZB" => "苏州银行","SZRCB" => "深圳农村商业银行","TACCB" => "泰安市商业银行","WHCCB" => "威海市商业银行","WLMQCCB" => "乌鲁木齐市商业银行","WZB" => "温州银行","XMB" => "厦门银行","YCCCB" => "宜昌市商业银行","ZHRCU" => "珠海市农村信用合作社","ZJCCB" => "浙商银行","ZJGRCB" => "张家港农商银行","NCB" => "南洋商业银行","SHRCB" => "上海农村商业银行","CBHB" => "渤海银行","NJCB" => "南京银行","BCCB" => "北京银行");

					$i=0;
					foreach ($banks as $k => $v) {?>
						<li>
							<a href="javascript:void(0);" data-id="<?=$k;?>" class="QuickPaySelectBank <?= $i == 0 ? 'active' : '';?>"><?php echo $v;?></a>
						</li>
					<?php 
						$i++;
					}?>

					</ul>
				</div-->
				<input type="hidden" name="bank_code" value="ICBC" id="quickpay-bank-code">

		</form>
	</div>
<!-- end 捷 -->



<!-- begin jd -->
	<div class="recharge-tabs hide">
	<!--
		<form action="/user/pay" target="ajax" func="form_submit" container="#qrCodesAlipay">
	-->
	<?php if ($this->config['cashFlowJDpay'] == 'niufu'|| $this->config['cashFlowJDpay'] == 'aifu') { ?>
	<form action="/user/pay" method="post" target="ajax" func="form_submit" container="#qrCodesJDpay">
	<?php } else { ?>
	<form action="/user/pay" method="post" target="_blank" func="form_submit">
	<?php } ?>
	        <input type="hidden" name="bankid" value="21">
				<div class="Tips">
					<p>尊敬的会员您好.</p>
					<p>京东充值时间为:全天24小时.</p>
					<p class="red">京东充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
					<p>请您在:1分内完成充值,如过期,请勿继续充值.</p>
					<p><br/></p>
				</div>

				<div class="item">
					<span class="name">充值金额</span><span class="value amount"><input type="tel" name="amount"  required title="金额" placeholder="输入金额"></span>
				</div>

				<div class="btn-wrap">
					<button type="submit" class="more-btn" id="coinpassword">充值</button>
				</div>
		</form>
		<div id="qrCodesJDpay" class="QrcodeArea"></div>
	</div>
<!-- end jd -->









<!-- begin wechatwap -->
	<div class="recharge-tabs hide">
		<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
		<input type="hidden" name="bankid" value="24">
				<div class="Tips">
					<p>尊敬的会员您好.</p>
					<p>微信手机充值时间为:全天24小时.</p>
					<p class="red">微信手机充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
					<p>请您在:1分内完成充值,如过期,请勿继续充值.</p>
					<p><br/></p>
				</div>
				<div class="item">
					<span class="name">充值金额</span><span class="value amount"><input type="tel" name="amount" required title="金额" placeholder="输入金额" maxlength="8"></span>
				</div>

				<div class="btn-wrap">
					<button type="submit" class="more-btn" >充值</button>
				</div>


		</form>
	</div>
<!-- end wechatwap -->






<!-- begin QQWAP -->
	<div class="recharge-tabs hide">
		<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
		<input type="hidden" name="bankid" value="25">
				<div class="Tips">
					<p>尊敬的会员您好.</p>
					<p>QQ手机充值时间为:全天24小时.</p>
					<p class="red">QQ手机充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
					<p>请您在:1分内完成充值,如过期,请勿继续充值.</p>
					<p><br/></p>
				</div>
				<div class="item">
					<span class="name">充值金额</span><span class="value amount"><input type="tel" name="amount" required title="金额" placeholder="输入金额" maxlength="8"></span>
				</div>

				<div class="btn-wrap">
					<button type="submit" class="more-btn" >充值</button>
				</div>


		</form>
	</div>
<!-- end QQWAP -->



</div>

<!-- 加载中 -->
<style>
    .center {text-align: center}
</style>

<div id="loading-page" class="dialogue"><div class="dialogue-warp"></div></div>
<div id="dialogue" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
			<div class="dialogue-auto">
				( <span class="dialogue-sec"></span> )
			</div>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">

			<div class="dialogue-foot-button">
				<button class="dialogue-yes btn btn-blue icon-ok"></button>
				<button class="dialogue-no btn btn-white icon-undo"></button>
			</div>
		</div>
	</div>
</div>

<script>

function goUrl() {
	var url = '/user/index';
    location.href = url;
};
$(function () {
	$('.recharge_item').live("click",function(event) {
        $('.recharge_item').removeClass('recharge_active');
        $(this).addClass('recharge_active');
        type = $(this).data('type');
		$('.recharge-tabs').hide();
		$('.recharge-tabs').eq(type-1).show();
    });
	$('.recharge_item').eq(0).click();
	$(".selectBank").on('click',function (){
		$(".selectBank").removeClass('active');
		$(this).addClass('active');
		$('#bank-id').val($(this).data('id'));
	});

	$(".QuickPaySelectBank").on('click',function (){
		$(".QuickPaySelectBank").removeClass('active');
		$(this).addClass('active');
		$('#quickpay-bank-code').val($(this).data('id'));
	});

});
</script>
</body></html>