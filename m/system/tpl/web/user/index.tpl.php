<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link href="/static/theme/<?=THEME;?>/css/m.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>


    <title>首页</title>
</head><script type="text/javascript" id="useragent-switcher">
navigator.__defineGetter__("userAgent", function() {return "Mozilla/5.0 (Android 4.4; Mobile; rv:18.0) Gecko/18.0 Firefox/18.0"})</script>
<body class="login-bg">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl()">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">
                <div class="bett-tit-noborder">
                    账户中心
                </div>
            </div>
        </h1>
        <div class=" header-icon">
            <a class="header-logout" href="/user/logout">退出</a>
        </div>
    </div>
</div>

<div id="wrapper_1" class="scorllmain-content nobottom_bar" style="padding-top: 44px; padding-bottom: 61px;">
    <div class="sub_ScorllCont" style="background-color: #1b1c2e;">
        <div class="mine-top">
            <div class="mine-head">
                <div class="mine-img"><img src="/static/theme/<?=THEME;?>/image/geren_tou.png" alt=""></div>
                <div class="mine-name"><?php echo $user['username'];?> <span class="level">(VIP<?php echo $user['grade'];?>)</span></div>

            </div>
            <div class="mine-info">
                <ul>
                    <li>
                        <div class="mine-tit">￥<span id="balance"><?php echo $user['coin'];?></span></div>
                        <p>余额</p>
                    </li>
                    <li>
                        <div class="mine-tit">￥<span id="latewithdraw"><?php echo $user['score'];?></span></div>
                        <p>积分</p>
                    </li>
                    <li>
                        <a class="mine-refresh1" href="javascript:;" style="color: rgb(255, 255, 255);">刷新</a>
                    </li>
                </ul>
            </div>
        </div>

            <div class="mine-but" >
                <a href="/user/recharge_mobile" class="recharge" style="width:49.5%;border-right: 1px solid #9d9d9d;">
                	<img src="/static/theme/<?=THEME;?>/image/geren_cz_01.png" style="vertical-align: middle;width: 100px;height: 40px;" >
                </a>
                <a href="/user/cash_mobile" class="withdraw" style="width:50%;">
                	<img src="/static/theme/<?=THEME;?>/image/geren_tixian_01.png" style="vertical-align: middle;width: 100px;height: 39px;">
                	</a>
            </div>

        <div class="mine-list">
            <ul>
                <li>
                    <a href="/bet/log">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_13.png" alt="">投注记录
                    </a>
                </li>
                <li>
                    <a href="/bet/log?onlyWin=1">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_24.png" alt="">中奖记录
                    </a>
                </li>
                <li>
                    <a href="/user/coin">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_26.png" alt="">账户明细
                    </a>
                </li>
                <li>
                    <a href="/user/recharge">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_04.png" alt="">充值记录
                    </a>
                </li>
                <li>
                    <a href="/user/cash">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_14.png" alt="">提款记录
                    </a>
                </li>
                                <li>
                    <a  href="/user/message_receive" >
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_06.png" alt="">个人消息
<?php
                        if($user['count_unread'] > 0)
                        {
                        	echo '<span id="count_unread">('.$user['count_unread'].')</span> <i class="red-icon" id="flag_unread" style="display:black;"></i>';
                        }else
						{
							echo '<span id="count_unread"></span> <i class="red-icon" id="flag_unread" style="display:none;"></i>';
						}
?>

                    </a>
                </li>
<?php
if($user['type']>0)
{
?>
                <li>
                    <a href="/agent/member?set=1">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_15.png" alt="">添加下级
                    </a>
                </li>
                <li>
                    <a href="/agent/member?set=2">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_15.png" alt="">链接开户
                    </a>
                </li>
                <li>
                    <a href="/agent/member?set=3">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_04.png" alt="">团队资金
                    </a>
                </li>
<?php
}
?>
                <li>
                    <a href="/user/set">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_30.png" alt="">更多
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php require(TPL.'/index_foot.tpl.php');?>

<input type="hidden" id="refresh_unread" value="0">

<style>
    .center {text-align: center}
</style>

<div id="tip_bg" class="tips-bg" style="display: none;"></div>
<div id="confirm_bg" class="tips-bg" style="display: none;"></div>
<script>
    $(function() {
//        getCurBalance();
//        getWinAmount();
//        getUnReadCount();
    });

    $('a.mine-refresh1').click(function() {
        getCurBalance();
        //getWinAmount();
        //getUnReadCount()
    });

    //当前余额
    function getCurBalance() {
        $.ajax({
            url: '/user/fresh',
            type: 'POST',
            dataType: 'json',
            data: {
            },
            timeout: 30000,
            success: function (data) {
                if (data) {
                	$('span#balance').text(data.coin);
                	$('span#latewithdraw').text(data.score);
	                if(data.count_unread > 0)
	                {
	                    $('span#count_unread').text('('+data.count_unread+')');
	                    $('i#flag_unread').show();
                    }else
                    {
	                    $('span#count_unread').text('');
	                    $('i#flag_unread').hide();
                    }
                }
            }
        });
    }
function goUrl() {
	var url = '/';
    location.href = url;
}
</script>
</body></html>