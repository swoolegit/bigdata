<div id="recharge-panel" class="money-panel">
    <form action="/user/pay" id="recharge-form" method="post" target="ajax" func="form_submit" container="#qrCodes">
        <div class="Tips">温馨提示：
            <p>　　　　尊敬的会员您好.</p>
            <p>　　　　您当前该银行的充值时间为:全天24小时.</p>
            <p style="color:#EF0607;">　　　　您当前该银行的充值金额为:￥:<?php echo $this->config['rechargeMin']; ?> -
                ￥:<?php echo $this->config['rechargeMax']; ?>元.</p>
            <p>　　　　请您在:1分内完成充值,如过期,请勿继续充值.</p>
            <p><br/></p>
        </div>

        <li style="display: block;"><label>充值金额&nbsp;&nbsp;&nbsp; ：</label><input type="text" value=""
                                                                                  name="amount"><span>例如：<?php echo $this->config['rechargeMin']; ?>
                元.</span></li>

        <div class="main">
            <input type="hidden" id="bank-id" name="bankid" value="1">
            <button type="submit" class="submit btn btn-blue icon-ok">充值</button>
        </div>
    </form>
	<div id="qrCodes"></div>


    <script type="text/javascript">
        $(function () {
            $('#home').removeClass('on');
            $('#user-recharge').addClass('on');
            // 菜单下拉固定
            $.scroll_fixed('#recharge-log .head');
            // 切换银行
            var recharge_type = $('#recharge-type');
            var recharge_type_hover = recharge_type.find('.hover');
            var recharge_type_choose = recharge_type.find('.choose');
            var bank_id = $('#bank-id');
            var bank_list = $('#bank-list');
            var recharge_form = $('#recharge-form');
            var recharge_current_img = $('#recharge-current img');
            recharge_type.hover(function () {
                recharge_type_hover.animate({'top': 0});
            }, function () {
                recharge_type_hover.animate({'top': '41px'});
            });
            recharge_type.bind('click', function () {
                if (bank_list.is(':hidden')) {
                    bank_list.slideDown();
                    recharge_type_choose.removeClass('icon-down-dir').addClass('icon-up-dir').text('收起');
                    recharge_type_hover.removeClass('icon-down-dir').addClass('icon-up-dir').text('点击收起银行');
                } else {
                    bank_list.slideUp();
                    recharge_type_choose.removeClass('icon-up-dir').addClass('icon-down-dir').text('切换');
                    recharge_type_hover.removeClass('icon-up-dir').addClass('icon-down-dir').text('点击切换银行');
                }
            });
            bank_list.find('img').bind('click', function () {
                recharge_current_img.attr('src', $(this).attr('src'));
                $(this).addClass('active').siblings().removeClass('active');
                bank_id.val($(this).data('id'));
                if ($(this).data('id') != 19) {
                    recharge_form.attr('target', 'ajax');
                } else {
                    recharge_form.attr('target', '_blank');
                }
            });
            // 输入框焦点效果
            $('#input-money').focus(function () {
                $(this).parent().addClass('focus');
            }).blur(function () {
                $(this).parent().removeClass('focus');
            });
            // 时间选择插件
            $('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
        });
    </script>