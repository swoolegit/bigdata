<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link href="/static/theme/<?=THEME;?>/css/m.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>


    <title>首页</title>
</head><script type="text/javascript" id="useragent-switcher">
navigator.__defineGetter__("userAgent", function() {return "Mozilla/5.0 (Android 4.4; Mobile; rv:18.0) Gecko/18.0 Firefox/18.0"})</script>
<body class="login-bg">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl(1)">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">    
                <div class="bett-tit-noborder">
                    更多
                </div>
            </div>
        </h1>
        <div class=" header-icon">
            
        </div>
    </div>
</div>


<div id="wrapper_1" class="scorllmain-content nobottom_bar" style="padding-top: 44px; padding-bottom: 61px;">
    <div class="sub_ScorllCont">
        
        <div class="mine-list">
            <ul>
                <li>
                    <a href="/user/card">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_16.png" alt="">绑定银行卡与设置资金密码
                    </a>
                </li>
                <li>
                    <a href="/user/pwd">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_25.png" alt="">修改登录密码
                    </a>
                </li>
                <!--li>
                    <a href="/user/coin">
                        <img src="/static/theme/<?=THEME;?>/image/geren_tubiao_26.png" alt="">关于我们
                    </a>
                </li-->
            </ul>
        </div>
    </div>
    <div class="btn-wrap"><button type="button" class="more-btn" onclick="goUrl(2)">退出登录</button></div>
</div>
<input type="hidden" id="refresh_unread" value="0">


<style>
    .center {text-align: center}
</style>

<div id="tip_bg" class="tips-bg" style="display: none;"></div>

<div id="confirm_bg" class="tips-bg" style="display: none;"></div>

</body>
<script>
function goUrl(type) {
	var url = '/user/index';
	if(type==1)
	{
		var url = '/user/index';
	}
	if(type==2)
	{
		var url = '/user/logout';
	}
    location.href = url;
}	
</script>
</html>