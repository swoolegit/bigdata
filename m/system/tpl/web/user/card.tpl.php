<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/common.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/agent.js?v=<?php echo $this->version;?>"></script>
    <link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/lobby.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>
    <title>首页</title>
</head><script type="text/javascript" id="useragent-switcher">
navigator.__defineGetter__("userAgent", function() {return "Mozilla/5.0 (Android 4.4; Mobile; rv:18.0) Gecko/18.0 Firefox/18.0"})</script>
<body class="login-bg">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl(1)">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">    
                <div class="bett-tit-noborder">
                    更多
                </div>
            </div>
        </h1>
        <div class=" header-icon">
            
        </div>
    </div>
</div>
<?php
	$uid = $this->user['uid'];
	$bank_me = $this->db->query("SELECT * FROM `{$this->db_prefix}member_bank` WHERE `uid`=$uid LIMIT 1", 2);
	if ($bank_me) {
		$bank_id = $bank_me['bankId'];
		if ($bank_me['reset'] == 1) {
			$disabled = false;
		}
		else{
			$disabled = true;
		}
	} else {
		$bank_id = 0;
		$disabled = false;
	}
?>
<div id="wrapper_1" class="scorllmain-content nobottom_bar bet_setup" style="padding-top: 44px; padding-bottom: 61px;">
    <div class="sub_ScorllCont">
		<?php if (!$disabled) {?>
    		<form method="POST" action="/user/setting_bank" target="ajax" func="form_submit">
    	<?php }else{ ?>
    		<form method="POST" action="/user/setting_coin_password" target="ajax" func="form_submit">
    	<?php } ?>
		<div class="containers">
			<div class="tips" style="font-size: 14px;text-align: center;margin-top: 10px;">为了您账户安全，请您绑定银行卡，设置资金密码，请填写真实信息</div>
			<div class="form">
	        <div class="set-list">
	            <ul>
	                <li>
	                    <span  class="bank_set">银行&nbsp;&nbsp;</span>
						<?php
							$bank_id = $bank_me ? $bank_me['bankId'] : 0;
							$bank_list = $this->db->query("SELECT * FROM `{$this->db_prefix}bank_list` WHERE `isDelete`=0 AND id NOT IN (1,18,19,20,21,24,25) ORDER BY `sort` DESC", 3);
							 if ($disabled) {
								foreach($bank_list as $bank) {
									if ($bank_id == $bank['id']) {
										echo $bank['name'];
										break;
									}
								}
							 } else {?>
						<input type="hidden" name="bankId" id="bankId" />
						<input type="button" name="bankName" id="bankName" onclick="showStep('detail');" class="bank-btn" value="请选择[银行]">
						<?php }?>
	                </li>
					<li>
	                    <span class="bank_set">银行卡号&nbsp;&nbsp;</span>
						<?php if ($disabled) {?>
						<?php echo preg_replace('/^(\w{4}).*(\w{4})$/', '\1***\2', htmlspecialchars($bank_me['account']));?>
						<?php } else {?>
						<input type="text" name="account" id="account" placeholder="请输入[银行账户]" style="padding: 0px 10px;" value="<?= isset($bank_me['account']) ? $bank_me['account'] : '' ?>">
						<?php }?>
	                </li>
	                <li>
	                    <span class="bank_set">开户行&nbsp;&nbsp;</span>
						<?php if ($disabled) {?>
						<?php echo preg_replace('/^(\w{4}).*(\w{4})$/', '\1***\2', htmlspecialchars($bank_me['countname']));?>
						<?php } else {?>
						<input type="text"  name="countname" id="countname" placeholder="请输入[开户行]" style="padding: 0px 10px;" value="<?= isset($bank_me['countname']) ? $bank_me['countname'] : '' ?>"></td>
						<?php }?>
	                </li>
	                <li>
	                    <span class="bank_set">开户人姓名&nbsp;&nbsp;</span>
						<?php if ($disabled) {?>
						<?php echo '*'.mb_substr(htmlspecialchars($bank_me['username']), 1);?>
						<?php } else {?>
						<input type="text" name="username" id="username" placeholder="请输入[银行户名]" style="padding: 0px 10px;" value="<?= isset($bank_me['username']) ? $bank_me['username'] : '' ?>">
						<?php }?>
	                </li>
	
				</ul>
	        </div>
	        <div class="set-list">
	        	<ul>
					<?php if( isset($bank_me['reset']) && $bank_me['reset'] == 1) { ?>

						<li>
							<span class="bank_set">資金密码&nbsp;&nbsp;</span> <input type="password" name="newpassword" id="newpassword" placeholder="请输入[资金密码]" style="padding: 0px 10px;">
						</li>


					<?php } else { ?>

						<?php if (!empty($this->user['coinPassword'])) {?>
						<li>
							<span class="bank_set">旧密码&nbsp;&nbsp;</span> <input type="password" name="oldpassword" id="oldpassword" placeholder="请输入[当前资金密码]" style="padding: 0px 10px;">
						</li>
						<?php } ?>
						<li>
							<span class="bank_set">新密码&nbsp;&nbsp;</span> <input type="password" name="newpassword" id="newpassword" placeholder="请输入[新资金密码]" style="padding: 0px 10px;">
						</li>

					<?php } ?>

				</ul>	        	
	        </div>
			</div>
    	</div>
    	<div class="btn-wrap">
		<?php if (!$disabled) {?>
			<button type="submit" class="more-btn" id="setbank">绑定银行卡与修改资金密码</button>
		<?php }else{?>
			<button type="submit" class="more-btn" id="coinpassword">修改资金密码</button>
		<?php }?>
    	</div>
    	</form>
	</div>
</div>

<style>
    .center {text-align: center}
</style>

<div class="bet_detail bet_revokeok header" style="display: none;">
    <div class="header">
        <div class="headerTop">
            <div class="ui-toolbar-left">
                <button class="reveal-left" onclick="showStep('setup')">reveal</button>
            </div>
            <h1 class="ui-toolbar-title"><span class="bet_detail">详情</span></h1>
        </div>
    </div>
</div>
<div id="wrapper_1" class="bet_detail scorllmain-content scorll-order nobottom_bar" style="display: none; padding-top: 44px;">
    <div class="set-list" id="bank_list">
    	<ul>
			<?php foreach($bank_list as $bank){ ?>
			<li>
				<a data-id="<?php echo $bank['id'];?>" data-name="<?php echo $bank['name'];?>"><div class="lotto-info"><span><?php echo $bank['name'];?></span></div></a>
			</li>
			<?php } ?>
		</ul>	        	
    </div>
</div>

<div id="loading-page" class="dialogue"><div class="dialogue-warp"></div></div>
<div id="dialogue" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
			<div class="dialogue-auto">
				( <span class="dialogue-sec"></span> )
			</div>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">

			<div class="dialogue-foot-button">
				<button class="dialogue-yes btn btn-blue icon-ok"></button>
				<button class="dialogue-no btn btn-white icon-undo"></button>
			</div>
		</div>
	</div>
</div>

</body>
<script>
function goUrl(type) {
	var url = '/user/set';
	if(type==1)
	{
		var url = '/user/set';
	}
	if(type==2)
	{
		var url = '/user/logout';
	}
    location.href = url;
};
function showStep(step) {
    if (step == undefined) {
        return;
    }
    $('.scorllmain-content').hide();
    $('.bet_detail').hide();
    $('.bet_revokeok').hide();
    $('.bet_'+step).show();
};

$(function () {
	/*
    $('#coinpassword').live("click",function() {
        coinpassword();
    });
    $('#setbank').live("click",function() {
        setbank();
    });
    */
	$('#bank_list li a').live("click",function(event) {
        bank_id = $(this).data('id');
        bank_name = $(this).data('name');
        $('#bankName').val(bank_name);
        $('#bankId').val(bank_id);
        showStep('setup');
    });

	<?php
		if (isset($bank_me['reset']) && $bank_me['reset'] == 1) {
			foreach($bank_list as $bank) {
				if ($bank['id'] == $bank_me['bankId']) {
					echo "$('#bankName').val('{$bank['name']}'); $('#bankId').val('{$bank['id']}');showStep('setup'); ";
				
					break;
				}
			
			}
			
		}
	?>
});


</script>
</html>