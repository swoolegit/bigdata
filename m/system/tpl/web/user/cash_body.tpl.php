<?php
	if ($data) {
		$stateName = array(
			'已到帐',
			'处理中',
			'已取消',
			'已支付',
			'失败',
		);
	}
	foreach ($data as $v) 
	{
?>
            <li>
            	<div class="order-list-tit">
            		<span class="fr c-red"><?php echo round($v['amount'],2);?> 元</span>
            		<span class="order-top-left">单号 <?php echo $v['id'];?></span><?php echo $v['bankName'] ? $v['bankName'] : $v['info'];?>
            	</div>
            	<div class="c-gary">
            		<span class="fr">
            			<?php echo isset($stateName[$v['state']]) ? $stateName[$v['state']] : '';?>
            			<?php echo $v['cashTime'] ? date('m-d H:i:s', $v['cashTime']) : '';?>
            		</span>
            		<p class="order-time"><?php echo date('m-d H:i:s', $v['actionTime']);?></p>
            	</div>
            </li>
<?php
	}
?>