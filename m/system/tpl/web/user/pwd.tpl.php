<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/common.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/agent.js?v=<?php echo $this->version;?>"></script>
    <link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/lobby.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>
    <title>首页</title>
</head><script type="text/javascript" id="useragent-switcher">
navigator.__defineGetter__("userAgent", function() {return "Mozilla/5.0 (Android 4.4; Mobile; rv:18.0) Gecko/18.0 Firefox/18.0"})</script>
<body class="login-bg" id="dom_body">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl(1)">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">    
                <div class="bett-tit-noborder">
                    更多
                </div>
            </div>
        </h1>
        <div class=" header-icon">
            
        </div>
    </div>
</div>

<form method="POST" action="/user/setting_login_password" target="ajax" func="form_submit" class="mb">
<div id="wrapper_1" class="scorllmain-content nobottom_bar bet_setup" style="padding-top: 44px; padding-bottom: 61px;">
    <div class="sub_ScorllCont">
		<div class="containers">
			<div class="form">
	        <div class="set-list">
	        </div>
	        <div class="set-list">
	        	<ul>
					<li>
						<span style="display:inline-block;width: 5em;">旧密码&nbsp;&nbsp;</span> <input type="password" name="oldpassword" id="oldpassword" placeholder="请输入[当前登录密码]" style="padding: 0px 10px;">
					</li>
					<li>
						<span style="display:inline-block;width: 5em;">新密码&nbsp;&nbsp;</span> <input type="password" name="newpassword" id="newpassword" placeholder="请输入[新登录密码]" style="padding: 0px 10px;">
					</li>
				</ul>	        	
	        </div>
			</div>
    	</div>
    	<div class="btn-wrap">
			<button type="submit" class="more-btn" id="coinpassword">修改登录密码</button>
    	</div>
	</div>
</div>
</form>
<style>
    .center {text-align: center}
</style>

<div id="loading-page" class="dialogue"><div class="dialogue-warp"></div></div>
<div id="dialogue" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
			<div class="dialogue-auto">
				( <span class="dialogue-sec"></span> )
			</div>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">

			<div class="dialogue-foot-button">
				<button class="dialogue-yes btn btn-blue icon-ok"></button>
				<button class="dialogue-no btn btn-white icon-undo"></button>
			</div>
		</div>
	</div>
</div>
</body>
<script>
function goUrl(type) {
	var url = '/user/set';
	if(type==1)
	{
		var url = '/user/set';
	}
	if(type==2)
	{
		var url = '/user/logout';
	}
    location.href = url;
};



</script>
</html>