<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/common.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/function.js?v=<?php echo $this->version;?>"></script>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/lobby.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
	<link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>
    <title>提款</title>
</head>
<body class="login-bg">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl(1)">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">
                <div class="bett-tit-noborder">
					提款
                </div>
            </div>
        </h1>
			<a class="header-right-btn" href="/user/cash">提款记录</a>
    </div>
</div>
<div id="cash-box">

		<form action="/user/cash_submit" target="ajax" func="cash_submit">

				<div class="mine-list">
					<ul>

						<li>
							<span>账户余额： &nbsp;<?php echo $this->user['coin'];?></span>
						</li>
						<li>
							<span>提款金额：</span>
							<input required autocomplete="off" class="cash-int" type="tel" id="input-money" name="money" min="<?php echo $this->config['cashMin'];?>" max="<?php echo $this->config['cashMax'];?>" placeholder="最少提款金额为<?php echo $this->config['cashMin'];?>元">
						</li>
						<li>
							<span>资金密码：</span>

							<input required type="password" class="cash-int" id="input-password" name="password" autocomplete="off" placeholder="请输入资金密码">
						</li>

					</ul>
				</div>

				<div class="tips">

							<div>提款事项</div>
							<li>每天受理提现请求的时间段为<span class="color blue"><?php echo $this->config['cashFromTime'];?> ~ <?php echo $this->config['cashToTime'];?></span></li>
							<li>每天提现次数上限为<span class="btn btn-green"><?php echo $info['times_limit'];?></span>次，今天您已经提交<span class="btn btn-blue"><?php echo $info['times'];?></span>次申请</li>
			<li>提现金额最小为 <span class="color red"><?php echo $this->config['cashMin'];?></span> 元，最大为 <span class="color red"><?php echo $this->config['cashMax'];?></span> 元</li>
			<?php if ($this->config['cashMinAmount'] > 0) { // 有限制时才显示条件 ?>
				<li>提现需满足条件如下，未满足条件则不能提现</li>
				<li>　最后一次提现至今的消费比例 &gt;=<span class="color red"><?=$this->config['cashMinAmount'];?>%</span> (有效投注金额 / 充值金额)；</li>
				<?php if ($info['amount_recharge']==0 && $info['proportion']==$info['amount_used_min']) { // 没有充值的情况 ?>
					<li>您的消费比例已达到 <span class="color red"><?php echo $info['proportion'];?>%</span>；</li>
				<?php } else { ?>
					<li>目前您的有效投注 <span class="color green"><?php echo $info['amount_bets'];?></span> 元，充值 <span class="color blue"><?php echo $info['amount_recharge'];?></span> 元，消费比例已达到 <span class="color red"><?php echo $info['proportion'];?>%</span></li>
				<?php } ?>
			<?php } ?>


				</div>

				<div class="mine-list">
					<ul>
						<li>
							<span>银行省份：</span>
							<?=$bank['bankprovince']?>
						</li>
						<li>
							<span>银行城市：</span>
							<?=$bank['bankcity']?>
						</li>

						<li>
							<span>银行户名：</span>
							<?=$bank['username']?>
						</li>
						<li>
							<span>收款银行：</span>
							<?=$bank['bankName']?>
						</li>
						<li>
							<span>开&nbsp;&nbsp;户&nbsp;&nbsp;行：</span>
							<?=$bank['countname']?>
						</li>
						<li>
							<span>银行卡号：</span>
							<?=$bank['account']?>
						</li>
					</ul>
				</div>


	        <input type="hidden" name="bankid" value="19">

				<div class="btn-wrap">
					<button type="submit" class="more-btn" id="coinpassword">确认</button>
				</div>
		</form>
<!-- end 微信 -->


</div>

<!-- 加载中 -->
<style>
    .center {text-align: center}
</style>

<div id="loading-page" class="dialogue"><div class="dialogue-warp"></div></div>
<div id="dialogue" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
			<div class="dialogue-auto">
				( <span class="dialogue-sec"></span> )
			</div>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">

			<div class="dialogue-foot-button">
				<button class="dialogue-yes btn btn-blue icon-ok"></button>
				<button class="dialogue-no btn btn-white icon-undo"></button>
			</div>
		</div>
	</div>
</div>

<script>

function goUrl() {
	var url = '/user/index';
    location.href = url;
};
$(function () {

<?php
	if(isset($error))
	{
?>
		$.dialogue({
			type: 'error',
			text: '<?php echo $error?>',
			auto: true,
			yes: {
				text: '前往设置',
				func: function() {
					location.href = '/user/card';
				}
			}
		});
<?php
	}
?>
});
</script>
</body></html>