<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/user.main.js?v=<?php echo $this->version;?>"></script>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/lobby.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>
    <title>投注记录</title>
</head>
<body class="login-bg">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl()">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">    
                <div class="bett-tit">
                    <span id="order_type">全部类型</span><i class="bett-attr" style="display: &#39;&#39;"></i>
                </div>
            </div>
        </h1>
        <div class="ui-bett-refresh">
            <a class="bett-refresh" href="javascript:;" ></a>
        </div>
    </div>
</div>



<div id="wrapper_1" class="bet_list scorllmain-content scorll-order nobottom_bar paddingbutton" style="padding-top: 44px; padding-bottom: 44px;">
    <div class="sub_ScorllCont">
        <div class="mine-message" style="display: none;">
            <div class="mine-mess"><img src="/static/theme/default/image/wuxinxi.png"></div>
            <p>目前暂无记录哦！</p>
        </div>
        <div class="order-center">
            <ul id="bet_list">
<?php
	foreach ($data as $v) 
	{
?>
            <li><a href="javascript:showMessage('<?php echo $v['id'];?>')" data-id='<?php echo $v['id'];?>'  >
            	<div class="order-list-tit">
            		<span class="fr c-red <?php echo $v['id'];?>"><?php  if(!$v['is_readed']) echo '未读';?></span>
            		<span class="order-top-left"><?php echo $v['title'];?></span>
            	</div>
            	<div class="c-gary">
            		<span class="fr">
            			发件人 : <?php echo htmlspecialchars($v['from_username']);?>
            		</span>
            		<p class="order-time"><?php echo date('Y-m-d H:i',$v['time']);?></p>
            	</div>
            	</a>
            </li>
<?php
	}
?>
			</ul>
        </div>
    </div>
</div>

<!-- 直选tips -->
<div class="beet-tips" id="beet-tips" style="display: none;">
    <!--<div class="beet-tips-tit"><span>普通玩法</span></div>-->
    <div class="clear"></div>
    <ul id="group_list" style="background-color: #1b1c2e;">
    	<li><a data-id='0' href='javascript:;' class='on' >全部类型</a></li>
    	<li><a data-id='1' href='javascript:;' >未读</a></li>
    	<li><a data-id='2' href='javascript:;' >已读</a></li>
    </ul>
</div>
<div class="bet_detail bet_revokeok header" style="display: none;">
    <div class="header">
        <div class="headerTop">
            <div class="ui-toolbar-left">
                <button class="reveal-left" onclick="showStep(&#39;list&#39;)">reveal</button>
            </div>
            <h1 class="ui-toolbar-title"><span class="bet_detail">详情</span></h1>
        </div>
    </div>
</div>
<div id="wrapper_1" class="bet_detail scorllmain-content scorll-order nobottom_bar" style="display: none; padding-top: 44px;">
</div>

<!-- 加载中 -->
<div class="loading" style="left: 50%; margin-left: -2em; display: none;">加载中...</div>
<div class="loading-bg" style="display: none;"></div>
<input type="hidden" id="page_type" value="message" />
<script>
    function loadingShow(tips,bg) {
        if(tips == ""||typeof(tips) == "undefined"){
            $(".loading").css("left","50%");
            $(".loading").css("margin-left","-2em");
            $(".loading").html("加载中...");
        }else{
            $(".loading").html(tips);
            $(".loading").css("left",Math.ceil(($(document).width() - $(".loading").width())/2));
            $(".loading").css("margin-left",0);
        }

        bg   = (bg == ""||typeof(bg) == "undefined")?1:0;
        if (bg == 1){
            $(".loading-bg").show();
        }else{
            $(".loading-bg").hide();
        }
        $(".loading").show();
    }
    function loadingHide() {
        $(".loading").hide();
        $(".loading-bg").hide();
    }
</script>

<style>
    .center {text-align: center}
</style>

<div id="tip_bg" class="tips-bg" style="display: none;"></div>

<div id="confirm_bg" class="tips-bg" style="display: none;"></div>

<script>
function showStep(step,key) {
    if (step == undefined) {
        return;
    }
    $('.bet_list').hide();
    $('.bet_detail').hide();
    $('.bet_revokeok').hide();
    $('.bet_'+step).show();
    if (step == 'detail') {
        showDetail(key);
    }
}
//弹窗
$(function(){
	$('.bett-top-box').click(function(event){
    //$('.ui-betting-title').bind('touchend', function (event) {
        event.stopPropagation();
	    $('.beet-tips').toggle();
        //$('.beet-rig').hide();
	});
    $(window).bind("load",function(){
	    $('.beet-tips').live("click",function(event){
	    	if($(this).css('display')!='none'){
	    		event.stopPropagation();
	    	}
	    });
	    $(document).on("click",function() {
	        if($('.beet-tips').is(':visible')) {
	           $('.beet-tips').toggle();
	        }
	    });
    });

   
    // scroll toggle off
    $(document).scroll(function() {
      if($('.beet-tips').is(':visible')) {
          $('.beet-tips').toggle();
        }
        else {
          $('.beet-tips').css('display','none');
        }
    });
    // end

    //  hide address bar
    window.addEventListener("load",function() {
        // Set a timeout...
        setTimeout(function(){
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 0);
    });


});
function goUrl() {
	var url = '/user/index';
    location.href = url;
}
</script>
</body></html>