<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        body.login-bg{padding-top:97px;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>
    <title>首页</title>
</head><script type="text/javascript" id="useragent-switcher">
navigator.__defineGetter__("userAgent", function() {return "Mozilla/5.0 (Android 4.4; Mobile; rv:18.0) Gecko/18.0 Firefox/18.0"})</script>
<link href="/static/theme/<?=THEME;?>/css/login.css" rel="stylesheet" type="text/css"/>
<link href="/static/theme/<?=THEME;?>/css/icon.css" rel="stylesheet" type="text/css"/>
<script src="/static/script/jquery.1.7.2.min.js"></script>
<script src="/static/script/reg.js"></script>
<title><?php echo $this->config['webName'];?></title>
<body>
<div class="top-wrap" id="top-wrap" style="text-align: center;margin-top: 30px;">
    <div class="form-pic"><img src="/static/theme/<?=THEME;?>/image/login/login-header-logo.png" alt=""></div>
    <div class="form-login" id="login" style="width:80%; margin:0 auto">
		<div style="height: 46px;" >
            <div class="remind remind-error" style="display: none;">
                <span>!</span><strong id="error_value"></strong>
            </div>
        </div>
        <div class="form-item" style="margin-bottom: 5px;">
			<input id="username" type="text" name="username" class="form-input form-user" placeholder="请输入账户名" autocomplete="off" value="" required="" autofocus="">
        </div>
        <div class="tip" style="margin-bottom: 5px;color:#999">账户名由字母、数字、下划线组成，4~16位字符</div>
        <div class="form-item">
            <input id="password" type="password" class="form-input form-password" placeholder="请输入登录密码">
        </div>
        <div class="form-item">
            <input id="password_repeat" type="password" class="form-input form-password" placeholder="请再次输入登录密码">
        </div>
        <div class="form-btn">
            <button id="submit" type="button" class="common-btn">
                <span class="btn-text">提交注册</span>
            </button>
            <input type="hidden" value="<?php echo $lid;?>" id="lid">
        </div>
        <div class="forget" style="color: #fff;text-align: left">
			<label><span class="icon-user" style="color:#999">如果您已有本站账号，直接<a id="clogin" href="/user/login?client_type=<?php echo $this->client_type;?>" style="color:#ddd"> [点击登录]</a></span></label>
		</div>
	</div>
</div>
</body>
</html>