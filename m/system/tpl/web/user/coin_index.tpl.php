<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/coin.list.js?v=<?php echo $this->version;?>"></script>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/lobby.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>
    <title>投注记录</title>
</head>
<body class="login-bg">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl()">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">    
                <div class="bett-tit">
                    <span id="order_type">全部订单</span><i class="bett-attr" style="display: &#39;&#39;"></i>
                </div>
            </div>
        </h1>
        <div class="ui-bett-refresh">
            <a class="bett-refresh" href="javascript:;" ></a>
        </div>
    </div>
</div>

<div class="bet_detail bet_revokeok header" style="display: none;">
    <div class="header">
        <div class="headerTop">
            <div class="ui-toolbar-left">
                <button class="reveal-left" onclick="showStep(&#39;list&#39;)">reveal</button>
            </div>
            <h1 class="ui-toolbar-title"><span class="bet_detail">详情</span><span class="bet_revokeok">撤单</span></h1>
        </div>
    </div>
</div>

<div id="wrapper_1" class="bet_list scorllmain-content scorll-order nobottom_bar paddingbutton" style="padding-top: 44px; padding-bottom: 44px;">
    <div class="sub_ScorllCont">
        <div class="mine-message" style="display: none;">
            <div class="mine-mess"><img src="/static/theme/default/image/wuxinxi.png"></div>
            <p>目前暂无记录哦！</p>
        </div>
        <div class="order-center">
            <ul id="bet_list">
<?php
	foreach ($data as $v) 
	{
?>
            <li>
            	<div class="order-list-tit">
            		<span class="fr c-red"><?php $coin = number_format($v['coin'], 2, '.', '');echo $coin > 0 ? '<span style="color: #46BD36;">'.$coin.'元</span>' : '<span >'.$coin.'元</span>';?></span>
            		<span class="order-top-left"><?php echo array_key_exists($v['liqType'], $this->coin_types) ? $this->coin_types[$v['liqType']] : '--';?></span>
            	</div>
            	<div class="c-gary"><span class="fr">
				<?php echo $v['info'] ? $v['info'] : '--';?>				
            	</span><p class="order-time"><?php echo date('Y-m-d H:i:s', $v['actionTime']);?></p></div>
            </li>
<?php
	}
?>
			</ul>
        </div>
    </div>
</div>

<!-- 直选tips -->
<div class="beet-tips" id="beet-tips" style="display: none;">
    <!--<div class="beet-tips-tit"><span>普通玩法</span></div>-->
    <div class="clear"></div>
    <ul id="group_list" style="background-color: #1b1c2e;">
    </ul>
    <div class="beet-tips-tit" style="border-bottom: 1px dashed #555;"></div>
    <div class="clear"></div>
    <ul id="play_list" class="play_list" style="background-color: #1b1c2e;">
    </ul>
</div>

<!-- 加载中 -->
<div class="loading" style="left: 50%; margin-left: -2em; display: none;">加载中...</div>
<div class="loading-bg" style="display: none;"></div>

<script>
    function loadingShow(tips,bg) {
        if(tips == ""||typeof(tips) == "undefined"){
            $(".loading").css("left","50%");
            $(".loading").css("margin-left","-2em");
            $(".loading").html("加载中...");
        }else{
            $(".loading").html(tips);
            $(".loading").css("left",Math.ceil(($(document).width() - $(".loading").width())/2));
            $(".loading").css("margin-left",0);
        }

        bg   = (bg == ""||typeof(bg) == "undefined")?1:0;
        if (bg == 1){
            $(".loading-bg").show();
        }else{
            $(".loading-bg").hide();
        }
        $(".loading").show();
    }
    function loadingHide() {
        $(".loading").hide();
        $(".loading-bg").hide();
    }
</script>

<style>
    .center {text-align: center}
</style>

<div id="tip_bg" class="tips-bg" style="display: none;"></div>

<div id="confirm_bg" class="tips-bg" style="display: none;"></div>

<script>
	var resUrl="/static/theme/default/image/lottery_type";
    function showStep(step,key) {
        if (step == undefined) {
            return;
        }
        $('.bet_list').hide();
        $('.bet_detail').hide();
        $('.bet_revokeok').hide();
        $('.bet_'+step).show();
        if (step == 'detail') {
            showDetail(key);
        }
    }

//弹窗
$(function(){
	$('.bett-top-box').click(function(event){
    //$('.ui-betting-title').bind('touchend', function (event) {
        event.stopPropagation();
	    $('.beet-tips').toggle();
        //$('.beet-rig').hide();
	});
    $(window).bind("load",function(){
	    $('.beet-tips').live("click",function(event){
	    	if($(this).css('display')!='none'){
	    		event.stopPropagation();
	    	}
	    });
	    $(document).on("click",function() {
	        if($('.beet-tips').is(':visible')) {
	           $('.beet-tips').toggle();
	        }
	    });
    });

   
    // scroll toggle off
    $(document).scroll(function() {
      if($('.beet-tips').is(':visible')) {
          $('.beet-tips').toggle();
        }
        else {
          $('.beet-tips').css('display','none');
        }
    });
    // end

    //  hide address bar
    window.addEventListener("load",function() {
        // Set a timeout...
        setTimeout(function(){
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 0);
    });
    <?php
    if(!$data)
	{
		echo "$('div.mine-message').show();";
	}
	$up_list="<li><a data-id='99' href='javascript:;' class='on' >全部类型</a></li>";
	$dn_list=array();
	$i=0;
	foreach ($this->coin_type_data as $n => $vs) 
	{
		$up_list.="<li><a data-id='".$i."' href='javascript:;' >".$n."</a></li>";
		foreach ($vs as $k => $v)
		{
			if(isset($dn_list[$i]))
			{
				$dn_list[$i].='<li><a style="font-size: 14px;" data-id="'.$k.'" href="javascript:;">'.$v.'</a></li>';
			}else
			{
				$dn_list[$i]='<li><a style="font-size: 14px;" data-id="'.$k.'" href="javascript:;">'.$v.'</a></li>';
			}
		}
		$i++;
	}
	?>
	window.play_list = {
		0: '<?php echo $dn_list[0];?>',
		1: '<?php echo $dn_list[1];?>',
		2: '<?php echo $dn_list[2];?>',
		3: '<?php echo $dn_list[3];?>',
	};
	$('#group_list').html("<?php echo $up_list;?>");
	$('#play_list').html('<?php echo $dn_list[0];?>');

});
function goUrl() {
	var url = '/user/index';
    location.href = url;
}
</script>
</body></html>