<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
<title><?php echo $this->config['webName'];?></title>
<link href="/static/theme/<?=THEME;?>/css/login.css" rel="stylesheet" type="text/css"/>
<link href="/static/theme/<?=THEME;?>/css/icon.css" rel="stylesheet" type="text/css"/>
<script src="/static/script/jquery.1.7.2.min.js"></script>
<script src="/static/script/login.js"></script>
<script src="/static/script/jquery.cookie.js"></script>
</head>
<body style="background-color: #555">
<div class="top-wrap" id="top-wrap" style="text-align: center;margin-top: 30px;">
    <div class="form-pic"><img src="/static/theme/<?=THEME;?>/image/login/login-header-logo.png" alt=""></div>
    <div class="form-login" id="login" style="width:80%; margin:0 auto">
		<div style="height: 46px;" >
            <div class="remind remind-error" style="display: none;">
                <span>!</span><strong id="error_value"></strong>
            </div>
        </div>
        <div class="form-item">
			<input id="username" type="text" name="username" class="form-input form-user" placeholder="请输入您的用户名" value="" required="" autofocus="">
        </div>
        <div class="form-item">
            <input id="password" type="password" class="form-input form-password" placeholder="请输入您的密码">
        </div>
        <div class="form-btn">
            <button id="submit" type="button" class="common-btn">
                <span class="btn-text">立即登录</span>
            </button>
        </div>
        <div class="forget" style="color: #fff;text-align: left">
			<label><input type="checkbox" name="remember" id="remember" >&nbsp;记住账户</label>
		</div>
	</div>
</div>
</body>
<script>
$(function(){
	if($.cookie('remember')=='1')
	{
		$('#username').val($.cookie('username'));
		$('#remember').attr("checked", true);
	}
});	
</script>
</html>