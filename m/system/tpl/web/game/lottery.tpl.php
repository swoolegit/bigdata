    <div class="lot-time">
        <p><span id="last_period"><?php echo $last['actionNo']; ?></span>期</p>
        <div class="wait-lot"><i class="time-ico" style="display: none;"></i><span id="last_open" style="font-size: 16px; color: #CD5C5B;">
        	<?php
        	if($lottery)
			{
				if($types[$type_id]['type']!=11)
				{
					echo implode(" ",$lottery);
				}else
				{
					$map = array(
						'color_red'=>'3,6,9,12,15,18,21,24',
						'color_green'=>'1,4,7,10,16,19,22,25',
						'color_blue'=>'2,5,8,11,17,20,23,26'
					);
					$set_color="nocolor";
					foreach($map as $k=>$v)
					{
						$_narray = explode(',',$v);
						if(in_array($lottery[3],$_narray)) 
						{
							$set_color=$k;
							break;
						}
					}
					echo $lottery[0]." + ".$lottery[1]." + ".$lottery[2]." = <span class='luck28ball ".$set_color."'>".$lottery[3]."</span>";
				}
			}else
			{
				echo "正在开奖";	
			}
        	?>
        	</span></div>
    </div>
    <div class="dat-time" style="border:0px">
        <p>距<span id="current_period"><?php echo $current['actionNo']; ?></span>期<span id="current_period_status">截止</span></p>
        <div id="timer_lottery" class="time-late">
            <span id="time_h1">0</span>
            <span id="time_h2">0</span>
            <span class="time-kong"></span>
            <span id="time_m1">0</span>
            <span id="time_m2">0</span>
            <span class="time-kong"></span>
            <span id="time_s1">0</span>
            <span id="time_s2">0</span>
        </div>
    </div>
<script type="text/javascript">
    $(function () {
    	window.actionNo='<?php echo $current['actionNo']; ?>';
        window.S = <?php echo json_encode($diffTime > 0);?>;
        window.KS = <?php echo json_encode($kjDiffTime > 0);?>;
        window.kjTime = parseInt(<?php echo json_encode($kjdTime);?>);
        if (lottery.timer.T) clearTimeout(lottery.timer.T);
        if (lottery.timer.KT) clearTimeout(lottery.timer.KT);
        if (lottery.timer.moveno) clearInterval(lottery.timer.moveno);
        <?php if($diffTime > 0){?>
        lottery.timer.T = setTimeout(function () {
            lottery.countdown(<?php echo $diffTime;?>);
        }, 1000);
        <?php }?>
        <?php if($kjDiffTime > 0){?>
        lottery.timer.KT = setTimeout(function () {
            lottery.waiting(<?php echo $kjDiffTime;?>);
        }, 1000);
        <?php }?>
        <?php if($kjDiffTime <= 0 && !$lottery){?>
        lottery.timer.KT = setTimeout(function () {
            lottery.waiting(6);
        }, 1000);
        <?php }?>
        if(lottery.isRobot)
        {
        	lottery.star_robot();
        }
	});
</script>