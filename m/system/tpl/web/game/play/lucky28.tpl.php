<input type="hidden" name="playedGroup" id="lucky28_playedGroup" value="" />
<input type="hidden" name="playedId" id="lucky28_playedId" value="" />
<input type="hidden" name="type" value="<?php echo $type_id;?>" />
<div class="pp pp28" action="tz11x5Multi" length="1" >
	<div id="lucky28_setup1" class="ch_numball six_numball ch_numball28">
	<div class="lot-number-tip" >和值 </div>
	<ul>
	<?php
	foreach($plays[84] as $k2=>$v2)
	{
	?>
			<li class="specific-cell-o code_value_246" name="lt_place_0" >
			<input type="button" value="<?php echo $v2['example'];?>" data-playedgroup="84" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" class="code code28" />
			<div class="lot-odds">
				<?php echo $v2['bonusProp'];?>
			</div>
			</li>
	<?php
	}
	?>
	</ul>
	</div>

	<div id="lucky28_setup2" class="ch_numball code_numball" hidden=''>
	<div class="lot-number-tip" >两面 </div>
	<ul>
	<?php
	foreach($plays[85] as $k2=>$v2)
	{
		$number=explode(",", $v2['example']);
		foreach($number as $k3=>$v3)
		{
			?>
				<li class="specific-cell-o code_value_246" name="lt_place_0" >
					<input type="button" value="<?php echo $v3;?>" data-playedgroup="85" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" class="code code28 code_box" />
					<div class="lot-odds">
						<?php echo $v2['bonusProp'];?>
					</div>
				</li>
			<?php
		}
	}
	?>
	</ul>
	</div>

	<div id="lucky28_setup3" class="ch_numball color_numball" hidden=''>
	<div class="lot-number-tip" >色波 </div>
	<ul>
	<?php
	foreach($plays[86] as $k2=>$v2)
	{
		if($k2==357)
		{
			$number=explode(",", $v2['example']);
			foreach($number as $k3=>$v3)
			{
				?>
					<li class="specific-cell-o code_value_246" name="lt_place_0" >
						<input type="button" value="<?php echo $v3;?>" data-playedgroup="86" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" class="code code28 code_box" />
						<div class="lot-odds">
							<?php echo $v2['bonusProp'];?>
						</div>
					</li>
				<?php
			}
			break;			
		}
	}
	?>
	</ul>
	</div>
	
	<div id="lucky28_setup4" class="ch_numball color_numball" hidden=''>
	<div class="lot-number-tip" >豹子 </div>
	<ul>
	<?php
	foreach($plays[86] as $k2=>$v2)
	{
		if($k2==358)
		{
			$number=explode(",", $v2['example']);
			foreach($number as $k3=>$v3)
			{
				?>
					<li class="specific-cell-o code_value_246" name="lt_place_0" >
						<input type="button" value="<?php echo $v3;?>" data-playedgroup="86" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" class="code code28 code_box" />
						<div class="lot-odds">
							<?php echo $v2['bonusProp'];?>
						</div>
					</li>
				<?php
			}
			break;
		}
	}
	?>
	</ul>
	</div>
	<div id="lucky28_setup5" class="ch_numball ch_numball28 six_numball_nopl" hidden=''>
	<div class="lot-number-tip" >特码包三  <span class="lot-pei">赔率:<?php echo $plays[86][359]['bonusProp'];?></span></div>
	<ul>
	<?php
	for($i=0;$i<28;$i++)
	{
	?>
		<li class="specific-cell-o code_value_246" name="lt_place_0" >
			<input type="button" value="<?php echo $i;?>" data-playedgroup="86" data-playedid="<?php echo $plays[86][359]['id'];?>" data-bonuspropbase="<?php echo $plays[86][359]['bonusPropBase'];?>" data-bonusprop="<?php echo $plays[86][359]['bonusProp'];?>" class="code code28" />
		</li>
	<?php
	}
	?>
	</ul>
	</div>
</div>
<style>
	.lucky28_pl{
		text-align: center;
	}
</style>
