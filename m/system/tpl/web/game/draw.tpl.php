<!DOCTYPE html>
<!-- saved from url=(0034)http://9788vip.com/draw/index.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
    </style>    
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link href="/static/theme/<?=THEME;?>/css/m.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/jquery.cookie.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/jquery.slimscroll.min.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/array.ext.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/rawdeflate.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/select.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/common.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/function.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/game.js?v=<?php echo $this->version;?>"></script>
    <title>开奖大厅</title>
</head>
<body class="login-bg" >
<div class="header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl()" >reveal</button>
        </div>
        <h1 class="ui-toolbar-title">开奖大厅</h1>
    </div>
</div>
<div id="wrapper_1" class="scorllmain-content nobottom_bar" style="padding-top: 44px; padding-bottom: 62px;">
    <div class="sub_ScorllCont">
        <div class="lott-list">
            <ul id="draw_list">
<?php
			foreach($data as $k=>$v)
			{
?>
            	<li class="list-k3">
            		<a href="/game/draw_list?id=<?=$k?>" data-gid="<?=$k?>">
            			<div class="lott-list-tit"><span><?php echo $v['type_this']['title']?></span><span class="gray" style="font-size: 11px;">第<?php echo $v['last']['actionNo']?>期<?php echo $v['last']['stopTimeFull']?></span></div>
            			<div class="two-ball two-lottery nums-open">
            				<span class="nums">
<?php
						if(count($v['lottery'])>0)
						{
							if($v['type_this']['type']==11)
							{
								$map = array(
									'color_red'=>'3,6,9,12,15,18,21,24',
									'color_green'=>'1,4,7,10,16,19,22,25',
									'color_blue'=>'2,5,8,11,17,20,23,26'
								);
								$set_color="nocolor";
								foreach($map as $k1=>$v1)
								{
									$_narray = explode(',',$v1);
									if(in_array($v['lottery'][3],$_narray)) 
									{
										$set_color=$k1;
										break;
									}
								}
								echo '<div class="two-ball tow-ball-cont nums-pcdd" style="font-size:140%">'.$v['lottery'][0].' + '.$v['lottery'][1].' + '.$v['lottery'][2].' = <span class="luck28ball draw_luck28 '.$set_color.'">'.$v['lottery'][3].'</span></div>';
							}else
							{
								echo "<i>".implode("</i><i>",$v['lottery'])."</i>";
							}
						}else
						{
							echo "正在开奖";	
						}
 ?>            					
            				</span>
            			</div>
            		</a>
            	</li>
<?php
			}
?>
            </ul>
        </div>
    </div>
</div>
<!-- 加载中 -->
<?php require(TPL.'/index_foot.tpl.php');?>
<script>
function goUrl() {
    var url = '/';
    location.href = url;
}	
</script>
</body></html>