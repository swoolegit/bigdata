<!DOCTYPE html>
<!-- saved from url=(0043)http://9788vip.com/draw/list.html?gameId=51 -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="format-detection" content="telephone=no" />
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
    </style>
    
    
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link href="/static/theme/<?=THEME;?>/css/m.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/jquery.cookie.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/jquery.datetimepicker.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/jquery.slimscroll.min.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/array.ext.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/rawdeflate.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/select.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/common.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/function.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/game.js?v=<?php echo $this->version;?>"></script>
    <title>开奖大厅</title>
</head>
<body class="login-bg">
<div class="header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl()" >reveal</button>
        </div>
        <h1 class="ui-toolbar-title"><?php echo $title?></h1>
    </div>
</div>
<div id="wrapper_1" class="scorllmain-content nobottom_bar" style="padding-top: 44px; padding-bottom: 62px;">
    <div class="sub_ScorllCont">
        <div class="lott-list">
            <ul id="draw_list">
<?php

			foreach($lottery as $k=>$v)
			{
?>

            	<li class="list-k3">
            		<div class="lott-list-tit">第<?php echo $v['number']?>期 <?php echo date('Y-m-d h:m:s',$v['time'])?>
            			<div class="two-ball tow-ball-cont two-lottery nums-open">
<?php
						if($type!=11)
						{
							echo "<i>".str_replace(',','</i><i>',$v['data'])."</i>";
						}else
						{
							$map = array(
								'color_red'=>'3,6,9,12,15,18,21,24',
								'color_green'=>'1,4,7,10,16,19,22,25',
								'color_blue'=>'2,5,8,11,17,20,23,26'
							);
							$set_color="nocolor";
							$_num=explode(",",$v['data']);
							foreach($map as $k1=>$v1)
							{
								$_narray = explode(',',$v1);
								if(in_array($_num[3],$_narray)) 
								{
									$set_color=$k1;
									break;
								}
							}
							echo "<span class='draw_list_luck28'>".$_num[0]." + ".$_num[1]." + ".$_num[2]." = <span class='luck28ball draw_list_luck28 ".$set_color."'>".$_num[3]."</span></span>";
						}
?>
            			</div>
            		</div>
            	</li>
<?php
			}
?>
            </ul>
        </div>
    </div>
</div>
<?php require(TPL.'/index_foot.tpl.php');?>
<script>
    function goUrl() {
        var url = '/game/index?id=<?=$type_id?>';
        location.href = url;
    }
</script>
</body></html>