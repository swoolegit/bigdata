<html lang="en"><head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
<meta name="format-detection" content="telephone=no" />
<meta name="apple-mobile-web-app-capable" content="yes">
<title><?php echo $this->config['webName'];?></title>
<link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<link href="/static/theme/<?=THEME;?>/css/m.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<link href="/static/theme/<?=THEME;?>/css/lottery.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/jquery.zclip.min.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/jquery.cookie.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/jquery.slimscroll.min.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/array.ext.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/rawdeflate.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/select.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/common.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/function.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/game.js?v=<?php echo $this->version;?>"></script>
<script src="/static/script/jquery.rotate.js?v=<?php echo $this->version;?>"></script>

</head>
<body>
<div class="bet_step_1 header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="popPrizeWin(0);">reveal</button>
        </div>
        <h1 class="ui-betting-title">
            <div class="bett-top-box">
                <div class="bett-play">玩法</div>
                <div class="bett-tit"><span id="play_name" style="font-size: 14px;">定位胆-定位胆</span><i class="bett-attr"></i></div>
            </div>
        </h1>
        <div class="ui-bett-right">
            <a class="bett-head" href="javascript:;"></a>
        </div>
    </div>
    <!-- 菜单右侧tips -->
	<div class="beet-rig" style="display: none;">
	    <ul>
	        <li><a href="/bet/log">投注记录</a></li>
	        <li><a href="/game/draw_list?id=<?php echo $type_id?>">最近开奖</a></li>
	        <?php
	        $id=array('1','3','5','6','7','9','10','12','14','26','15','16','20','25','60','63','64','53','61','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89');
			if(in_array($type_id, $id))
			{
				?>
				<li><a href="/trend/index?id=<?=$type_id?>">走势图</a></li>
				<?php
			}else
			{
				?>
				<li><a href="/user/index">会员中心</a></li>
				<?php				
			}
	        ?>	        
	    </ul>
	</div>
</div>
<div id="step_1">
        <div id="wrapper_1" class="scorllmain-content scorllmain-Beet nobottom_bar" style="padding-top: 44px; padding-bottom: 44px;">
            <div id="game-lottery" class="sub_ScorllCont" type="<?php echo $type_id;?>" ctype="<?php echo $types[$type_id]['type']?>">
                <div id="lot-tip" class="lot-tip">
				  <figure>
					<div class="dot white"></div>
					<div class="dot"></div>
					<div class="dot"></div>
					<div class="dot"></div>
					<div class="dot"></div>
				</figure>
                </div>
                <div class="lot-center">
                    <div class="lot-up">
                        <div class="lot-up-left"><a href="javascript:;" onclick="lottery.gameRandomOneBet();"><img src="/static/theme/<?=THEME;?>/image/yaoyiyao.png"></a></div>
                        <!-- div class="menu_offon">
                        <span class="go-back" style="display: none; ">
                            <i id="all-count2">0</i>
                            <a href="javascript:popPrizeWin(1);">购彩清单</a>
                        </span>
                        <span id="menu_switch_css"; class="add btn btn-red"><a id="menu_switch" href="javascript:changeMenu();">清单on</a></span>
                        </div -->
                        <!-- 文字选择 -->
                        <span class="icon-calendar robot_bet" onclick="lottery.game_robot();">
                            <a href="javascript:;">挂机</a>
                        </span>
                    </div>
					<div id="play-data" class="play-data">
						<?php require(TPL.'/game/play_data.tpl.php');?>
					</div>
					<div style="height: 35px;">
					</div>
				</div>
            </div>
        </div>
    <!-- begin 玩法menu tips -->
<?php
if($type_type<>11)
{
?>
    <div id="beet-tips" class="beet-tips" style="display: none;" hidden="">
        <div class="beet-tips-tit"><span id="play_title">定位胆</span></div>
        <div class="clear"></div>
        <ul id="group_list">
			<?php
				foreach ($groups as $gid => $group) {
					$class = $gid == $group_id ? ' class="on"' : '';
			?>
			<li><a data-id="<?php echo $group['id'];?>" href="javascript:;"<?php echo $class;?>><?php echo $group['groupName'];?></a></li>
			<?php }?>
        </ul>
        <div class="beet-tips-tit"><span id="sub_title">定位胆</span></div>
        <div class="clear"></div>
        <ul id="play_list" class="play_list">
        	<?php require(TPL.'/game/play_index.tpl.php');?>
        </ul>
    </div>
<?php
}else
{
?>
    <div id="beet-tips" class="beet-tips" style="display: none;" hidden="">
        <div class="beet-tips-tit"><span id="play_title">定位胆</span></div>
        <div class="clear"></div>
        <ul id="group_list">
			<li><a data-id="84" href="javascript:;" class="on">和值</a></li>
			<li><a data-id="85" href="javascript:;">两面</a></li>
			<li><a data-id="86-1" href="javascript:;">色波</a></li>
			<li><a data-id="86-2" href="javascript:;">豹子</a></li>
			<li><a data-id="86-3" href="javascript:;">特码包三</a></li>
        </ul>
    </div>
<?php	
}
?>
    <!-- end 玩法menu tips -->
<div class="bett-foot-box">
	<div class="bett-foot-2">
		<span id="play-mod" class="bett-foot-2-txt">
		<?php
			$mods = array(
				array(
					'switch' => $this->config['yuanmosi'],
					'rebate' => $this->config['betModeMaxFanDian0'],
					'value'  => '2.000',
					'name'   => '元',
				),
				array(
					'switch' => $this->config['jiaomosi'],
					'rebate' => $this->config['betModeMaxFanDian1'],
					'value'  => '0.200',
					'name'   => '角',
				),
				array(
					'switch' => $this->config['fenmosi'],
					'rebate' => $this->config['betModeMaxFanDian2'],
					'value'  => '0.020',
					'name'   => '分',
				),
				array(
					'switch' => $this->config['limosi'],
					'rebate' => $this->config['betModeMaxFanDian3'],
					'value'  => '0.002',
					'name'   => '厘',
				),
			);
			$first = true;
			foreach ($mods as $mod) {
				if ($mod['switch'] == 1) {
					if ($first) {
						$class = 'danwei trans on';
						$first = false;
					} else {
						$class = 'danwei trans';
					}
					echo '<b value="'.$mod['value'].'" data-max-fan-dian="'.$mod['rebate'].'" class="'.$class.'">'.$mod['name'].'</b>';
				}
			}
		?>
		</span>
		<div id="beishu-warp">
			<span class="name">倍数：</span>
			<i class="sur trans icon-minus"></i>
			<input type="text" autocomplete="off" id="beishu-value" value="<?php echo (array_key_exists('beiShu', $_COOKIE) && is_numeric($_COOKIE['beiShu']) && $_COOKIE['beiShu'] > 0) ? intval($_COOKIE['beiShu']) : 1;?>">
			<i class="add trans icon-plus" ></i>
		</div>

	</div>

	<div class="bett-foot">
		<!--button class="btn-none" onclick="lottery.game_remove_checked();">清空</button-->
<?php
if($type_type<>11)
{
?>
		<button class="btn-none" onclick="lottery.game_zhui_hao();">追号</button>
<?php
}
?>
		<div class="beet-foot-txt">
	        <p id="bet_step_1"><span id="all-count">0</span>注&nbsp;&nbsp;<span id="all-amount" class="red">0</span>元</p>
		</div>
		<button class="btn-add" id="click_buy" onclick="lottery.gameOrderRightNow();">投注</button>
		<button class="btn-add" id="click_prepare" onclick="lottery.game_add_code();" style="display: none;">放入清单</button>
	</div>
</div>
</div>
<!-- begin 投注籃 -->
<div id="step_2" style="margin-top: 64px; margin-bottom: 44px; display: none;">
	<div class="header">
	    <div class="headerTop">
	        <div class="ui-toolbar-left">
	            <button class="reveal-left" onclick="popPrizeWin(0);">reveal</button>
	        </div>
	        <h1 class="ui-toolbar-title">
	            <div class="bett-top-box">购彩清单</div>
	        </h1>
	    </div>
	</div>
	<div id="wrapper_1" class="scorllmain-content scorllmain-Beet-list nobottom_bar">
	    <div class="sub_ScorllCont">
	        <div class="betting-wrap">
	            <div class="bett-top-btn">
	                <button type="button" onclick="popPrizeWin(0);" class="btn-default bet-btn">返回添加</button>
	                <button type="button" onclick="lottery.gameRandomOneBet(2);" class="btn-default bet-btn">机选一注</button>
	                <button type="button" onclick="lottery.game_zhui_hao();" class="btn-default bet-btn">智能追号</button>
	            </div>
	            <div class="betting-info-box">

                <div class="bet-top"></div>
                <div class="bet-info" id="bets-cart">
                    <ul id="bet_list">

                    </ul>
                </div>
                <div class="bet-foot"></div>

	            </div>
	        </div>
	    </div>
	</div>
<div class="bett-foot-box">
	<div class="bett-foot">
		<input type="hidden" id="zhuiHao" name="zhuiHao" value="0">
		<button class="btn-none" onclick="lottery.game_remove_code();">清空</button>
		<div class="beet-foot-txt">
	        <p id="bet_step_1"><span id="all-count3">0</span>注&nbsp;&nbsp;<span id="all-amount2" class="red">0</span>元</p>
		</div>
		<button class="btn-add" onclick="lottery.game_post_code();" id="btnPostBet">投注</button>
	</div>
</div>
</div>
<!-- end 投注籃 -->
<!-- begin 追號-->
<div id="step_3" style="margin-top: 44px; margin-bottom: 44px; display: none;">
	<div class="header">
	    <div class="headerTop">
	        <div class="ui-toolbar-left">
	            <button class="reveal-left" onclick="popPrizeWin(0);">reveal</button>
	        </div>
	        <h1 class="ui-toolbar-title">
	            <div class="bett-top-box">追号清单</div>
	        </h1>
	    </div>
	</div>
	<div id="zhuihao_display" class="zhuihao_display" >

	</div>
<div class="bett-foot-box">
	<div class="bett-foot">
		<input type="hidden" id="zhuiHao" name="zhuiHao" value="0">
		<div class="beet-foot-txt">
			<span class="zhuiHao_mode"><input type="checkbox" id="zhuiHao_mode" checked></span>
			<label class="zhuiHao_mode_text" for="zhuiHao_mode">中奖后停止追号</label>
	        <p id="bet_step_1" class="zhuiHao_foot_text"><span id="zhuihao_num">0</span>期&nbsp;&nbsp;<span id="zhuihao_total" class="red">0</span>元</p>
		</div>
		<button class="btn-add" onclick="lottery.zhuihao_sure();">投注</button>
	</div>
</div>

</div>
<!-- end 追號 -->
<div id="loading-page" class="dialogue"><div class="dialogue-warp"></div></div>
<div id="dialogue" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
			<div class="dialogue-auto">
				( <span class="dialogue-sec"></span> )
			</div>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">

			<div class="dialogue-foot-button">
				<button class="dialogue-yes btn btn-blue icon-ok"></button>
				<button class="dialogue-no btn btn-white icon-undo"></button>
			</div>
		</div>
	</div>
</div>

<!-- begin 挂机 -->
<div id="game_robot" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">
			<div class="dialogue-foot-button">
				<button class="dialogue-yes btn btn-blue icon-ok" onclick="lottery.star_robot();" id="star_robot">自动挂机</button>
				<button class="dialogue-no btn btn-white icon-undo" onclick="lottery.stop_robot();">取消挂机</button>
			</div>
		</div>
	</div>
</div>
<!-- end 挂机 -->

<div id="fandian-value" data-bet-count="<?php echo $this->config['betMaxCount'];?>" data-bet-zj-amount="<?php echo $this->config['betMaxZjAmount'];?>" max="<?php echo $this->user['fanDian'];?>" game-fan-dian="<?php echo $this->config['fanDianMax'];?>" fan-dian="<?php echo $this->user['fanDian'];?>" game-fan-dian-bdw="<?php echo $this->config['fanDianBdwMax'];?>" fan-dian-bdw="<?php echo $this->user['fanDianBdw'];?>" class="left" style="display: none;"></div>
<script type="text/javascript">
var _display=0;
~(function() {
	window.game = {
		type: <?php echo $type_id;?>,
		type_type: <?php echo $type_type;?>,
		groupId: <?php echo $group_id;?>,
<?php
	if($type_type<>11)
	{
?>
		played: <?php echo $play_id;?>,
<?php
	}
?>
		stop: <?php echo $this->config['switchBuy'] == 0 ? 'true' : 'false';?>,
		ban: <?php echo ($this->config['switchDLBuy'] == 0 && $this->user['type']) ? 'true' : 'false';?>,
	};
	// 初始化历史选择模式
	var mode = $.cookie('mode');
	if (mode) $('#play-mod b[value="' + mode + '"]').addClass('on').siblings('b.on').removeClass('on');
	// 选择追号投注
	$('#bets-cart tr.code').live('click', function() {
		$(this).addClass('choosed').siblings('tr.choosed').removeClass('choosed');
	});
	// 开奖数据块首次加载
	setTimeout(function() {
		$.load('/game/lottery?id=<?php echo $type_id;?>', '#lot-tip');
	}, 100);
	$('.ui-betting-title').click(function(event){
    //$('.ui-betting-title').bind('touchend', function (event) {
    	//event.preventDefault();
    	event.stopPropagation();
	    $('.beet-tips').toggle();
        $('.beet-rig').hide();
	});
	$('.ui-bett-right').click(function(event){
    	event.stopPropagation();
	    $('.beet-tips').hide();
        $('.beet-rig').toggle();
	});
})();
function popPrizeWin (status) {
	if(status==0 && _display==0)
	{
		location.href="/";
	}
	if(status==0 && _display==1)
	{
		_display=0;
		$('#zhuiHao').val(0);
		//begin 不使用選號藍  by robert
		lottery.game_remove_code();
		//end 不使用選號藍  by robert
		$('#step_2').hide();
		$('#step_1').show();
		$('#step_3').hide();
	}
	if(status==1)
	{
		_display=1;
		$('#zhuiHao').val(0);
		$('#step_3').hide();
		$('#step_2').hide();
		$('#step_1').show();
	}
	if(status==2)
	{
		_display=1
		//_display=2;
		$('#step_3').show();
		$('#step_2').hide();
		$('#step_1').hide();
	}
}
$(function() {
	lottery.play_title=$('#group_list .on').html();
	lottery.sub_title=$('#play_list .on').html();
	$('#play_title').html(lottery.play_title);
	$('#sub_title').html(lottery.sub_title);
	lottery.set_play_name();

});
function changeMenu()
{
	$('.go-back').toggle();
	if($('.go-back').is(':visible')) {
		$('#menu_switch').text('清单off');
		$('#click_prepare').show();
		$('#click_buy').hide();
		$('#menu_switch_css').css("background-color","#166355");
	}else
	{
		$('#menu_switch').text('清单on');
		$('#click_prepare').hide();
		$('#click_buy').show();
		$('#menu_switch_css').css("background-color","#d33a32");
	}
}
</script>
</body>
</html>