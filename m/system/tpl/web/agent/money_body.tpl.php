<?php

?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="tr_team_list">
		<td class="team_list">投注 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['expenditure'] ? number_format($all['expenditure'], 3, '.', '') : '0.000';?></td>
	</tr>	
	<tr class="tr_team_list">
		<td class="team_list">中奖 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['zj'] ? number_format($all['zj'], 3, '.', '') : '0.000';?></td>
	</tr>
	<tr class="tr_team_list">
		<td class="team_list">返点 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['fandian'] ? number_format($all['fandian'], 3, '.', '') : '0.000';?></td>
	</tr>	
	<!--tr  class="tr_team_list">
		<td class="team_list">分红 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['bonus'] ? number_format($all['bonus'], 3, '.', '') : '0.000';?></td>
	</tr-->
<?php
if($this->user['gongZi']>0)
{
?>

	<tr class="tr_team_list">
		<td class="team_list">工资 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['gongzi'] ? number_format($all['gongzi'], 3, '.', '') : '0.000';?></td>
	</tr>	
<?php
}
?>
	<tr class="tr_team_list">
		<td class="team_list">活动 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['broker'] ? number_format($all['broker'], 3, '.', '') : '0.000';?></td>
	</tr>
	<tr class="tr_team_list">
		<td class="team_list">充值 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['recharge'] ? number_format($all['recharge'], 3, '.', '') : '0.000';?></td>
	</tr>	
	<tr class="tr_team_list">
		<td class="team_list">提现 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['cash'] ? number_format($all['cash'], 3, '.', '') : '0.000';?></td>
	</tr>
	<!--tr class="tr_team_list">
		<td class="team_list">转账 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['transfer'] ? number_format($all['transfer'], 3, '.', '') : '0.000';?></td>
	</tr>
	<tr class="tr_team_list">
		<td class="team_list">总收入 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['income'] ? number_format($all['income'], 3, '.', '') : '0.000';?></td>
	</tr>	
	<tr class="tr_team_list">
		<td class="team_list">总支出 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['expenditure'] ? number_format($all['expenditure'], 3, '.', '') : '0.000';?></td>
	</tr-->
	<tr class="tr_team_list">
		<td class="team_list">盈亏 :</td>
        <td class="team_list team_list_right">￥ <?php echo $all['total'] ? number_format($all['total'], 3, '.', '') : '0.000';?></td>
	</tr>
</table>