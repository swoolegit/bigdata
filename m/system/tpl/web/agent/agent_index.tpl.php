<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/common.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/agent.js?v=<?php echo $this->version;?>"></script>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/lobby.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
	<link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link href="/static/theme/<?=THEME;?>/css/mobiscroll.css" rel="stylesheet" type="text/css" />
    <script src="/static/script/mobiscroll.js" type="text/javascript"></script>
    <title>投注记录</title>
	<style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .order-center ul li a
        {
        	height:auto;
        	background:none;
        }
    </style>
</head>

<body class="login-bg">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl(1)">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">    
                <div class="bett-tit-noborder">
                	<span id="teamtitle">
<?php
switch($args['set'])
{
	case '1':
		echo "添加下级";
		break;
	case '2':
		echo "链接开户";
		break;
	case '3':
		echo "团队资金";
		break;
}
?>
					</span>
                </div>
            </div>
        </h1>
        <div class=" header-icon">
            
        </div>
    </div>
</div>

<!--div style="padding-top: 44px;">
	<div style="width: 100%">
		<a href="javascript:;"><div class="agent_item agent_active" data-type="1" >添加下级</div></a>
		<a href="javascript:;"><div class="agent_item" data-type="2"  >链接开户</div></a>
		<a href="javascript:;"><div id="teammoney" class="agent_item" data-type="3" style="border-right: 0px;">团队资金</div></a>
	</div>
</div-->
<div id="agent-member-dom">
<!-- begin 添加代理 -->
<div id="member_add" class="member_add_box" <?php if($args['set']!="1"){echo 'style="display: none"';}?> >
<form action="/agent/member_add" target="ajax" func="form_submit">
		<div class="item">
			<span class="name">用户名</span><span class="value fandian"><input type="text" name="username" id="username" required title="用户名" placeholder="输入用户名" style="width: 192px;" ></span>
		</div>
		<div class="item">
			<span class="name">登录密码</span><span class="value fandian"><input type="text" name="password" id="password" required title="登录密码" placeholder="输入登录密码" style="width: 192px;"></span>
		</div>	
		<div class="item">
			<span class="name">会员类型</span><span class="addon name" style="width: 192px;padding: 0px 10px;">
				<label><input type="radio" name="type" value="1" title="代理" checked="checked" >代理</label>&nbsp;&nbsp;&nbsp;
				<label><input name="type" type="radio" value="0" title="会员" >会员</label>				
				</span>
		</div>	
		<div class="item">
			<span class="name">用户返点</span><span class="value fandian"><input type="text" name="fanDian" id="fanDian" required title="用户返点" placeholder="投注返点" style="width:70px"></span><span class="addon name">%，上限：<?php echo $max;?></span>
		</div>
<?php
if($this->user['gongZi']>0)
{
?>
		<div class="item">
			<span class="name">日工资</span><span class="value fandian"><input type="text" name="gongZi" id="gongZi" required title="日工资" placeholder="日工资" style="width:70px"></span><span class="addon name">%，上限：<?php echo $this->user['gongZi'];?></span>
		</div>
<?php 
}
if($this->user['fenHong']>0)
{
?>
		<div class="item">
			<span class="name">分红</span><span class="value fandian"><input type="text" name="fenHong" id="fenHong" required title="分红" placeholder="分红" style="width:70px"></span><span class="addon name">%，上限：<?php echo $this->user['fenHong'];?></span>
		</div>
<?php
}
?>
		<div class="item">
			<span class="name">备注</span><span class="value fandian"><input type="text" name="memo" id="memo" title="备注" placeholder="备注" style="width: 192px;" ></span>
		</div>
    	<div class="btn-wrap">
			<button type="submit" class="more-btn" id="coinpassword">确认添加</button>
    	</div>		
</form>
</div>
<!-- end 添加代理 -->

<!-- begin  链接开户-->
<div id="spread_add" class="member_add_box" container="#agent-spread-dom .body" <?php if($args['set']!="2"){echo 'style="display: none"';}?> >
<form action="/agent/spread_link_add" target="ajax" func="form_submit">
		<div class="item">
			<span class="name">用户返点</span><span class="value fandian"><input type="text" name="fanDian" id="fanDian" required title="用户返点" placeholder="投注返点" style="width:70px"></span><span class="addon name">%，上限：<?php echo $max;?></span>
		</div>
		<div class="item">
			<span class="name">会员类型</span><span class="addon name" style="width: 192px;padding: 0px 10px;">
				<label><input type="radio" name="type" value="1" title="代理" checked="checked" >代理</label>&nbsp;&nbsp;&nbsp;
				<label><input name="type" type="radio" value="0" title="会员" >会员</label>				
				</span>
		</div>	
    	<div class="btn-wrap">
			<button type="submit" class="more-btn" id="coinpassword">确认添加</button>
    	</div>		
</form>
</div>
<!-- end 链接开户 -->

<!-- begin  团队资金-->
<div id="team_list" class="member_add_box" container="#agent-spread-dom .body"  <?php if($args['set']!="3"){echo 'style="display: none"';}?> >
<form action="/agent/money_search" class="search" container="#agent-money-dom .body" data-ispage="true" target="ajax" func="form_submit">
        <div data-role="content" data-theme="d" class="item">
          <span class="name">开始 : </span><span class="value fandian"><input type="text" name="fromTime" id="datetimepicker_fromTime" class="mobiscroll"  value="<?php echo date('Y-m-d', $this->request_time_from);?>" /></span>
        </div>
        <div data-role="content" data-theme="d" class="item">
         <span class="name"> 结束 : </span><span class="value fandian"><input type="text" name="toTime" id="datetimepicker_toTime" class="mobiscroll"  value="<?php echo date('Y-m-d', $this->request_time_to);?>" /></span>
        </div>        
		<div class="item">
			<span class="name">用户名</span><span class="value fandian"><input type="text" name="username" id="username"  title="用户名" placeholder="输入用户名"></span>
		</div>
    	<div class="btn-wrap">
			<button type="submit" class="more-btn" id="coinpassword">确认查询</button>
    	</div>
</form>	
</div>
<!-- end 团队资金 -->

</div>
<div id="wrapper_1" class="bet_list scorllmain-content scorll-order nobottom_bar paddingbutton" style="padding-top: 40px; padding-bottom: 44px;background-color: #252944;">
    <div class="sub_ScorllCont">
        <div class="mine-message" style="display: none;">
            <div class="mine-mess"><img src="/static/theme/default/image/wuxinxi.png"></div>
            <p>目前暂无记录哦！</p>
        </div>
        <div class="order-center">

        	
<?php
if($args['set']=="1")
{
?>
        	<ul id="bet_list">
<?php	
	$AES = core::lib('aes');
	foreach ($data as $v) 
	{
?>
            <li>
            	<div class="order-list-tit">
            		<span class="fr" style="color: #46BD36;">
            			返点: <?php echo $v['fanDian'];?>%
<?php
	if($this->user['gongZi']>0)
	{
?>

            			&nbsp;,&nbsp;工资: <?php echo $v['gongZi'];?>%
<?php 
	}
	if($this->user['fenHong']>0)
	{
?>
						&nbsp;,&nbsp;分红: <?php echo $v['fenHong'];?>%
<?php
	}
?>

            		</span>
            		<span class="order-top-left" style="width: auto;"><?php echo $v['username'];if ($v['uid'] == $this->user['uid']) echo '<span class="red" style="margin-left:3px">(自己)</span>';?></span>
            	</div>
            	<div class="c-gary">
            		<span style="width: 70px; float: left">
						<?php
							if ($this->user['uid'] != $v['uid'] && $v['parentId'] == $this->user['uid'] && $this->config['recharge']) {
								echo '<a class="icon-chart-line" onclick="team_list2(\''.$AES->urlsafe_b64encode($AES->encrypt($v['uid'])).'\',\''.$v['username'].'\')" href="javascript:;" >统计</a>';
							}
						?>
            		</span>
	            	<span class="fr" style="width: 70px;text-align: right;">
						<?php
							if ($this->user['uid'] != $v['uid'] && $v['parentId'] == $this->user['uid'] && $this->config['recharge']) {
								echo '<a class="icon-exchange" href="/agent/recharge?uid='.$AES->urlsafe_b64encode($AES->encrypt($v['uid'])) .'" target="ajax" func="loadpage">转账</a>';
							}
						?>		          					
	            	</span>
            	</div>
            </li>
<?php
	}
?>
			</ul>
<?php
}
?>

        </div>
    </div>
</div>
<div class="bet_detail bet_revokeok header" style="display: none;">
    <div class="header">
        <div class="headerTop">
            <div class="ui-toolbar-left">
                <button class="reveal-left" onclick="showStep(&#39;list&#39;)">reveal</button>
            </div>
            <h1 class="ui-toolbar-title"><span class="bet_detail">详情</span></h1>
        </div>
    </div>
</div>
<div id="wrapper_1" class="bet_detail scorllmain-content scorll-order nobottom_bar" style="display: none; padding-top: 44px;">
</div>

<!-- 加载中 -->
<style>
    .center {text-align: center}
</style>

<div id="loading-page" class="dialogue"><div class="dialogue-warp"></div></div>
<div id="dialogue" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
			<div class="dialogue-auto">
				( <span class="dialogue-sec"></span> )
			</div>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">

			<div class="dialogue-foot-button">
				<button class="dialogue-yes btn btn-blue icon-ok"></button>
				<button class="dialogue-no btn btn-white icon-undo"></button>
			</div>
		</div>
	</div>
</div>

<script>
function member_list()
{
	//order-center
	url="/agent/member_search";
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: {
        },
        timeout: 30000,
        success: function (data) {
            if(data!="")
            {
				$('.order-center').html(data);
            }
        }
    });
};
function spread_list()
{
	//order-center
	url="/agent/spread";
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: {
        },
        timeout: 30000,
        success: function (data) {
            if(data!="")
            {
				$('.order-center').html(data);
            }
        }
    });
};
function team_list()
{
	//order-center
	url="/agent/money";
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: {
        },
        timeout: 30000,
        success: function (data) {
            if(data!="")
            {
				$('.order-center').html(data);
            }
        }
    });
};
function team_list2(a1,a2)
{
	//order-center
    $('.agent_item').removeClass('agent_active');
    $('#teammoney').addClass('agent_active');
	$('#member_add').hide();
	$('#spread_add').hide();
	$('#team_list').show();
	$('#teamtitle').html('团队资金');
	url="/agent/money_search";
	$('#team_list #username').val(a2);
    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'html',
        data: {
			uid: a1,
			fromTime: $('#datetimepicker_fromTime').val(),
			toTime: $('#datetimepicker_toTime').val()
        },
        timeout: 30000,
        success: function (data) {
            if(data!="")
            {
				$('.order-center').html(data);
            }
        }
    });
};
function goUrl() {
	var url = '/user/index';
    location.href = url;
};
$(function () {
	$('.agent_item').live("click",function(event) {
        $('.agent_item').removeClass('agent_active');
        $(this).addClass('agent_active');
        type = $(this).data('type');
        if(type=="1")
        {
        	member_list();
        	$('#member_add').show();
        	$('#spread_add').hide();
        	$('#team_list').hide();
        }
        if(type=="2")
        {
        	spread_list();
        	$('#member_add').hide();
        	$('#spread_add').show();
        	$('#team_list').hide();
        }
        if(type=="3")
        {
        	team_list();
        	$('#member_add').hide();
        	$('#spread_add').hide();
        	$('#team_list').show();
        }
        
    });
    $('#datetimepicker_fromTime').scroller({ theme: 'ios',dateFormat: "yy-mm-dd", dateOrder: "yymmdd" });
	$('#datetimepicker_toTime').scroller({ theme: 'ios',dateFormat: "yy-mm-dd", dateOrder: "yymmdd" });
<?php
switch($args['set'])
{
	case '2':
		echo "spread_list();";
		break;
	case '3':
		echo "team_list();";
		break;
}
?>

});
</script>
</body></html>