<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telphone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link href="/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link href="/static/theme/<?=THEME;?>/css/m.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
	<script src="/static/script/common.js?v=<?php echo $this->version;?>"></script>

	<style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        body.login-bg{padding-top:97px;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>

    <title>首页</title>
</head><script type="text/javascript" id="useragent-switcher">
navigator.__defineGetter__("userAgent", function() {return "Mozilla/5.0 (Android 4.4; Mobile; rv:18.0) Gecko/18.0 Firefox/18.0"})</script>
<body id="body1">
<div class="bet_list header" style="position: fixed; top: 0;">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">    
                <div class="bett-tit-noborder">
                    大数据
                </div>
            </div>
        </h1>
        <div class=" header-icon">
            <a class="header-logout" href="/user/logout">退出</a>
        </div>
    </div>
</div>

<div id="wrapper_1" class="scorllmain-content nobottom_bar scrollable" style="padding-top: 44px; padding-bottom: 80px;">
    <div class="sub_ScorllCont">
		<!-- begin 跑馬燈 -->
        <div class="bulletin" >
            <i class="icon-volume-down"></i>
            <marquee behavior="scroll"><span id="horse"><?php echo $this->config['webGG'];?></span></marquee>
        </div>
        <!-- end 跑馬燈 -->

    	<!-- begin banner -->
        <div class="header-banner" style="overflow: hidden;">
            <img class="banner" style="width: 100%; display: block; height: 150px;" src="<?=IMAGE_PATCH?>/promotImg_004.jpg?v=3">
		</div>
		<!-- end banner -->
        <div style="clear:both;"></div>
        <div class="hot-tit" style="height: 10px;"></div>
        <!-- begin 彩種入口 -->
        <div class="hot-lot">
                        <ul>
<?php
	$types = $this->get_types();
	foreach ($types as $name => $list) {
		if (!$list) continue;
		foreach ($list as $v) {
			if($v['id']==24 || $v['id']==69)
			{
				continue;
			}
			$img_id=array
			(
				1=>1,
				70=>70,
				74=>1,
				63=>72,
				72=>1,
				64=>70,
				73=>72,
				71=>70,
				12=>1,
				5=>72,
				53=>53,
				65=>65,
				76=>1,
				77=>72,
				78=>1,
				79=>72,
				80=>1,
				81=>72,
				82=>1,
				83=>70,
				84=>70,
				85=>70,
				86=>70,
				87=>1,
				88=>72,
				89=>72,
				62=>72,
				66=>65,
				67=>65,
				68=>65,
				25=>25,
				7=>7,
				6=>7,
				9=>9,
				75=>75,
				20=>20,
				61=>75,
			);
?>
                <li>
                    <a data-gid="<?php echo $v['id'];?>" href="javascript:goUrl(<?php echo $v['id'];?>)">
                        <div class="hot-icon icon_idx_0" data-idx="<?php echo $v['id'];?>" data-gid="0"><img src="/static/theme/default/image/lottery_type/<?=$img_id[$v['id']];?>.png"></div>
                        <p class="hot-text text_idx_0"><?php echo $v['title'];?></p>
                    </a>
                </li>
<?php
		}
	}
?>
            </ul>
        </div>
        <!-- end 彩種入口 -->

    </div>
</div>
<?php require(TPL.'/index_foot.tpl.php');?>
<style>
    .center {text-align: center}
</style>
<script>
function goUrl(gid) {
    //if ($('li.game_'+gid+' img').css('filter') != 'none') {
    /*
    if (gameMaintCls.indexOf('.game_'+gid+' img') > -1) {
        msgAlert('该彩种正在维护中');
        return;
    }
    */
    var url = '/game/index?id='+gid;
    //var url = '/bet/'+gameOpArr[gid]+'.html';
    location.href = url;
}

$(function (){
<?php
	
	if (!isset($_SESSION['alertMsgs'])) {
		$sql = "select * from {$this->db_prefix}content where enable=2";
		$alertMsgs = $this->db->getRows($sql);
		if ($alertMsgs) foreach ($alertMsgs as $rows) {
			$content = preg_replace("/\r*\n/","<br>",htmlspecialchars($rows['content']));
			echo "$.dialogue({type: 'success',text: '{$content}',auto: true,yes: {text: '我同意'}});";
		}
		$_SESSION['alertMsgs'] = true;
	}
?>


});
</script>
<div id="loading-page" class="dialogue"><div class="dialogue-warp"></div></div>
<div id="dialogue" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
			<div class="dialogue-auto">
				( <span class="dialogue-sec"></span> )
			</div>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">

			<div class="dialogue-foot-button">
				<button class="dialogue-yes btn btn-blue icon-ok"></button>
				<button class="dialogue-no btn btn-white icon-undo"></button>
			</div>
		</div>
	</div>
</div>
</body></html>