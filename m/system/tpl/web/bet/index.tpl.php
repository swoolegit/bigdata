<!DOCTYPE html>
<html lang="en"><head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=0" name="viewport">
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <script src="/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/bet.list.js?v=<?php echo $this->version;?>"></script>
    <script src="/static/script/lottery_data.js?v=<?php echo $this->version;?>"></script>
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/index.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/lobby.css" type="text/css">
    <link rel="stylesheet" href="/static/theme/<?=THEME;?>/css/user.css" type="text/css">
    <style>
        body,#wrapper_1{-webkit-overflow-scrolling:touch;overflow-scrolling:touch;}/*解决苹果滚动条卡顿的问题*/
        #wrapper_1{overflow-y:visible!important;}
        .lott-menu{position:fixed;top:44px;left:0;margin-top:0!important;}
    </style>
    <title>投注记录</title>
</head>
<body class="login-bg">
<div class="bet_list header">
    <div class="headerTop">
        <div class="ui-toolbar-left">
            <button id="reveal-left" onclick="goUrl()">reveal</button>
        </div>
        <h1 class="ui-betting-title">
             <div class="bett-top-box">    
                <div class="bett-tit">
                    <span id="order_type">全部订单</span><i class="bett-attr" style="display: &#39;&#39;"></i>
                </div>
            </div>
        </h1>
        <div class="ui-bett-refresh">
            <a class="bett-refresh" href="javascript:;" ></a>
        </div>
    </div>
</div>

<div class="bet_detail bet_revokeok header" style="display: none;">
    <div class="header">
        <div class="headerTop">
            <div class="ui-toolbar-left">
                <button class="reveal-left" onclick="showStep(&#39;list&#39;)">reveal</button>
            </div>
            <h1 class="ui-toolbar-title"><span class="bet_detail">详情</span><span class="bet_revokeok">撤单</span></h1>
        </div>
    </div>
</div>

<div id="wrapper_1" class="bet_list scorllmain-content scorll-order nobottom_bar paddingbutton" style="padding-top: 44px; padding-bottom: 44px;">
    <div class="sub_ScorllCont">
        <div class="mine-message" style="display: none;">
            <div class="mine-mess"><img src="/static/theme/default/image/wuxinxi.png"></div>
            <p>目前暂无记录哦！</p>
        </div>
        <div class="order-center">
            <ul id="bet_list">
                <!--<li class="loading">
                    <div class="order-list-tit"></div>
                    <div class="c-gary" style="text-align: center">正在加载...</div>
                </li>-->
<?php
	foreach ($data as $v) 
	{
			$this_type = $types[$v['type']];
			$money=$v['mode'] * $v['beiShu'] * $v['actionNum'];
			$win=0;
			$needrevoke=1;
			$close=0;
			if($v['zjCount'])
			{
				$win=1;
				$close=1;
			}
			if($v['isDelete']==1)
			{
				$close=1;
				$win=2;
			}
			
			if ($v['lotteryNo'] || $v['isDelete']==1 || $v['kjTime'] < $this->time) {
				$close=1; 
				$needrevoke=0;
			} 
			$bonus=$v['lotteryNo'] ? number_format($v['bonus'], 2, '.', '') : '0.00';
?>
		<?php
		if (mb_strlen($v['actionData']) > 20) {
			$v['actionData'] = mb_substr( $v['actionData'],0,20,"utf-8").'...';		
		}
		
		?>
            <li><a href="javascript:showStep('detail','<?php echo $v['id'];?>')" 
            	data-detail="oid=<?php echo $v['wjorderId'];?>&money=<?php echo $money;?>&count=<?php echo $v['actionNum'];?>&prize=<?php echo $v['bonusProp'];?>&reward=<?php echo $v['fanDian'];?>&time=<?php echo date('Y-m-d H:i:s',$v['actionTime']);?>&win=<?php echo $win;?>&pname=<?php echo $plays[$v['playedId']]['name'];?>&code=<?php echo $v['actionData'];?>&subid=&gname=<?php echo $this_type['title'];?>&winamount=<?php echo $bonus;?>&period=<?php echo $v['actionNo'];?>&opennum=<?php echo $v['lotteryNo'];?>&close=<?php echo $close;?>&status=1&gid=<?php echo $v['type'];?>&pos=&key=<?php echo $v['id'];?>" 
            	data-needrevoke="1" class="<?php echo $v['id'];?>">
            	<div class="order-list-tit"><span class="fr c-red"><?php echo $money;?>元</span><span class="order-top-left"><?php echo $this_type['title'];?></span>第<?php echo $v['actionNo'];?>期</div>
            	<div class="c-gary"><span class="fr">
<?php
			if ($v['isDelete'] == 1) {
				echo '已撤单';
			} elseif (!$v['lotteryNo']) {
				echo '未开奖';
			}elseif($v['zjCount']){
				echo '<span style="color: #46BD36;">已派奖 '.$bonus.'元</span>';
			}else{
				echo '未中奖';
			}
?>
            	</span><p class="order-time"><?php echo date('Y-m-d H:i:s', $v['actionTime']);?></p></div></a>
            </li>
<?php
	}
?>
			</ul>
        </div>
    </div>
</div>

<!-- 直选tips -->
<div class="bet_list beet-tips" hidden="">
    <!--<div class="beet-tips-tit"><span>普通玩法</span></div>-->
    <div class="clear"></div>
    <ul style="background-color: #1b1c2e;">
	<?php foreach ($state as $k => $v) {
		$css="";
		if ($k == $args['state']) $css='on';
	?>
	<li><a class="<?php echo $css;?>" href="javascript:;" data-type="<?php echo $k;?>"><?php echo $v;?></a></li>
	<?php }?>
    </ul>
</div>

<div id="wrapper_1" class="bet_detail scorllmain-content scorll-order nobottom_bar" style="display: none; padding-top: 44px;">
    <div class="sub_ScorllCont">
        <div class="order-box">
            <div class="order-tit">
                <div class="order-icon" style="height: 55px;"><img id="game_logo" alt=""></div>
                <div class="order-top-right">
                    <span class="order-name f120"></span> <span class="grey f80"></span>
                    <div class="lot-number">
                        <span class="grey fl">开奖号码：</span>
                        <div id="open_num" class="red"></div>
                    </div>
                    <div id="win_amt" class="lot-number" style="display: none;">
                        <span class="green fl" style="color: #46BD36;"></span>
                    </div>
                </div>
            </div>
            <div class="order-info">
                <h3>订单内容</h3>
                <ul style="padding: 10px;">
                    <li style="padding-bottom: 3px;"><span class="grey1">订单号</span><span class="grey" id="bet_oid"></span></li>
                    <li style="padding-bottom: 3px;"><span class="grey1">投注金额</span><span class="grey" id="bet_money"></span></li>
                    <li style="padding-bottom: 3px;"><span class="grey1">投注注数</span><span class="grey" id="bet_count"></span></li>
                    <li style="padding-bottom: 3px;"><span class="grey1">投注返点</span><span class="grey" id="bet_reward"></span></li>
                    <li style="padding-bottom: 3px;"><span class="grey1">投注赔率</span><span class="grey" id="bet_prize"></span></li>
                    <li style="padding-bottom: 3px;"><span class="grey1">投注时间</span><span class="grey" id="bet_time"></span></li>
                    <li style="padding-bottom: 3px;"><span class="grey1">是否中奖</span><span class="grey" id="bet_status"></span></li>
                    <!--li><span class="grey">是否关盘</span><span id="bet_close"></span></li-->
                    <li style="padding-bottom: 3px;"><span class="grey1">玩法名称</span><span class="grey" id="bet_pname"></span></li>
                </ul>
            </div>
            <div class="order-info order-kit">
                <h3>投注号码</h3>
                <ul id="code" class="grey" style="padding: 10px;"></ul>
            </div>
        </div>
        <button class="order-btn" data-key="" data-gid="">再来一注</button>
    </div>
</div>

<div id="wrapper_1" class="bet_revokeok scorllmain-content scorll-order nobottom_bar" style="display: none; padding-top: 44px;">
    <div class="sub_ScorllCont">
        <div class="with-end text-center">
            <div class="wiht-img"><img src="/static/theme/default/image/gou_wancheng.png" alt=""></div>
            撤单成功，预祝您中奖
        </div>
        <button class="charge-btn" onclick="showStep(&#39;list&#39;);" style="margin: 20px auto">确认</button>
    </div>
</div>

<!-- 加载中 -->
<div class="loading" style="left: 50%; margin-left: -2em; display: none;">加载中...</div>
<div class="loading-bg" style="display: none;"></div>

<script>
    function loadingShow(tips,bg) {
        if(tips == ""||typeof(tips) == "undefined"){
            $(".loading").css("left","50%");
            $(".loading").css("margin-left","-2em");
            $(".loading").html("加载中...");
        }else{
            $(".loading").html(tips);
            $(".loading").css("left",Math.ceil(($(document).width() - $(".loading").width())/2));
            $(".loading").css("margin-left",0);
        }

        bg   = (bg == ""||typeof(bg) == "undefined")?1:0;
        if (bg == 1){
            $(".loading-bg").show();
        }else{
            $(".loading-bg").hide();
        }
        $(".loading").show();
    }
    function loadingHide() {
        $(".loading").hide();
        $(".loading-bg").hide();
    }
</script>

<style>
    .center {text-align: center}
</style>

<div id="tip_bg" class="tips-bg" style="display: none;"></div>

<script>
    var func;
    function tipOk() {
        $('#tip_pop').hide();
        $('#tip_bg').hide();
        if (/系统维护/g.test($('div#tip_pop_content').html())) {
            location.href = '/index/index.html';
            return;
        }
        if (typeof(func) == "function"){
            func();
            func = "";
        }else{
            if (typeof(doTipOk) == "function") {
                doTipOk();
            }
        }
    }
    function msgAlert (msg,funcParm) {
        $('div#tip_pop_content').html(msg);
        $('div#tip_pop').show();
        $('div#tip_bg').show();
        func = (funcParm == ""||typeof(funcParm) != "function")?'':funcParm;
    }
</script>

<div class="beet-odds-tips round" id="confirm_pop" style="display: none; height:130px;">
    <div class="beet-odds-info f100">
        <div class="beet-money" id="confirm_pop_content" style="font-size: 120%; margin-top: 15px; color:#666;">
            号码选择有误
        </div>
    </div>
    <div class="beet-odds-info text-center">
        <button class="btn-que btn-que-no" onclick="confirmCancel()"><span id="confirm_pop_cancel">取消</span></button>
        <button class="btn-que" style="width: 50%;" onclick="confirmOk()"><span id="confirm_pop_ok">确定</span></button>
    </div>
</div>

<div id="confirm_bg" class="tips-bg" style="display: none;"></div>

<script>
    var confirmFunc;
    function confirmOk() {
        $('#confirm_pop').hide();
        $('#confirm_bg').hide();
        if (/系统维护/g.test($('div#confirm_pop_content').html())) {
            location.href = '/index/index.html';
            return;
        }
        if (typeof(confirmFunc) == "function"){
            confirmFunc();
            confirmFunc = "";
        }else{
            if (typeof(doConfirmOk) == "function") {
                doConfirmOk();
            }
        }
    }
    function confirmCancel() {
        $('#confirm_pop').hide();
        $('#confirm_bg').hide();
        if (/系统维护/g.test($('div#confirm_pop_content').html())) {
            location.href = '/index/index.html';
            return;
        }
        if (typeof(doConfirmCancel) == "function") {
            doConfirmCancel();
        }
    }
    function msgConfirm (msg,funcParm,textConfirm,textCancel) {
        textConfirm = (textConfirm == undefined) ? '' : textConfirm;
        textCancel = (textCancel == undefined) ? '' : textCancel;
        if (textConfirm != '') {
            $('span#confirm_pop_ok').text(textConfirm);
        }
        if (textCancel != '') {
            $('span#confirm_pop_cancel').text(textCancel);
        }
        $('div#confirm_pop_content').text(msg);
        $('div#confirm_pop_cancel').html(textCancel);
        $('div#confirm_pop').show();
        $('div#confirm_bg').show();
        confirmFunc = (funcParm == ""||typeof(funcParm) != "function")?'':funcParm;
    }
</script>
<input type="hidden" id="revokeKey">

<script>
	var resUrl="/static/theme/default/image/lottery_type";
    function showStep(step,key) {
        if (step == undefined) {
            return;
        }
        $('.bet_list').hide();
        $('.bet_detail').hide();
        $('.bet_revokeok').hide();
        $('.bet_'+step).show();
        if (step == 'detail') {
            showDetail(key);
        }
    }

//弹窗
$(function(){
		$('.bett-top-box').click(function(event){
	    //$('.ui-betting-title').bind('touchend', function (event) {
	        event.stopPropagation();
		    $('.beet-tips').toggle();
	        //$('.beet-rig').hide();
		});
    $(window).bind("load",function(){

	    $('.beet-tips').click(function(event){
	    	if($(this).css('display')!='none'){
	    		event.stopPropagation();
	    	}
	    });
	    
	    $(document).on("click",function() {
	        if($('.beet-tips').is(':visible')) {
	           $('.beet-tips').toggle();
	        }
	    });	    
    });
    
   
    // scroll toggle off
    $(document).scroll(function() {
      if($('.beet-tips').is(':visible')) {
          $('.beet-tips').toggle();
        }
        else {
          $('.beet-tips').css('display','none');
        }
    });
    // end

    //  hide address bar
    window.addEventListener("load",function() {
        // Set a timeout...
        setTimeout(function(){
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 0);
    });
    <?php
    if(!$data)
	{
		echo "$('div.mine-message').show();";
	}
    ?>
    var url = '/bet/log';
    <?php
    if(isset($_GET['onlyWin']))
	{
		echo "$('.bett-attr').hide();";
		echo "$('.bett-top-box').attr('onclick','').unbind('click');";
		echo "url = '/bet/log?onlyWin=1';";
	}
    ?>    
});
function goUrl() {
	window.history.back();
}
</script>
</body></html>