<?php

class mod_trend extends mod
{
    public function index()
    {
		$id=array('1','3','5','6','7','9','10','12','14','26','15','16','20','25','60','63','64','53','61');
		$pgsid=array('30','50','80','100','120','');	
		//此处设置彩种id
		if (!array_key_exists('id', $_GET)) $_GET['id'] = 1;
		if(!in_array($_GET['id'],$id)) die("typeid error");
		$type_id=$_GET['id'];
		//每页默认显示
		$pgs=30;
		$sql="select time,number,data from `{$this->db_prefix}data` where type={$type_id} limit {$pgs}";
        $data = $this->db->query($sql, 3);

		$sql="select id,type,shortName from `{$this->db_prefix}type` where id={$type_id}";
        $type = $this->db->query($sql, 2);

		$sql="select id,type,shortName from `{$this->db_prefix}type` where enable=1 and type in (".implode(",",$id).") order by sort asc";
        $type_data = $this->db->query($sql, 3);

        $this->display('trend/index', array('data'=>$data,'type_data'=>$type_data,'type'=>$type['type'],'type_id'=>$type_id,'trendPos'=>1));
    }	

    public function trend_list()
    {
		$id=array('1','3','5','6','7','9','10','12','14','26','15','16','20','25','60','63','64','53','61');
		$pgsid=array('30','50','80','100','120','');	
		//此处设置彩种id
		if (!array_key_exists('trendGid', $_GET)) $_GET['trendGid'] = 1;
		if(!in_array($_GET['trendGid'],$id)) die("typeid error");
		$type_id=$_GET['trendGid'];
		//每页默认显示
		$pgs=30;
		$sql="select time,number,data from `{$this->db_prefix}data` where type={$type_id} limit {$pgs}";
        $data = $this->db->query($sql, 3);
		$sql="select id,type,shortName from `{$this->db_prefix}type` where id={$type_id}";
        $type = $this->db->query($sql, 2);
        $this->display('trend/trend_list', array('data'=>$data,'trendPos'=>$_GET['trendPos'],'type'=>$type['type']));
    }	

}
?>