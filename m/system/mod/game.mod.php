<?php

class mod_game extends mod
{
    private function get_play_info($play_id)
    {
        $sql = "SELECT `simpleInfo`,`info`,`example`,`groupId`,`playedTpl` FROM `{$this->db_prefix}played` WHERE `id`=$play_id LIMIT 1";
        return $this->db->query($sql, 2);
    }

    private function get_recent_bets($type_id)
    {
        $recentNo = core::lib('game')->get_game_recent_no($type_id, 5);
        $actionNo = $recentNo['actionNo'];
        $uid = $this->user['uid'];
        $sql = "SELECT * FROM `{$this->db_prefix}bets_repl` WHERE `isDelete`=0 AND `type`={$type_id} AND `uid`=$uid AND `actionNo`>='{$actionNo}' ORDER BY `id` DESC, `actionTime` DESC LIMIT 0, 50";
        return $this->db->query($sql, 3);
    }

    public function index()
    {
        if (!array_key_exists('id', $_GET)) $_GET['id'] = 1;
        $type_id = $this->get_id();
        $data = $this->db->query("SELECT `enable`,`title`,`type` FROM `{$this->db_prefix}type` WHERE `id`={$type_id} LIMIT 1", 2);
        if (!$data) core::__403();
        if (!$data['enable']) core::error($data['title'] . '已经关闭');
        	switch(true)
        	{
        		case $data['type'] == 11:
					$group_id = 6;
					//$play_id = 37;
			        $types = core::lib('game')->get_types();
		            $sql_type = $types[$type_id]['type'];
		            $sql = "SELECT `id`,`groupName`,`enable` FROM `{$this->db_prefix}played_group` WHERE `enable`=1 AND `type`=$sql_type ORDER BY `sort`";
		            $data = $this->db->query($sql, 3);
		            if (!$data) core::error('当前彩种下暂未添加玩法');
		            $groups = array();
		            foreach ($data as $v) $groups[$v['id']] = $v;
		            if (!array_key_exists($group_id, $groups)) $group_id = $data[0]['id'];
		            $sql = "SELECT `id`,bonusProp,bonusPropBase,example,groupId FROM `{$this->db_prefix}played` WHERE `enable`=1 AND `type`=$sql_type ORDER BY `sort`";
		            $data = $this->db->query($sql, 3);
		            $plays = array();
					foreach ($data as $v) 
					{
						$diff_fanDian = $this->config['fanDianMax'] - $this->user['fanDian'];
						$proportion = 1 - $diff_fanDian / 100;
						$v['bonusProp'] = number_format($v['bonusProp'] * $proportion, 2, '.', '');
						$v['bonusPropBase'] = number_format($v['bonusPropBase'] * $proportion, 2, '.', '');
						$plays[$v['groupId']][$v['id']] = $v;
					}
		            $play_tpl = 'lucky28';
		            $args = array();
		            $args['type_id'] = $type_id;
					$args['group_id'] = $group_id;
					$args['type_type'] = $types[$type_id]['type'];
		            $args['types'] = $types;
		            $args['groups'] = $groups;
		            $args['plays'] = $plays;
		            $args['play_tpl'] = $play_tpl;
		            $args['all_plays'] = $this->get_plays(0,'all');
				break;
				default:
		            $group_id = 6;
		            $play_id = 37;
		            $types = core::lib('game')->get_types();
		            $sql_type = $types[$type_id]['type'];
		            $sql = "SELECT `id`,`groupName`,`enable` FROM `{$this->db_prefix}played_group` WHERE `enable`=1 AND `type`=$sql_type ORDER BY `sort`";
		            $data = $this->db->query($sql, 3);
		            if (!$data) core::error('当前彩种下暂未添加玩法');
		            $groups = array();
		            foreach ($data as $v) $groups[$v['id']] = $v;
		            if (!array_key_exists($group_id, $groups)) $group_id = $data[0]['id'];
		            $plays = $this->get_plays($group_id);
		            if (!$plays) core::error('当前彩种下暂未添加玩法');
		            if (!array_key_exists($play_id, $plays)) {
		                foreach ($plays as $play) {
		                    $play_id = $play['id'];
		                    $play_tpl = $play['playedTpl'];
		                    break;
		                }
		            } else {
		                $play_tpl = $plays[$play_id]['playedTpl'];
		            }
		            $play_info = $this->get_play_info($play_id);
		            //$bets_recent = $this->get_recent_bets($type_id);
		            $args = array();
		            $args['type_id'] = $type_id;
		            $args['type_type'] = $types[$type_id]['type'];
		            $args['group_id'] = $group_id;
		            $args['play_id'] = $play_id;
		            $args['types'] = $types;
		            $args['groups'] = $groups;
		            $args['plays'] = $plays;
		            $args['play_info'] = $play_info;
		            $args['play_tpl'] = $play_tpl;
		            $args['all_plays'] = $this->get_plays(0,'all');
					//$args['beiShu-mode'] = $$this->modes;
		            //$args['bets_recent'] = $bets_recent;
		        break;
		     }
            $this->display('game/index', $args);
    }

    public function zhuihao()
    {
        $this->check_post();
        $types = core::lib('game')->get_types();
        if (
            !array_key_exists('type', $_GET) || !array_key_exists($_GET['type'], $types) ||
            !array_key_exists('num', $_GET) || $_GET['num']>50 || $_GET['num']<1 ||
            !array_key_exists('beiShu', $_GET) || !core::lib('validate')->number($_GET['beiShu']) ||
            !array_key_exists('mode', $_GET) || !array_key_exists($_GET['mode'] = number_format($_GET['mode'], 3, '.', ''), $this->modes) ||
            !array_key_exists('amount', $_GET) || !core::lib('validate')->number_float($_GET['amount'], 3)
        ) core::error('403');
        $type = intval($_GET['type']);
        $num = intval($_GET['num']);
        $beiShu = intval($_GET['beiShu']);
        $mode = floatval($_GET['mode']);
        $amount = floatval($_GET['amount']);
        $bonusProp = floatval($_GET['bonusProp']);
        $betZjAmount = floatval($_GET['betZjAmount']);
		$html='';
		$html = '<div id="submit-bets" class="zhuihao_box">';
        $html .= '<input type="hidden" id="zhuihao_amount" value="' . $amount . '">';
        $html .= '<input type="hidden" id="zhuihao_beiShu" value="' . $beiShu . '">';
        //$html .= '<div class="submit-bets-head">';
		//$html .= '<div class="track-setup-2"><input type="checkbox" id="zhuiHao_mode" style="vertical-align:-2px" checked>中奖后停止追号</div>';
		$html .= '<div><span class="btn btn-yellow">中奖上限：<span class="maxZj">'.$betZjAmount.'</span></span></div>';
        $html .= '
                   <div class="track-setup-1">追号<input type="tel" value="'.$num.'" id="beiTou-tcycle" class="form-control2">期数</div>
                   <div class="track-setup-1">起始 <input type="tel" value="1" id="beiTou-start" class="form-control">倍数</div>
                   <div class="track-setup-2">每隔<input type="tel" value="1" id="beiTou-cycle" class="form-control">期</div>
                   <div class="track-setup-2">倍数 <input type="tel" value="1" id="beiTou-beiShu" class="form-control"></div>
		'
		;
		$html .= '<div class="bets-body">';
        $html .= '<table cellpadding="0" cellspacing="0">';
        $html .= '<tr class="head">';
        $html .= '<td class="choose_all" style="cursor:pointer">全选</td>';
        $html .= '<td>期号</td>';
        $html .= '<td width="80">倍数</td>';
        //$html .= '<td width="50">模式</td>';
        $html .= '<td width="80">金额</td>';
        $html .= '<td width="80">奖金</td>';
        //$html .= '<td width="50"></td>';
        //$html .= '<td>开奖时间</td>';
        $html .= '</tr>';
        $nos = core::lib('game')->get_game_next_nos($type, $num);
        foreach ($nos as $no) {
            $html .= '<tr>';
            $html .= '<td><input type="checkbox" data-actionno="' . $no['actionNo'] . '" data-actiontime="' . $no['actionTime'] . '"></td>';
            $html .= '<td>' . $no['actionNo'] . '</td>';
            $html .= '<td><input type="text" value="' . $beiShu . '"  maxlength="3" class="beiShu readonly" readonly="readonly"></td>';
            //$html .= '<td class="beiShu-mode">' . $mode . '</td>';
            $html .= '<td class="beiShu-amount">' .  number_format($amount, 2, '.', '') . '</td>';
            $html .= '<td class="bonusProp-amount">' . number_format($bonusProp * $beiShu * $mode / 2, 2, '.', '') . '</td>';
			//$html .= '<td class="prompt"></td>';
			//$html .= '<td>' . $no['actionTime'] . '</td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        $html .= '</div>';
        $html .= '</div>';
		$html .= '<input type="hidden" id="beiShu-mode" name="beiShu-mode" value="'.$mode.'">';
        $html .= '<script>$(function ()
        {
        	$(".bets-body td.choose_all").click();

		}
		);
        </script>';
		echo $html;
    }
    public function submit()
    {
        $this->check_post();
        if (
            !array_key_exists('code', $_POST) ||
            !array_key_exists('para', $_POST) ||
            !is_array($_POST['code']) ||
            !is_array($_POST['para'])
        ) core::__403();

		$codes = array();
		for($i=0;$i<count($_POST['code']);$i++) {
			foreach (array('fanDian','bonusProp','mode','beiShu','orderId','actionData','actionNum','playedGroup','playedId','type','weiShu','playedName') as $key){
				if(isset($_POST['code'][$i][$key])) {
					$codes[$i][$key] = $_POST['code'][$i][$key];
				}
			}
		}

		$para = array();
		foreach (array('zhuiHao','zhuiHaoMode','type','actionNo','kjTime') as $key){
			if(isset($_POST['para'][$key])) {
				$para[$key] = $_POST['para'][$key];
			}
		}
        $para['type'] = intval($para['type']);
		$zhuihao = false;
        array_key_exists('zhuiHao', $_POST) && ($zhuihao = $_POST['zhuiHao']);

        $amount = 0;
        $mincoin = 0;

		if ($this->config['switchBuy'] == 0) core::error('本平台已经停止购买');
        if ($this->config['switchDLBuy'] == 0 && $this->user['type']) core::error('代理不能下注');
        if ($this->config['switchZDLBuy'] == 0 && $this->user['parents'] == $this->user['uid']) core::error('总代理不能下注');


        if (count($codes) == 0) core::error('请先选择号码再提交投注');
        if (!array_key_exists('kjTime', $para)) core::__403();
        if (!array_key_exists('type', $para) || !core::lib('validate')->number($para['type'])) core::__403();
        if (!array_key_exists('actionNo', $para) || !preg_match('/^[0-9_\-]+$/', $para['actionNo'])) core::__403();

        /* 標準 actionNo 時間檢查 */
        $ftime = core::lib('game')->get_type_ftime($para['type']);
        $actionTimeTS = core::lib('game')->get_game_current_time($para['type']);
        $actionNo = core::lib('game')->get_game_no($para['type']);
        if ($actionNo['actionNo'] != $para['actionNo'] ||
            ($actionTimeTS - $ftime) < $this->time
        ) core::error('投注失败，您投注的第<span class="btn btn-red">' . $para['actionNo'] . '</span>期已过购买时间');
        /* end:標準 actionNo 時間檢查 */

		$mosi = array();
		if ($this->config['yuanmosi'] == 1) array_unshift($mosi, '2.000');
		if ($this->config['jiaomosi'] == 1) array_unshift($mosi, '0.200');
		if ($this->config['fenmosi'] == 1) array_unshift($mosi, '0.020');
		if ($this->config['limosi'] == 1) array_unshift($mosi, '0.002');
		//玩法
        $plays = $this->get_plays();
        foreach ($codes as $key => $code)
        {

			if (!array_key_exists('actionData', $code) || !is_string($code['actionData'])) core::__403();
            if (preg_match('/^deflate\-(\d+)\-([a-zA-Z0-9\+\/\=]+)$/is', trim($code['actionData']), $match)) {
                $actionData = base64_decode($match[2]);
                if (!$actionData) core::error('请求数据错误，请重新提交');
                $actionData = gzinflate($actionData);
                if (!$actionData || strlen($actionData) !== intval($match[1])) core::error('请求数据错误，请重新提交');
                $code['actionData'] = $actionData;
                $codes[$key]['actionData'] = $actionData;
            }
            if (preg_match('/[a-z]/i', $code['actionData'])) core::error('请求数据错误，请重新提交');

			//檢查膽拖格式
			$match = array();
			preg_match('/^\(([\d ]+)\)([\d ]+)$/', $code['actionData'], $match);
			if($match) {
				$arr = explode(' ', $match[1]);
				for($i=0;$i<count($arr);$i++) {
					if($code['type'] == 25) {
						if(strlen($arr[$i]) != 1 || strpos($match[2],$arr[$i]) !== false) core::error('请求数据错误，请重新提交');
					}
					else {
						if(strlen($arr[$i]) != 2 || strpos($match[2],$arr[$i]) !== false) core::error('请求数据错误，请重新提交');
					}
				}
			}

			if (!array_key_exists('playedId', $code) || !core::lib('validate')->number($code['playedId'])) core::__403();
            $code['playedId'] = intval($code['playedId']);
            if ($code['playedId'] === 24) {
                if (!array_key_exists('playedName', $code) || !is_string($code['playedName'])) core::__403();
                if ($code['playedName'] === '任选三组六') {
                    $codes[$key]['playedId'] = 23;
                    $code['playedId'] = 23;
                } else if ($code['playedName'] === '任选三组三') {
                    $codes[$key]['playedId'] = 22;
                    $code['playedId'] = 22;
                }
            }
            if (!array_key_exists('type', $code) || !core::lib('validate')->number($code['type'])) core::__403();

			$codes[$key]['type'] = intval($code['type']);
			$codes[$key]['playedId'] = intval($code['playedId']);
			$codes[$key]['playedGroup'] = intval($code['playedGroup']);
			$codes[$key]['beiShu'] = ceil($code['beiShu']);

			//檢查玩法組合是否正確
			if(!$this->checkGamePlayed($codes[$key]['type'],$codes[$key]['playedGroup'],$codes[$key]['playedId'])) core::error('您提交的游戏玩法不存在或已被禁用');

            /* 追號 actionNo 時間檢查 */
            $code['type'] = intval($code['type']);
            $ftime2 = core::lib('game')->get_type_ftime($code['type']);
            $actionTimeTS2 = core::lib('game')->get_game_current_time($code['type']);
            $actionNo2 = core::lib('game')->get_game_no($code['type']);
            if ($actionNo2['actionNo'] != $para['actionNo'] ||
                ($actionTimeTS2 - $ftime2) < $this->time
            ) core::error('投注失败，您投注的第<span class="btn btn-red">' . $para['actionNo'] . '</span>期已过购买时间');
            /* end:追號 actionNo 時間檢查 */

            /* check playedid */
            if (!array_key_exists($code['playedId'], $plays)) core::__403();
            $played = $plays[$code['playedId']];
			$played['maxBalls'] = (int)$played['maxBalls'];
			//單式不算球數
			if (strpos($code['actionData'],'|') === false && $played['maxBalls'] > 0) {
				$actonDataArray = explode(',',$code['actionData']);

				foreach ($actonDataArray as $rows){
					if($played['type'] == 1 || $played['type'] == 3) {
						$balls = str_split($rows);
					}
					else {
						$balls = explode(' ',$rows);
					}

					if (count($balls) > $played['maxBalls']) core::error('投注号码超过<span class="btn btn-red">'.$played['maxBalls'].'</span>个上限');
				}
			}



            $diff_fanDian = $this->config['fanDianMax'] - $this->user['fanDian'];
			//返點強制設0
			$code['fanDian'] = 0;

            $proportion = 1 - $diff_fanDian / 100;
            $bonusProp = number_format($played['bonusProp'] * $proportion, 2, '.', '');
            $bonusPropBase = number_format($played['bonusPropBase'] * $proportion, 2, '.', '');
            if (!array_key_exists('bonusProp', $code) || !core::lib('validate')->number_float($code['bonusProp'], 2)) core::__403();
            if ($code['bonusProp'] > $bonusProp) core::error('提交奖金大于最大奖金，请重新投注');
            if ($code['bonusProp'] < $bonusPropBase) core::error('提交奖金小于最小奖金，请重新投注');
            if (!array_key_exists('fanDian', $code) || !core::lib('validate')->number_float($code['fanDian'], 1)) core::__403();


			//倍數檢查
            if (!array_key_exists('beiShu', $code) || !core::lib('validate')->number($code['beiShu'])) core::__403();

			//位數檢查
            if (!array_key_exists('weiShu', $code) || (!core::lib('validate')->number($code['weiShu']) && $code['weiShu'] != 0)) core::__403();
			if (in_array($code['playedId'], ['9'])) {
                if (!in_array($code['weiShu'], ['15', '23', '27', '29', '30', '31'])) core::__403();
            }
            if (in_array($code['playedId'], ['15', '22', '23', '24', '41', '196', '201', '202', '219'])) {
                if (!in_array($code['weiShu'],['7', '11', '13', '14', '15', '19', '21', '22', '23', '25', '26', '27', '28', '29', '30', '31'])) core::__403();
            }
            if (in_array($code['playedId'], ['30', '35', '36', '213', '214', '208'])) {
                if (!in_array($code['weiShu'], ['3', '5', '6', '7', '9', '10', '11', '12', '13', '14', '15', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'])) core::__403();
            }

			//模式檢查
            if (!array_key_exists('mode', $code) || !core::lib('validate')->number_float($code['mode'], 3)) core::__403();
            if (!in_array($code['mode'], $mosi)) core::error('投注模式出错，请重新投注');

			//柱數檢查
            if (!array_key_exists('actionNum', $code) || !core::lib('validate')->number($code['actionNum'])) core::__403();
            if ($betCountFun = $played['betCountFun']) {
            	switch(true)
				{
					case $played['type']==11:
						if($zhuihao)core::error('此彩种禁止追号');
						if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'],$played['example'])) core::error('下单失败，您投注号码不符合投注规则，请重新投注');
						break;
					default:
						if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'])) core::error('下单失败，您投注号码不符合投注规则，请重新投注');
						break;
				}
            }

			if ($code['actionNum'] > $played['maxcount']) core::error('[' . $played['name'] . ']投注上限为<span class="btn btn-red">' . $played['maxcount'] . '</span>注，请重新投注');

			//中獎金額檢查
			if (($code['beiShu'] * $bonusProp * $code['mode'] / 2) > $this->config['betMaxZjAmount'] && $this->config['betMaxZjAmount']) {
				core::error('第<span class="btn btn-white">'. $actionNo['actionNo'] . '</span>期，中奖金额超过<span class="btn btn-red">' . $this->config['betMaxZjAmount'] . '</span>元上限，请重新投注!');
			}
        }

		$code = current($codes);
		$actionIP=$this->ip(true);
		require(SYSTEM.'/lib/IP.class.php');
		$betArea=IP::find(long2ip($actionIP));
        $para = array_merge($para, array(
            'actionTime' => $this->time,
            'actionNo' => $actionNo['actionNo'],
            'kjTime' => $actionTimeTS,
            'actionIP' => $actionIP,
            'uid' => $this->user['uid'],
            'username' => $this->user['username'],
            'betArea' => $betArea[1],
            'betCity' => $betArea[2],
            'forTest' => $this->user['forTest'],
            'serializeId' => md5($this->user['uid'].$this->time.uniqid())
        ));
        $code = array_merge($code, $para);

        $limit_per_actionNO = $this->user['limitPreActionNo'];//小於等於0==>無限制

        if ($zhuihao) { /* 追号 */
            $liqType = 102;
            $codes = array();
            $info = '追号投注';
            if (isset($para['actionNo'])) unset($para['actionNo']);
            if (isset($para['kjTime'])) unset($para['kjTime']);
            $zhuihao = explode(';', $zhuihao);

			$allTimes = core::lib('game')->get_game_type_all_time($code['type']);
			$target = array_search($actionNo['actionTime'], array_column($allTimes, 'actionTime'));
			if($target === false) core::error('开奖时间异常');
			$zhuihaoStartNumber = $actionNo['actionNo'] - $allTimes[$target]['actionNo'];

			foreach ($zhuihao as $var) {
                list($code['actionNo'], $code['beiShu'], $code['kjTime']) = explode('|', $var);
                $code['type'] = intval($code['type']);
                $code['kjTime'] = strtotime($code['kjTime']);
                $code['beiShu'] = abs(ceil($code['beiShu']));

				$checkActionNo = $code['actionNo'] - $zhuihaoStartNumber;
				$tempIndex = array_search($checkActionNo, array_column($allTimes, 'actionNo'));
				if($tempIndex === false) core::error('追号期数异常');
				if(strtotime(date('Y-m-d ').$allTimes[$tempIndex]['actionTime']) !==  $code['kjTime']) {
					core::error('追号时间异常');
				}


                /* 追號 actionNo 檢查 */
                if (strnatcmp($actionNo['actionNo'], $code['actionNo']) > 0) {
                    core::error('投注失败，您追投的第<span class="btn btn-red">' . $code['actionNo'] . '</span>期不允许购买');
                }

                if (strtotime($actionNo['actionTimeFull']) - $ftime < $this->time) {
                    core::error('投注失败，您追投的第<span class="btn btn-red">' . $code['actionNo'] . '</span>期已过购买时间');
                }
                /* end:追號 actionNo 檢查 */

                if ($limit_per_actionNO > 0) {//小於等於0==>無限制
                    $history_amount = $this->db->query("SELECT sum(actionNum*mode*beiShu) as sumary FROM `{$this->db_prefix}bets_repl` WHERE `uid`={$this->user['uid']} AND `actionNo`=" . $code['actionNo'] . " AND `type`=" . $para['type'] . " AND `isDelete`=0", 2);
                    $history_amount = $history_amount['sumary'];
                    if ($history_amount == null) $history_amount = 0;
                    $total_amount = $code['actionNum'] * $code['mode'] * $code['beiShu'] + $history_amount;
                    if ($total_amount > $limit_per_actionNO) core::error('超过当期投注金额上限' . $limit_per_actionNO . '元');
                }
				if (($code['beiShu'] * $code['bonusProp'] * $code['mode'] / 2) > $this->config['betMaxZjAmount'] && $this->config['betMaxZjAmount']) {
					core::error('第<span class="btn btn-white">'. $code['actionNo'] . '</span>期，中奖金额超过<span class="btn btn-red">' . $this->config['betMaxZjAmount'] . '</span>元上限，请重新投注!');
				}

                $amount += abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
                $codes[] = $code;
            }
        } else { /* 一般下注 */
            $liqType = 101;
            $info = '投注';
            if ($actionNo['actionNo'] != $code['actionNo']) {
                core::error('投注失败，您投注的第<span class="btn btn-red">' . $actionNo['actionNo'] . '</span>期已经过购买时间');
            }

            foreach ($codes as $i => $code) {
                $codes[$i] = array_merge($code, $para);
                $this_amount = abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
                if ($this_amount < 0.01) core::error('单笔投注金额不得小于0.01元');
                $amount += $this_amount;
            }

            if ($limit_per_actionNO > 0) {//小於等於0==>無限制
                $history_amount = $this->db->query("SELECT sum(actionNum*mode*beiShu) as sumary FROM `{$this->db_prefix}bets_repl` WHERE `uid`={$this->user['uid']} AND `actionNo`=" . $actionNo['actionNo'] . " AND `type`=" . $para['type'] . " AND `isDelete`=0", 2);
                $history_amount = $history_amount['sumary'];
                if ($history_amount == null) $history_amount = 0;
                $total_amount = $amount + $history_amount;
                if ($total_amount > $limit_per_actionNO) core::error('超过当期投注金额上限' . $limit_per_actionNO . '元');
            }
        }

        if ($amount < $mincoin) core::error('您的投注金额小于最低消费金额<span class="btn btn-red">' . $mincoin . '</span>元，请重新投注');
        $uid = $this->user['uid'];
        $userAmount = $this->db->query("SELECT `coin` FROM `{$this->db_prefix}members` WHERE `uid`={$uid} LIMIT 1", 2);
        $userAmount = $userAmount['coin'];
        if ($userAmount < $amount) core::error('您的可用资金不足，是否充值？');
        $this->db->transaction('begin');

        try {
            /* 批次下注 (已验证) */
            //$code['actionData']
			$zikiArray=array(
				0=>array(0,0,0,0,0,0,0,0,0,0),
				1=>array(0,0,0,0,0,0,0,0,0,0),
				2=>array(0,0,0,0,0,0,0,0,0,0),
				3=>array(0,0,0,0,0,0,0,0,0,0),
				4=>array(0,0,0,0,0,0,0,0,0,0)
			);
			$zikipk10Array=array(
				0=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				1=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				2=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				3=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				4=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				5=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				6=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				7=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				8=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				9=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0)
			);
			$this->config['zikairule3'];
			$zikiZhuihao=true;
			$zikaiBet=0;
			$zikaiZj=0;
			$zikai_Zc_Bet=0;
			$zikai_Zc_Zj=0;
			$zikai_type=array(5,71,72,73,74,77,76,79,80,81,82,83,84,85,86,87,88,89);
			$zikaipk10_type=array(75);
			$zikiSource=$zikiArray;
			$sqlZhuihao=array();
			$sqlZhuihao_SP=array();
			$keyZcID=array(268,269,270,271,272); //組選
            foreach ($codes as $code)
            {
				//返點強制設0
				$code['fanDian'] = 0;
                unset($code['playedName']);
				$code['wjorderId'] = $code['type'] . $code['playedId'] . $this->randomkeys(10 - strlen($code['type'] . $code['playedId']));
                $code['actionNum'] = abs($code['actionNum']);
                $code['mode'] = abs($code['mode']);
                $code['beiShu'] = abs(ceil($code['beiShu']));
                $amount = abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
				$zj = abs(($code['mode']/2) * $code['beiShu'] * $code['bonusProp']);
				$code['md5'] = md5($code['actionData'].$code['wjorderId'].$code['serializeId'].$amount);
                $id = $this->db->insert($this->db_prefix . 'bets', $code);
                $this->set_coin(array(
                    'uid' => $this->user['uid'],
                    'type' => $code['type'],
                    'liqType' => $liqType,
                    'info' => $info,
                    'extfield0' => $id,
                    'extfield1' => $para['serializeId'],
                    'coin' => -$amount,
                ));
				if(in_array($code['type'],$zikai_type) && $code['playedGroup']!=87 && $this->user['forTest']==0)
				{
					$this->build_ziki_number($zikiArray,$code);
					switch(true)
					{
						case $zhuihao:							
							//$zikiZhuihao=false;
							$zikaiBet=abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
							$zikaiZj=$zj;
							switch(true)
							{
								case in_array($code['playedId'],$keyZcID):
									$zikai_Zc_Bet=abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
									$zikai_Zc_Zj=abs(($code['mode']/2) * $code['beiShu'] * $code['bonusProp']);
									$sqlZhuihao_SP[]="(".$code['type'].",".$code['actionNo'].",".$zikai_Zc_Bet.",".$zikai_Zc_Zj.",1,'','',0,0)";
									break;
							}
							$bsql=array();
							foreach($zikiArray as $k=>$v)
							{
								foreach($v as $k1=>$v1)
								{
									$bsql[]=$v1;
								}
							}
							$sqlZhuihao[]="(".$code['type'].",".$this->user['uid'].",".$code['actionNo'].",'".$code['serializeId']."',".$zikaiBet.",".$zikaiZj.",'".$code['md5']."',".implode(",", $bsql).")";
							$zikiArray=$zikiSource;
							break;
						default:
							$zikaiBet=abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
							$zikaiZj=$zj;
							$bsql=array();
							foreach($zikiArray as $k=>$v)
							{
								foreach($v as $k1=>$v1)
								{
									$bsql[]=$v1;
								}
							}							
							$sqlZhuihao[]="(".$code['type'].",".$this->user['uid'].",".$code['actionNo'].",".$zikaiBet.",".$zikaiZj.",'".$code['md5']."',".implode(",", $bsql).")";
							$zikiArray=$zikiSource;
							switch(true)
							{
								case in_array($code['playedId'],$keyZcID):
									$zikai_Zc_Bet+=abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
									$zikai_Zc_Zj+=abs(($code['mode']/2) * $code['beiShu'] * $code['bonusProp']);
									break;
							}							
							break;
					}
					if($zj >= $this->config['zikairule3'])
					{
						$sqlZhuihao_SP[]="(".$code['type'].",".$code['actionNo'].",".$amount.",".$zj.",2,'".$code['actionData']."','".$code['md5']."',".$code['playedId'].",".$code['weiShu'].")";
						
					}
				}
				if(in_array($code['type'],$zikaipk10_type) && $code['playedGroup']!=68 && $code['playedGroup']!=70 && $zikiZhuihao && $this->user['forTest']==0)
				{
					if($zhuihao)/* 如果是追号只统计一次 */ 
					{
						$zikiZhuihao=false;
					}
					$this->build_zikipk10_number($zikipk10Array,$code);
				}
            }
			if(in_array($code['type'],$zikai_type) && $code['playedGroup']!=87 && $this->user['forTest']==0)
			{
				if($zhuihao)
				{
					$sql="insert {$this->db_prefix}ziki_zhuihao";
					$asql=array();
					//$bsql=array();
					foreach($zikiArray as $k=>$v)
					{
						foreach($v as $k1=>$v1)
						{
							$asql[]="`".$k.$k1."`";
							//$bsql[]=$v1;
						}
					}
					//$sql=$sql."(`type`,`uid`,`actionNo`,`serializeId`,bet,zj,".implode(",", $asql).")values(".$code['type'].",".$this->user['uid'].",".$code['actionNo'].",'".$code['serializeId']."',".$zikaiBet.",".$zikaiZj.",".implode(",", $bsql).")";
					$sql=$sql."(`type`,`uid`,`actionNo`,`serializeId`,bet,zj,md5,".implode(",", $asql).")values".implode(",", $sqlZhuihao);
					$this->db->query($sql, 0);
				}else
				{				
					$sql="insert {$this->db_prefix}ziki_number";
					$asql=array();
					//$bsql=array();
					foreach($zikiArray as $k=>$v)
					{
						foreach($v as $k1=>$v1)
						{
							$asql[]="`".$k.$k1."`";
							//$bsql[]=$v1;
						}
					}
					//$sql=$sql."(`type`,`actionNo`,bet,zj,md5,".implode(",", $asql).")values(".$code['type'].",".$code['actionNo'].",".$zikaiBet.",".$zikaiZj.",'".$code['md5']."',".implode(",", $bsql).")";
					$sql=$sql."(`type`,`uid`,`actionNo`,bet,zj,md5,".implode(",", $asql).")values".implode(",", $sqlZhuihao);
					$this->db->query($sql, 0);
					if($zikai_Zc_Zj > 0)
					{
						$sql="insert {$this->db_prefix}ziki_sprule(type,actionNo,bet,zj,rule_type,actionData,md5,playedId,weiShu)value(".$code['type'].",".$code['actionNo'].",".$zikai_Zc_Bet.",".$zikai_Zc_Zj.",1,'','',0,0)";
						$this->db->query($sql, 0);
					}
				}
				if(count($sqlZhuihao_SP) > 0)
				{
					$sql="insert {$this->db_prefix}ziki_sprule(type,actionNo,bet,zj,rule_type,actionData,md5,playedId,weiShu)values".implode(",", $sqlZhuihao_SP);
					$this->db->query($sql, 0);
				}
			}
			if(in_array($code['type'],$zikaipk10_type) && $code['playedGroup']!=68 && $code['playedGroup']!=70 && $this->user['forTest']==0)
			{
				if($zhuihao)
				{
					$sql="insert {$this->db_prefix}zikipk10_zhuihao";
					$asql=array();
					$bsql=array();
					foreach($zikipk10Array as $k=>$v)
					{
						foreach($v as $k1=>$v1)
						{
							$asql[]="`".$k.$k1."`";
							$bsql[]=$v1;
						}
					}
					$sql=$sql."(`type`,`uid`,`actionNo`,`serializeId`,".implode(",", $asql).")values(".$code['type'].",".$this->user['uid'].",".$code['actionNo'].",'".$code['serializeId']."',".implode(",", $bsql).")";
				}else
				{				
					$sql="insert {$this->db_prefix}zikipk10_number";
					$asql=array();
					$bsql=array();
					foreach($zikipk10Array as $k=>$v)
					{
						foreach($v as $k1=>$v1)
						{
							$asql[]="`".$k.$k1."`";
							$bsql[]=$v1;
						}
					}
					$sql=$sql."(`type`,`actionNo`,".implode(",", $asql).")values(".$code['type'].",".$code['actionNo'].",".implode(",", $bsql).")";
				}
				$this->db->query($sql, 0);
			}
            $this->db->transaction('commit');
            echo '投注成功';
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            core::error($e->getMessage());
        }
    }

    private function build_ziki_number(&$zikiArray,$code)
    {
    	$key8q2dxdsID=array(42); //大小單雙前2
    	$key8h2dxdsID=array(43); //大小單雙前2
    	$key8q3dxdsID=array(265); //大小單雙前2
    	$key8h3dxdsID=array(266); //大小單雙前2
    	
    	$keyZhID=array(2,3,4,5,6,7,8,9,10,11,12,13,14,15,25,26,27,28,29,30); //直選
    	
    	$key5ID=array(258,268,269,270,271,272); //5星组3组6
		$key4zqID=array(4,5);
		$key4zhID=array(6,7);
		
		$key3zqID=array(10,11);
		$key3zcID=array(287,288);
		$key3zhID=array(12,13);
		$key3z36ID=array(22,23); //任选任3组3组6
		$key3zr36ID=array(24); //任选任三混合组选,对写法不放心关闭玩法
		
		$key2zqID=array(25,26);
		$key2zhID=array(27,28);
		$key2zrID=array(35,36);
		
		$key4hID=array(273,275,276,277);
		$key3qID=array(16,17,18);
		$key3cID=array(289,290,307);
		$key3hID=array(19,20,21);
		
		$keybdwz3ID=array(39,142); //不定位前三1码2码
		$keybdwc3ID=array(40);
		$keybdwh3ID=array(38,143); //不定位后三1码2码
		$keybdw5ID=array(261,262); //不定位5星3码2码
		$keybdw4ID=array(263,264); //不定位后4星3码2码
		$rule=0;
		$a1=0;
		$a2=0;
		$weiShuKey=array();
		$value=abs(($code['mode']/2) * $code['beiShu']*$code['bonusProp']);
		if($code['weiShu']>0)
		{
            $w = array(
                16 => '万',
                8 => '千',
                4 => '百',
                2 => '十',
                1 => '个'
            );
            foreach ($w as $p => $v) {
                if ($code['weiShu'] & $p)
				{
					switch($p)
					{
						case 16:
							$weiShuKey[]=0;
							break;
						case 8:
							$weiShuKey[]=1;
							break;
						case 4:
							$weiShuKey[]=2;
							break;
						case 2:
							$weiShuKey[]=3;
							break;
						case 1:
							$weiShuKey[]=4;
							break;
					}
				}
            }
		}
    	switch($code['playedId'])
		{
			case in_array($code['playedId'],$keyZhID):
				$rule=6;
				break;
			case in_array($code['playedId'],$keybdw4ID):
				$rule=1;
				$a1=1;
				$a2=4;
				break;
			case in_array($code['playedId'],$keybdw5ID):
				$rule=1;
				$a1=0;
				$a2=4;
				break;
			case in_array($code['playedId'],$keybdwh3ID):
				$rule=1;
				$a1=2;
				$a2=4;
				break;
			case in_array($code['playedId'],$keybdwc3ID):
				$rule=1;
				$a1=1;
				$a2=3;
				break;
			case in_array($code['playedId'],$keybdwz3ID):
				$rule=1;
				$a1=0;
				$a2=2;
				break;
			case in_array($code['playedId'],$key3zr36ID):
				$rule=4;
				break;	
			case in_array($code['playedId'],$key3z36ID):
				$rule=3;
				break;	
			case in_array($code['playedId'],$key2zrID):
				$rule=3;
				break;	
			case in_array($code['playedId'],$key3zqID):
				$rule=2;
				$a1=0;
				break;	
			case in_array($code['playedId'],$key3zcID):
				$rule=2;
				$a1=1;
				break;	
			case in_array($code['playedId'],$key3zhID):
				$rule=2;
				$a1=2;
				break;
			case in_array($code['playedId'],$key2zhID):
				$rule=2;
				$a1=3;
				break;
			case in_array($code['playedId'],$key2zqID):
				$rule=2;
				$a1=0;
				break;
			case in_array($code['playedId'],$key4zhID):
				$rule=2;
				$a1=1;
				break;
			case in_array($code['playedId'],$key4zqID):
				$rule=2;
				$a1=0;
				break;
			case in_array($code['playedId'],$key3hID):
				$rule=1;
				$a1=2;
				$a2=4;
				break;
			case in_array($code['playedId'],$key3cID):
				$rule=1;
				$a1=1;
				$a2=3;
				break;
			case in_array($code['playedId'],$key3qID):
				$rule=1;
				$a1=0;
				$a2=2;
				break;
			case in_array($code['playedId'],$key5ID):
				$rule=1;
				$a1=0;
				$a2=4;
				break;
			case in_array($code['playedId'],$key4hID):
				$rule=1;
				$a1=1;
				$a2=4;
				break;		
			case in_array($code['playedId'],$key8q2dxdsID):
				$rule=5;
				$a1=0;
				break;		
			case in_array($code['playedId'],$key8h2dxdsID):
				$rule=5;
				$a1=3;
				break;		
			case in_array($code['playedId'],$key8q3dxdsID):
				$rule=5;
				$a1=0;
				break;		
			case in_array($code['playedId'],$key8h3dxdsID):
				$rule=5;
				$a1=2;
				break;		
		}
		switch(true)
		{
			case $rule==6:
		    	$s=explode("|", $code['actionData']);
				foreach($s as $sv)
				{
					$a=explode(",", $sv);
					//$kcheck1[array_rand($kcheck1,1)];
					$key=array_rand($a,1);
					while($a[$key]=='-')
					{
						$key=array_rand($a,1);
					}
					//foreach ($a as $k => $v) {
						//if($v!='-')
						//{
							$b=str_split($a[$key]);
							foreach($b as $k1=>$v1)
							{
								$zikiArray[$key][$v1]+=$value;
							}
						//}
					//}			
				}
				break;
			case $rule==0:
		    	$s=explode("|", $code['actionData']);
				foreach($s as $sv)
				{
					$a=explode(",", $sv);
					foreach ($a as $k => $v) {
						if($v!='-')
						{
							$b=str_split($v);
							foreach($b as $k1=>$v1)
							{
								$zikiArray[$k][$v1]+=$value;
							}
						}
					}			
				}
				break;
			case $rule==1:
				$s=str_replace(",","", $code['actionData']);
				$s=str_replace(" ","", $s);
				$b=str_split($s);
				for ($k=$a1;$k<=$a2;$k++) {
					foreach($b as $k1=>$v1)
					{
						$zikiArray[$k][$v1]+=$value;
					}
				}
				break;
			case $rule==2:
		    	$s=explode("|", $code['actionData']);
				foreach($s as $sv)
				{
					$a=explode(",", $sv);
					foreach ($a as $k => $v) {
						$b=str_split($v);
						foreach($b as $k1=>$v1)
						{
							$zikiArray[$k+$a1][$v1]+=$value;
						}
					}			
				}				
				break;
			case $rule==3:
		    	$s=explode("|", $code['actionData']);
				foreach($s as $sv)
				{
		            foreach ($weiShuKey as $p => $v) {
						$b=str_split($sv);
						foreach($b as $k1=>$v1)
						{
							$zikiArray[$v][$v1]+=$value;
						}
		            }
				}				
				break;
			case $rule==4:
		    	$s=explode(",", $code['actionData']);
				foreach($s as $sv)
				{
		            foreach ($weiShuKey as $p => $v) {
						$b=str_split($sv);
						foreach($b as $k1=>$v1)
						{
							$zikiArray[$v][$v1]+=$value;
						}
		            }
				}				
				break;
			case $rule==5:
		    	$s=explode(",", $code['actionData']);
				foreach($s as $k=>$sv)
				{
					$b=array();
					switch($sv)
					{
						case "单":
							$b=array(1,3,5,7,9);
						break;
						case "小":
							$b=array(0,1,2,3,4);
						break;
						case "大":
							$b=array(5,6,7,8,9);
						break;
						case "双":
							$b=array(0,2,4,6,8);
						break;

					}
					foreach($b as $k1=>$v1)
					{
						$zikiArray[$k+$a1][$v1]+=$value;
					}
				}				
				break;
		}
    }

    private function build_zikipk10_number(&$zikipk10Array,$code)
    {
    	$key93ID=array(93,94,95,96); //猜冠军,猜冠亚军,猜前三名,定位胆选
    	$key225ID=array(225,226,227,228,229,230,231,232,233,234); //两面
    	$key244ID=array(244); //冠亚季选一
    	$key247ID=array(247); //冠亚组合
		$rule=0;
		$a1=0;
		$a2=0;
		$weiShuKey=array();
		//$value=abs($code['mode'] * $code['beiShu']);
		$value=abs(($code['mode']/2) * $code['beiShu']*$code['bonusProp']);
    	switch($code['playedId'])
		{
			case in_array($code['playedId'],$key93ID):
				$rule=0;
				break;	
			case in_array($code['playedId'],$key225ID):
				$rule=1;
				break;	
			case in_array($code['playedId'],$key244ID):
				$rule=2;
				break;	
			case in_array($code['playedId'],$key247ID):
				$rule=3;
				break;
		}
		switch(true)
		{
			case $rule==0:
		    	$s=explode(",", $code['actionData']);
				foreach($s as $sk=>$sv)
				{
					if($sv=="-")continue;
					$a=explode(" ", $sv);
					foreach ($a as $k => $v) {
						$zikipk10Array[$sk][$v]+=$value;
					}			
				}
				break;
			case $rule==1:
				$sk=0;
				switch($code['playedId'])
				{
					case 225;
						$sk=0;
					break;
					case 226;
						$sk=1;
					break;
					case 227;
						$sk=2;
					break;
					case 228;
						$sk=3;
					break;
					case 229;
						$sk=4;
					break;
					case 230;
						$sk=5;
					break;
					case 231;
						$sk=6;
					break;
					case 232;
						$sk=7;
					break;
					case 233;
						$sk=8;
					break;					
					case 234;
						$sk=9;
					break;
				}
				$a=preg_split('/(?<!^)(?!$)/u', $code['actionData']); 
				foreach ($a as $k => $v) {
					switch($v)
					{
						case '大';
							$number=array('06','07','08','09','10');
						break;
						case '小';
							$number=array('01','02','03','04','05');
						break;
						case '单';
							$number=array('01','03','05','07','09');
						break;
						case '双';
							$number=array('02','04','06','08','10');
						break;
					}
					foreach($number as $kk => $vv)
					{
						$zikipk10Array[$sk][$vv]+=$value;
					}
				}
				break;
			case $rule==2:
				$pos=array(0,1,2);
		    	$s=explode(" ", $code['actionData']);
				foreach($pos as $kk => $vv)
				{
					foreach($s as $sk=>$sv)
					{
						$zikipk10Array[$vv][$sv]+=$value;			
					}
				}
				break;
			case $rule==3:
				$pos=array(0,1);
		    	$s=explode("-", $code['actionData']);
				print_r($s);
				foreach($pos as $kk => $vv)
				{
					foreach($s as $sk=>$sv)
					{
						if(strlen($sv)<2)
						{
							$sv="0".$sv;
						}
						$zikipk10Array[$vv][$sv]+=$value;			
					}
				}
				break;
		}
    }


    private function randomkeys($length)
    {
        $key = '';
        $pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $pattern1 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pattern2 = '0123456789';
        for ($i = 0; $i < $length; $i++) $key .= $pattern{mt_rand(0, 35)};
        return $key;
    }

    public function bets()
    {
        $this->check_post();
        $type_id = $this->get_id();
        $bets_recent = $this->get_recent_bets($type_id);
        $this->display('game/bets_recent', array(
            'bets_recent' => $bets_recent,
            'types' => core::lib('game')->get_types(),
            'all_plays' => $this->get_plays(),
        ));
    }

    public function group()
    {
        $this->check_post();
        $type_id = $this->get_id('type_id');
        $group_id = $this->get_id('group_id');
        $plays = $this->get_plays($group_id);
        if (!$plays) core::__403();
        foreach ($plays as $play) {
            $play_id = $play['id'];
            $play_tpl = $play['playedTpl'];
            break;
        }
        $play_info = $this->get_play_info($play_id);
        $this->display('game/play_index_redata', array(
            'type_id' => $type_id,
            'group_id' => $group_id,
            'play_id' => $play_id,
            'plays' => $plays,
            'play_info' => $play_info,
            'play_tpl' => $play_tpl,
        ));
    }

    public function play()
    {
        $this->check_post();
        $type_id = $this->get_id('type_id');
        $play_id = $this->get_id('play_id');
		$group_id = $this->get_id('group_id');
		if($group_id<>87)
		{
			$plays=array();
	        $play_info = $this->get_play_info($play_id);
	        if (!$play_info) core::__403();
		}else
		{
			$play_info=array(
				'groupId'=>87,
				'playedTpl'=>'ssclhnn'
				);
	        $plays = $this->get_plays($group_id);
	        if (!$plays) core::__403();			
		}
        $this->display('game/play_data', array(
            'type_id' => $type_id,
            'group_id' => $play_info['groupId'],
            'play_id' => $play_id,
            'plays' => $plays,
            'play_info' => $play_info,
            'play_tpl' => $play_info['playedTpl'],
        ));
    }

    public function lottery()
    {
        $this->check_post();
        $type_id = $this->get_id();

        $last = core::lib('game')->get_game_last_no($type_id);
        $actionNo = $last['actionNo'];
        $sql = "SELECT `data` FROM `{$this->db_prefix}data` WHERE `type`={$type_id} AND `number`='{$actionNo}' LIMIT 1";
        $lottery = $this->db->query($sql, 2);
        $lottery = $lottery ? explode(',', $lottery['data']) : array();
        $current = core::lib('game')->get_game_no($type_id);
        $types = core::lib('game')->get_types();
        $kjdTime = $types[$type_id]['data_ftime'];

        $diffTime = strtotime($current['actionTimeFull']) - ($this->time) - ($kjdTime);
        $kjDiffTime = strtotime($last['actionTimeFull']) - ($this->time);
        //$sql = "SELECT `time`,`number`,`data` FROM `{$this->db_prefix}data` WHERE `type`={$type_id} ORDER BY `id` DESC LIMIT 20";
        //$history = $this->db->query($sql, 3);
        $this->display('game/lottery', array(
            'types' => $types,
            'type_id' => $type_id,
            'type_this' => $types[$type_id],
            'last' => $last,
            'current' => $current,
            'lottery' => $lottery,
            'kjdTime' => $kjdTime,
            'diffTime' => $diffTime,
            'kjDiffTime' => $kjDiffTime,
            //'history' => $history,
        ));
    }
	public function draw()
	{
		$lottery_array=array();
		$sql = "SELECT id,data_ftime,title,shortname,type FROM `{$this->db_prefix}type` WHERE `isDelete`=0 and enable=1 ORDER BY `sort` ASC";
		$data = $this->db->query($sql, 3);
		foreach ($data as $v)
		{
			$types[$v['id']]=$v;
			$type_id = $v['id'];
	        $last = core::lib('game')->get_game_last_no($type_id);
	        $actionNo = $last['actionNo'];
	        $sql = "SELECT `data` FROM `{$this->db_prefix}data` WHERE `type`={$type_id} AND `number`='{$actionNo}' LIMIT 1";
	        $lottery = $this->db->query($sql, 2);
	        $lottery = $lottery ? explode(',', $lottery['data']) : array();
	        $current = core::lib('game')->get_game_no($type_id);
			$kjdTime = $v['data_ftime'];
	        $diffTime = strtotime($current['actionTimeFull']) - ($this->time) - ($kjdTime);
	        $kjDiffTime = strtotime($last['actionTimeFull']) - ($this->time);
			$lottery_array[$type_id]=array(
	            'type_id' => $type_id,
	            'type_this' => $types[$type_id],
	            'last' => $last,
	            'lottery' => $lottery
        	);
		}
        $this->display('game/draw',array('data'=>$lottery_array));
	}
	public function draw_list()
	{
    	if (!array_key_exists('id', $_GET)) $_GET['id'] = 1;
        $type_id = $this->get_id();
		$sql = "SELECT title,type FROM `{$this->db_prefix}type` WHERE id=".$type_id;
		$data = $this->db->query($sql, 2);
        $sql = "SELECT time,number,`data` FROM `{$this->db_prefix}data` WHERE `type`={$type_id} order by time desc LIMIT 20";
        $lottery = $this->db->query($sql, 3);
        $this->display('game/draw_list',array('type_id'=>$type_id,'title'=>$data['title'],'lottery'=>$lottery,'type'=>$data['type']));
	}

	public function lobby()
	{
		$lottery_array=array();
		$sql = "SELECT id,data_ftime,title,shortname,type FROM `{$this->db_prefix}type` WHERE `isDelete`=0 and enable=1 ORDER BY `sort` ASC";
		$data = $this->db->query($sql, 3);
		foreach ($data as $v)
		{
			$types[$v['id']]=$v;
			$type_id = $v['id'];
	        $last = core::lib('game')->get_game_last_no($type_id);
	        $actionNo = $last['actionNo'];
	        $sql = "SELECT `data` FROM `{$this->db_prefix}data` WHERE `type`={$type_id} AND `number`='{$actionNo}' LIMIT 1";
	        $lottery = $this->db->query($sql, 2);
	        $lottery = $lottery ? explode(',', $lottery['data']) : array();
	        $current = core::lib('game')->get_game_no($type_id);
			$kjdTime = $v['data_ftime'];
	        $diffTime = strtotime($current['actionTimeFull']) - ($this->time) - ($kjdTime);
	        $kjDiffTime = strtotime($last['actionTimeFull']) - ($this->time);
			$lottery_array[$type_id]=array(
	            'type_id' => $type_id,
	            'type_this' => $types[$type_id],
	            'last' => $last,
	            'current' => $current,
	            'lottery' => $lottery,
	            'kjdTime' => $kjdTime,
	            'diffTime' => $diffTime,
	            'kjDiffTime' => $kjDiffTime,
        	);
		}
        $this->display('game/lobby',array('data'=>$lottery_array));
	}

    public function lobby_last()
    {
    	if (!array_key_exists('id', $_GET)) $_GET['id'] = 1;
    	$last_data=array();
        $type_id = $this->get_id();
        $lottery = $this->db->query("SELECT id,data_ftime FROM `{$this->db_prefix}type` WHERE id=".$type_id, 2);
        $last = core::lib('game')->get_game_last_no($type_id);
        if (!$last) core::error('查找最后开奖期号出错');
        $current = core::lib('game')->get_game_no($type_id);
		$kjdTime = $lottery['data_ftime'];
        $diffTime = strtotime($current['actionTimeFull']) - ($this->time) - ($kjdTime);
        $kjDiffTime = strtotime($last['actionTimeFull']) - ($this->time);
		$current_data=array(
	            'type_id' => $type_id,
	            'current' => $current,
	            'kjdTime' => $kjdTime,
	            'diffTime' => $diffTime,
	            'kjDiffTime' => 5,
        	);
        echo json_encode($current_data);
    }
	public function get_num()
	{
    	if (!array_key_exists('id', $_GET)) $_GET['id'] = 1;
        $type_id = $this->get_id();
        $last = core::lib('game')->get_game_last_no($type_id);
        $actionNo = $last['actionNo'];
        $sql = "SELECT `data` FROM `{$this->db_prefix}data` WHERE `type`={$type_id} AND `number`='{$actionNo}' LIMIT 1";
        $lottery = $this->db->query($sql, 2);
        $lottery = $lottery ? explode(',', $lottery['data']) : array();
        echo json_encode($lottery);
	}

    public function last()
    {
        $this->check_post();
        $type_id = $this->get_id();
        $last = core::lib('game')->get_game_last_no($type_id);
        if (!$last) core::error('查找最后开奖期号出错');
        $actionNo = $last['actionNo'];
        $lottery = $this->db->query("SELECT `data` FROM `{$this->db_prefix}data` WHERE `type`={$type_id} AND `number`='{$actionNo}' LIMIT 1", 2);

        echo $lottery ? 1 : 0;
    }

    public function current()
    {
        $this->check_post();
        $type_id = $this->get_id();
        $actionNo = core::lib('game')->get_game_no($type_id);
//        if ($type_id == 1 && $actionNo['actionTime'] == '00:00:00') {
//            $actionNo['actionTime'] = strtotime($actionNo['actionTime']) + 24 * 3600;
//        } else {
//            $actionNo['actionTime'] = strtotime($actionNo['actionTime']);
//        }
        echo json_encode($actionNo);
    }

	public function upload() {
		if(isset($_FILES["file"])){
			echo file_get_contents($_FILES['file']['tmp_name']);
			@unlink($_FILES['file']['tmp_name']);
		}
	}

}

?>