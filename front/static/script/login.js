$(function(){
	// 加载动画
	var login = $('#login');

	// 表单处理
	var username = $('#username');
	var password = $('#password');
	var remember = $('#remember');
	var submit = $('#submit');
	var error_value = $('.error');
	var requesting = false;
	var input_event = function(dom) {
		dom.bind('focus', function() {
			$(this).parent().addClass('focus');
		});
		dom.bind('blur', function() {
			$(this).parent().removeClass('focus');
		});
	};
	input_event(username);
	input_event(password);
	document.onkeydown = function(e) {
		var ev = document.all ? window.event : e;
		if (ev.keyCode == 13) submit.trigger('click');
	}
	submit.bind('click', function() {
		var t = $('.error').find('span');
		var error_tip = function(val) {
			error_value.text(val).removeClass('hidden').fadeIn(0);
		};
		error_tip('登录中...');
		$.ajax({
			url: window.location.href,
			type: 'POST',
			cache: false,
			dataType: 'html',
			data: {username: username.val(), password: password.val(), remember: remember.attr('checked') === 'checked' ? '1' : '0'},
			error: function() {
				error_tip('请求失败，请重试');
			},
			success: function(data, textStatus, xhr) {
				var error_message = xhr.getResponseHeader('X-Error-Message');
				if (error_message) {
					error_tip(decodeURIComponent(error_message));
				} else {
					login.fadeOut(function() {
						window.location.href = '/';
					});
				}
			}
		});
	});

	// 手機端下載
	$('#android_qrcode').live('mouseover',function() {
		var action = $(this).attr('action');
		$('#' + action).siblings('.play-eg').hide();
		$('#' + action).show();
	});
	$('#android_qrcode').live('mouseout',function() {
		$('.play-eg').hide();
	});

});

$(function () {
    sizeEnter();
    downMenu();

});

//登录第一页
function sizeEnter() {
    var oBody = $('body');
    var oHeader = $('#j-l-header');
    var oEnter = $('.enter .common-btn');
    var oArrow = $('.arrow-d');
    var oNav = $('.top-nav');
    var oBtn = $('.nav-btn');
    var oCon = $('.nav-con');
    var oWrap = $('.nav-wrap');
    var oClose = $('.nav-close');
    var emailBT = $('.email-btn');
    var oHeight = document.documentElement.clientHeight;
    // var h = document.body.clientHeight;
    $(window).resize(function () {
        oHeight = document.documentElement.clientHeight;
    });
    //点击向下的箭头
    oArrow.click(function () {
        $('html,body').animate({'scrollTop': oHeight}, 400);
    });
    //点击进入按钮
    oEnter.click(function () {
        $('.top-slide').addClass('top-slide-c');
        $('.top-wrap').addClass('top-wrap-c');
    });
    //点击左上角的按钮，让phone-con出来
    oBtn.click(function () {
        oCon.fadeIn(200);
        oWrap.addClass('nav-wrap-c');
        oNav.addClass('top-nav-c');
    });
    //点击关闭按钮，让phone-con关闭
    oClose.click(function () {
        oCon.fadeOut(200);
        oWrap.removeClass('nav-wrap-c');
        oNav.removeClass('top-nav-c');
    });
    //订阅按钮
    emailBT.click(function(){
        var reg_email = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
        var email_text = $('.order-input').val();

        if(email_text == ''){
            $('#email-remind-error strong').text('请输入您的E-Mail电子邮箱地址');
            $('#email-remind-error').addClass('remind-error');
        }else{
            if(!reg_email.test(email_text)){
                $('#email-remind-error strong').text('邮箱格式不正确，请您重新输入');
                $('#email-remind-error').addClass('remind-error');
            }else{
                //提交订阅邮箱
                var url = '/auth/saveemail';
                $.ajax({
                    url: url,
                    dataType: 'JSON',
                    method: 'POST',
                    data: {email : email_text},
                    success: function(data) {
                        if(data){
                            switch(data['status']){
                                case 'success' : 
                                    $("#order-email-box").hide();
                                    $("#order-email-success").show();
                                    break;
                                default:
                                    $('#email-remind-error strong').text('邮箱只可订阅一次，请勿重复提交，如有疑问请联系在线客服');
                                    $('#email-remind-error').addClass('remind-error');
                                    break;
                            }
                        }
                    }
                });
            }
        }
    });
    $('.order-input').bind('input propertychange',function(){
        $('#email-remind-error').removeClass('remind-error');
    });
    //页面往下滚动，头部颜色变黑
    $(window).scroll(function () {
        var top = oBody.scrollTop();
        if(top < 140){
            $('.top-nav').css('background','rgba(0,0,0,'+top/150+')');
        }
    });
}
//首页右上角下拉效果
function downMenu() {
    $('.down-center').hover(function () {
        $(this).find('.down-drop').stop(true, false).fadeIn(400);
        $(this).find('.down-menu').addClass('down-menu-hov');
    }, function () {
        $(this).find('.down-drop').stop(true, false).fadeOut(400);
        $(this).find('.down-menu').removeClass('down-menu-hov');
    })
}


