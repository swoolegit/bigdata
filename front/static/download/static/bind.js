/*整理共用呼叫api的模組
設計模式：給定class 綁定click事件
相依：
1.jQuery
2.cms_pub.js
3.jsMessage.js
class 定義：
1.BG_FISH : bg捕魚
2.LOTTO_VR : VR彩票
*/
(function(){
  function loginCheck() {
    if ('' == cms.getToken()){
      JsMsg.warnMsg('请先登录!');
      return false;
    }
    return true;
  }

  function checkMaintenance(code){
    var modules = cms.getCookie("Modules"), result;
    if (modules == null) modules = "null";
    if (modules == undefined) modules = "undefined";
    if (modules != "null" && modules != "undefined") {
      var mitems = eval(modules);
      $.each(mitems, function(i) {
        var gc = mitems[i];
        if (code == gc) {
          result = "/web/maintenance-m.html";
          return false;
        }
      });
    }
    return result;
  }

  function bgFish(){
    $('.BG_FISH').unbind('click').click(function(){
      if(!loginCheck()) return;

      var rs = checkMaintenance('225');
      if(rs) {
        window.open(rs);
        return;
      }
      var _win = window.open('about:blank');
      $.cloudCall({
        method: "thirdparty.bgfish.game.url",
        isLoading: false,
        params: {
          sessionId: cms.getToken(),
          lang: "zh-CN",
          gameType: "101"
        },
        success: function(obj) {
          if (obj.error == null && obj.result) {
            _win.location.href = obj.result
          } else {
            JsMsg.errorObjMsg(obj.error);
          }
        }
      });
    });
  };

  function lottoVR() {
    $('.LOTTO_VR').unbind('click').click(function(){
      if(!loginCheck()) return;
      var rs = checkMaintenance('227');
      if(rs) {
        window.open(rs);
        return;
      }
      var _win = window.open('about:blank');
      $.cloudCall({
        method: "tp.vr.game.url",
        async: true,
        isLoading: false,
        params: {
          sessionId: cms.getToken(),
          lobbyUrl : document.domain
        },
        success: function(obj) {
          if (obj.error == null && obj.result != null) {
            _win.location.href = obj.result;
          } else {
            JsMsg.errorObjMsg(obj.error);
          }
        }
      });
    });
  }

  $(function(){
    all();
  });

  function all() {
    bgFish();
    lottoVR();
  }

  window.bindTool = {
    bgFish : bgFish,
    all : all
  }

})();
