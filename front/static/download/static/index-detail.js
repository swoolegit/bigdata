function noticeClick(num) {
    indexDetail.sethover(num);
}

function noticeBoxClose() {
    $('#ember215').hide();
    $('body').css('overflow-y', 'auto');
}

jQuery.support.cors = true;
var indexDetail = {
    sethover: function(ntype) {
        var tStr = "";
        if (ntype == 1) {
            tStr = "彩票公告";
        } else if (ntype == 2) {
            tStr = "视讯公告";
        } else if (ntype == 3) {
            tStr = "体育公告";
        } else if (ntype == 4) {
            tStr = "电游公告";
        }

        $('.noticetitle strong').html(tStr);
        indexDetail.loadNoticeData(ntype);

    },
    linkInit: function() {
        var aobj = $('.activity-nav').find("a");
        aobj.click(function() {
            aobj.removeClass("hover");
            $(this).addClass("hover");
            var trgtxt = $(this).text();
            var ntype = 0;
            if (trgtxt == "彩票公告") {
                ntype = 1;
            } else if (trgtxt == "视讯公告") {
                ntype = 2;
            } else if (trgtxt == "体育公告") {
                ntype = 3;
            } else if (trgtxt == "电游公告") {
                ntype = 4;
            }
            indexDetail.loadNoticeData(ntype);
        });
    },
    loadNoticeData: function(ntype) {
        var params = {
            sn: cms.getWebSn(),
            popupFlag: "N",
            noticeFrom: ntype,
            pageIndex: 1,
            pageSize: 5
        };

        $.cloudCall({
            method: "sn.notice.new.layer.query",
            params: params,
            success: function(obj) {
                if (obj.error == null && obj.result != null) {
                    var dObj = $(".noticeCwarp");
                    var datas = obj.result.items;
                    var liStr = "";
                    $.each(datas, function(i) {
                        var item = datas[i];
                        var conStr = "<div class='noticeContent'>";
                        var content = JSON.parse(item.content);
                        var Allday = item.createTime.slice(0, 10);
                        if (content != undefined) {
                            conStr += '<div class="date"><span class="inner-title">' + Allday + '</span></div><div class="msg"><span class="inner-title">' + content.contentZh + '</span></div><div class="clear"></div>';
                        }
                        liStr += conStr + "</div><div class='line'></div>";
                    });
                    if (liStr == "") {
                        dObj.html('<div class="noticeContent"><div class="date"><span class="inner-title">无任何公告。</span></div><div class="clear"></div></div>');
                    } else {
                        dObj.html(liStr);
                    }
                } else {
                    JsMsg.errorMsg(obj.error);
                }
                $('body').css('overflow-y', 'hidden');
                $('#ember215').show();
            }
        });

    }
};
