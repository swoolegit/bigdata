var sliceSetting = {
    sliceInit: function(obj) {
        var dataObj = obj;
        var dStr = "";

        $.each(dataObj, function(i) {
            var item = dataObj[i];
            var filePath = cms.updatePath(cms.page.tobase64Decode(item.filePathZh));
            dStr += '<img src="' + filePath + '" />';
        });

        if (dStr != "") {
            $("#slides").html(dStr);
        }
        if (dataObj.length > 1){
            $(function() {
                if ($('#slides')[0]) {
                    $('#slides').slidesjs({
                        width: $('.container').width(),
                        // height: '100%',
                        navigation: true,
                        play: {
                            active: false,
                            auto: true,
                            interval: 4000,
                            swap: true
                        }
                    });
                    setTimeout(function(){
                      bgPage.bannerAdjust();
                    },500);
                }
            });
        }
    }
};
