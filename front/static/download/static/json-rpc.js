jQuery.support.cors = true;
(function($) {

	function jQstringify( obj ) {
		var arr = [];
		$.each( obj, function( key, val ) {
		var next = '"'+ key +'"' + ": ";
		next += $.isPlainObject( val ) ? jQstringify( val ) : '"'+ val +'"';
		arr.push( next );
		});
		return '{ ' + arr.join( ", " ) + ' }' ;
		}

	function  queryString(key) {
            return (document.location.search.match(new RegExp("(?:^\\?|&)" + key + "=(.*?)(?=&|$)")) || ['', null])[1];
    }


	var date = new Date();
	$.extend($, {
		randNum : function() {
			return date.getTime() + '_' + Math.random();
		},
		cloudCall : function(cnf) {
			var $cnf = $.extend({
				method : "",
				params : {},
				Jsonrpc : "2.0",
				dataType : "json",
				ApiPath : 'cloud',
				isLoading : true,
				async : true,
				cache : false,
				success : function(data) {
				},
				error : function(err) {
				}
			}, cnf || {});

			var token = queryString("token");
      if (null != token && "" != token) {
        $cnf.params.sessionId = token;
				$cnf.params.token = token;
      }
      var pathMap = {
        platform:'/platform/api/',
        cloud:'/cloud/api/',
        lottery: '/lottery-api/api/',
        gameArt: '/tpgameart/api/'
      };
			var path = pathMap[$cnf.ApiPath];
      var _host = document.domain;
      var isProd = !(_host.indexOf('localhost') != -1 || _host.indexOf('10.37') != -1 || _host.indexOf('bgbet3') != -1);
      if(!isProd) path = '' + path;
	  return;
			var sn = $cnf.params.sn;
			if((sn == "dv00" || sn == "dy00" || sn == "dx00") && $cnf.method == "auth.login"){
				var encode = new Encode();
				var gcpwd = encode.encodeSha1($("#loginPsw").val());
				var pwd =  {salt:"", token:gcpwd};
				var gparams = $cnf.params;
				gparams.gcSaltedPassword = sha256($("#loginPsw").val());
				gparams.saltedPassword = pwd.token;
				gparams.salt = pwd.salt;
				$cnf.method = "auth.gclogin";
			}

			var json = {
				"id" : $.randNum(),
				"method" : $cnf.method,
				"params" : $cnf.params,
				"jsonrpc" : $cnf.Jsonrpc
			};
			var url = path + $cnf.method;

            var jstr = JSON.stringify(json);
        	$.ajax({
        		type : "POST",
				url : url,
				dataType : $cnf.dataType,
        		data : {json : jstr},
        		cache : $cnf.cache,
        		async : $cnf.async,
        		beforeSend: function(){
        			if($cnf.isLoading){
					  loader(0);
					}
				},
        		success : function(data) {
        		  if($cnf.isLoading){
        		    loader(1);
        		  }
        		  $cnf.success(data);
        		},
        		error : function(error,e,c) {
        		  if($cnf.isLoading){
             		    loader(1);
             	  }
        		  $cnf.error(error);
        		}
        	});

        	function loader(o){
        		if(o){
        			$(".status").fadeOut();
        			$(".preloader").delay(100).fadeOut("fast", function(){
        				$(this).remove();
        			});
        		}else{
        			// if(!$(".preloader")[0])$('body').append('<div class="preloader"><div class="status">&nbsp;</div></div>');
              if(!$(".preloader")[0])$('body').append('<div id="foo" class="preloader"><div id="loadongFrame"></div></div>');
        			var opts = {
        			lines: 13 // The number of lines to draw
        			, length: 28 // The length of each line
        			, width: 14 // The line thickness
        			, radius: 42 // The radius of the inner circle
        			, scale: 0.4 // Scales overall size of the spinner
        			, corners: 1 // Corner roundness (0..1)
        			, color: '#fff' // #rgb or #rrggbb or array of colors
        			, opacity: 0.05 // Opacity of the lines
        			, rotate: 0 // The rotation offset
        			, direction: 1 // 1: clockwise, -1: counterclockwise
        			, speed: 0.7 // Rounds per second
        			, trail: 60 // Afterglow percentage
        			, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
        			, zIndex: 2e9 // The z-index (defaults to 2000000000)
        			, className: 'spinner' // The CSS class to assign to the spinner
        			, top: '50%' // Top position relative to parent
        			, left: '50%' // Left position relative to parent
        			, shadow: false // Whether to render a shadow
        			, hwaccel: false // Whether to use hardware acceleration
        			, position: 'absolute' // Element positioning
        			}
        			var target = document.getElementById('loadongFrame')
        			var spinner = new Spinner(opts).spin(target);
        		}
        	}

		}
	});



})(jQuery);
// http://spin.js.org/#v2.3.2
!function(t,e){t.Spinner=e()}(this,function(){"use strict";function t(t,e){var i,o=document.createElement(t||"div");for(i in e)o[i]=e[i];return o}function e(t){for(var e=1,i=arguments.length;e<i;e++)t.appendChild(arguments[e]);return t}function i(t,e,i,o){var n=["opacity",e,~~(100*t),i,o].join("-"),r=.01+i/o*100,s=Math.max(1-(1-t)/e*(100-r),t),a=l.substring(0,l.indexOf("Animation")).toLowerCase(),d=a&&"-"+a+"-"||"";return p[n]||(c.insertRule("@"+d+"keyframes "+n+"{0%{opacity:"+s+"}"+r+"%{opacity:"+t+"}"+(r+.01)+"%{opacity:1}"+(r+e)%100+"%{opacity:"+t+"}100%{opacity:"+s+"}}",c.cssRules.length),p[n]=1),n}function o(t,e){var i,o,n=t.style;if(e=e.charAt(0).toUpperCase()+e.slice(1),void 0!==n[e])return e;for(o=0;o<d.length;o++)if(i=d[o]+e,void 0!==n[i])return i}function n(t,e){for(var i in e)t.style[o(t,i)||i]=e[i];return t}function r(t){for(var e=1;e<arguments.length;e++){var i=arguments[e];for(var o in i)void 0===t[o]&&(t[o]=i[o])}return t}function s(t,e){return"string"==typeof t?t:t[e%t.length]}function a(t){this.opts=r(t||{},a.defaults,u)}var l,c,d=["webkit","Moz","ms","O"],p={},u={lines:12,length:7,width:5,radius:10,scale:1,corners:1,color:"#000",opacity:.25,rotate:0,direction:1,speed:1,trail:100,fps:20,zIndex:2e9,className:"spinner",top:"50%",left:"50%",shadow:!1,hwaccel:!1,position:"absolute"};if(a.defaults={},r(a.prototype,{spin:function(e){this.stop();var i=this,o=i.opts,r=i.el=t(null,{className:o.className});if(n(r,{position:o.position,width:0,zIndex:o.zIndex,left:o.left,top:o.top}),e&&e.insertBefore(r,e.firstChild||null),r.setAttribute("role","progressbar"),i.lines(r,i.opts),!l){var s,a=0,c=(o.lines-1)*(1-o.direction)/2,d=o.fps,p=d/o.speed,u=(1-o.opacity)/(p*o.trail/100),f=p/o.lines;!function t(){a++;for(var e=0;e<o.lines;e++)s=Math.max(1-(a+(o.lines-e)*f)%p*u,o.opacity),i.opacity(r,e*o.direction+c,s,o);i.timeout=i.el&&setTimeout(t,~~(1e3/d))}()}return i},stop:function(){var t=this.el;return t&&(clearTimeout(this.timeout),t.parentNode&&t.parentNode.removeChild(t),this.el=void 0),this},lines:function(o,r){function a(e,i){return n(t(),{position:"absolute",width:r.scale*(r.length+r.width)+"px",height:r.scale*r.width+"px",background:e,boxShadow:i,transformOrigin:"left",transform:"rotate("+~~(360/r.lines*d+r.rotate)+"deg) translate("+r.scale*r.radius+"px,0)",borderRadius:(r.corners*r.scale*r.width>>1)+"px"})}for(var c,d=0,p=(r.lines-1)*(1-r.direction)/2;d<r.lines;d++)c=n(t(),{position:"absolute",top:1+~(r.scale*r.width/2)+"px",transform:r.hwaccel?"translate3d(0,0,0)":"",opacity:r.opacity,animation:l&&i(r.opacity,r.trail,p+d*r.direction,r.lines)+" "+1/r.speed+"s linear infinite"}),r.shadow&&e(c,n(a("#000","0 0 4px #000"),{top:"2px"})),e(o,e(c,a(s(r.color,d),"0 0 1px rgba(0,0,0,.1)")));return o},opacity:function(t,e,i){e<t.childNodes.length&&(t.childNodes[e].style.opacity=i)}}),"undefined"!=typeof document){c=function(){var i=t("style",{type:"text/css"});return e(document.getElementsByTagName("head")[0],i),i.sheet||i.styleSheet}();var f=n(t("group"),{behavior:"url(#default#VML)"});!o(f,"transform")&&f.adj?function(){function i(e,i){return t("<"+e+' xmlns="urn:schemas-microsoft.com:vml" class="spin-vml">',i)}c.addRule(".spin-vml","behavior:url(#default#VML)"),a.prototype.lines=function(t,o){function r(){return n(i("group",{coordsize:d+" "+d,coordorigin:-c+" "+-c}),{width:d,height:d})}function a(t,a,l){e(u,e(n(r(),{rotation:360/o.lines*t+"deg",left:~~a}),e(n(i("roundrect",{arcsize:o.corners}),{width:c,height:o.scale*o.width,left:o.scale*o.radius,top:-o.scale*o.width>>1,filter:l}),i("fill",{color:s(o.color,t),opacity:o.opacity}),i("stroke",{opacity:0}))))}var l,c=o.scale*(o.length+o.width),d=2*o.scale*c,p=-(o.width+o.length)*o.scale*2+"px",u=n(r(),{position:"absolute",top:p,left:p});if(o.shadow)for(l=1;l<=o.lines;l++)a(l,-2,"progid:DXImageTransform.Microsoft.Blur(pixelradius=2,makeshadow=1,shadowopacity=.3)");for(l=1;l<=o.lines;l++)a(l);return e(t,u)},a.prototype.opacity=function(t,e,i,o){var n=t.firstChild;o=o.shadow&&o.lines||0,n&&e+o<n.childNodes.length&&(n=(n=(n=n.childNodes[e+o])&&n.firstChild)&&n.firstChild)&&(n.opacity=i)}}():l=o(f,"animation")}return a});
