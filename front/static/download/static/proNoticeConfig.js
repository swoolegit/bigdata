﻿var proNoticeConfig = {
    proNoticeInit: function(obj) {

        var nobj = $("#cboxContent");
        var hStr = "";
        cms.delCookie("proNotice");
        $.each(obj, function(i) {
            var item = obj[i];
            var sDate = new Date(item.startDate);
            var eDate = new Date(item.endDate);
            var cDate = new Date(cms.util.getToday());
            var min = cDate.getMinutes();
            var sdiff = parseInt(sDate.getTime() - cDate.getTime());
            var ediff = parseInt(eDate.getTime() - cDate.getTime());
            var ipath = cms.updatePath(item.data);
            if(typeof item.href === 'undefined' || item.href == ""){
                hStr += "<a>";
            }else{
                hStr += "<a href='" + item.href + "' target='_self'>";
            }
            hStr += "<img src='" + ipath + "' style='width:100%;height:100%;'></a><button type='button' id='cboxClose'>close</button>";
            if (sdiff <= 0 && ediff >= 0) {
                var fNotice = cms.getCookie("proNotice");
                if (fNotice != 'undefined' && null != fNotice) {
                    return;
                }
                nobj.html("").html(hStr);
                showLayer();
                cms.setCookieTime("proNotice", item.endDate);
            } else {
                cms.delCookie("proNotice");
            }
        });
        $("#cboxClose").click(function() {
            hideLayer();
        });

        function showLayer() {
            $("#colorbox").show();
            return false;
        }

        function hideLayer() {
            $("#colorbox").hide();
            return false;
        }
    }
}
