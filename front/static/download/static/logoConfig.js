var logoObjData = {
    logoInit: function(path) {
    	var params = {
				"background":'url('+ path +') no-repeat',
				"width":"172px",
				"height":"51px",
				"text-indent":"-9999px",
				"display":"block",
				"position": "absolute"
			 };
    	
    	return params;
    }
}
