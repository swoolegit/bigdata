

var mainbox = "margin: 0px auto;text-align: center; top: 36%;left: 36%;";
var smileStyle = "float: left;font-size: 48px;color: #ffae2e;margin-right: 10px;"
var JsMsg = {
	layerBack : function() {
		var bgObj = document.createElement("div");
		var wh = 'position: fixed; top: 0px; filter: alpha(opacity=30);  z-index: 10099; left: 0px; display: none;';
		bgObj.style.cssText = wh;
		document.body.appendChild(bgObj);
		return bgObj;
	},
	showLy : function() {
		var ly = JsMsg.layerBack();
		var wh = 'width:'
				+ document.body.clientWidth
				+ 'px;height:'
				+ document.body.clientHeight
				+ 'px; position: fixed; top: 0px; filter: alpha(opacity=30);  z-index: 10099; left: 0px; display: block;';
		ly.style.cssText = wh;
		return ly;
	},
	messBox : function(className) {
		var divObj = document.createElement("div");
		divObj.setAttribute("class", className);
		divObj.style.cssText = mainbox;
		document.body.appendChild(divObj);
		return divObj;
	},
	closeButton : function(backObj, msgObj) {
		backObj.style.cssText = "display: none;"
		msgObj.style.cssText = "display: none;"
		document.body.removeChild(backObj);
		document.body.removeChild(msgObj);
	},
	infoMsg : function(message, cnf) {
		var $cnf = $.extend({
			title : "提示信息",
			callback : function() {
			}
		}, cnf || {});

		var mainBox = JsMsg.messBox("layer-main");

		var title = '<div class="layer-title">' + $cnf.title + '</div>';
		var body = ' <div class="layer-body"><ul style="text-align: left;"><li><i class="icon-info-sign"></i><span>'
				+ message + '</span></li></ul></div>';
		var foot = '<div class="layer-foot"> <input class="queren" type="button"  value="确认"  data-disable-with="Login" ></div>';

		mainBox.innerHTML = title + body + foot;
		mainBox.style.cssText = mainbox;
		var ly = JsMsg.showLy();
		mainBox.lastChild.lastChild.onclick = function() {
			JsMsg.closeButton(ly, mainBox);
			$cnf.callback();

		}

	},
	confirmMsg : function(message, cnf) {
		var $cnf = $.extend({
			title : "提示信息",
			callback : function() {
			}
		}, cnf || {});

		var mainBox = JsMsg.messBox("layer-main");

		var title = '<div class="layer-title">' + $cnf.title + '</div>';
		var body = ' <div class="layer-body"><ul style="text-align: left;"><li><i class="icon-info-sign"></i><span>'
				+ message + '</span></li></ul></div>';
		var foot = '<div class="layer-foot"> <input class="queren" type="button"  value="确认"  data-disable-with="Login" > <input class="queren" type="button" value="取消" data-disable-with="Login" style="background: gray;"></div>';

		mainBox.innerHTML = title + body + foot;
		mainBox.style.cssText = mainbox;
		var ly = JsMsg.showLy();
		mainBox.lastChild.childNodes[1].onclick = function() {
			JsMsg.closeButton(ly, mainBox);
			$cnf.callback();
		}

		mainBox.lastChild.childNodes[3].onclick = function() {
			JsMsg.closeButton(ly, mainBox);
		}

	},
	errorMsg : function(obj, cnf) {
		if(obj == null)return;
		var $cnf = $.extend({
			title : "温馨提醒",
			code : obj.code,
			message: obj.message,
			callback : function() {
			}
		}, cnf || {});
		var mainBox = JsMsg.messBox("layer-main");
		var message = $cnf.message;
		if($cnf.code=="110"){
			message = "网络异常,请联系客服!"
		}

		if($cnf.code=="2202" || $cnf.code=="2203"){
				message = "您已在其他设备登陆, 请重新登陆";
		}

		var title = '<div class="layer-title" >' + $cnf.title + '</div>';
		var body = ' <div class="layer-body"><ul style="text-align: left;"><li><i class="icon-smile" style="' + smileStyle + '"></i><span>'
				+  message + '</span></li></ul></div>';
		var foot = '<div class="layer-foot"> <input class="queren" type="button"  value="确认"  data-disable-with="Login" ></div>';

		mainBox.innerHTML = title + body + foot;
		mainBox.style.cssText = mainbox;
		var ly = JsMsg.showLy();
		mainBox.lastChild.lastChild.onclick = function() {
			JsMsg.closeButton(ly, mainBox);
			$cnf.callback();
			//强制登出时
			if($cnf.code=="2264" || $cnf.code=="2221" || $cnf.code=="2202" || $cnf.code=="2203" || $cnf.code=="2204"){
				cms.setCookie("loginOut","1");
				cms.delCookie("isLogin");
				cms.delCookie("Token");
				cms.delCookie("loginStatus");
				cms.delCookie("trialPlay");
				cms.delCookie("WebSn");
			}
		}
	},
	errorObjMsg : function(obj, cnf) {
		if(obj == null)return;
		var $cnf = $.extend({
			title : "温馨提醒",
			code : obj.code,
			message: obj.message,
			callback : function() {
			}
		}, cnf || {});
		if($cnf.code=="700"){
			return;
		}
		var mainBox = JsMsg.messBox("layer-main");

		var message = $cnf.message;
		if($cnf.code=="2202" || $cnf.code=="2203"){
				message = "您已在其他设备登陆, 请重新登陆";
		}

		var title = '<div class="layer-title" >' + $cnf.title + '</div>';
		var body = ' <div class="layer-body"><ul style="text-align: left;"><li><i class="icon-smile" style="' + smileStyle + '"></i><span>'
				+  message + '</span></li></ul></div>';
		var foot = '<div class="layer-foot"> <input class="queren" type="button"  value="确认"  data-disable-with="Login" ></div>';

		mainBox.innerHTML = title + body + foot;
		mainBox.style.cssText = mainbox;
		var ly = JsMsg.showLy();
		mainBox.lastChild.lastChild.onclick = function() {
			JsMsg.closeButton(ly, mainBox);
			$cnf.callback();
			//强制登出时
			if($cnf.code=="2264" || $cnf.code=="2221" || $cnf.code=="2202" || $cnf.code=="2203" || $cnf.code=="2204"){
				cms.setCookie("loginOut","1");
				window.opener = null;
				window.close();
			}
		}
	},
	warnMsg : function(message, cnf) {
		var $cnf = $.extend({
			title : "温馨提醒",
			callback : function() {
			}
		}, cnf || {});
		var mainBox = JsMsg.messBox("layer-main");

		var title = '<div class="layer-title" >' + $cnf.title + '</div>';
		var body = ' <div class="layer-body"><ul class="warning-information" style="text-align: left;"><li><i class="icon-smile" style="' + smileStyle +'"></i><span>'
				+ message + '</span></li></ul></div>';
		var foot = '<div class="layer-foot"> <input class="queren" type="button"  value="确认"  data-disable-with="Login" ></div>';

		mainBox.innerHTML = title + body + foot;
		mainBox.style.cssText = mainbox;
		var ly = JsMsg.showLy();
		mainBox.lastChild.lastChild.onclick = function() {
			JsMsg.closeButton(ly, mainBox);
			$cnf.callback();

			if($cnf.code=="2264" || $cnf.code=="2221" || $cnf.code=="2202" || $cnf.code=="2203" || $cnf.code=="2204"){
				cms.setCookie("loginOut","1");
			}
		};

	}
};
