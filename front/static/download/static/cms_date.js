Date.prototype.format = function (fmt) {
	var o = {
		"M+": this.getMonth() + 1, 
		"d+": this.getDate(),
		"H+": this.getHours(),
		"m+": this.getMinutes(), 
		"s+": this.getSeconds(),
		"q+": Math.floor((this.getMonth() + 3) / 3),
		"S": this.getMilliseconds()
	};
	if (/(y+)/.test(fmt)){
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
	}
	for (var k in o){
		if (new RegExp("(" + k + ")").test(fmt)){
			 fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
};

var cms =  {};

 cms.util = {
	dateFmt:{
		DEFAULT_DATETIME_FORMAT:"yyyy-MM-dd HH:mm:ss",
		YYYY_MM_DD_HH_MM_SS:"yyyy-MM-dd HH:mm:ss",
		YYYY_MM_DD:"yyyy-MM-dd",
		HH_MM_SS:"HH:mm:ss"
	},
	/**
	 * 获取当前时间
	 */
	getCurrTime:function() {
		var date = new Date();
		return date.format(this.dateFmt.DEFAULT_DATETIME_FORMAT);
	},
	/**
	 * 获取当前日期
	 */
	getToday:function() {
		var date = new Date();
		return date.format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取昨天日期
	 */
	getYesterday:function() {
		var date = new Date();
		return this.parseAfterDate(date, 0, 0, -1, 0, 0, 0).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前周第一天
	 */
	getCurrWeekFristDay:function() {
		var date = new Date();
		return this.parseWeekFirstDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前周最后一天
	 */
	getCurrWeekLastDay:function() {
		var date = new Date();
		return this.parseWeekLastDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取上周第一天
	 */
	getLastWeekFristDay:function() {
		var date = new Date();
		date = this.parseAfterDate(date, 0, 0, -7, 0, 0, 0);
		return this.parseWeekFirstDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取上周最后一天
	 */
	getLastWeekLastDay:function() {
		var date = new Date();
		date = this.parseAfterDate(date, 0, 0, -7, 0, 0, 0);
		return this.parseWeekLastDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前月第一天
	 */
	getCurrMonthFristDay:function() {
		var date = new Date();
		return this.parseMonthFirstDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前月最后一天
	 */
	getCurrMonthLastDay:function() {
		var date = new Date();
		return this.parseMonthLastDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前月第一天
	 */
	getLastMonthFristDay:function() {
		var date = new Date();
		date = this.parseAfterDate(date, 0, -1, 0, 0, 0, 0);
		return this.parseMonthFirstDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前月最后一天
	 */
	getLastMonthLastDay:function() {
		var date = new Date();
		date = this.parseAfterDate(date, 0, -1, 0, 0, 0, 0);
		return this.parseMonthLastDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 按指定格式格式化时间
	 * @param {Date} 当前日期
	 * @param {String} 格式串
	 */
	formatDate:function(date,dfm) {
		if(!(date instanceof Date)){
			date = new Date();
		}
		var destDfm = dfm == undefined ? this.dateFmt.DEFAULT_DATETIME_FORMAT : dfm;
		return date.format(destDfm);
	},
	/**
	 * 获取当前日期所在周的第一天
	 * @param {Date} 当前日期
	 */
	parseWeekFirstDay:function(date) {
		if(!(date instanceof Date)){
			date = new Date();
		}
		var days = date.getDay();
		var destDays = 0 - days + 1;
		return this.parseAfterDate(date, 0, 0, destDays, 0, 0, 0);
	},
	/**
	 * 获取当前日期所在周的最后一天
	 * @param {Date} 当前日期
	 */
	parseWeekLastDay:function(date) {
		if(!(date instanceof Date)){
			date = new Date();
		}
		var days = date.getDay();
		var destDays = 6 - days + 1;
		return this.parseAfterDate(date, 0, 0, destDays, 0, 0, 0);
	},
	/**
	 * 获取当前日期所在月的第一天
	 * @param {Date} 当前日期
	 */
	parseMonthFirstDay:function(date) {
		if(!(date instanceof Date)){
			date = new Date();
		}
		date.setDate(1);
		return date;
	},
	/**
	 * 获取当前日期所在月的最后一天
	 * @param {Date} 当前日期
	 */
	parseMonthLastDay:function(date) {
		if(!(date instanceof Date)){
			date = new Date();
		}
		date = this.parseAfterDate(date, 0, 1, 0, 0, 0, 0);
		date.setDate(0);
		return date;
	},
	/**
	 * 返回追加指定年数、月数、天数、小时数、分钟数后的时间对象
	 * @param {String} 表单ID
	 */
	parseAfterDate:function(date,years,months,dates,hours,minutes,secs) {
		if(!(date instanceof Date)){
			date = new Date();
		}
		var destYear = date.getFullYear() + years;
		var destMonth = date.getMonth() + months;
		var destDate = date.getDate() + dates;
		var destHour = date.getHours() + hours;
		var destMinute = date.getMinutes() + minutes;
		var destSec = date.getSeconds() + secs;
		var destMs = date.getMilliseconds();
		date.setFullYear(destYear, destMonth, destDate);
		date.setHours(destHour, destMinute, destSec, destMs);
		return date;
	},
	parseCurrAferDate:function(years,months,dates,hours,minutes,secs) {
		var date = new Date();
		return this.parseAfterDate(date, years, months, dates, hours, minutes, secs);
	}
};

