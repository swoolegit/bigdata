$(function() {
    if ($('#scrollBox')[0]) indexNotice.loadNoticeData();
});
jQuery.support.cors = true;
var indexNotice = {
    loadNoticeData: function() {
        var params = {
            sn: cms.getWebSn(),
            popupFlag: "N",
            pageIndex: 1,
            pageSize: 5
        };

        $.cloudCall({
            method: "sn.notice.new.layer.query",
            isLoading: false,
            params: params,
            success: function(obj) {
                if (obj.error == null && obj.result != null) {
                    var datas = obj.result.items,
                        div = $("#scrollBox"),
                        liStr = "";
                    div.empty().append('<div id="NoticeBoard_H" class="NoticeBoard_H"><marquee scrollamount="3" onMouseOver="this.stop()" onMouseOut="this.start()"><ul></ul></marquee></div>');
                    $.each(datas, function(i) {
                        var item = datas[i];
                        var conStr = "";
                        var content = JSON.parse(item.content);
                        var noticeFrom = item.noticeFrom;
                        if (content != undefined) {
                            var dto = content;
                            conStr = dto.contentZh;
                            conStr = '<li><a href="#" onclick="noticeClick(' + noticeFrom + '); return false;">' + conStr + '</a></li>';
                        }
                        liStr += conStr;
                    });
                    if (liStr == "") {
                        div.find('ul').append('<li>无任何公告。</li>');
                    } else {
                        div.find('ul').append(liStr);
                    }
                    $('body').append('<div id="ember215" style="display:none;"><div class="maskOpacity" style="width: 100%; height: 100%; position: fixed; opacity: 0.4;"></div><div class="noticeMain-wrap" style="top: 148.5px;"><div class="noticeHead"><div class="noticetitle"><strong></strong></div><a class="closed" title="关闭" href="#" hidefocus="true" onclick="noticeBoxClose(); return false;">关闭</a></div><div class="noticeMain"><div class="noticeCwarp"></div></div><div class="noticeBottom"></div></div></div>');
                } else {
                    JsMsg.errorMsg(obj.error);
                }
            }
        });

    }
};
