$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

jQuery.support.cors = true;
(function($) {
	var date = new Date();
	$.extend($, {
		randNum : function() {
			return date.getTime() + '_' + Math.random();
		}
	});
})(jQuery);

Array.prototype.contains = function(item) {
	var i  = this.length;
	while(i--){
		if(this[i] == item){
			return true;
		}
	}
	return false;
};

var cms = {
	language : function() {
			return "Zh";
	},
	queryString : function(key) {
		var location = document.location.search;
	    if(cms.isNull(location))
	    location = window.top.location.search;

	    return (location.match(new RegExp("(?:^\\?|&)" + key
		+ "=(.*?)(?=&|$)")) || [ '', null ])[1];
	},
	getModulesCode : function(mid){
		var result = "";
        var menus = new Array();
        menus["#n4,#j1"] = 4;//game bg
        menus["#n4,#j2"] = 211;//game pt
        menus["#n4,#j3"] = 202;//game mg
        menus["#n5"] = 1;//lottery bg
        menus["#n6"] = 3;//sport bg
        menus["#j61"] = 208;//sport im
        menus["#j62"] = 213;//sport saba
        menus["#n3"] = 2;//video bg
        menus["#n21"] = 207;//video xtd
        menus["#n22"] = 209;//video gd
        menus["#n23"] = 210;//video ab
        menus["#n24"] = 211;//video pt??
        menus["#n25"] = 205;//video AG
        menus["#n26"] = 204;//video bbin
        menus["#n27"] = 201;//video lebo
        menus["#n28"] = 212;//video ytx
        menus["#n29"] = 214;//video sunbet
        menus["#n30"] = 206;//video og
        menus["#n31"] = 215;//video dg
        menus["#n32"] = 216;//video vegas
        menus["#n4,#j4"] = 217;//game GPI
        menus["#n4,#j8"] = 219;//game HABA
        menus["#n4,#j9"] = 220;//game QT
        menus["#n33"] = 220;//QT
        menus["#n34"] = 221;//IMGAME
        menus["#n35"] = 222;//THREESINGSPORT
        menus["#n36"] = 223;//ISOFTBET
        menus["#n37"] = 224;//UGSPORT
        menus["#n38"] = 225;//BGFISH
        menus["#n39"] = 218;//video gc
        menus["#n4,#j11"] = 221;//game ttg
        menus["#n4,#j12"] = 221;//game spg
        menus["#n4,#j13"] = 221;//game prg
	    var sn = cms.getWebSn();
	    if(sn == "SP01" || sn == "ac00"|| sn == "ae00"|| sn == "am00"){
	      return result;
	    }
		var modules = cms.getCookie("Modules");
		if(modules == null) modules = "null";
		if(modules == undefined) modules = "undefined";
		if(modules != "null" && modules != "undefined"){
			var mitems = eval(modules);
			$.each(mitems,function(i){
				var nc = menus[mid];
				var gc = mitems[i];
				if(nc == gc){
					result = "/web/maintenance-m.html";
				}else if(gc === "0"){
					var url = cms.getCookie("ForwardUrl");
					window.location.href = url;
					return;
				}
			});
		}

		return result;
	},
	isProd : function(){
		var unProd = ['localhost', '10.37', 'bgbet3'], result = true;
		for(var i=0; i < unProd.length; i++){
			var str = unProd[i];
			if(document.domain.indexOf(str) != -1) {
				result = false;
				break;
			}
		}
		return result;
	},
	updatePath : function(filePath){
		// var opath = "/upload";
		// var tpath = cms.getCookie("SitePath");
		// var result = filePath.replace(new RegExp(opath,'gm'),tpath);
		// return result;
		var cndPath = cms.isProd() ? cms.getCookie('cdnPath')||'' : '';
		if(filePath.charAt(0) == '/') filePath = cndPath + filePath;
		return filePath;
	},
	checkLoginVar : function() {
		var model = cms.getLoginModel();
		return model != null && !cms.isNull(model.token) && !cms.isNull(model.account);
	},
	validateToken : function(callback, cnf) {
		var $cnf = $.extend({
			sessionId : cms.getToken()
		}, cnf || {});

		$.cloudCall({
			method : "auth.session.validate",
			async : true,
			isLoading : false,
			params : {
				sessionId : $cnf.sessionId
			},
			success : function(obj) {
				if (obj.error == null && jQuery.isFunction(callback)) {
					callback();
				} else {
					if (obj.error.code == "2264") {
						JsMsg.errorMsg(obj.error);
					} else {
						JsMsg.warnMsg("请先登录!");
					}
				}
			}
		});
	},
	validateLoginStatus : function(cnf) {
		//don not used
		var $cnf = $.extend({
			sessionId : window.top.token
		}, cnf || {});

		var st = setInterval(function() {
			getVailStatus();
		}, 5000);

		function getVailStatus() {
			$.cloudCall({
				method : "auth.session.validate",
				async : true,
				isLoading : false,
				params : {
					sessionId : $cnf.sessionId
				},
				success : function(obj) {
					if (obj.error == null) {

					} else {
						clearInterval(st);
                        JsMsg.infoMsg("您长时间未操作,请重新登录!", {
							callback : function() {
								cms.delCookie("loginStatus");
								if (opener != null) {
									var exp = new Date();
									window.location.href = '/skins/' + cms.getWebPath() + '/index.html?t=' + exp.getTime();
								} else {
									window.opener = null;
									window.close();
								}
							}
						});
					}
				}
			});
		}

	},
	valiCentertLoginStatus : function(cnf) {
		var st = setInterval(function() {getVailStatus();}, 5000);
		function getVailStatus() {
			var cookieObj = cms.getCookie("loginStatus");
			if (cookieObj != null && cookieObj != '') {

			} else {
				clearInterval(st);
                JsMsg.infoMsg("您长时间未操作,请重新登录!", {
					callback : function() {
						cms.delCookie("loginStatus");
						window.opener = null;
						window.close();
					}
				});
			}
		}
	},
	curentTime : function() {
		var now = new Date();
		var year = now.getFullYear(); // 年
		var month = now.getMonth() + 1; // 月
		var day = now.getDate(); // 日
		var hh = now.getHours(); // 时
		var mm = now.getMinutes(); // 分
		var ss = now.getSeconds(); // 秒
		var clock = year;

		if (month < 10)
			clock += "0";
		clock += month;
		if (day < 10)
			clock += "0";
		clock += day;
		if (hh < 10)
			clock += "0";
		clock += hh;
		if (mm < 10)
			clock += '0';
		clock += mm;
		if (ss < 10)
			clock += '0';
		clock += ss;
		return (clock);
	},
	openVideoWin : function(url){
		window.open(url, "view_video");
	},
	setCookie : function(name, value,imark) {
		var Days = 1;
		var exp = new Date();
		exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);

		if(imark == 1){
			document.cookie = name + "=" + escape(value)+";path=/";
		}else{
		  document.cookie = name + "=" + escape(value) + ";path=/;expires="+ exp.toGMTString();
		}
	},
	setCookieTime : function(name, value) {
		var Days = 1;
		var exp = new Date(value);
		exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
		document.cookie = name + "=" + escape(value)+ ";path=/;expires=" + exp.toGMTString();
	},
	getCookie : function(name) {
		var arr, reg = new RegExp("(^| )" + name + "=([^;]*)(;|$)");
		if (arr = document.cookie.match(reg))
			return unescape(arr[2]);
		else
			return null;
	},
	delCookie : function(name) {
		var exp = new Date();
		exp.setTime(exp.getTime() - 1);
		var cval = cms.getCookie(name);
		if (cval != null)
			document.cookie = name + "=" + cval + ";path=/;expires=" + exp.toGMTString();
	},
	setSubCookie: function (k,v) {
      document.cookie = k + "=" + v + ";path=/;domain=." + window.location.host;
      // document.cookie = k + "=" + v + ";path=/;domain=.gi506.bgbet3.com";
    },
    delSubCookie: function (k) {
      document.cookie = k + "=;domain=." + window.location.host + ";path=/;expires=2017-05-22T03:45:20.000Z";
      // document.cookie = k + "=;domain=.gi506.bgbet3.com;path=/;expires=2017-05-22T03:45:20.000Z";
    },
	getWebPath : function() {
		var webpath = "";
		if (undefined == window.top.snConfig) {
			var wp = cms.getCookie("WebPath");
			webpath = wp;
		} else {
			webpath = window.top.snConfig.webPath;
			cms.setCookie("WebPath", window.top.snConfig.webPath)
		}
		return webpath;
	},
	getToken : function() {
		var token = "";
		var wp = cms.getCookie("Token");
		if (null != wp && undefined != wp) {
			token = wp;
		}
		return token;
	},
	getLoginModel : function() {
		var model = {};
		var cookieObj = cms.getCookie("loginStatus");
		if (null != cookieObj && undefined != cookieObj) {
		   model = eval('(' + cookieObj + ')');
		}
		return model;
	},
	getUid : function() {
		var cookieObj = cms.getCookie("loginStatus");
		if (null != cookieObj && undefined != cookieObj) {
			var model = eval('(' + cookieObj + ')');
			token = model.uid;
		} else {
			token = window.top.uid;
		}
		return token;
	},
	getWebSn : function() {
		var wp = cms.getCookie("WebSn");
		var webpath = "";
		if (null != wp && undefined != wp) {
			webpath = wp;
		} else {
			webpath = window.top.sn;
			cms.setCookie("WebSn", window.top.sn,1)
		}
		return webpath;
	},
	getWebSn2 : function() {
		var wp = cms.getCookie("WebSn");
		var webpath = "";
		if (null != wp && undefined != wp) {
			webpath = wp;
		} else {
			webpath = window.top.sn;
			cms.setCookie("WebSn", window.top.sn,1)
		}
		return webpath;
	},
	getCloudPath : function() {
		var webpath = "";
		var wp = cms.getCookie("CloudPath");
		if (null != wp && undefined != wp) {
			webpath = wp;
		} else {
			webpath = window.top.cloudPath;
			cms.setCookie("CloudPath", window.top.cloudPath);
		}
		return webpath;
	},
	getPayPath : function() {
		var webpath = "";
		var wp = cms.getCookie("PayPath");
		if (null != wp && undefined != wp) {
			webpath = wp;
		} else {
			webpath = window.top.payPath;
			cms.setCookie("PayPath", window.top.payPath);
		}
		return webpath;
	},
	getLotteryPath : function() {
		var path = "";
		var wp = cms.getCookie("LotteryPath");
		if (null != wp && undefined != wp) {
			path = wp;
		} else {
			path = window.top.lotteryPath;
			cms.setCookie("LotteryPath", window.top.lotteryPath);
		}
		return path;
	},
	getVideoPath : function() {
		var path = "";
		var wp = cms.getCookie("VideoPath");
		if (null != wp && undefined != wp) {
			path = wp;
		} else {
			path = window.top.videoPath;
			cms.setCookie("VideoPath", window.top.videoPath);
		}
		return path;
	},
	MGetPager : function(mo, sn) {
		var snStr = "";
		window.open("/center/" + mo + ".html", "MACENTER", "top=50,left=50,width=1020,height=600,status=no,scrollbars=yes,resizable=no");
	},
	MGetBocoPager : function(mo, sn) {
		var snStr = "";
		window.open("/center_boco/" + mo + ".html", "MACENTER", "top=50,left=50,width=1150,height=800,status=no,scrollbars=yes,resizable=no");
	},
	/**
	 * 去除空格
	 *
	 * @param {String}
	 *            字符串
	 * @return {String} 处理后字符串
	 */
	trim : function(str) {
		return (str + '').replace(/(\s+)$/g, '').replace(/^\s+/g, '');
	},
	/**
	 * 去除左右空格
	 *
	 * @param {String}
	 *            字符串
	 * @return {String} 处理后字符串
	 */
	trim2 : function(str) {
		var trim_str = trim(str);
		return trim_str.replace(/[^\S](\s*)[^\S]/g, " ");
	},
	/**
	 * 返回字符串长度
	 *
	 * @param {String}
	 *            字符串
	 * @return {String} 长度
	 */
	getCharLen : function(str) {
		var total_length = 0;
		for (var i = 0; i < str.length; i++) {
			var intCode = str.charCodeAt(i);
			if (intCode >= 0 && intCode <= 128) {
				total_length = total_length + 1;
			} else {
				total_length = total_length + 2;
			}
		}
		return total_length;
	},
	/**
	 * 判断是否没找到对象
	 *
	 * @param {String}
	 *            对象
	 * @return {Boolean} true:是,false:否
	 */
	isUndefined : function(variable) {
		return typeof variable == 'undefined' ? true : false;
	},
	/**
	 * 获取对象值
	 *
	 * @param {String}
	 *            对象ID
	 * @return {String} 对象值
	 */
	val : function(itemId) {
		return $("#" + itemId).val();
	},
	/**
	 * 设置对象值
	 *
	 * @param {String}
	 *            对象ID
	 * @return {String} 对象值
	 */
	setVal : function(itemId, itemVal) {
		$("#" + itemId).val(itemVal);
	},
	/**
	 * 判断对象值是否等于指定值
	 *
	 * @param {String}
	 *            对象ID
	 * @param {String}
	 *            目标值
	 * @return {Boolean} true:等于,false:不等于
	 */
	isValEq : function(itemId, destVal) {
		var itemVal = cms.val(itemId);
		if (itemVal == destVal) {
			return true;
		} else {
			return false;
		}
	},
	/**
	 * 判断是否为空
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:空,false:非空
	 */
	isNull : function(str) {
		if (str == null || str == "") {
			return true;
		} else {
			return false;
		}
	},
	/**
	 * 判断是否为数字
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:数字,false:非数字
	 */
	isNum : function(val) {
		return !/\D/.test(val);
	},
	/**
	 * 判断是否为电子邮箱
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isEmail : function(vEMail) {
		var regInvalid = /(@.*@)|(\.\.)|(@\.)|(\.@)|(^\.)/;
		var regValid = /^.+\@(\[?)[a-zA-Z0-9\-\.]+\.([a-zA-Z]{2,3}|[0-9]{1,3})(\]?)$/;
		return (!regInvalid.test(vEMail) && regValid.test(vEMail));
	},
	/**
	 * 判断是否为IP地址
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isIp : function(strIP) {
		if (isNull(strIP)) {
			return false;
		}
		var re = /^(\d+)\.(\d+)\.(\d+)\.(\d+)$/g;
		if (re.test(strIP)) {
			if (RegExp.$1 < 256 && RegExp.$2 < 256 && RegExp.$3 < 256
					&& RegExp.$4 < 256)
				return true;
		}
		return false;
	},
	/**
	 * 判断是否为电话号码
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isPhone : function(str) {
		var telReg = /^(([0\+]\d{2,3}-)?(0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/;
		if (!telReg.test(str)) {
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断是否为手机号码
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isMobile : function(str) {
		var mobileReg = /^(0)?[1]{1}[358]{1}\d{9}$/;
		if (!mobileReg.test(str)) {
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断是否为邮政编码
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isPostCode : function(str) {
		var postCodeReg = /^[1-9]{1}\d{5}$/;
		if (!postCodeReg.test(str)) {
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断是否为身份证号码
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isIdCard : function(str) {
		var testReg = /^(\d{14}|\d{17})(\d|[xX])$/;
		if (!testReg.test(str)) {
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断是否格式为yyyy-MM-dd的日期
	 *
	 * @param {String}
	 *            日期字符串
	 * @return {Boolean} true:是,false:否
	 */
	isDate_yyyy_mm_dd : function(str) {
		var dReg = /^((((1[6-9]|[2-9]\d)\d{2})-(0?[13578]|1[02])-(0?[1-9]|[12]\d|3[01]))|(((1[6-9]|[2-9]\d)\d{2})-(0?[13456789]|1[012])-(0?[1-9]|[12]\d|30))|(((1[6-9]|[2-9]\d)\d{2})-0?2-(0?[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))-0?2-29-))$/;
		if (!dReg.test(str)) {
			return false;
		}
		return true;
	},
	/**
	 * 比较判断日期str1是否小于日期str2
	 *
	 * @param {String}
	 *            日期1
	 * @param {String}
	 *            日期2
	 * @return {Boolean} true:是,false:否
	 */
	compareDateLt : function(str1, str2) {
		var sDateStr1 = str1.replace(/-/g, "/");
		var sDateStr2 = str2.replace(/-/g, "/");
		try {
			sDate1 = new Date(sDateStr1);
			sDate2 = new Date(sDateStr2);
			if (sDate1 > sDate2) {
				return false;
			} else {
				return true;
			}
		} catch (e) {
			alert("错误的日期格式！");
			return false;
		}
	},
	/**
	 * 判断是否为两位小数
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isDecimal2 : function(str) {
		var decimalReg = /^(0|[1-9]\d*)(\.\d{1,2})?$/;
		if (!decimalReg.test(str)) {
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断是否为四位小数
	 *
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isDecimal4 : function(str) {
		var decimalReg = /^(0|[1-9]\d*)(\.\d{1,4})?$/;
		if (!decimalReg.test(str)) {
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断是否为指定格式及长度的字符串(由字母、数字、下划线组成的[长度：min-max]字符串)
	 *
	 * @param {Boolean}
	 *            true:必须字母开头,false:未限定
	 * @param {Number}
	 *            最小长度
	 * @param {Number}
	 *            最大长度
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isAcceptChars : function(isLetterStart, min, max, itemVal) {
		var destReg = null;
		if (isLetterStart) {
			min = min - 1;
			max = max - 1;
		}
		if (min < 0) {
			min = 0;
		}
		if (max < min) {
			max = min;
		}
		if (isLetterStart) {
			destReg = eval("/^[a-zA-Z][a-zA-Z0-9_]{" + min + "," + max + "}$/;");
		} else {
			destReg = eval("/^[a-zA-Z0-9_]{" + min + "," + max + "}$/;");
		}
		if (!destReg.test(itemVal)) {
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断是否为指定格式及长度的字符串(由字母、数字组成的[长度：min-max]字符串)
	 *
	 * @param {Boolean}
	 *            true:必须字母开头,false:未限定
	 * @param {Number}
	 *            最小长度
	 * @param {Number}
	 *            最大长度
	 * @param {String}
	 *            值
	 * @return {Boolean} true:是,false:否
	 */
	isAcceptChars2 : function(isLetterStart, min, max, itemVal) {
		var destReg = null;
		if (isLetterStart) {
			min = min - 1;
			max = max - 1;
		}
		if (min < 0) {
			min = 0;
		}
		if (max < min) {
			max = min;
		}
		if (isLetterStart) {
			destReg = eval("/^[a-zA-Z][a-zA-Z0-9]{" + min + "," + max + "}$/;");
		} else {
			destReg = eval("/^[a-zA-Z0-9]{" + min + "," + max + "}$/;");
		}
		if (!destReg.test(itemVal)) {
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断对象值是否等于指定值
	 *
	 * @param {String}
	 *            对象ID
	 * @param {String}
	 *            目标值
	 * @param {String}
	 *            提示信息
	 * @return {Boolean} true:等于,false:不等于
	 */
	isValEq_tip : function(itemId, destVal, tipMsg) {
		if (cms.isValEq(itemId, destVal)) {
			alert(tipMsg);
			return true;
		} else {
			return false;
		}
	},
	/**
	 * 判断对象值是否不等于对象值2
	 *
	 * @param {String}
	 *            对象ID
	 * @param {String}
	 *            对象ID2
	 * @param {String}
	 *            提示信息
	 * @return {Boolean} true:等于,false:不等于
	 */
	isValNotEq_tip : function(itemId, itemId2, tipMsg) {
		var destVal = cms.val(itemId2);
		if (!cms.isValEq(itemId, destVal)) {
			alert(tipMsg);
			return true;
		} else {
			return false;
		}
	},
	/**
	 * 判断是否为空
	 *
	 * @param {String}
	 *            对象ID
	 * @param {String}
	 *            提示信息
	 * @return {Boolean} true:空,false:非空
	 */
	isNull_tip : function(itemId, tipMsg) {
		var itemVal = cms.val(itemId);
		if (cms.isNull(itemVal)) {
			alert(tipMsg);
			return true;
		} else {
			return false;
		}
	},
	/**
	 * 判断是否为数字
	 *
	 * @param {String}
	 *            ID
	 * @param {String}
	 *            提示信息
	 * @return {Boolean} true:数字,false:非数字
	 */
	isNum_tip : function(itemId, tipMsg) {
		var itemVal = cms.val(itemId);
		if (cms.isNum(itemVal)) {
			return true;
		} else {
			alert(tipMsg);
			return false;
		}
	},
	/**
	 * 判断是否为两位小数金额
	 *
	 * @param {String}
	 *            对象ID
	 * @param {String}
	 *            提示信息
	 * @return {Boolean} true:是,false:否
	 */
	isMoney2_tip : function(itemId, tipMsg) {
		var itemVal = cms.val(itemId);
		if (!cms.isDecimal2(itemVal)) {
			alert(tipMsg);
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断是否为四位小数金额
	 *
	 * @param {String}
	 *            对象ID
	 * @param {String}
	 *            提示信息
	 * @return {Boolean} true:是,false:否
	 */
	isMoney4_tip : function(itemId, tipMsg) {
		var itemVal = cms.val(itemId);
		if (!cms.isDecimal4(itemVal)) {
			alert(tipMsg);
			return false;
		} else {
			return true;
		}
	},
	/**
	 * 判断是否为指定格式及长度的字符串(由字母、数字、下划线组成的[长度：min-max]字符串)
	 *
	 * @param {Boolean}
	 *            true:必须字母开头,false:未限定
	 * @param {Number}
	 *            最小长度
	 * @param {Number}
	 *            最大长度
	 * @param {String}
	 *            对象ID
	 * @param {String}
	 *            提示信息
	 * @return {Boolean} true:是,false:否
	 */
	isAcceptChars_tip : function(isLetterStart, min, max, itemId, tipMsg) {
		var itemVal = cms.val(itemId);
		if (!cms.isAcceptChars(isLetterStart, min, max, itemVal)) {
			alert(tipMsg);
			return false;
		} else {
			return true;
		}
	}
};
cms.form = cms.prototype = {
	/**
	 * 设置表单提交地址
	 *
	 * @param {String}
	 *            表单ID
	 * @param {String}
	 *            提交地址
	 */
	action : function(formId, destVal) {
		$("#" + formId).attr("action", destVal);
	},
	/**
	 * 提交表单
	 *
	 * @param {String}
	 *            表单ID
	 */
	submit : function(formId) {
		$("#" + formId).submit();
	},
	/**
	 * 设置对象是否可用
	 *
	 * @param {String}
	 *            对象ID
	 * @param {Boolean}
	 *            是否
	 */
	setEnable : function(itemId, isEnable) {
		if ($("#" + itemId).length > 0) {
			if (isEnable) {
				$("#" + itemId).attr("disabled", false);
				$("#" + itemId).css("border", "");
			} else {
				$("#" + itemId).attr("disabled", true);
				$("#" + itemId).css("border", "1px #ccc dashed");
			}
		}
	},
	/**
	 * 设置单选框是否可用
	 *
	 * @param {String}
	 *            对象ID
	 * @param {Boolean}
	 *            是否
	 */
	setRadioEnable : function(radioName, isEnable) {
		$("input[type='radio'][name='" + radioName + "']").each(function() {
			if (isEnable) {
				$(this).attr("disabled", false);
				$(this).css("border", "");
			} else {
				$(this).attr("disabled", true);
				$(this).css("border", "1px #ccc dashed");
			}
		});
	},
	/**
	 * 设置对象是否只读
	 *
	 * @param {String}
	 *            对象ID
	 * @param {Boolean}
	 *            是否
	 */
	setReadonly : function(itemId, isEnable) {
		if ($("#" + itemId).length > 0) {
			if (isEnable) {
				$("#" + itemId).attr("readonly", true);
				$("#" + itemId).css("border", "1px #ccc dashed");
			} else {
				$("#" + itemId).attr("readonly", false);
				$("#" + itemId).css("border", "");
			}
		}
	},
	/**
	 * 清空值
	 *
	 * @param {String}
	 *            对象ID
	 */
	clearValue : function(itemId) {
		if ($("#" + itemId).length > 0) {
			if ($("#" + itemId).attr("tagName") == "SELECT") {
				$("#" + itemId)[0].selectedIndex = 0;
			} else {
				$("#" + itemId).val("");
			}
		}
	},
	/**
	 * 设置单选框选定值
	 *
	 * @param {String}
	 *            单选框名称
	 * @return {Number} 选定值
	 */
	setRadioVal : function(radioName, itemVal) {
		$("input[type='radio'][name='" + radioName + "']").each(function() {
			if ($(this).val() == itemVal) {
				$(this).attr("checked", true);
				return;
			}
		});
	},
	/**
	 * 返回单选框选定值
	 *
	 * @param {String}
	 *            单选框名称
	 * @return {Number} 选定值
	 */
	getRadioVal : function(radioName) {
		return $("input[type='radio'][name='" + radioName + "']:checked").val();
	},
	/**
	 * 设置复选框选中
	 *
	 * @param {String}
	 *            复选框名称
	 * @return {String} 选定值(以逗号隔开)
	 */
	setSelectVal : function(boxName, itemVal) {
		var itemValArr = itemVal.split(",");
		$.each(itemValArr, function(i, val) {
			$(
					"input[type='checkbox'][name='" + boxName + "'][value='"
							+ val + "']").attr("checked", true);
		});
	},
	/**
	 * 返回复选框选定数量
	 *
	 * @param {String}
	 *            复选框名称
	 * @return {Number} 选定数量
	 */
	selectCount : function(boxName) {
		return $("input[type='checkbox'][name='" + boxName + "']:checked").length;
	},
	/**
	 * 复选框全选
	 *
	 * @param {String}
	 *            全选对象ID
	 * @param {String}
	 *            复选框名称
	 */
	selectAll : function(selAllId, boxName) {
		var flag = false;
		if ($("#" + selAllId).length > 0) {
			flag = $("#" + selAllId).attr("checked");
		}
		$("input[type='checkbox'][name='" + boxName + "']").each(function() {
			$(this).attr("checked", flag);
		});
	},
	/**
	 * 复选框反选
	 *
	 * @param {String}
	 *            复选框名称
	 */
	selectAnti : function(boxName) {
		$("input[type='checkbox'][name='" + boxName + "']").each(function() {
			$(this).attr("checked", !this.checked);
		});
	},
	/**
	 * 获取复选框选中值
	 *
	 * @param {String}
	 *            复选框名称
	 * @return {String} 复选框选中值，以逗号隔开
	 */
	getSelectVal : function(boxName) {
		var destVal = "";
		$("input[type='checkbox'][name='" + boxName + "']:checked").each(
				function() {
					destVal += $(this).val() + ",";
				});
		if (destVal.length > 0) {
			destVal = destVal.substring(0, destVal.length - 1);
		}
		return destVal;
	},
	/**
	 * 检查是否选择了一个复选操作项及确认操作
	 *
	 * @param {String}
	 *            复选框名称
	 * @return {Boolean} true:是,false:否
	 */
	selectOneConfirm : function(boxName) {
		var destCount = $("input[type='checkbox'][name='" + boxName
				+ "']:checked").length;
		if (destCount == 1) {
			return confirm("你确定要执行此操作吗？");
		} else {
			alert("请选择1个操作项");
			return false;
		}
	},
	/**
	 * 检查是否选择了至少指定数量复选操作项及确认操作
	 *
	 * @param {String}
	 *            复选框名称
	 * @param {Number}
	 *            最小选定数量
	 * @return {Boolean} true:是,false:否
	 */
	selectBatchConfirm : function(boxName, minNum) {
		var destCount = $("input[type='checkbox'][name='" + boxName
				+ "']:checked").length;
		if (destCount < minNum) {
			alert("请至少选择" + minNum + "个操作项");
			return false;
		}
		return confirm("你确定要执行此操作吗？");
	},
	/**
	 * 检查是否选择了指定范围数量复选操作项及确认操作
	 *
	 * @param {String}
	 *            复选框名称
	 * @param {Number}
	 *            最小选定数量
	 * @param {Number}
	 *            最大选定数量
	 * @return {Boolean} true:是,false:否
	 */
	selectBatchConfirm2 : function(boxName, minNum, maxNum) {
		var destCount = $("input[type='checkbox'][name='" + boxName
				+ "']:checked").length;
		if (destCount < minNum) {
			alert("请至少选择" + minNum + "个操作项");
			return false;
		}
		if (destCount > maxNum) {
			alert("最多可选择" + maxNum + "个操作项");
			return false;
		}
		return confirm("你确定要执行此操作吗？");
	},
	/**
	 * 确认提示
	 *
	 * @return {Boolean} true:是,false:否
	 */
	confirm : function() {
		return confirm("你确定要执行此操作吗？");
	}
};
cms.page = cms.prototype = {
	tobase64Decode : function(str) {



		function utf8to16(str) {
			var out, i, len, c;
			var char2, char3;
			out = "";
			len = str.length;
			i = 0;
			while (i < len) {
				c = str.charCodeAt(i++);
				switch (c >> 4) {
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
					// 0xxxxxxx
					out += str.charAt(i - 1);
					break;
				case 12:
				case 13:
					// 110x xxxx 10xx xxxx
					char2 = str.charCodeAt(i++);
					out += String.fromCharCode(((c & 0x1F) << 6)
							| (char2 & 0x3F));
					break;
				case 14:
					// 1110 xxxx 10xx xxxx 10xx xxxx
					char2 = str.charCodeAt(i++);
					char3 = str.charCodeAt(i++);
					out += String.fromCharCode(((c & 0x0F) << 12)
							| ((char2 & 0x3F) << 6) | ((char3 & 0x3F) << 0));
					break;
				}
			}
			return out;
		}

		return utf8to16(cms.page.base64Decode(str));

	},
	base64Encode : function (str) {
		function utf16to8(str) {
			var out, i, len, c;
			out = "";
			len = str.length;
			for (i = 0; i < len; i++) {
				c = str.charCodeAt(i);
				if ((c >= 0x0001) && (c <= 0x007F)) {
					out += str.charAt(i);
				} else if (c > 0x07FF) {
					out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
					out += String.fromCharCode(0x80 | ((c >> 6) & 0x3F));
					out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
				} else {
					out += String.fromCharCode(0xC0 | ((c >> 6) & 0x1F));
					out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
				}
			}
			return out;
		}

		str = utf16to8(str);
		var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	    var out, i, len;
	    var c1, c2, c3;

	    len = str.length;
	    i = 0;
	    out = "";
	    while(i < len) {
	    c1 = str.charCodeAt(i++) & 0xff;
	    if(i == len)
	    {
	        out += base64EncodeChars.charAt(c1 >> 2);
	        out += base64EncodeChars.charAt((c1 & 0x3) << 4);
	        out += "==";
	        break;
	    }
	    c2 = str.charCodeAt(i++);
	    if(i == len)
	    {
	        out += base64EncodeChars.charAt(c1 >> 2);
	        out += base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
	        out += base64EncodeChars.charAt((c2 & 0xF) << 2);
	        out += "=";
	        break;
	    }
	    c3 = str.charCodeAt(i++);
	    out += base64EncodeChars.charAt(c1 >> 2);
	    out += base64EncodeChars.charAt(((c1 & 0x3)<< 4) | ((c2 & 0xF0) >> 4));
	    out += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >>6));
	    out += base64EncodeChars.charAt(c3 & 0x3F);
	    }
	    return out;
	},
	base64Decode : function(str) {

		var c1, c2, c3, c4;
		var base64DecodeChars = new Array(-1, -1, -1, -1, -1, -1, -1, -1, -1,
				-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
				-1, -1, 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60,
				61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
				10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
				-1, -1, -1, -1, -1, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
				36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,
				-1, -1, -1, -1, -1);
		var i = 0;
		var len = str == undefined ? 0 : str.length;
		var string = '';

		while (i < len) {
			do {
				c1 = base64DecodeChars[str.charCodeAt(i++) & 0xff]
			} while (i < len && c1 == -1);
			if (c1 == -1)
				break;
			do {
				c2 = base64DecodeChars[str.charCodeAt(i++) & 0xff]
			} while (i < len && c2 == -1);

			if (c2 == -1)
				break;
			string += String.fromCharCode((c1 << 2) | ((c2 & 0x30) >> 4));
			do {
				c3 = str.charCodeAt(i++) & 0xff;
				if (c3 == 61)
					return string;
				c3 = base64DecodeChars[c3]
			} while (i < len && c3 == -1);
			if (c3 == -1)
				break;
			string += String.fromCharCode(((c2 & 0XF) << 4)
					| ((c3 & 0x3C) >> 2));
			do {
				c4 = str.charCodeAt(i++) & 0xff;
				if (c4 == 61)
					return string;
				c4 = base64DecodeChars[c4]
			} while (i < len && c4 == -1);

			if (c4 == -1)
				break;

			string += String.fromCharCode(((c3 & 0x03) << 6) | c4)
		}
		return string;

	},
	config : {
		url : "#",
		async : true,
		cache : false
	},
	setVal : function(itemId, itemVal, autoLoad) {
		cms.setVal(itemId, itemVal);
		if (autoLoad) {
			cms.page.search();
		}
	},
	/**
	 * 提交表单
	 *
	 * @param {String}
	 *            表单ID
	 * @param {String}
	 *            提交地址
	 */
	submit : function(formId, destUrl) {
		cms.form.action(formId, destUrl);
		cms.form.submit(formId);
	},
	/**
	 * 提交搜索表单
	 *
	 * @param {String}
	 *            表单ID
	 * @param {String}
	 *            提交地址
	 */
	searchAct : function(formId, destUrl) {
		var pageWrap = $('#pageContainer');
		$.ajax({
			type : "POST",
			url : destUrl,
			dataType : "html",
			data : $("#" + formId).serialize(),
			cache : cms.page.config.cache,
			async : cms.page.config.async,
			success : function(data) {
				pageWrap.html("");
				pageWrap.show();
				pageWrap.html(data);
				cms.page.processListLine();
			},
			error : function(error) {
			}
		});
	},
	/**
	 * 提交搜索表单
	 */
	search : function() {
		$("#page").val("1");
		var formId = "infoForm";
		var destUrl = cms.page.config.url;
		if (destUrl == "#")
			destUrl = window.top.homeUrl;
		cms.page.searchAct(formId, destUrl);
	},
	/**
	 * 设置表单提交地址
	 *
	 * @param {String}
	 *            提交地址
	 * @param {String}
	 *            目标页
	 */
	to : function(destUrl, destPage) {
		$("#page").val(destPage);
		cms.page.searchAct("infoForm", destUrl);
	},
	/**
	 * 设置表单提交地址
	 *
	 * @param {String}
	 *            提交地址
	 */
	go : function(destUrl) {
		cms.page.searchAct("infoForm", destUrl);
	},
	/**
	 * 设置分页大小并提交表单
	 *
	 * @param {String}
	 *            目标地址
	 */
	size : function(destUrl) {
		var destSize = $("#cmsPageSize").val();
		$("#pageSize").val(destSize);
		cms.page.searchAct("infoForm", destUrl);
	},
	/**
	 * 设置表单提交地址
	 *
	 * @param {String}
	 *            目标地址
	 */
	resetPageUrl : function(destUrl) {
		cms.page.config.url = destUrl;
		window.top.homeUrl = destUrl;
	},
	/**
	 * 加载
	 *
	 * @param {String}
	 *            表单ID
	 */
	load : function(destUrl, cnf) {
		var $cnf = $.extend({
			dataType : "html",
			data : {},
			success : function(data) {
			},
			error : function(err) {
			}
		}, cnf || {});
		if (null != destUrl) {
			cms.page.resetPageUrl(destUrl);
		}
		var pageWrap = $('#pageContainer');
		$.ajax({
			type : "POST",
			url : destUrl,
			dataType : $cnf.dataType,
			data : $cnf.data,
			cache : cms.page.config.cache,
			async : cms.page.config.async,
			success : function(data) {
				pageWrap.html("");
				pageWrap.show();
				pageWrap.html(data);
				cms.page.processListLine();
			},
			error : function(error) {
			}
		});
	},
	/**
	 * 发送数据
	 *
	 * @param destUrl
	 *            目标地址
	 * @param cnf
	 *            配置
	 */
	send : function(destUrl, cnf) {
		var $cnf = $.extend({
			dataType : "json",
			data : {},
			success : function(data) {
			},
			error : function(err) {
			}
		}, cnf || {});
		$.ajax({
			type : "POST",
			url : destUrl,
			data : $cnf.data,
			dataType : $cnf.dataType,
			cache : cms.page.config.cache,
			async : cms.page.config.async,
			success : function(data) {
				$cnf.success(data);
			},
			error : function(error) {
				$cnf.error(error);
			}
		});
	},
	/**
	 * 发送表单数据
	 *
	 * @param destUrl
	 *            目标地址
	 * @param formId
	 *            表单ID
	 * @param cnf
	 *            配置
	 */
	sendForm : function(destUrl, formId, cnf) {
		var $cnf = $.extend({
			dataType : "json",
			success : function(data) {
			},
			error : function(err) {
			}
		}, cnf || {});
		$.ajax({
			type : "POST",
			url : destUrl,
			data : $("#" + formId).serialize(),
			dataType : $cnf.dataType,
			cache : cms.page.config.cache,
			async : cms.page.config.async,
			success : function(data) {
				$cnf.success(data);
			},
			error : function(error) {
				$cnf.error(data);
			}
		});
	},
	/**
	 * 重新加载
	 */
	reload : function() {
		var formId = "infoForm";
		var destUrl = cms.page.config.url;
		if (destUrl == "#")
			destUrl = window.top.homeUrl;
		cms.page.searchAct(formId, destUrl);
	},
	/**
	 * 处理列表隔行附加颜色
	 */
	processListLine : function() {
		$('.table tbody tr:odd').addClass('odd');
	},
	/**
	 * 友情提示错误信息
	 *
	 * @param {Boolean}
	 *            true:显示,false:隐藏
	 */
	processMsgTip : function(isShow) {
		if (isShow) {

		} else {

		}
	},
	/**
	 * 处理无记录提示
	 *
	 * @param {String}
	 *            表单ID
	 */
	processEmptyTip : function(formId) {
		var tbTbodys = $("#" + formId + " table tbody").length;
		var tbRows = $("#" + formId + " table tbody tr").length;
		if (tbTbodys > 0 && tbRows == 0) {
			var thCols = $("#" + formId + " table thead tr th").length;
			if (thCols > 0) {
				var destNullHtml = "<tr><td colspan=\""
						+ thCols
						+ "\" align=\"center\" style=\"height:200px;\">对不起，没有找到相关数据！</td></tr>";
				$("#" + formId + " table tbody").append(destNullHtml);
			}
		}
	},
	/**
	 * 加载操作对话框
	 *
	 * @param destUrl
	 *            目标地址
	 * @param title
	 *            标题
	 * @param width
	 *            宽
	 * @param height
	 *            高
	 * @param cnf
	 *            配置
	 */
	loadAction : function(destUrl, title, width, height, cnf) {
		var $cnf = $.extend({
			lock : true,
			saveText : "保存",
			closeText : "关闭",
			close : function() {
				cms.page.reload();
			}
		}, cnf || {});
		art.dialog.open(destUrl, {
			title : title,
			width : width,
			height : height,
			lock : $cnf.lock,
			button : [ {
				name : $cnf.saveText,
				callback : function() {
					this.iframe.contentWindow.saveAct();
					return false;
				}
			}, {
				name : $cnf.closeText
			} ],
			close : function() {
				$cnf.close();
			}
		});
	},
	/**
	 * 加载浏览对话框
	 *
	 * @param destUrl
	 *            目标地址
	 * @param title
	 *            标题
	 * @param width
	 *            宽
	 * @param height
	 *            高
	 * @param cnf
	 *            配置
	 */
	browseAction : function(destUrl, title, width, height, cnf) {
		var $cnf = $.extend({
			lock : true,
			close : function() {
				cms.page.reload();
			}
		}, cnf || {});
		art.dialog.open(destUrl, {
			title : title,
			width : width,
			height : height,
			lock : $cnf.lock,
			close : function() {
				$cnf.close();
			}
		});
	},
	/**
	 * 加载信息对话框
	 *
	 * @param destUrl
	 *            目标地址
	 * @param title
	 *            标题
	 * @param width
	 *            宽
	 * @param height
	 *            高
	 * @param cnf
	 *            配置
	 */
	loadInfo : function(destUrl, title, width, height, cnf) {
		var $cnf = $.extend({
			lock : true,
			closeText : "关闭",
			close : function() {
				cms.page.reload();
			}
		}, cnf || {});
		art.dialog.open(destUrl, {
			title : title,
			width : width,
			height : height,
			lock : $cnf.lock,
			button : [ {
				name : $cnf.closeText
			} ],
			close : function() {
				$cnf.close();
			}
		});
	},
	delAction : function(destUrl, fieldMsg) {
		if (confirm("确定要删除[" + fieldMsg + "]的信息吗?")) {
			$.get(destUrl, function(result) {
				cms.page.reload();
			});
		}
	},
	confirmAction : function(destUrl, fieldMsg) {
		if (confirm("确定要执行[" + fieldMsg + "]的操作吗?")) {
			$.get(destUrl, function(result) {
				cms.page.reload();
			});
		}
	}
};

Date.prototype.format = function(fmt) {
	var o = {
		"M+" : this.getMonth() + 1,
		"d+" : this.getDate(),
		"H+" : this.getHours(),
		"m+" : this.getMinutes(),
		"s+" : this.getSeconds(),
		"q+" : Math.floor((this.getMonth() + 3) / 3),
		"S" : this.getMilliseconds()
	};
	if (/(y+)/.test(fmt)) {
		fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
				.substr(4 - RegExp.$1.length));
	}
	for ( var k in o) {
		if (new RegExp("(" + k + ")").test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
					: (("00" + o[k]).substr(("" + o[k]).length)));
		}
	}
	return fmt;
};

cms.util = cms.prototype = {
	dateFmt : {
		DEFAULT_DATETIME_FORMAT : "yyyy-MM-dd HH:mm:ss",
		YYYY_MM_DD_HH_MM_SS : "yyyy-MM-dd HH:mm:ss",
		YYYY_MM_DD : "yyyy-MM-dd",
		HH_MM_SS : "HH:mm:ss"
	},
	/**
	 * 获取当前时间
	 */
	getCurrTime : function() {
		var date = new Date();
		return date.format(this.dateFmt.DEFAULT_DATETIME_FORMAT);
	},
	/**
	 * 获取当前日期
	 */
	getToday : function() {
		var date = new Date();
		return date.format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取昨天日期
	 */
	getYesterday : function() {
		var date = new Date();
		return this.parseAfterDate(date, 0, 0, -1, 0, 0, 0).format(
				this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前周第一天
	 */
	getCurrWeekFristDay : function() {
		var date = new Date();
		return this.parseWeekFirstDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前周最后一天
	 */
	getCurrWeekLastDay : function() {
		var date = new Date();
		return this.parseWeekLastDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取上周第一天
	 */
	getLastWeekFristDay : function() {
		var date = new Date();
		date = this.parseAfterDate(date, 0, 0, -7, 0, 0, 0);
		return this.parseWeekFirstDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取上周最后一天
	 */
	getLastWeekLastDay : function() {
		var date = new Date();
		date = this.parseAfterDate(date, 0, 0, -7, 0, 0, 0);
		return this.parseWeekLastDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前月第一天
	 */
	getCurrMonthFristDay : function() {
		var date = new Date();
		return this.parseMonthFirstDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前月最后一天
	 */
	getCurrMonthLastDay : function() {
		var date = new Date();
		return this.parseMonthLastDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前月第一天
	 */
	getLastMonthFristDay : function() {
		var date = new Date();
		date = this.parseAfterDate(date, 0, -1, 0, 0, 0, 0);
		return this.parseMonthFirstDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 获取当前月最后一天
	 */
	getLastMonthLastDay : function() {
		var date = new Date();
		date = this.parseAfterDate(date, 0, -1, 0, 0, 0, 0);
		return this.parseMonthLastDay(date).format(this.dateFmt.YYYY_MM_DD);
	},
	/**
	 * 按指定格式格式化时间
	 *
	 * @param {Date}
	 *            当前日期
	 * @param {String}
	 *            格式串
	 */
	formatDate : function(date, dfm) {
		if (!(date instanceof Date)) {
			date = new Date();
		}
		var destDfm = dfm == undefined ? this.dateFmt.DEFAULT_DATETIME_FORMAT
				: dfm;
		return date.format(destDfm);
	},
	/**
	 * 获取当前日期所在周的第一天
	 *
	 * @param {Date}
	 *            当前日期
	 */
	parseWeekFirstDay : function(date) {
		if (!(date instanceof Date)) {
			date = new Date();
		}
		var days = date.getDay();
		var destDays = 0 - days + 1;
		return this.parseAfterDate(date, 0, 0, destDays, 0, 0, 0);
	},
	/**
	 * 获取当前日期所在周的最后一天
	 *
	 * @param {Date}
	 *            当前日期
	 */
	parseWeekLastDay : function(date) {
		if (!(date instanceof Date)) {
			date = new Date();
		}
		var days = date.getDay();
		var destDays = 6 - days + 1;
		return this.parseAfterDate(date, 0, 0, destDays, 0, 0, 0);
	},
	/**
	 * 获取当前日期所在月的第一天
	 *
	 * @param {Date}
	 *            当前日期
	 */
	parseMonthFirstDay : function(date) {
		if (!(date instanceof Date)) {
			date = new Date();
		}
		date.setDate(1);
		return date;
	},
	/**
	 * 获取当前日期所在月的最后一天
	 *
	 * @param {Date}
	 *            当前日期
	 */
	parseMonthLastDay : function(date) {
		if (!(date instanceof Date)) {
			date = new Date();
		}
		date = this.parseAfterDate(date, 0, 1, 0, 0, 0, 0);
		date.setDate(0);
		return date;
	},
	/**
	 * 返回追加指定年数、月数、天数、小时数、分钟数后的时间对象
	 *
	 * @param {String}
	 *            表单ID
	 */
	parseAfterDate : function(date, years, months, dates, hours, minutes, secs) {
		if (!(date instanceof Date)) {
			date = new Date();
		}
		var destYear = date.getFullYear() + years;
		var destMonth = date.getMonth() + months;
		var destDate = date.getDate() + dates;
		var destHour = date.getHours() + hours;
		var destMinute = date.getMinutes() + minutes;
		var destSec = date.getSeconds() + secs;
		var destMs = date.getMilliseconds();
		date.setFullYear(destYear, destMonth, destDate);
		date.setHours(destHour, destMinute, destSec, destMs);
		return date;
	},
	parseCurrAferDate : function(years, months, dates, hours, minutes, secs) {
		var date = new Date();
		return this.parseAfterDate(date, years, months, dates, hours, minutes,
				secs);
	}
};

cms.app = {
  host : 'https://www.wed0411.com',
  getIOSInstallAddress : function(){
    var _skin = cms.getCookie("WebPath");
    return this.host + '?' + cms.page.base64Encode(_skin);
  },
  getAndroidInstallAddress : function(){
    var _skin = cms.getCookie("WebPath");
    /*2017-08-22微信掃碼不能直接下載，所以改成開啟下載頁面，後面加上a用來判斷ios或安卓*/
    return this.host + '?' + cms.page.base64Encode(_skin + 'a');
    // return this.host + '/app/android/' + _skin + '.apk';
  },
  getIOSInstallQRCode : function(str){
    //如果有傳入str 就用str生成qrcode，也許這個app已上架，沒有放在自家空間
    var data = str ? str : this.getIOSInstallAddress();
    return cms.getCloudPath() + "api.do?pa=qrcode.generator&data=" + data;
  },
  getAndroidInstallQRCode : function(str){
    var data = str ? str : this.getAndroidInstallAddress();
    return cms.getCloudPath() + "api.do?pa=qrcode.generator&data=" + data;
  }
}
