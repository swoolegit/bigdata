﻿var layConfig = {
    layInit: function(obj) {
        var dataObj = obj;
        var cDate = new Date();
        var min = cDate.getMinutes();
        $.each(dataObj, function(i) {
            var item = dataObj[i];
            var id = ".online-service";
            if (item.id == ".layer-le") {
                id = ".download-area";
            }
            if (item.status == "1") {
                var dlObj = $(id);
                if (cms.language() == "Zh") {
                    var entity = item.itemZh;
                    createData(entity.items, dlObj);
                } else if (cms.language() == "Tw") {
                    var entity = item.itemTw;
                    createData(entity.items, dlObj);
                } else if (cms.language() == "En") {
                    var entity = item.itemEn;
                    createData(entity.items, dlObj);
                }
            } else {
                $(id).hide();
            }
            bgPage.getMobileUrl();
        });

        function createData(objs, dlObj) {
            var hStr = "";
            $.each(objs, function(i) {
                var item = objs[i];
                if (item.img == "") {
                    hStr += cms.page.tobase64Decode(item.content);
                } else if (item.href == "") {
                    hStr += "<img src='" + item.img + "' />" + cms.page.tobase64Decode(item.content);
                } else {
                    hStr += "<a href='" + item.href + "' target='" + item.target + "' style='cursor: pointer;'><img src='" + item.img + "' ></a>";
                }
            });
            if (hStr != "") {
                dlObj.html(hStr);
            }

            $(".leftclose").click(function() {
                        $(this).parents(".download-area").fadeOut(300);
            });
            $(".rightclose").click(function() {
                        $(this).parents(".online-service").fadeOut(300);
            });
        }

        var float ='';
        float += '<div id="sidebar">\
                    <ul>\
                        <li class="Service on"><i></i></li>\
                        <li class="goTop"><i></i></li>\
                    </ul>\
                </div>\
                <div id="sideMenu" class="active">\
                    <dl>\
                        <dt>\
                            <h1>服务</h1><i id="closesideMenu"></i>\
                        </dt>\
                        <dd class="live"><a href="javascript: window.open(cms.getCookie(\'CustomerUrl\')); void(0);"><i></i><span>在线客服</span></a></dd>\
                        <dd class="qq"><a href="tencent://message/?uin=11855500&Site=web&Menu=yes"><i></i><span>QQ客服</span></a></dd>\
                        <dd class="draw"><a href="javascript:void(0)" id="floatdraw" onclick="layConfig.floatdraw()"><i></i><span>快速充值</span></a></dd>\
                        <dd class="partners"><a href="agency-agreement.html"><i></i><span>合作加盟</span></a></dd>\
                    </dl>\
                </div>';

                /* 這3個功能沒有
                <dd class="activity"><a href="#" ><i></i><span>活动申请</span></a></dd>\
                <dd class="vip"><a href="#" ><i></i><span>VIP查询</span></a></dd>\
                <dd class="lucky"><a href="#"><i></i><span>幸运转盘</span></a></dd>\
                */
        $('body').append(float);

        $('#closesideMenu').click(function() {
            $('.Service').removeClass('on');
            $("#sideMenu").addClass('on');
        });
        $('.Service').click(function() {
            $('.Service').addClass('on');
            $("#sideMenu").removeClass('on');
        });
        $(".goTop").click(function() {
            jQuery("html,body").animate({
                scrollTop: 0
            }, 800);
        });
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.goTop').addClass('on');
            } else {
                $('.goTop').removeClass('on');
            }
        });
    },
    floatdraw: function() {
        if ("" == cms.getToken()) {
            JsMsg.warnMsg("请先登录!", { code: "2264" });
        }else if("DEMO" == cms.getWebSn()){
            JsMsg.warnMsg("请使用正式帐号登录!");
        }else {
            cms.MGetPager('member-center-jb');
        }
    }
}
