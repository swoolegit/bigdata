
$(function() {
    //右边浮层
    var flag = 0;
    $('#rightArrow').on("click", function() {
        if (flag == 1) {
            $("#OnlineService").animate({ right: '0px' }, 300);
            $(this).animate({ right: '105px' }, 300);
            $(this).find("i").addClass('icon-double-angle-right').removeClass('icon-double-angle-left');
            flag = 0;
        } else {
            $("#OnlineService").animate({ right: '-105px' }, 300);
            $(this).animate({ right: '0' }, 300);
            $(this).find("i").addClass('icon-double-angle-left').removeClass('icon-double-angle-right');
            flag = 1;
        }
    });

    //左边浮层
    var flagL = 0;
    $('#leftArrow').on("click", function() {
        if (flagL == 1) {
            $("#DownloadArea").animate({ left: '0px' }, 300);
            $(this).animate({ left: '105px' }, 300);
            $(this).find("i").addClass('icon-double-angle-left').removeClass('icon-double-angle-right');
            flagL = 0;
        } else {
            $("#DownloadArea").animate({ left: '-105px' }, 300);
            $(this).animate({ left: '0' }, 300);
            $(this).find("i").addClass('icon-double-angle-right').removeClass('icon-double-angle-left');
            flagL = 1;
        }
    });

    setTimeout(function() {
        if (flag == 0) $('#rightArrow').click();
        if (flagL == 0) $('#leftArrow').click();
    }, 5000);


});



if (navigator.appName.indexOf("Microsoft Internet Explorer") != -1 && (navigator.appVersion.match(/8./i) == "8.")) {
    JsMsg.warnMsg('温馨提示：本系统全面停止对IE6/IE7/IE8的支持。请安装IE9以上的版本。');
}


$(window).load(function() {
    $('table').attr({
        border: '1',
        width: '100%'
    });
});
