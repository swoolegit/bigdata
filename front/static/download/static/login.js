jQuery.support.cors = true;
$(function(s) {
    $(".logo").hide();
    bgPage.initControlEvent();
    bgPage.setTitleData();
    bgPage.loadAgentCode();
    bgPage.getMobileUrl();
    bgPage.onMonitor();
    bgPage.initBalanceExchange();
    bgPage.bannerRWD();
});

var firstCount = 0;
var firstCount2 = 0;
var mobileCount = 0;
var menucode = "";
var mark = true;
var thirdPartyList;
var windows = {};

//  TODO  依客戶要求,每隔30sec餘額刷新
var refreshInt = self.setInterval("bgPage.refreshLogin()", 30000);

//  TODO  初始化介面
var bgPage = {
    //{{{ TODO  Banner rwd
    bannerRWD: function() {
        $(window).resize(function() {
            bgPage.bannerAdjust();
        });
    },
    //}}}
    //{{{ TODO  Banner adjust
    bannerAdjust: function() {
        if ($(window).width() > 1900) {
            $('div.container, #slides, .slidesjs-container').css('height', '575px');
            $('.slidesjs-pagination').css('top', '560px');
            return;
        }
        var imgH = $('.slidesjs-slide').height();
        $('div.container, #slides, .slidesjs-container').css('height', imgH + 'px');
        $('.slidesjs-pagination').css('top', (imgH - 30) + 'px');
    },
    //}}}
    //{{{ TODO  Is login trial
    isLoginTrial: function() {
        return !!cms.getCookie("trialPlay");
    },
    //}}}
    //{{{ TODO  Is hover
    isHover: function(id) {
        return $("#" + id + ":hover").length > 0;
    },
    //}}}
    //{{{ TODO  Balance exchange
    doBalanceExchange: function(to, amount) {
        $.cloudCall({
            method: "thirdparty.user.balance.exchange",
            isLoading: false,
            params: {
                sessionId: cms.getToken(),
                from: 0,
                to: to,
                amount: amount
            },
            success: function(obj) {
                if (obj.error == null) {
                    JsMsg.infoMsg('额度转换成功', {
                        callback: function() {
                            if ($('#thConversion').is(':visible')) {
                                $('#thConversion').modal('hide');
                                if ($('#thirdPartyUrl').val() != '#') {
                                    if ($('#thirdPartyTarget').val() != '') {
                                        window.open($('#thirdPartyUrl').val(), '_blank');
                                    } else {
                                        window.location.replace($('#thirdPartyUrl').val());
                                    }
                                } else {
                                    if ($('#thirdPartyChkId').val() == 'mvideoMG') {
                                        $('#formMGVideo').submit();
                                    } else {
                                        JsMsg.errorMsg({
                                            message: '系统维护中'
                                        });
                                    }
                                }
                            }
                        }
                    });
                    bgPage.clearThirdpartyBalanceList();
                    bgPage.setThirdpartyBalanceList();
                    bgPage.refreshLogin();
                } else {
                    JsMsg.errorMsg(obj.error);
                }
            }
        });
    },
    //}}}
    //{{{ TODO  Clear third party balance list
    clearThirdpartyBalanceList: function() {
        $('#selected li').unbind('click');
        $('#tList li').unbind('click');
        $('#selected').empty();
        $('#tList').empty();
    },
    //}}}
    //{{{ TODO  Set third party balance list
    setThirdpartyBalanceList: function() {
        $.cloudCall({
            method: "thirdparty.user.balance.list",
            isLoading: false,
            params: {
                sessionId: cms.getToken()
            },
            success: function(obj) {
                if (obj.result && obj.result.items) {
                    $.each(obj.result.items, function(i, vo) {
                        if (!(vo.thirdpartyName == '' || vo.thirdpartyName == null)) {
                            var _list = [
                                '<tr>',
                                '<td>',
                                '<div class="col-md-5 th-name">' + vo.thirdpartyName + '</div>',
                                '<div class="col-md-6 th-balance"><i class="fa fa-jpy" aria-hidden="true"></i> ' + vo.balance + '</div>',
                                '</td>',
                                '<td class="text-center">',
                                '<button type="button" class="btn btn-primary goToConversion" data-thirdptid="' + vo.thirdpartyId + '" data-thirdptname="' + vo.thirdpartyName + '">',
                                '一键转入',
                                '</button>',
                                '<button type="button" class="btn btn-warning goToGame" data-thirdptid="' + vo.thirdpartyId + '">进入游戏</button>',
                                '</td>',
                                '</tr>'
                            ].join(' ');
                            $('#tList').append(_list);
                        }
                    });
                }
            }
        });
    },
    //}}}
    //{{{ TODO  Init balance exchange
    initBalanceExchange: function() {
        if (!('' == cms.getToken() || bgPage.isLoginTrial())) {
            var _cookieObj = cms.getCookie('loginStatus');
            var _model = eval('(' + _cookieObj + ')');

            $('#balanceBox').remove();
            $.get('fastExchange.html', function(result) {
                $(document.body).append(result);
                $('#main-balance').text(_model.balance);

                // TODO hover popup視窗
                $('#div_balance,#balanceBox').bind("mouseenter", function() {
                    $('#balanceBox').show();
                });
                $('#div_balance,#balanceBox').bind("mouseleave", function() {
                    setTimeout(function() {
                        if (!bgPage.isHover('div_balance') && !bgPage.isHover('balanceBox')) {
                            $('#balanceBox').hide();
                        }
                    }, 100);
                });

                //  TODO  換頁快速鍵
                $('#goExchange').click(function() {
                    bgPage.newCenter('member-new-conversion');
                });

                $('#goDeposit').click(function() {
                    bgPage.newCenter('member-new-deposit');
                });


                //  TODO  一鍵轉入
                $('body').delegate('#balanceBox .goToConversion', 'click', function() {
                    var _this = $(this);
                    $.cloudCall({
                        method: "user.balance.get",
                        isLoading: false,
                        params: {
                            sessionId: cms.getToken()
                        },
                        success: function(_obj) {
                            if (_obj.error == null) {
                                var _balance = Math.floor(_obj.result.balance); //  TODO  取整數
                                if (_balance == 0) {
                                    JsMsg.errorMsg({
                                        message: '系统馀额不足'
                                    });
                                } else {
                                    JsMsg.confirmMsg('确定要将系统馀额[' + _balance + ']轉換至' + _this.data('thirdptname'), {
                                        callback: function() {
                                            bgPage.doBalanceExchange(_this.data('thirdptid'), _balance);
                                        }
                                    });
                                }
                            } else {
                                JsMsg.errorMsg(obj.error);
                                bgPage.loginOut();
                            }
                        }
                    });
                });

                //  TODO  進入遊戲
                $('body').delegate('#balanceBox .goToGame', 'click', function() {
                    var _this = $(this);
                    switch (_this.data('thirdptid')) {
                        case 1:
                            JsMsg.warnMsg('即将上线，敬请期待！');
                            break;
                        case 2:
                            $('#formMGVideo').submit();
                            break;
                        case 4:
                            window.open($('#mvideo5').attr('href'));
                            break;
                        case 5:
                            window.open($('#mvideo4').attr('href'));
                            break;
                        case 6:
                            window.open($('#mvideo8').attr('href'));
                            break;
                        case 8:
                            window.location.replace('sport.html?sportType=sportIm');
                            break;
                        case 9:
                            window.open($('#mvideo3').attr('href'));
                            break;
                        case 10:
                            window.open($('#mvideo6').attr('href'));
                            break;
                        case 11:
                            window.open($('#mvideo7').attr('href'));
                            break;
                        case 12:
                            window.open($('#j5').attr('href'));
                            break;
                        case 13:
                            window.location.replace('sport.html?sportType=sportOext');
                            break;
                        case 14:
                            window.open($('#mvideo10').attr('href'));
                            break;
                        case 15:
                            window.open($('#mvideo11').attr('href'));
                            break;
                        case 16:
                            JsMsg.warnMsg('即将上线，敬请期待！');
                            break;
                        case 17:
                            window.location.replace($('#j4').attr('href'));
                            break;
                    }
                });

                //  TODO  設定box位置
                var offset = $('#div_balance').offset();
                if (offset) {
                    $('#balanceBox').css({
                        top: offset.top + 35,
                        left: offset.left - 390
                    });
                }
                bgPage.setThirdpartyBalanceList();
            });
        }
    },
    //}}}
    //{{{ TODO  On monitor
    onMonitor: function() {
        var st = setInterval(function() { w(); }, 1000);

        function w() {
            var w = cms.getCookie("loginOut");
            if (w === "1") {
                cms.delCookie("loginOut");
                bgPage.loginOut();
            }
        }
    },
    //}}}
    //{{{ TODO  Get web path
    getWebPath: function() {
        return cms.getWebPath();
    },
    //}}}
    //{{{ TODO  Load agent code
    loadAgentCode: function() {
        var agentCode = cms.queryString('agentCode')||cms.queryString('c');
        if (!cms.isNull(agentCode)) {
            cms.setCookie("agentCode", agentCode)
        } else {
            $.cloudCall({
                method: "agent.code",
                isLoading: false,
                params: {
                    domain: document.domain
                },
                success: function(obj) {
                    if (obj.error == null && obj.result != null) {
                        cms.setCookie("agentCode", obj.result.agentCode)
                    }
                }
            });
        }
    },
    //}}}
    //{{{ TODO  Set title data
    setTitleData: function() {
        var title = cms.getCookie("Sitetile");
        parent.document.title = "大游集团";
        if (null != title) {
            parent.document.title = title;
        };
        var logo = cms.getCookie("logoPic");
        if (null != logo && '' != logo) {
            var params = eval('(' + logo + ')');
            $(".logo").find('a').css(params);
        }
        $(".logo").show();
    },
    //}}}
    //{{{ TODO  Logout
    loginOut: function() {
        $.cloudCall({
            method: "auth.online.logout",
            isLoading: false,
            params: {
                sessionId: cms.getToken()
            },
            success: function(obj) {
                showDemo();
                window.top.isLogin = false;
                cms.delCookie("isLogin");
                cms.delCookie("Token");
                cms.delCookie("loginStatus");
                cms.delCookie("trialPlay");
                window.top.token = '';
                window.top.uid = '';
                firstCount = 0;
                firstCount2 = 0;
                mobileCount = 0;
                menucode = "";
                bgPage.resetLoginFrom();
                bgPage.initControlEvent();
                var navName = "index.html";
                var nav = cms.getCookie("CurrentUrl");
                if (null != nav) {
                    navName = nav;
                }
                if (navName === "e-mg.html") {
                    navName = "mgcasino-slotgame.html";
                }
                if (navName === "e-pt.html") {
                    navName = "ptcasino-slotgame.html";
                }
                if (navName === "e-gpi.html") {
                    navName = "gpicasino-slotgame.html";
                }
                if (navName != "index.html") {
                    var exp = new Date();
                    window.location.href = '/skins/' + cms.getWebPath() + '/' + navName + '?t=' + exp.getTime();
                }
                $("#j5,#j6,#j7,.indexbbin,.indexag,.agFish,.bgFish,.indexfish").unbind("click").attr("href", "javascript:void(0)");
                if ("" == cms.getToken()) {
                    $("#j5,#j6,#j7,.indexbbin,.indexag,.agFish,.bgFish,.indexfish").bind("click", function() {
                        JsMsg.warnMsg("请先登录!", { code: "2264" });
                    });
                }
            }
        });

        mark = true;
        var d = cms.getCookie("WebSn");
        if (d === "DEMO") {
            cms.setCookie("WebSn", cms.getCookie("WebSn2"), 1);
        }

        function showDemo() {
            $(".demo").show();
            $(".join").show();
            return false;
        }
    },
    //}}}
    //{{{ TODO  Load login
    loadLogin: function() {
        var cookieObj = cms.getCookie("loginStatus");

        function hideDemo() {
            $(".demo").hide();
            $(".join").hide();
            return false;
        }
        if (cookieObj != null && cookieObj != '') {
            var model = eval('(' + cookieObj + ')');
            bgPage.setReStatus(model);
            hideDemo();
        } else {
            bgPage.resetLoginFrom();
            var token = cms.getToken();
            var login = cms.getCookie("isLogin");
            if (null != login && "true" == login.toString() && !cms.isNull(token)) {
                bgPage.gologin(token);
            } else {
                // set lottery menu
                bgPage.setLotteryMenu();
                bgPage.setGameMenuPm();
            }
        }

        bgPage.resetTopLeft();
        bgPage.resetFoot();
        //bgPage.setSportMenu();

        var rn = Math.random();
        var sn = cms.getWebPath();
        var oHead = document.getElementsByTagName("HEAD").item(0);
        var logo = document.createElement("script");
        var layer = document.createElement("script");
        var proNotice = document.createElement("script");
        var slice = document.createElement("script");
        logo.type = "text/javascript";
        logo.src = baseS + '/site/' + sn + '/logo/logo.js?t=' + rn;
        layer.type = "text/javascript";
        layer.src = baseS + "/site/" + sn + "/layer/layer.js?t=" + rn;
        proNotice.type = "text/javascript";
        proNotice.src = baseS + "/site/" + sn + "/logo/pronotice.js?t=" + rn;
        slice.type = "text/javascript";
        slice.src = baseS + "/site/" + sn + "/logo/slice.js?t=" + rn;
        oHead.appendChild(logo);
        oHead.appendChild(layer);
        oHead.appendChild(proNotice);
        oHead.appendChild(slice);

        setTimeout(function() {
            bgPage.setBgVideoMenu();
        }, 800);
    },
    //}}}
    //{{{ TODO  Init control event
    initControlEvent: function() {
        if (document.onkeydown === null) {
            document.onkeydown = function(event) {
                var e = event || window.event || arguments.callee.caller.arguments[0];
                if (e && e.keyCode == "13") {
                    setTimeout(function() {
                        var ly = $("#ly2").is(":hidden");
                        if (ly) {
                            bgPage.loginAction();
                        } else {
                            $("#tryBtn").click();
                        }
                    }, 0);
                }
            }
        }
        var fg = '<div class="tipsWarp">\
              <a href="javascript:void(0);" onclick="bgPage.setHome(this)">设为首页</a>\
              <a href="tutorial.html" target="_blank">新手教程</a>\
              </div>\
              <div class="EasternTime">\
              <ul>\
              <li class="user-time"><i class="icon-time"></i><span>美东时间：</span></li>\
              </ul>\
              </div>';

        $(".tips").html(fg);
        document.body.focus();
        $("#loginName,#loginPsw").val('');
        $("#register-user").click(function() {
            window.location.href = '/skins/' + bgPage.getWebPath() + '/register.html';
        });
        $("#loginBtn").click(function() {
            //IE9/10 - 防止試玩動作觸發
            if (cms.isNull($("#tcode").val()))
                bgPage.loginAction();
            return false;
        });
        $("#loginName, #loginPsw, #verifyCode").keypress(function(e) {
            if (e.keyCode == 13) {
                bgPage.loginAction();
                return false;
            }
        });
    },
    //}}}
    //{{{ TODO  Login action
    loginAction: function() {
        if (mark == false) {
            return;
        }
        if (cms.isNull($("#loginName").val())) {
            JsMsg.warnMsg("用户名不可为空!");
            return false;
        }
        if (cms.isNull($("#loginPsw").val())) {
            JsMsg.warnMsg("密码不可为空 !");
            return false;
        }
        if (cms.isNull($("#verifyCode").val())) {
            JsMsg.warnMsg("验证码不可为空!");
            return false;
        }
        if ($("#verifyCode").val().length < 4) {
            JsMsg.warnMsg("验证码错误!");
            return false;
        }
        mark = false;
        var encode = new Encode();
        var pwd = encode.encodePsw($("#loginPsw").val());
        var sn = cms.getWebSn();
        $.cloudCall({
            method: "auth.login",
            ApiPath: 'cloud',
            async: true,
            isLoading: false,
            params: {
                sn: sn,
                loginId: cms.trim($("#loginName").val()),
                saltedPassword: pwd.token,
                salt: pwd.salt,
                captchaKey: window.top.codeKey.toString(),
                captchaCode: $("#verifyCode").val(),
                withAuth: '1',
                withProfile: '1',
            },
            success: function(obj) {
                function hideDemo() {
                    $(".demo").hide();
                    $(".join").hide();
                    return false;
                }
                if (obj.error == null && obj.result != null && (obj.result.auth.status == 1 || obj.result.auth.status == 5)) {
                    hideDemo();
                    var me = obj.result;
                    var entity = {};
                    window.top.token = me.sessionId;
                    cms.setCookie("Token", window.top.token, 1);
                    entity.account = $("#loginName").val();
                    entity.sn = me.auth.sn;
                    entity.uid = me.auth.userId;
                    entity.unreadNotice = me.auth.unreadNotice;
                    entity.token = me.sessionId;
                    entity.currency = me.profile.currency;
                    entity.loginLastUpdateTime = me.auth.loginLastUpdateTime;
                    cms.setCookie("UserInfo", JSON.stringify(entity), 1);
                    cms.setCookie("isLogin", true, 1);
                    var navName = "index.html";
                    var nav = cms.getCookie("CurrentUrl");
                    if (null != nav) {
                        navName = nav;
                    }
                    if (navName === "e-mg.html") {
                        navName = "mgcasino-slotgame.html";
                    }
                    if (navName === "e-pt.html") {
                        navName = "ptcasino-slotgame.html";
                    }
                    if (navName === "e-gpi.html") {
                        navName = "gpicasino-slotgame.html";
                    }

                    if (navName != "index.html") {
                        var exp = new Date();
                        window.location.href = '/skins/' + cms.getWebPath() + '/' + navName + '?t=' + exp.getTime();
                    } else {
                        bgPage.gologin(entity.token);
                    }
                    setTimeout(function() {
                        bgPage.initBalanceExchange();
                    }, 800);
                    // bgPage.setSportLink();
                } else {
                    mark = true;
                    if (obj.error != null) {
                        JsMsg.errorMsg(obj.error);
                        if (obj.error.code == '2301') {
                            bgPage.loadVcode();
                        }
                    }
                }
            }
        });
    },
    //}}}
    //{{{ TODO  Trial login
    trialLogin: function() {
        if (mark == false) {
            return;
        }
        if (cms.isNull($("#tcode").val())) {
            JsMsg.warnMsg("验证码不可为空!");
            return;
        }
        mark = false;
        var sn = cms.getWebSn();
        $.cloudCall({
            method: "auth.trial.login",
            ApiPath: 'cloud',
            async: true,
            isLoading: false,
            params: {
                sn: sn,
                captchaKey: window.top.codeKey
                    .toString(),
                captchaCode: $("#tcode").val()
            },
            success: function(obj) {
                if (obj.error == null && obj.result != null && (obj.result.auth.status == 1 || obj.result.auth.status == 5)) {
                    var me = obj.result;
                    window.top.token = me.sessionId;
                    window.top.account = me.loginId;
                    window.top.profile = me.profile;
                    window.top.sessionType = me.sessionType;
                    window.top.uid = me.userId;
                    window.top.isLogin = true;
                    var entity = {};
                    entity.sn = me.auth.sn;
                    entity.uid = me.auth.userId;
                    entity.unreadNotice = me.auth.unreadNotice;
                    entity.account = me.loginId;
                    entity.token = window.top.token;
                    entity.currency = me.profile.currency
                    entity.loginLastUpdateTime = me.auth.loginLastUpdateTime;
                    hideLayer();
                    cms.setCookie("UserInfo", JSON.stringify(entity), 1);
                    cms.setCookie("Token", window.top.token, 1);
                    cms.setCookie("isLogin", true, 1);
                    cms.setCookie("trialPlay", true, 1);
                    cms.setCookie("WebSn", me.auth.sn, 1)
                    bgPage.gologin(entity.token);
                    hideDemo();
                } else {
                    mark = true;
                    if (obj.error != null) {
                        JsMsg.errorMsg(obj.error, {
                            callback: function() {
                                $("#tcode").val("");
                                var timestamp = (new Date()).valueOf();
                                $('#timg').attr("src",
                                    cms.getCloudPath() + 'api.do?pa=captcha.next&key=' + timestamp);
                                window.top.codeKey = timestamp;
                            }
                        });
                    }

                }
            }
        });

        function hideDemo() {
            $(".demo").hide();
            return false;
        }

        function showLayer() {
            $("#ly2").css({
                "display": "block",
                "width": document.body.clientWidth,
                "height": document.body.clientHeight
            });
            $("#tlayer").css({
                "display": "block"
            });
            $("#ly2,#tlayer").show();
            document.getElementById("tcode").focus();
            return false;
        }

        function hideLayer() {
            $("#ly2,#tlayer").hide();
            return false;
        }
    },
    //}}}
    //{{{ TODO  Trial play
    trialPlay: function() {
        showLayer();
        $("#tcode").val('');
        $("#tryCan").click(function() {
            hideLayer();
        });

        $("#tryBtn").click(
            function() {
                bgPage.trialLogin();
                return false;
            });

        function showLayer() {
            $("#ly2").css({
                "display": "block",
                "width": document.body.clientWidth,
                "height": document.body.clientHeight
            });
            $("#tlayer").css({
                "display": "block"
            });
            $("#ly2,#tlayer").show();
            document.getElementById("tcode").focus();
            return false;
        }

        function hideLayer() {
            $("#ly2,#tlayer").hide();
            return false;
        }
    },
    //}}}
    //{{{ TODO  Go login
    gologin: function(token) {
        $.cloudCall({
            method: "user.balance.get",
            isLoading: false,
            params: {
                sessionId: token
            },
            success: function(obj) {
                if (obj.error == null) {
                    var model = {};
                    var cookieObj = cms.getCookie("UserInfo");
                    model = eval('(' + cookieObj + ')');
                    model.balance = obj.result.balance;
                    model.isLogin = window.top.isLogin;
                    var jsonObj = JSON.stringify(model);
                    bgPage.setLotteryMenu(model.token, 1);
                    bgPage.setGameMenuPm();
                    bgPage.setGameMenu(model.token);
                    cms.setCookie("loginStatus", jsonObj, 1);
                    var t = cms.curentTime();
                    cms.setCookie("loginTime", t, 1);
                    cms.delCookie("UserInfo");
                    bgPage.setTopLoginStatus();

                    bgPage.resetTopLeft();
                    bgPage.resetFoot();

                    var rn = Math.random();
                    var ph = cms.getWebPath();
                    var oHead = document.getElementsByTagName("HEAD").item(0);
                    var logo = document.createElement("script");
                    var layer = document.createElement("script");
                    var proNotice = document.createElement("script");
                    logo.type = "text/javascript";
                    logo.src = baseS + '/site/' + ph + '/logo/logo.js?t=' + rn;
                    layer.type = "text/javascript";
                    layer.src = baseS + "/site/" + ph + "/layer/layer.js?t=" + rn;
                    proNotice.type = "text/javascript";
                    proNotice.src = baseS + "/site/" + ph + "/logo/pronotice.js?t=" + rn;
                    oHead.appendChild(logo);
                    oHead.appendChild(layer);
                    oHead.appendChild(proNotice);

                    bgPage.resetTopLeft();
                    bgPage.resetMenu();
                    bgPage.resetFoot();
                    bgPage.setSportMenu();


                    setTimeout(function() {
                        bgPage.setBgVideoMenu();
                    }, 800);

                } else {
                    JsMsg.errorMsg(obj.error);
                    bgPage.loginOut();
                }
            }
        });

        bgPage.showUserNotice();
    },
    //}}}
    //{{{ TODO  Set re status
    setReStatus: function(model) {
        window.top.isLogin = model.isLogin;
        window.top.balance = model.balance;
        window.top.token = model.token;
        window.top.account = model.account;
        window.top.uid = model.uid;
        window.top.profile = model.profile;
        bgPage.setTopLoginStatus();
        bgPage.setLotteryMenu(model.token, 1);
        bgPage.setGameMenu(model.token);
        bgPage.setGameMenuPm();
    },
    //}}}
    //{{{ TODO  Refresh login
    refreshLogin: function() {
        $("#imgR").removeAttr("src").attr("src", "images/refresh.gif");
        $.cloudCall({
            method: "user.balance.get",
            async: true,
            isLoading: false,
            params: {
                sessionId: cms.getToken()
            },
            success: function(obj) {
                if (obj.error == null && obj.result != null) {
                    var cookieObj = cms.getCookie("loginStatus");
                    if (cookieObj != null && cookieObj != '') {
                        var model = eval('(' + cookieObj + ')');
                        model.balance = obj.result.balance;
                        var jsonObj = JSON.stringify(model);
                        cms.setCookie("loginStatus", jsonObj, 1);
                        $('#main-balance').text(model.balance);
                        setTimeout(function() { bgPage.setTopLoginStatus(); }, 500);
                    } else {
                        JsMsg.infoMsg('您长时间未操作,请重新登录!', {
                            callback: function() {
                                cms.delCookie("loginStatus");
                                bgPage.loginOut();
                            }
                        });
                    }
                } else {
                    JsMsg.errorObjMsg(obj.error);
                    setTimeout(function() {
                        $("#imgR").attr("src", "images/refresh.png");
                    }, 100);
                }
            }
        });

        $.cloudCall({
            method: "user.profile.get",
            async: true,
            isLoading: false,
            params: {
                sessionId: cms.getToken()
            },
            success: function(obj) {
                if (obj.error == null && obj.result != null) {
                    var cookieObj = cms.getCookie("loginStatus");
                    if (cookieObj != null && cookieObj != '') {
                        var model = eval('(' + cookieObj + ')');
                        model.unreadNotice = obj.result.unreadNotice;
                        var jsonObj = JSON.stringify(model);
                        cms.setCookie("loginStatus", jsonObj, 1);
                        setTimeout(function() {
                            bgPage.setTopLoginStatus();
                        }, 500);
                    }
                }
            }
        });
    },
    //}}}
    //{{{ TODO  Set lottery menu
    setLotteryMenu: function(token, mark) {
        token = cms.getToken();
        var tr = "";
        try {
            tr = cms.getModulesCode("#n5");
        } catch (e) {}
        if (tr != "") {
            $("#lotteryFrm").html('<iframe id="lotteryFrm2" src="' + tr + '" scrolling="no" width="100%" height="560" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
        } else {
            var path = bgPage.getWebPath();
            var sn = cms.getWebSn();
            var frmUrl = "http://" + window.top.location.host + "/skins/" + path + "/LotteryAgent.html";
            var href = cms.getLotteryPath() + "lotteryLobby.html?sn=" + sn + "&ag=" + frmUrl;
            if (mark == 1) {
                href = cms.getLotteryPath() + "lotteryLobby.html?sn=" + sn + "&ag=" + frmUrl;
                if (token) href += '&token=' + token;
            }
        }
        var lotteryNav = cms.queryString("lotterynav");
        if (lotteryNav) {
            href = cms.getLotteryPath() + "index.html?ltId=" + lotteryNav + "&cs=" + path + "&ag=" + frmUrl;
            if (token) href += '&token=' + token;
        } else {
            href = cms.getLotteryPath() + "lotteryLobby.html?sn=" + sn + "&ag=" + frmUrl;
            if (mark == 1) {
                href = cms.getLotteryPath() + "lotteryLobby.html?sn=" + sn + "&ag=" + frmUrl;
                if (token) href += '&token=' + token;
            }
        }
        $("#lotteryFrm").html('<iframe id="lotteryFrm2" src="' + href + '" scrolling="no" width="100%" height="1288" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
    },
    //}}}
    //{{{ TODO  Set game menu
    setGameMenu: function(token) {
        var tr = cms.getModulesCode(menucode);
        if (tr != "") {
            $("#gameFrm").html('<iframe id="gameFrm2" src="' + tr + '" scrolling="no" width="100%" height="560" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
        } else {
            var path = bgPage.getWebPath();
            var sn = cms.getWebSn();
            var href = 'http://game.bg1207.com:8080/AS3TnexWeb/lobby.html?sn=' + sn + '&token=' + token;
            $("#gameFrm").html('<iframe id="gameFrm2" src="' + href + '" scrolling="no" width="100%" height="978" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
        }
    },
    //}}}
    //{{{ TODO  Set game menu pm
    setGameMenuPm: function() {
        var tr = cms.getModulesCode(menucode);
        if (tr != "") {
            $("#electronicFrm").html('<iframe id="electronicFrm2" src="' + tr + '" scrolling="no" width="100%" height="560" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
        } else {
            switch (menucode) {
                case "#n4,#j2":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/pt-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
                case "#n4,#j3":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/mg-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
                case "#n4,#j4":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/gpi-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
                case "#n4,#j8":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/haba-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
                case "#n4,#j9":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/qt-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
                case "#n4,#j10":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/isb-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
                case "#n4,#j11":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/ttg-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
                case "#n4,#j12":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/sg-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
                case "#n4,#j13":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/prg-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
                case "#n4,#j14":
                    $("#electronicFrm").html('<iframe id="electronicFrm2" src="/open/bg-game2.html" scrolling="no" width="100%" height="100%" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
                    break;
            }
        }
    },
    //}}}
    //{{{ TODO  Set sport 3 sing
    setSport3sing: function() { //皇冠體育
        var tr = cms.getModulesCode("#n35");
        if (tr != "") {
            $("#Product_SportGame_GT_Container_E74vu14vmv").html(' <div id="sportFrm" class="live-video-main" style="background:#000; text-align:center;"><iframe id="sportFrm" src="' + tr + '" scrolling="no" width="100%" height="968" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe></div>');
            return;
        }
        $.cloudCall({
            method: "thirdparty.threesing.sport.url",
            isLoading: true,
            params: {
                sessionId: cms.getToken(),
                lang: 2,
                device: "pc"
            },
            success: function(obj) {
                if (obj.error == null && obj.result != null) {
                    var href = obj.result;
                    $("#Product_SportGame_GT_Container_E74vu14vmv").html(' <div id="sportFrm" class="live-video-main" style="background:#000; text-align:center;"><iframe id="sportFrm" src="' + href + '" scrolling="no" width="100%" height="968" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe></div>');
                }
            }
        });
    },
    //}}}
    //{{{ TODO  Set sport IM
    setSportIM: function() { //IM體育
        var tr = cms.getModulesCode("#j61");
        if (tr != "") {
            $("#Product_SportGame_GT_Container_E74vu14vmv").html(' <div id="sportFrm" class="live-video-main" style="background:#000; text-align:center;"><iframe id="sportFrm" src="' + tr + '" scrolling="no" width="100%" height="968" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe></div>');
            return;
        }
        $.cloudCall({
            method: "thirdparty.im.sport.url",
            isLoading: false,
            params: { sessionId: cms.getToken() },
            success: function(obj) {
                if (obj.error == null && obj.result != null) {
                    var href = obj.result;
                    $("#Product_SportGame_GT_Container_E74vu14vmv").html(' <div id="sportFrm" class="live-video-main" style="background:#000; text-align:center;"><iframe id="sportFrm2" src="' + href + '" scrolling="no" width="100%" height="968" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe></div>');
                }
            }
        });
    },
    //}}}
    //{{{ TODO  Set sport oext
    setSportOext: function() { //沙巴體育
        var tr = cms.getModulesCode("#j62");
        if (tr != "") {
            $("#Product_SportGame_GT_Container_E74vu14vmv").html(' <div id="sportFrm" class="live-video-main" style="background:#000; text-align:center;"><iframe id="sportFrm2" src="' + tr + '" scrolling="no" width="100%" height="968" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe></div>');
            return;
        }
        var token = cms.getToken()
        $("#Product_SportGame_GT_Container_E74vu14vmv").html(' <div id="sportFrm" class="live-video-main" style="background:#000; text-align:center;"><iframe id="sportFrm2" src="http://oext.dyvip888.com/dyvip888.html?token=' + token + '" scrolling="no" width="100%" height="968" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe></div>');
    },
    //}}}
    //{{{ TODO  Set sport UG
    setSportUG: function() { //UG體育
        var tr = cms.getModulesCode("#n37");
        if (tr != "") {
            $("#Product_SportGame_GT_Container_E74vu14vmv").html(' <div id="sportFrm" class="live-video-main" style="background:#000; text-align:center;"><iframe id="sportFrm2" src="' + tr + '" scrolling="no" width="100%" height="968" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe></div>');
        }
        $.cloudCall({
            method: "thirdparty.ug.sport.launch",
            isLoading: true,
            params: {
                sessionId: cms.getToken(),
                device: "PC"
            },
            success: function(obj) {
                if (obj.error == null && obj.result != null) {
                    var href = obj.result;
                    $("#Product_SportGame_GT_Container_E74vu14vmv").html(' <div id="sportFrm" class="live-video-main" style="background:#000; text-align:center;"><iframe id="sportFrm2" src="' + href + '" scrolling="no" width="100%" height="968" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe></div>');
                }
            }
        });
    },
    //}}}
    //{{{ TODO  Set sport menu
    setSportMenu: function() {
        $('.sportLink').unbind('click');
        //皇冠體育
        $('.sport3sing').click(function() {
            if ("" == cms.getToken()) {
                $(".sport3sing").attr("href", 'javascript:void(0)');
                JsMsg.warnMsg("请先登录!");
                return;
            } else if (bgPage.isLoginTrial()) {
                $(".sport3sing").attr("href", 'javascript:void(0)');
                JsMsg.warnMsg("请使用正式帐号登录!");
                return;
            }
            $(".sport3sing").attr("href", 'sport.html?sportType=sport3sing');
        });

        //IM體育
        $(".sportIm").click(function() {
            if ("" == cms.getToken()) {
                $(".sportIm").attr("href", 'javascript:void(0)');
                JsMsg.warnMsg("请先登录!");
                return;
            } else if (bgPage.isLoginTrial()) {
                $(".sportIm").attr("href", 'javascript:void(0)');
                JsMsg.warnMsg("请使用正式帐号登录!");
                return;
            }
            $(".sportIm").attr("href", 'sport.html?sportType=sportIm');
        });

        //沙巴體育
        $(".sportOext").click(function() {
            if ("" == cms.getToken()) {
                $(".sportOext").attr("href", 'javascript:void(0)');
                JsMsg.warnMsg("请先登录!");
                return;
            } else if (bgPage.isLoginTrial()) {
                $(".sportOext").attr("href", 'javascript:void(0)');
                JsMsg.warnMsg("请使用正式帐号登录!");
                return;
            }
            $(".sportOext").attr("href", 'sport.html?sportType=sportOext');
        });

        //UG體育
        $('.sportUG').click(function() {
            if ("" == cms.getToken()) {
                $(".sportUG").attr("href", 'javascript:void(0)');
                JsMsg.warnMsg("请先登录!");
                return;
            } else if (bgPage.isLoginTrial()) {
                $(".sportUG").attr("href", 'javascript:void(0)');
                JsMsg.warnMsg("请使用正式帐号登录!");
                return;
            }
            $(".sportUG").attr("href", 'sport.html?sportType=sportUG');
        });
    },
    //}}}
    //{{{ TODO  Set favorite
    setFavorite: function() {
        var url = window.top.location.href;
        var title = cms.getCookie("Sitetile");
        if (window.external && 'addFavorite' in window.external) {
            window.external.addFavorite(url, title);
        } else if (window.sidebar && window.sidebar.addPanel) {
            window.sidebar.addPanel(url, title);
        } else if (window.opera && window.print) {
            this.title = title;
            return true;
        } else {
            JsMsg.warnMsg("抱歉，您所使用的浏览器无法完成此操作。\n\n如需加入收藏，请使用Ctrl+D进行添加");
        }
    },
    //}}}
    //{{{ TODO  Home setting
    setHome: function(obj) {
        try {
            var url = window.top.location.href;
            obj.style.behavior = 'url(#default#homepage)';
            obj.setHomePage(url);
        } catch (e) {
            if (window.netscape) {
                try {
                    netscape.security.PrivilegeManager
                        .enablePrivilege("UniversalXPConnect");
                } catch (e) {
                    JsMsg.warnMsg("抱歉，此操作被浏览器拒绝！\n\n请在浏览器地址栏输入“about:config”并回车然后将[signed.applets.codebase_principal_support]设置为'true'");
                }
            } else {
                JsMsg.warnMsg("抱歉，您所使用的浏览器无法完成此操作。\n\n您需要手动将【" + url + "】设置为首页。");
            }
        }
    },
    //}}}
    //{{{ TODO  Show user notice
    showUserNotice: function() {
        var dtostr = bgPage.loadSnNewNoticeData();
    },
    //}}}
    //{{{ TODO  Clear read information
    clearReadInfo: function() {
        $.cloudCall({
            method: "user.notice.unread.touch",
            isLoading: false,
            params: {
                sessionId: cms.getToken()
            },
            success: function(obj) {
                if (obj.error == null && obj.result != null) {
                    var model = cms.getLoginModel();
                    model.unreadNotice = 0;
                    cms.delCookie("loginStatus");
                    var jsonObj = JSON.stringify(model);
                    cms.setCookie("loginStatus", jsonObj, 1);
                    bgPage.setTopLoginStatus();
                } else {
                    JsMsg.errorMsg(obj.error);
                }
            }
        });
    },
    //}}}
    //{{{ TODO  New member center
    newCenter: function(mo) {
        window.open("/centerNew/" + mo + ".html", "MACENTER", "top=50,left=50,width=1020,height=600,status=no,scrollbars=yes,resizable=no")
    },
    //}}}
    //{{{ TODO  Set top login status
    setTopLoginStatus: function() {

        var pageMain = $(".login");
        var loginStr = "";
        var trial = cms.getCookie("trialPlay");
        if (null != trial && "true" == trial.toString()) {
            var model = cms.getLoginModel();
            loginStr += '<div id="Profile_BaseInfo_E74vs5v15f" class="logined">';
            loginStr += '<div class="fw toAdjust">欢迎您：<span class="name">' + model.account + '</span></div>';
            loginStr += '<div class="fw" style="margin-left:7px;">账户余额：<span id="balance">' + model.balance + '</span><a href="javascript:void(0)" id="refreshAmount" onclick="bgPage.refreshLogin();" class="sx"><img id="imgR" src="images/refresh.png"></a></div>';
            loginStr += '<a id="a_logout" href="javascript:void(0)" onclick="bgPage.loginOut();"><div id="logoutbtn"><p>登出</p></div></a>';
            loginStr += '</div>';
        } else {
            /*
              var userCentrel = "cms.MGetPager('member-center-jb');";
              var userDeposit = "cms.MGetPager('member-center-xsck');";
              var userWljl = "cms.MGetPager('member-center-wljl');";
              var userXsqk = "cms.MGetPager('member-center-xsqk');";
              var userGrxx = "cms.MGetPager('member-center-grxx');bgPage.clearReadInfo()";
              var userEd = "cms.MGetPager('member-center-ed');";
              */

            var userCentrel = "bgPage.newCenter('member');";
            var userDeposit = "bgPage.newCenter('member-new-deposit');";
            var userWljl = "bgPage.newCenter('member-new-bet');"
            var userXsqk = "bgPage.newCenter('member-new-withdrawal');"
            var userGrxx = "bgPage.newCenter('member-new-personalMsg');bgPage.clearReadInfo()";
            var userEd = "bgPage.newCenter('member-new-conversion');";


            var model = cms.getLoginModel();

            loginStr += '<div id="Profile_BaseInfo_E74vs5v15f" class="logined">';
            loginStr += '<div class="toAdjust" >欢迎您：<span class="name">' + model.account + '</span></div><div class="toAdjust fw" style="margin-left:7px;">账户余额：<span id="balance">' + model.balance + '</span><a href="javascript:void(0)" id="refreshAmount" onclick="bgPage.refreshLogin();" class="sx"><img id="imgR" src="images/refresh.png"></a></div>';
            loginStr += '<div class="ssw"><a href="javascript:void(0)" onclick="' + userGrxx + '">未读讯息(<font id="newCount" style="color:#ebd25e;">' + model.unreadNotice + '</font>)</a></div><div class="sssw"><a href="javascript:void(0)" onclick="' + userCentrel + '">会员中心</a></div><div class="sssw" id="div_balance"><a href="javascript:void(0)" onclick="' + userEd + '">额度转换</a></div><div class="sssw"><a href="javascript:void(0)" onclick="' + userDeposit + '">线上存款</a></div><div class="sssw"><a href="javascript:void(0)" onclick="' + userXsqk + '">线上取款</a></div><div class="sssw"><a href="javascript:void(0)" onclick="' + userWljl + '">投注记录</a></div><div class="sssw"><a href="javascript: window.open(cms.getCookie(\'CustomerUrl\')); void(0);" >在线客服</a></div>';
            loginStr += '<a id="a_logout" href="javascript:void(0);" onclick="bgPage.loginOut();"><div id="logoutbtn"><p>登出</p></div></a>';
            loginStr += '</div>';
        }


        pageMain.html(loginStr);
        bgPage.layoutAdjust();

    },
    //}}}
    //{{{ TODO  Layout adjust
    layoutAdjust: function() { //跨瀏覽器問題
        if (bgPage.isIE()) {
            $('.toAdjust').css({
                position: 'relative',
                bottom: '8px'
            });
        }
    },
    //}}}
    //{{{ TODO  Is IE
    isIE: function() {
        var userAgent = navigator.userAgent;
        return userAgent.indexOf("MSIE ") > -1 || userAgent.indexOf("Trident/") > -1 || userAgent.indexOf("Edge/") > -1;
    },
    //}}}
    //{{{ TODO  Reset login from
    resetLoginFrom: function() {
        var pageMain = $(".login"); // .unuer
        var customUrl = cms.getCookie("CustomerUrl");
        var loginStr = '';
        loginStr += '<input type="text" placeholder="账号" name="txtLoginUsername" id="loginName" maxlength="16" autocomplete="off" class="username" />\
                <input type="password" placeholder="密码" name="txtLoginPassword" id="loginPsw" class="password" autocomplete="off" maxlength="20" />\
                <input id="verifyCode" type="text" class="login-input" style="font-size: 12px;" placeholder="验证码" maxlength="4" autocomplete="off" />\
                <input id="loginBtn" type="submit" class="login-submit" value="登录" />\
                <a href="register.html" class="registerbtn btna">注册</a>\
                <a class="tryplay btna" hidefocus="true" href="javascript:void(0);" onclick="bgPage.trialPlay()">试玩</a>\
                <a href="javascript: window.open(cms.getCookie(\'CustomerUrl\')); void(0);" class="custom">在线客服</a>\
                <img name="checkNum_img" id="fimg" class="fimg" alt="点击更换" src="images/code.jpg" class="nub" onerror="bgPage.loadVcode()">\
                <a id="fgpwd" href="javascript:void(0);" class="fgpwd">忘记?</a>';

        pageMain.html(loginStr);



        document.getElementById("verifyCode").onfocus = function() {
            if (firstCount == 0) {
                bgPage.loadVcode();
                firstCount++;
            }
        };

        $('#fimg,.refimg').click(function() {
            bgPage.loadVcode();
        });

        $("#fgpwd").click(function() {
            JsMsg.infoMsg('请联系客服', {
                callback: function() {
                    var customUrl = cms.getCookie("CustomerUrl");
                    window.location.href = customUrl;
                }
            });

        });

        document.getElementById("loginName").focus();
    },
    //}}}
    //{{{ TODO  Load V code
    loadVcode: function() {
        var timestamp = (new Date()).valueOf();
        var src = cms.getCloudPath() + 'api.do?pa=captcha.next&key=' + timestamp;
        $('#fimg').attr("src", src);
        window.top.codeKey = timestamp;
    },
    //}}}
    //{{{ TODO  Resrt foot
    resetFoot: function() {
        var pageFoot = $("footer");
        var footLeft = '';
        footLeft += '<div class="mian">\
            <div class="head">\
                <div class="lab help">\
                    <div class="hd">\
                        <h1>H</h1>\
                        <span>\
                        <p>使用帮助</p>\
                        <b>elp</b>\
                    </span>\
                    </div>\
                    <div class="bd copyRight">\
                        <ul>\
                            <li><a href="about.html">关于我们</a></li>\
                            <li>|</li>\
                            <li><a href="deposit.html">如何存款</a></li>\
                            <li>|</li>\
                            <li><a href="withdraw.html">如何取款</a></li>\
                            <li>|</li>\
                            <li><a href="faq.html">常见问题</a></li>\
                            <li>|</li>\
                            <li><a href="responsible.html">责任博彩</a></li>\
                            <li>|</li>\
                            <li><a href="agreement-rules.html">协议与规则</a></li>\
                            <li>|</li>\
                            <li><a href="privacy.html">隐私声明</a></li>\
                        </ul>\
                    </div>\
                </div>\
                <div class="lab contact">\
                    <div class="hd">\
                        <h1>C</h1>\
                        <span>\
                        <p>联系我们</p>\
                        <b>ontact us</b>\
                    </span>\
                    </div>\
                    <div class="bd">\
                        <ul>\
                            <li><a href="tencent://message/?uin=11855500&Site=web&Menu=yes">QQ客服</a></li>\
                            <li>|</li>\
                            <li><a href="javascript: window.open(cms.getCookie(\'CustomerUrl\')); void(0);">在线客服</a></li>\
                            <li>|</li>\
                            <li><a href="contact.html">电子邮箱</a></li>\
                        </ul>\
                    </div>\
                </div>\
                <div class="lab daili">\
                    <div class="hd">\
                        <h1>A</h1>\
                        <span>\
                        <p>代理合营</p>\
                        <b>gency</b>\
                    </span>\
                    </div>\
                    <div class="bd">\
                        <ul>\
                            <li><a href="agency-agreement.html">合营代理</a></li>\
                        </ul>\
                    </div>\
                </div>\
            </div>\
            <div class="footimg">\
                <img src="images/footer_company.png" alt="">\
            </div>\
        </div>\
        <div class="copyright">版权声明:Copyright<span>©</span> BG娱乐版权所有 Reserved</div>';

        pageFoot.html("").html(footLeft);


        $('#tcode').css({ 'width': '90px', 'height': '28px' });
        document.getElementById("tcode").onfocus = function() {
            if (firstCount2 == 0) {
                bgPage.tryCode();
                firstCount2++;
            }
        };


        if ("" == cms.getToken()) {
            $("#j3").bind("click", function() {
                JsMsg.warnMsg("请先登录!", { code: "2264" });
            });
        }






        if (menucode == "#n4,#j2" || menucode == "#n4,#j3" || menucode == "#n4,#j4" || menucode == "#n4,#j8" || menucode == "#n4,#j9") {
            var tr = cms.getModulesCode(menucode);
            if (tr != "") {
                var iObj = parent.document.getElementById('electronicFrm');
                $(iObj).html('<iframe id="electronicFrm2" src="' + tr + '" scrolling="no" width="100%" height="560" frameborder="no" border="0"marginwidth="0" marginheight="0" allowtransparency="yes"></iframe>');
            }
        }


        $('#timg').click(function() {
            bgPage.tryCode();
        });

        function tryCode() {
            var timestamp = (new Date()).valueOf();
            $('#timg').attr("src", cms.getCloudPath() + 'api.do?pa=captcha.next&key=' + timestamp);
            window.top.codeKey = timestamp;
        }


    },
    //}}}
    //{{{ TODO  Try code
    tryCode: function() {
        var timestamp = (new Date()).valueOf();
        $('#timg').attr("src",
            cms.getCloudPath() + 'api.do?pa=captcha.next&key=' + timestamp);
        window.top.codeKey = timestamp;
    },
    //}}}
    //{{{ TODO  Bg video menu setting
    setBgVideoMenu: function() {
        if ("" != cms.getToken()) {
            var locale = 'zh_CN';
            var model = cms.getLoginModel();
            var account = model.account;
            var path = cms.getWebPath();
            var sn = cms.getWebSn();
            var token = cms.getToken();
            var uid = cms.getUid();
            var frmUrl = "http://" + window.top.location.host + "/web/index.html";

            //{{{ TODO  ng, imgN3
            $("#n3,#imgN3").unbind("click");
            $("#n3,#imgN3").click(function() {
                var tr = cms.getModulesCode("#n3,#imgN3");
                if (tr != "") {
                    $("#n3,#imgN3").attr("href", tr).attr("target", "main_view");
                    setTimeout(function() {
                        $("#n3,#imgN3").attr("href", "#").attr("target", "main_view");
                    }, 100);
                } else {
                    url = cms.getVideoPath() + "?locale=" + locale + "&account=" + account + "&sn=" + sn + "&token=" + token + "&uid=" + uid + "&return='" + frmUrl + "'";
                    $("#n3,#imgN3").attr("href", url).attr("target", "bgvideo_view");
                }
            });
            //}}}

            //{{{ TODO  j3
            $("#j3").attr("href", "mgcasino-slotgame.html#electronicFrm");
            var sd = cms.getWebSn();
            if ("DEMO" != sd && "" != sd) {
                var trr2 = cms.getModulesCode("#n26");
                if (trr2 != "") {
                    $(".indexbbin").bind("click", function() {
                        $(".indexbbin").attr("href", trr2).attr("target", "main_view");
                        setTimeout(function() {
                            $(".indexbbin").attr("href", "#").attr("target", "main_view");
                        }, 100);
                    });
                } else {
                    $.cloudCall({
                        method: "thirdparty.bbin.game.play.url",
                        isLoading: false,
                        params: {
                            sessionId: cms.getToken(),
                            lang: "1",
                            type: "4"
                        },
                        success: function(obj) {
                            if (obj.error == null) {
                                var result = obj.result;
                                $(".indexbbin").attr("href", result).attr("target", "_block");
                            }
                        }
                    });
                }
            }
            //}}}

            //{{{ n4
            $("#n4").bind("mouseenter", function() {
                var sd = cms.getWebSn();
                if ("DEMO" != sd && "" != sd) {
                    //{{{ TODO  捕魚天下
                    var tr = cms.getModulesCode("#n28");
                    if (tr != "") {
                        $("#j5").bind("click", function() {
                            $("#j5").attr("href", tr).attr("target", "main_view");
                            setTimeout(function() {
                                $("#j5").attr("href", "#").attr("target", "main_view");
                            }, 100);
                        });
                    } else {
                        bgPage.setYtxFishGame();
                    }
                    //}}}

                    //{{{ TODO  bbin
                    var tr2 = cms.getModulesCode("#n26");
                    if (tr2 != "") {
                        $("#j6").bind("click", function() {
                            $("#j6").attr("href", tr2).attr("target", "main_view");
                            setTimeout(function() {
                                $("#j6").attr("href", "#").attr("target", "main_view");
                            }, 100);
                        });
                    } else {
                        $.cloudCall({
                            method: "thirdparty.bbin.game.play.url",
                            isLoading: false,
                            params: {
                                sessionId: cms.getToken(),
                                lang: "1",
                                type: "4"
                            },
                            success: function(obj) {
                                if (obj.error == null) {
                                    var result = obj.result;
                                    $("#j6").attr("href", result).attr("target", "_block");
                                }
                            }
                        });
                    }
                    //}}}

                    //{{{ TODO  AG Now available
                    var tr3 = cms.getModulesCode("#n25");
                    if (tr3 != "") {
                        $("#j7").bind("click", function() {
                            $("#j7").attr("href", tr3).attr("target", "main_view");
                            setTimeout(function() {
                                $("#j7").attr("href", "#").attr("target", "main_view");
                            }, 100);
                        });
                    } else {
                        $.cloudCall({
                            method: "thirdparty.ag.game.video.url",
                            isLoading: false,
                            params: {
                                sessionId: cms.getToken(),
                                dm: window.location.host,
                                lang: 1,
                                gameType: "8"
                            },
                            success: function(data) {
                                if (data.error == null && data.result != null) {
                                    var escape2Html = function(str) {
                                        var arrEntities = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
                                        return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function(all, t) {
                                            return arrEntities[t];
                                        });
                                    }
                                    var url = escape2Html(data.result);
                                    $("#j7").attr("href", url).attr("target", "_block");
                                }
                            }
                        });
                    }
                    //}}}

                    //{{{ TODO  AG cannot find
                    var ttr3 = cms.getModulesCode("#n25");
                    if (ttr3 != '') {
                        $(".indexag").bind("click", function() {
                            $(".indexag").attr("href", tr3).attr("target", "main_view");
                            setTimeout(function() {
                                $(".indexag").attr("href", "#").attr("target", "main_view");
                            }, 100);
                        });
                    } else {
                        $.cloudCall({
                            method: "thirdparty.ag.game.video.url",
                            isLoading: false,
                            params: {
                                sessionId: cms.getToken(),
                                dm: window.location.host,
                                lang: 1,
                                gameType: "8"
                            },
                            success: function(data) {
                                if (data.error == null && data.result != null) {
                                    var escape2Html = function(str) {
                                        var arrEntities = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
                                        return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function(all, t) {
                                            return arrEntities[t];
                                        });
                                    }
                                    var url = escape2Html(data.result);
                                    $(".indexag").attr("href", url).attr("target", "_block");
                                }
                            }
                        });
                    }
                    //}}}
                }
            });
            //}}}
        }
    },
    //}}}
    //{{{ TODO  Reset top left
    resetTopLeft: function() {
        $('.topnav')
            .html('<ul><li><a href="javascript:void(0);" onclick="bgPage.setHome(this)">设为首页</a></li><li><a href="javascript:void(0);" onclick="bgPage.setFavorite()">添加收藏</a></li><li><a href="tutorial.html" target="_blank">新手教程</a></li><li><div class="dropdown language"><i></i><button type="button" class="btn dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown"> 简体中文 <span class="caret"></span></button><ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1"><li role="presentation"> <a role="menuitem" tabindex="-1" href="#"><i class="fanti"></i>繁体中文</a></li><li role="presentation"> <a role="menuitem" tabindex="-1" href="#"><i class="english"></i>English</a></li></ul></div></li><li class="user-time"><i class="icon-time"></i><span>美东时间：2016/07/11 22:52:38</span></li></ul>');
    },
    getMobileUrl: function() {
        var token = cms.getToken();
        var sn = cms.getWebSn();
        var webimg = $("#btnmobile");
        var baseUrl = cms.getCloudPath() + "api.do?pa=qrcode.generator&data=";
        var dm = document.domain;
        if (null != token && "" != token) {
            url = "http://m." + dm + "/?sn=" + sn + "&token=" + token + "&device=mobile&qr=true";
            webimg.attr("src", baseUrl + url);
        } else {
            url = "http://m." + dm + "/?sn=" + sn + "&device=mobile&qr=true"
            webimg.attr("src", baseUrl + url);
        }

        $(".mobilelink").attr("href", "http://m." + dm + "/?sn=" + sn);
        $("#mobilelink").attr("href", "http://m." + dm + "/?sn=" + sn).html("http://m." + dm + "/?sn=" + sn);
    },
    //}}}
    //{{{ TODO  Set maintenance
    setMaintenance: function(target, modulesCode) { //維護
        $(target).click(function() {
            var tr = cms.getModulesCode(modulesCode),
                ia = this;
            if (tr != "") {
                $(ia).attr("href", 'javascript:void(0);');
                indexliveVideo.getMaintenance(tr, ia);
            }
        });
    },
    //}}}
    //{{{ TODO  Set Fish game
    setYtxFishGame: function() {
        $('#j5,.indexfish').unbind('click');
        $("#j5,.indexfish").attr("href", 'javascript:void(0)');
        if ("" == cms.getToken() || bgPage.isLoginTrial()) return;
        $.cloudCall({
            method: "thirdparty.ytx.game.url",
            isLoading: false,
            params: {
                sessionId: cms.getToken(),
                lang: "zh-CN",
                gameType: "101"
            },
            success: function(obj) {
                if (obj.error == null) {
                    var result = obj.result;
                    $("#j5,.indexfish").attr("href", result).attr("target", "_block");
                }
            }
        });
    },
    //}}}
    //{{{ TODO  Set AG fish
    setAGFishGame: function() { //AG捕魚王
        $('.agFish').unbind('click');
        $(".agFish").attr("href", 'javascript:void(0)');
        if ("" == cms.getToken() || bgPage.isLoginTrial()) return;
        $.cloudCall({
            method: "thirdparty.ag.game.video.url",
            isLoading: false,
            params: {
                sessionId: cms.getToken(),
                dm: document.domain,
                lang: 1,
                gameType: 6
            },
            success: function(obj) {
                if (obj.error == null) {
                    var result = obj.result;
                    $(".agFish").attr("href", result).attr("target", "_block");
                }
            }
        });
        bgPage.setMaintenance('.agFish', '#n25');
    },
    //}}}
    //{{{ TODO  Set bg fish game
    setBGFishGame: function() { //BG捕魚大師
        $('.bgFish').unbind('click');
        $(".bgFish").attr("href", 'javascript:void(0)');
        if ("" == cms.getToken()) return;
        $.cloudCall({
            method: "thirdparty.bgfish.game.url",
            isLoading: false,
            params: {
                sessionId: cms.getToken(),
                lang: "zh-CN",
                gameType: "101"
            },
            success: function(obj) {
                if (obj.error == null) {
                    var result = obj.result;
                    $(".bgFish").attr("href", result).attr("target", "_block");
                } else { //BG捕魚還沒上到生產
                    $('.bgFish').unbind('click');
                    $('.bgFish').click(function() {
                        JsMsg.warnMsg('即将上线，敬请期待！');
                    });
                }
            }
        });
        bgPage.setMaintenance('.bgFish', '#n38');
    },
    //}}}
    //{{{ TODO  Conversion modal
    openConversionModal: function(_id, _url, _target) {
        $('#thirdPartyChkId').val(_id);
        $('#thirdPartyUrl').val(_url);
        $('#thirdPartyTarget').val(_target);
        $('#thConversion').modal('show');
    },
    //}}}
    //{{{ TODO  Reset menu
    resetMenu: function(id) {
        menucode = id;
        var nid = id == null ? null : id.split(',')[0];
        var jid = id == null ? null : id.split(',')[1];
        var customUrl = cms.getCookie("CustomerUrl");
        var menuStr = '';

        menuStr += '<li><a id="n_index" href="index.html">首页</a></li>\
                  <li><a id="n2" href="live-video.html">真人娱乐<i></i></a></li>\
                  <li><a id="n3" href="javascript:void(0);">BG视讯</a></li>\
                  <li><a id="n5" class="gotoPlay" href="#" data-id="0">彩票游戏<i></i></a></li>\
                  <li><a id="n4" href="javascript:void(0);">电子游戏<i></i></a></li>\
                  <li><a id="n4_bgfish" href="javascript:void(0);">捕鱼达人<i></i></a></li>\
                  <li><a id="n6" href="sport.html">体育投注<i></i></a></li>\
                  <li><a href="promotion.html">优惠活动</a></li>\
                  <li><a href="phone.html">手机投注</a></li>\
                  <li><a href="agency-agreement.html">合营代理</a></li>';

        $('.navul').html(menuStr);
        $('.nav').find('a').removeClass('backg');

        if (nid != null) {
            $(nid).parent().removeClass('backg').addClass('backg');
        }
        if (jid != null) {
            $(jid).parent().removeClass('backg').addClass('backg');
        }

        var url = window.location.pathname;
        var file = url.substring(url.lastIndexOf("/") + 1);
        cms.setCookie("CurrentUrl", file, 1);

        if ("" == cms.getToken()) {
            $("#n3").click(function() {
                JsMsg.warnMsg("请先登录!", { code: "2264" });
            });
        }

        bgPage.setYtxFishGame();
        bgPage.setAGFishGame();
        // bgPage.setBGFishGame();

        $("#j5,#j6,#j7,.indexbbin,.indexag,.indexfish,.bgFish,.agFish").unbind("click");
        if ("" == cms.getToken()) {
            $("#j5,#j6,#j7,.indexbbin,.indexag,.indexfish,.bgFish,.agFish").bind("click", function() {
                JsMsg.warnMsg("请先登录!", { code: "2264" });
            });
        }

        if (bgPage.isLoginTrial()) {
            $("#j5,#j6,#j7,.indexbbin,.indexag,.indexfish,.agFisg").bind("click", function() {
                $(this).attr('href', 'javascript:void(0)');
                JsMsg.warnMsg("请使用正式帐号登录!");
            });
        }

        var subMenu = {
            status: false,
            open: function(m) {
                m.stop().slideDown(200);
                subMenu.status = true;
            },
            close: function(m) {
                m.stop().slideUp(200);
                subMenu.status = false;
            }
        }

        $('.navul>li').on('mouseenter', function(e) {
            var iu = ($(this).index() + 1);
            var o = $('#list' + iu);
            subMenu.open(o);

            $(this).off('mouseleave');
            $(this).on('mouseleave', function() {
                setTimeout(function() {
                    if (subMenu.status == true) {
                        subMenu.close(o);
                        subMenu.status = true;
                    }
                }, 100);
            });

            o.off('mouseleave');
            o.on('mouseleave', function() {
                subMenu.close($(this));
            });

            o.off('mouseenter');
            o.on('mouseenter', function() {
                subMenu.status = false;
                //  關閉100 ms執行，滑鼠在100ms內移置另一個選單，會重覆出現
                //  所以 當 div#list其中一個有mouseenter ，就關閉其他的div#list
                $('div[id^="list"]').each(function(i, el) {
                    var _index = this.id.substring(4);
                    if (_index != iu) subMenu.close($('#list' + _index));
                });
            });
        });

        $.ajax({
            type: "GET",
            url: "menu.html",
            success: function(data) {
                //  TODO  GET menu template
                $('body').append(data);

                bgPage.setYtxFishGame();
                bgPage.setAGFishGame();
                // bgPage.setBGFishGame();
                bindTool.all();

                //{{{ TODO  Login confirm.
                if ("" == cms.getToken()) {
                    $("#j5,#j6,#j7,.agFish,.bgFish,.indexfish").bind("click", function() {
                        JsMsg.warnMsg("请先登录!");
                    });
                }
                //}}}

                //{{{ TODO  Normal User login confirm.
                if (bgPage.isLoginTrial()) {
                    $("#j5,#j6,#j7,.agFish,.indexfish").bind("click", function() {
                        $(this).attr('href', 'javascript:void(0)');
                        JsMsg.warnMsg("请使用正式帐号登录!");
                    });
                }
                //}}}

                bgPage.setSportMenu();
                // bgPage.setSportLink();

                var livemenuVideo = {
                    //{{{ TODO  Init data
                    initData: function() {
                        var controls = $(".navlive").find("a");
                        $.cloudCall({
                            method: 'thirdparty.list',
                            async: true,
                            params: {
                                sessionId: cms.getToken()
                            },
                            success: function(data) {
                                thirdPartyList = data.result;
                            }
                        });
                        $.each(controls, function(i) {
                            var ia = controls[i];
                            var id = ia.id;
                            $(ia).unbind("click");
                            if ("" == cms.getToken()) {
                                $(ia).click(function() {
                                    $(ia).attr("href", "javascript:void(0);");
                                    JsMsg.warnMsg("请先登录!", { code: "2264" });
                                });
                            } else if (bgPage.isLoginTrial() && id != "mvideo1") {
                                $(ia).click(function() {
                                    $(ia).attr("href", "javascript:void(0);");
                                    JsMsg.warnMsg("请使用正式帐号登录!");
                                });
                            } else {
                                $(ia).click(function(e) {
                                    var id = this.id;
                                    var _linkEl = $(e.target).parent();
                                    if ("" == cms.getToken()) {
                                        $(ia).attr("href", "javascript:void(0);");
                                        JsMsg.warnMsg("请先登录!", { code: "2264" });
                                        return false;
                                    }
                                    if (id != 'mvideo1') {
                                        e.preventDefault();
                                        bgPage.openConversionModal(id, _linkEl.attr('href'), _linkEl.attr('target'));
                                    }
                                });

                                if (id == "mvideo1") {
                                    livemenuVideo.openBgVideo(ia);
                                } else if (id == "mvideo5") {
                                    livemenuVideo.openBbinVideo(ia);
                                } else if (id == "mvideo3") {
                                    livemenuVideo.openGdVideo(ia);
                                } else if (id == "mvideo4") {
                                    livemenuVideo.openAgVideo(ia);
                                } else if (id == "mvideo6") {
                                    livemenuVideo.openAbVideo(ia);
                                } else if (id == "mvideo9") {
                                    livemenuVideo.openLeboVideo(ia);
                                } else if (id == "mvideo7") {
                                    livemenuVideo.openPtVideo(ia);
                                } else if (id == "mvideo8") {
                                    livemenuVideo.openOgVideo(ia);
                                } else if (id == "mvideo10") {
                                    livemenuVideo.openSunbetVideo(ia);
                                } else if (id == "mvideo11") {
                                    livemenuVideo.openDGVideo(ia);
                                } else if (id == "mvideo12") {
                                    livemenuVideo.openGcVideo(ia);
                                } else if (id == "mvideoMG") {
                                    livemenuVideo.openMGVideo(ia);
                                }
                            }
                        });
                    },
                    //}}}
                    //{{{ TODO  Open Bg video
                    openBgVideo: function(ia) {
                        var tr = cms.getModulesCode("#n3");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            var locale = 'zh_CN';
                            var model = cms.getLoginModel();
                            var an = model.account;
                            if (an === undefined) {
                                an = cms.getCookie("account");
                            }
                            var account = an;
                            var path = cms.getWebPath();
                            var token = cms.getToken();
                            var sn = "";
                            if (token.length > 0) {
                                sn = token.substring(0, 4);
                            } else {
                                sn = cms.getWebSn();
                            }
                            var uid = cms.getUid();
                            var frmUrl = "http://" + window.top.location.host + "/web/index.html";
                            url = cms.getVideoPath() + "?locale=" + locale + "&account=" + account + "&sn=" + sn + "&token=" + token + "&uid=" + uid + "&return='" + frmUrl + "'";
                            $(ia).attr("href", url).attr("target", "bgvideo_view");
                        }
                    },
                    //}}}
                    //{{{ TODO  Open bbin video
                    openBbinVideo: function(ia) {
                        var tr = cms.getModulesCode("#n26");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            $.cloudCall({
                                method: "thirdparty.bbin.game.play.url",
                                async: true,
                                params: {
                                    sessionId: cms.getToken(),
                                    lang: 1,
                                    type: 1
                                },
                                success: function(data) {
                                    if (data.error == null && data.result != null) {
                                        url = livemenuVideo.escape2Html(data.result);
                                        $(ia).attr("href", url).attr("target", "bbinvideo_view");
                                    }
                                }
                            });
                        }
                    },
                    //}}}
                    //{{{ TODO  Open pt video
                    openPtVideo: function(ia) {
                        var tr = cms.getModulesCode("#n24");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            var lan = "ZH-CN";
                            var url = livemenuVideo.escape2Html("http://game.dyvip888.com/pt-play.html?gameCode=bal&sessionId=" + cms.getToken() + "&lan=" + lan);
                            $(ia).attr("href", url).attr("target", "ptvideo_view");

                        }
                    },
                    //}}}
                    //{{{ TODO  Open gd video
                    openGdVideo: function(ia) {
                        var tr = cms.getModulesCode("#n22");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            $.cloudCall({
                                method: "thirdparty.gd.game.video.url",
                                async: true,
                                params: {
                                    sessionId: cms.getToken()
                                },
                                success: function(data) {
                                    if (data.error == null && data.result != null) {
                                        url = livemenuVideo.escape2Html(data.result);
                                        $(ia).attr("href", url).attr("target", "gdvideo_view");
                                    }
                                }
                            });
                        }
                    },
                    //}}}
                    //{{{ TODO  Open sunbet video
                    openSunbetVideo: function(ia) {
                        var tr = cms.getModulesCode("#n29");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            $.cloudCall({
                                method: "thirdparty.sunbet.game.video.url",
                                async: true,
                                params: {
                                    sessionId: cms.getToken(),
                                    lang: 1,
                                    type: 0
                                },
                                success: function(data) {
                                    if (data.error == null && data.result != null) {
                                        url = livemenuVideo.escape2Html(data.result);
                                        $(ia).attr("href", url).attr("target", "sunbetvideo_view");
                                    }
                                }
                            });
                        }
                    },
                    //}}}
                    //{{{ TODO  Open og video
                    openOgVideo: function(ia) {
                        var tr = cms.getModulesCode("#n30");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            $.cloudCall({
                                method: "thirdparty.og.game.video.url",
                                async: true,
                                params: {
                                    sessionId: cms.getToken(),
                                    dm: window.location.host,
                                    lang: 1,
                                    gameType: "1"
                                },
                                success: function(data) {
                                    if (data.error == null && data.result != null) {
                                        url = livemenuVideo.escape2Html(data.result);
                                        $(ia).attr("href", url).attr("target", "sunbetvideo_view");
                                    }
                                }
                            });
                        }
                    },
                    //}}}
                    //{{{ TODO  Open ab video
                    openAbVideo: function(ia) {
                        var url = "";
                        var tr = cms.getModulesCode("#n23");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {

                            $.cloudCall({
                                method: "thirdparty.ab.game.video.url",
                                async: true,
                                params: {
                                    sessionId: cms.getToken()
                                },
                                success: function(data) {
                                    if (data.error == null && data.result != null) {
                                        url = livemenuVideo.escape2Html(data.result);
                                        $(ia).attr("href", url).attr("target", "abvideo_view");
                                    }
                                }
                            });

                            setInterval(function() {
                                $.cloudCall({
                                    method: "thirdparty.ab.game.video.url",
                                    async: true,
                                    isLoading: false,
                                    params: {
                                        sessionId: cms.getToken()
                                    },
                                    success: function(data) {
                                        if (data.error == null && data.result != null) {
                                            url = livemenuVideo.escape2Html(data.result);
                                            $(ia).attr("href", url).attr("target", "abvideo_view");
                                        }
                                    }
                                });
                            }, 30000);

                        }

                    },
                    //}}}
                    //{{{ TODO  Open LEBO video
                    openLeboVideo: function(ia) {
                        var url = "";
                        var lan = "";
                        var tr = cms.getModulesCode("#n27");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            $.cloudCall({
                                method: "thirdparty.lb.game.video.url",
                                async: true,
                                params: {
                                    sessionId: cms.getToken(),
                                    lang: 1
                                },
                                success: function(data) {
                                    if (data.error == null && data.result != null) {
                                        url = livemenuVideo.escape2Html(data.result);
                                        window.top.lbVideo = url;
                                        $(ia).attr("href", url).attr("target", "lbvideo_view");
                                    } else {
                                        if (window.top.lbVideo) $(ia).attr("href", window.top.lbVideo).attr("target", "lbvideo_view");
                                    }
                                }
                            });
                        }
                    },
                    //}}}
                    //{{{ TODO  Open ag video
                    openAgVideo: function(ia) {
                        var url = "";
                        var lan = "";
                        var tr = cms.getModulesCode("#n25");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            $.cloudCall({
                                method: "thirdparty.ag.game.video.url",
                                async: true,
                                params: {
                                    sessionId: cms.getToken(),
                                    dm: window.location.host,
                                    lang: 1,
                                    gameType: "0"
                                },
                                success: function(data) {
                                    if (data.error == null && data.result != null) {
                                        url = livemenuVideo.escape2Html(data.result);
                                        $(ia).attr("href", url).attr("target", "agvideo_view");
                                    }
                                }
                            });
                        }
                    },
                    //}}}
                    //{{{ TODO  Open dg video
                    openDGVideo: function(ia) {
                        var url = "";
                        var lan = "";
                        var tr = cms.getModulesCode("#n31");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            $.cloudCall({
                                method: "thirdparty.dg.game.video.url",
                                async: true,
                                params: {
                                    sessionId: cms.getToken(),
                                    lang: "cn",
                                    gameType: "1"
                                },
                                success: function(data) {
                                    if (data.error == null && data.result != null) {
                                        url = livemenuVideo.escape2Html(data.result);
                                        $(ia).attr("href", url).attr("target", "dgvideo_view");
                                    }
                                }
                            });
                        }
                    },
                    //}}}
                    //{{{ TODO  Open gc video
                    openGcVideo: function(ia) {
                        var tr = cms.getModulesCode("#n39");
                        if (tr != "") {
                            livemenuVideo.getMaintenance(tr, ia);
                        } else {
                            $.cloudCall({
                                method: "thirdparty.gc.game.play.url",
                                async: true,
                                params: {
                                    sessionId: cms.getToken(),
                                    lang: 1,
                                    type: 1
                                },
                                success: function(data) {
                                    if (data.error == null && data.result != null) {
                                        url = livemenuVideo.escape2Html(data.result);
                                        $(ia).attr("href", url).attr("target", "gcvideo_view");
                                    }
                                }
                            });
                        }
                    },
                    //}}}
                    //{{{ TODO  Open mg video
                    openMGVideo: function() {
                        $.cloudCall({
                            method: "thirdparty.mg.game.url",
                            isLoading: false,
                            params: {
                                sessionId: cms.getToken()
                            },
                            success: function(data) {
                                if (data.error == null && data.result != null) {
                                    var getHidden = function(n, v) {
                                        var hiddenField = document.createElement("input");
                                        hiddenField.setAttribute("name", n);
                                        hiddenField.setAttribute("value", v);
                                        hiddenField.setAttribute("type", "hidden");
                                        return hiddenField;
                                    };

                                    var s1 = data.result.st6;
                                    var s2 = data.result.st8;
                                    var ul = "zh";
                                    var action = "https://webservice.basestatic.net/ETILandingPage/index.aspx";
                                    var form = document.createElement("form");
                                    form.setAttribute("method", "post");
                                    form.setAttribute("action", action);
                                    form.setAttribute("target", "_blank");
                                    form.setAttribute("style", "display:none");
                                    form.setAttribute("id", "formMGVideo");

                                    form.appendChild(getHidden('LoginName', s1));
                                    form.appendChild(getHidden('Password', s2));
                                    form.appendChild(getHidden('UL', 'zh-cn'));
                                    form.appendChild(getHidden('CasinoID', '16113'));
                                    form.appendChild(getHidden('ClientID', '6'));
                                    form.appendChild(getHidden('BetProfileID', 'MobilePostLogin'));
                                    form.appendChild(getHidden('StartingTab', 'SPSicbo'));
                                    form.appendChild(getHidden('BrandID', 'igaming'));
                                    form.appendChild(getHidden('altProxy', 'TNG'));
                                    form.appendChild(getHidden('LogoutRedirect', document.domain));

                                    document.body.appendChild(form);
                                    //  form.submit();
                                } else {
                                    //  JsMsg.errorObjMsg(data.error);
                                }
                            }
                        });
                    },
                    //}}}
                    //{{{ TODO  Open maintenance
                    getMaintenance: function(tr, ia) {
                        var frmUrl = "http://" + window.top.location.host + tr;
                        $(ia).click(function() {
                            $(ia).attr("href", frmUrl).attr("target", "main_view");
                            setTimeout(function() {
                                $(ia).attr("href", "#").attr("target", "main_view");
                            }, 100);
                        });
                    },
                    //}}}
                    //{{{ TODO  Open  Escape2Html
                    escape2Html: function(str) {
                            var arrEntities = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
                            return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function(all, t) {
                                return arrEntities[t];
                            });
                        }
                        //}}}
                };

                //  TODO  真人視訊 自動連結
                livemenuVideo.initData();

            }
        });
    },
    //}}}
    //{{{ TODO  Load sn new notice data
    loadSnNewNoticeData: function() {
        var dtoStr = "";
        $.cloudCall({
            method: "sn.notice.new.query",
            async: true,
            isLoading: false,
            params: {
                sessionId: cms.getToken(),
                popupFlag: "Y"
            },
            success: function(obj) {
                $("#emptyData").hide();
                if (obj.error == null && obj.result != null) {
                    var datas = obj.result;
                    var dtoList = datas.items;
                    $("#dtoList").html("");
                    $.each(dtoList, function(i) {
                        var entity = dtoList[i];
                        var conStr = "";
                        var content = JSON.parse(entity.content);
                        if (content != undefined) {
                            var dto = content;
                            conStr = dto.contentZh;
                            // dto.contentTw dto.contentEn
                        }
                        var ckey = "noticeUser" + entity.noticePk;
                        var cookieObj = cms.getCookie(ckey);
                        if (null != cookieObj) {

                        } else {
                            dtoStr += "<tr><td align='center' style='padding:10px;'>" + entity.createTime + "</td><td align='left' style='padding:10px;'>" + conStr + "</td></tr>";
                            cms.setCookieTime(ckey, entity.endTime);
                        }
                    });

                    if (dtoStr != '') {
                        bgPage
                            .showNoticeInfo(
                                '公告信息',
                                '<div class=\'content-two\' style=\'margin-top:10px;\'><table class=\'table layer-body\' width=\'100%\' border=\'0\' cellspacing=\'0\' cellpadding=\'0\'><tr><th scope=\'col\' width=\'25%\' style=\'text-align: center;\'>日期</th><th scope=\'col\' width=\'60%\' style=\'text-align: center;\'>内容</th></tr><tbody id=\'dtoList\'>' + dtoStr + '</tbody></tr></table></div>',
                                700, 550);
                    }
                } else {
                    JsMsg.errorMsg(obj.error);
                }
            }
        });
        return dtoStr;
    },
    //}}}
    //{{{ TODO  Show notice info
    showNoticeInfo: function(title, msg, w, h) {
        var titleheight = "22px"; // 提示窗口标题高度
        var bordercolor = "#666699"; // 提示窗口的边框颜色
        var titlecolor = "#FFFFFF"; // 提示窗口的标题颜色
        var titlebgcolor = "#666699"; // 提示窗口的标题背景色
        var bgcolor = "#FFFFFF"; // 提示内容的背景色

        var iWidth = document.documentElement.clientWidth;
        var iHeight = document.documentElement.clientHeight;
        var bgObj = document.createElement("div");
        bgObj.style.cssText = "position:absolute;left:0px;top:0px;width:" + iWidth + "px;height:" + Math.max(document.body.clientHeight, iHeight) + "px;filter:Alpha(Opacity=30);opacity:0.3;background-color:#000000;z-index:9001;";
        document.body.appendChild(bgObj);

        var msgObj = document.createElement("div");
        msgObj.style.cssText = "position:absolute;font:11px '宋体';top:" + (iHeight - h) / 2 + "px;left:" + (iWidth - w) / 2 + "px;width:" + w + "px;height:" + h + "px;text-align:center;border:1px solid " + bordercolor + ";background-color:" + bgcolor + ";padding:1px;line-height:22px;z-index:9002;overflow-y: auto;";
        document.body.appendChild(msgObj);

        var table = document.createElement("table");
        msgObj.appendChild(table);
        table.style.cssText = "margin:0px;border:0px;padding:0px;word-break: break-all;";
        table.cellSpacing = 0;
        var tr = table.insertRow(-1);
        var titleBar = tr.insertCell(-1);

        titleBar.style.paddingLeft = "10px";
        titleBar.style.width = "98%";
        titleBar.style.borderTopLeftRadius = "0px";
        titleBar.style.borderTopRightRadius = "0px";
        var ltitle = "layer-title";
        titleBar.className = ltitle;
        titleBar.innerHTML = title;
        var moveX = 0;
        var moveY = 0;
        var moveTop = 0;
        var moveLeft = 0;
        var moveable = false;
        var docMouseMoveEvent = document.onmousemove;
        var docMouseUpEvent = document.onmouseup;
        titleBar.onmousedown = function() {
            var evt = getEvent();
            moveable = true;
            moveX = evt.clientX;
            moveY = evt.clientY;
            moveTop = parseInt(msgObj.style.top);
            moveLeft = parseInt(msgObj.style.left);

            document.onmousemove = function() {
                if (moveable) {
                    var evt = getEvent();
                    var x = moveLeft + evt.clientX - moveX;
                    var y = moveTop + evt.clientY - moveY;
                    if (x > 0 && (x + w < iWidth) && y > 0 && (y + h < iHeight)) {
                        msgObj.style.left = x + "px";
                        msgObj.style.top = y + "px";
                    }
                }
            };
            document.onmouseup = function() {
                if (moveable) {
                    document.onmousemove = docMouseMoveEvent;
                    document.onmouseup = docMouseUpEvent;
                    moveable = false;
                    moveX = 0;
                    moveY = 0;
                    moveTop = 0;
                    moveLeft = 0;
                }
            };
        }

        var closeBtn = tr.insertCell(-1);
        closeBtn.className = ltitle;
        closeBtn.style.borderTopLeftRadius = "0px";
        closeBtn.style.borderTopRightRadius = "0px";
        closeBtn.innerHTML = "<span style='font-size:15pt; color:" + titlecolor + ";float:right;padding-right: 10px;cursor: pointer;'>×</span>";
        closeBtn.onclick = function() {
            document.body.removeChild(bgObj);
            document.body.removeChild(msgObj);
        }
        var msgBox = table.insertRow(-1).insertCell(-1);
        msgBox.style.cssText = "font:10pt '宋体';";
        msgBox.colSpan = 2;
        msgBox.innerHTML = msg;

        // 获得事件Event对象，用于兼容IE和FireFox
        function getEvent() {
            return window.event || arguments.callee.caller.arguments[0];
        }

    },
    //}}}
    //{{{ TODO  Lottery url
    getLotteryUrl: function() {
        var host = window.location.host;
        var webPathStr = cms.getCookie('WebSn2');
        if (host.indexOf('bg866') !== -1) {
            host = 'http://lt.bgvip77.com/common/';
        } else if (host.indexOf('localhost') !== -1 || host.indexOf('10.37') !== -1 || host.indexOf('bgbet3') !== -1 || host.indexOf('bg1207') !== -1) {
            host = 'http://gi506.bgbet3.com/common/';
        } else {
            host = 'http://lt.bgvip77.com/common/';
        }
        return host;
    },
    //}}}
    //{{{ TODO  window open
    winopen: function(url, name, width, height) {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");
        var isIE11 = !!navigator.userAgent.match(/Trident\/7\./);
        var heightFlag = (msie === -1 && !isIE11) ? 0 : 80;
        var widthFlag = (msie === -1 && !isIE11) ? 0 : 20;
        var newwin = window.open();
        newwin.location = url;
        newwin.moveTo(0, 0);
        windows[name] = newwin;
    },
    //}}}
    //{{{ TODO  Lottery object
    lotteryObj: {
        1: { id: 1, url: '/gameFtc/fcsd' }, //  福彩3D
        2: { id: 2, url: '/gameTcps/tcps' }, //  体彩P3
        12: { id: 5, url: '/game/cqssc' }, //  重庆时时彩
        15: { id: 55, url: '/game/hljssc' }, //  黑龙江时时彩
        14: { id: 7, url: '/game/xjssc' }, //  新疆时时彩
        11: { id: 4, url: '/game/tjssc' }, //  天津时时彩
        51: { id: 41, url: '/pcdd/index' }, //  PC蛋蛋
        49: { id: 18, url: '/sixGame/index' }, //  六合彩
        10: { id: 3, url: '/gameSsl/index' }, //  上海时时乐
        24: { id: 9, url: '/game/bjsc' }, //  北京赛车
        26: { id: 11, url: '/gameKs/ahks' }, //  安徽快三
        25: { id: 10, url: '/gameKs/jsks' }, //  江苏快三
        31: { id: 15, url: '/elevenGame/gd' }, //  广东11选5
        30: { id: 14, url: '/elevenGame/jx' }, //  江西11选5
        32: { id: 13, url: '/elevenGame/sh' }, //  上海11选5
        28: { id: 12, url: '/elevenGame/sd' } //  山东11选5
    }
    //}}}
};

function gotoVr(){
  var _win = window.open('about:blank');
  $.cloudCall({
    method: "tp.vr.game.url",
    async: true,
    isLoading: false,
    params: {
      sessionId: cms.getToken(),
      lobbyUrl : document.domain
    },
    success: function(obj) {
      if (obj.error == null && obj.result != null) {
        _win.location.href = obj.result;
      } else {
        JsMsg.errorObjMsg(obj.error);
      }
    }
  });
};

//{{{ TODO  Document ready
$(document).ready(function() {
    //  TODO  彩票改到新版大廳
    $('body').delegate('.gotoPlay', 'click', function(e) {
        e.preventDefault();
        var _this = $(e.target);
        var _id = _this.data('id');
        var _param = _this.data('param');
        if(_id == 'vr') {
          if ("" == cms.getToken()) {
              JsMsg.warnMsg("请先登录!");
              return;
          } else if (cms.getToken().substring(0,4) == 'DEMO') {
              JsMsg.warnMsg("请使用正式帐号登录!");
              return;
          } else {
            // gotoVr();
            return;
          }
          return;
        }
        var _url = (_id == 0) ? '&_isOpenWin=lottery' : ('#_url_=' + bgPage.lotteryObj[_id].url + (_param ? '?' + _param + "&_isOpenWin=lottery" : '?_isOpenWin=lottery'));
        _url = '&domain=' + window.location.host + '&skins=' + cms.getWebPath() + _url;

        if (cms.getCookie("Token")) {
            _url = '&token=' + cms.getCookie("Token") + _url;
        }

        var _webSn2 = cms.getCookie('WebSn2') ? cms.getCookie('WebSn2') : '';

        if (_webSn2) {
            _url = bgPage.getLotteryUrl() + '?sn=' + cms.getCookie('WebSn2') + '&spm=1' + _url;
        } else {
            _url = bgPage.getLotteryUrl() + '?sn=' + cms.getWebSn() + '&spm=1' + _url;
        }

        bgPage.winopen(_url, 'lotteryLobby');
        cms.delCookie('lottoryToken');
    });

    //  TODO  轉帳畫面開啟時設定資料
    $('#thConversion').on('show.bs.modal', function(e) {
        $('#amount').val('');
        $('#thirdpartyBalance > span').text(0);
        $('#mainBalance > span').text(0);

        var _modalEl = $(e.delegateTarget);
        var _chkId = _modalEl.find('#thirdPartyChkId').val();
        var _conversionId = _modalEl.find('#thirdPartyId');
        if (_chkId.indexOf('mvideo') != -1) {
            //  TODO  視訊
            switch (_chkId) {
                case 'mvideo5':
                    _conversionId.val(4);
                    break;
                case 'mvideo3':
                    _conversionId.val(9);
                    break;
                case 'mvideo4':
                    _conversionId.val(5);
                    break;
                case 'mvideo6':
                    _conversionId.val(10);
                    break;
                case 'mvideo9':
                    _conversionId.val(1);
                    break;
                case 'mvideo7':
                    _conversionId.val(11);
                    break;
                case 'mvideo8':
                    _conversionId.val(6);
                    break;
                case 'mvideo10':
                    _conversionId.val(14);
                    break;
                case 'mvideo11':
                    _conversionId.val(15);
                    break;
                case 'mvideo12':
                    _conversionId.val();
                    break;
                case 'mvideoMG':
                    _conversionId.val(2);
                    break;
            }
        } else {
            //  TODO  電遊 & 捕魚
            if (isNaN(_chkId)) {
                if (_chkId == 'j5') {
                    _conversionId.val(12);
                }
            } else {
                _conversionId.val(_chkId);
            }
        }

        //  TODO  平台名稱
        if (_chkId == 24) {
            $('#thirdPartyName').text('UG');
        } else if (_chkId == 22) {
            $('#thirdPartyName').text('皇冠(3 Sing)');
        } else {
            $.each(thirdPartyList, function(_key, _item) {
                if (_conversionId.val() == _item.id) {
                    $('#thirdPartyName').text(_item.name);
                }
            });
        }

        //  TODO  若沒有在上述的名稱中則開始配對特殊組合
        if ('20' == _conversionId.val()) {
            $('#thirdPartyName').text('QT电游');
        }

        //  TODO  GG捕魚名稱
        if ('12' == _conversionId.val()) {
            $('#thirdPartyName').text('GG捕魚');
        }

        //  TODO  取得用戶餘額
        $.cloudCall({
            method: 'user.balance.get',
            async: true,
            params: {
                sessionId: cms.getToken()
            },
            success: function(data) {
                $('#mainBalance > span').text(data.result.balance);
            }
        });

        //  TODO  取得特定第三方平台餘額
        $.cloudCall({
            method: 'thirdparty.user.balance.get',
            async: true,
            params: {
                sessionId: cms.getToken(),
                tpPid: _conversionId.val()
            },
            success: function(data) {
                $('#thirdpartyBalance > span').text(data.result.balance);
            }
        });
    });

    $('body').delegate('.no-conversion', 'click', function() {
        console.log($('#thirdPartyUrl').val());
        if ($('#thirdPartyUrl').val() != '#') {
            if ($('#thirdPartyTarget').val() != '') {
                window.open($('#thirdPartyUrl').val(), '_blank');
            } else {
                window.location.replace($('#thirdPartyUrl').val());
            }
        } else {
            if ($('#thirdPartyChkId').val() == 'mvideoMG') {
                $('#formMGVideo').submit();
            } else {
                JsMsg.errorMsg({
                    message: '系统维护中'
                });
            }
        }
    });

    //  TODO  點擊轉帳畫面快速儲值按鍵
    $('body').delegate('#thConversion .btn-quick-price', 'click', function(e) {
        var _this = $(this);
        var _amount = _this.data('price');
        if (isNaN(_amount)) {
            $('#amount').val($('#balance').text());
        } else {
            $('#amount').val(_amount);
        }
    });

    //  TODO  點擊儲值
    $('body').delegate('#thConversion .submit-conversion', 'click', function(e) {
        bgPage.doBalanceExchange($('#thirdPartyId').val(), $('#amount').val());
    });

    //  TODO  電子遊戲按鈕
    $('body').delegate('.navgame.list a', 'click', function(e) {
        var _gameBtn = $(e.target).parent();
        if ('' != cms.getToken()) {
            if (_gameBtn.data('thirdpartyid') != '') {
                e.preventDefault();
                bgPage.openConversionModal(_gameBtn.data('thirdpartyid'), _gameBtn.attr('href'), _gameBtn.attr('target'));
            }
        }
    });

    //  TODO  捕魚達人按鈕,需要延遲100豪秒避免無法抓到ul物件
    setTimeout(function() {
        $('body').delegate('#ul_fish2 a', 'click', function(e) {
            var _gameBtn = $(e.target).parent();
            if ('' != cms.getToken()) {
                if (_gameBtn.data('thirdpartyid') != '') {
                    e.preventDefault();
                    bgPage.openConversionModal(_gameBtn.data('thirdpartyid'), _gameBtn.attr('href'), _gameBtn.attr('target'));
                }
            }
        });
    }, 100);

    //  TODO  體育
    $('body').delegate('#sports-list a', 'click', function(e) {
        var _gameBtn = $(e.target).parent();
        if ('' != cms.getToken()) {
            if (_gameBtn.data('thirdpartyid') != '') {
                e.preventDefault();
                bgPage.openConversionModal(_gameBtn.data('thirdpartyid'), _gameBtn.attr('href'), _gameBtn.attr('target'));
            }
        }
    });

    //  TODO  關閉第三方轉換視窗
    $('body').delegate('#thConversion #closebtn', 'click', function(e) {
        $('#thConversion').modal('hide');
    });

    //  TODO  只能輸入數字
    $('body').delegate('#thConversion #amount', 'blur', function(e) {
        var _this = $(e.target);
        var _amountRegex = /^[+-]?\d*[.]?\d*$/;
        if (!_amountRegex.test(_this.val())) {
            JsMsg.errorMsg({
                message: '请输入正确的金额'
            }, {
                callback: function() {
                    if ($('#thConversion').is(':visible')) {
                        $('#thConversion #amount').val('');
                    }
                }
            });
        }

        if (parseFloat(_this.val()) > parseFloat($('#mainBalance > span').text())) {
            JsMsg.errorMsg({
                message: '输入的金额超过现有额度'
            }, {
                callback: function() {
                    if ($('#thConversion').is(':visible')) {
                        $('#thConversion #amount').val('');
                    }
                }
            });
        }
    });
});
//}}}
