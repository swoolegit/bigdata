<?php
require('../config.php');
require_once './mysql.class.php';
$dbconf = array("conn"=>DB_HOST.":".DB_PORT, "user"=>DB_USER, "pwd"=>DB_PASS, "db"=>DB_NAME);
$prename=DB_PREFIX; //表前缀
$mydb = new MYSQL($dbconf);
$sql="select id,title from {$prename}type where enable=1 order by type,sort";
$gRs = $mydb->row_query($sql);
$data=array();
foreach($gRs as $k=>$v)
{
	$data[]=array(
		'name'=>$v['title'],
		'id'=>$v['id']
	);
}
echo json_encode($data);
?>
