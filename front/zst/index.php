<?php
ob_start('ob_output');
function ob_output($html) {
	// 一些用户喜欢使用windows笔记本编辑文件，因此在输出时需要检查是否包含BOM头
	if (ord(substr($html, 0, 1)) === 239 && ord(substr($html, 1, 2)) === 187 && ord(substr($html, 2, 1)) === 191) $html = substr($html, 3);
	// gzip输出
	if(
		!headers_sent() && // 如果页面头部信息还没有输出
		extension_loaded("zlib") && // 而且zlib扩展已经加载到PHP中
		array_key_exists('HTTP_ACCEPT_ENCODING', $_SERVER) &&
		stripos($_SERVER["HTTP_ACCEPT_ENCODING"], "gzip") !== false // 而且浏览器说它可以接受GZIP的页面
	) {
		$html = gzencode($html, 3);
		header('Content-Encoding: gzip');
		header('Vary: Accept-Encoding');
	}
	header('Content-Length: '.strlen($html));
	return $html;
}
require('../config.php');
$id=array('1','3','5','6','7','9','10','12','14','26','15','16','20','25','60','63','64','53','61','70','71','72','73','74','75','76','77','78','79','80','81','82','83','84','85','86','87','88','89');
$pgsid=array('30','50','80','100','120','');
include(dirname(__FILE__)."/inc/comfunc.php");


//此处设置彩种id
$typeid=intval($_GET['typeid']);
if(!in_array($typeid,$id)) die("typeid error");
if(!$typeid) $typeid=14;
//每页默认显示
$pgs=intval($_GET['pgs']);
if(!in_array($pgs,$pgsid)) die("pgs error");
if(!$pgs) $pgs=30;
//当前页面
$page=intval($_GET['page']);
if(!$page) $page=1;
//传参
$toUrl="?page=";
$params=http_build_query($_REQUEST, '', '&');
if(!$mydb) $mydb = new MYSQL($dbconf);
$gRs = $mydb->row($conf['db']['prename']."type","shortName,type","id=".$typeid);
if($gRs){
	$shortName=$gRs[0][0];
	$lotteryType=$gRs[0][1];
}

$fromTime=$_GET['fromTime'];
$toTime=$_GET['toTime'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="IE=EmulateIE8" http-equiv="X-UA-Compatible" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="renderer" content="webkit" />
<meta http-equiv="Pragma" content="no-cache">
<title><?=$shortName?>走势分析图</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/line.css"  rel="stylesheet" type="text/css">
<link href="js/jqueryui/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
</head>
<body style="background:none;">
<div id="header">
    <div id="header-inner">
        <div class="logo"><?=$shortName?>走势分析图</div>
    </div>

</div>
<div id="content">
<div class="search">
<b class="b5"></b>
<b class="b6"></b>
<b class="b7"></b>
<b class="b8"></b>
<table width="100%" id="titlemessage" border="0" cellpadding="0" cellspacing="0" style="background:#DDE0E5;">
	<tbody><tr>
		<td><b><span class="redtext"><?=$shortName?>基本走势</span></b></td>
		<td>
			<a href="?typeid=<?=$typeid?>&pgs=30" class="ml10<?php if($pgs==30) echo ' on'?>" target="_self">最近30期</a>
			<a href="?typeid=<?=$typeid?>&pgs=50" class="ml10<?php if($pgs==50) echo ' on'?>" target="_self">最近50期</a>
            <a href="?typeid=<?=$typeid?>&pgs=80" class="ml10<?php if($pgs==80) echo ' on'?>" target="_self">最近80期</a>
			<a href="?typeid=<?=$typeid?>&pgs=100" class="ml10<?php if($pgs==100) echo ' on'?>" target="_self">最近100期</a>
            <a href="?typeid=<?=$typeid?>&pgs=120" class="ml10<?php if($pgs==120) echo ' on'?>" target="_self">最近120期</a>
		</td>
		<td>
        <!--form action="" method="get">
        	<input type="hidden" name="typeid" value="<?=$typeid?>" />
            <input type="hidden" name="pgs" value="<?=$pgs?>" />
            <input type="text" value="<?=$fromTime?>" class="datetxt" name="fromTime" id="fromTime" style="width:80px;">
            <img src="images/date.png" style="vertical-align:middle;">
            至
            <input type="text" value="<?=$toTime?>" class="datetxt" name="toTime" id="toTime" style="width:80px;">
            <img src="images/date.png" style="vertical-align:middle;">
            <input type="submit" value="查询" id="showissue1">
        </form-->
		</td>
	</tr>

</tbody></table>
<b class="b8"></b>
<b class="b7"></b>
<b class="b6"></b>
<b class="b5"></b>
</div>
<table height="5"><tbody><tr><td></td></tr></tbody></table>
<table align="center">
	 <tbody><tr>
        <td colspan="3" style="border:0px;">
			标注形式选择&nbsp;<input type="checkbox" name="checkbox2" value="checkbox" id="has_line">
            <span><b><label for="has_line">显示走势折线</label></b></span>&nbsp;
            <span>
            <label for="no_miss">
                <input type="checkbox" name="checkbox" value="checkbox" id="no_miss">不带遗漏
            </label>
            </span>
        </td>
      </tr>
</tbody></table>
<table height="5"><tbody><tr><td></td></tr></tbody></table>
<div style="position: relative; height: 756px;" id="container">
<?php
$pos=0;
switch(true)
{
	case $lotteryType==1 || $lotteryType==2:
		$pos=5;
		break;
	case $lotteryType==3 || $lotteryType==9:
		$pos=3;
		break;
	case $lotteryType==6:
		$pos=10;
		//$pos=8;
		break;
}
?>

<table id="chartsTable" width="100%" cellpadding="0" cellspacing="0" border="0" style="position:absolute; top:0; left:0;">
      <tbody><tr id="title">
             <td rowspan="2"><strong>期号</strong></td>
             <td rowspan="2" colspan="<?=$pos?>" class="redtext"><strong>开奖号码</strong></td>
<?php
switch(true)
{
	case $lotteryType==1 || $lotteryType==2:
		$colspan=10;
		if($lotteryType==2)$colspan=11;
?>
                          <td colspan="<?=$colspan?>"><strong>万位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>千位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>百位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>十位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>个位</strong></td>
<?php
		break;
	case $lotteryType==3 || $lotteryType==9:
		$colspan=10;
		if($lotteryType==9)$colspan=6;
?>
                          <td colspan="<?=$colspan?>"><strong>百位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>十位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>个位</strong></td>
<?php
		break;
	case $lotteryType==6:
?>
                          <td colspan="10"><strong>第一位</strong></td>
                          <td colspan="10"><strong>第二位</strong></td>
                          <td colspan="10"><strong>第三位</strong></td>
                          <td colspan="10"><strong>第四位</strong></td>
                          <td colspan="10"><strong>第五位</strong></td>
                          <td colspan="10"><strong>第六位</strong></td>
                          <td colspan="10"><strong>第七位</strong></td>
                          <td colspan="10"><strong>第八位</strong></td>
                          <!--td colspan="10"><strong>第九位</strong></td>
                          <td colspan="10"><strong>第十位</strong></td-->
<?php
		break;
}
?>
                 </tr>
                    <tr id="head">
<?php
$ball=0;
switch(true)
{
	case $lotteryType==1 || $lotteryType==3:
		$ball=10;
		$trunCount=5;
		if($lotteryType==3)
		{
			$trunCount=3;
		}
		for($y=1;$y<=$trunCount;$y++)
		{
			for($i=0;$i<10;$i++)
			{
				echo "<td class='wdh' align='center'><strong>".$i."</strong></td>";
			}			
		}
		break;
	case $lotteryType==9:
		$ball=6;
		$trunCount=3;
		for($y=1;$y<=$trunCount;$y++)
		{
			for($i=1;$i<7;$i++)
			{
				echo "<td class='wdh' align='center'><strong>".$i."</strong></td>";
			}
		}
		break;
	case $lotteryType==6:
		$ball=10;
		//$trunCount=10;
		$trunCount=8;
		for($y=1;$y<=$trunCount;$y++)
		{
			for($i=1;$i<11;$i++)
			{
				echo "<td class='wdh' align='center'><strong>".$i."</strong></td>";
			}
		}
		break;
	case $lotteryType==2:
		$ball=11;
		$trunCount=5;
		for($y=1;$y<=$trunCount;$y++)
		{		
			for($i=1;$i<12;$i++)
			{
				echo "<td class='wdh' align='center'><strong>".$i."</strong></td>";
			}
		}
		break;
}
?>
                    </tr>
          		<?php
				if($fromTime) $fromTime=strtotime($fromTime);
				if($toTime) $toTime=strtotime($toTime)+24*3600;

				$pg=trim($_REQUEST["page"]);
				if(!$pg){$pg=1;}
				if(!$pgs){$pgs=30;}
				$tableStr=$conf['db']['prename']."trend";
				$tableStr2=$conf['db']['prename']."trend a";
				$fieldsStr="time, number, data, data1, data2, data3, data4, data5, data6, data7, data8, data9, data10";

				$fieldsStr2="time, number, data, data1, data2, data3, data4, data5, data6, data7, data8, data9, data10";
				$whereStr=" type=$typeid ";
				$whereStr2=" a.type=$typeid ";
				if($fromTime && $toTime){
					$whereStr.=" and time between $fromTime and $toTime";
					$whereStr2.=" and a.time between $fromTime and $toTime";
				}elseif($fromTime){
					$whereStr.=' and time>='.$fromTime;
					$whereStr2.=' and a.time>='.$fromTime;
				}elseif($toTime){
					$whereStr.=' and time<'.$toTime;
					$whereStr2.=' and a.time<'.$toTime;
				}else{}
				//$orderStr=" order by a.id asc";
				$orderStr=" order by a.id desc";

				$totalNumber = $mydb->row_count($tableStr,$whereStr);

				if ($totalNumber>0){

                $countcount=0;
				$perNumber=$pgs; //每页显示的记录数
				$page=$pg; //获得当前的页面值
				if (!isset($page)) $page=1;

				//$totalPage=ceil($totalNumber/$perNumber); //计算出总页数
				//$startCount=($page-1)*$perNumber; //分页开始,根据此方法计算出开始的记录
				$data = $mydb->row($tableStr2,$fieldsStr2,$whereStr2.' '.$orderStr." limit 0,$perNumber");
				krsort($data);
				$five=array();
				for($i=0;$i<11;$i++)
				{
					$five['SW'.$i]=0;
					$five['LCW'.$i]=0;
					$five['MW'.$i]=0;
					$five['MLCW'.$i]=0;

					$five['SQ'.$i]=0;
					$five['LCQ'.$i]=0;
					$five['MQ'.$i]=0;
					$five['MLCQ'.$i]=0;

					$five['SB'.$i]=0;
					$five['LCB'.$i]=0;
					$five['MB'.$i]=0;
					$five['MLCB'.$i]=0;

					$five['SS'.$i]=0;
					$five['LCS'.$i]=0;
					$five['MS'.$i]=0;
					$five['MLCS'.$i]=0;

					$five['SG'.$i]=0;
					$five['LCG'.$i]=0;
					$five['MG'.$i]=0;
					$five['MLCG'.$i]=0;

					$five['SH'.$i]=0;
					$five['LCH'.$i]=0;
					$five['MH'.$i]=0;
					$five['MLCH'.$i]=0;

					$five['SI'.$i]=0;
					$five['LCI'.$i]=0;
					$five['MI'.$i]=0;
					$five['MLCI'.$i]=0;

					$five['SJ'.$i]=0;
					$five['LCJ'.$i]=0;
					$five['MJ'.$i]=0;
					$five['MLCJ'.$i]=0;
				}				
				if($data) foreach($data as $var)
				{	
				$data1=explode(",",$var[3]);
				$data2=explode(",",$var[4]);
				$data3=explode(",",$var[5]);
				$data4=explode(",",$var[6]);
				$data5=explode(",",$var[7]);
				$data6=explode(",",$var[8]);
				$data7=explode(",",$var[9]);
				$data8=explode(",",$var[10]);
				$data9=explode(",",$var[11]);
				$data10=explode(",",$var[12]);
				
				$dArry=explode(",",$var[2]);
				/*
				$var['d1']=$dArry[0];
				$var['d2']=$dArry[1];
				$var['d3']=$dArry[2];
				$var['d4']=$dArry[3];
				$var['d5']=$dArry[4];
				*/
				echo '<tr>';
				echo '<td id="title">'.$var[1].'</td>';
				foreach($dArry as $k=>$v)
				{
					echo '<td class="wdh" align="center"><div class="ball02">'.$v.'</div></td>';
				}
				/*
				echo '<td class="wdh" align="center"><div class="ball02">'.$var['d1'].'</div></td>';
				echo '<td class="wdh" align="center"><div class="ball02">'.$var['d2'].'</div></td>';
				echo '<td class="wdh" align="center"><div class="ball02">'.$var['d3'].'</div></td>';
				echo '<td class="wdh" align="center"><div class="ball02">'.$var['d4'].'</div></td>';
				echo '<td class="wdh" align="center"><div class="ball02">'.$var['d5'].'</div></td>';
				*/
				foreach($data1 as $k=>$v)//萬位
				{
					$count=explode(":",$v);
					if($count[1]==0)
					{
						echo '<td class="charball" align="center"><div class="ball01">'.$count[0].'</div></td>';
						if($five['SW'.$k]){$five['SW'.$k]++;}else{$five['SW'.$k]=1;} //出现总次数
						if($five['LCW'.$k]){$five['LCW'.$k]++;}else{$five['LCW'.$k]=1;} //最大连出值
						
					}else
					{
						echo '<td class="wdh" align="center"><div class="ball03">'.$count[1].'</div></td>';
						$five['LCW'.$k]=0;
					}
					//最大遗漏值
					if($five['MW'.$k]<$count[1]){$five['MW'.$k]=$count[1];}
					//最大连出值
					if($five['MLCW'.$k]<$five['LCW'.$k]){$five['MLCW'.$k]=$five['LCW'.$k];}
				}

				foreach($data2 as $k=>$v)//千位
				{
					$count=explode(":",$v);
					if($count[1]==0)
					{
						echo '<td class="charball" align="center"><div class="ball02">'.$count[0].'</div></td>';
						if($five['SQ'.$k]){$five['SQ'.$k]++;}else{$five['SQ'.$k]=1;} //出现总次数
						if($five['LCQ'.$k]){$five['LCQ'.$k]++;}else{$five['LCQ'.$k]=1;} //最大连出值
						
					}else
					{
						echo '<td class="wdh" align="center"><div class="ball04">'.$count[1].'</div></td>';
						$five['LCQ'.$k]=0;
					}
					//最大遗漏值
					if($five['MQ'.$k]<$count[1]){$five['MQ'.$k]=$count[1];}
					//最大连出值
					if($five['MLCQ'.$k]<$five['LCQ'.$k]){$five['MLCQ'.$k]=$five['LCQ'.$k];}
				}
				foreach($data3 as $k=>$v)//百位
				{
					$count=explode(":",$v);
					if($count[1]==0)
					{
						echo '<td class="charball" align="center"><div class="ball01">'.$count[0].'</div></td>';
						if($five['SB'.$k]){$five['SB'.$k]++;}else{$five['SB'.$k]=1;} //出现总次数
						if($five['LCB'.$k]){$five['LCB'.$k]++;}else{$five['LCB'.$k]=1;} //最大连出值
						
					}else
					{
						echo '<td class="wdh" align="center"><div class="ball03">'.$count[1].'</div></td>';
						$five['LCB'.$k]=0;
					}
					//最大遗漏值
					if($five['MB'.$k]<$count[1]){$five['MB'.$k]=$count[1];}
					//最大连出值
					if($five['MLCB'.$k]<$five['LCB'.$k]){$five['MLCB'.$k]=$five['LCB'.$k];}
				}

				if($lotteryType==1 || $lotteryType==2 || $lotteryType==6)
				{
					foreach($data4 as $k=>$v)//十位
					{
						$count=explode(":",$v);
						if($count[1]==0)
						{
							echo '<td class="charball" align="center"><div class="ball02">'.$count[0].'</div></td>';
							if($five['SS'.$k]){$five['SS'.$k]++;}else{$five['SS'.$k]=1;} //出现总次数
							if($five['LCS'.$k]){$five['LCS'.$k]++;}else{$five['LCS'.$k]=1;} //最大连出值
							
						}else
						{
							echo '<td class="wdh" align="center"><div class="ball04">'.$count[1].'</div></td>';
							$five['LCS'.$k]=0;
						}
						//最大遗漏值
						if($five['MS'.$k]<$count[1]){$five['MS'.$k]=$count[1];}
						//最大连出值
						if($five['MLCS'.$k]<$five['LCS'.$k]){$five['MLCS'.$k]=$five['LCS'.$k];}
					}
	
					foreach($data5 as $k=>$v)//个位
					{
						$count=explode(":",$v);
						if($count[1]==0)
						{
							echo '<td class="charball" align="center"><div class="ball01">'.$count[0].'</div></td>';
							if($five['SG'.$k]){$five['SG'.$k]++;}else{$five['SG'.$k]=1;} //出现总次数
							if($five['LCG'.$k]){$five['LCG'.$k]++;}else{$five['LCG'.$k]=1;} //最大连出值
							
						}else
						{
							echo '<td class="wdh" align="center"><div class="ball03">'.$count[1].'</div></td>';
							$five['LCG'.$k]=0;
						}
						//最大遗漏值
						if($five['MG'.$k]<$count[1]){$five['MG'.$k]=$count[1];}
						//最大连出值
						if($five['MLCG'.$k]<$five['LCG'.$k]){$five['MLCG'.$k]=$five['LCG'.$k];}
					}					
				}
				if($lotteryType==6)
				{
					foreach($data6 as $k=>$v)//十位
					{
						$count=explode(":",$v);
						if($count[1]==0)
						{
							echo '<td class="charball" align="center"><div class="ball02">'.$count[0].'</div></td>';
							if($five['SH'.$k]){$five['SS'.$k]++;}else{$five['SH'.$k]=1;} //出现总次数
							if($five['LCH'.$k]){$five['LCH'.$k]++;}else{$five['LCH'.$k]=1;} //最大连出值
							
						}else
						{
							echo '<td class="wdh" align="center"><div class="ball04">'.$count[1].'</div></td>';
							$five['LCH'.$k]=0;
						}
						//最大遗漏值
						if($five['MH'.$k]<$count[1]){$five['MH'.$k]=$count[1];}
						//最大连出值
						if($five['MLCH'.$k]<$five['LCS'.$k]){$five['MLCH'.$k]=$five['LCH'.$k];}
					}
	
					foreach($data7 as $k=>$v)//个位
					{
						$count=explode(":",$v);
						if($count[1]==0)
						{
							echo '<td class="charball" align="center"><div class="ball01">'.$count[0].'</div></td>';
							if($five['SI'.$k]){$five['SI'.$k]++;}else{$five['SI'.$k]=1;} //出现总次数
							if($five['LCI'.$k]){$five['LCI'.$k]++;}else{$five['LCI'.$k]=1;} //最大连出值
							
						}else
						{
							echo '<td class="wdh" align="center"><div class="ball03">'.$count[1].'</div></td>';
							$five['LCI'.$k]=0;
						}
						//最大遗漏值
						if($five['MI'.$k]<$count[1]){$five['MI'.$k]=$count[1];}
						//最大连出值
						if($five['MLCI'.$k]<$five['LCI'.$k]){$five['MLCI'.$k]=$five['LCI'.$k];}
					}					
					foreach($data8 as $k=>$v)//个位
					{
						$count=explode(":",$v);
						if($count[1]==0)
						{
							echo '<td class="charball" align="center"><div class="ball02">'.$count[0].'</div></td>';
							if($five['SJ'.$k]){$five['SJ'.$k]++;}else{$five['SJ'.$k]=1;} //出现总次数
							if($five['LCJ'.$k]){$five['LCJ'.$k]++;}else{$five['LCJ'.$k]=1;} //最大连出值
							
						}else
						{
							echo '<td class="wdh" align="center"><div class="ball04">'.$count[1].'</div></td>';
							$five['LCJ'.$k]=0;
						}
						//最大遗漏值
						if($five['MJ'.$k]<$count[1]){$five['MJ'.$k]=$count[1];}
						//最大连出值
						if($five['MLCJ'.$k]<$five['LCJ'.$k]){$five['MLCJ'.$k]=$five['LCJ'.$k];}
					}
					/*
					foreach($data9 as $k=>$v)//个位
					{
						$count=explode(":",$v);
						if($count[1]==0)
						{
							echo '<td class="charball" align="center"><div class="ball01">'.$count[0].'</div></td>';
							if($five['SK'.$k]){$five['SK'.$k]++;}else{$five['SK'.$k]=1;} //出现总次数
							if($five['LCK'.$k]){$five['LCK'.$k]++;}else{$five['LCK'.$k]=1;} //最大连出值
							
						}else
						{
							echo '<td class="wdh" align="center"><div class="ball03">'.$count[1].'</div></td>';
							$five['LCK'.$k]=0;
						}
						//最大遗漏值
						if($five['MK'.$k]<$count[1]){$five['MK'.$k]=$count[1];}
						//最大连出值
						if($five['MLCK'.$k]<$five['LCJ'.$k]){$five['MLCK'.$k]=$five['LCK'.$k];}
					}
					foreach($data10 as $k=>$v)//个位
					{
						$count=explode(":",$v);
						if($count[1]==0)
						{
							echo '<td class="charball" align="center"><div class="ball01">'.$count[0].'</div></td>';
							if($five['SE'.$k]){$five['SE'.$k]++;}else{$five['SE'.$k]=1;} //出现总次数
							if($five['LCE'.$k]){$five['LCE'.$k]++;}else{$five['LCE'.$k]=1;} //最大连出值
							
						}else
						{
							echo '<td class="wdh" align="center"><div class="ball03">'.$count[1].'</div></td>';
							$five['LCE'.$k]=0;
						}
						//最大遗漏值
						if($five['ME'.$k]<$count[1]){$five['ME'.$k]=$count[1];}
						//最大连出值
						if($five['MLCE'.$k]<$five['LCE'.$k]){$five['MLCE'.$k]=$five['LCE'.$k];}
					}
					 *
					 */
				}
 
			echo '</tr>';
            }
?>
    <tr>
    <td nowrap="">出现总次数</td>
    <td align="center" colspan="<?=$pos?>">&nbsp;</td>
    <?php

    $count=0;
	//foreach(array('W','Q','B','S','G','H','I','J','K','E') as $var){
	foreach(array('W','Q','B','S','G','H','I','J') as $var){
		$count++;
		if($count>$pos)break;
		for($i=0;$i<$ball;$i++)
		{
			if($five['S'.$var.$i]){
				$five['D'.$var.$i]=$five['S'.$var.$i];
			}else{
				$five['D'.$var.$i]=0;
			}
			echo '<td align="center">'.$five['D'.$var.$i].'</td>';
		}
	}
	?>
    </tr>
    <tr>
    <td nowrap="">平均遗漏值</td>
    <td align="center" colspan="<?=$pos?>">&nbsp;</td>
    <?php
    $count=0;
	//foreach(array('W','Q','B','S','G','H','I','J','K','E') as $var){
	foreach(array('W','Q','B','S','G','H','I','J') as $var){
		$count++;
		if($count>$pos)break;
		for($i=0;$i<$ball;$i++)
		{
			$five['P'.$var.$i]=intval($pgs/($five['D'.$var.$i]+1));
			echo '<td align="center">'.$five['P'.$var.$i].'</td>';
		}
	}
	?>
    </tr>
    <tr>
    <td nowrap>最大遗漏值</td>
    <td align="center" colspan="<?=$pos?>">&nbsp;</td>
    <?php
    $count=0;
	//foreach(array('W','Q','B','S','G','H','I','J','K','E') as $var){
	foreach(array('W','Q','B','S','G','H','I','J') as $var){
		$count++;
		if($count>$pos)break;
		for($i=0;$i<$ball;$i++)
		{
			if($five['M'.$var.$i]){
				$five['Max'.$var.$i]=$five['M'.$var.$i];
			}else{
				$five['Max'.$var.$i]=0;
			}
			echo '<td align="center">'.$five['Max'.$var.$i].'</td>';
		}
	}
	?>
    </tr>
    <tr>
    <td nowrap>最大连出值</td>
    <td align="center" colspan="<?=$pos?>">&nbsp;</td>
    <?php
    $count=0;
	//foreach(array('W','Q','B','S','G','H','I','J','K','E') as $var){
	foreach(array('W','Q','B','S','G','H','I','J') as $var){
		$count++;
		if($count>$pos)break;
		for($i=0;$i<$ball;$i++)
		{
			if($five['MLC'.$var.$i]){
				$five['MaxLC'.$var.$i]=$five['MLC'.$var.$i];
			}else{
				$five['MaxLC'.$var.$i]=0;
			}
			echo '<td align="center">'.$five['MaxLC'.$var.$i].'</td>';
		}
	}
	?>
    </tr>
    <tr id="head">
        <td rowspan="2" align="center"><strong>期号</strong></td>
        <td rowspan="2" align="center" colspan="<?=$pos?>"><strong>开奖号码</strong></td>
<?php
switch(true)
{
	case $lotteryType==1 || $lotteryType==3:
		$ball=10;
		$trunCount=5;
		if($lotteryType==3)
		{
			$trunCount=3;
		}
		for($y=1;$y<=$trunCount;$y++)
		{
			for($i=0;$i<10;$i++)
			{
				echo "<td class='wdh' align='center'><strong>".$i."</strong></td>";
			}			
		}
		break;
	case $lotteryType==9:
		$ball=6;
		$trunCount=3;
		for($y=1;$y<=$trunCount;$y++)
		{
			for($i=1;$i<7;$i++)
			{
				echo "<td class='wdh' align='center'><strong>".$i."</strong></td>";
			}
		}
		break;
	case $lotteryType==6:
		$ball=10;
		//$trunCount=10;
		$trunCount=8;
		for($y=1;$y<=$trunCount;$y++)
		{
			for($i=1;$i<11;$i++)
			{
				echo "<td class='wdh' align='center'><strong>".$i."</strong></td>";
			}
		}
		break;
	case $lotteryType==2:
		$ball=11;
		$trunCount=5;
		for($y=1;$y<=$trunCount;$y++)
		{		
			for($i=1;$i<12;$i++)
			{
				echo "<td class='wdh' align='center'><strong>".$i."</strong></td>";
			}
		}
		break;
}
?>
    </tr>
    <tr id="title">
<?php
switch(true)
{
	case $lotteryType==1 || $lotteryType==2:
		$colspan=10;
		if($lotteryType==2)$colspan=11;
?>
                          <td colspan="<?=$colspan?>"><strong>万位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>千位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>百位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>十位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>个位</strong></td>
<?php
		break;
	case $lotteryType==3 || $lotteryType==9:
		$colspan=10;
		if($lotteryType==9)$colspan=6;
?>
                          <td colspan="<?=$colspan?>"><strong>百位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>十位</strong></td>
                          <td colspan="<?=$colspan?>"><strong>个位</strong></td>
<?php
		break;
	case $lotteryType==6:
?>
                          <td colspan="10"><strong>第一位</strong></td>
                          <td colspan="10"><strong>第二位</strong></td>
                          <td colspan="10"><strong>第三位</strong></td>
                          <td colspan="10"><strong>第四位</strong></td>
                          <td colspan="10"><strong>第五位</strong></td>
                          <td colspan="10"><strong>第六位</strong></td>
                          <td colspan="10"><strong>第七位</strong></td>
                          <td colspan="10"><strong>第八位</strong></td>
                          <!--td colspan="10"><strong>第九位</strong></td>
                          <td colspan="10"><strong>第十位</strong></td-->
<?php
		break;
}
?>
    </tr>
    <?php

		}
	?>
</tbody></table>
</div>

<dl class="tips">
    <dt>图表参数说明</dt>
　　<dd>出现总次数：统计期数内实际出现的次数。</dd>
　　<dd>平均遗漏值：统计期数内遗漏的平均值。（计算公式：平均遗漏＝统计期数/(出现次数+1)。）</dd>
　　<dd>最大遗漏值：统计期数内遗漏的最大值。</dd>
　　<dd>最大连出值：统计期数内连续开出的最大值。</dd>
</dl>
</div>
<!--<div id="footer">Copyright © 娱乐</div>-->
<script language="javascript" type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
<script language="javascript" type="text/javascript" src="js/line.js"></script>
<script language="javascript" type="text/javascript" src="js/jqueryui/jquery-ui-1.8.23.custom.min.js"></script>
<script type="text/javascript">window.onerror=function(){return true;}</script>
<script language="javascript">
fw.onReady(function(){
	Chart.init();
	DrawLine.bind("chartsTable","has_line");
	DrawLine.color('#499495');
	DrawLine.add((parseInt(0)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);
	DrawLine.color('#E4A8A8');
	DrawLine.add((parseInt(1)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);
	DrawLine.color('#499495');
	DrawLine.add((parseInt(2)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);
	DrawLine.color('#E4A8A8');
	DrawLine.add((parseInt(3)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);
	DrawLine.color('#499495');
	DrawLine.add((parseInt(4)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);
	DrawLine.color('#E4A8A8');
	DrawLine.add((parseInt(5)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);
	DrawLine.color('#499495');
	DrawLine.add((parseInt(6)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);
	DrawLine.color('#E4A8A8');
	DrawLine.add((parseInt(7)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);
	DrawLine.color('#499495');
	DrawLine.add((parseInt(8)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);
	DrawLine.color('#E4A8A8');
	DrawLine.add((parseInt(9)*<?=$ball?>+<?=$pos?>+1),2,<?=$ball?>,0);

	DrawLine.draw(Chart.ini.default_has_line);
	if($("#chartsTable").width()>$('body').width())
	{
	   $('body').width($("#chartsTable").width() + "px");
	}
	$("#container").height($("#chartsTable").height() + "px");
    resize();

	var nols = $(".ball04,.ball03");
	$("#no_miss").click(function(){

		var checked = $(this).attr("checked");
		$.each(nols,function(i,n){
			if(checked==true || checked=='checked'){
				n.style.display='none';
			}else{
				n.style.display='block';
			}
		});
	});
});
function resize(){
    window.onresize = func;
    function func(){
        window.location.href=window.location.href;
    }
}
$(function(){
	$(".datetxt").datepicker({ onSelect: function(dateText, inst) {$(this).val(dateText);} });
})
</script>
</body>
</html>