<?php
// [牛付]支付接口
class pay_niufu
{

    //real use!
    private $partner = '0755000040'; //商戶號
    private $merchantKey = '25f03fac-1096-11e7-8fb7-11c807d5574d'; //密鑰
/*
0000	交易成功
1001	商户号未提交
1002	订单号未提交
1003	MD5摘要未提交
1004	查询商户信息异常,请稍后再试
1005	无商户信息
1006	MD5验证失败
1007	查询订单错,请稍后再试
1008	未找到订单信息
1009	返回信息数字签名失败
1010	返回信息MD5摘要失败
1011	当前订单状态不允许退款
1012	订单数据异常，请联系移联
1013	查询订单支付记录错,请稍后再试
1014	未找到订单的支付信息，请联系移联
1015	记录退款申请失败，请稍候再试
1016	记录退款交易失败，请稍候再试
1017	更新订单退款金额失败，请稍候再试
1020	更新银行支付记录状态失败，请稍候再试
1021	记录流水失败，请稍候再试
1030	退款金额未提交
1031	发送时间未提交
1032	退款金额过大，订单可退金额不足
1033	查询退款申请异常
1036	商户退款流水号未提交
1037	查询退款数据异常


*/

    // 银行代码

    private $banks = array(
        //'8' => 'CIB', // 兴业银行 v  <== 牛付沒這家銀行
		/*
		'1' => 'ALIPAY', // 支付寶 v
        '19' => 'WECHAT', // 微信/财付通 v
		'3' => 'CGBFF', // 广东发展银行v
        '4' => 'HXBFF',  // 华夏银行v
        '5' => 'COMMFF', // 交通银行 v
        '6' => 'PABFF', // 平安银行v
        '7' => 'SPDBFF', // 上海浦东发展银行v
        '9' => 'PSBCFF', // 中国邮政储蓄银行v
        '10' => 'CEBFF', // 中国光大银行v
		'11' => 'ICBCFF', // 中国工商银行 v
        '12' => 'CCBFF', // 中国建设银行 v
        '13' => 'CMSBFF', // 中国民生银行v
        '14' => 'ABCFF', // 中国农业银行 v
        '15' => 'BOCFF', // 中国银行 v
        '16' => 'CMBFF', // 招商银行v
        '17' => 'CNCBFF', // 中信银行v
		*/

		'1' => 'ALIPAY', // 支付寶 v		xxx
        '19' => 'WECHAT', // 微信/财付通 v
        '20' => 'QQSCAN', // QQ錢包
		'3' => 'GDB', // 广东发展银行v
        '4' => 'HXB',  // 华夏银行v
        '5' => 'COMM', // 交通银行 v
        '6' => 'PAB', // 平安银行v
        '7' => 'SPDB', // 上海浦东发展银行v
        '9' => 'POSTGC', // 中国邮政储蓄银行v
        '10' => 'CEB', // 中国光大银行v
		'11' => 'ICBC', // 中国工商银行 v
        '12' => 'CCB', // 中国建设银行 v
        '13' => 'CMBC', // 中国民生银行v
        '14' => 'ABC', // 中国农业银行 v
        '15' => 'BOC', // 中国银行 v
        '16' => 'CMB', // 招商银行v
        '17' => 'CITIC', // 中信银行v
		'8' => 'CIB', // 兴业银行 v

/*

{"msg":[{"ABNAME":"农业银行","BANKID":"ABC","LOGOSRC":""},{"ABNAME":"东亚银行","BANKID":"BEA","LOGOSRC":""},{"ABNAME":"北京银行","BANKID":"BOBJ","LOGOSRC":""},{"ABNAME":"中国银行","BANKID":"BOC","LOGOSRC":""},{"ABNAME":"建设银行","BANKID":"CCB","LOGOSRC":""},{"ABNAME":"光大银行","BANKID":"CEB","LOGOSRC":""},{"ABNAME":"兴业银行","BANKID":"CIB","LOGOSRC":""},{"ABNAME":"中信银行","BANKID":"CITIC","LOGOSRC":""},{"ABNAME":"招商银行","BANKID":"CMB","LOGOSRC":""},{"ABNAME":"民生银行","BANKID":"CMBC","LOGOSRC":""},{"ABNAME":"交通银行","BANKID":"COMM","LOGOSRC":""},{"ABNAME":"广发银行","BANKID":"GDB","LOGOSRC":""},{"ABNAME":"华夏银行","BANKID":"HXB","LOGOSRC":""},{"ABNAME":"工商银行","BANKID":"ICBC","LOGOSRC":""},{"ABNAME":"南京银行","BANKID":"NJCB","LOGOSRC":""},{"ABNAME":"平安银行","BANKID":"PAB","LOGOSRC":""},{"ABNAME":"邮政储蓄","BANKID":"POSTGC","LOGOSRC":""},{"ABNAME":"浦发银行","BANKID":"SPDB","LOGOSRC":""},{"ABNAME":"上海农商银行","BANKID":"SRCB","LOGOSRC":""}]}

*/

	);

	public function getBankId() {

		$parameter = array(

            'partnerId'	=> $this->partner,
            'channel'	=> 'WEBPLAY', // WEBPAY:表示获取支持WEB网站的银行列表  WAPPAY:表示获取支持手机网站的银行列表
            //'channel'	=> 'WAPPAY', // WEBPAY:表示获取支持WEB网站的银行列表  WAPPAY:表示获取支持手机网站的银行列表
            //'rstType'	=> 'json',
        );

		//$string = "version={$parameter['version']}&partnerId={$parameter['partnerId']}&orderId={$parameter['orderId']}&goods={$parameter['goods']}&amount={$parameter['amount']}&expTime={$parameter['expTime']}&notifyUrl={$parameter['notifyUrl']}&pageUrl={$parameter['pageUrl']}&reserve={$parameter['reserve']}&extendInfo={$parameter['extendInfo']}&payMode={$parameter['payMode']}&bankId={$parameter['bankId']}&creditType={$parameter['creditType']}&key={$this->merchantKey}";
		$sign = '';
		foreach ($parameter as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$parameter['rstType'] = 'json';
		$sign .= 'key='.$this->merchantKey.'&rstType='.$parameter['rstType'];
		$parameter['sign'] = md5($sign);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "https://pay.newpaypay.com/servlet/InitBankLogoServlet");
		curl_setopt($curl, CURLOPT_POST, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query( $parameter ));
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		$response = curl_exec($curl);
		curl_close($curl);
		$response = mb_convert_encoding($response, "UTF-8", "GBK");
		$response = json_decode($response, true);
		$bankListArray = $response['msg'];

		$this->banks = array(
			'1' => 'ALIPAY', // 支付寶 v		xxx
			'19' => 'WECHAT', // 微信/财付通 v
			'20' => 'QQSCAN', // QQ錢包			
		);

		$bankMapping = array(
			'农业银行'	=> 14,
			//'北京银行'	=> ,
			//'农村商业银行'	=> ,
			'中国银行'	=> 15,
			'交通银行'	=> 5,
			'建设银行'	=> 12,
			//'银联通道'	=> ,
			'广发银行'	=> 3,
			'兴业银行'	=> 8,
			'招商银行'	=> 16,
			'民生银行'	=> 13,
			'中信银行'	=> 17,
			'华夏银行'	=> 4,
			'工商银行'	=> 11,
			'平安银行'	=> 6,
			'邮政储蓄银行'	=> 9,
			//'上海银行'	=> ,
			'浦发银行'	=> 7,
			//'银联通道'	=> 

		
		);

		foreach ($bankListArray as $rows) {
			if (isset($bankMapping[$rows['ABNAME']])) {
				$this->banks[$bankMapping[$rows['ABNAME']]] = $rows['BANKID'];		
			}
		}



	}


    /**
     * @name 支付方法
     * @param int bankid 银行ID
     * @param int amount 充值金额
     * @param string orderid 订单ID
     * @param string url_callback 回调地址
     * @param string url_return 充值完成后返回地址
     */
    public function pay($bankid, $amount, $orderid, $url_callback, $url_return)
    {
		$this->getBankId();

		/*
		商户ID	partner	N	Y	商户id,由银宝商务分配
		银行类型	banktype	N	Y	银行类型，具体参考附录1
		金额	paymoney	N	Y	单位元（人民币）
		商户订单号	ordernumber	N	Y	商户系统订单号，该订单号将作为银宝商务接口的返回数据。该值需在商户系统内唯一，银宝商务系统暂时不检查该值是否唯一
		下行异步通知地址	callbackurl	N	Y	下行异步通知的地址，需要以http://开头且没有任何参数
		下行同步通知地址	hrefbackurl	Y	N	下行同步通知过程的返回地址(在支付完成后银宝商务接口将会跳转到的商户系统连接地址)。
		注：若提交值无该参数，或者该参数值为空，则在支付完成后，银宝商务接口将不会跳转到商户系统，用户将停留在银宝商务接口系统提示支付成功的页面。
		备注信息	attach	Y	N	备注信息，下行中会原样返回。若该值包含中文，请注意编码
		MD5签名	sign	N	N	32位小写MD5签名值，GB2312编码

		*/
		//获取输入参数
		/*
        $parameter = array(
            'partner'	=> $this->partner,
            'banktype'	=> $this->banks[$bankid],
            'paymoney'	 => sprintf("%.2f", $amount),
            'ordernumber' => $orderid,
            'callbackurl' => $url_callback,
        );

		$parameter = array(
            'partnerId'	=> $this->partner,
            'channel'	=> 'WAPPAY',
            'rstType'	=> 'json',
        );
		$string = "partnerId={$this->partner}&channel={$parameter['channel']}&key={$this->merchantKey}&rstType={$parameter['rstType']}";
		$parameter['sign'] = md5($string);
		*/

		/*
		version	接口版本	10	N	默认v1
		partnerId	商户代码	30	Y	由支付网关统一分配
		orderId	订单号	30	Y	商户需要保证订单号在商户系统中的唯一性(订单号只允许是数字和字母)
		goods	商品描述	128	N	最长不超过128位英文字符，使用base64进行编码后提交。
		amount	付款金额	12	Y	单位为元，格式为nnn.nn，小数点必须保留足两位。
		暂只支持使用人民币
		expTime	订单有效时间	14	N	超过订单有效时间未支付，订单作废；不提交该参数，采用系统的默认时间（从接收订单后有效时间为3天）
		格式为：年[4 位]月[2 位]日[2 位]时[2 位]分[2 位]秒[2位]
		例如：20090210140101
		notifyUrl	服务器端主动通知支付结果URL	128	N	支付平台服务器通知商户的地址
		pageUrl	页面链接通知支付结果URL	128	N	手机浏览器通过页面链接方式通知商户的地址，商户处理结束后，应该展现一个处理结果的wap页面
		reserve	商户保留信息	512	N	通知商户订单支付结果时，按原样传给商户
		extendInfo	订单扩展信息	2048	N	订单扩展信息，根据不同的银行送不同的数据
		payMode	支付模式	2	Y	00:WAP手机银行支付模式
		01:网银支付模式
		02:委托银行扣款模式
		03:客户端集成银行客户端程序模式
		04:客户端调用银行WAP页面支付模式
		05:手机网页支付模式
		bankId	银行编号	40	Y	CCB|CEB-NET-B2C
		支付平台通道编号 | 子通道编号
		creditType
			允许支付的卡类型	1	Y	0:表示仅允许使用借记卡支付
		1:表示仅允许使用信用卡支付
		2:表示借记卡和信用卡都能对订单进行支付
		sign	MD5签名	256	Y	接口版本、商户代码、订单号、商品描述、付款金额、订单有效时间、服务器端主动通知支付结果URL、页面链接通知支付结果URL、保留信息、订单扩展信息、支付模式、银行编号、允许支付的卡类型与密钥组合，经MD5 摘要生成的32 位字符串。

		*/
		//$product = ['QQ币', '会员充值', '移动充值', '联通充值', '电信充值', '话费', '水电费'];
		$product = ['MemberRecharge', 'MobileRecharge'];
		shuffle($product);
		$parameter = array(
			'version'	=> 'v1',
            'partnerId'	=> $this->partner,
            'orderId'	=> $orderid,
            'goods'		=> base64_encode($product[0]),
            'amount'	=> sprintf("%.2f", $amount),
            'expTime'	=> '',
            'notifyUrl'	=> $url_callback,
            'pageUrl'	=> $url_return,
			'reserve'=> '',
			'extendInfo'=> '',
			'payMode'	=> ($bankid == 1 || $bankid == 19 || $bankid == 20)? '09' : '01',
			'bankId'	=> $this->banks[$bankid],
            'creditType' => 0,
        );

		//$string = "version={$parameter['version']}&partnerId={$parameter['partnerId']}&orderId={$parameter['orderId']}&goods={$parameter['goods']}&amount={$parameter['amount']}&expTime={$parameter['expTime']}&notifyUrl={$parameter['notifyUrl']}&pageUrl={$parameter['pageUrl']}&reserve={$parameter['reserve']}&extendInfo={$parameter['extendInfo']}&payMode={$parameter['payMode']}&bankId={$parameter['bankId']}&creditType={$parameter['creditType']}&key={$this->merchantKey}";
		$sign = '';
		foreach ($parameter as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$sign .= 'key='.$this->merchantKey;

		$parameter['sign'] = md5($sign);

        core::logger($parameter);
		$middleUrl = MIDDLE_URL.(str_replace('pay_','',__CLASS__)).'.php';

		//支付寶或微信
		if($bankid == 1 || $bankid == 19 || $bankid == 20) {

			/*
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, "https://pay.newpaypay.com/center/proxy/partner/v1/pay.jsp");
			curl_setopt($curl, CURLOPT_POST, false);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query( $parameter ));
			curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
			$response = curl_exec($curl);
			curl_close($curl);*/
			$response = core::lib('middle')->onlinePay($parameter,$middleUrl);
			core::logger(['response' => trim($response)]);


			$json = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', trim($response)), true );
			return $json;
		}
		else {
			//使用中轉
			core::lib('middle')->bankPay($parameter,$middleUrl);
			exit;
			$html = '';
			$html .= '<html>';
			$html .= '<head>';
			$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
			$html .= '</head>';
			$html .= '<body onLoad="document.pay.submit();">';
			$html .= '正在跳转 ...';

			//$html .= '<form name="pay" method="GET" action="https://pay.newpaypay.com/servlet/InitBankLogoServlet">';
			$html .= '<form name="pay" method="GET" action="https://pay.newpaypay.com/center/proxy/partner/v1/pay.jsp">';
			foreach ($parameter as $key => $val) {
				$html .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />';
			}
			$html .= '<script type="text/javascript">document.pay.submit();</script>';
			$html .= '</form>';
			$html .= '</body>';
			$html .= '</html>';
			return $html;
		}
    }


	//反查訂單，以防假單注入
	function checkOrder($orderid) {

		$parameter = array(
            'partnerId'	=> $this->partner,
            'orderId'	=> $orderid,
        );
		$sign = '';
		foreach ($parameter as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$sign .= 'key='.$this->merchantKey;

		$parameter['md5'] = md5($sign);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "https://pay.newpaypay.com/center/proxy/partner/v1/queryOrder.jsp");
		curl_setopt($curl, CURLOPT_POST, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query( $parameter ));
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		$response = curl_exec($curl);
		curl_close($curl);
		/*
		S ：成功
		F ： 失败
		U ： 交易不确定
		C：已冲正
		R：全额退款
		H：部分退款
		I：未支付，默认值(Init)
		*/
		$json = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true );
		return $json;
	}

    #	取得返回串中的所有参数
    function verifyCallbackRequest()
    {
		$verifyString  = "partnerId={$_GET['partnerId']}&";
		$verifyString .= "orderId={$_GET['orderId']}&";
		$verifyString .= "amount={$_GET['amount']}&";
		$verifyString .= "result={$_GET['result']}&";
		$verifyString .= "payTime={$_GET['payTime']}&";
		$verifyString .= "traceId={$_GET['traceId']}&";
		$verifyString .= "reserve={$_GET['reserve']}&";
		$verifyString .= "creditType={$_GET['creditType']}&";
		$verifyString .= "key=".$this->merchantKey;
        return	md5($verifyString) === $_GET['md5'];
    }


    /**
     * @name 回调方法
     */
    public function callback($returnDeviceType = 'pc')
    {

		$log = [
			'GET' => json_encode($_GET),
			'POST' => json_encode($_POST),
		];
        core::logger($log);

		if (!$this->verifyCallbackRequest()) {
            die('認證錯誤');
		}

		//付款成功
        if ($_GET['result'] === 'S') {

			//反查訂單
			$orderId = intval($_GET['orderId']);
			$result = $this->checkOrder($orderId);
			if($result['msg']['result'] !== 'S') {
				die('非法请求');
			}

			$amount = $result['msg']['amount'];
            $orderReturn = '';

            require_once(SYSTEM . "/core/pay.core.php");
            $pay = new pay();
            $pay->call($amount, $orderId, $orderReturn);

            $pay_call = array(
               'PAY_CORE' => 'pay.core.php-->call',
               'amount' => $amount,
               'orderId' => 'orderId='.$orderId,
               'orderReturn' => $orderReturn,
            );
            core::logger($pay_call);
			return;

        } else {
            echo '充值失败';
            return;
        }
    }

}
