<?php
// [艾付]支付接口
class pay_aifu
{

    //real use!
    private $partner = '144790007168'; //商戶號
    private $merchantKey = 'c0c9bc2b-979f-11e7-840d-4797948f370c'; //密鑰
/*
0000	交易成功
1001	商户号未提交
1002	订单号未提交
1003	MD5摘要未提交
1004	查询商户信息异常,请稍后再试
1005	无商户信息
1006	MD5验证失败
1007	查询订单错,请稍后再试
1008	未找到订单信息
1009	返回信息数字签名失败
1010	返回信息MD5摘要失败
1011	当前订单状态不允许退款
1012	订单数据异常，请联系移联
1013	查询订单支付记录错,请稍后再试
1014	未找到订单的支付信息，请联系移联
1015	记录退款申请失败，请稍候再试
1016	记录退款交易失败，请稍候再试
1017	更新订单退款金额失败，请稍候再试
1020	更新银行支付记录状态失败，请稍候再试
1021	记录流水失败，请稍候再试
1030	退款金额未提交
1031	发送时间未提交
1032	退款金额过大，订单可退金额不足
1033	查询退款申请异常
1036	商户退款流水号未提交
1037	查询退款数据异常


*/

    // 银行代码
	// 快捷用
    private $banks = array(
        //'8' => 'CIB', // 兴业银行 v  <== 牛付沒這家銀行
		/*
		'1' => 'ALIPAY', // 支付寶 v
        '19' => 'WECHAT', // 微信/财付通 v
		'3' => 'CGBFF', // 广东发展银行v
        '4' => 'HXBFF',  // 华夏银行v
        '5' => 'COMMFF', // 交通银行 v
        '6' => 'PABFF', // 平安银行v
        '7' => 'SPDBFF', // 上海浦东发展银行v
        '9' => 'PSBCFF', // 中国邮政储蓄银行v
        '10' => 'CEBFF', // 中国光大银行v
		'11' => 'ICBCFF', // 中国工商银行 v
        '12' => 'CCBFF', // 中国建设银行 v
        '13' => 'CMSBFF', // 中国民生银行v
        '14' => 'ABCFF', // 中国农业银行 v
        '15' => 'BOCFF', // 中国银行 v
        '16' => 'CMBFF', // 招商银行v
        '17' => 'CNCBFF', // 中信银行v
		*/

		'1' => 'ALIPAY', // 支付寶 v		xxx
        '19' => 'WECHAT', // 微信/财付通 v
        '20' => 'QQSCAN', // QQ錢包
        '21' => 'JDSCAN', // 京东
		'3' => 'GDB', // 广东发展银行v
        '4' => 'HXB',  // 华夏银行v
        '5' => 'BOCOM', // 交通银行 v
        '6' => 'PAB', // 平安银行v
        '7' => 'SPDB', // 上海浦东发展银行v
        '9' => 'PSBC', // 中国邮政储蓄银行v
        '10' => 'CEB', // 中国光大银行v
		'11' => 'ICBC', // 中国工商银行 v
        '12' => 'CCB', // 中国建设银行 v
        '13' => 'CMBC', // 中国民生银行v
        '14' => 'ABC', // 中国农业银行 v
        '15' => 'BOC', // 中国银行 v
        '16' => 'CMB', // 招商银行v
        '17' => 'CNCB', // 中信银行v
		'8' => 'CIB', // 兴业银行 v
		'22' => 'BCCB', // 北京银行 v
		'23' => 'BOS', // 上海银行 v

/*

{"msg":[{"ABNAME":"农业银行","BANKID":"ABC","LOGOSRC":""},{"ABNAME":"东亚银行","BANKID":"BEA","LOGOSRC":""},{"ABNAME":"北京银行","BANKID":"BOBJ","LOGOSRC":""},{"ABNAME":"中国银行","BANKID":"BOC","LOGOSRC":""},{"ABNAME":"建设银行","BANKID":"CCB","LOGOSRC":""},{"ABNAME":"光大银行","BANKID":"CEB","LOGOSRC":""},{"ABNAME":"兴业银行","BANKID":"CIB","LOGOSRC":""},{"ABNAME":"中信银行","BANKID":"CITIC","LOGOSRC":""},{"ABNAME":"招商银行","BANKID":"CMB","LOGOSRC":""},{"ABNAME":"民生银行","BANKID":"CMBC","LOGOSRC":""},{"ABNAME":"交通银行","BANKID":"COMM","LOGOSRC":""},{"ABNAME":"广发银行","BANKID":"GDB","LOGOSRC":""},{"ABNAME":"华夏银行","BANKID":"HXB","LOGOSRC":""},{"ABNAME":"工商银行","BANKID":"ICBC","LOGOSRC":""},{"ABNAME":"南京银行","BANKID":"NJCB","LOGOSRC":""},{"ABNAME":"平安银行","BANKID":"PAB","LOGOSRC":""},{"ABNAME":"邮政储蓄","BANKID":"POSTGC","LOGOSRC":""},{"ABNAME":"浦发银行","BANKID":"SPDB","LOGOSRC":""},{"ABNAME":"上海农商银行","BANKID":"SRCB","LOGOSRC":""}]}

*/

	);

	public function getBankId() {
		global $globalBankMapping;
		$parameter = array(

            'merchant_no'	=> $this->partner,
            'mode'	=> 'WEBPAY', // WEBPAY:表示获取支持WEB网站的银行列表  WAPPAY:表示获取支持手机网站的银行列表
            //'channel'	=> 'WAPPAY', // WEBPAY:表示获取支持WEB网站的银行列表  WAPPAY:表示获取支持手机网站的银行列表
            //'rstType'	=> 'json',
        );

		//$string = "version={$parameter['version']}&partnerId={$parameter['partnerId']}&orderId={$parameter['orderId']}&goods={$parameter['goods']}&amount={$parameter['amount']}&expTime={$parameter['expTime']}&notifyUrl={$parameter['notifyUrl']}&pageUrl={$parameter['pageUrl']}&reserve={$parameter['reserve']}&extendInfo={$parameter['extendInfo']}&payMode={$parameter['payMode']}&bankId={$parameter['bankId']}&creditType={$parameter['creditType']}&key={$this->merchantKey}";
		$sign = '';
		foreach ($parameter as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$sign .= 'key='.$this->merchantKey;
		$parameter['sign'] = md5($sign);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "https://pay.ifeepay.com/gateway/queryBankList");
		curl_setopt($curl, CURLOPT_POST, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query( $parameter ));
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);
		curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
		$response = curl_exec($curl);
		//http://pay.ifeepay.com/gateway/queryBankList?merchant_no=144790007168&mode=WEBPLAY&sign=9b78e5f7c4dd292046f7f7ea6607afec

		curl_close($curl);
		//$response = mb_convert_encoding($response, "UTF-8", "GBK");
		$response = json_decode($response, true);


		$bankListArray = $response['bank_list'];

		$this->banks = array(
			'1' => 'ALIPAY', // 支付寶 v		xxx
			'19' => 'WECHAT', // 微信/财付通 v
			'20' => 'QQSCAN', // QQ錢包			
			'21' => 'JDSCAN', // 京东		
		);

		$bankMapping = array(
			'农业银行'	=> 14,
			'北京银行'	=> 22,
			//'农村商业银行'	=> ,
			'中国银行'	=> 15,
			'交通银行'	=> 5,
			'建设银行'	=> 12,
			//'银联通道'	=> ,
			'广发银行'	=> 3,
			'兴业银行'	=> 8,
			'招商银行'	=> 16,
			'民生银行'	=> 13,
			'中信银行'	=> 17,
			'华夏银行'	=> 4,
			'工商银行'	=> 11,
			'平安银行'	=> 6,
			'邮政储蓄银行'	=> 9,
			'上海银行'	=> 23,
			'浦发银行'	=> 7,
			'光大银行'	=> 10,
			//'银联通道'	=> 
		);
		$bankMapping = $globalBankMapping;

		foreach ($bankListArray as $rows) {
			$rows['BANK_NAME'] = str_replace('网关','',$rows['BANK_NAME']);
			if (isset($bankMapping[$rows['BANK_NAME']])) {
				$this->banks[$bankMapping[$rows['BANK_NAME']]] = $rows['BANK_CODE'];		
			}
		}

		//print_r($this->banks);
		//exit;

	}


    /**
     * @name 支付方法
     * @param int bankid 银行ID
     * @param int amount 充值金额
     * @param string orderid 订单ID
     * @param string url_callback 回调地址
     * @param string url_return 充值完成后返回地址
     */
    public function pay($bankid, $amount, $orderid, $url_callback, $url_return)
    {

		$this->user = unserialize($_SESSION['USER']);

		if (!$this->user['uid']) core::error('帐号ID異常');
		
		//快捷
		if ($bankid == 18) {
			//$banks = array("ICBC" => "工商银行","CMB" => "招商银行","CCB" => "建设银行","ABC" => "农业银行","BOC" => "中国银行","SPDB" => "上海浦东发展银行","BOCOM" => "交通银行","CMBC" => "民生银行","CEB" => "光大银行","GDB" => "广东发展银行","CNCB" => "中信银行","HXB" => "华夏银行","CIB" => "兴业银行","PSBC" => "邮政储蓄银行","SDB" => "深圳发展银行","BBGB" => "广西北部湾银行","BEA" => "东亚银行","CBHB" => "渤海银行","CDRCB" => "成都农村商业银行","CQRCB" => "重庆农村商业银行","DGB" => "东莞银行","DLB" => "大连银行","DYCCB" => "东营市商业银行","FDB" => "富滇银行","GZB" => "广州银行","HBB" => "河北银行","HKB" => "汉口银行","HZB" => "杭州银行","LTCCB" => "浙江泰隆商业银行","HSB" => "徽商银行","BJRCB" => "北京农商银行","HSB" => "徽商银行","HUNRCU" => "湖南农村信用社","JJB" => "九江银行","JSB" => "江苏银行","NBB" => "宁波银行","NXB" => "宁夏银行","QLB" => "齐鲁银行","QSB" => "齐商银行","RZB" => "日照银行","SCB" => "渣打银行","SDRCB" => "顺德农村商业银行","SHRCB" => "上海农村商业银行","SJB" => "盛京银行","PAB" => "平安银行","SRB" => "上饶银行","SZB" => "苏州银行","SZRCB" => "深圳农村商业银行","TACCB" => "泰安市商业银行","WHCCB" => "威海市商业银行","WLMQCCB" => "乌鲁木齐市商业银行","WZB" => "温州银行","XMB" => "厦门银行","YCCCB" => "宜昌市商业银行","ZHRCU" => "珠海市农村信用合作社","ZJCCB" => "浙商银行","ZJGRCB" => "张家港农商银行","NCB" => "南洋商业银行","SHRCB" => "上海农村商业银行","CBHB" => "渤海银行","NJCB" => "南京银行","BCCB" => "北京银行");
			//if (!array_key_exists($_POST['bank_code'],$banks)) {
			//	core::error('銀行ID異常');
			//}
		}
		else {
			$this->getBankId();
			if (!array_key_exists($bankid,$this->banks)) {
				core::error('銀行ID異常');
			}
		}

		//$product = ['QQ币', '会员充值', '移动充值', '联通充值', '电信充值', '话费', '水电费'];
		$product = ['MemberRecharge', 'MobileRecharge'];
		shuffle($product);


		//快捷
		if ($bankid == 18) {

			$parameter = array(
				'version'	=> 'v1',
				'merchant_no'	=> $this->partner,
				'order_no'	=> $orderid,
				'goods_name'		=> base64_encode($product[0]),
				'order_amount'	=> sprintf("%.2f", $amount),
				'backend_url'	=> $url_callback,
				'frontend_url'	=> $url_return,
				'reserve'=> '',
				'pay_mode' => '07',
				//'bank_code'	=> 'QUICKPAY|'.$_POST['bank_code'],
				'bank_code'	=> 'QUICKPAY',
				'card_type' => 0,
				'merchant_user_id' => 'aifu_'.$this->user['uid']
				/*
				'bank_card_no' => $_POST['bank_card_no'],
				'cardholder_name' => base64_encode($_POST['cardholder_name']),
				'id_card_no' => $_POST['id_card_no'],
				'mobile' => $_POST['mobile'],*/
			);

		}
		else {
			$parameter = array(
				'version'	=> 'v1',
				'merchant_no'	=> $this->partner,
				'order_no'	=> $orderid,
				'goods_name'		=> base64_encode($product[0]),
				'order_amount'	=> sprintf("%.2f", $amount),
				'backend_url'	=> $url_callback,
				'frontend_url'	=> $url_return,
				'reserve'=> '',
				'pay_mode'	=> ($bankid == 1 || $bankid == 19 || $bankid == 20|| $bankid == 21)? '09' : '01',
				'bank_code'	=> $this->banks[$bankid],
				'card_type' => 2,
			);
		}


		//$string = "version={$parameter['version']}&partnerId={$parameter['partnerId']}&orderId={$parameter['orderId']}&goods={$parameter['goods']}&amount={$parameter['amount']}&expTime={$parameter['expTime']}&notifyUrl={$parameter['notifyUrl']}&pageUrl={$parameter['pageUrl']}&reserve={$parameter['reserve']}&extendInfo={$parameter['extendInfo']}&payMode={$parameter['payMode']}&bankId={$parameter['bankId']}&creditType={$parameter['creditType']}&key={$this->merchantKey}";
		$sign = '';
		foreach ($parameter as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$sign .= 'key='.$this->merchantKey;


		$parameter['sign'] = md5($sign);

        core::logger($parameter);

		$middleUrl = MIDDLE_URL.(str_replace('pay_','',__CLASS__)).'.php';

		
		//支付寶或微信
		if($bankid == 1 || $bankid == 19 || $bankid == 20 || $bankid == 21) {

			$response = core::lib('middle')->onlinePay($parameter,$middleUrl);

			core::logger(['response' => trim($response)]);

			//$json = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', trim($response)), true );
			$json = json_decode( trim($response), true );
			return $json;
		}
		else {

			//使用中轉
			core::lib('middle')->bankPay($parameter,$middleUrl);
			exit;
			$html = '';
			$html .= '<html>';
			$html .= '<head>';
			$html .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
			$html .= '</head>';
			$html .= '<body onLoad="document.pay.submit();">';
			$html .= '正在跳转 ...';

			//$html .= '<form name="pay" method="GET" action="https://pay.newpaypay.com/servlet/InitBankLogoServlet">';
			$html .= '<form name="pay" method="GET" action="https://pay.newpaypay.com/center/proxy/partner/v1/pay.jsp">';
			foreach ($parameter as $key => $val) {
				$html .= '<input type="hidden" name="' . $key . '" value="' . $val . '" />';
			}
			$html .= '<script type="text/javascript">document.pay.submit();</script>';
			$html .= '</form>';
			$html .= '</body>';
			$html .= '</html>';
			return $html;
		}
    }


	//反查訂單，以防假單注入
	function checkOrder($orderid) {

		$parameter = array(
            'partnerId'	=> $this->partner,
            'orderId'	=> $orderid,
        );
		$sign = '';
		foreach ($parameter as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$sign .= 'key='.$this->merchantKey;

		$parameter['md5'] = md5($sign);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, "https://pay.newpaypay.com/center/proxy/partner/v1/queryOrder.jsp");
		curl_setopt($curl, CURLOPT_POST, false);
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query( $parameter ));
		curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
		$response = curl_exec($curl);
		curl_close($curl);
		/*
		S ：成功
		F ： 失败
		U ： 交易不确定
		C：已冲正
		R：全额退款
		H：部分退款
		I：未支付，默认值(Init)
		*/
		$json = json_decode( preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response), true );
		return $json;
	}

    #	取得返回串中的所有参数
    function verifyCallbackRequest()
    {
		$verifyString  = "merchant_no={$_GET['merchant_no']}&";
		$verifyString .= "order_no={$_GET['order_no']}&";
		$verifyString .= "order_amount={$_GET['order_amount']}&";
		$verifyString .= "original_amount={$_GET['original_amount']}&";
		$verifyString .= "upstream_settle={$_GET['upstream_settle']}&";
		$verifyString .= "result={$_GET['result']}&";
		$verifyString .= "pay_time={$_GET['pay_time']}&";
		$verifyString .= "trace_id={$_GET['trace_id']}&";
		$verifyString .= "reserve={$_GET['reserve']}&";
		$verifyString .= "key=".$this->merchantKey;
        return	md5($verifyString) === $_GET['sign'];
    }


    /**
     * @name 回调方法
     */
    public function callback($returnDeviceType = 'pc')
    {

		

		$log = [
			'GET' => json_encode($_GET),
			'POST' => json_encode($_POST),
		];

		if ($this->getUserIP() != '119.23.111.222') {
			$log['alarm_msg'] = '來源IP非法' . $this->getUserIP();
			core::logger($log);
			exit;
		}

		core::logger($log);

		if (!$this->verifyCallbackRequest()) {
			$log['alarm_msg'] = 'verifyCallbackRequest認證錯誤-充值失败';
            core::logger($log);
			die('認證錯誤-充值失败');
		}

		//付款成功
        if ($_GET['result'] === 'S') {

			//反查訂單
			$orderId = intval($_GET['order_no']);
			//$result = $this->checkOrder($orderId);
			//if($result['msg']['result'] !== 'S') {
			//	core::logger();

			//	die('非法请求');
			//}

			$amount = $_GET['order_amount'];
            $orderReturn = '';

            require_once(SYSTEM . "/core/pay.core.php");
            $pay = new pay();
            $pay->call($amount, $orderId, $orderReturn);

            $pay_call = array(
               'PAY_CORE' => 'pay.core.php-->call',
               'amount' => $amount,
               'orderId' => 'orderId='.$orderId,
               'orderReturn' => $orderReturn,
            );
            core::logger($pay_call);
			return;

        } else {
            echo '充值失败';
            return;
        }
    }

	function getUserIP()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP']))
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}

		return $ip;
	}

}
