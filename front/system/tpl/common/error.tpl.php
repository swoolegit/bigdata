<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Request Error</title>
	<meta name="description" content="">  
	<meta name="author" content="">

   <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="/static/error/css/base.css">  
   <link rel="stylesheet" href="/static/error/css/main.css">




</head>

<body>

	<!-- header 
   ================================================== -->
   <header class="main-header">
   	<div class="row">
   		<div class="logo">
	         <a href="/"></a>
	      </div>   		
   	</div>   

   </header> <!-- /header -->

	<!-- main content
   ================================================== -->
   <main id="main-404-content" class="main-content-static">

   	<div class="content-wrap">

		   <div class="shadow-overlay"></div>

		   <div class="main-content">
		   	<div class="row">
		   		<div class="col-twelve">
			  		
			  			<h1 class="kern-this">200 Error.</h1>
			  			<p>
						您请求的访问页面出错了，请查看下面的错误讯息：
			  			</p>

	<div class="body">
		<div class="container error">
			<div class="errtit"></div>
			<div class="errmsg"><?php echo $msg;?></div>
		</div>
	</div>		

			   	</div> <!-- /twelve --> 		   			
		   	</div> <!-- /row -->    		 		
		   </div> <!-- /main-content --> 

		   <footer>
		   	<div class="row">

		   		<!--div class="col-seven tab-full social-links pull-right">
			   		<ul>
				   		<li><a href="#"><i class="fa fa-facebook"></i></a></li>
					      <li><a href="#"><i class="fa fa-behance"></i></a></li>
					      <li><a href="#"><i class="fa fa-twitter"></i></a></li>
					      <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
					      <li><a href="#"><i class="fa fa-instagram"></i></a></li>   			
				   	</ul>
			   	</div-->
		   			
		  			<div class="col-five tab-full bottom-links">
			   		<ul class="links">
				   		<li><a href="/">回到首页</a></li>
				         <li><a href="javascript:history.back(-1)">回上一页</a></li>
				   	</ul>

				   	<div class="credits">
				   		<p>&copy; <?php echo date('Y');?> 大数据</p>
				   	</div>
			   	</div>   		   		

		   	</div> <!-- /row -->    		  		
		   </footer>

		</div> <!-- /content-wrap -->
   
   </main> <!-- /main-404-content -->

   <!--div id="preloader"> 
    	<div id="loader"></div>
   </div--> 

   <!-- Java Script
   ================================================== --> 
   <!--
   <script src="/static/error/js/jquery-2.1.3.min.js"></script>
   <script src="/static/error/js/main.js"></script>
	-->
</body>

</html>