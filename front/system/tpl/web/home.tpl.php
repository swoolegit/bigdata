<div id="home">
	<?php if ($this->config['webGG'] && (!array_key_exists('ntc', $_COOKIE) || $_COOKIE['ntc'] != 1)) {?>
	<div class="notice">
		<div style="float: left;width:120px;">
			<span class="close btn btn-notice icon-cancel" id="nt-close">不再显示</span>
			<span class="icon icon-volume-down"></span>
		</div>
		<div class="notice_marquee">
			<marquee direction="left" scrollamount="2" scrolldelay="1"><?php echo $this->config['webGG'];?></marquee>
		</div>
	</div>
	<?php }?>
	<div class="bonus_data">
		<div id="recent_bonus_data" style="width:65%; height:280px;"></div>
		<div id="yt_bonus_data" style="width:30%;height:224px; ">
			<div class="overview_today">
				<p class="overview_day">今日统计</p>
				<p class="overview_count" style="color:#76a4fa"><?php echo $yt_bonus_data['today']['money'];?></p>
				<p class="overview_type">盈亏</p>
				<p class="overview_count" style="color:#81c65b"><?php echo $yt_bonus_data['today']['bets'];?></p>
				<p class="overview_type">投注额</p>
			</div>
			<div class="overview_previous">
				<p class="overview_day">昨日统计</p>
				<p class="overview_count" style="color:#76a4fa"><?php echo $yt_bonus_data['yestoday']['money'];?></p>
				<p class="overview_type">盈亏</p>
				<p class="overview_count" style="color:#81c65b"><?php echo $yt_bonus_data['yestoday']['bets'];?></p>
				<p class="overview_type">投注额</p>
			</div>
			<a class="btn-noborder btn-green icon-gauge" style="width: 100%;float: left; bottom;margin-top: 33px;height: 50px;border-top: 1px solid #494949;line-height:50px;font-size: 16px;" href="/report/index" target="ajax" func="loadpage">&nbsp更多盈亏数据</a>

		</div>
	</div>
	<?php if ($this->user['type']) {?>
	<div class="agent common">
		<div class="head" style="border-bottom: 1px solid #434857;">
			<div class="name icon-sitemap">代理推广数据</div>
			<a href="javascript:;" onclick="$('#agent-spread').trigger('click');" class="link icon-link-ext">获取推广链接</a>
		</div>
		<div class="body" style="border-bottom-right-radius: 4px;border-bottom-left-radius: 4px;">
			<div class="block money">
				<span class="icon icon-yen"></span>
				<div class="value red"><?php echo $agent_data['money'];?></div>
				<div class="text">返点佣金活动总额</div>
			</div>
			<div class="block child">
				<span class="icon icon-briefcase"></span>
				<div class="value green"><?php echo $agent_data['child'];?></div>
				<div class="text">直属下线数量</div>
			</div>
			<div class="block childs">
				<span class="icon icon-suitcase"></span>
				<div class="value blue"><?php echo $agent_data['childs'];?></div>
				<sdivpan class="text">所有下线数量</div>
			</div>
		</div>
	</div>
	<?php }?>
	<div class="bet common" style="margin-top: 25px;">
		<div class="head">
			<div class="name icon-sweden">近期投注记录</div>
			<a href="javascript:;" onclick="$('#bet-log').trigger('click');" class="link icon-dot-3">更多投注记录</a>
		</div>
		<div class="body" style="border-bottom-right-radius: 4px;border-bottom-left-radius: 4px; ">
			<?php
				if ($bet_data) {
			?>
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="title">
					<td>编号</td>
					<td>投注时间</td>
					<td>彩种</td>
					<td>期号</td>
					<td>玩法</td>
					<td>总额(元)</td>
					<td>奖金(元)</td>
					<td>开奖号码</td>
					<td>状态</td>
					<td>操作</td>
				</tr>
				<?php
					foreach ($bet_data as $v) {
						$this_type = $gtypes[$v['type']];
				?>
				<tr>
					<td><a href="/bet/info?id=<?php echo $v['id'];?>" title="投注信息" target="ajax" func="loadpage"><?php echo $v['wjorderId'];?></a></td>
					<td><?php echo date('m-d H:i:s', $v['actionTime']);?></td>
					<td><?php echo array_key_exists('shortName', $this_type) ? $this_type['shortName'] : $this_type['title'];?></td>
					<td><?php echo $v['actionNo'];?></td>
					<td><?php echo $plays[$v['playedId']]['name'];?></td>
					<td><?php echo $v['actionAmount'];?></td>
					<td><?php echo $v['lotteryNo'] ? number_format($v['bonus'], 2, '.', '') : '0.00';?></td>
					<td><?php echo $v['lotteryNo'] ? $v['lotteryNo'] : '--';?></td>
					<td><?php
						if ($v['isDelete'] == 1) {
							echo '<span class="gray">已撤单</span>';
						} elseif (!$v['lotteryNo']) {
							echo '<span class="green">未开奖</span>';
						}elseif($v['zjCount']){
							echo '<span class="red">已中奖</span>';
						}else{
							echo '未中奖';
						}
					?></td>
					<td>
					<?php if ($v['lotteryNo'] || $v['isDelete']==1 || $v['kjTime'] < $this->time) { ?>
						--
					<?php } else { ?>
						<a href="javascript:;" data-id="<?php echo $v['id'];?>" remove="false" class="remove_single">撤单</a>
					<?php } ?>
					</td>
				</tr>
				<?php }?>
			</table>
			<?php } else {?>
			<div class="empty"></div>
			<?php }?>
		</div>
	</div>
</div>
<script type="text/javascript">
$(function () {
	// 绑定撤单事件
	$('.common.bet .body .remove_single').live('click', beter.remove_single);
	// 菜单下拉固定
	$.scroll_fixed('#home');
	// 近期中奖数据统计
function runchart()
{
	if($.cookie('colorfile')!='')
	{
		var myChart = echarts.init(document.getElementById('recent_bonus_data'),'vintage');
	}
	else
	{
		var myChart = echarts.init(document.getElementById('recent_bonus_data'),'macarons');
	}

	option = {
	    title: {
	        text: '近期收益统计',
            textStyle:{
            	color:'#ddd',
            	fontWeight:'normal'
            },
            left: 'center',
            padding:[20,10,10,15]
	    },
	    backgroundColor:'',
	    tooltip: {
	        trigger: 'axis'
	    },
        color:['#7CB5EC'],
        textStyle:{
        	color:'#ddd'
        },
	    xAxis:  {
	        type: 'category',
	        boundaryGap: false,
	        data: [<?php echo $recent_bonus_data['xAxis'];?>]
	    },
	    yAxis: {
	        type: 'value',
	        axisLabel: {
	            formatter: '{value} 元'
	        }
	    },
	    series: [
	        {
	            name:'近期收益',
	            type:'line',
	            data:[<?php echo $recent_bonus_data['series'];?>],
	            areaStyle:{
	            	normal:{
	            		color: '#7CB5EC'
	            	}
	            }
	        }
	    ]
	};
    myChart.setOption(option);
	window.addEventListener('resize', function () {
	myChart.resize();
	});
}
setTimeout(function() {
	runchart();
}, 300);
	
<?php
	/* 舊公告方式，不用了
	if (!isset($_SESSION['alertMsgs'])) {
		$sql = "select * from {$this->db_prefix}content where enable=2";
		$alertMsgs = $this->db->getRows($sql);
		if ($alertMsgs) foreach ($alertMsgs as $rows) {
			$content = preg_replace("/\r*\n/","<br>",htmlspecialchars($rows['content']));
			echo "$.dialogue({type: 'success',text: '{$content}',auto: true,yes: {text: '我知道了'}});";
		}
		$_SESSION['alertMsgs'] = true;
	}*/
?>

});
</script>
<?php
	$sql = "select * from {$this->db_prefix}content where enable=2 order by id desc";
	$alertMsgs = $this->db->getRows($sql);
	$html = '';
	if ($alertMsgs) {
		foreach ($alertMsgs as $rows) {
			$html .= '
				<div class="dialog_content" style="display:none">
					<div class="dialogModal_header">'.$rows['title'].'</div>
					<div class="dialogModal_content">
					'.preg_replace("/\r*\n/","<br>",htmlspecialchars($rows['content'])).'
					</div>
					<div class="dialogModal_footer">
						<!--button type="button" class="btn btn-primary" data-dialogmodal-but="next">下一页</button-->
						<button type="button" class="btn btn-default black-inputs btnc black-inputs-order" data-dialogmodal-but="cancel">我同意</button>
					</div>
				</div>
			';		
		}
		echo $html;
		echo "
<script>
$(function (){
	$('.dialog_content').dialogModal({
		topOffset: 0,
		top: '15%',
		type: 'modal',
		onOkBut: function(event, el, current) {},
		onCancelBut: function(event, el, current) {},
		onLoad: function(el, current) {},
		onClose: function(el, current) {},
		onChange: function(el, current) {}
	});
});
</script>
		
		";
	
	}

?>
