<input type="hidden" name="playedGroup" id="lucky28_playedGroup" value="" />
<input type="hidden" name="playedId" id="lucky28_playedId" value="" />
<input type="hidden" name="type" value="<?php echo $type_id;?>" />
<div class="pp pp11 nolost" action="tz11x5Multi" length="1"  >
	<div class="title28">总和值</div>
		<span class="play-info" style="padding: 0px;">
			<a href="javascript:;" class="btn btn-garydark"><span action="play-help" class="icon-help showeg">说明</span></a>
			<div id="play-help" class="play-eg hide">
				<div> 
					大小：根据开奖第一球 ~ 第五球开出的球号数字总和值来判断：总和大>=23，总和小<=22。
				</div>
				<div>
					单双：根据开奖第一球 ~ 第五球开出的球号数字总和值来判断：总和双=偶数，总和单=奇数。
				</div>				
			</div>
		</span>	
	<div style="border-bottom: 1px solid #353642; padding-bottom: 8px;height: 80px;">
	<div style="width: 800px;">
<?php
foreach ($plays as $k=>$v) 
{
	$diff_fanDian = $this->config['fanDianMax'] - $this->user['fanDian'];
	$proportion = 1 - $diff_fanDian / 100;
	$plays[$v['id']]['bonusProp'] = number_format($v['bonusProp'] * $proportion, 2, '.', '');
	$plays[$v['id']]['bonusPropBase'] = number_format($v['bonusPropBase'] * $proportion, 2, '.', '');
}
$v=$plays[360];
		$number=explode(",", $v['example']);
		$i=0;
		foreach($number as $k3=>$v3)
		{
			$style_line="";
			if($i==0)
			{
				$style_line="border-left: 1px solid #353642;border-right: 1px solid #353642;border-top: 1px solid #353642;border-bottom: 1px solid #353642;";
			}else
			{
				$style_line="border-right: 1px solid #353642;border-top: 1px solid #353642;border-bottom: 1px solid #353642;";
			}			
?>
			<div style="float: left;width: 24%;<?php echo $style_line?>">
				<div >
					<input type="button" value="<?php echo $v3;?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>
<?php
			$i++;
		}

?>
	</div>
	</div>
	<div class="title28">龙虎</div>
		<span class="play-info" style="padding: 0px;">
			<a href="javascript:;" class="btn btn-garydark"><span action="play-help1" class="icon-help showeg">说明</span></a>
			<div id="play-help1" class="play-eg hide">
				<div class="help"> 
					龙：开奖第一球(万位)的号码 > 第五球(个位)的号码。
				</div>
				<div class="help">
					虎：开奖第一球(万位)的号码 < 第五球(个位)的号码。
				</div>
				<div class="help">
					和：开奖第一球(万位)的号码 = 第五球(个位)的号码。
				</div>			
			</div>
		</span>	
	<div style="border-bottom: 1px solid #353642; padding-bottom: 8px;height: 80px;">
	<div style="width: 800px;">
		<div class="play-info" style="padding: 0px;">

		</div>
<?php
$v=$plays[361];
?>
			<div  style="float: left;width: 33%;margin-bottom: 5px;border: 1px solid #353642;">
				<div>
					<input type="button" value="<?php echo $v['example'];?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>
<?php
$v=$plays[362];
?>
			<div  style="float: left;width: 33%;margin-bottom: 5px;border-right: 1px solid #353642;border-top: 1px solid #353642;border-bottom: 1px solid #353642;">
				<div>
					<input type="button" value="<?php echo $v['example'];?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>
<?php
$v=$plays[363];
?>
			<div  style="float: left;width: 33%;margin-bottom: 5px;border-right: 1px solid #353642;border-top: 1px solid #353642;border-bottom: 1px solid #353642;">
				<div>
					<input type="button" value="<?php echo $v['example'];?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>

	</div>
	</div>

	<div class="title28">牛牛</div>
		<span class="play-info" style="padding: 0px;">
			<a href="javascript:;" class="btn btn-garydark"><span action="play-help2" class="icon-help showeg">说明</span></a>
			<div id="play-help2" class="play-eg hide">
				<div class="help"> 
					牛牛：根据开奖第一球 ~ 第五球开出的球号数字为基础，任意组合三个号码成0或10的倍数，取剩余两个号码之和为点数（大于10时减去10后的数字作为对奖基数)。
				</div>
				<div class="help">
					大小：牛大(牛6,牛7,牛8,牛9,牛牛)，牛小(牛1,牛2,牛3,牛4,牛5)
				</div>
				<div class="help">
					单双：牛单(牛1,牛3,牛5,牛7,牛9)，牛双(牛2,牛4,牛6,牛8,牛牛)
				</div>		
			</div>
		</span>	

	<div style="padding-bottom: 8px;height: 180px;">
	<div style="width: 880px;">
<?php
$v=$plays[364];
		$number=explode(",", $v['example']);
		$i=0;
		foreach($number as $k3=>$v3)
		{
			$style_line="";
			if($i==0)
			{
				$style_line="border-left: 1px solid #353642;border-right: 1px solid #353642;border-top: 1px solid #353642;";
			}else
			{
				$style_line="border-right: 1px solid #353642;border-top: 1px solid #353642;";
			}			
?>
			<div style="float: left;width: 80px;<?php echo $style_line?>">
				<div >
					<input type="button" value="<?php echo $v3;?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>
<?php
			$i++;
		}

?>
<?php
$v=$plays[365];
?>
			<div  style="float: left;width: 364px;border: 1px solid #353642;">
				<div>
					<input type="button" value="<?php echo $v['example'];?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>
<?php
$v=$plays[366];
?>
			<div  style="float: left;width: 363px;border-right: 1px solid #353642;border-top: 1px solid #353642;border-bottom: 1px solid #353642;">
				<div>
					<input type="button" value="<?php echo $v['example'];?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>
<?php
$v=$plays[367];
$number=explode(",", $v['example']);
?>
			<div  style="float: left;width: 182px;margin-bottom: 5px;border-right: 1px solid #353642;border-bottom: 1px solid #353642;border-left: 1px solid #353642;">
				<div>
					<input type="button" value="<?php echo $number[0];?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>
			<div  style="float: left;width: 181px;margin-bottom: 5px;border-right: 1px solid #353642;border-bottom: 1px solid #353642;">
				<div>
					<input type="button" value="<?php echo $number[1];?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>
			<div  style="float: left;width: 181px;margin-bottom: 5px;border-right: 1px solid #353642;border-bottom: 1px solid #353642;">
				<div>
					<input type="button" value="<?php echo $number[2];?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>
			<div  style="float: left;width: 181px;margin-bottom: 5px;border-right: 1px solid #353642;border-bottom: 1px solid #353642;">
				<div>
					<input type="button" value="<?php echo $number[3];?>" data-playedgroup="87" data-playedid="<?php echo $v['id'];?>" data-bonuspropbase="<?php echo $v['bonusPropBase'];?>" data-bonusprop="<?php echo $v['bonusProp'];?>" class="code code_box" style="width: 100%;" />
				</div>
				<div class="lucky28_pl">
					<?php echo $v['bonusProp'];?>
				</div>
			</div>

	</div>
	</div>
</div>
<script type="text/javascript">
$(function() {
	$('#btnZhuiHao').hide();
	$('#btnRandNum').hide();
	$('#fandian-value').hide();
});
</script>
<style>
	.lucky28_pl{
		color:#666;
		text-align: center;
		font-size: 10px;
		padding-top: 2px;
	}
</style>
