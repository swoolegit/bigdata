<style>
	.lhcBox dl  a.code-box{
		width: 590px;
		float: left;
		height: 34px !important;
		border-right: 1px solid #F0AD4E;
		border-bottom: 1px solid #F0AD4E;
	}
	.lhcBox span.sxcode {
		width: 590px;
	}
	.lhcBox dl  a.code-box span{
		margin-top: 4px !important;
	} 
	.lhcBox dl  a.code-box span.red{
		background: none;
		background-color: #F13131;
		border-radius: 50%;
	}
	.lhcBox dl  a.code-box span.blue{
		background: none;
		border-radius: 50%;
		background-color: #2a74dd !important;
	}
	.lhcBox dl  a.code-box span.green{
		background: none;
		border-radius: 50%;
		background-color: #1ca01c  !important;
	}
	.lhcBox a.code-box span.green {
	    margin-left: 5px;
	    width: 28px !important;
	    height: 28px !important;
	    line-height: 28px;
	    color: #fff;
	    text-align: center;
	    font-size: 14px;
	}	
	.lhcBox a.code-box span.blue {
	    margin-left: 5px;
	    width: 28px !important;
	    height: 28px !important;
	    line-height: 28px;
	    color: #fff;
	    text-align: center;
	    font-size: 14px;
	}	
	.lhcBox a.code-box span.red {
	    margin-left: 5px;
	    width: 28px !important;
	    height: 28px !important;
	    line-height: 28px;
	    color: #fff;
	    text-align: center;
	    font-size: 14px;
	}
	.lhcBox a.code-box span:last-child {
	    width: 28px !important;
	    height: 28px !important;
	}
	.lhcBox .border-r-none
	{
		width:189px !important;
	}
</style>
<input type="hidden" name="playedGroup" value="<?=$group_id?>" />
<input type="hidden" name="playedId" value="<?=$play_id?>" />
<input type="hidden" name="type" value="<?=$type_id?>" />
<div class="lotteryView_lhc pp pp11 nolost"  action="tz11x5Multi" length="1" style="border-top: 1px solid #353642;border-bottom: 0px" >
                <div class="Contentbox" id="Contentbox_0">
        <div class="lhcBox" >
		<dl style="width:760px">
		<dt>
			<span class="sp1">特码色波</span><span class="sxcode">号码</span><span class="sp2" style="width:60px">赔率</span>
		</dt>
		<dd data-value="红波" data-playedgroup="<?=$group_id?>" data-playedid="RteSPRR" data-bonuspropbase="<?=$this->getLHCRte('RteSPRR',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPRR',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">红波
			</span>
			  <a class="code-box">
				  <?php
					foreach($this->LHCXX['red'] as $k=>$v)
					{
						?>
							<span class="red"><?=$v?></span>
						<?php
					}
				  ?>
			  </a>
			<span class="num" style="width:70px"><?=$this->getLHCRte('RteSPRR',$play_id)?></span>
		</dd>
		<dd data-value="蓝波" data-playedgroup="<?=$group_id?>" data-playedid="RteSPBB" data-bonuspropbase="<?=$this->getLHCRte('RteSPBB',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPBB',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">蓝波
			</span>
			  <a class="code-box">
				  <?php
					foreach($this->LHCXX['blue'] as $k=>$v)
					{
						?>
							<span class="blue"><?=$v?></span>
						<?php
					}
				  ?>
			  </a>
			<span class="num" style="width:70px"><?=$this->getLHCRte('RteSPBB',$play_id)?></span>
		</dd>
		<dd data-value="绿波" data-playedgroup="<?=$group_id?>" data-playedid="RteSPGG" data-bonuspropbase="<?=$this->getLHCRte('RteSPGG',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPGG',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">绿波
			</span>
			  <a class="code-box">
				  <?php
					foreach($this->LHCXX['green'] as $k=>$v)
					{
						?>
							<span class="green"><?=$v?></span>
						<?php
					}
				  ?>
			  </a>
			<span class="num" style="width:70px"><?=$this->getLHCRte('RteSPGG',$play_id)?></span>
		</dd>		
		</dl>
		</div>

		<div class="lhcBox" >
			
		<dl class="border-r-none">
		<dt>
			<span class="sp1">特码半波</span><span class="sp2">赔率</span>
		</dt>
		<dd data-value="红大" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCRD" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCRD',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCRD',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">红大
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCRD',$play_id)?></span>
		</dd>
		<dd data-value="蓝大" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCBD" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCBD',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCBD',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">蓝大
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCBD',$play_id)?></span>
		</dd>
		<dd data-value="绿大" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCGD" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCGD',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCGD',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">绿大
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCGD',$play_id)?></span>
		</dd>
		</dl>
		
		<dl class="border-r-none">
		<dt>
			<span class="sp1">特码半波</span><span class="sp2">赔率</span>
		</dt>
		<dd data-value="红小" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCRS" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCRS',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCRS',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">红小
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCRS',$play_id)?></span>
		</dd>
		<dd data-value="蓝小" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCBS" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCBS',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCBS',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">蓝小
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCBS',$play_id)?></span>
		</dd>
		<dd data-value="绿小" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCGS" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCGS',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCGS',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">绿小
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCGS',$play_id)?></span>
		</dd>
		</dl>
		<dl class="border-r-none">
		<dt>
			<span class="sp1">特码半波</span><span class="sp2">赔率</span>
		</dt>
		<dd data-value="红单" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCRO" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCRO',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCRO',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">红单
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCRO',$play_id)?></span>
		</dd>
		<dd data-value="蓝单" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCBO" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCBO',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCBO',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">蓝单
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCBO',$play_id)?></span>
		</dd>
		<dd data-value="绿单" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCGO" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCGO',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCGO',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">绿单
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCGO',$play_id)?></span>
		</dd>
		</dl>
		<dl class="border-r-none">
		<dt>
			<span class="sp1">特码半波</span><span class="sp2">赔率</span>
		</dt>
		<dd data-value="红双" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCRE" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCRE',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCRE',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">红双
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCRE',$play_id)?></span>
		</dd>
		<dd data-value="蓝双" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCBE" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCBE',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCBE',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">蓝双
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCBE',$play_id)?></span>
		</dd>
		<dd data-value="绿双" data-playedgroup="<?=$group_id?>" data-playedid="RteSPHCGE" data-bonuspropbase="<?=$this->getLHCRte('RteSPHCGE',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPHCGE',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">绿双
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPHCGE',$play_id)?></span>
		</dd>
		</dl>
		
		</div>
		<div class="lhcBox" >
		<dl  class="border-r-none">
		<dt>
			<span class="sp1">特码半半波</span><span class="sp2">赔率</span>
		</dt>
		<dd data-value="红大单" data-playedgroup="<?=$group_id?>" data-playedid="RteSPRDO" data-bonuspropbase="<?=$this->getLHCRte('RteSPRDO',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPRDO',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">红大单
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPRDO',$play_id)?></span>
		</dd>
		<dd data-value="蓝大单" data-playedgroup="<?=$group_id?>" data-playedid="RteSPBDO" data-bonuspropbase="<?=$this->getLHCRte('RteSPBDO',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPBDO',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">蓝大单
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPBDO',$play_id)?></span>
		</dd>
		<dd data-value="绿大单" data-playedgroup="<?=$group_id?>" data-playedid="RteSPGDO" data-bonuspropbase="<?=$this->getLHCRte('RteSPGDO',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPGDO',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">绿大单
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPGDO',$play_id)?></span>
		</dd>
		</dl>
		<dl  class="border-r-none">
		<dt>
			<span class="sp1">特码半半波</span><span class="sp2">赔率</span>
		</dt>
		<dd data-value="红大双" data-playedgroup="<?=$group_id?>" data-playedid="RteSPRDE" data-bonuspropbase="<?=$this->getLHCRte('RteSPRDE',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPRDE',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">红大双
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPRDE',$play_id)?></span>
		</dd>
		<dd data-value="蓝大双" data-playedgroup="<?=$group_id?>" data-playedid="RteSPBDE" data-bonuspropbase="<?=$this->getLHCRte('RteSPBDE',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPBDE',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">蓝大双
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPBDE',$play_id)?></span>
		</dd>
		<dd data-value="绿大双" data-playedgroup="<?=$group_id?>" data-playedid="RteSPGDE" data-bonuspropbase="<?=$this->getLHCRte('RteSPGDE',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPGDE',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">绿大双
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPGDE',$play_id)?></span>
		</dd>
		</dl>
		<dl  class="border-r-none">
		<dt>
			<span class="sp1">特码半半波</span><span class="sp2">赔率</span>
		</dt>
		<dd data-value="红小单" data-playedgroup="<?=$group_id?>" data-playedid="RteSPRSO" data-bonuspropbase="<?=$this->getLHCRte('RteSPRSO',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPRSO',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">红小单
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPRSO',$play_id)?></span>
		</dd>
		<dd data-value="蓝小单" data-playedgroup="<?=$group_id?>" data-playedid="RteSPBSO" data-bonuspropbase="<?=$this->getLHCRte('RteSPBSO',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPBSO',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">蓝小单
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPBSO',$play_id)?></span>
		</dd>
		<dd data-value="绿小单" data-playedgroup="<?=$group_id?>" data-playedid="RteSPGSO" data-bonuspropbase="<?=$this->getLHCRte('RteSPGSO',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPGSO',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">绿小单
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPGSO',$play_id)?></span>
		</dd>
		</dl>
		<dl  class="border-r-none">
		<dt>
			<span class="sp1">特码半半波</span><span class="sp2">赔率</span>
		</dt>
		<dd data-value="红小双" data-playedgroup="<?=$group_id?>" data-playedid="RteSPRSE" data-bonuspropbase="<?=$this->getLHCRte('RteSPRSE',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPRSE',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">红小双
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPRSE',$play_id)?></span>
		</dd>
		<dd data-value="蓝小双" data-playedgroup="<?=$group_id?>" data-playedid="RteSPBSE" data-bonuspropbase="<?=$this->getLHCRte('RteSPBSE',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPBSE',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">蓝小双
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPBSE',$play_id)?></span>
		</dd>
		<dd data-value="绿小双" data-playedgroup="<?=$group_id?>" data-playedid="RteSPGSE" data-bonuspropbase="<?=$this->getLHCRte('RteSPGSE',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteSPGSE',$play_id)?>" class="code_lhc">
			<span class="sGameStatusItem">绿小双
			</span>
			<span class="num" ><?=$this->getLHCRte('RteSPGSE',$play_id)?></span>
		</dd>
		</dl>
    
		</div>

        </div>
        </div>
