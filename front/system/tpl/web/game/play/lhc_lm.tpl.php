<input type="hidden" name="playedGroup" value="<?=$group_id?>" />
<input type="hidden" name="playedId" value="<?=$play_id?>" />
<input type="hidden" name="type" value="<?=$type_id?>" />
<style>
	.lotteryView_lhc .lhcBox dl{
		width: 187px !important;
	}
	.lotteryView_lhc td{
		color:#000000;
	}
	.lotteryView_lhc .lhcBox span {
		width: 65px !important;
	}
	.lotteryView_lhc .lhcBox span:last-child {
		width: 120px !important;
	}
	.lotteryView_lhc .lhcBox span.input {
		width: 120px !important;
	}
	.u-table2{
		width:100%;
		text-align: center;
		border: 1px solid #000000;
		border-bottom:none;
		margin-top: 10px;
	}
	.u-table2 thead th {
	    background: #f8f8f8 !important;
	    color: #272727 !important;
	    /*border: 1px solid #d6d6d6;*/
	    border-right: 1px solid #000000;
	    border-bottom: 1px solid #000000;
	}
	.u-table2 thead th:last-child {
		border-right:none;
	}
	.u-table2 .name {
    	background-color: #f8f8f8;
	}
	.u-table2 td {
	    border-right: 1px solid #000000;
	    border-bottom: 1px solid #000000;
	    color: #1c1c1c;
	}
	.u-table2 td:last-child {
		border-right:none;
	}
</style>
<div class="lotteryView_lhc pp pp11 nolost"  action="tzlhcSelect" length="1" style="border-top: 1px solid #353642;border-bottom: 0px" >

        <div class="Contentbox" id="Contentbox_0">
        <div class="dGameStatus">
				
        <div class="lhcBox" >
	     <table class="u-table2" cellpadding="5" cellspacing="0">
	      <thead id="lm_radios">
	       <tr>
	        <th class="radio_td" colspan="2"><input id="RteLM2OF31" type="radio" value="LM2OF31" rel="3" data-value="三中二" data-playedgroup="<?=$group_id?>" data-playedid="RteLM2OF31" data-bonuspropbase="<?=$this->getLHCRte('RteLM2OF31',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteLM2OF31',$play_id)?>" name="txtGameItem" class="inputnoborder"></th>
	        <th class="radio_td"><input id="RteLM3OF3" type="radio" value="LM3OF3" rel="3" data-value="三全中" data-playedgroup="<?=$group_id?>" data-playedid="RteLM3OF3" data-bonuspropbase="<?=$this->getLHCRte('RteLM3OF3',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteLM3OF3',$play_id)?>" name="txtGameItem" class="inputnoborder"></th>
	        <th class="radio_td"><input id="RteLM2OF2" type="radio" value="LM2OF2" rel="2" data-value="二全中" data-playedgroup="<?=$group_id?>" data-playedid="RteLM2OF2" data-bonuspropbase="<?=$this->getLHCRte('RteLM2OF2',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteLM2OF2',$play_id)?>" name="txtGameItem" class="inputnoborder"></th>
	
	        <th class="radio_td" colspan="2"><input id="RteLMSPOF21" type="radio" value="LMSPOF21" rel="2" data-value="二中特" data-playedgroup="<?=$group_id?>" data-playedid="RteLMSPOF21" data-bonuspropbase="<?=$this->getLHCRte('RteLMSPOF21',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteLMSPOF21',$play_id)?>" name="txtGameItem" class="inputnoborder"></th>
	        <th class="radio_td"><input id="RteLMSPOF" type="radio" value="LMSPOF" name="txtGameItem" rel="2" data-value="特串" data-playedgroup="<?=$group_id?>" data-playedid="RteLMSPOF" data-bonuspropbase="<?=$this->getLHCRte('RteLMSPOF',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteLMSPOF',$play_id)?>" class="inputnoborder"></th>
	        <th class="radio_td"><input  id="RteLM4OF4" type="radio" value="LM4OF4" rel="4"  data-value="四全中" data-playedgroup="<?=$group_id?>" data-playedid="RteLM4OF4" data-bonuspropbase="<?=$this->getLHCRte('RteLM4OF4',$play_id)?>" data-bonusprop="<?=$this->getLHCRte('RteLM4OF4',$play_id)?>" name="txtGameItem" class="inputnoborder"></th>
	       </tr>
	      </thead>
	      <tbody>
	       <tr>
	        <td class="name not-event" id="play_name_7010001" colspan="2">三中二</td>
	        <td class="name not-event" id="play_name_7010002">三全中</td>
	        <td class="name not-event" id="play_name_7010003">二全中</td>
	
	        <td class="name not-event" id="play_name_7010004" colspan="2">二中特</td>
	        <td class="name not-event" id="play_name_7010005">特串</td>
	        <td class="name not-event" id="play_name_7010006">四全中</td>
	       </tr>
	       <tr>
	        <td class="odds not-event" id="play_odds_7010001"><span class="c-txt3"><?=$this->getLHCRte('RteLM2OF31',$play_id)?></span></td>
	        <td class="odds not-event" id="play_odds_7010002"><span class="c-txt3"><?=$this->getLHCRte('RteLM2OF32',$play_id)?></span></td>
	
	        <td class="odds not-event" id="play_odds_7010003"><span class="c-txt3"><?=$this->getLHCRte('RteLM3OF3',$play_id)?></span></td>
	        <td class="odds not-event" id="play_odds_7010004"><span class="c-txt3"><?=$this->getLHCRte('RteLM2OF2',$play_id)?></span></td>
	
	        <td class="odds not-event" id="play_odds_7010005"><span class="c-txt3"><?=$this->getLHCRte('RteLMSPOF21',$play_id)?></span></td>
	        <td class="odds not-event" id="play_odds_7010006"><span class="c-txt3"><?=$this->getLHCRte('RteLMSPOF22',$play_id)?></span></td>
	
	        <td class="odds not-event" id="play_odds_7010007"><span class="c-txt3"><?=$this->getLHCRte('RteLMSPOF',$play_id)?></span></td>
	        <td class="odds not-event" id="play_odds_7010008"><span class="c-txt3"><?=$this->getLHCRte('RteLM4OF4',$play_id)?></span></td>
	
	       </tr>
	      </tbody>
	      </table>
		</div>
    
        <div class="lhcBox" >	  
		<dl>
			<dt>
		<span>号码</span><span >选择</span>
			</dt>

<?php
$key="";
for($i=1;$i<11;$i++)
{
	if($i<10)
	{
		$key="0".$i;
	}else
	{
		$key=$i;
	}
?>
		<dd>
			<span><div class="<?=$this->getLHCColorNum($key);?>"><?=$key?></div></span>
			<span class="input"><input type="checkbox" value="<?=$key?>" id="checkbox_<?=$key?>" name="checkbox" class="inputnoborder" acno="<?=$key?>"></span>
		</dd>
<?php
}
?>
		</dl>
		
		<dl>
			<dt>
		<span>号码</span><span >选择</span>
			</dt>
<?php
$key="";
for($i=11;$i<21;$i++)
{
	if($i<10)
	{
		$key="0".$i;
	}else
	{
		$key=$i;
	}
?>
		<dd>
			<span><div class="<?=$this->getLHCColorNum($key);?>"><?=$key?></div></span>
			<span class="input"><input type="checkbox" value="<?=$key?>" id="checkbox_<?=$key?>" name="checkbox" class="inputnoborder" acno="<?=$key?>"></span>
		</dd>
<?php
}
?>
		</dl>
		<dl>
			<dt>
		<span>号码</span><span >选择</span>
			</dt>
<?php
$key="";
for($i=21;$i<31;$i++)
{
	if($i<10)
	{
		$key="0".$i;
	}else
	{
		$key=$i;
	}
?>
		<dd>
			<span><div class="<?=$this->getLHCColorNum($key);?>"><?=$key?></div></span>
			<span class="input"><input type="checkbox" value="<?=$key?>" id="checkbox_<?=$key?>" name="checkbox" class="inputnoborder" acno="<?=$key?>"></span>
		</dd>
<?php
}
?>
		</dl>
		<dl>
			<dt>
		<span>号码</span><span >选择</span>
			</dt>
<?php
$key="";
for($i=31;$i<41;$i++)
{
	if($i<10)
	{
		$key="0".$i;
	}else
	{
		$key=$i;
	}
?>
		<dd>
			<span><div class="<?=$this->getLHCColorNum($key);?>"><?=$key?></div></span>
			<span class="input"><input type="checkbox" value="<?=$key?>" id="checkbox_<?=$key?>" name="checkbox" class="inputnoborder" acno="<?=$key?>"></span>
		</dd>
<?php
}
?>
		</dl>
		<dl>
			<dt>
		<span>号码</span><span>选择</span>
			</dt>
<?php
$key="";
for($i=41;$i<50;$i++)
{
	if($i<10)
	{
		$key="0".$i;
	}else
	{
		$key=$i;
	}
?>
		<dd>
			<span><div class="<?=$this->getLHCColorNum($key);?>"><?=$key?></div></span>
			<span class="input"><input type="checkbox" value="<?=$key?>" id="checkbox_<?=$key?>" name="checkbox" class="inputnoborder" acno="<?=$key?>"></span>
		</dd>
<?php
}
?>		
		<dd style="height: 34px; border-bottom: 1px solid #F0AD4E;">
		</dd>
		</dl>
	    </div>
	        
		</div>
		</div>
		</div>
		</div>