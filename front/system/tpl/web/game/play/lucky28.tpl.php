<input type="hidden" name="playedGroup" id="lucky28_playedGroup" value="" />
<input type="hidden" name="playedId" id="lucky28_playedId" value="" />
<input type="hidden" name="type" value="<?php echo $type_id;?>" />
<div class="pp pp11 nolost" action="tz11x5Multi" length="1" style="border-top: 1px solid #353642;border-bottom: 0px" >
	<div class="title28">和值</div>
	<div style="border-bottom: 1px solid #353642; padding-bottom: 25px;height: 120px;">
	<?php
	$i_color=array('color_red','color_green','color_blue');
	$i=0;
	foreach($plays[84] as $k2=>$v2)
	{
		$get_color="";
		if($i!=0 && $i!=13 && $i!=14 && $i!=27)
		{
			$get_color=$i_color[($i%3)];
		}
	?>	<div class="lucky28_pl" style="float: left;width: 7%;margin-bottom: 5px;">
			<div>
				<input type="button" value="<?php echo $v2['example'];?>" data-playedName="<?=$v2['name']?>" data-playedgroup="84" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" class="code <?php echo $get_color; ?>" style="margin-right: 0px;" />
			</div>
			<div class="lucky28_pl">
				<?php echo $v2['bonusProp'];?>
			</div>
		</div>
	<?php
		$i++;
	}
	?>
	</div>
	<div class="title28">两面</div>
	<div style="border-bottom: 1px solid #353642; padding-bottom: 12px;height: 180px;">
	<div style="width: 800px;">
	<?php
	foreach($plays[85] as $k2=>$v2)
	{
		$number=explode(",", $v2['example']);
		$i=0;
		$style_line="";
		foreach($number as $k3=>$v3)
		{
			if($k2==354 || $k2==368 || $k2==369 || $k2==370 || $k2==371)
			{					
				if($i==0)
				{
					$style_line="border-left: 0px solid #353642;border-right: 0px solid #353642;border-top: 1px solid #353642;";
				}else
				{
					$style_line="border-right: 0px solid #353642;border-top: 1px solid #353642;";
				}			
				?>
				<div style="float: left;width: 24%;<?php echo $style_line?>">
					<div >
						<input type="button" value="<?php echo $v3;?>" data-playedName="<?=$v2['name']?>" data-playedgroup="85" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" class="code code_box" style="width: 100%;" />
					</div>
					<div class="lucky28_pl">
						<?php echo $v2['bonusProp'];?>
					</div>
				</div>
			<?php
			}
			else
			{
				if($i==0)
				{
					$style_line="width: 385px;border-left: 1px solid #353642;border-right: 1px solid #353642;border-bottom: 1px solid #353642;border-top: 1px solid #353642;";
				}else
				{
					$style_line="width: 385px;border-right: 1px solid #353642;border-bottom: 1px solid #353642;border-top: 1px solid #353642;";
				}			
				?>
				<div style="float: left;<?php echo $style_line?>">
					<div>
						<input type="button" value="<?php echo $v3;?>" data-playedName="<?=$v2['name']?>" data-playedgroup="85" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" class="code code_box" style="width: 100%;" />
					</div>
					<div class="lucky28_pl">
						<?php echo $v2['bonusProp'];?>
					</div>
				</div>							
			<?php
			}
			$i++;
		}
	}
	?>
	</div>
	</div>
	<div class="title28">其他</div>
	<div style="border-bottom: 1px solid #353642; padding-bottom: 12px;height: 120px;">
	<div style="width: 600px;">
	<?php
	foreach($plays[86] as $k2=>$v2)
	{
		$number=explode(",", $v2['example']);
		$i=0;
		$style_line="";
		foreach($number as $k3=>$v3)
		{
			switch(true)
			{
				case $k2==357:
					if($i==0)
					{
						$style_line="border-left: 1px solid #353642;border-right: 1px solid #353642;border-top: 1px solid #353642;";
					}else
					{
						$style_line="border-right: 1px solid #353642;border-top: 1px solid #353642;";
					}
					?>
					<div style="float: left;width: 33%;<?php echo $style_line?>">
						<div>
							<input type="button" value="<?php echo $v3;?>" data-playedName="<?=$v2['name']?>" data-playedgroup="86" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" class="code code_box <?php echo $i_color[$i]; ?>" style="width: 100%;" />
						</div>
						<div class="lucky28_pl">
							<?php echo $v2['bonusProp'];?>
						</div>
					</div>
					<?php
					break;
				case $k2==358:
					?>
					<div style="float: left;width: 198px;border-left: 1px solid #353642;border-right: 1px solid #353642;border-bottom: 1px solid #353642;border-top: 1px solid #353642;height: 63px;">
						<div>
							<input type="button" value="<?php echo $v3;?>" data-playedName="<?=$v2['name']?>" data-playedgroup="86" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" class="code code_box" style="width: 100%;height: 42px;" />
						</div>
						<div class="lucky28_pl">
							<?php echo $v2['bonusProp'];?>
						</div>
					</div>
					<?php
					break;				
				case $k2==359:
					?>
					<div style="float: left;width: 397px;border-right: 1px solid #353642;border-bottom: 1px solid #353642;border-top: 1px solid #353642;height: 63px;" >
						<div id="lucky28r3"  class="code code_box"  style="width:100%;">
							<?php echo $v3;?>&nbsp;<select id="num1" class="code code_box" style="width:70px;float: right;"></select><select id="num2" class="code code_box" style="width:70px;float: right;"></select><select id="num3" data-playedgroup="86" data-playedid="<?php echo $v2['id'];?>" data-bonuspropbase="<?php echo $v2['bonusPropBase'];?>" data-bonusprop="<?php echo $v2['bonusProp'];?>" data-playedName="<?=$v2['name']?>" class="code code_box" style="width:70px;float: right;"></select>
						</div>
						<div class="lucky28_pl">
							<?php echo $v2['bonusProp'];?>
						</div>
					</div>
					<?php
					break;
			}
			$i++;
		}
	}
	?>
	</div>
	</div>
</div>
<script type="text/javascript">
function reset_option()
{
	for(var i=0;i<28;i++)
	{
		$('#num1 option')[i].hidden=false;
		$('#num2 option')[i].hidden=false;
		$('#num3 option')[i].hidden=false;		
	}
	var selectIndex = $('#num1').find(":selected").index();
	if(selectIndex > -1)
	{
		$('#num2 option')[selectIndex].hidden=true;
		$('#num3 option')[selectIndex].hidden=true;		
	}
	var selectIndex2 = $('#num2').find(":selected").index();
	if(selectIndex2 > -1)
	{
		$('#num1 option')[selectIndex2].hidden=true;
		$('#num3 option')[selectIndex2].hidden=true;
	}
	var selectIndex3 = $('#num3').find(":selected").index();
	if(selectIndex3 > -1)
	{
		$('#num1 option')[selectIndex3].hidden=true;
		$('#num2 option')[selectIndex3].hidden=true;
	}		
}
$(function() {
	for(var i=0;i<28;i++)
	{
		$('#num1').append($("<option></option>").attr("value",i).text(i)); 
		$('#num2').append($("<option></option>").attr("value",i).text(i));
		$('#num3').append($("<option></option>").attr("value",i).text(i));
	}
	$('#num1 option')[0].selected=true;
	$('#num2 option')[1].selected=true;
	$('#num3 option')[2].selected=true;
	reset_option();
	// 选号按钮点击事件
	$('#num-select select.code').live('change', function() {
		reset_option();
		var call = $(this).attr('action');
		if (call && $.isFunction(call = lottery.select_funcs[call])) {
			call.call(this, $(this).parent());
		} else {
			$('#num1').addClass('checked');
			$('#num2').addClass('checked');
			$('#num3').addClass('checked');
		}
		// 重新计算总预投注数和金额
		lottery.prepare_bets();
	});
	$('#btnZhuiHao').hide();
	$('#btnRandNum').hide();
	$('#fandian-value').hide();
});
</script>
<style>
	.lucky28_pl{
		color:#666;
		text-align: center;
		font-size: 10px;
		padding-top: 2px;
	}
</style>
