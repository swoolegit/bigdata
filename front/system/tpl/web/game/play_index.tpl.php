<?php
	if($type_type<>11 && $group_id<>87 && $group_id<>88)
	{
?>
<div class="play-list">
	<?php
		foreach ($plays as $play) {
			$class = $play_id == $play['id'] ? ' class="on"' : '';
	?>
	<span><a data-id="<?php echo $play['id'];?>" href="javascript:;"<?php echo $class;?>><?php echo $play['name'];?></a></span>
	<?php }?>
</div>
<?php
	}
?>
<div id="play-data">
	<?php require(TPL.'/game/play_data.tpl.php');?>
</div>
