<?php
if ($lottery) {
    $_lottery = $lottery;
} else {
    $_lottery = array();
    for ($i = 0; $i < 100; $i++) array_push($_lottery, 0);
}
?>
<div class="info">
    <div class="info_1">
    	<span class="icon-list-alt" style="float: left;padding-top: 5px;margin-right: 5px;"><?php echo $type_this['title']; ?></span>
    	<div style="font-size: 12px;vertical-align: text-top;">
    		<?php
        $analysis_type_ids = array(1, 3, 5, 6, 7, 9, 10, 12, 14, 26, 15, 16, 25, 60,63,53,64,20,61,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89);
        if (in_array($type_id, $analysis_type_ids)) {
            ?>
            <a href="/zst/index.php?typeid=<?php echo $type_id; ?>" target="_blank"
               class="gray icon-target">号码走势</a>
        <?php }?>
		</div>
		<div style="font-size: 12px;">
			<a href="javascript:;" class="gray" onclick="voice.switcher()"><span id="voice" class="icon-volume-down">关闭声音</span></a>
        </div>
	    <div class="last">
	        <div class="name">第 <span id="last_action_no"><?php echo $current['actionNo']; ?></span> 期投注截止计时
	        </div>
	        <div id="timer_lottery">
				<div class="dowebok">
<?php
if($type_id==90)
{
?>
					<div class="days"></div>
					<div class="colon">-</div>
<?php	
}
?>
					<div class="hours"></div>
					<div class="colon">:</div>
					<div class="minutes"></div>
					<div class="colon">:</div>
					<div class="seconds"></div>
				</div>
	        </div>
	    </div>
    </div>
    <div class="info_2" >
	    <div class="current block">
	        <div class="current_award icon-award" ><span id="lottery-current-text">第 <?php echo $last['actionNo']; ?> 期<span
	                    class="val">开奖号码</span></span></div>
<?php
switch(true)
{
	case $types[$type_id]['type'] == 1:
?>
	            <div class="num_right kj-hao" ctype="ssc">
	                <em class="num_red_b ball_0"><?php echo intval($_lottery[0]); ?></em>
	                <em class="num_red_b ball_1"><?php echo intval($_lottery[1]); ?></em>
	                <em class="num_red_b ball_2"><?php echo intval($_lottery[2]); ?></em>
	                <em class="num_red_b ball_3"><?php echo intval($_lottery[3]); ?></em>
	                <em class="num_red_b ball_4"><?php echo intval($_lottery[4]); ?></em>
	            </div>
<?php
		break;
	case $types[$type_id]['type'] == 4:
?>
	            <div class="num_right_new kj-hao" ctype="kl10">
	                <em class="num_red_b_new ball_01"> <?php echo $_lottery[0]; ?> </em>
	                <em class="num_red_b_new ball_02"> <?php echo $_lottery[1]; ?> </em>
	                <em class="num_red_b_new ball_03"> <?php echo $_lottery[2]; ?> </em>
	                <em class="num_red_b_new ball_04"> <?php echo $_lottery[3]; ?> </em>
	                <em class="num_red_b_new ball_01"> <?php echo $_lottery[4]; ?> </em>
	                <em class="num_red_b_new ball_02"> <?php echo $_lottery[5]; ?> </em>
	                <em class="num_red_b_new ball_03"> <?php echo $_lottery[6]; ?> </em>
	                <em class="num_red_b_new ball_04"> <?php echo $_lottery[7]; ?> </em>
	            </div>
<?php
		break;
	case $types[$type_id]['type'] == 2:
?>
	            <div class="num_right  kj-hao" ctype="11x5">
	                <em class="num_red_b ball_0"><?php echo intval($_lottery[0]); ?> </em>
	                <em class="num_red_b ball_1"><?php echo intval($_lottery[1]); ?> </em>
	                <em class="num_red_b ball_2"><?php echo intval($_lottery[2]); ?> </em>
	                <em class="num_red_b ball_3"><?php echo intval($_lottery[3]); ?> </em>
	                <em class="num_red_b ball_4"><?php echo intval($_lottery[4]); ?> </em>
	            </div>
<?php
		break;
	case $types[$type_id]['type'] == 9:
?>
	            <div class="num_right_k3 kj-hao" ctype="k3">
	                <em class="num_red_b_k3  ball_0"><?php echo intval($_lottery[0]); ?> </em>
	                <em class="num_red_b_k3  ball_1"><?php echo intval($_lottery[1]); ?> </em>
	                <em class="num_red_b_k3  ball_2"><?php echo intval($_lottery[2]); ?> </em>
	            </div>
<?php
		break;
	case $types[$type_id]['type'] == 6:
?>
	            <div class="num_right_pk10 kj-hao" ctype="pk10">
	                <em class="num_red_b_pk10 ball_01"> <?php echo $_lottery[0]; ?> </em>
	                <em class="num_red_b_pk10 ball_02"> <?php echo $_lottery[1]; ?> </em>
	                <em class="num_red_b_pk10 ball_03"> <?php echo $_lottery[2]; ?> </em>
	                <em class="num_red_b_pk10 ball_04"> <?php echo $_lottery[3]; ?> </em>
	                <em class="num_red_b_pk10 ball_01"> <?php echo $_lottery[4]; ?> </em>
	                <em class="num_red_b_pk10 ball_02"> <?php echo $_lottery[5]; ?> </em>
	                <em class="num_red_b_pk10 ball_03"> <?php echo $_lottery[6]; ?> </em>
	                <em class="num_red_b_pk10 ball_04"> <?php echo $_lottery[7]; ?> </em>
	                <em class="num_red_b_pk10 ball_01"> <?php echo $_lottery[8]; ?> </em>
	                <em class="num_red_b_pk10 ball_02"> <?php echo $_lottery[9]; ?> </em>
	            </div>
<?php
		break;
	case $types[$type_id]['type'] == 8:
?>
	            <div class="num_right_kl kj-hao" ctype="g1" cnum="80">
	                <em id="span_lot_0" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[0]; ?> </em>
	                <em id="span_lot_1" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[1]; ?> </em>
	                <em id="span_lot_2" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[2]; ?> </em>
	                <em id="span_lot_3" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[3]; ?> </em>
	                <em id="span_lot_4" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[4]; ?> </em>
	                <em id="span_lot_5" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[5]; ?> </em>
	                <em id="span_lot_6" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[6]; ?> </em>
	                <em id="span_lot_7" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[7]; ?> </em>
	                <em id="span_lot_8" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[8]; ?> </em>
	                <em id="span_lot_9" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[9]; ?> </em>
	                <em id="span_lot_10" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[10]; ?> </em>
	                <em id="span_lot_11" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[11]; ?> </em>
	                <em id="span_lot_12" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[12]; ?> </em>
	                <em id="span_lot_13" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[13]; ?> </em>
	                <em id="span_lot_14" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[14]; ?> </em>
	                <em id="span_lot_15" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[15]; ?> </em>
	                <em id="span_lot_16" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[16]; ?> </em>
	                <em id="span_lot_17" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[17]; ?> </em>
	                <em id="span_lot_18" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[18]; ?> </em>
	                <em id="span_lot_19" class="num_red_b_kl gr_s gr_s020"> <?php echo $_lottery[19]; ?> </em>
	            </div>
<?php
		break;
	case $types[$type_id]['type'] == 3:
	        	if($types[$type_id]['name'] == 'ssc-plw')
				{ ?>
				<div class="num_right kj-hao" ctype="ssc">
	                <em class="num_red_b ball_0"><?php echo intval($_lottery[0]); ?></em>
	                <em class="num_red_b ball_1"><?php echo intval($_lottery[1]); ?></em>
	                <em class="num_red_b ball_2"><?php echo intval($_lottery[2]); ?></em>
	                <em class="num_red_b ball_3"><?php echo intval($_lottery[3]); ?></em>
	                <em class="num_red_b ball_4"><?php echo intval($_lottery[4]); ?></em>
	            </div>
				<?php }else{ ?>
	            <div class="num_right_3d  kj-hao" ctype="3d">
	                <em class="num_red_b ball_0"><?php echo intval($_lottery[0]); ?> </em>
	                <em class="num_red_b ball_1"><?php echo intval($_lottery[1]); ?> </em>
	                <em class="num_red_b ball_2"><?php echo intval($_lottery[2]); ?> </em>
	            </div>

<?php
				}
		break;
	case $types[$type_id]['type'] == 11:
		$map = array(
			'color_red'=>'3,6,9,12,15,18,21,24',
			'color_green'=>'1,4,7,10,16,19,22,25',
			'color_blue'=>'2,5,8,11,17,20,23,26'
		);
		$set_color="";
		foreach($map as $k=>$v)
		{
			$_narray = explode(',',$v);
			if(in_array($_lottery[3],$_narray)) 
			{
				$set_color=$k;
				break;
			}
		}
?>
	            <div class="num_right  kj-hao" ctype="lucky28">
	            	<ul>
	            		<li><em class="num_red_b ball_0"><?php echo intval($_lottery[0]); ?></em><span class="num_right_lucky28">+</span></li>
	            		<li><em class="num_red_b ball_1"><?php echo intval($_lottery[1]); ?></em><span class="num_right_lucky28">+</span></li>
	            		<li><em class="num_red_b ball_2"><?php echo intval($_lottery[2]); ?></em><span class="num_right_lucky28">=</span></li>
	            		<li><em class="num_red_b ball_3 <?php echo $set_color;?>"><?php echo intval($_lottery[3]); ?></em></li>
	            	</ul>
	            </div>
<?php
		break;
}
?>	                    

	    </div>
    </div>
    <div class="info_3" >
	    <div class="history block"><div  id="slimScroll">
	        <table width="100%" cellpadding="0" cellspacing="0">
	            <tr class="head">
	                <td>最近期号</td>
	                <td>开奖号码</td>
	            </tr>
	            <?php foreach ($history as $key => $var) { 
					$_strlottery='<span>'.implode('</span><span>',explode(',',$var['data'])).'</span>';
					if($types[$type_id]['type'] == 11)
					{
						$_narray=explode(',',$var['data']);
						$_strlottery='<span>'.$_narray[0].'</span>+ <span>'.$_narray[1].'</span>+ <span>'.$_narray[2].'</span>= <span>'.$_narray[3].'</span>';
						$_substr="";
						if($_narray[3]>=14) $_substr="大";
						if($_narray[3]<=13) $_substr="小";
						if($_narray[3]>=22) $_substr="极大";
						if($_narray[3]<=5) $_substr="极小";
						$_substr.=($_narray[3]%2)?",单":",双";
						
						$_strlottery='<span>'.$_narray[0].'</span>+ <span>'.$_narray[1].'</span>+ <span>'.$_narray[2].'</span>= <span>'.$_narray[3].'</span>('.$_substr.')';
					}
	            	?>
	                <tr>
	                    <td title="于 <?php echo date('Y-m-d H:i:s', $var['time']); ?> 开奖"><?php echo $var['number']; ?></td>
	                    <td class="kj-ball-small">
<?php 
						echo $_strlottery."<span class='memo87' style='display:none'>".$var['memo87']."</span>";
?>
	                    </td>
	                </tr>
	            <?php } ?>
	        </table>
			</div>
	    </div>
    </div>
</div>
<!--div id="kjsay" class="hide">开奖倒计时(<em class="kjtips">00:00</em>)</div-->
<script type="text/javascript">
    $(function () {
<?php
if(!$nocount)
{
?>    	
        if(lottery.isRobot)
        {
        	lottery.star_robot();
        }
<?php
}
?>
        window.S = <?php echo json_encode($diffTime > 0);?>;
        window.KS = <?php echo json_encode($kjDiffTime > 0);?>;
        window.kjTime = parseInt(<?php echo json_encode($kjdTime);?>);
        if (lottery.timer.T) clearTimeout(lottery.timer.T);
        if (lottery.timer.KT) clearTimeout(lottery.timer.KT);
        if (lottery.timer.moveno) clearInterval(lottery.timer.moveno);
        if (lottery.timer.load_last_data) clearInterval(lottery.timer.load_last_data);
        <?php if($diffTime > 0){?>
		var nowTime = new Date().getTime() + <?=$diffTime*1000 + 2000?>;
		var d = new Date(nowTime);
		var endDate = d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getDate() + ' ' + d.getHours() + ':' + d.getMinutes() + ':' + (d.getSeconds() );
		$('.dowebok').flipTimer({
			direction: 'down',
			date: endDate
			,callback: function(){
				lottery.countdown(0);
			}
		});
        <?php }?>

        <?php if($lottery){?>
        voice.play('/static/sound/voice-lottery.wav', 'voice-lottery');
        <?php } else {?>
        lottery.start();
        lottery.timer.load_last_data = setTimeout(lottery.load_last_data, 5000);
        <?php }?>
<?php
$pos=0;
if(isset($trend) && is_array($trend))
{
	foreach($trend as $k=>$v)
	{
		if($v==null)break;
		$data=explode(",",$v);
		$posNum=0;
		foreach($data as $k1=>$v1)
		{
			$dataNum=explode(":",$v1);
			echo "$('#trend_data".$pos."_num".$posNum."').html('".$dataNum[1]."');";
			$posNum++;
		}
		$pos++;
	}	
}
?>

	$('#slimScroll').slimScroll({
		height: '130px',
	//	width:'340px',
	});
});
</script>