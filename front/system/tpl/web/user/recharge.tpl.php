<?php
   /*20161103 暫時關閉限制
   //20161102 20:35 加上: 有充值成功過 才能看到 微信及支付寶支付功能 where  uid={$this->user['uid']}
   $uid = $this->user['uid'];
	 $isBeingRecharge = $this->db->query("SELECT * FROM `{$this->db_prefix}member_recharge` WHERE `uid`=$uid and state=1", 2);
	 if ($isBeingRecharge) {
	 */

/*
<a href="/user/recharge_alipay" target="_blank" func="loadpage" id="user-cash"> <img src="/static/theme/<?=THEME;?>/image/bank/bank_1.jpg" border="0"></a>
<a href="/user/recharge_wepay"  target="_blank" func="loadpage" id="user-cash"> <img src="/static/theme/<?=THEME;?>/image/bank/bank_19.jpg" border="0"></a>
*/
?>
<div style="border-bottom: 1px solid #434857;">
	<ul class="recharge-ul" >
<?php if ($this->config['cashFlowQQpay'] != 'close') { ?>
		<li class="recharge-tab recharge-on">
			<a>QQ錢包</a>
		</li>
<?php } ?>
<?php if ($this->config['cashFlowBank'] != 'close') { ?>
		<li class="recharge-tab">
			<a >银行支付</a>
		</li>
<?php } ?>
<?php if ($this->config['cashFlowQuickpay'] != 'close') { ?>
		<li class="recharge-tab">
			<a>快捷支付</a>
		</li>
<?php } ?>
<?php if ($this->config['cashFlowWechat'] != 'close' ) { ?>
		<li class="recharge-tab">
			<a>微信</a>
		</li>
<?php } ?>
<?php if ($this->config['cashFlowAlipay'] != 'close') { ?>
		<li class="recharge-tab">
			<a>支付宝</a>
		</li>
<?php } ?>
<?php if ($this->config['cashFlowJDpay'] != 'close') { ?>
		<li class="recharge-tab">
			<a>京东</a>
		</li>
<?php } ?>

	</ul>
</div>
<?php if ($this->config['cashFlowQQpay'] != 'close') { ?>

<div id="recharge-panel" class="money-panel tab-panel hide">
	<?php if ($this->config['cashFlowQQpay'] == 'niufu'  || $this->config['cashFlowQQpay'] == 'aifu') { ?>
	<form action="/user/pay" id="recharge-form" method="post" target="ajax" func="form_submit" container="#qrCodesQQpay">
	<?php } else { ?>
	<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
	<?php } ?>
		<div class="Tips">温馨提示：
            <p>　　　　尊敬的会员您好.</p>
            <p>　　　　QQ钱包充值时间为:全天24小时.</p>
            <p style="color:#EF0607;"></p>
            <p style="color:#EF0607;">　　　　QQ钱包充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
            <p>　　　　请您在:1分内完成充值,如过期,请勿继续充值.</p>
            <p><br/></p>
        </div>
        <li style="display: block;">
    	<div class="input mr15">
			<span class="icon icon-yen"></span>
			<input autocomplete="off" name="amount" required="required" type="text" id="input-money" min="<?php echo $this->config['rechargeMinQR'];?>" max="<?php echo $this->config['rechargeMaxQR'];?>" placeholder="请输入您的充值金额，最低：<?php echo $this->config['rechargeMinQR'];?>元，最高：<?php echo $this->config['rechargeMaxQR'];?>元">
		</div>
        <button type="submit" class="submit btn btn-blue icon-ok">充值</button>
        </li>
        <input type="hidden" name="bankid" value="20">
    </form>
	<div id="qrCodesQQpay" class="QrcodeArea" ></div>
</div>
<?php } ?>

<?php if ($this->config['cashFlowBank'] != 'close') { ?>
<div id="recharge-panel" class="money-panel tab-panel hide">
	<div class="main">
		<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
		<input type="hidden" id="bank-id" name="bankid" value="<?php echo $bank_default['id'];?>">
		<div id="recharge-type" class="type">
			<div id="recharge-current" class="current"><img width="88" height="35" src="/static/theme/<?=THEME;?>/image/bank/bank_<?php echo $bank_default['id'];?>.jpg" title="<?php echo $bank_default['name'];?>"></div>
			<span class="choose icon-down-dir">切换</span>
			<span class="hover icon-down-dir">点击切换银行</span>
		</div>
		<div class="input mr15">
			<span class="icon icon-yen"></span>
			<input autocomplete="off" name="amount" required="required" type="text" id="input-money" min="<?php echo $this->config['rechargeMin'];?>" max="<?php echo $this->config['rechargeMax'];?>" placeholder="请输入您的充值金额，最低：<?php echo $this->config['rechargeMin'];?>元，最高：<?php echo $this->config['rechargeMax'];?>元">
		</div>
		<button type="submit" class="submit btn btn-blue icon-ok">充值</button>
		</form>
	</div>
	<div id="bank-list" class="addon hide">
<?php 
if ($this->config['cashFlowBank'] == 'aifu') {

	$result = file_get_contents('https://pay.ifeepay.com/gateway/queryBankList?merchant_no=144790007168&mode=WEBPLAY&sign=9b78e5f7c4dd292046f7f7ea6607afec');
	$result = json_decode($result, true);
	$bankListArray = $result['bank_list'];


	global $globalBankMapping;
	$bankMapping = $globalBankMapping;

	$tempBanks = array();
	$newBanks = array();
	foreach ($banks as $bank){
		$tempBanks[$bank['id']] = $bank;	
	}
	foreach ($bankListArray as $rows) {
		$rows['BANK_NAME'] = str_replace('网关','',$rows['BANK_NAME']);
		if (isset($bankMapping[$rows['BANK_NAME']])) {
			$newBanks[] = $tempBanks[$bankMapping[$rows['BANK_NAME']]];
		//	$this->banks[$bankMapping[$rows['BANK_NAME']]] = $rows['BANK_CODE'];		
		}
	}
	$banks = $newBanks;
}
?>


		<?php foreach ($banks as $bank) {?>
		<img width="103" height="41" class="trans<?php if($bank['id'] === $bank_default['id']) echo ' active';?>" src="/static/theme/<?=THEME;?>/image/bank/bank_<?php echo $bank['id'];?>.jpg" title="<?php echo $bank['name'];?>" data-id="<?php echo $bank['id'];?>">
		<?php }?>
	</div>
</div>
<?php } ?>


<?php if ($this->config['cashFlowQuickpay'] != 'close') { ?>

<?php
	$banks = array("ICBC" => "工商银行","CMB" => "招商银行","CCB" => "建设银行","ABC" => "农业银行","BOC" => "中国银行","SPDB" => "上海浦东发展银行","BOCOM" => "交通银行","CMBC" => "民生银行","CEB" => "光大银行","GDB" => "广东发展银行","CNCB" => "中信银行","HXB" => "华夏银行","CIB" => "兴业银行","PSBC" => "邮政储蓄银行","SDB" => "深圳发展银行","BBGB" => "广西北部湾银行","BEA" => "东亚银行","CBHB" => "渤海银行","CDRCB" => "成都农村商业银行","CQRCB" => "重庆农村商业银行","DGB" => "东莞银行","DLB" => "大连银行","DYCCB" => "东营市商业银行","FDB" => "富滇银行","GZB" => "广州银行","HBB" => "河北银行","HKB" => "汉口银行","HZB" => "杭州银行","LTCCB" => "浙江泰隆商业银行","HSB" => "徽商银行","BJRCB" => "北京农商银行","HSB" => "徽商银行","HUNRCU" => "湖南农村信用社","JJB" => "九江银行","JSB" => "江苏银行","NBB" => "宁波银行","NXB" => "宁夏银行","QLB" => "齐鲁银行","QSB" => "齐商银行","RZB" => "日照银行","SCB" => "渣打银行","SDRCB" => "顺德农村商业银行","SHRCB" => "上海农村商业银行","SJB" => "盛京银行","PAB" => "平安银行","SRB" => "上饶银行","SZB" => "苏州银行","SZRCB" => "深圳农村商业银行","TACCB" => "泰安市商业银行","WHCCB" => "威海市商业银行","WLMQCCB" => "乌鲁木齐市商业银行","WZB" => "温州银行","XMB" => "厦门银行","YCCCB" => "宜昌市商业银行","ZHRCU" => "珠海市农村信用合作社","ZJCCB" => "浙商银行","ZJGRCB" => "张家港农商银行","NCB" => "南洋商业银行","SHRCB" => "上海农村商业银行","CBHB" => "渤海银行","NJCB" => "南京银行","BCCB" => "北京银行");
?>
<div id="recharge-panel" class="money-panel tab-panel hide">
	<div class="main quickpay">
		<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
		<input type="hidden" id="bank-id" name="bankid" value="18">
		<!--
		<div class="input2">
			<span class="">选择银行：</span>
				<select name="bank_code" class="select">
					<?php foreach ($banks as $k => $v) {?>
					<option value="<?php echo $k;?>"><?php echo $v;?></option>
					<?php }?>
				</select>

		</div>-->

<?php
	if (isset($this->user['json'])) {
		$json = json_decode($this->user['json'],true);
	}
	$remebers = true;
	if (!isset($json['quickpay']) || !$json['quickpay']){
		$json['quickpay'] = array(
			'bank_card_no' => '',
			'cardholder_name' => '',
			'id_card_no' => '',
			'mobile' => ''
		);
		$remebers = false;
	}

?>
		<!--
		<div class="input2">
			<span class="">银行卡号：</span>
			<input autocomplete="off" name="bank_card_no" required="required" type="text" id="input-money"" placeholder="请输入您的银行卡号" maxlength="30" value="<?=$json['quickpay']['bank_card_no']?>">
		</div>
		<div class="input2">
			<span class="">持卡人姓名：</span>
			<input autocomplete="off" name="cardholder_name" required="required" type="text" id="input-money"" placeholder="请输入您的持卡人姓名" maxlength="20" value="<?=$json['quickpay']['cardholder_name']?>">
		</div>
		<div class="input2">
			<span class="">持卡人身份证号：</span>
			<input autocomplete="off" name="id_card_no" required="required" type="text" id="input-money"" placeholder="请输入您的持卡人身份证号" maxlength="20" value="<?=$json['quickpay']['id_card_no']?>">
		</div>
		<div class="input2">
			<span class="">银行预留手机号：</span>
			<input autocomplete="off" name="mobile" required="required" type="text" id="input-money"" placeholder="请输入您的银行预留手机号" maxlength="11" value="<?=$json['quickpay']['mobile']?>">
		</div>
		-->
		
		<div class="input" style="float: initial;display: inline-block;    padding: 12px 10px 8px 35px;    margin-right: 5px;">
			<span class="icon icon-yen"></span>
			<input autocomplete="off" name="amount" required="required" type="text" id="input-money" min="<?php echo $this->config['rechargeMinQuickpay'];?>" max="<?php echo $this->config['rechargeMaxQuickpay'];?>" placeholder="请输入您的充值金额，最低：<?php echo $this->config['rechargeMinQuickpay'];?>元，最高：<?php echo $this->config['rechargeMaxQuickpay'];?>元">
		</div>
		<button type="submit" class="submit btn btn-blue icon-ok">充值</button>
		<!--
		<div class="input3">
			
			<input type="checkbox" id="remebers" name="remebers" style="vertical-align: middle;" <?= $remebers?'checked':''?>><label for="remebers">记住以上资料(下次免填)</label>
		</div>-->

		
		</form>
	</div>

</div>
<?php } ?>


<?php if ($this->config['cashFlowWechat'] != 'close' ) { ?>


<div id="recharge-panel" class="money-panel tab-panel hide">
	<?php if ($this->config['cashFlowWechat'] == 'niufu' || $this->config['cashFlowWechat'] == 'aifu' ) { ?>
	<form action="/user/pay" id="recharge-form" method="post" target="ajax" func="form_submit" container="#qrCodesWepay">
	<?php } else { ?>
	<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
	<?php } ?>

		<div class="Tips">温馨提示：
            <p>　　　　尊敬的会员您好.</p>
            <p>　　　　微信充值时间为:全天24小时.</p>
            <p style="color:#EF0607;"></p>
            <p style="color:#EF0607;">　　　　微信充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
            <p>　　　　请您在:1分内完成充值,如过期,请勿继续充值.</p>
            <p><br/></p>
        </div>
        <li style="display: block;">
    	<div class="input mr15">
			<span class="icon icon-yen"></span>
			<input autocomplete="off" name="amount" required="required" type="text" id="input-money" min="<?php echo $this->config['rechargeMinQR'];?>" max="<?php echo $this->config['rechargeMaxQR'];?>" placeholder="请输入您的充值金额，最低：<?php echo $this->config['rechargeMinQR'];?>元，最高：<?php echo $this->config['rechargeMaxQR'];?>元">
		</div>
        <button type="submit" class="submit btn btn-blue icon-ok">充值</button>
        </li>
        <input type="hidden" name="bankid" value="19">
    </form>
	<div id="qrCodesWepay" class="QrcodeArea"></div>
</div>

<?php } ?>

<?php if ($this->config['cashFlowAlipay'] != 'close') { ?>

<div id="recharge-panel" class="money-panel tab-panel hide">
	<?php if ($this->config['cashFlowAlipay'] == 'niufu' || $this->config['cashFlowAlipay'] == 'aifu') { ?>
	<form action="/user/pay" id="recharge-form" method="post" target="ajax" func="form_submit" container="#qrCodesAlipay">
	<?php } else { ?>
	<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
	<?php } ?>
		<div class="Tips">温馨提示：
            <p>　　　　尊敬的会员您好.</p>
            <p>　　　　支付宝充值时间为:全天24小时.</p>
            <p style="color:#EF0607;"></p>
            <p style="color:#EF0607;">　　　　支付宝充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
            <p>　　　　请您在:1分内完成充值,如过期,请勿继续充值.</p>
            <p><br/></p>
        </div>
        <li style="display: block;">
    	<div class="input mr15">
			<span class="icon icon-yen"></span>
			<input autocomplete="off" name="amount" required="required" type="text" id="input-money" min="<?php echo $this->config['rechargeMinQR'];?>" max="<?php echo $this->config['rechargeMaxQR'];?>" placeholder="请输入您的充值金额，最低：<?php echo $this->config['rechargeMinQR'];?>元，最高：<?php echo $this->config['rechargeMaxQR'];?>元">
		</div>
        <button type="submit" class="submit btn btn-blue icon-ok">充值</button>
        </li>
        <input type="hidden" name="bankid" value="1">
    </form>
	<div id="qrCodesAlipay" class="QrcodeArea" ></div>
</div>
<?php } ?>

<?php if ($this->config['cashFlowJDpay'] != 'close') { ?>
<div id="recharge-panel" class="money-panel tab-panel hide">
	<?php if ($this->config['cashFlowJDpay'] == 'niufu'  || $this->config['cashFlowJDpay'] == 'aifu') { ?>
	<form action="/user/pay" id="recharge-form" method="post" target="ajax" func="form_submit" container="#qrCodesJDpay">
	<?php } else { ?>
	<form action="/user/pay" id="recharge-form" method="post" target="_blank" func="form_submit">
	<?php } ?>
		<div class="Tips">温馨提示：
            <p>　　　　尊敬的会员您好.</p>
            <p>　　　　京东充值时间为:全天24小时.</p>
            <p style="color:#EF0607;"></p>
            <p style="color:#EF0607;">　　　　京东充值金额为:￥<?php echo $this->config['rechargeMinQR']; ?> - ￥<?php echo $this->config['rechargeMaxQR']; ?>元.</p>
            <p>　　　　请您在:1分内完成充值,如过期,请勿继续充值.</p>
            <p><br/></p>
        </div>
        <li style="display: block;">
    	<div class="input mr15">
			<span class="icon icon-yen"></span>
			<input autocomplete="off" name="amount" required="required" type="text" id="input-money" min="<?php echo $this->config['rechargeMinQR'];?>" max="<?php echo $this->config['rechargeMaxQR'];?>" placeholder="请输入您的充值金额，最低：<?php echo $this->config['rechargeMinQR'];?>元，最高：<?php echo $this->config['rechargeMaxQR'];?>元">
		</div>
        <button type="submit" class="submit btn btn-blue icon-ok">充值</button>
        </li>
        <input type="hidden" name="bankid" value="21">
    </form>
	<div id="qrCodesJDpay" class="QrcodeArea" ></div>
</div>
<?php } ?>





<div id="recharge-log" class="common">
	<div class="head">
		<div class="name icon-credit-card">充值记录</div>
		<form action="/user/recharge_search" class="search" data-ispage="true" container="#recharge-log .body" target="ajax" func="form_submit">
			<div class="timer">
				<input autocomplete="off" type="text" name="fromTime" value="<?php echo date('Y-m-d', $this->request_time_from);?>" id="datetimepicker_fromTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<div class="sep icon-exchange"></div>
			<div class="timer">
				<input autocomplete="off" type="text" name="toTime" value="<?php echo date('Y-m-d', $this->request_time_to);?>" id="datetimepicker_toTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<button type="submit" class="btn btn-brown icon-search">查询</button>
		</form>
	</div>
	<div class="body"><?php require(TPL.'/user/recharge_body.tpl.php');?></div>
</div>
<script type="text/javascript">
$(function() {
	$('.recharge-tab').click(function (){
		var _index = $('.recharge-tab').index(this);
		$('.tab-panel').hide().eq(_index).fadeIn();
		//console.log($('.recharge-tab').index(this));
        $('.recharge-tab').removeClass('recharge-on');
        $(this).addClass('recharge-on');
	});

	$('.recharge-tab').eq(0).click();
	$('#home').removeClass('on');
	$('#user-recharge').addClass('on');
	// 菜单下拉固定
	$.scroll_fixed('#recharge-log .head');
	// 切换银行
	var recharge_type = $('#recharge-type');
	var recharge_type_hover = recharge_type.find('.hover');
	var recharge_type_choose = recharge_type.find('.choose');
	var bank_id = $('#bank-id');
	var bank_list = $('#bank-list');
	var recharge_form = $('#recharge-form');
	var recharge_current_img = $('#recharge-current img');
	recharge_type.hover(function() {
		recharge_type_hover.animate({'top': 0});
	}, function() {
		recharge_type_hover.animate({'top': '41px'});
	});
	recharge_type.bind('click', function() {
		if (bank_list.is(':hidden')) {
			bank_list.slideDown();
			recharge_type_choose.removeClass('icon-down-dir').addClass('icon-up-dir').text('收起');
			recharge_type_hover.removeClass('icon-down-dir').addClass('icon-up-dir').text('点击收起银行');
		} else {
			bank_list.slideUp();
			recharge_type_choose.removeClass('icon-up-dir').addClass('icon-down-dir').text('切换');
			recharge_type_hover.removeClass('icon-up-dir').addClass('icon-down-dir').text('点击切换银行');
		}
	});
	bank_list.find('img').bind('click', function() {
		recharge_current_img.attr('src', $(this).attr('src'));
		$(this).addClass('active').siblings().removeClass('active');

		bank_id.val($(this).data('id'));
		/*
		console.log(bank_id.val());
		if ($(this).data('id') != 19) {
			recharge_form.attr('target', 'ajax');
		} else {
			recharge_form.attr('target', '_blank');
		}*/
	});
	// 输入框焦点效果
	$('#input-money').focus(function() {
		$(this).parent().addClass('focus');
	}).blur(function() {
		$(this).parent().removeClass('focus');
	});
	// 时间选择插件
	$('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
	$('#recharge-type .icon-down-dir').click();
});
</script>