<div style="border-bottom: 1px solid #434857;">
	<ul class="recharge-ul" >
		<li class="recharge-tab recharge-on" data-action="1">
			<a>时时彩</a>
		</li>
		<li class="recharge-tab" data-action="2">
			<a>PC蛋蛋</a>
		</li>
		<li class="recharge-tab" data-action="3">
			<a>快三</a>
		</li>
		<li class="recharge-tab" data-action="4">
			<a>11选5</a>
		</li>
		<li class="recharge-tab" data-action="5">
			<a>低频彩</a>
		</li>
		<li class="recharge-tab" data-action="6">
			<a>PK10</a>
		</li>		
		<li class="recharge-tab" data-action="7">
			<a>快乐8</a>
		</li>
	</ul>
</div>
<div class="report_top" >
	<ul class="report_ul">
	</ul>
	<ul class="report_ul hide">
	</ul>
	<ul class="report_ul hide">
	</ul>
	<ul class="report_ul hide">
	</ul>	
	<ul class="report_ul hide">
		<li><a class="report_li" data-action="5">福彩3D</a></li>
	</ul>
	<ul class="report_ul hide">		
	</ul>
	<ul class="report_ul hide">		
	</ul>
</div>
<div class="report" id="rule_view" style="width: 100%;margin-top: 25px;">
	<?php 
	require(TPL.'/sys/rule/1.tpl.php');
	?>
</div>
<script type="text/javascript">
$(function() {
	$('.recharge-tab').click(function (){
		var _index = $('.recharge-tab').index(this);
        $('.recharge-tab').removeClass('recharge-on');
        $(this).addClass('recharge-on');
        $('.report_ul').hide().eq(_index).fadeIn();
        $('.report_li').removeClass('on');
        $('.report_li').eq(0).addClass('on');
        get_report($(this).data("action"));
	});
	$('.report_li').click(function (){
		$('.report_li').removeClass('on');
		$(this).addClass('on');
		get_report($(this).data("action"));
	});

	$('#home').removeClass('on');
	$('#sys-notice').addClass('on');
});
function get_report(action,target)
{
	if(target==null)
	{
		target = $('#rule_view');
	}
	loadpage.before(function() {
		var complete = function(err, data) {
			loadpage.close(function() {
				if (err) {
					$.error(err);
				} else {
					target.html(data);
				}
			});
		};
		$.ajax({
			url: '/sys/rule?type=' + action,
			cache: false,
			data: {},
			type: 'POST',
			dataType: 'html',
			error: function(xhr, textStatus, errThrow) {
				$.error(errThrow || textStatus);
			},
			success:function(data, textStatus, xhr) {
				//$.error(data);
				//target.html(data);
				var errorMessage = xhr.getResponseHeader('X-Error-Message');
				//console.log(errorMessage)
				if (errorMessage) {
					complete(errorMessage === 'dialogue' ? data : decodeURIComponent(errorMessage));
				} else {
					complete(null, data);
				}				
			}
		});
	});
}
</script>