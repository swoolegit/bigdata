<!-- begin -->
<div class="rbar">
    <h1><span class="gamek-9">PK拾玩法规则</span></h1>
</div>
<div class="rcon">

<b><p>一、玩法说明</p></b>
采用按位置顺序选择投注号码、固定设奖的玩法。投注者首先根据不同玩法确定选择最少1个、最多10个位置进行投注，然后按照从左到右、从1号到10号投注位置的顺序，从1号位置开始，在每个位置上从编号为1至10的号码中任意选择1个号码，且不能与其它位置上已经选择的号码相同，按照从1 到10号位置的顺序排序组成一组号码，每一组选定的按位置排序的号码称为一注投注号码。
<br>
<br>
<b><p id="2">二、中奖规则</p></b>
<p>（一）设奖</p>
1. 采用专用的摇奖计算机系统进行摇奖，中奖号码从数字1 到10 总共10 个数字中随机产生。每次摇奖时按照从左至右、从1 号位置到10 号位置的顺序进行，第一个摇出的数字对应1 号位置，第二个摇出的数字对应2 号位置，依次类推，直到摇出对应10 号位置的第十个数字为止，这10 个位置和对应的数字组成当期中奖号码。
<br>2.	按不同投注方式设奖，均为固定奖。奖金规定如下：
    <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
        <tbody><tr align="center">
            <td height="25"  class="td cu" colspan="2">玩法</td>
            <td height="25"  class="td cu">开奖号码示例</td>
            <td height="25"  class="td cu">投注号码示例</td>
            <td height="25"  class="td cu">中奖规则</td>
        </tr>
        <tr align="center">
            <td height="25" >前一</td>
            <td height="25" >直选复式</td>
            <td height="25" >05 06...</td>
            <td height="25" >05</td>
            <td height="25" >选号与开奖号按位猜中1位即中奖</td>
        </tr>
        <tr align="center">
            <td height="25"  rowspan="2">前二</td>
            <td height="25" >直选复式</td>
            <td height="25"  rowspan="2">05 06 07...</td>
            <td height="25"  rowspan="2">05 06</td>
            <td height="25"  rowspan="2">选号与开奖号按位猜中2位即中奖</td>
        </tr>
        <tr align="center">
            <td height="25" >直选单式</td>
        </tr>
        <tr align="center">
            <td height="25"  rowspan="2">前三</td>
            <td height="25" >直选复式</td>
            <td height="25"  rowspan="2">05 06 07 08...</td>
            <td height="25"  rowspan="2">05 06 07</td>
            <td height="25"  rowspan="2">选号与开奖号按位猜中3位即中奖</td>
        </tr>
        <tr align="center">
            <td height="25" >直选单式</td>
        </tr>
        <tr align="center">
            <td height="25"  colspan="2">定位胆</td>
            <td height="25" >05 06...</td>
            <td height="25" >冠军：05</td>
            <td height="25" >选号与开奖号按位猜中1位或1位以上即中奖</td>
        </tr>
        </tbody>
    </table>
<br>
<b><p id="3">三、玩法介绍</p></b>
<p>（一）投注方式</p>
1、前一：从第一名中至少选择1个号码组成一注，选号和开奖号码猜对一位即中奖。
    <table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
        <tbody>
        <tr align="center">
            <td height="25"  rowspan="2">例：</td>
            <td height="25" >开奖：01 02 03 04 05 06 07 08 09 10</td>
        </tr>
        <tr align="center">
            <td height="25" >选号：冠军01</td>
        </tr>
        </tbody>
    </table>
<br>2、前二直选：从第一名、第二名中各至少选择1个号码组成一注，选号和开奖号码猜对2位即中奖。
    <table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
        <tbody>
        <tr align="center">
            <td height="25"  rowspan="2">例：</td>
            <td height="25" >开奖：01 02 03 04 05 06 07 08 09 10</td>
        </tr>
        <tr align="center">
            <td height="25" >选号：冠军01，亚军02</td>
        </tr>
        </tbody>
    </table>
<br>3、前三直选：从第一名、第二名、第三名中至少选择1个号码组成一注，选号和开奖号码猜对3位即中奖。
    <table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
        <tbody>
        <tr align="center">
            <td height="25"  rowspan="2">例：</td>
            <td height="25" >开奖：01 02 03 04 05 06 07 08 09 10</td>
        </tr>
        <tr align="center">
            <td height="25" >选号：冠军01，亚军02，季军03</td>
        </tr>
        </tbody>
    </table>
<br>4、定位胆：从第一名到第十名任意位置上选择1个或1个以上号码，所选号码与相同位置上的开奖号码一致。
    <table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
        <tbody>
        <tr align="center">
            <td height="25"  rowspan="2">例：</td>
            <td height="25" >开奖：01 02 03 04 05 06 07 08 09 10</td>
        </tr>
        <tr align="center">
            <td height="25" >选号：冠军01</td>
        </tr>
        </tbody>
    </table>
</div>
<!-- end -->
