<!-- begin -->
<div class="rbar">
        <h1><span class="gamek-4">时时彩玩法规则</span></h1>
</div>
<div class="rcon">
<br>
<b><p id="4">一、玩法说明</p></b>
1. 时时彩投注区分为万位、千位、百位、十位和个位，各位号码范围为0～9。每期从各位上开出1个号码作为中奖号码，即开奖号码为5位数。时时彩玩法即是竞猜5位开奖号码的全部号码、部分号码或部分号码特征。
<div align="center"></div>
<br>
2. 时时彩包括星彩玩、定位胆、不定位、大小单双、任选等玩法。星彩玩法分为二星、前后三星、四星、五星四种玩法如下：
<br>
<table width="100%" border="0" cellpadding="0" cellspacing="0"  class="s-table" >
    <tbody>
    <tr align="center" class="cu" >
        <td width="50" height="25"  class="td" colspan="2">玩法</td>
        <td width="514" height="25"  class="td">规则</td>
    </tr>
    <tr align="center">
        <td height="25" >五星</td>
        <td height="25" >直选</td>
        <td width="514" height="25" >竞猜全部5位号码，即万位、千位、百位、十位和个位，且顺序一致。</td>
    </tr>
    <tr align="center">
        <td height="25" >四星</td>
        <td height="25" >直选</td>
        <td width="514" height="25" >竞猜全部4位号码，即千位、百位、十位和个位，且顺序一致。</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="3">前三/后三</td>
        <td height="25" >直选</td>
        <td width="514" height="25" >竟猜前/后三码，即(万、千、百)位；或(百、十、个)位，且顺序一致。</td>
    </tr>
    <tr align="center">
        <td height="25" >组选三</td>
        <td width="514" height="25" >竟猜前/后三码，即(万、千、百)位；或(百、十、个)位，顺序不限，且投注时三位号码有两位相同。</td>
    </tr>
    <tr align="center">
        <td height="25" >组选六</td>
        <td width="514" height="25" >竟猜前/后三码，即(万、千、百)位；或(百、十、个)位，顺序不限，且投注时三位号码各不相同。</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="2">前二</td>
        <td height="25" >直选</td>
        <td width="514" height="25" >竟猜前两码，即万位和千位，且顺序一致。</td>
    </tr>
    <tr align="center">
        <td height="25" >组选</td>
        <td width="514" height="25" >竟猜前两码，即万位和千位，顺序不限。</td>
    </tr>
    <tr align="center">
        <td height="25"  colspan="2">定位胆</td>
        <td width="514" height="25" >竞猜万位、千位、百位、十位、个位任意位置上任意1个或1个以上号码。</td>
    </tr>
    <tr align="center">
        <td height="25"  colspan="2">不定位</td>
        <td width="514" height="25" >竞猜对应三星、四星、五星玩法位置上任意1个或1个以上号码，且顺序不限。</td>
    </tr>
    <tr align="center">
        <td height="25"  colspan="2">大小单双</td>
        <td width="514" height="25" >竟猜万位、千位中的“大、小、单、双”中至少各选一个组成一注。</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="3">任选</td>
        <td height="25" >任选二</td>
        <td width="514" height="25" >竞猜万位、千位、百位、十位、个位任意位置上任意2个号码或者2个号码和值。</td>
    </tr>
    <tr align="center">
        <td height="25" >任选三</td>
        <td width="514" height="25" >竞猜万位、千位、百位、十位、个位任意位置上任意3个号码或者3个号码和值。</td>
    </tr>
    <tr align="center">
        <td height="25" >任选四</td>
        <td width="514" height="25" >竞猜万位、千位、百位、十位、个位任意位置上任意4个号码或者4个号码和值。</td>
    </tr>
    </tbody>
</table>
注：<1>直选：将投注号码以惟一的排列方式进行投注。
<br>　　<2>组选：将投注号码的所有排列方式作为一注投注号码进行投注。示例：123，排列方式有123、132、213、231、312、321，共计6种。
<br>　　<3>组选三：在三星组选中，如果一注组选号码的3个数字有两个数字相同，则有3种不同的排列方式，因而就有3个中奖机会，这种组选投注方式简称组选三。示例：112，排列方式有112、121、211。
<br>　　<4>组选六：在三星组选中，如果一注组选号码的3个数字各不相同，则有6种不同的排列方式，因而就有6个中奖机会，这种组选投注方式简称组选六。示例：123，排列方式有123、132、213、231、312、321，共计6种。
<br>　　<5>大小单双：即把10个自然数按“大”“小”，或者“单”，“双”性质分为两组，0-4为小号，5-9为大号。0，2，4，6，8 为双号。1，3，5，7，9 为单号。

<br><br>
<b><p id="5">二、设奖及中奖</p></b>
奖金计算说明列表请浏览本站主页“开奖公告”页面，点击重庆时时彩查阅。
<br>
<table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody><tr align="center">
        <td height="25"  colspan="2" class="td cu">玩法</td>
        <td height="25"  class="td cu">开奖号码</td>
        <td height="25"  class="td cu">投注号码示例</td>
        <td height="25"  class="td cu"></td>
        <td height="25"  class="td cu">中奖概率</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="7">三星</td>
        <td height="25" >直选(复式)</td>
        <td height="25"  rowspan="2">678</td>
        <td height="25" >678</td>
        <td height="25"  rowspan="3">定位中三码</td>
        <td height="25"  rowspan="3">1/1000</td>
    </tr>
    <tr align="center">
        <td height="25" >直选(单式)</td>
        <td height="25" >678</td>
    </tr>
    <tr align="center">
        <td height="25" >直选和值</td>
        <td height="25" >13</td>
        <td height="25" >13</td>
    </tr>
    <tr align="center">
        <td height="25" >组选(组三)</td>
        <td height="25" >113</td>
        <td height="25" >113</td>
        <td height="25"  rowspan="4">不定位中三码</td>
        <td height="25" >1/90</td>
    </tr>
    <tr align="center">
        <td height="25" >组选(组六)</td>
        <td height="25" >123</td>
        <td height="25" >123</td>
        <td height="25" >1/120</td>
    </tr>
    <tr align="center">
        <td height="25" >组选和值(组三)</td>
        <td height="25" >5(113)</td>
        <td height="25" >5(113)</td>
        <td height="25" >1/90</td>
    </tr>
    <tr align="center">
        <td height="25" >组选和值(组六)</td>
        <td height="25" >6(123)</td>
        <td height="25" >6(123)</td>
        <td height="25" >1/120</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="2">二星</td>
        <td height="25" >直选(前/后二)</td>
        <td height="25" >78</td>
        <td height="25" >78</td>
        <td height="25" >定位中三码</td>
        <td height="25" >1/100</td>
    </tr>
    <tr align="center">
        <td height="25" >组选(前/后二)</td>
        <td height="25" >78</td>
        <td height="25" >78</td>
        <td height="25" >不定位中三码</td>
        <td height="25" >1/45</td>
    </tr>
    <tr align="center">
        <td height="25"  colspan="2">大小单双</td>
        <td height="25" >678</td>
        <td height="25" >大单</td>
        <td height="25" >大小</td>
        <td height="25" >1/16</td>
    </tr>
    </tbody>
</table>
注：<1>、假设当期的开奖号码为45678则组选三不中奖（后三组选适用开奖号码为45668）。
<br>　　<2>、前三码和后三码：前三码指开奖号码的前三位号码，后三码指开奖号码的后三位号码。示例：开奖号码为45678，前三码为456，后三码为678。
<br>　　<3>、前两码和后两码：前两码指开奖号码的前两位号码，后两码指开奖号码的后两位号码。示例：开奖号码为45678，前两码为45，后两码为78。
<br>　　<4>、定位胆：所选号码与相同位置上和开奖号码一致。示例：开奖号码为45678，万位4即中定位胆万位。
<br>　　<5>、定位和不定位：定位指投注号码与开奖号码按位一致，不定位指投注号码与开奖号码一致，顺序不限。示例：开奖号码为45678，78则定位中后两码，78或87则为不定位中后两码。
<br>　　<6>、任选和值：所选号码与开奖号码的和值相同。示例：开奖号码为45678，位置选万位、百位，和值号码即10。

<br><br>
<b><p id="6">三、投注方式</p></b>
<p>1、五星直选</p>
对万位、千位、百位、十位和个位各选1个号码为一注，每位号码可从0～9全选，投注号码与开奖号码按位一致，即为中奖。
<p>2、四星直选</p>
对千位、百位、十位和个位各选1个号码为一注，每位号码可从0～9全选，投注号码与开奖号码按位一致，即为中奖。
<p>3、三星直选</p>
对百位、十位和个位各选1个号码为一注，每位号码最多可0～9全选，投注号码与开奖号码后三位按位一致即为中奖。
<p>4、三星和值</p>
和值指号码各位数相加之和，如号码001，和值为1。三星和值投注指用某一个三星和值对应的所有号码进行投注，所选和值与开奖号码后三位和值一致即为中奖，单注最高奖金980元。示例：选择三星和值1投注，即用和值1所对应的号码（001、010、100）投注，如开奖号码后三位和值为1即中奖。
<p>5、组三组选</p>
组三包号指用所选号码的所有组三排列方式进行组选三投注，如开奖号码为组三号且包含在所选号码中即为中奖，单注最高奖金326.666元元。示例：组三包号12，共2注（112、122），如开奖号码后三位为112、121、211、122、212、221皆为中奖。
包号速算表如下：
<table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  class="td cu">包号个数（10选n）</td>
        <td height="25"  class="td cu">2</td>
        <td height="25"  class="td cu">3</td>
        <td height="25"  class="td cu">4</td>
        <td height="25"  class="td cu">5</td>
        <td height="25"  class="td cu">6</td>
        <td height="25"  class="td cu">7</td>
        <td height="25"  class="td cu">8</td>
        <td height="25"  class="td cu">9</td>
        <td height="25"  class="td cu">10</td>
    </tr>
    <tr align="center">
        <td height="25"  class="td cu">投注金额（2元）</td>
        <td height="25"  class="td cu">4</td>
        <td height="25"  class="td cu">12</td>
        <td height="25"  class="td cu">24</td>
        <td height="25"  class="td cu">40</td>
        <td height="25"  class="td cu">60</td>
        <td height="25"  class="td cu">84</td>
        <td height="25"  class="td cu">112</td>
        <td height="25"  class="td cu">144</td>
        <td height="25"  class="td cu">180</td>
    </tr>
    </tbody>
</table>
<p>6、组六组选</p>
组六包号指用所选号码的所有组六排列方式进行组选六投注，如开奖号码为组六号且包含在所选号码中即为中奖，单注最高奖金163.333元。示例：组六包号123，共1注，如开奖号码后三位为123、132、213、231、312、321皆为中奖。
包号速算表如下：
<table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  class="td cu">包号个数（10选n）</td>
        <td height="25"  class="td cu">4</td>
        <td height="25"  class="td cu">5</td>
        <td height="25"  class="td cu">6</td>
        <td height="25"  class="td cu">7</td>
        <td height="25"  class="td cu">8</td>
        <td height="25"  class="td cu">9</td>
        <td height="25"  class="td cu">10</td>
    </tr>
    <tr align="center">
        <td height="25"  class="td cu">投注金额（2元）</td>
        <td height="25"  class="td cu">8</td>
        <td height="25"  class="td cu">20</td>
        <td height="25"  class="td cu">40</td>
        <td height="25"  class="td cu">74</td>
        <td height="25"  class="td cu">112</td>
        <td height="25"  class="td cu">168</td>
        <td height="25"  class="td cu">240</td>
    </tr>
    </tbody>
</table>
<p>7、二星直选</p>
对万位和千位各选1个号码为一注，每位号码最多可0～9全选，投注号码与开奖号码后两位按位一致即为中奖。
<p>8、二星和值</p>
和值指号码各位数相加之和，如号码01，和值为1。二星和值投注指用某一个二星和值对应的所有号码进行投注，所选和值与开奖号码前两位和值一致即为中奖，单注最高奖金98元。示例：选择二星和值1投注，即用和值1所对应的号码（01、10）投注，如开奖号码前两位和值为1即中奖。
<p>9、二星组选</p>
从号码0—9中任选两个数字对万位和千位进行投注，最多可0～9全选。所选号码与开奖号码前两位一致，顺序不限，即为中奖。示例：选择12，共1注，如开奖号码前两位为12或21即中奖。
<p>10、大小单双</p>
对万位和千位的大小单双4种特征中各选一种特征为一注，最多可4种特征全选，所选特征与开奖号码后两位号码特征一致即中奖。示例：开奖号码后两位为78，则大大、大双、单双、单大为中奖。
<p>11、总和值</p>
<p>大小：根据开奖第一球 ~ 第五球开出的球号数字总和值来判断：总和大>=23，总和小<=22。</p>
<p>单双：根据开奖第一球 ~ 第五球开出的球号数字总和值来判断：总和双=偶数，总和单=奇数。</p>
<p>12、龙虎</p>
<p>龙：开奖第一球(万位)的号码 > 第五球(个位)的号码。如：6XXX2、8XXX6、5XXX1...开奖为龙，投注龙中奖，其它不中奖。</p>
<p>虎：开奖第一球(万位)的号码 < 第五球(个位)的号码。如：2XXX6、6XXX8、1XXX5...开奖为虎，投注虎中奖，其它不中奖。</p>
<p>和：开奖第一球(万位)的号码 = 第五球(个位)的号码。如：2XXX2、6XXX6、8XXX8...开奖为和，投注和中奖，其它不中奖。</p>
<p>13、牛牛</p>
<p>牛牛：根据开奖第一球 ~ 第五球开出的球号数字为基础，任意组合三个号码成0或10的倍数，取剩余两个号码之和为点数（大于10时减去10后的数字作为对奖基数)。
<ul style="padding-left: 20px;">
	<li>
		如：00026为牛8，02818为牛9；
	</li>
	<li>
		如：68628、23500皆为牛10俗称牛牛；
	</li>
	<li>
		如：26378、15286因任意三个号码都无法组合成0或10的倍数，称为没牛。
	</li>
	<li>
		当五个号码相同时，只有00000视为牛牛，其它11111，66666等皆视为没牛
	</li>
</ul>	
</p>
<p>大小：牛大(牛6,牛7,牛8,牛9,牛牛)，牛小(牛1,牛2,牛3,牛4,牛5)，若开出斗牛结果为没牛，则投注牛大牛小皆为不中奖。</p>
<p>单双：牛单(牛1,牛3,牛5,牛7,牛9)，牛双(牛2,牛4,牛6,牛8,牛牛)，若开出斗牛结果为没牛，则投注牛单牛双皆为不中奖。</p>
</div>
<!-- end -->
