<!-- begin -->
<div class="rbar">
    <h1><span class="gamek-12">11选5玩法规则</span></h1>
</div>
<div class="rcon">
<b>
<p>一、玩法说明</p>
</b>
11选5投注区号码范围为01～11，每期开出5个号码作为中奖号码。11选5玩法即是竞猜5位开奖号码的全部或部分号码。
    <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
        <tbody><tr align="center" class="cu">
            <td height="25"  class="td" colspan="2">玩法</td>
            <td width="514" height="25"  class="td">规则</td>
        </tr>
        <tr align="center">
            <td height="25"  rowspan="8">任选</td>
            <td height="25" >任选一</td>
            <td width="514" height="25" >竞猜第1位开奖号码。</td>
        </tr>
        <tr align="center">
            <td height="25" >任选二</td>
            <td width="514" height="25" >竞猜开奖号码中的任意2位。</td>
        </tr>
        <tr align="center">
            <td height="25" >任选三</td>
            <td width="514" height="25" >竞猜开奖号码中的任意3位。</td>
        </tr>
        <tr align="center">
            <td height="25" >任选四</td>
            <td width="514" height="25" >竞猜开奖号码中的任意4位。</td>
        </tr>
        <tr align="center">
            <td height="25" >任选五</td>
            <td width="514" height="25" >选择5个号码，竞猜全部5位开奖号码。</td>
        </tr>
        <tr align="center">
            <td height="25" >任选六</td>
            <td width="514" height="25" >选择6个号码，竞猜全部5位开奖号码。</td>
        </tr>
        <tr align="center">
            <td height="25" >任选七</td>
            <td width="514" height="25" >选择7个号码，竞猜全部5位开奖号码。</td>
        </tr>
        <tr align="center">
            <td height="25" >任选八</td>
            <td width="514" height="25" >选择8个号码，竞猜全部5位开奖号码。</td>
        </tr>
        <tr align="center">
            <td height="25"  rowspan="2">二星</td>
            <td height="25" >直选(复式)</td>
            <td width="514" height="25" >竞猜开奖号码前/后2位，且顺序一致。</td>
        </tr>
        <tr align="center">
            <td height="25" >组选(单式)</td>
            <td width="514" height="25" >竞猜开奖号码前/后2位，顺序不限。</td>
        </tr>
        <tr align="center">
            <td height="25"  rowspan="2">三星</td>
            <td height="25" >直选(复式)</td>
            <td width="514" height="25" >竞猜开奖号码前/后3位，且顺序一致。</td>
        </tr>
        <tr align="center">
            <td height="25" >组选(单式)</td>
            <td width="514" height="25" >竞猜开奖号码前/后3位，顺序不限。</td>
        </tr>
        </tbody>
    </table>
    注：<1>直选：将投注号码以唯一的排列方式进行投注。
    <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<2>组选：将投注号码的所有排列方式作为一注投注号码进行投注。示例：前三组选01 02 03，排列方式有01 02 03、01 03 02、02 01 03、02 03 01、03 01 02、03 02 01，共计6种。

    <br><br>
<b><p id="2">二、中奖规则</p></b>
<table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  colspan="2" class="td cu">奖级</td>
        <td height="25"  class="td cu">中奖条件</td>
        <td height="25"  class="td cu">中奖号码示例</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="8">任选</td>
        <td height="25" >任选一</td>
        <td height="25" >任意中1码</td>
        <td height="25" >01</td>
    </tr>
    <tr align="center">
        <td height="25" >任选二</td>
        <td height="25" >任意中2码</td>
        <td height="25" >01 02，01 03</td>
    </tr>
    <tr align="center">
        <td height="25" >任选三</td>
        <td height="25" >任意中3码</td>
        <td height="25" >01 02 03，01 02 04</td>
    </tr>
    <tr align="center">
        <td height="25" >任选四</td>
        <td height="25" >任意中4码</td>
        <td height="25" >01 02 03 04，01 02 03 05</td>
    </tr>
    <tr align="center">
        <td height="25" >任选五</td>
        <td height="25" >不定位中5码</td>
        <td height="25" >01 02 03 04 05</td>
    </tr>
    <tr align="center">
        <td height="25" >任选六</td>
        <td height="25" >不定位中5码</td>
        <td height="25" >01 02 03 04 05 X</td>
    </tr>
    <tr align="center">
        <td height="25" >任选七</td>
        <td height="25" >不定位中5码</td>
        <td height="25" >01 02 03 04 05 X X</td>
    </tr>
    <tr align="center">
        <td height="25" >任选八</td>
        <td height="25" >不定位中5码</td>
        <td height="25" >01 02 03 04 05 X X X</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="2">三码</td>
        <td height="25" >直选</td>
        <td height="25" >定位中前/中/后3码</td>
        <td height="25" >01 02 03</td>
    </tr>
    <tr align="center">
        <td height="25" >组选</td>
        <td height="25" >不定位中前/中/后3码</td>
        <td height="25" >01 02 03，03 02 01</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="2">二码</td>
        <td height="25" >直选</td>
        <td height="25" >定位中前/后2码</td>
        <td height="25" >01 02 03</td>
    </tr>
    <tr align="center">
        <td height="25" >组选</td>
        <td height="25" >不定位中前/后2码</td>
        <td height="25" >01 02，01 02</td>
    </tr>
    </tbody>
</table>
注：<1>、假设当期的开奖号码为01 02 03 04 05。
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<2>、2码和3码：2码指开奖号码的前/后2位号码，3码指开奖号码的前/中/后3位号码，示例：开奖号码为01 02 03 04 05，前1码为01，前2码为01 02，前3码为01 02 03。
<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<3>、定位和不定位：定位指投注号码与开奖号码按位一致，不定位指投注号码与开奖号码一致，顺序不限。示例：开奖号码为01 02 03 04 05，01 02则定位中前2码，01 02或02 01则为不定位中前2码。

<br><br>
<b><p id="3">三、玩法介绍</p></b>
<p>1、任选一</p>
从01～11中任选1个或多个号码为一注，投注号码与开奖号码任意1位一致即为中奖
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：04 05 02 09 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：05</td>
    </tr>
    </tbody>
</table>
-命中任意一位号码，顺序不限
<p>2、任选二：</p>
从01～11中至少选择2个号码投注，选号与开奖号码任意2位。投注时可选择1个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：04 11 02 09 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：11 04</td>
    </tr>
    </tbody>
</table>
-命中任意两位号码，顺序不限
<p>3、任选三</p>
从01～11中任选3个号码为一注，选择3个以上号码为复式投注，投注号码与开奖号码中任意3个号码相同即为中奖。投注时可选择1～2个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：04 05 08 10 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：04 08 07</td>
    </tr>
    </tbody>
</table>
-命中任意三位号码，顺序不限
<p>4、任选四</p>
从01～11中任选4个号码为一注，选择4个以上号码为复式投注，投注号码与开奖号码中任意4个号码相同即为中奖。投注时可选择1～3个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：01 02 03 04 05</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：01 03 04 05</td>
    </tr>
    </tbody>
</table>
-命中任意四位号码，顺序不限
<p>5、任选五</p>
从01～11中任选5个号码为一注，选择5个以上号码为复式投注，投注号码与开奖号码全部相同即为中奖。投注时可选择1～4个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：01 03 08 10 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：03 01 08 07 10</td>
    </tr>
    </tbody>
</table>
-命中任意五位号码，顺序不限
<p>6、任选六</p>
从01～11中任选6个号码为一注，选择6个以上号码为复式投注，投注号码中任意5个号码与5位开奖号码相同即为中奖。投注时可选择1～5个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：01 03 08 10 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：03 01 08 07 10 02</td>
    </tr>
    </tbody>
</table>
-命中任意六位号码，顺序不限
<p>7、任选七</p>
从01～11中任选7个号码为一注，选择7个以上号码即为复式投注，投注号码中任意5个号码与5位开奖号码相同即为中奖。投注时可选择1～6个号码作为每注都有的胆码，再补充其它号码作为拖码进行投注。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：01 03 08 10 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：03 01 08 07 10 02 04</td>
    </tr>
    </tbody>
</table>
-命中任意七位号码，顺序不限
<p>8、任选八</p>
从01～11中任选8个号码为一注，选择8个以上号码为复式投注，投注号码中任意5个号码与5位开奖号码相同即为中奖。投注时可选择1～7个号码作为每注都有的胆码，再补充其它号码作为拖码投注。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：01 03 08 10 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：03 01 08 07 10 02 04 05</td>
    </tr>
    </tbody>
</table>
-命中任意八位号码，顺序不限
9、二码直选
对前/后2位各选1个号码为一注，某一位或两位选择2个以上号码为复式投注，投注号码与开奖号码前/后2位按位一致即为中奖。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：01 03 08 10 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：万位01 千位03</td>
    </tr>
    </tbody>
</table>
-命中开奖号码的前两位号码，且顺序一致
<p>10、二码组选</p>
从01～11中任选2个号码为一注，选择2个以上号码为复式投注，投注号码与开奖号码前/后2位一致，顺序不限即为中奖。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：01 03 08 10 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：01 03</td>
    </tr>
    </tbody>
</table>
-命中开奖号码的前两位，顺序不限
<p>11、三码直选</p>
对前/中/后3位各选1个号码为一注，某一位或几位选择2个以上号码为复式投注，投注号码与开奖号码前/中/后3位按位一致即为中奖，单注最高奖金1940元。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：01 03 08 10 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：万位01 千位03 百位08</td>
    </tr>
    </tbody>
</table>
-命中开奖号码的前三位号码，且顺序一致
<p>12、三码组选</p>
从01～11中任选3个号码为一注，选择3个以上号码为复式投注，投注号码与开奖号码前/中/后3位一致，顺序不限，即为中奖。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：01 03 08 10 07</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：01 03 08</td>
    </tr>
    </tbody>
</table>
-命中开奖号码的前三位，顺序不限
</div>
<!-- end -->