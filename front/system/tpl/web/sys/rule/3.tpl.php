<!-- begin -->
<div class="rbar">
    <h1><span class="gamek-11">快三玩法规则</span></h1>
</div>
<div class="rcon">
<b>
<p>一、玩法说明</p>
</b>
1.	快三投注是指以3个号码组合为一注彩票进行单式投注，每个投注号码为1-6共六个自然数中的任意一个，一组3个号码的组合称为一注。
<br>2.	三号码组合共设置“和值”、“三同号”、“二同号”、“三不同号”、“二不同号”、“三连号通选”投注方式，具体规定如下：
<br>（一）和值投注是指对3个号码的和值进行投注。
<br>（二）三同号投注是指对3个相同的号码进行投注，具体分为：
<br>&nbsp;&nbsp;&nbsp;&nbsp;1.	三同号通选：是指对所有相同的3个号码进行投注；
<br>&nbsp;&nbsp;&nbsp;&nbsp;2.	三同号单选：是指从所有相同的3个号码中任意选择一组号码进行投注。
<br>（三）二同号投注是指对2个指定的相同号码进行投注，具体分为：
<br>&nbsp;&nbsp;&nbsp;&nbsp;1.	二同号复选：是指对3个号码中2个指定的相同号码和一个任意号码进行投注；
<br>&nbsp;&nbsp;&nbsp;&nbsp;2.	二同号单选：是指对3个号码中2个指定的相同号码和一个指定的不同号码进行投注。
<br>（四）三不同号投注：是指对3个各不相同的号码进行投注。
<br>（五）二不同号投注：是指对3个号码中2个指定的不同号码和一个任意号码进行投注。
<br>（六）三连号通选投注：是指对所有3个相连的号码进行投注。

<br>游戏号码组合参考列表：
<table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody><tr align="center" class="cu">
        <td height="25"  class="td">投注方法</td>
        <td width="400" height="25"  class="td">号码组合（号码不排序）</td>
        <td height="25"  class="td">投注号数</td>
    </tr>
    <tr align="center">
        <td height="25" >和值3</td>
        <td height="25" >111</td>
        <td height="25" >三同号单选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值4</td>
        <td height="25" >112</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值5</td>
        <td height="25" >113,112</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值6</td>
        <td height="25" >114,123,222</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值7</td>
        <td height="25" >115,124,133,223</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值8</td>
        <td height="25" >116,125,134,224,233</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值9</td>
        <td height="25" >126,135,144,225,234,333</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值10</td>
        <td height="25" >136,145,226,235,244,334</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值11</td>
        <td height="25" >146,155,236,245,335,344</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值12</td>
        <td height="25" >156,246,255,336,345,444</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值13</td>
        <td height="25" >166,256,346,355,445</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值14</td>
        <td height="25" >266,356,446,455</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值15</td>
        <td height="25" >366,456,555</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值16</td>
        <td height="25" >466,556</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值17</td>
        <td height="25" >566</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >和值18</td>
        <td height="25" >666</td>
        <td height="25" >三同号单选</td>
    </tr>
    <tr align="center">
        <td height="25" >三同号通选</td>
        <td height="25"  rowspan="2">111,222,333,444,555,666</td>
        <td height="25" >全选</td>
    </tr>
    <tr align="center">
        <td height="25" >三同号单选</td>
        <td height="25" >选一个</td>
    </tr>
    <tr align="center">
        <td height="25" >二同号复选</td>
        <td height="25" >11,22,33,44,55,66</td>
        <td height="25" >选一个</td>
    </tr>
    <tr align="center">
        <td height="25" >二同号单选</td>
        <td height="25" >112,113,114,115,116,122,223,224,225,226,133,
            <br>233,334,335,336,144,244,344,445,446,155,
            <br>255,355,455,556,166,266,366,466,566</td>
        <td height="25" >选一个</td>
    </tr>
    <tr align="center">
        <td height="25" >三不同号</td>
        <td height="25" >123,124,125,126,134,135,136,145,146,156,
            <br>234,235,236,245,246,256,345,346,356,456</td>
        <td height="25" >选一个</td>
    </tr>
    <tr align="center">
        <td height="25" >二不同号</td>
        <td height="25" >12*,13*,14*,15*,16*,23*,24*,25*,
            <br>26*,34*,35*,36*,45*,46*,56*</td>
        <td height="25" >选一个</td>
    </tr>
    <tr align="center">
        <td height="25" >三连号通选</td>
        <td height="25" >123,234,345,456</td>
        <td height="25" >全选</td>
    </tr>
    </tbody>
</table>

<br><br>
<b><p id="2">二、中奖规则</p></b>
<p>（一）设奖</p>
1. 快三按不同单式投注方式设奖，均为固定奖。奖金规定如下：
<table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  colspan="2" class="td cu">玩法</td>
        <td height="25"  class="td cu">开奖示例</td>
        <td height="25"  class="td cu">投注示例</td>
        <td height="25"  class="td cu">中奖规则</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="2">三同号</td>
        <td height="25" >通选</td>
        <td height="25"  rowspan="2">333</td>
        <td height="25" >三同号通选</td>
        <td height="25" >对所有相同的三个号码（111、222、…、666）进行全包投注,与开奖号相同</td>
    </tr>
    <tr align="center">
        <td height="25" >单选</td>
        <td height="25" >333</td>
        <td height="25" >从所有相同的三个号码（111、…、666）中任意选择一组号码进行投注,与开奖号相同</td>
    </tr>
    <tr align="center">
        <td height="25"  rowspan="2">二同号</td>
        <td height="25" >复选</td>
        <td height="25"  rowspan="2">223</td>
        <td height="25" >22</td>
        <td height="25" >对三个号码中两个指定的相同号码和一个任意号码进行投注,与开奖号相同</td>
    </tr>
    <tr align="center">
        <td height="25" >单选</td>
        <td height="25" >同号22, 不同号3</td>
        <td height="25" >对三个号码中两个指定的相同号码和一个指定的不同号码进行投注,与开奖号相同</td>
    </tr>
    <tr align="center">
        <td height="25"  colspan="2">三不同号</td>
        <td height="25"  rowspan="3">123</td>
        <td height="25" >123</td>
        <td height="25" >对三个各不相同的号码进行投注,与开奖号相同</td>
    </tr>
    <tr align="center">
        <td height="25"  colspan="2">二不同号</td>
        <td height="25" >13</td>
        <td height="25" >对三个号码中两个指定的不同号码和一个任意号码进行投注,与开奖号相同</td>
    </tr>
    <tr align="center">
        <td height="25"  colspan="2">三连号通选</td>
        <td height="25" >三连号通选</td>
        <td height="25" >对所有三个相连的号码（123、234、345、456）进行投注,与开奖号相同</td>
    </tr>
    </tbody>
</table>
<br>2. 当期每注投注号码按其投注方式只有一次中奖机会，不能兼中兼得，特别设奖除外。

<br><br>
<b><p id="3">三、玩法介绍</p></b>
<p>（一）投注方式</p>
1. 和值：至少选择1个号码，与开奖的3个号码相加之和相同即中奖。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：112</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：4</td>
    </tr>
    </tbody>
</table>
<br>2. 三同号通选：当开奖号码为三同号(111，222，333，444，555，666)中的任一组即中奖，1/36中奖机会。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：666</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：三同号通选</td>
    </tr>
    </tbody>
</table>
<br>3. 三同号单选：至少选1组号码，所选号码与开奖号码相同即中奖，1/216中奖机会。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：111</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：111</td>
    </tr>
    </tbody>
</table>
<br>4. 三不同号：至少选择3个号码，与开奖号码全相同即中奖，1/36的中奖机会。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：124</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：124</td>
    </tr>
    </tbody>
</table>
<br>5. 三连号通选：当开奖号码为三连号(123，234，345，456)中的任一组即中奖，1/9中奖机会。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：123</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：三连号通选</td>
    </tr>
    </tbody>
</table>
<br>6. 二同号复选：至少选1个对子投注，开奖号包含此对子即中奖，1/13.5中奖机会。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：112</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：114</td>
    </tr>
    </tbody>
</table>
<br>7. 二同号单选：至少选1个对子和1个不同号投注，与开奖号码全相同即中奖，1/72中奖机会。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：112</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：112</td>
    </tr>
    </tbody>
</table>
<br>8. 二不同号玩法：至少选择2个号码，与开奖的任意2个号码相同即中奖，1/7.2的中奖机会。
<table width="300" cellspacing="0" cellpadding="0" border="0"  class="s-table">
    <tbody>
    <tr align="center">
        <td height="25"  rowspan="2">例：</td>
        <td height="25" >开奖：123</td>
    </tr>
    <tr align="center">
        <td height="25" >选号：12</td>
    </tr>
    </tbody>
</table>

</div>
<!-- end -->
