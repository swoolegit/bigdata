        <!-- begin -->
<div class="rbar">
	<h1><span class="gamek-1">福彩3D玩法规则</span></h1>
 </div>
 <div class="rcon">
<b>
<p id="4">一、玩法说明</p>
</b>
  1．福彩3D投注区分为百位、十位和个位，以一个3位自然数为投注号码的彩种玩法，投注者从000到999的范围内选择一个3位数进行投注，福彩3D玩法即是竞猜3位开奖号码。
  <br>
  2. 在本站中，福彩3D支持的玩法有“福彩3D分直选”、“组选三”、和“组选六”三种玩法如下：
  <br>
  <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
      <tbody><tr align="center" class="cu">
          <td width="79" height="25"  class="td">玩法</td>
          <td width="514" height="25"  class="td">规则</td>
      </tr>
      <tr align="center">
          <td height="25" >直选</td>
          <td width="514" height="25" >竟猜三位开奖号码，即百位、十位和个位，且顺序一致。</td>
      </tr>
      <tr align="center">
          <td height="25" >组选三</td>
          <td width="514" height="25" >竟猜三位开奖号码，即百位、十位和个位，顺序不限，且投注时三位号码有且只有两位相同。</td>
      </tr>
      <tr align="center">
          <td height="25" >组选六</td>
          <td width="514" height="25" >竟猜三位开奖号码，即百位、十位和个位，顺序不限，且投注时三位号码各不相同。</td>
      </tr>
      </tbody>
  </table>
  注：<1>直选：将投注号码以唯一的排列方式进行投注。
  <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<2>组选：将投注号码的所有排列方式作为一注投注号码进行投注。示例：123，排列方式有123、132、213、231、312、321，共计6种。
  <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<3>组选三：在组选中，如果一注组选号码的3个数字有两个数字相同，则有3种不同的排列方式，因而就有3个中奖机会，这种组选投注方式简称组选三。示例：112，排列方式有112、121、211。
  <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<4>组选六：在组选中，如果一注组选号码的3个数字各不相同，则有6种不同的排列方式，因而就有6个中奖机会，这种组选投注方式简称组选六。示例：123，排列方式有123、132、213、231、312、321，共计6种。
  <br>
  <br>
  <b>
  <p id="5">二、设奖及中奖</p>
  </b>

      奖金计算说明列表请浏览本站主页“开奖公告”页面，点击福彩3D查阅。
      <br>

      <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
          <tbody><tr align="center">
              <td height="25"  class="td cu" colspan="2">玩法</td>
              <td height="25"  class="td cu">开奖号码</td>
              <td height="25"  class="td cu">投注号码示例</td>
              <td height="25"  class="td cu"></td>
              <td height="25"  class="td cu">中奖概率</td>
          </tr>
          <tr align="center">
              <td height="25"  rowspan="7">三星</td>
              <td height="25" >直选(复式)</td>
              <td height="25"  rowspan="2">678</td>
              <td height="25" >678</td>
              <td height="25"  rowspan="3">定位中三码</td>
              <td height="25"  rowspan="3">1/1000</td>
          </tr>
          <tr align="center">
              <td height="25" >直选(单式)</td>
              <td height="25" >678</td>
          </tr>
          <tr align="center">
              <td height="25" >直选和值</td>
              <td height="25" >13</td>
              <td height="25" >13</td>
          </tr>
          <tr align="center">
              <td height="25" >组选(组三)</td>
              <td height="25" >113</td>
              <td height="25" >113</td>
              <td height="25"  rowspan="4">不定位中三码</td>
              <td height="25" >1/90</td>
          </tr>
          <tr align="center">
              <td height="25" >组选(组六)</td>
              <td height="25" >123</td>
              <td height="25" >123</td>
              <td height="25" >1/120</td>
          </tr>
          <tr align="center">
              <td height="25" >组选和值(组三)</td>
              <td height="25" >5(113)</td>
              <td height="25" >5(113)</td>
              <td height="25" >1/90</td>
          </tr>
          <tr align="center">
              <td height="25" >组选和值(组六)</td>
              <td height="25" >6(123)</td>
              <td height="25" >6(123)</td>
              <td height="25" >1/120</td>
          </tr>
          <tr align="center">
              <td height="25"  rowspan="2">二星</td>
              <td height="25" >直选(前/后二)</td>
              <td height="25" >78</td>
              <td height="25" >78</td>
              <td height="25" >定位中三码</td>
              <td height="25" >1/100</td>
          </tr>
          <tr align="center">
              <td height="25" >组选(前/后二)</td>
              <td height="25" >78</td>
              <td height="25" >78</td>
              <td height="25" >不定位中三码</td>
              <td height="25" >1/45</td>
          </tr>
          <tr align="center">
              <td height="25"  colspan="2">大小单双</td>
              <td height="25" >678</td>
              <td height="25" >大单</td>
              <td height="25" >大小</td>
              <td height="25" >1/16</td>
          </tr>
          </tbody>
      </table>

      注：<1>、假设当期的开奖号码为678组选三不中奖（组选三适用开奖号码为668）。
      <br>
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<2>、定位和不定位：定位指投注号码与开奖号码按位一致，不定位指投注号码与开奖号码一致，顺序不限。示例：开奖号码为678，678则定位中三码，768或867则为不定位中三码。
	<br><br>
      <b>
      <p id="6">三、投注方式</p>
      </b>
      1、直选标准
      <br>
      对百位、十位和个位各选1个号码为一注，每位号码最多可0～9全选，投注号码与开奖号码顺序一致即为中奖。（每位号码可以同时选择多个进行复式投注）
      <br>
      2、组三组选
      <br>
      组三组选指用所选号码的所有组三排列方式进行组选三投注，如开奖号码为组三号且包含在所选号码中即为中奖。示例：组三组选12，共2注（112、122），如开奖号码为112、121、211、122、212、221皆为中奖。
      <br>
      包号速算表如下：
  <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
      <tbody>
      <tr align="center">
          <td height="25"  class="td cu">包号个数（10选n）</td>
          <td height="25"  class="td cu">2</td>
          <td height="25"  class="td cu">3</td>
          <td height="25"  class="td cu">4</td>
          <td height="25"  class="td cu">5</td>
          <td height="25"  class="td cu">6</td>
          <td height="25"  class="td cu">7</td>
          <td height="25"  class="td cu">8</td>
          <td height="25"  class="td cu">9</td>
          <td height="25"  class="td cu">10</td>
      </tr>
      <tr align="center">
          <td height="25"  class="td cu">投注金额（2元）</td>
          <td height="25"  class="td cu">4</td>
          <td height="25"  class="td cu">12</td>
          <td height="25"  class="td cu">24</td>
          <td height="25"  class="td cu">40</td>
          <td height="25"  class="td cu">60</td>
          <td height="25"  class="td cu">84</td>
          <td height="25"  class="td cu">112</td>
          <td height="25"  class="td cu">144</td>
          <td height="25"  class="td cu">180</td>
      </tr>
      </tbody>
  </table>
      3、组三和值
      <br>
      和值指号码各位数相加之和，如号码001，和值为1。组三和值投注指用某一组选三和值对应的所有号码进行组三投注，所选和值与开奖号码和值一致，且开奖号为组三号即为中奖。
      示例：选择组三和值2投注，即用组三和值2所对应的号码（011、002）投注，如开奖号码为组三号且和值为2即中奖。
      <br>
      4、组六组选
      <br>
      组六组选指用所选号码的所有组六排列方式进行组选六投注，如开奖号码为组六号且包含在所选号码中即为中奖。示例：组六组选123，共1注，如开奖号码后三位为123、132、213、231、312、321皆为中奖。
      包号速算表如下：
      <br>
  <table width="100%" cellspacing="0" cellpadding="0" border="0"  class="s-table">
      <tbody>
      <tr align="center">
          <td height="25"  class="td cu">包号个数（10选n）</td>
          <td height="25"  class="td cu">4</td>
          <td height="25"  class="td cu">5</td>
          <td height="25"  class="td cu">6</td>
          <td height="25"  class="td cu">7</td>
          <td height="25"  class="td cu">8</td>
          <td height="25"  class="td cu">9</td>
          <td height="25"  class="td cu">10</td>
      </tr>
      <tr align="center">
          <td height="25"  class="td cu">投注金额（2元）</td>
          <td height="25"  class="td cu">8</td>
          <td height="25"  class="td cu">20</td>
          <td height="25"  class="td cu">40</td>
          <td height="25"  class="td cu">70</td>
          <td height="25"  class="td cu">112</td>
          <td height="25"  class="td cu">168</td>
          <td height="25"  class="td cu">240</td>
      </tr>
      </tbody>
  </table>
  5、组六和值
  <br>
  和值指号码各位数相加之和，如号码123，和值为6。和值投注指用某一组六和值对应的所有号码进行组六投注，所选和值与开奖号码和值一致，且开奖号为组六号即为中奖。
  <br>
  示例：选择组六和值6投注，即用组六和值6所对应的号码（123、015、024）投注，如开奖号码为组六号且和值为6即中奖。
  <br>
  6、单式上传
  <br>
  将固定格式的单式号码统一上传给系统进行投注。此投注方式对于将过滤软件过滤出的单式号码统一投注十分方便。
  <br>

  <br>
</div>
        <!-- end -->
