<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit">
<title><?php echo $this->config['webName'];?></title>
<link href="<?=CDN;?>/static/theme/<?=THEME;?>/css/popModal.min.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<link href="<?=CDN;?>/static/theme/<?=THEME;?>/css/common.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<link href="<?=CDN;?>/static/theme/<?=THEME;?>/css/icon.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<link href="<?=CDN;?>/static/theme/<?=THEME;?>/css/page.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<link href="<?=CDN;?>/static/theme/<?=THEME;?>/css/fliptimer.css?v=<?php echo $this->version;?>" rel="stylesheet" type="text/css"/>
<script src="<?=CDN;?>/static/script/echarts.min.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/macarons.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/vintage.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/jquery.1.7.2.min.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/jquery.zclip.min.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/jquery.cookie.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/jquery.datetimepicker.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/jquery.slimscroll.min.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/array.ext.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/rawdeflate.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/select.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/common.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/function.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/game.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/jquery.uploadfile.min.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/flipTimer.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/jquery.rotate.js?v=<?php echo $this->version;?>"></script>
<script src="<?=CDN;?>/static/script/popModal.min.js?v=<?php echo $this->version;?>"></script>
<script type="text/javascript">
var datetimepicker_opt = {
	lang: 'ch',
	//format: 'Y-m-d H:i',
	format: 'Y-m-d',
	timepicker:false,
	closeOnDateSelect: true,
	validateOnBlur: true,
	formatDate: 'Y-m-d',
	minDate: '<?php echo date('Y-m-d', $this->user['regTime']);?>',
	maxDate: '<?php echo date('Y/m/d');?>',
};
var _timecount;
var changeColor = function (file){
	$('head .theme-color').remove()
	if (file!="colorX")
	{
		$('head').append( $('<link rel="stylesheet" type="text/css" class="theme-color" />').attr('href', '/static/theme/<?=THEME;?>/css/'+file+'.css') );		
	}
	$.cookie('colorfile', file, { expires: 30 });
	/*
	if (file) {
		$.cookie('colorfile', file, { expires: 30 });
		$('head').append( $('<link rel="stylesheet" type="text/css" class="theme-color" />').attr('href', '/static/theme/<?=THEME;?>/css/'+file+'.css') );
	}else
	{
		$.cookie('colorfile', '');
	}
	*/
};

</script>
</head>
<body>
<div id="dom_body" >
<div id="nav">
	<div class="center" style="margin:0px;">
		<div class="game-bg"></div>
		<a href="/index/home" class="home on" target="ajax" func="loadpage" id="home" disabled>首页</a>
		<a href="/user/setting" target="ajax" func="loadpage" class="" id="user-setting">个人设置</a>
		<a href="/bet/log" target="ajax" func="loadpage" class="" id="bet-log">投注记录</a>
		<!--a href="/user/money" target="ajax" func="loadpage" class="" id="user-money">盈亏报表</a-->
		<a href="/report/index" target="ajax" func="loadpage" class="" id="report-money" >数据统计</a>
		<?php if ($this->user['type']) {?>
		<div class="agent-nav">
			<a href="/agent/index" target="ajax" func="loadpage" class="" id="agent">代理中心</a>
			<ul class="agent-nav-list hide">
				<li><a href="/agent/member2" target="ajax" func="loadpage" class="icon-address-book" id="agent-member">会员管理</a></li>
				<!--li><a href="/agent/log" target="ajax" func="loadpage" class="icon-list-alt" id="agent-log">团队记录</a></li-->
				<li><a href="/agent/money" target="ajax" func="loadpage" class="icon-yen" id="agent-money">盈亏统计</a></li>
				<!--li><a href="/agent/coin" target="ajax" func="loadpage" class="icon-chart-bar" id="agent-coin">帐变日志</a></li-->
				<li><a href="/agent/spread" target="ajax" func="loadpage" class="icon-link-ext" id="agent-spread">推广链接</a></li>
			</ul>
		</div>
		<?php }?>
		<?php if ($this->getDzpSwitch()){ ?>
			<a href="/activity/rotary" target="ajax" func="loadpage" class="" id="activity">幸运大转盘</a>
		<?php }?>
		<div class="agent-nav">
			<a href="/sys/notice" target="ajax" func="loadpage" class="" id="sys-notice">活动公告 <span class="icon-down-dir"></span></a>
			<ul class="agent-nav-list hide">
				<li><a href="/sys/notice" target="ajax" func="loadpage" class="icon-popup" >活动中心</a></li>
				<li><a href="/sys/notice_rule" target="ajax" func="loadpage" class="icon-help" >玩法说明</a></li>
			</ul>			
		</div>
		<?php if ($this->config['kefuStatus']) {?>
		<a href="<?php echo $this->config['kefuGG'];?>" target="_blank" class="" id="call-service">客服专线</a>
		<?php }?>
	</div>
</div>
<div id="main" class="center clearfix">
	<div id="sidebar">
		<div class="user">
			<div class="info" id="user-info">
			</div>
			<div class="info" style="text-align: center;" >
				<a href="javascript:;" onclick="changeColor('colorX')" class="changecolor c1"></a>
				<!--a href="javascript:;" onclick="changeColor('color1')" class="changecolor c2"></a-->
				<a href="javascript:;" onclick="changeColor('color2')" class="changecolor c3"></a>
			</div>
			<div class="opt">
				<div class="top-line"></div>
				<div class="line">
					<a class="black-inputs-recharge" onmouseover="this.style.cssText='color:gray; text-decoration:none;'" href="/user/recharge" target="ajax" func="loadpage" id="user-recharge" >
						充值
					</a>
					<a class="black-inputs-recharge" href="/user/cash" target="ajax" func="loadpage" id="user-cash" style="float: right" >
						提现
					</a>
				</div>
				<div class="line">
					<a href="/user/coin" target="ajax" func="loadpage" id="user-coin">
						<span class="icon-log text">帐变</span>
					</a>
					<a href="/user/message_receive" target="ajax" func="loadpage" class="float-right" id="message-receive">
						<span class="icon-emails text">私信</span>
					</a>
				</div>
				<div class="line">
					<a href="/user/sign" target="ajax" func="loadpage" >
						<span class="icon-sign text">签到</span>
					</a>
					<a href="/user/logout" target="ajax" func="loadpage" class="float-right">
						<span class="icon-logout text">退出</span>
					</a>
				</div>
			</div>
		</div>

		<div class="gs" id="game-nav">
			<?php
				$types_sort = array('时时彩','外国系列','互联网','PC蛋蛋','11选5','低频彩','PK10','快乐8','六合彩','快三');

				$types = $this->get_types();
				
				$types['外国系列'] = array();
				$types['互联网'] = array();
				
				foreach ($types['时时彩'] as $i => $rows){
					if ($rows['id'] == 53) {
						$types['低频彩'][] = $rows;
						unset($types['时时彩'][$i]);
						continue;
					}
					if (in_array($rows['id'], array(78,79,80,81,82,71,72,73,74,62,63,5))) {
						$types['外国系列'][] = $rows;
						unset($types['时时彩'][$i]);
						continue;
					}
					if (in_array($rows['id'], array(83,84,85,86,87,88,89))) {
						$types['互联网'][] = $rows;
						unset($types['时时彩'][$i]);
						continue;
					}
				}
				$newTypes = array();
				foreach ($types_sort as $rows) {
					$newTypes[$rows] = $types[$rows];				
				}

				foreach ($newTypes as $name => $list) {
					if (!$list) continue;
			?>
			<div class="g">
				<div class="game-name icon-<?=md5($name);?>">
					<span class="text"><?php echo $name;?></span>
					<span class="count"><?php echo count($list);?> 款彩种</span>
				</div>
				<ul class="list" style="position: relative;">
					<?php foreach ($list as $v) {?>
					<li >
						<a type-id="<?php echo $v['id'];?>" href="/game/index?id=<?php echo $v['id'];?>"  func="loadpage" class="nav_lottery_list" target="ajax">
							<?php echo $v['title'];?>
						</a>
					</li>
					<?php }?>
				</ul>
			</div>
			<?php }?>
		</div>
		<div class="client">
			<div class="line">
				<a href="/static/download/index.html" target="_blank">
					<span class="icon icon-windows window-blue"></span>
				</a>
				<a href="/static/download/index.html" target="_blank">
					<span class="icon icon-android-1"></span>
				</a>
				<a href="/static/download/index.html" target="_blank">
					<span class="icon icon-appstore apple-silver"></span>
				</a>
			</div>
			<div class="top-line" style="clear: both"></div>
			<div class="line">
				<a class="icon-download" style="color: #d8d8d8;padding-bottom:10px;display:block;width:100%" href="/static/download/" target="_blank">下载手机端</a>
			</div>
			<div class="top-line" style="clear: both"></div>
			<div class="line">
				<a class="icon-download" style="color: #d8d8d8;padding-bottom:10px;display:block;width:100%" href="/static/download/dblottery1.0.3.rar" target="_blank">下载挂机软件</a>
			</div>
			<div class="top-line" style="clear: both"></div>
			<div class="line">
				<a class="icon-download" style="color: #d8d8d8;padding-bottom:10px;display:block;width:100%" href="/static/download/dbtask1.0.rar" target="_blank">下载计划软件</a>
			</div>
		</div>
	</div>
	<div id="container">
		<div id="container_warp">
			<?php if (isset($load_self) && $load_self) {?>
			<script type="text/javascript">
				$(function() {
					$.load(window.location.href, '#container_warp', {
						callback: function() {
							$.inited = true;
						},
					});
				});
			</script>
			<?php
				} else {
					require(TPL.'/home.tpl.php');
					echo '<script type="text/javascript">$.inited = true;</script>';
				}
			?>
		</div>
	</div>
</div>
<div id="loading-page" class="dialogue"><div class="dialogue-warp"></div></div>
<div id="dialogue" class="dialogue">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">
			<div class="dialogue-auto">
				<span class="dialogue-sec"></span>秒后自动关闭
			</div>
			<div class="right">
				<button class="dialogue-yes btn btn-blue icon-ok"></button>
				<button class="dialogue-no btn btn-gray icon-undo"></button>
			</div>
		</div>
	</div>
</div>
<!-- begin 挂机 -->
<div id="game_robot" class="dialogue" style="z-index: 99">
	<div class="dialogue-warp">
		<div class="dialogue-head">
			<span class="dialogue-title icon-lamp">系统提示</span>
		</div>
		<div class="dialogue-body"></div>
		<div class="dialogue-foot">
			<div class="right">
				<button class="dialogue-yes btn btn-blue icon-ok" onclick="lottery.star_robot();" id="star_robot">自动挂机</button>
				<button class="dialogue-no btn btn-gray icon-undo" onclick="lottery.stop_robot();">取消挂机</button>
			</div>
		</div>
	</div>
</div>
<!-- end 挂机 -->
<!--begin 页脚-->
<div style="clear: both"></div>
<div class="jack-foot" id="jack-foot">

</div>
<!--end 页脚-->
</div>
<script type="text/javascript">
$(function() {
	// 更新用户信息
	lottery.user_fresh();
	// 代理中心下拉菜单
	var agent_nav = $('#nav .agent-nav');
	if (agent_nav.length > 0) {
		agent_nav.hover(function() {
			$(this).find('.agent-nav-list').fadeIn(150);
		}, function() {
			$(this).find('.agent-nav-list').fadeOut(150);
		});
	}
	// 关闭滚动公告 Q
	$('#nt-close').bind('click', function() {
		$(this).parent().fadeOut(function() {
			$.cookie('ntc', 1);
			$('.notice').hide();
		});
	});
	if($.cookie('colorfile')=="" || $.cookie('colorfile')==null)
	{
		$.cookie('colorfile', 'color2', { expires: 30 });
	}
	if ($.cookie('colorfile')!="colorX")
	{
		$('head').append( $('<link rel="stylesheet" type="text/css" class="theme-color" />').attr('href', '/static/theme/<?=THEME;?>/css/'+$.cookie('colorfile')+'.css') );		
	}
});
</script>
</body>
</html>
