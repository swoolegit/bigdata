<div id="coin-log" class="common" style="margin-top: 25px;">
	<div class="head" style="border:0px;">
		<form action="/report/game" class="search" data-ispage="true" container="#report_view" target="ajax" func="form_submit">
			<div class="timer">
				<input type="text" autocomplete="off" name="fromTime" value="<?php echo date('Y-m-d', $this->request_time_from);?>" id="datetimepicker_fromTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<div class="sep icon-exchange"></div>
			<div class="timer">
				<input type="text" autocomplete="off" name="toTime" value="<?php echo date('Y-m-d', $this->request_time_to);?>" id="datetimepicker_toTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<button type="submit" class="btn btn-brown icon-search">查询</button>
		</form>
	</div>
</div>
<?php
if($total)
{
?>
	<div class="bet common" style="margin-top: 25px; width:100%;">
		<div class="head" style="border-top-right-radius: 0px;">
			<div class="name icon-chart-line"> <?php echo date('Y-m-d', $this->request_time_from);?> ~ <?php echo date('Y-m-d', $this->request_time_to);?></div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="key">投注金额</td><td class="val"><?=$this->formatNum($total['betAmount'])?></td>
					<td class="key">中奖金额</td><td class="val"><?=$this->formatNum($total['zjAmount'])?></td>
					<td class="key">撤单金额</td><td class="val"><?=$this->formatNum($total['delAmount'])?></td>
					<td class="key">追号注数</td><td class="val"><?=$total['zhActionNum']?></td>
				</tr>
				<tr>
					<td class="key">投注注数</td><td class="val"><?=$total['actionNum']?></td>
					<td class="key">中奖注数</td><td class="val"><?=$total['zjActionNum']?></td>
					<td class="key">撤单注数</td><td class="val"><?=$total['delActionNum']?></td>
					<td class="key">追号注单</td><td class="val"><?=$total['zhOrder']?></td>
				</tr>
				<tr>
					<td class="key">投注订单</td><td class="val"><?=$total['norder']?></td>
					<td class="key">中奖订单</td><td class="val"><?=$total['zjOrder']?></td>
					<td class="key">撤单订单</td><td class="val"><?=$total['delOrder']?></td>
					<td class="key"> -- </td><td class="val"> -- </td>
				</tr>
			</table>
		</div>
	</div>
<?php
}
?>
<div class="bet common" style="margin-top: 25px; width:100%;">
	<div class="head" style="border-top-right-radius: 0px;">
		<div class="name icon-chart-line">今日投注</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="key">今日投注金额</td><td class="val"><?=$this->formatNum($today['betAmount'])?></td>
				<td class="key">今日中奖金额</td><td class="val"><?=$this->formatNum($today['zjAmount'])?></td>
				<td class="key">今日撤单金额</td><td class="val"><?=$this->formatNum($today['delAmount'])?></td>
				<td class="key">今日追号注数</td><td class="val"><?=$today['zhActionNum']?></td>
			</tr>
			<tr>
				<td class="key">今日投注注数</td><td class="val"><?=$today['actionNum']?></td>
				<td class="key">今日中奖注数</td><td class="val"><?=$today['zjActionNum']?></td>
				<td class="key">今日撤单注数</td><td class="val"><?=$today['delActionNum']?></td>
				<td class="key">今日追号注单</td><td class="val"><?=$today['zhOrder']?></td>
			</tr>
			<tr>
				<td class="key">今日投注订单</td><td class="val"><?=$today['norder']?></td>
				<td class="key">今日中奖订单</td><td class="val"><?=$today['zjOrder']?></td>
				<td class="key">今日撤单订单</td><td class="val"><?=$today['delOrder']?></td>
				<td class="key"> -- </td><td class="val"> -- </td>
			</tr>
		</table>
	</div>
</div>

<div class="bet common" style="margin-top: 25px; width:100%;">
	<div class="head" style="border-top-right-radius: 0px;">
		<div class="name icon-chart-line">昨日投注</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="key">昨日投注金额</td><td class="val"><?=$this->formatNum($yestoday['betAmount'])?></td>
				<td class="key">昨日中奖金额</td><td class="val"><?=$this->formatNum($yestoday['zjAmount'])?></td>
				<td class="key">昨日撤单金额</td><td class="val"><?=$this->formatNum($yestoday['delAmount'])?></td>
				<td class="key">昨日追号注数</td><td class="val"><?=$yestoday['zhActionNum']?></td>
			</tr>
			<tr>
				<td class="key">昨日投注注数</td><td class="val"><?=$yestoday['actionNum']?></td>
				<td class="key">昨日中奖注数</td><td class="val"><?=$yestoday['zjActionNum']?></td>
				<td class="key">昨日撤单注数</td><td class="val"><?=$yestoday['delActionNum']?></td>
				<td class="key">昨日追号注单</td><td class="val"><?=$yestoday['zhOrder']?></td>
			</tr>
			<tr>
				<td class="key">昨日投注订单</td><td class="val"><?=$yestoday['norder']?></td>
				<td class="key">昨日中奖订单</td><td class="val"><?=$yestoday['zjOrder']?></td>
				<td class="key">昨日撤单订单</td><td class="val"><?=$yestoday['delOrder']?></td>
				<td class="key"> -- </td><td class="val"> -- </td>
			</tr>

		</table>
	</div>
</div>
<script>
$(function() {
	// 时间选择插件
	$('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
});
</script>