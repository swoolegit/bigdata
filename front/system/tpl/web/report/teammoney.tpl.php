<div style="float:left;width: 100%;">
<?php
$month=$data['month1'];
$base = strtotime(date('Y-m',time()) . '-01 00:00:00');
$start	= strtotime('-1 month', $base);
$m=date('Y-m',$start);
$start_month=$m;
?>
		<div class="bet common" style="margin-top: 25px; float: left;width:33%;">
			<div class="head" style="border-top-right-radius: 0px;">
				<!--div class="name icon-sweden"><?=$m?>月統計</div-->
				<div class="name icon-sweden">上周統計</div>
			</div>
			<div class="body" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr><td class="key">盈亏</td><td class="val"><?=$this->formatNum($month['money'])?></td></tr>
					<tr><td class="key">投注</td><td class="val"><?=$this->formatNum($month['betAmount'])?></td></tr>
					<tr><td class="key">充值</td><td class="val"><?=$this->formatNum($month['rechargeAmount'])?></td></tr>
					<tr><td class="key">提款</td><td class="val"><?=$this->formatNum($month['cashAmount'])?></td></tr>
				</table>
			</div>
		</div>
<?php
$month=$data['month2'];
$start	= strtotime('-2 month', $base);
$m=date('Y-m',$start);
$middle_month=date('Y-m',$start);
?>
		<div class="bet common" style="margin-top: 25px; float: left;width:33%;">
			<div class="head" style="border-top-right-radius: 0px;border-top-left-radius: 0px;border-right: 0px;border-left: 0px;">
				<!--div class="name icon-sweden"><?=$m?>月統計</div-->
				<div class="name icon-sweden">上上周統計</div>
			</div>
			<div class="body" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px;border-right: 0px;">
					<tr><td class="key">盈亏</td><td class="val"><?=$this->formatNum($month['money'])?></td></tr>
					<tr><td class="key">投注</td><td class="val"><?=$this->formatNum($month['betAmount'])?></td></tr>
					<tr><td class="key">充值</td><td class="val"><?=$this->formatNum($month['rechargeAmount'])?></td></tr>
					<tr><td class="key">提款</td><td class="val"><?=$this->formatNum($month['cashAmount'])?></td></tr>
				</table>
			</div>
		</div>	
<?php
$month=$data['month3'];
$start	= strtotime('-3 month', $base);
$m=date('Y-m',$start);
$end_month=$m;
?>
		<div class="bet common" style="margin-top: 25px; float: left;width:33%; margin-bottom: 20px;">
			<div class="head" style="border-top-left-radius: 0px;">
				<!--div class="name icon-sweden"><?=$m?>月統計</div-->
				<div class="name icon-sweden">上三周統計</div>
			</div>
			<div class="body" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0" >
					<tr><td class="key">盈亏</td><td class="val"><?=$this->formatNum($month['money'])?></td></tr>
					<tr><td class="key">投注</td><td class="val"><?=$this->formatNum($month['betAmount'])?></td></tr>
					<tr><td class="key">充值</td><td class="val"><?=$this->formatNum($month['rechargeAmount'])?></td></tr>
					<tr><td class="key">提款</td><td class="val"><?=$this->formatNum($month['cashAmount'])?></td></tr>
				</table>
			</div>
		</div>
		<div class="top-line" style="clear: both"></div>
		<div id="lotteryChart" style="width:100%; height:600px;"></div>
</div>



<script>
function runchart()
{
	if($.cookie('colorfile')!='')
	{
		var lotteryChart = echarts.init(document.getElementById('lotteryChart'),'vintage');
	}else
	{
		var lotteryChart = echarts.init(document.getElementById('lotteryChart'),'macarons');
	}
	
	var option = {
	    title : {
//	        text: '<?=$start_month?> ~ <?=$end_month?> 分析表',
	        text: '上周 ~ 上三周 分析表',
            textStyle:{
            	color:'#ddd',
            	fontWeight:'normal'
            },
            left: 'center'
	    },
	    tooltip : {
	        trigger: 'axis'
	    },
	    legend: {
            textStyle:{
            	color:'#ddd'
            },
	        orient: 'vertical',
	        left: 'left',
	        //data:['<?=$start_month?>','<?=$middle_month?>','<?=$end_month?>']
	        data:['上周','上上周','上三周']
	    },
	    toolbox: {
	        show : true,
	        feature : {
	            dataView : {show: true, readOnly: false},
	            magicType : {show: true, type: ['line', 'bar']},
	            restore : {show: true},
	            saveAsImage : {show: true}
	        }
	    },
	    calculable : true,
	    xAxis : [
	        {
	            type : 'category',
	            data : ['盈亏','投注','充值','提款']
	        }
	    ],
	    yAxis : [
	        {
	            type : 'value'
	        }
	    ],
	    series : [
	        {
	            //name:'<?=$start_month?>',
	            name:'上周',
	            type:'bar',
	            data:[<?=$data['month1']['money']?>, <?=$data['month1']['betAmount']?>, <?=$data['month1']['rechargeAmount']?>, <?=$data['month1']['cashAmount']?>]
	        },
	        {
	            //name:'<?=$middle_month?>',
	            name:'上上周',
	            type:'bar',
	            data:[<?=$data['month2']['money']?>, <?=$data['month2']['betAmount']?>, <?=$data['month2']['rechargeAmount']?>, <?=$data['month2']['cashAmount']?>]
	        },
	        {
	            //name:'<?=$end_month?>',
	            name:'上三周',
	            type:'bar',
	            data:[<?=$data['month3']['money']?>, <?=$data['month3']['betAmount']?>, <?=$data['month3']['rechargeAmount']?>, <?=$data['month3']['cashAmount']?>]
	        }
	    ]
	};	
    lotteryChart.setOption(option);
	window.addEventListener('resize', function () {
	lotteryChart.resize();
	});
};
setTimeout(function() {
	runchart();
}, 300);
	
</script>