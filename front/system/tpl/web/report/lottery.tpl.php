<div id="coin-log" class="common" style="margin-top: 25px;">
	<div class="head" style="border:0px;">
		<form action="/report/lottery" class="search" data-ispage="true" container="#report_view" target="ajax" func="form_submit">
			<div class="timer">
				<input type="text" autocomplete="off" name="fromTime" value="<?php echo date('Y-m-d', $this->request_time_from);?>" id="datetimepicker_fromTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<div class="sep icon-exchange"></div>
			<div class="timer">
				<input type="text" autocomplete="off" name="toTime" value="<?php echo date('Y-m-d', $this->request_time_to);?>" id="datetimepicker_toTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<button type="submit" class="btn btn-brown icon-search">查询</button>
		</form>
	</div>
</div>
<div class="bet common" style="margin-top: 10px; width:100%;">	
	<div id="lotteryChart1" style="margin-top: 10px;width:50%; height:500px;float: left;"></div>
	<div id="lotteryChart2" style="margin-top: 10px;width:50%; height:500px;float: left;"></div>
	<div id="lotteryChart3" style="margin-top: 10px;width:50%; height:500px;float: left;"></div>
	<div class="head" style="border-top-right-radius: 0px;clear: both">
		<div class="name icon-sweden">彩种统计</div>
	</div>
	<div class="body" >
		<ul class="lottery_ul">
<?php	
$types = $this->get_types();
$LPie1=array();
$LPieData1=array();
$LPie2=array();
$LPieData2=array();
$LPie3=array();
$LPieData3=array();
foreach ($types as $name => $list) {
	foreach ($list as $v) {
		$setColor="";
		if(!isset($today[$v['id']]))
		{
			$today[$v['id']]['betAmount']=0;
			$today[$v['id']]['actionNum']=0;
			$today[$v['id']]['norder']=0;
		}
		if(!isset($yestoday[$v['id']]))
		{
			$yestoday[$v['id']]['betAmount']=0;
			$yestoday[$v['id']]['actionNum']=0;
			$yestoday[$v['id']]['norder']=0;
		}		
		if(!isset($total[$v['id']]))
		{
			$total[$v['id']]['win_show']=false;
			$total[$v['id']]['betAmount']=0;
			$total[$v['id']]['bonus']=0;
			$total[$v['id']]['win']=0;
			$total[$v['id']]['tip']="";
		}
		else 
		{
			$total[$v['id']]['win_show']=false;
			if($total[$v['id']]['betAmount'] > 0)
			{
				$total[$v['id']]['win_show']=true;
				$setColor="color: #FFAA0D;";
				$total[$v['id']]['win']= number_format(($total[$v['id']]['bonus']/$total[$v['id']]['betAmount']),2,".","")." %";
				$total[$v['id']]['tip']= $total[$v['id']]['bonus']." / ".$total[$v['id']]['betAmount'];
				$LPieData1[]="{value:".$total[$v['id']]['betAmount'].", name:'".$v['title']."'}";
				$LPie1[]=$v['title'];
				$LPieData2[]="{value:".$total[$v['id']]['bonus'].", name:'".$v['title']."'}";
				$LPie2[]=$v['title'];
				$LPieData3[]="{value:".$total[$v['id']]['actionNum'].", name:'".$v['title']."'}";
				$LPie3[]=$v['title'];								
			}else
			{
				$total[$v['id']]['win']=0;
				$total[$v['id']]['tip']="";				
			}
		}
?>
			<li class="lottery_li tip" data-memo="<?=$total[$v['id']]['tip']?>" action="tip-memo">
				<div class="lottery_div big">
					<span class="icon-award" style="font-size: 14px ; line-height: 30px; <?=$setColor?>"></span><span><?php echo $v['title'];?></span>
					<div>总投注
						<?php
							$css="";
							if($today[$v['id']]['betAmount']>0)
							{
								$css="#B05E54";
							}else
							{
								$css="#555";
							}
						?> 
						<span style="color: <?=$css?>">￥<?=$this->formatNum($today[$v['id']]['betAmount'])?> / <?=$today[$v['id']]['actionNum']?>注 / <?=$today[$v['id']]['norder']?>单</span>
					</div>
					<div>累积中投比 
						<?php
							$css="";
							if($total[$v['id']]['win_show'])
							{
								$css="#23A966";
							}else
							{
								$css="#555";
							}
						?>
						<span style="color: <?=$css?>"><?=$total[$v['id']]['win']?> (中奖金额/投注金额)</span>
					</div>
				</div>
			</li>	
	<?php
	}
}
?>
		</ul>
	</div>
</div>
<div id="tip-memo" class="play-eg hide"></div>
<script>
function runchart()
{
	if($.cookie('colorfile')!='')
	{
		var lotteryChart1 = echarts.init(document.getElementById('lotteryChart1'),'vintage');
		var lotteryChart2 = echarts.init(document.getElementById('lotteryChart2'),'vintage');
		var lotteryChart3 = echarts.init(document.getElementById('lotteryChart3'),'vintage');
	
	}else
	{
		var lotteryChart1 = echarts.init(document.getElementById('lotteryChart1'),'macarons');
		var lotteryChart2 = echarts.init(document.getElementById('lotteryChart2'),'macarons');
		var lotteryChart3 = echarts.init(document.getElementById('lotteryChart3'),'macarons');
	}
	option = {
	    title : {
	        text: '总投注金额比例',
	        x:'center'
	    },
	    tooltip : {
	        trigger: 'item',
	        formatter: "{a} <br/>{b} : {c} ({d}%)"
	    },
	    legend: {
	        orient: 'vertical',
	        left: 'left',
	        data: ['<?=implode("','",$LPie1)?>'],
            textStyle:{
            	color:'#ddd'
            }	        
	    },
	    series : [
	        {
	            name: '总投注金额比例',
	            type: 'pie',
	            radius : '55%',
	            center: ['50%', '60%'],
	            data:[
	                <?=implode(",",$LPieData1)?>
	            ],
	            itemStyle: {
	                emphasis: {
	                    shadowBlur: 10,
	                    shadowOffsetX: 0,
	                    shadowColor: 'rgba(0, 0, 0, 0.5)'
	                }
	            }
	        }
	    ]
	};
	lotteryChart1.setOption(option);
    option.title.text="总中奖金额比例";
    option.series[0].name="总中奖金额比例";
    option.legend.data=['<?=implode("','",$LPie2)?>'];
    option.series[0].data=[<?=implode(",",$LPieData2)?>];
    lotteryChart2.setOption(option);
    option.title.text="总注数量比例";
    option.series[0].name="总注数量比例";
    option.legend.data=['<?=implode("','",$LPie3)?>'];
    option.series[0].data=[<?=implode(",",$LPieData3)?>];
    lotteryChart3.setOption(option);
	window.addEventListener('resize', function () {
	lotteryChart1.resize();
	lotteryChart2.resize();
	lotteryChart3.resize();
	});
}
setTimeout(function() {
	runchart();
}, 300);
$(function() {
	// 时间选择插件
	$('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
});
</script>
