<div id="coin-log" class="common" style="margin-top: 25px;">
	<div class="head" style="border:0px;">
		<form action="/report/transfer" class="search" data-ispage="true" container="#report_view" target="ajax" func="form_submit">
			<div class="timer">
				<input type="text" autocomplete="off" name="fromTime" value="<?php echo date('Y-m-d', $this->request_time_from);?>" id="datetimepicker_fromTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<div class="sep icon-exchange"></div>
			<div class="timer">
				<input type="text" autocomplete="off" name="toTime" value="<?php echo date('Y-m-d', $this->request_time_to);?>" id="datetimepicker_toTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<button type="submit" class="btn btn-brown icon-search">查询</button>
		</form>
	</div>
</div>
<div class="bet common" style="margin-top: 40px; margin-right: 12px;">
	<div class="head" style="border-top-right-radius: 0px;">
		<div class="name icon-sweden">上下级转账</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="key key_left" style="text-align: left;">上下级转账总金额</td><td class="val"><?=$this->formatNum($total['transfer'])?></td>
				<td class="key key_left" style="text-align: left;">上下级转账次数</td><td class="val"><?=$total['transferCo']?></td>
			</tr>
			<tr>
				<td class="key key_left" style="text-align: left;">上级转账总金额</td><td class="val"><?=$this->formatNum($total['headTransfer'])?></td>
				<td class="key key_left" style="text-align: left;">上级转账次数</td><td class="val"><?=$total['headTransferCo']?></td>
			</tr>
			<tr>
				<td class="key key_left" style="text-align: left;">下级转账总金额</td><td class="val"><?=$this->formatNum($total['sonTransfer'])?></td>
				<td class="key key_left" style="text-align: left;">下级转账次数</td><td class="val"><?=$total['sonTransferCo']?></td>
			</tr>
		</table>
	</div>
</div>

<?php
$month=$month1;
$base = strtotime(date('Y-m',time()) . '-01 00:00:00');
$start	= strtotime('-1 month', $base);
$m=date('Y-m',$start);
?>
		<div class="bet common" style="margin-top: 25px; float: left;width:33%;">
			<div class="head" style="border-top-right-radius: 0px;">
				<!--div class="name icon-sweden"><?=$m?>月統計</div-->
				<div class="name icon-sweden">上周統計</div>
			</div>
			<div class="body" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr><td class="key">上下级转账总金额</td><td class="val"><?=$this->formatNum($month['transfer'])?></td></tr>
					<tr><td class="key">上级转账总金额</td><td class="val"><?=$this->formatNum($month['headTransfer'])?></td></tr>
					<tr><td class="key">下级转账总金额</td><td class="val"><?=$this->formatNum($month['sonTransfer'])?></td></tr>
				</table>
			</div>
		</div>
<?php
$month=$month2;
$start	= strtotime('-2 month', $base);
$m=date('Y-m',$start);
?>
		<div class="bet common" style="margin-top: 25px; float: left;width:33%;">
			<div class="head" style="border-top-right-radius: 0px;border-top-left-radius: 0px;border-right: 0px;border-left: 0px;">
				<!--div class="name icon-sweden"><?=$m?>月統計</div-->
				<div class="name icon-sweden">上上周統計</div>
			</div>
			<div class="body" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px;border-right: 0px;">
					<tr><td class="key">上下级转账总金额</td><td class="val"><?=$this->formatNum($month['transfer'])?></td></tr>
					<tr><td class="key">上级转账总金额</td><td class="val"><?=$this->formatNum($month['headTransfer'])?></td></tr>
					<tr><td class="key">下级转账总金额</td><td class="val"><?=$this->formatNum($month['sonTransfer'])?></td></tr>
				</table>
			</div>
		</div>	
<?php
$month=$month3;
$start	= strtotime('-3 month', $base);
$m=date('Y-m',$start);
?>
		<div class="bet common" style="margin-top: 25px; float: left;width:33%;">
			<div class="head" style="border-top-left-radius: 0px;">
				<!--div class="name icon-sweden"><?=$m?>月統計</div-->
				<div class="name icon-sweden">上三周統計</div>
			</div>
			<div class="body" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0" >
					<tr><td class="key">上下级转账总金额</td><td class="val"><?=$this->formatNum($month['transfer'])?></td></tr>
					<tr><td class="key">上级转账总金额</td><td class="val"><?=$this->formatNum($month['headTransfer'])?></td></tr>
					<tr><td class="key">下级转账总金额</td><td class="val"><?=$this->formatNum($month['sonTransfer'])?></td></tr>
				</table>
			</div>
		</div>
<script>
$(function() {
	// 时间选择插件
	$('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
});
</script>