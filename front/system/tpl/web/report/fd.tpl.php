<div id="coin-log" class="common" style="margin-top: 25px;">
	<div class="head" style="border:0px;">
		<form action="/report/fd" class="search" data-ispage="true" container="#report_view" target="ajax" func="form_submit">
			<div class="timer">
				<input type="text" autocomplete="off" name="fromTime" value="<?php echo date('Y-m-d', $this->request_time_from);?>" id="datetimepicker_fromTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<div class="sep icon-exchange"></div>
			<div class="timer">
				<input type="text" autocomplete="off" name="toTime" value="<?php echo date('Y-m-d', $this->request_time_to);?>" id="datetimepicker_toTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<button type="submit" class="btn btn-brown icon-search">查询</button>
		</form>
	</div>
</div>
<div class="bet common" style="margin-top: 40px; margin-right: 12px;">
	<div class="head" style="border-top-right-radius: 0px;">
		<div class="name icon-sweden">返点
<?php
$item=array();
$item[]="返点";
if($this->user['fenHong']>0)
{
	$item[]="分红";
?>
			 , 分红
<?php
}
?>
<?php
if($this->user['gongZi']>0)
{
	$item[]="日工资";
?>

			 , 日工资
<?php
}
?>
			</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="key key_left" style="text-align: left;">返点总金额</td><td class="val"><?=$this->formatNum($total['fanDian'])?></td>
				<td class="key key_left" style="text-align: left;">返点次数</td><td class="val"><?=$total['fanDianCo']?></td>
			</tr>
<?php
if($this->user['fenHong']>0)
{
?>
			<!--tr>
				<td class="key key_left" style="text-align: left;">分红总金额</td><td class="val"><?=$this->formatNum($total['bonus'])?></td>
				<td class="key key_left" style="text-align: left;">分红次数</td><td class="val"><?=$total['bonusCo']?></td>
			</tr-->
<?php
}
?>
<?php
if($this->user['gongZi']>0)
{
?>
			<tr>
				<td class="key key_left" style="text-align: left;">日工资总金额</td><td class="val"><?=$this->formatNum($total['gongzi'])?></td>
				<td class="key key_left" style="text-align: left;">日工资次数</td><td class="val"><?=$total['gongziCo']?></td>
			</tr>
<?php
}
?>
		</table>
	</div>
</div>

<?php
$month=$month1;
$base = strtotime(date('Y-m',time()) . '-01 00:00:00');
$start	= strtotime('-1 month', $base);
$m=date('Y-m',$start);
$start_month=$m;
$item_m1=array();
$item_m1[]=$month['fanDian'];
?>
		<div class="bet common" style="margin-top: 25px; float: left;width:33%;">
			<div class="head" style="border-top-right-radius: 0px;">
				<!--div class="name icon-sweden"><?=$m?>月統計</div-->
				<div class="name icon-sweden">上周統計</div>
			</div>
			<div class="body" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td class="key key_left" style="text-align: left;">返点总金额</td><td class="val"><?=$this->formatNum($month['fanDian'])?></td>
					<td class="key key_left" style="text-align: left;">返点次数</td><td class="val"><?=$month['fanDianCo']?></td>
				</tr>
<?php
if($this->user['fenHong']>0)
{
	//$item_m1[]=$month['bonus'];
?>
				<!--tr>
					<td class="key key_left" style="text-align: left;">分红总金额</td><td class="val"><?=$this->formatNum($month['bonus'])?></td>
					<td class="key key_left" style="text-align: left;">分红次数</td><td class="val"><?=$month['bonusCo']?></td>
				</tr-->
<?php
}
?>
<?php
if($this->user['gongZi']>0)
{
	$item_m1[]=$month['gongzi'];
?>
				<tr>
					<td class="key key_left" style="text-align: left;">日工资总金额</td><td class="val"><?=$this->formatNum($month['gongzi'])?></td>
					<td class="key key_left" style="text-align: left;">日工资次数</td><td class="val"><?=$month['gongziCo']?></td>
				</tr>
<?php
}
?>
				</table>
			</div>
		</div>
<?php
$month=$month2;
$item_m2=array();
$start	= strtotime('-2 month', $base);
$m=date('Y-m',$start);
$item_m2[]=$month['fanDian'];
$middle_month=date('Y-m',$start);
?>
		<div class="bet common" style="margin-top: 25px; float: left;width:33%;">
			<div class="head" style="border-top-right-radius: 0px;border-top-left-radius: 0px;border-right: 0px;border-left: 0px;">
				<!--div class="name icon-sweden"><?=$m?>月統計</div-->
				<div class="name icon-sweden">上上周統計</div>
			</div>
			<div class="body" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px;border-right: 0px;">
				<tr>
					<td class="key key_left" style="text-align: left;">返点总金额</td><td class="val"><?=$this->formatNum($month['fanDian'])?></td>
					<td class="key key_left" style="text-align: left;">返点次数</td><td class="val"><?=$month['fanDianCo']?></td>
				</tr>
<?php
if($this->user['fenHong']>0)
{
	//$item_m2[]=$month['bonus'];
?>
				<!--tr>
					<td class="key key_left" style="text-align: left;">分红总金额</td><td class="val"><?=$this->formatNum($month['bonus'])?></td>
					<td class="key key_left" style="text-align: left;">分红次数</td><td class="val"><?=$month['bonusCo']?></td>
				</tr-->
<?php
}
?>
<?php
if($this->user['gongZi']>0)
{
	$item_m2[]=$month['gongzi'];
?>
				<tr>
					<td class="key key_left" style="text-align: left;">日工资总金额</td><td class="val"><?=$this->formatNum($month['gongzi'])?></td>
					<td class="key key_left" style="text-align: left;">日工资次数</td><td class="val"><?=$month['gongziCo']?></td>
				</tr>
<?php
}
?>
				</table>
			</div>
		</div>	
<?php
$month=$month3;
$item_m3=array();
$start	= strtotime('-3 month', $base);
$m=date('Y-m',$start);
$item_m3[]=$month['fanDian'];
$end_month=$m;
?>
		<div class="bet common" style="margin-top: 25px; float: left;width:33%;margin-bottom: 20px;">
			<div class="head" style="border-top-left-radius: 0px;">
				<!--div class="name icon-sweden"><?=$m?>月統計</div-->
				<div class="name icon-sweden">上三周統計</div>
			</div>
			<div class="body" >
				<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr>
					<td class="key key_left" style="text-align: left;">返点总金额</td><td class="val"><?=$this->formatNum($month['fanDian'])?></td>
					<td class="key key_left" style="text-align: left;">返点次数</td><td class="val"><?=$month['fanDianCo']?></td>
				</tr>
<?php
if($this->user['fenHong']>0)
{
	//$item_m3[]=$month['bonus'];
?>
				<!--tr>
					<td class="key key_left" style="text-align: left;">分红总金额</td><td class="val"><?=$this->formatNum($month['bonus'])?></td>
					<td class="key key_left" style="text-align: left;">分红次数</td><td class="val"><?=$month['bonusCo']?></td>
				</tr-->
<?php
}
?>
<?php
if($this->user['gongZi']>0)
{
	$item_m3[]=$month['gongzi'];
?>
				<tr>
					<td class="key key_left" style="text-align: left;">日工资总金额</td><td class="val"><?=$this->formatNum($month['gongzi'])?></td>
					<td class="key key_left" style="text-align: left;">日工资次数</td><td class="val"><?=$month['gongziCo']?></td>
				</tr>
<?php
}
?>
				</table>
			</div>
		</div>
<div id="lotteryChart" style="width:100%; height:600px;float: left;"></div>
<script>
function runchart()
{
	if($.cookie('colorfile')!='')
	{
        var lotteryChart = echarts.init(document.getElementById('lotteryChart'),'vintage');
	
	}else
	{
        var lotteryChart = echarts.init(document.getElementById('lotteryChart'),'macarons');
	}
	var option = {
	    title : {
	        text: '上周 ~ 上三周 分析表',
            textStyle:{
            	color:'#ddd',
            	fontWeight:'normal'
            },
            left: 'center'
	    },
	    tooltip : {
	        trigger: 'axis'
	    },
	    legend: {
            textStyle:{
            	color:'#ddd'
            },
	        orient: 'vertical',
	        left: 'left',
	        data:['上周','上上周','上三周']
	    },
	    toolbox: {
	        show : true,
	        feature : {
	            dataView : {show: true, readOnly: false},
	            magicType : {show: true, type: ['line', 'bar']},
	            restore : {show: true},
	            saveAsImage : {show: true}
	        }
	    },
	    calculable : true,
	    xAxis : [
	        {
	            type : 'category',
	            data : ['<?=implode("','",$item)?>']
	        }
	    ],
	    yAxis : [
	        {
	            type : 'value'
	        }
	    ],
	    series : [
	        {
	            name:'上周',
	            type:'bar',
	            data:[<?=implode(",",$item_m1)?>]
	        },
	        {
	            name:'上上周',
	            type:'bar',
	            data:[<?=implode(",",$item_m2)?>]
	        },
	        {
	            name:'上三周',
	            type:'bar',
	            data:[<?=implode(",",$item_m3)?>]
	        }
	    ]
	};	
    lotteryChart.setOption(option);
	window.addEventListener('resize', function () {
	lotteryChart.resize();
	});
};
setTimeout(function() {
	runchart();
}, 300);
$(function() {
	// 时间选择插件
	$('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
});
</script>