<div id="coin-log" class="common" style="margin-top: 25px;">
	<div class="head" style="border:0px;">
		<form action="/report/teamcash" class="search" data-ispage="true" container="#report_view" target="ajax" func="form_submit">
			<div class="timer">
				<input type="text" autocomplete="off" name="fromTime" value="<?php echo date('Y-m-d', $this->request_time_from);?>" id="datetimepicker_fromTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<div class="sep icon-exchange"></div>
			<div class="timer">
				<input type="text" autocomplete="off" name="toTime" value="<?php echo date('Y-m-d', $this->request_time_to);?>" id="datetimepicker_toTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<button type="submit" class="btn btn-brown icon-search">查询</button>
		</form>
	</div>
</div>
<div class="bet common" style="margin-top: 25px; width:100%;float: left;">
	<div class="head" style="border-top-right-radius: 0px;">
		<div class="name icon-sweden">提款</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="key key_left" style="text-align: left;">提款总额</td><td class="val"><?=$this->formatNum($tcharge['amount'])?></td>
				<td class="key key_left" style="text-align: left;">提款申请笔数</td><td class="val"><?=$tcharge['co']?></td>
				<td class="key key_left" style="text-align: left;">提款成功笔数</td><td class="val"><?=$tcharge['ok']?></td>
			</tr>
		</table>
	</div>
</div>
<?php
$banks = array(
		'3' => '广东发展银行', // 广东发展银行v
        '4' => '华夏银行',  // 华夏银行v
        '5' => '交通银行', // 交通银行 v
        '6' => '平安银行', // 平安银行v
        '7' => '上海浦东发展银行', // 上海浦东发展银行v
        '9' => '中国邮政储蓄银行', // 中国邮政储蓄银行v
        '10' => '中国光大银行', // 中国光大银行v
		'11' => '中国工商银行', // 中国工商银行 v
        '12' => '中国建设银行', // 中国建设银行 v
        '13' => '中国民生银行', // 中国民生银行v
        '14' => '中国农业银行', // 中国农业银行 v
        '15' => '中国银行', // 中国银行 v
        '16' => '招商银行', // 招商银行v
        '17' => '中信银行', // 中信银行v
		'8' => '兴业银行', // 兴业银行 v
	);
$RPie=array();
$RPieAmount=array();
$RPieTime=array();
?>
<div id="lotteryChart1" style="margin-top: 40px;width:50%; height:280px;float: left"></div>
<div id="lotteryChart2" style="margin-top: 40px;width:50%; height:280px;float: left"></div>

<div class="bet common" style="margin-top: 25px; width:100%;float: left;">
	<div class="head" style="border-top-right-radius: 0px;">
		<div class="name icon-sweden">银行提款区分</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<?php
		foreach($banks as $k=>$v)
		{
			if($bank['data'][$k]['amount']>0)
			{
				$RPie[]=$v;
				$RPieAmount[]="{value:".$bank['data'][$k]['amount'].", name:'".$v."'}";
				$RPieTime[]="{value:".$bank['data'][$k]['ok'].", name:'".$v."'}";
			}
		?>
			<tr>
				<td class="key key_left" style="width:150px;text-align: left;"><?=$v?> <span class="icon-right-open-big"></span> </td>
				<td class="key key_left" style="text-align: left;">提款总额</td><td class="val"><?=$this->formatNum($bank['data'][$k]['amount'])?></td>
				<td class="key key_left" style="text-align: left;">提款申请笔数</td><td class="val"><?=$bank['data'][$k]['co']?></td>
				<td class="key key_left" style="text-align: left;">提款成功笔数</td><td class="val"><?=$bank['data'][$k]['ok']?></td>
			</tr>		
		<?php
		}
		?>
		</table>
	</div>
</div>
<style>
	.key_left{
		text-align: left;
	}
</style>

<script type="text/javascript">
function runchart()
{
	if($.cookie('colorfile')!='')
	{
        var lotteryChart1 = echarts.init(document.getElementById('lotteryChart1'),'vintage');
        var lotteryChart2 = echarts.init(document.getElementById('lotteryChart2'),'vintage');

	}else
	{
        var lotteryChart1 = echarts.init(document.getElementById('lotteryChart1'),'macarons');
        var lotteryChart2 = echarts.init(document.getElementById('lotteryChart2'),'macarons');
	}
		var option = {
		    title : {
		        text: '总提款金额比例',
		        textStyle:{
	            	color:'#ddd',
	            	fontWeight:'normal'
	            },
		        x:'center'
		    },
		    tooltip : {
		        trigger: 'item',
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    },
		    legend: {
		        orient: 'vertical',
		        left: 'left',
		        data: ['<?=implode("','",$RPie)?>'],
	            textStyle:{
	            	color:'#ddd'
	            }	        
		    },
		    series : [
		        {
		            name: '总提款金额比例',
		            type: 'pie',
		            radius : '55%',
		            center: ['50%', '60%'],
		            data:[<?=implode(",",$RPieAmount)?>],
		            itemStyle: {
		                emphasis: {
		                    shadowBlur: 10,
		                    shadowOffsetX: 0,
		                    shadowColor: 'rgba(0, 0, 0, 0.5)'
		                }
		            }
		        }
		    ]
		};
		lotteryChart1.setOption(option);
	    option.title.text="提款成功比例";
	    option.series[0].name="提款成功比例";	
	    option.series[0].data=[<?=implode(",",$RPieTime)?>];	
		lotteryChart2.setOption(option);
		// 使用刚指定的配置项和数据显示图表。
		window.addEventListener('resize', function () {
			lotteryChart1.resize();
			lotteryChart2.resize();
			myChart.resize();
		});
};
setTimeout(function() {
	runchart();
}, 300);
$(function() {
	// 时间选择插件
	$('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
});
</script>