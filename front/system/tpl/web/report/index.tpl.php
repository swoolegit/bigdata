<div style="border-bottom: 1px solid #434857;">
	<ul class="recharge-ul" >
		<li class="recharge-tab recharge-on" data-action="money">
			<a>实时数据</a>
		</li>
<?php
if($this->user['type']>0)
{
?>

		<li class="recharge-tab" data-action="team">
			<a>团队状况</a>
		</li>
		<li class="recharge-tab" data-action="teammoney">
			<a>团队资金</a>
		</li>
		<li class="recharge-tab" data-action="teamlottery_chart">
			<a>团队游戏</a>
		</li>
<?php
}
?>
		<li class="recharge-tab" data-action="money2">
			<a>个人资金</a>
		</li>
		<li class="recharge-tab" data-action="game">
			<a>个人游戏</a>
		</li>
		<!--li class="recharge-tab" data-action="game">
			<a>个人游戏</a>
		</li-->
		<!--li class="recharge-tab" data-action="money">
			<a>大盘数据</a>
		</li-->		
	</ul>
</div>
<div class="report_top" >
	<ul class="report_ul"></ul>
<?php
if($this->user['type']>0)
{
?>
	<ul class="report_ul hide">
		<li><a class="report_li on" data-action="team">成员</a></li><li><a class="report_li" data-action="teamorder">來源</a></li>
	</ul>
	<ul class="report_ul hide">
		<li><a class="report_li" data-action="teammoney">盈亏</a></li><li><a class="report_li" data-action="teamrecharge">充值</a></li><li><a class="report_li" data-action="teamcash">提款</a></li><li><a class="report_li" data-action="teamtransfer">转账</a></li><li><a class="report_li" data-action="teamfd">返点/其他</a></li>
	</ul>
	<ul class="report_ul hide">
		<li><a class="report_li" data-action="teamlottery_chart">彩种</a></li><li><a class="report_li" data-action="teamplay">玩法</a></li>
	</ul>
<?php
}
?>
	<ul class="report_ul hide">
		<li><a class="report_li" data-action="money2">盈亏</a></li><li><a class="report_li" data-action="recharge">充值</a></li><li><a class="report_li" data-action="cash">提款</a></li><li><a class="report_li" data-action="transfer">转账</a></li><li><a class="report_li" data-action="fd">返点/其他</a></li>
	</ul>
	<ul class="report_ul hide">
		<li><a class="report_li on" data-action="game">游戏</a></li><li><a class="report_li" data-action="lottery">彩种</a></li><li><a class="report_li" data-action="play">玩法</a></li>
	</ul>	
	<ul class="report_ul hide">		
	</ul>
	<ul class="report_ul hide">		
	</ul>
</div>
<div class="report" id="report_view" style="width: 100%;margin-top: 25px;">
	<?php require(TPL.'/report/money.tpl.php');?>
</div>
<script type="text/javascript">
$(function() {
	$('.recharge-tab').click(function (){
		var _index = $('.recharge-tab').index(this);
        $('.recharge-tab').removeClass('recharge-on');
        $(this).addClass('recharge-on');
        $('.report_ul').hide().eq(_index).fadeIn();
        $('.report_li').removeClass('on');
<?php
if($this->user['type']>0)
{
?>
        $('.report_li').eq(0).addClass('on');
        $('.report_li').eq(2).addClass('on');
        $('.report_li').eq(7).addClass('on');
        $('.report_li').eq(9).addClass('on');
        $('.report_li').eq(14).addClass('on');
<?php
}else{
?>
        $('.report_li').eq(0).addClass('on');
        $('.report_li').eq(5).addClass('on');
<?php
}
?>
        get_report($(this).data("action"));
	});
	$('.report_li').click(function (){
		$('.report_li').removeClass('on');
		$(this).addClass('on');
		get_report($(this).data("action"));
	});
	$('#home').removeClass('on');
	$('#report-money').addClass('on');
});
function get_report(action,target)
{
	if(target==null)
	{
		target = $('#report_view');
	}
	loadpage.before(function() {
		var complete = function(err, data) {
			loadpage.close(function() {
				if (err) {
					$.error(err);
				} else {
					target.html(data);
				}
			});
		};
		$.ajax({
			url: '/report/' + action,
			cache: false,
			data: {},
			type: 'POST',
			dataType: 'html',
			error: function(xhr, textStatus, errThrow) {
				$.error(errThrow || textStatus);
			},
			success:function(data, textStatus, xhr) {
				//$.error(data);
				//target.html(data);
				var errorMessage = xhr.getResponseHeader('X-Error-Message');
				//console.log(errorMessage)
				if (errorMessage) {
					complete(errorMessage === 'dialogue' ? data : decodeURIComponent(errorMessage));
				} else {
					complete(null, data);
				}				
			}
		});
	});
}
</script>