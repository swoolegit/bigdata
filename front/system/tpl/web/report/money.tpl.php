	<div class="money_head">
		<div class="money_title" >当前余额</div><div class="money_val"><?=$this->formatNum($member['coin']+($member['fcoin']*-1))?></div>
		<div class="money_title">冻结金额</div><div class="money_val"><?=$this->formatNum($member['fcoin']*-1)?></div>
		<div class="money_title">可提余额</div><div class="money_val" style="border-right: 0px;"><?=$this->formatNum($member['coin'])?></div>
	</div>
<?php
if($this->user['type']>0)
{
	$tdata=$data['tdata'];
?>
	<div class="bet common" style="margin-top: 25px;">
		<div class="head" >
			<div class="name icon-sweden">团队实时数据</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr>
					<td class="key">团队总人数</td><td class="val"><?=($tdata['agent']+$tdata['member'])?></td>
					<td class="key">团队总余额</td><td class="val"><?=$tdata['total_coin']?></td>
					<td class="key">代理总数</td><td class="val"><?=intval($tdata['agent'])?></td>
					<td class="key">会员总数</td><td class="val"><?=intval($tdata['member'])?></td>
					<td class="key"> -- </td><td class="val"> -- </td>
				</tr>
				<tr>
					<td class="key">今日注册数</td><td class="val"><?=$tdata['new']?></td>
					<td class="key">昨日注册数</td><td class="val"><?=$tdata['newY']?></td>
					<td class="key">当前在线人数</td><td class="val"><?=($tdata['online_mobile']+$tdata['online_pc'])?></td>
					<td class="key">页端在线</td><td class="val"><?=$tdata['online_pc']?></td>
					<td class="key">移动端在线</td><td class="val"><?=$tdata['online_mobile']?></td>
				</tr>
			</table>
		</div>
	</div>	
<?php
	$today=$tdata['TeamTD'];
?>
	<div class="bet common" style="margin-top: 25px; float: left;width:50%;">
		<div class="head" style="border-top-right-radius: 0px;">
			<div class="name icon-sweden">团队今日数据</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td class="key">今日盈亏</td><td class="val"><?=$today['money']?></td></tr>
				<tr><td class="key">今日投注</td><td class="val"><?=$today['betAmount']?></td></tr>
				<tr><td class="key">今日中奖</td><td class="val"><?=$today['zjAmount']?></td></tr>
			</table>
		</div>
	</div>
<?php
	$yestoday=$tdata['TeamYE'];
?>
	<div class="bet common" style="margin-top: 25px; float: left;width:50%;">
		<div class="head" style="border-top-right-radius: 0px;border-top-left-radius: 0px;border-left: 0px;">
			<div class="name icon-sweden">团队昨日数据</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px;">
				<tr><td class="key">昨日盈亏</td><td class="val"><?=$yestoday['money']?></td></tr>
				<tr><td class="key">昨日投注</td><td class="val"><?=$yestoday['betAmount']?></td></tr>
				<tr><td class="key">昨日中奖</td><td class="val"><?=$yestoday['zjAmount']?></td></tr>
			</table>
		</div>
	</div>
	
	<div class="bet common" style="margin-top: 25px; float: left;width:50%;">
		<div class="head" style="border-top-right-radius: 0px;">
			<div class="name icon-sweden">团队今日充值</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr><td class="key">充值总额</td><td class="val"><?=$today['rechargeAmount']?></td></tr>
				<tr><td class="key">微信充值总额</td><td class="val"><?=$today['recharge_wc']?></td></tr>
				<tr><td class="key">支付宝充值总额</td><td class="val"><?=$today['recharge_ali']?></td></tr>
				<tr><td class="key">QQ钱包充值总额</td><td class="val"><?=$today['recharge_qq']?></td></tr>
				<tr><td class="key">快捷支付充值总额</td><td class="val"><?=$today['recharge_qp']?></td></tr>				
				<tr><td class="key">网银充值总额</td><td class="val"><?=$today['recharge_bank']?></td></tr>
			</table>
		</div>
		<div class="head" style="border-radius: 0px;border: 0px;border-right: 1px solid #434857;border-left: 1px solid #434857;">
			<div class="name icon-sweden">团队今日提款</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr><td class="key">提款总额</td><td class="val"><?=$today['cashAmount']?></td></tr>
			</table>
		</div>
	</div>

	<div class="bet common" style="margin-top: 25px; float: left;width:50%;">
		<div class="head" style="border-top-right-radius: 0px;border-top-left-radius: 0px;border-left: 0px;">
			<div class="name icon-sweden">团队昨日充值</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px;">
				<tr><td class="key">充值总额</td><td class="val"><?=$yestoday['rechargeAmount']?></td></tr>
				<tr><td class="key">微信充值总额</td><td class="val"><?=$yestoday['recharge_wc']?></td></tr>
				<tr><td class="key">支付宝充值总额</td><td class="val"><?=$yestoday['recharge_ali']?></td></tr>
				<tr><td class="key">QQ钱包充值总额</td><td class="val"><?=$yestoday['recharge_qq']?></td></tr>
				<tr><td class="key">快捷支付充值总额</td><td class="val"><?=$yestoday['recharge_qp']?></td></tr>				
				<tr><td class="key">网银充值总额</td><td class="val"><?=$yestoday['recharge_bank']?></td></tr>
			</table>
		</div>
		<div class="head" style="border-radius: 0px;border: 0px;border-right: 1px solid #434857;border-left: 0px ">
			<div class="name icon-sweden">团队昨日提款</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px;">
				<tr><td class="key">提款总额</td><td class="val"><?=$yestoday['cashAmount']?></td></tr>
			</table>
		</div>

	</div>
<?php
}
$today=$data['today'];
?>
	<div class="bet common" style="margin-top: 25px; float: left;width:50%;">
		<div class="head" style="border-top-right-radius: 0px;">
			<div class="name icon-sweden">个人今日盈亏</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr><td class="key">今日盈亏</td><td class="val"><?=$today['money']?></td></tr>
			</table>
		</div>

		<div class="head" style="border-radius: 0px;border: 0px;border-right: 1px solid #434857;border-left: 1px solid #434857;">
			<div class="name icon-sweden">个人今日数据</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td class="key">今日投注</td><td class="val"><?=$today['betAmount']?></td></tr>
				<tr><td class="key">今日中奖</td><td class="val"><?=$today['zjAmount']?></td></tr>
				<tr><td class="key">今日返点</td><td class="val"><?=$today['fanDianAmount']?></td></tr>
				<tr><td class="key">今日活动奖励</td><td class="val"><?=$today['brokerageAmount']?></td></tr>
				<tr><td class="key">今日上下级转账</td><td class="val"><?=$today['transfer']?></td></tr>
<?php
if($this->user['gongZi']>0)
{
?>
				<tr><td class="key">今日日工资</td><td class="val"><?=$today['gongzi']?></td></tr>
<?php 
}
?>
<?php
if($this->user['fenHong']>0)
{
?>
				<!--tr><td class="key">今日分红累计</td><td class="val"><?=$today['bonusAmount']?></td></tr-->
<?php 
}
?>
			</table>
		</div>
	</div>
<?php
$yestoday=$data['yestoday'];
?>
	<div class="bet common" style="margin-top: 25px; float: left;width:50%;">
		<div class="head" style="border-top-right-radius: 0px;border-top-left-radius: 0px;border-left: 0px;">
			<div class="name icon-sweden">个人昨日盈亏</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px;">
				<tr><td class="key">昨日盈亏</td><td class="val"><?=$yestoday['money']?></td></tr>
			</table>
		</div>

		<div class="head" style="border-radius: 0px;border: 0px;border-right: 1px solid #434857;border-left: 0px ">
			<div class="name icon-sweden">个人昨日数据</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px;">
				<tr><td class="key">昨日投注</td><td class="val"><?=$yestoday['betAmount']?></td></tr>
				<tr><td class="key">昨日中奖</td><td class="val"><?=$yestoday['zjAmount']?></td></tr>
				<tr><td class="key">昨日返点</td><td class="val"><?=$yestoday['fanDianAmount']?></td></tr>
				<tr><td class="key">昨日活动奖励</td><td class="val"><?=$yestoday['brokerageAmount']?></td></tr>
				<tr><td class="key">昨日上下级转账</td><td class="val"><?=$yestoday['transfer']?></td></tr>
<?php
if($this->user['gongZi']>0)
{
?>
				<tr><td class="key">昨日日工资</td><td class="val"><?=$yestoday['gongzi']?></td></tr>
<?php 
}
?>
<?php
if($this->user['fenHong']>0)
{
?>
				<!--tr><td class="key">昨日分红累计</td><td class="val"><?=$yestoday['bonusAmount']?></td></tr-->
<?php 
}
?>
			</table>
		</div>
	</div>
	
	<div class="bet common" style="margin-top: 25px; float: left;width:50%;">
		<div class="head" style="border-top-right-radius: 0px;">
			<div class="name icon-sweden">个人今日充值</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr><td class="key">充值总额</td><td class="val"><?=$today['rechargeAmount']?></td></tr>
				<tr><td class="key">微信充值总额</td><td class="val"><?=$today['recharge_wc']?></td></tr>
				<tr><td class="key">支付宝充值总额</td><td class="val"><?=$today['recharge_ali']?></td></tr>
				<tr><td class="key">QQ钱包充值总额</td><td class="val"><?=$today['recharge_qq']?></td></tr>
				<tr><td class="key">快捷支付充值总额</td><td class="val"><?=$today['recharge_qp']?></td></tr>
				<tr><td class="key">网银充值总额</td><td class="val"><?=$today['recharge_bank']?></td></tr>
			</table>
		</div>
		<div class="head" style="border-radius: 0px;border: 0px;border-right: 1px solid #434857;border-left: 1px solid #434857;">
			<div class="name icon-sweden">个人今日提款</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" >
				<tr><td class="key">提款总额</td><td class="val"><?=$today['cashAmount']?></td></tr>
			</table>
		</div>
	</div>

	<div class="bet common" style="margin-top: 25px; float: left;width:50%;">
		<div class="head" style="border-top-right-radius: 0px;border-top-left-radius: 0px;border-left: 0px;">
			<div class="name icon-sweden">个人昨日充值</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px;">
				<tr><td class="key">充值总额</td><td class="val"><?=$yestoday['rechargeAmount']?></td></tr>
				<tr><td class="key">微信充值总额</td><td class="val"><?=$yestoday['recharge_wc']?></td></tr>
				<tr><td class="key">支付宝充值总额</td><td class="val"><?=$yestoday['recharge_ali']?></td></tr>
				<tr><td class="key">QQ钱包充值总额</td><td class="val"><?=$yestoday['recharge_qq']?></td></tr>
				<tr><td class="key">快捷支付充值总额</td><td class="val"><?=$yestoday['recharge_qp']?></td></tr>
				<tr><td class="key">网银充值总额</td><td class="val"><?=$yestoday['recharge_bank']?></td></tr>
			</table>
		</div>
		<div class="head" style="border-radius: 0px;border: 0px;border-right: 1px solid #434857;" >
			<div class="name icon-sweden">个人昨日提款</div>
		</div>
		<div class="body" >
			<table width="100%" border="0" cellspacing="0" cellpadding="0" style="border-left: 0px">
				<tr><td class="key">提款总额</td><td class="val"><?=$yestoday['cashAmount']?></td></tr>
			</table>
		</div>
	</div>

