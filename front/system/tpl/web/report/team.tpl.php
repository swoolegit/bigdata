
<div id="teamArea" style="margin-top: 30px;width: 100%;height:500px;"></div>
<div class="bet common" style="margin-top: 25px; width:100%;">
	<div class="head" >
		<div class="name icon-sweden">直属統計</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td class="key">总人数</td><td class="val"><?=$fmember['total']?></td><td class="key">新注册会员</td><td class="val"><?=$fmember['new_member']?></td></tr>
			<tr><td class="key">下级代理</td><td class="val"><?=$fmember['agent']?></td><td class="key">下级会员</td><td class="val"><?=$fmember['member']?></td></tr>
			<tr><td class="key">当前在线</td><td class="val"><?=$fmember['online_member']?></td><td class="key">团队余额</td><td class="val"><?=$this->formatNum($fmember['total_coin'])?></td></tr>
		</table>
	</div>
</div>

<div class="bet common" style="margin-top: 25px; width:100%;">
	<div class="head" >
		<div class="name icon-sweden">团队統計</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr><td class="key">总人数</td><td class="val"><?=$tmember['total']?></td><td class="key"> -- </td><td class="val"> -- </td></tr>
			<tr><td class="key">代理数量</td><td class="val"><?=$tmember['agent']?></td><td class="key">会员数量</td><td class="val"><?=$tmember['member']?></td></tr>
			<tr><td class="key">新注册代理</td><td class="val"><?=$tmember['new_agent']?></td><td class="key">新注册会员</td><td class="val"><?=$tmember['new_member']?></td></tr>
			<tr><td class="key">当前在线</td><td class="val"><?=$tmember['online_member']?></td><td class="key">团队余额</td><td class="val"><?=$this->formatNum($tmember['total_coin'])?></td></tr>
		</table>
	</div>
</div>

<?php

$max=25;

$agentList=array('北京'=>0,'天津'=>0,'上海'=>0,'重庆'=>0,'河北'=>0,'河南'=>0,'云南'=>0,'辽宁'=>0,'黑龙江'=>0,'湖南'=>0,'安徽'=>0,'山东'=>0,'新疆'=>0,'江苏'=>0,'浙江'=>0,'江西'=>0,'湖北'=>0,
'广西'=>0,'甘肃'=>0,'山西'=>0,'内蒙古'=>0,'陕西'=>0,'吉林'=>0,'福建'=>0,'贵州'=>0,'广东'=>0,'青海'=>0,'西藏'=>0,'四川'=>0,'宁夏'=>0,'海南'=>0,'台湾'=>0,'香港'=>0,'澳门'=>0,'南海诸岛'=>0);
$memberList=array('北京'=>0,'天津'=>0,'上海'=>0,'重庆'=>0,'河北'=>0,'河南'=>0,'云南'=>0,'辽宁'=>0,'黑龙江'=>0,'湖南'=>0,'安徽'=>0,'山东'=>0,'新疆'=>0,'江苏'=>0,'浙江'=>0,'江西'=>0,'湖北'=>0,
'广西'=>0,'甘肃'=>0,'山西'=>0,'内蒙古'=>0,'陕西'=>0,'吉林'=>0,'福建'=>0,'贵州'=>0,'广东'=>0,'青海'=>0,'西藏'=>0,'四川'=>0,'宁夏'=>0,'海南'=>0,'台湾'=>0,'香港'=>0,'澳门'=>0,'南海诸岛'=>0);
foreach($areaAgent as $k=>$v)
{
	$agentList[$v['regArea']]=$v['co'];
}
foreach($areaMember as $k=>$v)
{
	$memberList[$v['regArea']]=$v['co'];
}
?>
<script type="text/javascript">
jQuery.ajax({
      url: "http://www.echartsjs.com/gallery/vendors/echarts/map/js/china.js",
      dataType: "script",
      cache: true
}).done(function() {
	if($.cookie('colorfile')!='')
	{
		var chart = echarts.init(document.getElementById('teamArea'),'vintage');
	}
	else
	{
		var chart = echarts.init(document.getElementById('teamArea'),'macarons');
	}
	option = {
	    title: {
	        text: '注册人数分布图',
            textStyle:{
            	color:'#ddd',
            	fontWeight:'normal'
            },
            left: 'center',
      	},
		legend: {
	        orient: 'vertical',
	        left: '50',
	        data:['代理','会员'],
            textStyle:{
            	color:'#ddd'
            }
	    },
		tooltip: {},
	    visualMap: {
	        min: 0,
	        max: <?=$max?>,
	        left: '50',
	        top: 'bottom',
	        splitNumber: 5,
	        //orient:'horizontal',
            textStyle:{
            	color:'#ddd'
            }
	    },
        color:['#7CB5EC'],
        textStyle:{
        	color:'#ddd'
        },
	    series: [
	        {
	        	name: '代理',
	        	zoom:1.25,
	        	//top:0,
	            type: 'map',
	            mapType: 'china',
	            roam: false,
	            label: {
	                normal: {
	                    show: false,
	                    textStyle:{
	                    	color:'#21700D'
	                    }
	                },
	                emphasis: {
	                    show: true,
	                    textStyle:{
	                    	color:'#21700D'
	                    }
	                }
	            },
				itemStyle: {
		            normal:{
		                borderColor: 'rgba(0, 0, 0, 0.2)'
		            },
		            emphasis:{
		                areaColor: null,
		                shadowOffsetX: 0,
		                shadowOffsetY: 0,
		                shadowBlur: 10,
		                borderWidth: 0,
		                shadowColor: 'rgba(0, 0, 0, 0.5)'
		            }
		      	},
	            data:[
	            <?php
	            foreach($agentList as $k=>$v)
				{
					echo "{name: '".$k."',value: ".$v." },";
				}
	            ?>
	            ]
	        },
	        {
	        	name: '会员',
	        	zoom:1.25,
	        	//top:0,
	            type: 'map',
	            mapType: 'china',
	            roam: false,
	            label: {
	                normal: {
	                    show: false,
	                    textStyle:{
	                    	color:'#21700D'
	                    }
	                },
	                emphasis: {
	                    show: true,
	                    textStyle:{
	                    	color:'#21700D'
	                    }
	                }
	            },
				itemStyle: {
		            normal:{
		                borderColor: 'rgba(0, 0, 0, 0.2)'
		            },
		            emphasis:{
		                areaColor: null,
		                shadowOffsetX: 0,
		                shadowOffsetY: 0,
		                shadowBlur: 10,
		                borderWidth: 0,
		                shadowColor: 'rgba(0, 0, 0, 0.5)'
		            }
		      	},
	            data:[
	            <?php
	            foreach($memberList as $k=>$v)
				{
					echo "{name: '".$k."',value: ".$v." },";
				}
	            ?>
	            ]
	        }
	    ]
	};	
	// 使用刚指定的配置项和数据显示图表。  
	chart.setOption(option);	  
	window.addEventListener('resize', function () {
	chart.resize();
	});
});
</script>
    