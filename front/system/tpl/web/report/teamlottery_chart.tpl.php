<div id="coin-log" class="common" style="margin-top: 25px;">
	<div class="head" style="border:0px;">
		<form action="/report/teamlottery_chart" class="search" data-ispage="true" container="#report_view" target="ajax" func="form_submit">
			<div class="timer">
				<input type="text" autocomplete="off" name="fromTime" value="<?php echo date('Y-m-d', $this->request_time_from);?>" id="datetimepicker_fromTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<div class="sep icon-exchange"></div>
			<div class="timer">
				<input type="text" autocomplete="off" name="toTime" value="<?php echo date('Y-m-d', $this->request_time_to);?>" id="datetimepicker_toTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<button type="submit" class="btn btn-brown icon-search">查询</button>
		</form>
	</div>
</div>

<div id="lotteryChart" style="margin-top: 20px;width:100%; height:500px;"></div>

<div class="bet common" id="teamreport" style="margin-top: 25px; width:100%;">
	<div class="head" >
		<div class="name icon-sweden">彩种統計 </div>
	</div>
	<div class="body">
		<ul class="lottery_ul">
<?php
$types = $this->get_types();
$lotteryArray=array();
$amountArray=array();
$actionArray=array();
foreach ($types as $name => $list) {
	foreach ($list as $v) {
		$setColor="";
		if(!isset($lottery[$v['id']]))
		{
			$lottery[$v['id']]['betAmount']=0;
			$lottery[$v['id']]['actionNum']=0;
			$lottery[$v['id']]['norder']=0;
			$lottery[$v['id']]['nuid']=0;
		}
		if($lottery[$v['id']]['betAmount'] > 0)
		{
			$lotteryArray[]=$v['title'];
			$amountArray[]=$lottery[$v['id']]['betAmount'];
			$actionArray[]=$lottery[$v['id']]['actionNum'];			
			$setColor="color: #FFAA0D;";
		}
	?>
			<li class="lottery_li">
				<div class="lottery_div big">
					<span class="icon-award" style="font-size: 14px ; line-height: 30px;<?=$setColor?>"></span><span><?php echo $v['title'];?></span> 
					<div>总投注
						<?php
							$css="";
							if($lottery[$v['id']]['betAmount']>0)
							{
								$css="#B05E54";
							}else
							{
								$css="#555";
							}
						?> 
						<span style="color: <?=$css?>">￥<?=$this->formatNum($lottery[$v['id']]['betAmount'])?> / <?=$lottery[$v['id']]['actionNum']?>注 / <?=$lottery[$v['id']]['norder']?>单/ <?=$lottery[$v['id']]['nuid']?>人</span>
					</div>
				</div>
			</li>	
	<?php
	}	
}
?>
		</ul>
	</div>
</div>
	
<script>
function runchart()
{
		if($.cookie('colorfile')!='')
		{
		    var lotteryChart = echarts.init(document.getElementById('lotteryChart'),'vintage');
		
		}else
		{
			var lotteryChart = echarts.init(document.getElementById('lotteryChart'),'macarons');
		}
	    // 指定图表的配置项和数据
	    var option = {
	        title: {
	            text: '彩种投注分布图 ',
	            textStyle:{
	            	color:'#ddd',
	            	fontWeight:'normal'
	            },
	            left: 'center'
	        },
	        color:['#7CB5EC'],
	        textStyle:{
	        	color:'#ddd'
	        },
		    legend: {
	            textStyle:{
	            	color:'#ddd'
	            },	        
		        data:['投注额', '投注数'],
		        orient: 'vertical',
		        left: 'left'
		    },
	        tooltip: {
		        trigger: 'axis',
		        axisPointer: {
		            type: 'cross',
		            label: {
		                backgroundColor: '#283b56'
		            }
		        }	        	
	        },
	        xAxis: [
		        {
		        	type: 'category',
		            data: ['<?=implode("','",$lotteryArray)?>']
		        }		        
	        ],
	        yAxis:[ 
		        {
			        type: 'value',
			        name: '投注额'
		        },
		        {
			        type: 'value',
			        name: '投注数'
		        }
	        ],
	        series: [
		        {
		            name: '投注额',
		            type: 'bar',
		            data: [<?=implode(",",$amountArray)?>]
		        },
		        {
		            name: '投注数',
		            type: 'line',
		            yAxisIndex:1,
		            data: [<?=implode(",",$actionArray)?>]
		        }
			]
	    };

        // 使用刚指定的配置项和数据显示图表。
        lotteryChart.setOption(option);
		window.addEventListener('resize', function () {
		lotteryChart.resize();
		});
		setTimeout(function() {
			location.hash="#teamorder";
		}, 300);
};
setTimeout(function() {
	runchart();
}, 300);
$(function() {
	// 时间选择插件
	$('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
});
</script>