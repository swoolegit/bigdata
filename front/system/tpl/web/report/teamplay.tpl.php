<div id="coin-log" class="common" style="margin-top: 15px;">
	<div class="head" style="border:0px;">
		<form action="/report/teamplay" class="search" data-ispage="true" container="#report_view" target="ajax" func="form_submit">
			<div class="timer">
				<input type="text" autocomplete="off" name="fromTime" value="<?php echo date('Y-m-d', $this->request_time_from);?>" id="datetimepicker_fromTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<div class="sep icon-exchange"></div>
			<div class="timer">
				<input type="text" autocomplete="off" name="toTime" value="<?php echo date('Y-m-d', $this->request_time_to);?>" id="datetimepicker_toTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<button type="submit" class="btn btn-brown icon-search">查询</button>
		</form>
	</div>
</div>

<div class="bet common" style="margin-top: 15px; width:100%;">
	<div id="lotteryChart1" style="margin-top: 20px;width:100%; height:500px;float: left;"></div>
	<div id="lotteryChart2" style="margin-top: 20px;width:100%; height:500px;float: left;"></div>
	<div id="lotteryChart3" style="margin-top: 20px;width:100%; height:500px;float: left;"></div>
	<div class="head" style="border-top-right-radius: 0px;clear: both;">
		<div class="name icon-sweden">玩法统计 
			<span class="report_team_order" >
				隐藏 : <input type="checkbox" id="hplay" style="cursor: pointer">无投注
<?php
foreach($this->lottery_types as $k=>$v)
{
	echo ' , <input type="checkbox" id="htype'.$k.'" style="cursor: pointer">'.$v; 
}
?>			
			</span>
		</div>
	</div>
	<div class="body">
		<ul class="lottery_ul">
<?php	
$plays = $this->get_plays();
$LPie1=array();
$LPieData1=array();
$LPie2=array();
$LPieData2=array();
$LPie3=array();
$LPieData3=array();
foreach ($plays as $k => $v) {
		if(!isset($teamplay[$v['id']]))
		{
			$teamplay[$v['id']]['betAmount']=0;
			$teamplay[$v['id']]['actionNum']=0;
			$teamplay[$v['id']]['norder']=0;
			$teamplay[$v['id']]['nuid']=0;
		}
		$hideplay="";
		$hidetype="hidetype".$v['type'];
		$setColor="color: #FFAA0D;";
		if($teamplay[$v['id']]['betAmount']==0 )
		{
			$setColor="";
			$hideplay="hideplay";
		}else
		{
			$LPieData1[]="{value:".$teamplay[$v['id']]['betAmount'].", name:'".$v['name']."'}";
			$LPie1[]=$v['name'];
			$LPieData2[]="{value:".$teamplay[$v['id']]['bonus'].", name:'".$v['name']."'}";
			$LPie2[]=$v['name'];
			$LPieData3[]="{value:".$teamplay[$v['id']]['actionNum'].", name:'".$v['name']."'}";
			$LPie3[]=$v['name'];			
		}			
	?>
			<li class="lottery_li <?=$hideplay?> <?=$hidetype?>"> 
				<div class="lottery_div big">
					<span class="icon-award" style="font-size: 14px ; line-height: 30px;<?=$setColor?>"></span><span><?php echo $v['name'];?></span> 
					<div>总投注
						<?php
							$css="";
							if($teamplay[$v['id']]['betAmount']>0)
							{
								$css="#B05E54";
							}else
							{
								$css="#555";
							}
						?> 
						<span style="color: <?=$css?>">￥<?=$this->formatNum($teamplay[$v['id']]['betAmount'])?> / <?=$teamplay[$v['id']]['actionNum']?>注 / <?=$teamplay[$v['id']]['norder']?>单/ <?=$teamplay[$v['id']]['nuid']?>人</span>
					</div>
				</div>
			</li>	
	<?php	
}
?>
		</ul>
	</div>
<script>
function runchart()
{
	if($.cookie('colorfile')!='')
	{
		var lotteryChart1 = echarts.init(document.getElementById('lotteryChart1'),'vintage');
		var lotteryChart2 = echarts.init(document.getElementById('lotteryChart2'),'vintage');
		var lotteryChart3 = echarts.init(document.getElementById('lotteryChart3'),'vintage');

	}else
	{
		var lotteryChart1 = echarts.init(document.getElementById('lotteryChart1'),'macarons');
		var lotteryChart2 = echarts.init(document.getElementById('lotteryChart2'),'macarons');
		var lotteryChart3 = echarts.init(document.getElementById('lotteryChart3'),'macarons');

	}

	option = {
	    title : {
	        text: '总投注金额比例',
	        x:'center'
	    },
	    tooltip : {
	        trigger: 'item',
	        formatter: "{a} <br/>{b} : {c} ({d}%)"
	    },
	    legend: {
	        orient: 'vertical',
	        left: 'left',
	        data: ['<?=implode("','",$LPie1)?>'],
            textStyle:{
            	color:'#ddd'
            }	        
	    },
	    series : [
	        {
	            name: '总投注金额比例',
	            type: 'pie',
	            radius : '55%',
	            center: ['50%', '60%'],
	            data:[
	                <?=implode(",",$LPieData1)?>
	            ],
	            itemStyle: {
	                emphasis: {
	                    shadowBlur: 10,
	                    shadowOffsetX: 0,
	                    shadowColor: 'rgba(0, 0, 0, 0.5)'
	                }
	            }
	        }
	    ]
	};
	lotteryChart1.setOption(option);
    option.title.text="总中奖金额比例";
    option.series[0].name="总中奖金额比例";
    option.legend.data=['<?=implode("','",$LPie2)?>'];
    option.series[0].data=[<?=implode(",",$LPieData2)?>];
    lotteryChart2.setOption(option);
    option.title.text="总注数量比例";
    option.series[0].name="总注数量比例";
    option.legend.data=['<?=implode("','",$LPie3)?>'];
    option.series[0].data=[<?=implode(",",$LPieData3)?>];
    lotteryChart3.setOption(option);
	window.addEventListener('resize', function () {
	lotteryChart1.resize();
	lotteryChart2.resize();
	lotteryChart3.resize();
	});
}
setTimeout(function() {
	runchart();
}, 300);
$(function () {
	$('#hplay').click(function (){
		if($('#hplay').is(':checked'))
		{
			$('.hideplay').hide();
		}else
		{
			$('.hideplay').show();
<?php
foreach($this->lottery_types as $k=>$v)
{
?>
			if($('#htype<?=$k?>').is(':checked'))
			{
				$('.hidetype<?=$k?>').hide();
			}			
<?php 
}
?>			
		}
	});	
<?php
foreach($this->lottery_types as $k=>$v)
{
?>
	$('#htype<?=$k?>').click(function (){
		if($('#htype<?=$k?>').is(':checked'))
		{
			$('.hidetype<?=$k?>').hide();
		}else
		{
			$('.hidetype<?=$k?>').show();
			if($('#hplay').is(':checked'))
			{
				$('.hideplay').hide();
			}			
		}
	});
<?php 
}
?>
	// 时间选择插件
	$('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
});	
</script>
