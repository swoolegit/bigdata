<div id="coin-log" class="common" style="margin-top: 25px;">
	<div class="head" style="border:0px;">
		<form action="/report/teamrecharge" class="search" data-ispage="true" container="#report_view" target="ajax" func="form_submit">
			<div class="timer">
				<input type="text" autocomplete="off" name="fromTime" value="<?php echo date('Y-m-d', $this->request_time_from);?>" id="datetimepicker_fromTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<div class="sep icon-exchange"></div>
			<div class="timer">
				<input type="text" autocomplete="off" name="toTime" value="<?php echo date('Y-m-d', $this->request_time_to);?>" id="datetimepicker_toTime" class="timer">
				<span class="icon icon-calendar"></span>
			</div>
			<button type="submit" class="btn btn-brown icon-search">查询</button>
		</form>
	</div>
</div>
<style>
	.key_left{
		text-align: left;
	}
</style>
<div class="bet common" style="margin-top: 25px; width:100%;float: left">
	<div class="head" style="border-top-right-radius: 0px;">
		<div class="name icon-sweden">充值</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td class="key key_left" style="text-align: left;">充值总额</td><td class="val"><?=$this->formatNum($tcharge['amount'])?></td>
				<td class="key key_left" style="text-align: left;">充值申请笔数</td><td class="val"><?=$tcharge['co']?></td>
				<td class="key key_left" style="text-align: left;">充值成功笔数</td><td class="val"><?=$tcharge['ok']?></td>
			</tr>
			<tr>
				<td class="key key_left" style="text-align: left;">微信充值金额</td><td class="val"><?=$this->formatNum($wc['amount'])?></td>
				<td class="key key_left" style="text-align: left;">微信充值申请笔数</td><td class="val"><?=$wc['co']?></td>
				<td class="key key_left" style="text-align: left;">微信充值成功笔数</td><td class="val"><?=$wc['ok']?></td>
			</tr>
			<tr>
				<td class="key key_left" style="text-align: left;">支付宝充值总额</td><td class="val"><?=$this->formatNum($ali['amount'])?></td>
				<td class="key key_left" style="text-align: left;">支付宝充值申请笔数</td><td class="val"><?=$ali['co']?></td>
				<td class="key key_left" style="text-align: left;">支付宝充值成功笔数</td><td class="val"><?=$ali['ok']?></td>
			</tr>
			<tr>
				<td class="key key_left" style="text-align: left;">QQ钱包充值总额</td><td class="val"><?=$this->formatNum($qq['amount'])?></td>
				<td class="key key_left" style="text-align: left;">QQ钱包充值申请笔数</td><td class="val"><?=$qq['co']?></td>
				<td class="key key_left" style="text-align: left;">QQ钱包充值成功笔数</td><td class="val"><?=$qq['ok']?></td>
			</tr>
			<tr>
				<td class="key key_left" style="text-align: left;">快捷支付充值总额</td><td class="val"><?=$this->formatNum($qp['amount'])?></td>
				<td class="key key_left" style="text-align: left;">快捷支付充值申请笔数</td><td class="val"><?=$qp['co']?></td>
				<td class="key key_left" style="text-align: left;">快捷支付充值成功笔数</td><td class="val"><?=$qp['ok']?></td>
			</tr>
			<tr>
				<td class="key key_left" style="text-align: left;">京东充值总额</td><td class="val"><?=$this->formatNum($jd['amount'])?></td>
				<td class="key key_left" style="text-align: left;">京东充值申请笔数</td><td class="val"><?=$jd['co']?></td>
				<td class="key key_left" style="text-align: left;">京东充值成功笔数</td><td class="val"><?=$jd['ok']?></td>
			</tr>
			<tr>
				<td class="key key_left" style="text-align: left;">网银充值总额</td><td class="val"><?=$this->formatNum($bank['amount'])?></td>
				<td class="key key_left" style="text-align: left;">网银充值申请笔数</td><td class="val"><?=$bank['co']?></td>
				<td class="key key_left" style="text-align: left;">网银充值成功笔数</td><td class="val"><?=$bank['ok']?></td>
			</tr>
		</table>
	</div>
</div>
<?php
global $globalBankMapping;
$banks=$globalBankMapping;
$RPie=array();
$RPieAmount=array();
$RPieTime=array();
if($wc['amount']>0)
{
	$RPie[]="微信";
	$RPieAmount[]="{value:".$wc['amount'].", name:'微信'}";
	$RPieTime[]="{value:".$wc['ok'].", name:'微信'}";
}
if($ali['amount']>0)
{
	$RPie[]="支付宝";
	$RPieAmount[]="{value:".$ali['amount'].", name:'支付宝'}";
	$RPieTime[]="{value:".$ali['ok'].", name:'支付宝'}";
}
if($qq['amount']>0)
{
	$RPie[]="QQ钱包";
	$RPieAmount[]="{value:".$qq['amount'].", name:'QQ钱包'}";
	$RPieTime[]="{value:".$qq['ok'].", name:'QQ钱包'}";
}
if($qp['amount']>0)
{
	$RPie[]="快捷支付";
	$RPieAmount[]="{value:".$ali['amount'].", name:'快捷支付'}";
	$RPieTime[]="{value:".$ali['ok'].", name:'快捷支付'}";
}
if($jd['amount']>0)
{
	$RPie[]="京东";
	$RPieAmount[]="{value:".$ali['amount'].", name:'京东'}";
	$RPieTime[]="{value:".$ali['ok'].", name:'京东'}";
}
?>

<div id="lotteryChart1" style="margin-top: 40px;width:50%; height:280px;float: left"></div>
<div id="lotteryChart2" style="margin-top: 40px;width:50%; height:280px;float: left"></div>
<div class="bet common" style="margin-top: 25px; width:100%;float: left">
	<div class="head" style="border-top-right-radius: 0px;">
		<div class="name icon-sweden">银行充值区分</div>
	</div>
	<div class="body" >
		<table width="100%" border="0" cellspacing="0" cellpadding="0">
		<?php
		foreach($banks as $k=>$v)
		{
			if($bank['data'][$v]['amount']>0)
			{
				$RPie[]=$k;
				$RPieAmount[]="{value:".$bank['data'][$v]['amount'].", name:'".$k."'}";
				$RPieTime[]="{value:".$bank['data'][$v]['ok'].", name:'".$k."'}";
			}
		?>
			<tr>
				<td class="key key_left" style="width:150px;text-align: left;"><?=$k?> <span class="icon-right-open-big"></span> </td>
				<td class="key key_left" style="text-align: left;">充值总额</td><td class="val"><?=$this->formatNum($bank['data'][$v]['amount'])?></td>
				<td class="key key_left" style="text-align: left;">充值申请笔数</td><td class="val"><?=$bank['data'][$v]['co']?></td>
				<td class="key key_left" style="text-align: left;">充值成功笔数</td><td class="val"><?=$bank['data'][$v]['ok']?></td>
			</tr>		
		<?php
		}
		?>
		</table>
	</div>
</div>
<script type="text/javascript">
function runchart()
{
	if($.cookie('colorfile')!='')
	{
        var lotteryChart1 = echarts.init(document.getElementById('lotteryChart1'),'vintage');
        var lotteryChart2 = echarts.init(document.getElementById('lotteryChart2'),'vintage');

	}else
	{
        var lotteryChart1 = echarts.init(document.getElementById('lotteryChart1'),'macarons');
        var lotteryChart2 = echarts.init(document.getElementById('lotteryChart2'),'macarons');
	}
	
		var option = {
		    title : {
		        text: '总充值金额比例',
		        textStyle:{
	            	color:'#ddd',
	            	fontWeight:'normal'
	            },
		        x:'center'
		    },
		    tooltip : {
		        trigger: 'item',
		        formatter: "{a} <br/>{b} : {c} ({d}%)"
		    },
		    legend: {
		        orient: 'vertical',
		        left: 'left',
		        data: ['<?=implode("','",$RPie)?>'],
	            textStyle:{
	            	color:'#ddd'
	            }	        
		    },
		    series : [
		        {
		            name: '总充值金额比例',
		            type: 'pie',
		            radius : '55%',
		            center: ['50%', '60%'],
		            data:[<?=implode(",",$RPieAmount)?>],
		            itemStyle: {
		                emphasis: {
		                    shadowBlur: 10,
		                    shadowOffsetX: 0,
		                    shadowColor: 'rgba(0, 0, 0, 0.5)'
		                }
		            }
		        }
		    ]
		};
		lotteryChart1.setOption(option);
	    option.title.text="充值成功比例";
	    option.series[0].name="充值成功比例";	
	    option.series[0].data=[<?=implode(",",$RPieTime)?>];	
		lotteryChart2.setOption(option);
		window.addEventListener('resize', function () {
			lotteryChart1.resize();
			lotteryChart2.resize();
		});
};

setTimeout(function() {
	runchart();
}, 300);

$(function() {
	// 时间选择插件
	$('#datetimepicker_fromTime,#datetimepicker_toTime').datetimepicker(datetimepicker_opt);
});
</script>
