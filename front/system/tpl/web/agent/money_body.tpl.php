<?php
	if ($total) {
	$AES = core::lib('aes');
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr class="title">
		<td>用户名</td>
        <td>团队充值</td>
        <td>团队提现</td>
        <td>团队投注</td>
        <td>团队中奖</td>
        <td>团队返点</td>
		<td>团队工资</td>
        <td>团队活动</td>
		<td>团队盈亏</td>
		<td>查看</td>
	</tr>
	<?php
		$count = array(
			'bet' => 0,
			'income' => 0,
			'expenditure' => 0,
			'total' => 0,
			'zj' => 0,
			'fandian' => 0,
			'bonus' => 0,
			'gongzi' => 0,
			'broker' => 0,
			'recharge' => 0,
			'cash' => 0,
			'transfer' => 0,
		);
		foreach ($data as $v) {
			$v['bet']=$v['real_bet'];
			$v['total'] = $v['teamwin'];
			$count['bet'] += $v['bet'];
			$count['zj'] += $v['zj'];
			$count['fandian'] += $v['fandian'];
			$count['bonus'] += $v['bonus'];
			$count['gongzi'] += $v['gongzi'];
			$count['broker'] += $v['broker'];
			$count['recharge'] += $v['recharge'];
			$count['cash'] += $v['cash'];
			$count['total'] += $v['total'];
	?>

	<?php
	if($this->user['uid'] == $v['uid'])
	{
	?>
		<tr class="agent_list_top">
		<td><?php echo $v['username'];?> (自己)</td>
	<?php
	}else
	{
	?>
		<tr>
		<td><?php echo $v['username'];?></td>
	<?php
	}
	?>
        <td><?php echo $v['recharge'] ? number_format($v['recharge'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $v['cash'] ? number_format($v['cash'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $v['bet'] ? number_format($v['bet'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $v['zj'] ? number_format($v['zj'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $v['fandian'] ? number_format($v['fandian'], 3, '.', '') : '0.000';?></td>
		<td><?php echo $v['gongzi'] ? number_format($v['gongzi'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $v['broker'] ? number_format($v['broker'], 3, '.', '') : '0.000';?></td>
		<td><?php echo $v['total'] ? number_format($v['total'], 3, '.', '') : '0.000';?></td>
		<td>
			<a href="/agent/money?parentId=<?php echo $AES->urlsafe_b64encode($AES->encrypt($v['uid']));?>&fromTime=<?php echo date('Y-m-d', $this->request_time_from);?>&toTime=<?php echo date('Y-m-d', $this->request_time_to);?>" container="#agent-money-dom .body" data-ispage="true" target="ajax" func="loadpage" class="icon-download">下级</a>
			<?php if($v['uid'] != $this->user['uid'] && $v['parentId']) {?>
			  <a href="/agent/money?uid=<?php echo $AES->urlsafe_b64encode($AES->encrypt($v['uid']));?>&fromTime=<?php echo date('Y-m-d', $this->request_time_from);?>&toTime=<?php echo date('Y-m-d', $this->request_time_to);?>" container="#agent-money-dom .body" data-ispage="true" target="ajax" func="loadpage" class="icon-upload">上级</a>
			<?php }?>
		</td>
	</tr>
	<?php }?>
	<tr class="agent_list_down" >
		<td>本页总结</td>
        <td><?php echo $count['recharge'] ? number_format($count['recharge'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $count['cash'] ? number_format($count['cash'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $count['bet'] ? number_format($count['bet'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $count['zj'] ? number_format($count['zj'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $count['fandian'] ? number_format($count['fandian'], 3, '.', '') : '0.000';?></td>
		<td><?php echo $count['gongzi'] ? number_format($count['gongzi'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $count['broker'] ? number_format($count['broker'], 3, '.', '') : '0.000';?></td>
		<td><?php echo $count['total'] ? number_format($count['total'], 3, '.', '') : '0.000';?></td>
		<td>--</td>
	</tr>
	<tr class="agent_list_down" >
		<td><?php
			switch ($args['a_type']) {
				case 0: echo '团队总结'; break;
				case 1: echo '直属下级'; break;
				case 2: echo '所有下级'; break;
				default: echo '--';
			}
		?></td>
        <td><?php echo $all['recharge'] ? number_format($all['recharge'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $all['cash'] ? number_format($all['cash'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $all['bet'] ? number_format($all['bet'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $all['zj'] ? number_format($all['zj'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $all['fandian'] ? number_format($all['fandian'], 3, '.', '') : '0.000';?></td>
		<td><?php echo $all['gongzi'] ? number_format($all['gongzi'], 3, '.', '') : '0.000';?></td>
        <td><?php echo $all['broker'] ? number_format($all['broker'], 3, '.', '') : '0.000';?></td>
		<td><?php echo $all['total'] ? number_format($all['total'], 3, '.', '') : '0.000';?></td>
		<td>--</td>
	</tr>
</table>
<?php } else {?>
<div class="empty"></div>
<?php }?>
<div class="agent_list_rule" >
	盈亏公式 : 投注 - (中奖+返点+工资+活动)
</div>

<?php require(TPL.'/page.tpl.php');?>