<?php

class mod_third extends mod
{
    public function __construct()
    {
        $this->user_check = false;
        parent::__construct();
    }
    public function usdt()
    {
        $html=$_GET['html'];
        if(empty($html))
        {
            echo "錯誤參數";
        }
        header('content-Type: text/html;charset=utf-8');
        $file_type= $this->getCacheDir().$html;
        if(is_file($file_type))
        {
            echo file_get_contents($file_type);
        }
        return;
    }
}
?>