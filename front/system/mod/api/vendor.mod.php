<?php

class mod_vendor extends mod
{
    public $_status =[
        0=>"success",
        1=>"pending",
        2=>"failed",
        3=>"refund",
    ];
    public function __construct()
    {
        $this->user_check = false;
        parent::__construct();
    }

    public function balance()
    {
        global $conf;
        $channelId = array_key_exists('channelId', $_POST) ? trim($_POST['channelId']) : '';
        $accountId = array_key_exists('accountId', $_POST) ? trim($_POST['accountId']) : '';
        if(empty($channelId) || empty($accountId))
        {
            //core::json_err('账户名不能为空');
            return;
        }
        $sql = "SELECT `uid`,`coin` FROM `{$this->db_prefix}members` WHERE `username`='".$accountId."' and enable=1  LIMIT 1";
        $user = $this->db->query($sql, 2);
        $data=[
            'channelId'=> $channelId,
            'accountId'=> $accountId,
            'balance'=> $this->floor_dec($user["coin"],2),
            'currency'=> $conf["vendor"]["CG"]["currency"],
            'errorCode'=> 0,
            'returnTime'=> date("c",time()),
        ];
        core::json($data);
    }
    public function record()
    {
        global $conf;
        $channelId = array_key_exists('channelId', $_POST) ? trim($_POST['channelId']) : '';
        $mtcode = array_key_exists('mtcode', $_POST) ? trim($_POST['mtcode']) : '';
        if(empty($channelId) || empty($mtcode))
        {
            return;
        }
        $sql = "SELECT * FROM `{$this->db_prefix}egame_bet` WHERE `channelId`='".$channelId."' and `mtcode`='".$mtcode."' LIMIT 1";
        $egame_bet = $this->db->query($sql, 2);
		if($egame_bet)
		{
			$data=[
				"transaction_id"=>$egame_bet['id'],
				"action"=>$egame_bet['action'],
				"target"=>[
					"account"=>$egame_bet['accountId']
				],
				"balance"=>[
					"before"=>$egame_bet['before'],
					"after"=>$egame_bet['after'],
				],
				"status"=>[
					"createtime"=>$egame_bet['actionTime'],
					"endtime"=>$egame_bet['endTime'],
					"status"=>$this->_status[$egame_bet['status']],
					"message"=>"success",
				],
				"currency"=>$conf["vendor"]["CG"]["currency"],
				"incident"=>[
					"mtcode"=>$egame_bet['mtcode'],
					"amount"=>$egame_bet['amount'],
					"eventtime"=>$egame_bet['eventTime'],
				],
				"channelId"=>$egame_bet['channelId'],
				"errorCode"=>0,
				"returnTime"=>date("c"),
			];
		}else
		{
			$data=[
				"data"=>
				[
					"transaction_id"=>"null",
					"action"=>"",
					"target"=>"",
					"balance"=>"",
					"status"=>"",
					"currency"=>$conf["vendor"]["CG"]["currency"],
					"incident"=>"",
				],
				"channelId"=>$channelId,
				"errorCode"=>103,
				"returnTime"=>date("c"),
			];
		}
        core::json($data);
    }
    public function game()
    {
        global $conf;
        $uri = array_key_exists('REQUEST_URI', $_SERVER) ? $_SERVER['REQUEST_URI'] : '/';
        $uri_info = parse_url($uri);
        $path = explode('/',$uri_info['path']);
        $data=[
            'channelId'=> $_POST['channelId'],
            'accountId'=> $_POST['accountId'],
            'currency'=> $conf["vendor"]["CG"]["currency"],
            'errorCode'=> 0,
            'returnTime'=> date("c",time()),
        ];
        $eventTime=date("Y-m-d H:i:s",strtotime($_POST['eventTime']));
		$para = array(
			'channelId' => $_POST['channelId'],
			'accountId' => $_POST['accountId'],
			'gameType' => $_POST['gameType'],
			'roundId' => $_POST['roundId'],
			'amount' => $_POST['amount'],
			'currency' => $_POST['currency'],
			'mtcode' => $_POST['mtcode'],
			'eventTime' => $eventTime,
			'actionTime' => time(),
		);
        switch(true)
        {
            case $path[4] == "bet":
                // $liqType = 101;
                // $info = '电子投注';
                $this->db->transaction('begin');
                try {
                    $sql = "SELECT `uid`,`coin` FROM `{$this->db_prefix}members` WHERE `username`='".$_POST['accountId']."' and enable=1 LIMIT 1";
                    $user = $this->db->query($sql, 2);
                    $para['uid'] = $user['uid'];
                    $para['before'] = $user['coin'];
                    $para['after'] = $user['coin'] - floatval($_POST['amount']);
                    $para['status'] = 1;
                    $para['action'] = "bet";
                    $this->db->insert($this->db_prefix . 'egame_bet', $para);
                    $data['balance'] = $para['after'];
                    $this->db->transaction('commit');
                } catch (Exception $e) {
                    $this->db->transaction('rollBack');
                    core::json_err($e->getMessage());
                }
            break;
            case $path[4] == "endround":
                // $liqType = 6;
                // $info = '电子投注-結算';
                $this->db->transaction('begin');
                try {
                    $sql = "SELECT `uid`,`coin` FROM `{$this->db_prefix}members` WHERE `username`='".$_POST['accountId']."' and enable=1 LIMIT 1";
                    $user = $this->db->query($sql, 2);
                    $para['uid'] = $user['uid'];
                    $para['before'] = $user['coin'];
                    $para['after'] = $user['coin'] + floatval($_POST['amount']);
                    $para['status'] = 0;
                    $para['action'] = "endround";
                    $para['endTime'] = date("Y-m-d H:i:s");
                    $this->db->insert($this->db_prefix . 'egame_bet', $para);
                    // $sql="update `{$this->db_prefix}egame_bet` set winlost=".$_POST['amount'].",endTime=".$eventTime." where mtcode='".$_POST['mtcode']."' limit 1";
                    // $this->db->query($sql, 0);
                    $data['balance'] = $para['after'];
                    $this->db->transaction('commit');
                } catch (Exception $e) {
                    $this->db->transaction('rollBack');
                    core::json_err($e->getMessage());
                }
            break;
            case $path[4] == "refund":
                $this->db->transaction('begin');
                try {
                    $sql = "SELECT `uid`,`coin` FROM `{$this->db_prefix}members` WHERE `username`='".$_POST['accountId']."' and enable=1 LIMIT 1";
                    $user = $this->db->query($sql, 2);
                    $para['uid'] = $user['uid'];
                    $para['before'] = $user['coin'];
                    $para['after'] = $user['coin'] + floatval($_POST['amount']);
                    $para['status'] = 0;
                    $para['action'] = "refund";
                    $para['endTime'] = date("Y-m-d H:i:s");
                    $this->db->insert($this->db_prefix . 'egame_bet', $para);
                    // $sql="update `{$this->db_prefix}egame_bet` set winlost=".$_POST['amount'].",endTime=".$eventTime." where mtcode='".$_POST['mtcode']."' limit 1";
                    // $this->db->query($sql, 0);
                    $data['balance'] = $para['after'];
                    $this->db->transaction('commit');
                } catch (Exception $e) {
                    $this->db->transaction('rollBack');
                    core::json_err($e->getMessage());
                }
            break;
        }
        core::json($data);
    }
    
}

?>