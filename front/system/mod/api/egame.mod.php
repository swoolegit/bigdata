<?php

class mod_egame extends mod
{
    public function __construct()
    {
        parent::__construct();
    }
    public function check_egame($id,$vendor)
    {
        global $conf;
        if(empty($id))
        {
            core::json_err('请选择游戏');
        }
		if($conf["vendor"][$vendor]["enable"]=="0")
		{
			core::json_err('游戏尚未开放');
		}
    }
    public function start()
    {
        global $conf;
        $id=array_key_exists('id', $_POST) ? $_POST['id'] : '';
        $vendor=array_key_exists('vendor', $_POST) ? $_POST['vendor'] : '';
        // if(empty($id))
        // {
        //     core::json_err('请选择游戏');
        // }
		// if($conf["vendor"][$vendor]["enable"]=="0")
		// {
		// 	core::json_err('游戏尚未开放');
        // }
        $this->check_egame($id,$vendor);
		$vendor_file = SYSTEM . '/lib/vendor/' .$vendor . '.php';
		if (!is_file($vendor_file)) throw new Exception('错误厂商');
		require($vendor_file);
		$API = new $vendor;
        $result = $API->StartGame($id,$this->user['username']);
        //$url=core::lib('vendor')->StartGame($vendor,$id,$this->user['username']);
        core::json([
            "status" =>1,
            "data" =>$result,
        ]);
        return;
    }
    public function coin()
    {
        global $conf;
        $id=array_key_exists('id', $_POST) ? $_POST['id'] : '';
        $vendor=array_key_exists('vendor', $_POST) ? $_POST['vendor'] : '';
        $this->check_egame($id,$vendor);
        $vendor_file = SYSTEM . '/lib/vendor/' .$vendor . '.php';
		if (!is_file($vendor_file)) throw new Exception('错误厂商');
		require($vendor_file);
		$API = new $vendor;
        $result = $API->getBalance($id,$this->user['username']);
        //$amount=core::lib('vendor')->getBalance($vendor,$id,$this->user['username']);
        core::json([
            "status" =>1,
            "data" =>$result,
        ]);
        return;
    }
    public function transferIn()
    {
        global $conf;
        $id=array_key_exists('id', $_POST) ? $_POST['id'] : '';
        $vendor=array_key_exists('vendor', $_POST) ? $_POST['vendor'] : '';
        $amount=array_key_exists('amount', $_POST) ? $_POST['amount'] : '';
        $this->check_egame($id,$vendor);
        $amount = intval($amount);
        if(empty($amount))
        {
            core::json_err('请输入金额');
        }
        if($amount < 0)
        {
            core::json_err('请输入金额');
        }
        $this->fresh_user_session();
		if ($amount > $this->user['coin']) core::json_err('可用余额不足');
        $vendor_file = SYSTEM . '/lib/vendor/' .$vendor . '.php';
		if (!is_file($vendor_file)) throw new Exception('错误厂商');
		require($vendor_file);
		$API = new $vendor;
        $result = $API->transferIn($id,$this->user['username'],$amount);
        $this->db->transaction('begin');
        try {
            $this->set_coin(array(
                'coin' => 0 - $amount,
                'fcoin' => 0,  
                'uid' => $this->user['uid'],
                'liqType' => 90,
                'info' => "轉入BLG [".$vendor." - ".$id."]",
                'extfield1' => $result
            ));
            $this->db->transaction('commit');
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            core::json_err($e->getMessage());
        }
        core::json([
            "status" =>1
        ]);
        return;
    }
    public function transferOut()
    {
        global $conf;
        $id=array_key_exists('id', $_POST) ? $_POST['id'] : '';
        $vendor=array_key_exists('vendor', $_POST) ? $_POST['vendor'] : '';
        $amount=array_key_exists('amount', $_POST) ? $_POST['amount'] : '';
        $this->check_egame($id,$vendor);
        $amount = intval($amount);
        if(empty($amount))
        {
            core::json_err('请输入金额');
        }
        if($amount < 0)
        {
            core::json_err('请输入金额');
        }
        $vendor_file = SYSTEM . '/lib/vendor/' .$vendor . '.php';
		if (!is_file($vendor_file)) throw new Exception('错误厂商');
		require($vendor_file);
		$API = new $vendor;
        $result = $API->transferOut($id,$this->user['username'],$amount);
        $this->db->transaction('begin');
        try {
            $this->set_coin(array(
                'coin' => $amount,
                'fcoin' => 0,  
                'uid' => $this->user['uid'],
                'liqType' => 91,
                'info' => "轉出BLG [".$vendor." - ".$id."]",
                'extfield1' => $result
            ));
            $this->db->transaction('commit');
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            core::json_err($e->getMessage());
        }
        core::json([
            "status" =>1
        ]);
        return;
    }
}
?>