<?php

class mod_activity extends mod
{

    public function rotary_submit()
    {
        $this->check_post();
        //$this->getdzpSettings();
        //$config    = $this->dzpsettings;
        $config    = $this->getdzpSettings();
        $score     = $config['score'];
        $uid       = $this->user['uid'];
        $user_data = $this->db->query("SELECT `score` FROM `{$this->db_prefix}members` WHERE `uid`={$uid} LIMIT 1", 2);
        if ($user_data['score'] < $score) {
            //$result['angle'] = 0;
            $result['prize'] = '你拥有积分不足，不能能参加转盘抽奖活动！';
        } else if (!$config['switchWeb'] || !$score) {
            //$result['angle'] = 0;
            $result['prize'] = '幸运大转盘活动未开启，敬请关注！';
        } else {
            $prize_arr = array(
                // '0' => array(
                //     'id' => 1,
                //     'prize' => $config['goods289323'],
                //     'v' => $config['chance289323'],
                //     'j' => $config['coin289323']
                // ),
                '0' => array(
                    'id' => 1,
                    'prize' => $config['goods145179'],
                    'v' => $config['chance145179'],
                    'j' => $config['coin145179']
                ),
                '1' => array(
                    'id' => 2,
                    'prize' => $config['goods253287'],
                    'v' => $config['chance253287'],
                    'j' => $config['coin253287']
                ),
                '2' => array(
                    'id' => 3,
                    'prize' => $config['goods3771'],
                    'v' => $config['chance3771'],
                    'j' => $config['coin3771']
                ),
                '3' => array(
                    'id' => 4,
                    'prize' => $config['goods109143'],
                    'v' => $config['chance109143'],
                    'j' => $config['coin109143']
                ),
                '4' => array(
                    'id' => 5,
                    'prize' => $config['goods181215'],
                    'v' => $config['chance181215'],
                    'j' => $config['coin181215']
                ),
                '5' => array(
                    'id' => 6,
                    'prize' => $config['goods035'],
                    'v' => $config['chance035'],
                    'j' => $config['coin035']
                ),
                '6' => array(
                    'id' => 7,
                    'prize' => $config['goods73107'],
                    'v' => $config['chance73107'],
                    'j' => $config['coin73107']
                ),
                '7' => array(
                    'id' => 8,
                    'prize' => $config['goods217251'],
                    'v' => $config['chance217251'],
                    'j' => $config['coin217251']
                ),
                // '9' => array(
                //     'id' => 10,
                //     'prize' => $config['goods325359'],
                //     'v' => $config['chance325359'],
                //     'j' => $config['coin325359']
                // )
            );
            $arr       = $money = array();
            foreach ($prize_arr as $key => $val) {
                $arr[$val['id']] = $val['v'];
                if ($val['j'] > 0)
                    array_push($money, $val['id']);
            }
            $rid             = $this->get_rand($arr);
            $res             = $prize_arr[$rid - 1];
            $result['prize'] = $res['prize'];
            $result['id']=$rid;
            $this->db->transaction('begin');
            try {
                $sql = "UPDATE `{$this->db_prefix}members` SET `score`=`score`-{$score} where uid={$uid} LIMIT 1";
                if (!$this->db->query($sql, 0))
                    throw new Exception('数据库查询失败，请重试');
                $this->user['score'] -= $score;
                $_SESSION[$this->user_session] = serialize($this->user);
                if (in_array($rid, $money)) {
                    $this->set_coin(array(
                        'uid' => $this->user['uid'],
                        'coin' => $res['j'],
                        'liqType' => 120,
                        'extfield0' => 0,
                        'extfield1' => 0,
                        'info' => '大转盘奖金'
                    ));
                    $para = array(
                        'uid' => $this->user['uid'],
                        'info' => $res['prize'],
                        'swapTime' => $this->time,
                        'swapIp' => $this->ip(true),
                        'coin' => $res['j'],
                        'score' => $this->user['score'],
                        'xscore' => $score
                    );
                    if (!$this->db->insert($this->db_prefix . 'dzp_swap', $para))
                        throw new Exception('数据库更新失败，请重试');
                }
                $this->db->transaction('commit');
            }
            catch (Exception $e) {
                $this->db->transaction('rollBack');
                core::json_err('操作失败，请重试');
            }
        }
        core::json([
            "status" =>1,
            "data" =>$result,
        ]);
        return;
        //echo json_encode($result);
    }
    private function get_rand($proArr)
    {
        $result = '';
        $proSum = array_sum($proArr);
        foreach ($proArr as $key => $proCur) {
            $randNum = mt_rand(1, $proSum);
            if ($randNum <= $proCur) {
                $result = $key;
                break;
            } else {
                $proSum -= $proCur;
            }
        }
        unset($proArr);
        return $result;
    }
    // private function getdzpSettings()
    // {
    //     $sql = "SELECT * from `{$this->db_prefix}dzpparams`";
    //     if ($data = $this->db->query($sql, 3)) {
    //         foreach ($data as $var) {
    //             $this->dzpsettings[$var['name']] = $var['value'];
    //         }
    //     }
    // }
}
?>