<?php

class mod_index extends mod
{
    public function __construct()
    {
        $this->user_check = false;
        parent::__construct();
    }
    public function agentlogin()
    {
        $url=AGENT_LOGIN_URL;
        $username = array_key_exists('username', $_POST) ? trim($_POST['username']) : '';
        $password = array_key_exists('password', $_POST) ? trim($_POST['password']) : '';
        if (empty($username)) core::json_err('账户名不能为空');
        if (empty($password)) core::json_err('登录密码不能为空');
		$params = [
            "username" => $username ,
            "password"=>$password,
		];
        $command = 'curl -X POST "'.$url.'" -H "Content-Type: application/x-www-form-urlencoded" -d "'.http_build_query($params).'"';
        //echo $command;
        $result = json_decode(shell_exec($command),true);
        if($result['status']==1)
        {
            $username = 'agent@'.$username;
            $session = array(
                'uid' => 0,
                'username' => $username,
                'session_key' => session_id(),
                'loginTime' => $this->time,
                'accessTime' => $this->time,
                'loginIP' => $this->ip(true)
            );
            $session = array_merge($session, $this->get_browser());
            $session_id = $this->db->insert($this->db_prefix . 'member_session', $session);
            $user = [
                'uid' => 0,
                'username' => $username,
                'forTest' => 0,
                'regTime' => $this->time,
            ];
            if ($session_id) $user['sessionId'] = $session_id;
            $_SESSION[$this->user_session] = serialize($user);
            $this->db->query("UPDATE `{$this->db_prefix}member_session` SET `isOnLine`=0,`state`=1 WHERE `username`='{$username}' AND `id`<{$session_id}", 0);
            core::json([
                "status" =>1,
                "data" => [
                    'username'=>$username,
                ]
            ]);
        }else
        {
            core::json([
                "status" =>0,
                "data" => [
                    'message'=>$result['message'],
                ]
            ]);
        }
    }
    public function get_ccxt()
    {
        include_once SYSTEM . '/lib/ccxt/ccxt.php';
        //include_once "ccxt.php";
        //var_dump (\ccxt\Exchange::$exchanges);

        //date_default_timezone_set ('UTC');
        // instantiate the exchange by id

        $exchange = new \ccxt\binance (array (
            'verbose' => true,
            'timeout' => 30000,
        ));
        try {
            $symbol = 'BTC/USDT';
            $result = $exchange->fetch_ticker ($symbol);
            var_dump ($result);
        } catch (\ccxt\NetworkError $e) {
            echo '[Network Error] ' . $e->getMessage () . "\n";
        } catch (\ccxt\ExchangeError $e) {
            echo '[Exchange Error] ' . $e->getMessage () . "\n";
        } catch (Exception $e) {
            echo '[Error] ' . $e->getMessage () . "\n";
        }
        exit;        
        $exchange = '\\ccxt\\bitfinex2';
        $exchange = new $exchange (array ('enableRateLimit' => true));
        // load all markets from the exchange
        //$markets = $exchange->load_markets ();
        //var_dump($markets);
        $data=[];
        $limit = 30;
        //$data=run($exchange,'BTC/USD','1m',time()-10*60,10);
        $ohlcvs = $exchange->fetchOHLCV ('BTC/USD', '1m', (microtime(true)*1000)-($limit*60000),$limit);
        foreach ($ohlcvs as $v) {
            $data[]=[
                'dt'=>$v[0],
                'O'=>$v[1],
                'H'=>$v[2],
                'L'=>$v[3],
                'C'=>$v[4],
                'dts'=>date('Y-m-d H:i:s',$v[0]/1000),
            ];
        }
        // function run ($exchange, $symbol, $timeframe, $since, $limit) {
        //     $ohlcvs = $exchange->fetchOHLCV ($symbol, $timeframe, $since, $limit);
        //     $ohlc=[];
        //     foreach ($ohlcvs as $v) {
        //         //printf ("%s O:%.8f H:%.8f L:%.8f C:%.8f V:%.8f\n", $exchange->iso8601 ($v[0]), $v[1], $v[2], $v[3], $v[4], $v[5]);
        //         $ohlc[]=[
        //             'dt'=>$v[0],
        //             'O'=>$v[1],
        //             'H'=>$v[2],
        //             'L'=>$v[3],
        //             'C'=>$v[4],
        //         ];
        //     }
        //     return $ohlc;
        // }
        core::json([
            "status" =>1,
            "data"=>$data,
            ]);
    }
    // public function get_cfg()
    // {
    //     $fanDian=0;
    //     $file=md5(__FUNCTION__);
    //     if (array_key_exists($this->user_session, $_SESSION) && $_SESSION[$this->user_session]) {
    //         $this->user = unserialize($_SESSION[$this->user_session]);
    //         $fanDian=$this->user['fanDian'];
    //         $file=md5(__FUNCTION__.$this->user['uid'].$fanDian);
    //     }
    //     if($fanDian>$this->config['fanDianMax'])
    //     {
    //         throw new Exception('错误奖金');
    //     }
    //     $file= $this->getCacheDir().$file;
	// 	if(BPCACHE_TIME && is_file($file) && filemtime($file)+BPCACHE_TIME>$this->time)
	// 	{
	// 		$data= unserialize(file_get_contents($file));
	// 	}else
    //     {
    //         $proportion = 1 - ($this->config['fanDianMax']-$fanDian) / 100;
    //         $sql = "SELECT `id`,`type`,`name` FROM `{$this->db_prefix}type` WHERE `enable`=1 ";
    //         $lottery=$this->db->query($sql, 3);
    //         $sql = "SELECT `id`,`type`,`name`,truncate(`bonusProp`*{$proportion},2) as bp,`maxBalls`,groupId as playedGroup FROM `{$this->db_prefix}played` WHERE `enable`=1 ";
    //         $played=$this->db->query($sql, 3);
    
    //         $lotterys=[];
    //         foreach ($lottery as $v) 
    //         {
    //             $lotterys[$v['id']]=$v;
    //         }
    //         $playeds=[];
    //         foreach ($played as $v) 
    //         {
    //             $playeds[$v['id']]=$v;
    //         }
    //         $data=[
    //             'lottery'=> $lotterys,
    //             'played'=> $playeds,
    //         ];
    //         file_put_contents($file, serialize($data));
    //     }
    //     core::json($data);
    // }

    public function get_bp()
    {
        $fanDian=0;
        $key="type_id";
        $id = array_key_exists($key, $_POST) ? $_POST[$key] : '';
        if (!core::lib('validate')->number($id)) core::__403();
        $type_id = intval($id);
        $file=__FUNCTION__.$type_id;
        if (array_key_exists($this->user_session, $_SESSION) && $_SESSION[$this->user_session]) {
            $this->user = unserialize($_SESSION[$this->user_session]);
            $fanDian=$this->user['fanDian'];
            $file=__FUNCTION__.$type_id.$fanDian;
        }
        if($fanDian>$this->config['fanDianMax'])
        {
            throw new Exception('错误奖金');
        }
        $file=md5($file);
        $file= $this->getCacheDir().$file;
		if(BPCACHE_TIME && is_file($file) && filemtime($file)+BPCACHE_TIME>$this->time)
		{
			$data= unserialize(file_get_contents($file));
		}else
        {
            $proportion = 1 - ($this->config['fanDianMax']-$fanDian) / 100;
            $sql = "SELECT `id`,`type`,`name` FROM `{$this->db_prefix}type` WHERE `enable`=1 and id={$type_id} LIMIT 1";
            $lottery=$this->db->query($sql, 2);
            if(!$lottery)
            {
                throw new Exception('错误彩种');
            }
            $sql = "SELECT `id`,`type`,`name`,truncate(`bonusProp`*{$proportion},2) as bp,`tpl`,groupId as playedGroup,sort FROM `{$this->db_prefix}played` WHERE `enable`=1 and `type`=".$lottery['type'];
            $played=$this->db->query($sql, 3);
            $playeds=[];
            foreach ($played as $v) 
            {
                $playeds[$v['id']]=$v;
            }
            $data=[
                'group_type'=>$lottery['type'],
                'played'=>$playeds,
            ];
            file_put_contents($file, serialize($data));
        }
        core::json($data);
    }

    public function get_type()
    {
        $file=md5(__FUNCTION__);
        $file= $this->getCacheDir().$file;
		if(BPCACHE_TIME && is_file($file) && filemtime($file)+BPCACHE_TIME>$this->time)
		{
			$data= unserialize(file_get_contents($file));
		}else
        {
            $sql = "SELECT `id`,`type`,`title`,`name`,credit,official,hot,frequency,sort FROM `{$this->db_prefix}type` WHERE `enable`=1 order by sort asc";
            $lottery=$this->db->query($sql, 3);
            $lotterys=[];
            foreach ($lottery as $v) 
            {
                $lotterys[$v['id']]=$v;
            }
            $sql = "SELECT `game_type`,`cname`,`ename` FROM `{$this->db_prefix}egame` WHERE `enable`=1 ";
            $egame=$this->db->query($sql, 3);

            $dzpsettings = $this->getdzpSettings();
            $data=[
                'lottery'=> $lotterys,
                'egame'=> $egame,
                'config'=> [
                    'kfUrl'=> $this->config['kefuGG'],
                    'dzp_score'=> $dzpsettings['score'],
                    'app_name'=> $this->config['webName'],
                    'domain_name'=> QR_URL,
                    'bonus_list'=>$this->get_bonus(),
                ],
            ];
            file_put_contents($file, serialize($data));
        }
        //print_r($data);
        core::json($data);
    }
    public function get_group()
    {
        $key="type_id";
        $id = array_key_exists($key, $_POST) ? $_POST[$key] : '';
        $type_id = intval($id);
        $file=__FUNCTION__.$type_id;
        $file=md5($file);
        $file= $this->getCacheDir().$file;
		if(BPCACHE_TIME && is_file($file) && filemtime($file)+BPCACHE_TIME>$this->time)
		{
			$data= unserialize(file_get_contents($file));
		}else
        {

            $sql = "SELECT `id`,`type`,`groupName` FROM `{$this->db_prefix}played_group` WHERE `enable`=1 and isGuan=1 and `type`=".$type_id." order by sort asc";
            $play_group=$this->db->query($sql, 3);
            $play_groups=[];
            foreach ($play_group as $v) 
            {
                $play_groups[$v['type']][]=$v;
            }
            $data=[
                'group_type'=>$type_id,
                'groups'=>$play_groups,
            ];
            file_put_contents($file, serialize($data));
        }
        core::json($data);
    }
	public function get_current_issue(){
        $this->check_post();
        $key="type_id";
        $id = array_key_exists($key, $_POST) ? $_POST[$key] : '';
        if (!core::lib('validate')->number($id)) core::__403();
        //$this->time=strtotime("2019-10-13 23:59:10");
        //echo date("Y-m-d H:i:s",$this->time);
        $type_id = intval($id);
        $ftime=core::lib('game')->get_type_ftime($type_id);
        $next_issue = core::lib('game')->get_game_no_api($type_id);
        $current=[
            "actionNo"=>$next_issue["actionNo"],
            "actionTime"=>strtotime($next_issue['actionTime']),
            "stopTime"=>strtotime($next_issue['actionTime'])-$ftime,
        ];
        
        $last_issue = core::lib('game')->get_game_last_no_api($type_id);
        $openCode=null;
        $sql = "SELECT `data` FROM `{$this->db_prefix}data` WHERE `type`={$type_id} AND `number`='{$last_issue['actionNo']}' LIMIT 1";
        $lottery = $this->db->query($sql, 2);
        if($lottery)
        {
            $openCode=$lottery['data'];
        }
        $last=[
            'actionNo'=>$last_issue['actionNo'],
            'actionTime'=>strtotime($last_issue['actionTime']),
            'openCode'=>$openCode,
            //'openCode'=>"11,18,12,13,10,14,08,09",
            //'openCode'=>"32",
        ];
        core::json([
            "current"=>$current,
            "last"=>$last,
            ]);

    }
    //獲取當日最後一期獎期
    public function get_today_last(){
        $key="type_id";
        $id = array_key_exists($key, $_POST) ? $_POST[$key] : '';
        if (!core::lib('validate')->number($id)) core::__403();
        $type_id = intval($id);
        $last = core::lib('game')->get_today_last($type_id);
        core::json([
            "status" =>1,
            "data"=>$last,
            ]);
    }
	public function get_all_issue(){
        $this->check_post();
        $file=md5(__FUNCTION__);
        $file= $this->getCacheDir().$file;
        if(TIMECENTER_TIME && is_file($file) && filemtime($file)+TIMECENTER_TIME > $this->time)
        //if(false)
		{
            $file_type=md5('_FILE_TYPE');
            $file_type= $this->getCacheDir().$file_type;
            if(BPCACHE_TIME && is_file($file_type) && filemtime($file_type)+BPCACHE_TIME>$this->time)
            {
                $lotterys= unserialize(file_get_contents($file_type));
            }else
            {
                $sql = "SELECT `id`,`type`,`title`,data_ftime FROM `{$this->db_prefix}type` WHERE `enable`=1 order by sort asc";
                $lottery=$this->db->query($sql, 3);
                $lotterys=[];
                foreach ($lottery as $v) 
                {
                    $lotterys[$v['id']]=$v;
                }    
                file_put_contents($file_type, serialize($lotterys));
            }
            $data= unserialize(file_get_contents($file));
            foreach($data['current'] as $k=>$v)
            {
                if($v['stopTime'] <= $this->time)
                {
                    $next_issue = core::lib('game')->get_game_no_api($k);
                    $data['current'][$k]=[
                        "id"=>$k,
                        "title"=>$v['title'],    
                        "actionNo"=>$next_issue["actionNo"],
                        "actionTime"=>strtotime($next_issue['actionTime']),
                        "stopTime"=>strtotime($next_issue['actionTime'])-$lotterys[$k]['data_ftime'],
                    ];
                }
            }
            file_put_contents($file, serialize($data));
		}else
        {
            $file_type=md5('_FILE_TYPE');
            $file_type= $this->getCacheDir().$file_type;
            if(BPCACHE_TIME && is_file($file_type) && filemtime($file_type)+BPCACHE_TIME>$this->time)
            {
                $lotterys= unserialize(file_get_contents($file_type));
            }else
            {
                $sql = "SELECT `id`,`type`,`title`,data_ftime FROM `{$this->db_prefix}type` WHERE `enable`=1 order by sort asc";
                $lottery=$this->db->query($sql, 3);
                $lotterys=[];
                foreach ($lottery as $v) 
                {
                    $lotterys[$v['id']]=$v;
                }    
                file_put_contents($file_type, serialize($lotterys));
            }
            foreach($lotterys as $k=>$v)
            {
                $type_id = $k;
                $next_issue = core::lib('game')->get_game_no_api($type_id);
                $current[$k]=[
                    "id"=>$k,
                    "title"=>$v['title'],
                    "actionNo"=>$next_issue["actionNo"],
                    "actionTime"=>strtotime($next_issue['actionTime']),
                    "stopTime"=>strtotime($next_issue['actionTime'])-$v['data_ftime'],
                ];              
            }
            $sql="select type,number,data from `{$this->db_prefix}trend` where type in(".implode(",",INDEX_ISSUE).") ";
            $index_issue=$this->db->query($sql, 3);
            $index_issues=[];
            foreach ($index_issue as $v) 
            {
                if(isset($lotterys[$v['type']]))
                {
                    $index_issues[$v['type']]=[
                        'id'=>$v['type'],
                        "title"=>$lotterys[$v['type']]['title'],
                        "group_type"=>$lotterys[$v['type']]['type'],
                        'number'=>$v['number'],
                        'data'=>$v['data'],
                    ];
                }
            }
            $data=[
                'current' => $current,
                'index_issue' => $index_issues,
            ];
            file_put_contents($file, serialize($data));
        }
        if(!empty(($_POST['check_issue'])))
        {
            foreach($data['current'] as $k=>$v)
            {
                if(!in_array($k,$_POST['check_issue']))
                {
                    unset($data['current'][$k]);
                }
            }
        }
        core::json([
            "status" =>1,
            "data"=>$data,
            ]);
    }

	public function get_all_issue_game(){
        $this->check_post();
        $file=md5(__FUNCTION__);
        $file= $this->getCacheDir().$file;
        //if(TIMECENTER_TIME && is_file($file) && filemtime($file)+TIMECENTER_TIME > $this->time)
        if(false)
		{
            $file_type=md5('_FILE_TYPE');
            $file_type= $this->getCacheDir().$file_type;
            if(BPCACHE_TIME && is_file($file_type) && filemtime($file_type)+BPCACHE_TIME>$this->time)
            {
                $lotterys= unserialize(file_get_contents($file_type));
            }else
            {
                $sql = "SELECT `id`,`type`,`title`,data_ftime FROM `{$this->db_prefix}type` WHERE `enable`=1 order by sort asc";
                $lottery=$this->db->query($sql, 3);
                $lotterys=[];
                foreach ($lottery as $v) 
                {
                    $lotterys[$v['id']]=$v;
                }    
                file_put_contents($file_type, serialize($lotterys));
            }
            $data= unserialize(file_get_contents($file));
            foreach($data['current'] as $k=>$v)
            {
                if($v['stopTime'] <= $this->time)
                {
                    $next_issue = core::lib('game')->get_game_no_api($k);
                    $data['current'][$k]=[
                        "id"=>$k,
                        "title"=>$v['title'],    
                        "actionNo"=>$next_issue["actionNo"],
                        "actionTime"=>strtotime($next_issue['actionTime']),
                        "stopTime"=>strtotime($next_issue['actionTime'])-$lotterys[$k]['data_ftime'],
                    ];
                }
            }
            file_put_contents($file, serialize($data));
		}else
        {
            $file_type=md5('_FILE_TYPE');
            $file_type= $this->getCacheDir().$file_type;
            if(BPCACHE_TIME && is_file($file_type) && filemtime($file_type)+BPCACHE_TIME>$this->time)
            {
                $lotterys= unserialize(file_get_contents($file_type));
            }else
            {
                $sql = "SELECT `id`,`type`,`title`,data_ftime FROM `{$this->db_prefix}type` WHERE `enable`=1 order by sort asc";
                $lottery=$this->db->query($sql, 3);
                $lotterys=[];
                foreach ($lottery as $v) 
                {
                    $lotterys[$v['id']]=$v;
                }    
                file_put_contents($file_type, serialize($lotterys));
            }
            foreach($lotterys as $k=>$v)
            {
                $type_id = $k;
                $next_issue = core::lib('game')->get_game_no_api($type_id);
                $current[$k]=[
                    "id"=>$k,
                    "title"=>$v['title'],
                    "actionNo"=>$next_issue["actionNo"],
                    "actionTime"=>strtotime($next_issue['actionTime']),
                    "stopTime"=>strtotime($next_issue['actionTime'])-$v['data_ftime'],
                ];              
            }
            $sql="select type,number,data from `{$this->db_prefix}trend` ";
            $index_issue=$this->db->query($sql, 3);
            $index_issues=[];
            foreach ($index_issue as $v) 
            {
                if(isset($lotterys[$v['type']]))
                {
                    $index_issues[$v['type']]=[
                        'id'=>$v['type'],
                        "title"=>$lotterys[$v['type']]['title'],
                        "group_type"=>$lotterys[$v['type']]['type'],
                        'number'=>$v['number'],
                        'data'=>$v['data'],
                    ];    
                }
            }
            $data=[
                'current' => $current,
                'index_issue' => $index_issues,
            ];
            file_put_contents($file, serialize($data));
        }
        if(!empty(($_POST['check_issue'])))
        {
            foreach($data['current'] as $k=>$v)
            {
                if(!in_array($k,$_POST['check_issue']))
                {
                    unset($data['current'][$k]);
                }
            }
        }
        core::json([
            "status" =>1,
            "data"=>$data,
            ]);
    }

    public function login()
    {
        if (array_key_exists('client_type', $_GET) && in_array($_GET['client_type'], $this->clients)) $this->client_type = $_GET['client_type'];
        if ($this->post) {
            $username = array_key_exists('username', $_POST) ? trim($_POST['username']) : '';
            $password = array_key_exists('password', $_POST) ? trim($_POST['password']) : '';
            $remember = (array_key_exists('remember', $_POST) && $_POST['remember'] === '1') ? 1 : 0;
            if (empty($username)) core::json_err('账户名不能为空');
            if (empty($password)) core::json_err('登录密码不能为空');
            if (!core::lib('validate')->username($username)) core::json_err('账户名格式错误');
            $sql = "SELECT * FROM `{$this->db_prefix}members` WHERE `isDelete`=0 AND `username`='$username' LIMIT 1";
            $user = $this->db->query($sql, 2);
            
            if (!$user) core::json_err('您输入的账户不存在');
            if (md5($password.$this->password_key) !== $user['password']) core::json_err('您输入的密码错误');
            if (!$user['enable']) core::json_err('您输入的账户已被冻结，请联系管理员');

            // 检查 parent 是否有被冻结
            // if ($user['parentId']) {
            //     $sql =
            //         "SELECT SUM(disabled) AS parent_disabled
            //         FROM (
            //             SELECT
            //                 @uid AS _uid,
            //                 (SELECT IF(enable = 1, 0, 1) FROM {$this->db_prefix}members WHERE uid =@uid) AS disabled,
            //                 (SELECT @uid :=parentId FROM {$this->db_prefix}members WHERE uid =@uid) AS parentId
            //             FROM (SELECT @uid :=:uid) AS temp
            //                 JOIN {$this->db_prefix}members
            //             WHERE
            //                 @uid IS NOT NULL
            //         ) AS m";
            //     $row = $this->db->getRow($sql, array('uid' => $user['parentId']));
            //     if ($row['parent_disabled'] > 0) {
            //         core::json_err('您输入的账户已被冻结，请联系管理员');
            //     }
            // }

            if ($remember == 1) {
                setcookie('username', $username, $this->time + 86400);
                setcookie('remember', $remember, $this->time + 86400);
            } else {
                setcookie('username', $username, $this->time - 3600);
                setcookie('remember', $remember, $this->time - 3600);
            }
            
            $session = array(
                'uid' => $user['uid'],
                'username' => $user['username'],
                'session_key' => session_id(),
                'loginTime' => $this->time,
                'accessTime' => $this->time,
                'loginIP' => $this->ip(true)
            );
            $session = array_merge($session, $this->get_browser());
            $session_id = $this->db->insert($this->db_prefix . 'member_session', $session);
            
            if ($session_id) $user['sessionId'] = $session_id;
            $_SESSION[$this->user_session] = serialize($user);
            $uid = $user['uid'];
            $this->db->query("UPDATE `{$this->db_prefix}member_session` SET `isOnLine`=0,`state`=1 WHERE `uid`={$uid} AND `id`<{$session_id}", 0);
            $this->db->query("UPDATE `{$this->db_prefix}members` SET `updateTime`='".date('Y-m-d H:i:s',$this->time)."' WHERE `uid`={$uid}", 0);
            core::json([
                "status" =>1,
                "data" => [
                    'uid'=>$user['uid'],
                    'username'=>$user['username'],
                    'type'=>$user['type'],
                    'coin'=>$user['coin'],
                    'scoreTotal'=> $user['scoreTotal'],
                    'grade'=> $user['grade'],
                    'fanDian'=> $user['fanDian'],
                    'forTest'=> $user['forTest'],
                    'score'=> $user['score'],
                ]
            ]);
        }
    }

    public function reg()
    {
        //core::__403();
        //core::json_err('账户名不能为空');
        if (array_key_exists($this->user_session, $_SESSION) && $_SESSION[$this->user_session]) unset($_SESSION[$this->user_session]);
        //if (array_key_exists('client_type', $_GET) && in_array($_GET['client_type'], $this->clients)) $this->client_type = $_GET['client_type'];
        if ($this->post) {
            //if (!array_key_exists('lid', $_POST) || ($_POST['lid'] !== '0' && !core::lib('validate')->number($_POST['lid']))) core::__403();
            $lid=0;
            if(!empty($_POST['lid']))
            {
                $lid = intval($_POST['lid']);
            }
            $username = htmlspecialchars(array_key_exists('username', $_POST) ? trim($_POST['username']) : '',ENT_QUOTES);
            if(strpos($username,"guest_")!==false)
            {
                core::json_err('禁止使用guest_帐户名');
            }
            $password = array_key_exists('password', $_POST) ? trim($_POST['password']) : '';
            $password_repeat = array_key_exists('password_repeat', $_POST) ? trim($_POST['password_repeat']) : '';
            //$qq = array_key_exists('qq', $_POST) ? trim($_POST['qq']) : '';
            if (empty($username)) core::json_err('账户名不能为空');
            if (!core::lib('validate')->username($username)) core::json_err('账户名格式错误');
            if ($this->db->query("SELECT `uid` FROM `{$this->db_prefix}members` WHERE `username`='{$username}' LIMIT 1", 2)) core::json_err('账户名已存在');
            if (empty($password)) core::json_err('登录密码不能为空');
            if ($password !== $password_repeat) core::json_err('两次输入的密码不一致');
            if(strlen($username)<4)core::json_err('账户名小于4位');
            if(strlen($password)<4)core::json_err('密码不能小于4位');            
            //if (empty($qq)) core::json_err('QQ不能为空');
            //if (!core::lib('validate')->qq($qq)) core::json_err('您输入的QQ有误');
            $ip = $this->ip(true);
            $min_reg_time = $this->time - 86400;
            $sql = "SELECT count(uid) as ips FROM `{$this->db_prefix}members` WHERE `regIP`={$ip} AND `regTime`>{$min_reg_time} ORDER BY `uid` DESC LIMIT 1";
            $reged = $this->db->query($sql, 2);
            if ($reged)
            {
                if($reged['ips'] >= 300)
                {
                    core::json_err('同IP注册不能超过'.$reged['ips'].'次');
                }
            } 

            $defaultFandian = floatval($this->config['defaultFandian']);
            $rebate = floatval($this->config['rebate']);
            //if (!$lid && !$defaultFandian) core::json_err('系统已关闭直接注册，请通过邀请链接注册');
            $link = $lid ? $this->db->query("SELECT * FROM `{$this->db_prefix}links` WHERE `lid`={$lid} AND `enable`=1 LIMIT 1", 2) : array();
            
            if ($lid && !$link) {
                core::json_err('该链接已失效，请联系您的上级重新索取注册链接');
            } else {
            	/*
				 * 修正parents为空问题
				 */
                if($link)
                {
                    $parents = $this->db->query("SELECT uid,`parents`,forTest FROM `{$this->db_prefix}members` WHERE `uid`={$link['uid']} LIMIT 1", 2);
                    $fortest=$parents['forTest'];
                    if(empty($parents['parents']))
                    {
                        $parents = $parents['uid'];
                    }else
                    {
                        $parents = $parents['parents'];
                    }    
                }
            }
            $coin=0;
            /*
            if($fortest==1)
            {
            	$coin=1000;
            }
            */
            $line = "";
            if(isset($_POST['line']))
            {
                $line = $_POST['line'];
            }
            $fortest=0;
			require(SYSTEM.'/lib/IP.class.php');
			$regArea=IP::find(long2ip($ip));
            $para = array(
                'source' => 1,
                'username' => $username,
                'type' => $link ? $link['type'] : 1,
				//'type' => 1,
                'password' => md5($password.$this->password_key),
                'parentId' => $link ? $link['uid'] : 0,
                'parents' => $link ? $parents : '',
                'fanDian' => $link ? $link['fanDian'] : $defaultFandian,
                'rebate' => $rebate,
                'regIP' => $ip,
                'regTime' => $this->time,
                //'qq' => $qq,
                'coin' => $coin,
                'fcoin' => 0,
                'score' => 0,
                'scoreTotal' => 0,
                'reg_type'=>$this->client_type,
                'regArea'=>$regArea[1],
                'forTest'=>$fortest,
                'line'=>$line
            );
            try {
                
                $id = $this->db->insert($this->db_prefix . 'members', $para);
                if ($id) {
                    $this->db->transaction('begin');
                    if ($lid)
                    {
                        $sql = "UPDATE `{$this->db_prefix}members` SET `parents`=CONCAT(parents, ',', $id) WHERE `uid`=$id LIMIT 1";
                        $this->db->query($sql, 0);
                        $this->db->query("UPDATE `{$this->db_prefix}links` SET `usedTimes`=`usedTimes`+1,`updateTime`={$this->time} WHERE `lid`=$lid LIMIT 1", 0);
                    }else
                    {
                        $sql = "UPDATE `{$this->db_prefix}members` SET `parents`=$id WHERE `uid`=$id LIMIT 1";
                        $this->db->query($sql, 0);
                    }
                    $zczs = intval($this->config['zczs']);
                    if ($zczs > 0) {
                        $this->set_coin(array(
                            'uid' => $id,
                            'liqType' => 55,
                            'info' => '注册奖励',
                            'coin' => $zczs,
                        ));
                    }
                    $this->db->transaction('commit');
                    $msg = "注册成功";
                    $msg = $zczs !== 0 ? '注册成功，系统赠送您 ' . $zczs . ' 元' : '注册成功';
                    $user=$this->db->query("SELECT * FROM `{$this->db_prefix}members` WHERE `uid`={$id} LIMIT 1", 2);
                    //写入session
                    $session = array(
                        'uid' => $user['uid'],
                        'username' => $user['username'],
                        'session_key' => session_id(),
                        'loginTime' => $this->time,
                        'accessTime' => $this->time,
                        'loginIP' => $this->ip(true)
                    );
                    $session = array_merge($session, $this->get_browser());
                    $session_id = $this->db->insert($this->db_prefix . 'member_session', $session);                    
                    if ($session_id) $user['sessionId'] = $session_id;
                    $_SESSION[$this->user_session] = serialize($user);
                    core::json([
                        "status" =>1,
                        "msg" => $msg,
                        "data" => [
                            'uid'=>$user['uid'],
                            'username'=>$user['username'],
                            'type'=>$user['type'],
                            'coin'=>$user['coin'],
                            'scoreTotal'=> $user['scoreTotal'],
                            'grade'=> $user['grade'],
                            'fanDian'=> $user['fanDian'],
                            'forTest'=> $user['forTest'],
                        ],
                    ]);
                } else {
                    throw new Exception('添加用户信息到数据库失败');
                }
            } catch (Exception $e) {
                $this->db->transaction('rollBack');
                core::json_err($e->getMessage());
            }
        }
    }

    public function guest_login()
    {
        if (array_key_exists($this->user_session, $_SESSION) && $_SESSION[$this->user_session]) unset($_SESSION[$this->user_session]);   
        $ip = $this->ip(true);
        if ($this->post) {
            $username="guest_".$this->randomkeys(6);
            $para = array(
                'source' => 1,
                'username' => $username,
                'type' => 0,
				//'type' => 1,
                'password' => md5($username.$this->password_key),
                'parentId' =>  0,
                'parents' => '',
                'fanDian' => 0,
                'regIP' => $ip,
                'regTime' => $this->time,
                //'qq' => $qq,
                'coin' => 2000,
                'fcoin' => 0,
                'score' => 0,
                'scoreTotal' => 0,
                'reg_type'=>$this->client_type,
                'regArea'=>'',
                'forTest'=>1
            );
            try {
                $id = $this->db->insert($this->db_prefix . 'guest_members', $para);
                if ($id) {
                    $user=$this->db->query("SELECT * FROM `{$this->db_prefix}guest_members` WHERE `uid`={$id} LIMIT 1", 2);
                    //写入session
                    $session = array(
                        'uid' => $user['uid'],
                        'username' => $user['username'],
                        'session_key' => session_id(),
                        'loginTime' => $this->time,
                        'accessTime' => $this->time,
                        'loginIP' => $ip,
                        'guest' => 1,
                    );
                    $session = array_merge($session, $this->get_browser());
                    $session_id = $this->db->insert($this->db_prefix . 'member_session', $session);
                    if ($session_id) $user['sessionId'] = $session_id;
                    $_SESSION[$this->user_session] = serialize($user);
                    $msg = "登录成功";
                    core::json([
                        "status" =>1,
                        "msg" => $msg,
                        "data" => [
                            'uid'=>$user['uid'],
                            'username'=>$user['username'],
                            'type'=>$user['type'],
                            'coin'=>$user['coin'],
                            'scoreTotal'=> $user['scoreTotal'],
                            'grade'=> $user['grade'],
                            'fanDian'=> $user['fanDian'],
                            'forTest'=> $user['forTest'],
                        ],
                    ]);
                } else {
                    throw new Exception('添加用户信息到数据库失败');
                }
            } catch (Exception $e) {
                $this->db->transaction('rollBack');
                core::json_err($e->getMessage());
            }
        }
    }

    public function promotion()
    {
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $sql = "SELECT * FROM `{$this->db_prefix}promotion` WHERE `enable`=1  ORDER BY  `endTime` DESC LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        core::json([
            "status" =>1,
            "data" =>$data,
        ]);
        return;
    }

    public function bulletin()
    {
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $sql = "SELECT id,addTime,title,content FROM `{$this->db_prefix}content` WHERE `enable`=1  ORDER BY  `id` DESC LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        core::json([
            "status" =>1,
            "data" =>$data,
        ]);
        return;
    }

    public function get_recent_open_code()
    {
        $this->check_post();
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $_type = core::lib('validate')->number($_POST['type_id']) ? ($_POST['type_id']) : 0;
        
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $where="";
        if(!empty($_type))
        {
            $where.=" where type = ".$_type;
        }
        if(isset($_POST['fromTime']) && isset($_POST['toTime']))
        {
            $this->get_time(false);
            $where.=$this->build_where_time('`time`');
        }
        $table="{$this->db_prefix}data";
        $last_issue = core::lib('game')->get_game_last_no_api($_type);
        $where.=" and number <='".$last_issue['actionNo']."'";
        $sql="select id,number as actionNo,data as openCode,time as OpenTime
                from `{$table}` {$where} order by id desc LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        core::json([
            "status" =>1,
            "data" =>$data,
        ]);
        return;
    }

    public function hotlost()
    {
        $_type = core::lib('validate')->number($_POST['type_id']) ? ($_POST['type_id']) : 0;
        $sql = "SELECT lost,hot,sp_hot,sp_lost FROM `{$this->db_prefix}trend` WHERE `type`={$_type} order by id desc LIMIT 1";
        $data = $this->db->query($sql, 2);
        if($data)
        {
            $data['lost']=json_decode($data['lost']);
            $data['hot']=json_decode($data['hot']);
            $data['sp_lost']=json_decode($data['sp_lost']);
            $data['sp_hot']=json_decode($data['sp_hot']);
            core::json([
                "status" =>1,
                "data" =>$data,
            ]);    
        }
        return;
    }
    public function clong()
    {
        $_type = core::lib('validate')->number($_POST['type_id']) ? ($_POST['type_id']) : 0;
        $sql = "SELECT clong FROM `{$this->db_prefix}trend` WHERE `type`={$_type} order by id desc LIMIT 1";
        $data = $this->db->query($sql, 2);
        if($data)
        {
            $data=json_decode($data['clong'],true);
            arsort($data);
            core::json([
                "status" =>1,
                "data" =>$data,
            ]);    
        }
        return;
    }

    private function get_bonus()
    {
        $cat=[
            '瑪雅'=>
            [
                '重慶時時彩',
                '北京賽車',
                '幸運飛艇',
                '極速時時彩',
                'PC蛋蛋',
                '廣東11選5',
                '江蘇骰寶',
                '極速賽車',
                '極速5分賽車',
                '極速5分彩',
                '3分快3',
            ],
        ];
        $arr=[];
        for($i=0;$i<30;$i++)
        {
            $key=array_rand($cat);
            $key2=array_rand($cat[$key]);
            $arr[]=[
                "game"=>$cat[$key][$key2],
                "username"=>$this->getName(),
                "bonus"=>number_format(rand(100,9999)),
            ];
        }
        return $arr;
    }
    private function getName()
    {
        $name=chr(rand(97,122))."***".chr(rand(97,122));
        return $name;
    }
    private function randomkeys($length)
    {
        $key = '';
        $pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $pattern1 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pattern2 = '0123456789';
        for ($i = 0; $i < $length; $i++) $key .= $pattern[mt_rand(0, 35)];
        return $key;
    }    
/*
    private function get_trend($type_id)
    {
        $sql = "SELECT `data1`,`data2`,`data3`,`data4`,`data5`,`data6`,`data7`,`data8`,`data9`,`data10` FROM `{$this->db_prefix}trend` WHERE `type`={$type_id} order by id desc LIMIT 1";
        return $this->db->query($sql, 2);
    }

    private function get_recent_bets($type_id)
    {
        $recentNo = core::lib('game')->get_game_recent_no($type_id, 5);
        $actionNo = $recentNo['actionNo'];
        $uid = $this->user['uid'];
        $sql = "SELECT * FROM `{$this->db_prefix}bets_repl` WHERE `isDelete`=0 AND `type`={$type_id} AND `uid`=$uid AND `actionNo`>='{$actionNo}' ORDER BY `id` DESC, `actionTime` DESC LIMIT 0, 50";
        return $this->db->query($sql, 3);
    }
*/
    
}

?>