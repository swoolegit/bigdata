<?php

class mod_game extends mod
{

	public function bet_credit()
	{
		switch(true)
		{
			case $this->user['forTest']==1:
				$this->bet_credit_guest();
				break;
			case $this->user['forTest']==0:
				$this->bet_credit_user();
				break;
		}
	}

	public function bet_official()
	{
		switch(true)
		{
			case $this->user['forTest']==1:
				$this->bet_credit_guest();
				break;
			case $this->user['forTest']==0:
				$this->bet_official_user();
				break;
		}
	}

	public function bet_official_user()
    {
		set_time_limit(180);
		if($this->user['forTest']==1)
		{
			core::json_err('请先注册');
		}
		if($this->user['enable']==0)
		{
			core::json_err('禁止下單');
		}		
        $this->check_post();
        if (
            !array_key_exists('code', $_POST) ||
            !array_key_exists('para', $_POST) ||
            !is_array($_POST['code']) ||
            !is_array($_POST['para'])
        ) core::__403();
        $codes = array();
		for($i=0;$i<count($_POST['code']);$i++) {
			foreach (array('beiShu','playedId','type','actionData','weiShu') as $key){
				if(isset($_POST['code'][$i][$key])) {
					$codes[$i][$key] = $_POST['code'][$i][$key];
				}
			}
		}
		$para = array();
		foreach (array('betType','type','actionNo') as $key){
			if(isset($_POST['para'][$key])) {
				$para[$key] = $_POST['para'][$key];
			}
		}
		$para['type'] = intval($para['type']);

		$zhuihao = array();
		if(isset($_POST['zhuihao']))
		{
			for($i=0;$i<count($_POST['zhuihao']);$i++) {
				foreach (array('no','beisu','zhuihao_stop') as $key){
					if(isset($_POST['zhuihao'][$i][$key])) {
						$zhuihao[$i][$key] = $_POST['zhuihao'][$i][$key];
					}
				}
			}	
		}
        $amount = 0;
        $mincoin = 0;

		if ($this->config['switchBuy'] == 0) core::json_err('本平台已经停止购买');
        if ($this->config['switchDLBuy'] == 0 && $this->user['type']) core::json_err('代理不能下注');
        if ($this->config['switchZDLBuy'] == 0 && $this->user['parents'] == $this->user['uid']) core::json_err('总代理不能下注');

        if (count($codes) == 0) core::json_err('请先选择号码再提交投注');
        //if (!array_key_exists('kjTime', $para)) core::__403();
        if (!array_key_exists('type', $para) || !core::lib('validate')->number($para['type'])) core::__403();
        if (!array_key_exists('actionNo', $para) || !preg_match('/^[0-9_\-]+$/', $para['actionNo'])) core::__403();

        /* 标准 actionNo 时间检查 */
        $ftime = core::lib('game')->get_type_ftime($para['type']);
		$actionNo = core::lib('game')->get_game_no_api($para['type']);
		$actionTimeTS=strtotime($actionNo['actionTime']);

        if ($actionNo['actionNo'] != $para['actionNo'] ||
			($actionTimeTS - $ftime) < $this->time
		){
			core::json_err('投注失败，您投注的第' . $para['actionNo'] . '期已过购买时间，系统自动刷新页面',3);
		}
        /* end:标准 actionNo 时间检查 */
		if($this->user['delay']==1)
		{
			$sleep_time=$actionTimeTS  - $ftime;
			if($sleep_time >= $this->time)
			{
				$sleep_time= $sleep_time - $this->time +2;
			}
			//echo $this->config['closebet'];
			session_write_close();
			//sleep($sleep_time);
			sleep($this->config['closebet']);
			$this->time=time();
			$actionNo = core::lib('game')->get_game_no_api($para['type']);
		}
		$mosi = array();
		if ($this->config['yuanmosi'] == 1) array_unshift($mosi, '2.000');
		if ($this->config['jiaomosi'] == 1) array_unshift($mosi, '0.200');
		if ($this->config['fenmosi'] == 1) array_unshift($mosi, '0.020');
		if ($this->config['limosi'] == 1) array_unshift($mosi, '0.002');
		//玩法
		$plays = $this->get_plays();
		$play_group = $this->get_play_group();
		$actionIP=$this->ip(true);
		require(SYSTEM.'/lib/IP.class.php');
		$betArea=IP::find(long2ip($actionIP));		
        $para = array_merge($para, array(
			'actionTime' => $this->time,
			'updateTime' => $this->time,
            //'actionNo' => $actionNo['actionNo'],
            'kjTime' => $actionTimeTS,
            'actionIP' => $this->ip(true),
            'uid' => $this->user['uid'],
            'username' => $this->user['username'],
            'betArea' => $betArea[1],
            'betCity' => $betArea[2],
			'forTest' => $this->user['forTest'],
			'parentId' => $this->user['parentId'],
            'serializeId' => md5($this->user['uid'].$this->time.uniqid())
        ));
		//print_r($codes );

        foreach ($codes as $key => $code)
        {
			if (!array_key_exists('actionData', $code) || !is_string($code['actionData'])) core::__403();
			if (!array_key_exists('playedId', $code) || !core::lib('validate')->number($code['playedId'])) core::__403();
			if (!array_key_exists('type', $code) || !core::lib('validate')->number($code['type'])) core::__403();
			//begin 檢查注數
			//柱数检查 没做连码,柱数检查注解
			//if (!array_key_exists('actionNum', $code) || !core::lib('validate')->number($code['actionNum'])) core::__403();
            /* check playedid */
            if (!array_key_exists($code['playedId'], $plays)) core::__403();
            $played = $plays[$code['playedId']];
            if ($betCountFun = $played['betCountFun']) {
            	switch(true)
				{
					// case $played['type']==11:
					// 	if($zhuihao)core::json_err('此彩种禁止追号');
					// 	if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'],$played['example'])) core::json_err('下单失败，您投注号码不符合投注规则，请重新投注');
					// 	break;
					// case $played['type']==1 && $played['groupId']==88:
					// 	if($zhuihao)core::json_err('此彩种禁止追号');
					// 	if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'],$played['example'])) core::json_err('下单失败，您投注号码不符合投注规则，请重新投注');
					// 	$codes[$key]['weiShu']=$played['weiShu'];
					// 	break;
					// case $played['type']==12:
					// 	// 六合彩不做注数检查
					// 	break;
					default:
						//if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'])) core::json_err('下单失败，您投注号码不符合投注规则，请重新投注');
						$code['actionNum'] = core::lib('bet')->$betCountFun($code['actionData']);
						break;
				}
            }
			if ($code['actionNum'] > $played['maxcount']) core::json_err('[' . $played['name'] . ']投注上限为' . $played['maxcount'] . '注，请重新投注');
			if ($code['actionNum'] < 1 ) core::json_err('最低为1注，请重新投注');
			//end 檢查注數

			//倍數檢查
            if (!array_key_exists('beiShu', $code) || !core::lib('validate')->number($code['beiShu'])) core::__403();

			//begin 位數檢查
            if (!array_key_exists('weiShu', $code) || (!core::lib('validate')->number($code['weiShu']) && $code['weiShu'] != 0)) core::__403();
			if (in_array($code['playedId'], ['9'])) {
                if (!in_array($code['weiShu'], ['15', '23', '27', '29', '30', '31'])) core::__403();
            }
            if (in_array($code['playedId'], ['15', '22', '23', '24', '41', '196', '201', '202', '219'])) {
                if (!in_array($code['weiShu'],['7', '11', '13', '14', '15', '19', '21', '22', '23', '25', '26', '27', '28', '29', '30', '31'])) core::__403();
            }
            if (in_array($code['playedId'], ['30', '35', '36', '213', '214', '208'])) {
                if (!in_array($code['weiShu'], ['3', '5', '6', '7', '9', '10', '11', '12', '13', '14', '15', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31'])) core::__403();
			}
			//end 位數檢查

			$code['playedId'] = intval($code['playedId']);
			$code['actionAmount'] = intval($code['beiShu'])*$code['actionNum']*2 ;
			$code['mode'] = 2; //官方玩法模式1元起
			//$code['actionNum'] = 1;
			$code['actionNo'] = $actionNo['actionNo'];
			$code['type'] = intval($code['type']);
			$code['playedId'] = intval($code['playedId']);
			$code['playedGroup'] = $played['groupId'];
			//$codes[$key]['mode'] = $code['mode']; //信用玩法模式1元起
			$code['actionName'] = $played['name'];
			//$code['actionData'] = $plays[$code['playedId']]['name'];
			$code['beiShu'] = intval($code['beiShu']);
			$code['isGuan'] = 1;
			//print_r($code);

			if ($code['actionAmount'] < 0) core::json_err('单笔投注金额不得小于0元');
			//检查玩法组合是否正确
			if(!$this->checkGamePlayed($code['type'],$code['playedGroup'],$code['playedId'])) core::json_err('您提交的游戏玩法不存在或已被禁用');

			//$played['maxBalls'] = (int)$played['maxBalls'];
			/* 取得六合彩赔率 */
			// if($played['type']==12)
			// {
			// 	$played['bonusProp']=$this->getLHC_BaseRte($code['actionData'],$code['playedId']);
			// 	$played['bonusPropBase']=$this->getLHC_BaseRte($code['actionData'],$code['playedId']);				
			// }
			//单式不算球数
			if (strpos($code['actionData'],'|') === false && $played['maxBalls'] > 0) {
				$actonDataArray = explode(',',$code['actionData']);
				foreach ($actonDataArray as $rows){
					if($played['type'] == 1 || $played['type'] == 3) {
						$balls = str_split($rows);
					}
					else {
						$balls = explode(' ',$rows);
					}

					if (count($balls) > $played['maxBalls']) core::json_err('投注号码超过'.$played['maxBalls'].'个上限');
				}
			}
			
			$diff_fanDian = $this->config['fanDianMax'] - $this->user['fanDian'];
			if ($diff_fanDian < 0) core::__403();
			//返点强制设0
            $proportion = 1 - $diff_fanDian / 100;
			$bonusProp = $this->floor_dec($played['bonusProp'] * $proportion, 2);
			$code['bonusProp'] = $bonusProp;

            //$bonusPropBase = number_format($played['bonusPropBase'] * $proportion, 2, '.', '');

			//中奖金额检查
			if (( $bonusProp *$code['beiShu'] ) > $this->config['betMaxZjAmount'] && $this->config['betMaxZjAmount']) {
				core::json_err('中奖金额超过' . $this->config['betMaxZjAmount'] . '元上限，请重新投注!');
			}
			//$this_amount = abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
			// $this_amount = abs($code['actionNum'] * $code['actionAmount']);
			// if ($this_amount < 0.01) core::json_err('单笔投注金额不得小于0.01元');
			// if ($actionNo['actionNo'] != $code['actionNo']) {
			// 	core::json_err('投注失败，您投注的第' . $actionNo['actionNo'] .' - '.$code['actionNo']. '期已经过购买时间');
			// }
			$codes[$key] = array_merge($code, $para);
			$amount += $code['actionAmount'];
		}
		$limit_per_actionNO = $this->user['limitPreActionNo'];//小于等于0==>无限制
		if ($limit_per_actionNO > 0) {//小于等于0==>无限制
			$history_amount = $this->db->query("SELECT sum(actionAmount) as sumary FROM `{$this->db_prefix}bets_repl` WHERE `uid`={$this->user['uid']} AND `actionNo`=" . $actionNo['actionNo'] . " AND `type`=" . $para['type'] . " AND `isDelete`=0", 2);
			$history_amount = $history_amount['sumary'];
			if ($history_amount == null) $history_amount = 0;
			$total_amount = $amount + $history_amount;
			if ($total_amount > $limit_per_actionNO) core::json_err('超过当期投注金额上限' . $limit_per_actionNO . '元');
		}
		if ($amount < $mincoin) core::json_err('您的投注金额小于最低消费金额' . $mincoin . '元，请重新投注');
		if(count($zhuihao) > 0)
		{
			$zhuihao_data=[];
			$amount = 0;
			foreach($codes as $k=>$v)
			{
				foreach($zhuihao as $k1=>$v1)
				{
					$zhuihao_amount = intval($v1['beisu'])*$code['actionNum']*2;
					if($v1['no']==$v['actionNo'])
					{
						$codes[$k]['beiShu'] = $v1['beisu'];
						$codes[$k]['actionAmount'] = $zhuihao_amount;
						$codes[$k]['zhuiHao'] = 1;
						$codes[$k]['zhuiHaoMode'] = $v1['zhuihao_stop'];
					}else
					{
						if(strlen($v1['no'])!=strlen($v['actionNo']))
						{
							core::json_err('追号期数异常');
						}
						if(strnatcmp($v['actionNo'], $v1['no']) > 0)
						//if(intval($v1['no'])<intval($v['actionNo']))
						{
							core::json_err($v1['no'].'期已过购买时间,请刷新奖期');
						}
						$zhuihao_data[] = array_merge($v, [
							'beiShu' => $v1['beisu'],
							'actionAmount' => $zhuihao_amount,
							'actionNo' => $v1['no'],
							'zhuiHao' => 1,
							'zhuiHaoMode' => $v1['zhuihao_stop'],
						]);
					}
					$amount += $zhuihao_amount;
				}
			}
			$codes = array_merge($codes, $zhuihao_data);
		}

		$liqType = 101;
		$info = '投注';
        $this->db->transaction('begin');
        try {
			$uid = $this->user['uid'];
			$userAmount = $this->db->query("SELECT `coin` FROM `{$this->db_prefix}members` WHERE `uid`={$uid} LIMIT 1", 2);
			$userAmount = $userAmount['coin'];
			if ($userAmount < $amount) core::json_err('可用资金不足，请先充值');	
			/* 批次下注 (已验证) */
            //$code['actionData']
			// $zikiArray=array(
			// 	0=>array(0,0,0,0,0,0,0,0,0,0),
			// 	1=>array(0,0,0,0,0,0,0,0,0,0),
			// 	2=>array(0,0,0,0,0,0,0,0,0,0),
			// 	3=>array(0,0,0,0,0,0,0,0,0,0),
			// 	4=>array(0,0,0,0,0,0,0,0,0,0)
			// );
			// $zikipk10Array=array(
			// 	0=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	1=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	2=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	3=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	4=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	5=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	6=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	7=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	8=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	9=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0)
			// );
			$sqlZhuihao_SP=array();
			$zikai_type=array(5,26,60,62,71,72,73,74,75,76,77,93,94);
			// $zikaipk10_type=array(75);
			// $zikaik3_type=array(60,93);
            foreach ($codes as $code)
            {
				//返点强制设0
				$code['fanDian'] = 0;
                unset($code['playedName']);
				$code['wjorderId'] = $code['type'] . $code['playedId'] . $this->randomkeys(8);
                $code['actionNum'] = abs($code['actionNum']);
				//$amount = abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
				$amount = abs($code['actionAmount']);
				$zj = abs($code['actionNum'] * $code['bonusProp'] * $code['beiShu']);
				$code['md5'] = md5($code['actionData'].$code['wjorderId'].$code['serializeId'].$amount.uniqid());
                $id = $this->db->insert($this->db_prefix . 'bets', $code);
                $this->set_coin(array(
                    'uid' => $this->user['uid'],
                    'type' => $code['type'],
                    'liqType' => $liqType,
                    'info' => $info,
                    'extfield0' => $id,
                    'extfield1' => $para['serializeId'],
                    'coin' => -$amount,
				));
				
				//if(in_array($code['type'],$zikai_type) || in_array($code['type'],$zikaipk10_type)  || in_array($code['type'],$zikaik3_type))
				if(in_array($code['type'],$zikai_type))
				{
					$sqlZhuihao_SP[]="(".$code['type'].",".$code['actionNo'].",".$amount.",".$zj.",2,'".$code['actionData']."','".$code['md5']."',".$code['playedId'].",0)";
				}
			}
			if(count($sqlZhuihao_SP) > 0)
			{
				$sql="insert {$this->db_prefix}ziki_sprule(type,actionNo,bet,zj,rule_type,actionData,md5,playedId,weiShu)values".implode(",", $sqlZhuihao_SP);
				$this->db->query($sql, 0);
			}
			$this->db->transaction('commit');
			core::json([
				"status" =>1,
				"msg"=>'投注成功',
				"coin"=>$userAmount - $amount,
				]);
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            core::json_err($e->getMessage());
        }
	}

    public function bet_credit_user()
    {
		set_time_limit(180);
		if($this->user['forTest']==1)
		{
			core::json_err('请先注册');
		}
		if($this->user['enable']==0)
		{
			core::json_err('禁止下單');
		}		
        $this->check_post();
        if (
            !array_key_exists('code', $_POST) ||
            !array_key_exists('para', $_POST) ||
            !is_array($_POST['code']) ||
            !is_array($_POST['para'])
        ) core::__403();
        $codes = array();
		for($i=0;$i<count($_POST['code']);$i++) {
			foreach (array('actionAmount','playedId','type','actionData','playedGroup') as $key){
				if(isset($_POST['code'][$i][$key])) {
					$codes[$i][$key] = $_POST['code'][$i][$key];
				}
			}
		}
		$para = array();
		foreach (array('betType','type','actionNo') as $key){
			if(isset($_POST['para'][$key])) {
				$para[$key] = $_POST['para'][$key];
			}
		}
        $para['type'] = intval($para['type']);

        $amount = 0;
        $mincoin = 0;

		if ($this->config['switchBuy'] == 0) core::json_err('本平台已经停止购买');
        if ($this->config['switchDLBuy'] == 0 && $this->user['type']) core::json_err('代理不能下注');
        if ($this->config['switchZDLBuy'] == 0 && $this->user['parents'] == $this->user['uid']) core::json_err('总代理不能下注');

        if (count($codes) == 0) core::json_err('请先选择号码再提交投注');
        //if (!array_key_exists('kjTime', $para)) core::__403();
        if (!array_key_exists('type', $para) || !core::lib('validate')->number($para['type'])) core::__403();
        if (!array_key_exists('actionNo', $para) || !preg_match('/^[0-9_\-]+$/', $para['actionNo'])) core::__403();

        /* 标准 actionNo 时间检查 */
        $ftime = core::lib('game')->get_type_ftime($para['type']);
		$actionNo = core::lib('game')->get_game_no_api($para['type']);
		$actionTimeTS=strtotime($actionNo['actionTime']);

        if ($actionNo['actionNo'] != $para['actionNo'] ||
			($actionTimeTS - $ftime) < $this->time
		){
			core::json_err('投注失败，您投注的第' . $para['actionNo'] . '期已过购买时间，系统自动刷新页面',3);
		}
        /* end:标准 actionNo 时间检查 */
		if($this->user['delay']==1)
		{
			$sleep_time=$actionTimeTS  - $ftime;
			if($sleep_time >= $this->time)
			{
				$sleep_time= $sleep_time - $this->time +2;
			}
			//echo $this->config['closebet'];
			session_write_close();
			//sleep($sleep_time);
			sleep($this->config['closebet']);
			$this->time=time();
			$actionNo = core::lib('game')->get_game_no_api($para['type']);
		}
		$mosi = array();
		if ($this->config['yuanmosi'] == 1) array_unshift($mosi, '2.000');
		if ($this->config['jiaomosi'] == 1) array_unshift($mosi, '0.200');
		if ($this->config['fenmosi'] == 1) array_unshift($mosi, '0.020');
		if ($this->config['limosi'] == 1) array_unshift($mosi, '0.002');
		//玩法
		$types = $this->get_check_type($para['type']);
		$plays = $this->get_plays();
		$play_group = $this->get_play_group();
		$actionIP=$this->ip(true);
		require(SYSTEM.'/lib/IP.class.php');
		$betArea=IP::find(long2ip($actionIP));		
        $para = array_merge($para, array(
			'actionTime' => $this->time,
			'updateTime' => $this->time,
            'actionNo' => $actionNo['actionNo'],
            'kjTime' => $actionTimeTS,
            'actionIP' => $this->ip(true),
            'uid' => $this->user['uid'],
            'username' => $this->user['username'],
            'betArea' => $betArea[1],
            'betCity' => $betArea[2],
			'forTest' => $this->user['forTest'],
			'parentId' => $this->user['parentId'],
            'serializeId' => md5($this->user['uid'].$this->time.uniqid())
        ));
		
        foreach ($codes as $key => $code)
        {
			if (!array_key_exists('actionData', $code) || !is_string($code['actionData'])) core::__403();
			if (!array_key_exists('playedId', $code) || !core::lib('validate')->number($code['playedId'])) core::__403();
			if (!array_key_exists('type', $code) || !core::lib('validate')->number($code['type'])) core::__403();
			$code['playedId'] = intval($code['playedId']);
			$code['actionAmount'] = intval($code['actionAmount']);
			$code['mode'] = 1; //信用玩法模式1元起
			$code['actionNum'] = 1;
			$code['actionNo'] = $actionNo['actionNo'];
			$code['type'] = intval($code['type']);
			$code['playedId'] = intval($code['playedId']);
			$code['playedGroup'] = $plays[$code['playedId']]['groupId'];
			//$codes[$key]['mode'] = $code['mode']; //信用玩法模式1元起
			$code['actionName'] = $play_group[$code['playedGroup']]['groupName'];
			$code['actionData'] = $plays[$code['playedId']]['name'];
			$code['beiShu'] = 1;
			$code['isGuan'] = 0;

			if ($code['actionAmount'] < $this->config['min_bet']) core::json_err('单笔投注金额不得小于'.$this->config['min_bet'].'元');
			//检查玩法组合是否正确
			if(!$this->checkGamePlayed($code['type'],$code['playedGroup'],$code['playedId'])) core::json_err('您提交的游戏玩法不存在或已被禁用');
            /* check playedid */
            if (!array_key_exists($code['playedId'], $plays)) core::__403();
            $played = $plays[$code['playedId']];

			//$played['maxBalls'] = (int)$played['maxBalls'];
			/* 取得六合彩赔率 */
			// if($played['type']==12)
			// {
			// 	$played['bonusProp']=$this->getLHC_BaseRte($code['actionData'],$code['playedId']);
			// 	$played['bonusPropBase']=$this->getLHC_BaseRte($code['actionData'],$code['playedId']);				
			// }
			//单式不算球数
			// if (strpos($code['actionData'],'|') === false && $played['maxBalls'] > 0) {
			// 	$actonDataArray = explode(',',$code['actionData']);

			// 	foreach ($actonDataArray as $rows){
			// 		if($played['type'] == 1 || $played['type'] == 3) {
			// 			$balls = str_split($rows);
			// 		}
			// 		else {
			// 			$balls = explode(' ',$rows);
			// 		}

			// 		if (count($balls) > $played['maxBalls']) core::json_err('投注号码超过<span class="btn btn-red">'.$played['maxBalls'].'</span>个上限');
			// 	}
			// }
			
			$diff_fanDian = $this->config['fanDianMax'] - $this->user['fanDian'];
			if ($diff_fanDian < 0) core::__403();
			//返点强制设0
            $proportion = 1 - $diff_fanDian / 100;
			$bonusProp = $this->floor_dec($played['bonusProp'] * $proportion, 2);
			$code['bonusProp'] = $bonusProp;

            //$bonusPropBase = number_format($played['bonusPropBase'] * $proportion, 2, '.', '');

            // if (!array_key_exists('bonusProp', $code) || !core::lib('validate')->floor_dec($code['bonusProp'], 2)) core::__403();
			// if ($code['bonusProp'] > $bonusProp) core::json_err('提交奖金大于最大奖金，请重新投注 - '.$code['bonusProp']." - ".$bonusProp);
            //if ($code['bonusProp'] < $bonusPropBase) core::json_err('提交奖金小于最小奖金，请重新投注 - '.$code['bonusProp']." - ".$bonusPropBase);
            //if (!array_key_exists('fanDian', $code) || !core::lib('validate')->floor_dec($code['fanDian'], 1)) core::__403();

			//柱数检查 没做连码,柱数检查注解
            // if (!array_key_exists('actionNum', $code) || !core::lib('validate')->number($code['actionNum'])) core::__403();
            // if ($betCountFun = $played['betCountFun']) {
            // 	switch(true)
			// 	{
			// 		case $played['type']==11:
			// 			if($zhuihao)core::json_err('此彩种禁止追号');
			// 			if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'],$played['example'])) core::json_err('下单失败，您投注号码不符合投注规则，请重新投注');
			// 			break;
			// 		case $played['type']==1 && $played['groupId']==88:
			// 			if($zhuihao)core::json_err('此彩种禁止追号');
			// 			if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'],$played['example'])) core::json_err('下单失败，您投注号码不符合投注规则，请重新投注');
			// 			$codes[$key]['weiShu']=$played['weiShu'];
			// 			break;
			// 		case $played['type']==12:
			// 			// 六合彩不做注数检查
			// 			break;
			// 		default:
			// 			if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'])) core::json_err('下单失败，您投注号码不符合投注规则，请重新投注');
			// 			break;
			// 	}
            // }
			// if ($code['actionNum'] > $played['maxcount']) core::json_err('[' . $played['name'] . ']投注上限为<span class="btn btn-red">' . $played['maxcount'] . '</span>注，请重新投注');

			//中奖金额检查
			if (( $bonusProp *$code['actionAmount'] ) > $this->config['betMaxZjAmount'] && $this->config['betMaxZjAmount']) {
				core::json_err('中奖金额超过' . $this->config['betMaxZjAmount'] . '元上限，请重新投注!');
			}
			//$this_amount = abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
			// $this_amount = abs($code['actionNum'] * $code['actionAmount']);
			// if ($this_amount < 0.01) core::json_err('单笔投注金额不得小于0.01元');
			// if ($actionNo['actionNo'] != $code['actionNo']) {
			// 	core::json_err('投注失败，您投注的第' . $actionNo['actionNo'] .' - '.$code['actionNo']. '期已经过购买时间');
			// }
			$codes[$key] = array_merge($code, $para);
			$amount += $code['actionAmount'];
		}
		$limit_per_actionNO = $this->user['limitPreActionNo'];//小于等于0==>无限制
		if ($limit_per_actionNO > 0) {//小于等于0==>无限制
			$history_amount = $this->db->query("SELECT sum(actionAmount) as sumary FROM `{$this->db_prefix}bets_repl` WHERE `uid`={$this->user['uid']} AND `actionNo`=" . $actionNo['actionNo'] . " AND `type`=" . $para['type'] . " AND `isDelete`=0", 2);
			$history_amount = $history_amount['sumary'];
			if ($history_amount == null) $history_amount = 0;
			$total_amount = $amount + $history_amount;
			if ($total_amount > $limit_per_actionNO) core::json_err('超过当期投注金额上限' . $limit_per_actionNO . '元');
		}
		if ($amount < $mincoin) core::json_err('您的投注金额小于最低消费金额' . $mincoin . '元，请重新投注');
		$liqType = 101;
		$info = '投注';
        $this->db->transaction('begin');
        try {
			$uid = $this->user['uid'];
			$userAmount = $this->db->query("SELECT `coin` FROM `{$this->db_prefix}members` WHERE `uid`={$uid} LIMIT 1", 2);
			$userAmount = $userAmount['coin'];
			if ($userAmount < $amount) core::json_err('可用资金不足，请先充值');	
			/* 批次下注 (已验证) */
            //$code['actionData']
			// $zikiArray=array(
			// 	0=>array(0,0,0,0,0,0,0,0,0,0),
			// 	1=>array(0,0,0,0,0,0,0,0,0,0),
			// 	2=>array(0,0,0,0,0,0,0,0,0,0),
			// 	3=>array(0,0,0,0,0,0,0,0,0,0),
			// 	4=>array(0,0,0,0,0,0,0,0,0,0)
			// );
			// $zikipk10Array=array(
			// 	0=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	1=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	2=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	3=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	4=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	5=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	6=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	7=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	8=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
			// 	9=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0)
			// );
			$sqlZhuihao_SP=array();
			$zikai_type=array(5,26,60,62,71,72,73,74,75,76,77,93,94);
			// $zikaipk10_type=array(75);
			// $zikaik3_type=array(60,93);
            foreach ($codes as $code)
            {
				//返点强制设0
				$code['fanDian'] = 0;
                unset($code['playedName']);
				$code['wjorderId'] = $code['type'] . $code['playedId'] . $this->randomkeys(8);
                $code['actionNum'] = abs($code['actionNum']);
				//$amount = abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
				$amount = abs($code['actionAmount']);
				$zj = abs($code['actionAmount'] * $code['bonusProp']);
				$code['md5'] = md5($code['actionData'].$code['wjorderId'].$code['serializeId'].$amount.uniqid());
                $id = $this->db->insert($this->db_prefix . 'bets', $code);
                $this->set_coin(array(
                    'uid' => $this->user['uid'],
                    'type' => $code['type'],
                    'liqType' => $liqType,
                    'info' => $info,
                    'extfield0' => $id,
                    'extfield1' => $para['serializeId'],
                    'coin' => -$amount,
				));
				
				//if(in_array($code['type'],$zikai_type) || in_array($code['type'],$zikaipk10_type)  || in_array($code['type'],$zikaik3_type))
				//if(in_array($code['type'],$zikai_type))
				if($types['zikai']==1)
				{
					$sqlZhuihao_SP[]="(".$code['type'].",".$code['actionNo'].",".$amount.",".$zj.",2,'".$code['actionData']."','".$code['md5']."',".$code['playedId'].",0)";
				}
			}
			if(count($sqlZhuihao_SP) > 0)
			{
				$sql="insert {$this->db_prefix}ziki_sprule(type,actionNo,bet,zj,rule_type,actionData,md5,playedId,weiShu)values".implode(",", $sqlZhuihao_SP);
				$this->db->query($sql, 0);
			}
			$this->db->transaction('commit');
			core::json([
				"status" =>1,
				"msg"=>'投注成功',
				"coin"=>$userAmount - $amount,
				]);
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            core::json_err($e->getMessage());
        }
    }

    public function bet_credit_guest()
    {
		$this->check_post();
		if($this->user['forTest']==0)
		{
			core::json_err('仅供试玩帐号投注');
		}		
        if (
            !array_key_exists('code', $_POST) ||
            !array_key_exists('para', $_POST) ||
            !is_array($_POST['code']) ||
            !is_array($_POST['para'])
        ) core::__403();
        $codes = array();
		for($i=0;$i<count($_POST['code']);$i++) {
			foreach (array('actionAmount','playedId','type','actionData','playedGroup') as $key){
				if(isset($_POST['code'][$i][$key])) {
					$codes[$i][$key] = $_POST['code'][$i][$key];
				}
			}
		}
		$para = array();
		foreach (array('betType','type','actionNo') as $key){
			if(isset($_POST['para'][$key])) {
				$para[$key] = $_POST['para'][$key];
			}
		}
        $para['type'] = intval($para['type']);

        $amount = 0;
        $mincoin = 0;

		if ($this->config['switchBuy'] == 0) core::json_err('本平台已经停止购买');
        //if ($this->config['switchDLBuy'] == 0 && $this->user['type']) core::json_err('代理不能下注');
        //if ($this->config['switchZDLBuy'] == 0 && $this->user['parents'] == $this->user['uid']) core::json_err('总代理不能下注');

        if (count($codes) == 0) core::json_err('请先选择号码再提交投注');
        //if (!array_key_exists('kjTime', $para)) core::__403();
        if (!array_key_exists('type', $para) || !core::lib('validate')->number($para['type'])) core::__403();
        if (!array_key_exists('actionNo', $para) || !preg_match('/^[0-9_\-]+$/', $para['actionNo'])) core::__403();

        /* 标准 actionNo 时间检查 */
        $ftime = core::lib('game')->get_type_ftime($para['type']);
		$actionNo = core::lib('game')->get_game_no_api($para['type']);
		$actionTimeTS=strtotime($actionNo['actionTime']);

        if ($actionNo['actionNo'] != $para['actionNo'] ||
			($actionTimeTS - $ftime) < $this->time
		){
			core::json_err('投注失败，您投注的第' . $para['actionNo'] . '期已过购买时间');
		}
        /* end:标准 actionNo 时间检查 */

		$mosi = array();
		if ($this->config['yuanmosi'] == 1) array_unshift($mosi, '2.000');
		if ($this->config['jiaomosi'] == 1) array_unshift($mosi, '0.200');
		if ($this->config['fenmosi'] == 1) array_unshift($mosi, '0.020');
		if ($this->config['limosi'] == 1) array_unshift($mosi, '0.002');
		//玩法
		$plays = $this->get_plays();
		$play_group = $this->get_play_group();
		$actionIP=$this->ip(true);
		require(SYSTEM.'/lib/IP.class.php');
		$betArea=IP::find(long2ip($actionIP));		
        $para = array_merge($para, array(
            'actionTime' => $this->time,
            'actionNo' => $actionNo['actionNo'],
            'kjTime' => $actionTimeTS,
            'actionIP' => $this->ip(true),
            'uid' => $this->user['uid'],
            'username' => $this->user['username'],
            'betArea' => $betArea[1],
            'betCity' => $betArea[2],
            'forTest' => $this->user['forTest'],
            'serializeId' => md5($this->user['uid'].$this->time.uniqid())
        ));
		
        foreach ($codes as $key => $code)
        {
			if (!array_key_exists('actionData', $code) || !is_string($code['actionData'])) core::__403();
			if (!array_key_exists('playedId', $code) || !core::lib('validate')->number($code['playedId'])) core::__403();
			if (!array_key_exists('type', $code) || !core::lib('validate')->number($code['type'])) core::__403();
			$code['playedId'] = intval($code['playedId']);
			$code['actionAmount'] = intval($code['actionAmount']);
			$code['mode'] = 1; //信用玩法模式1元起
			$code['actionNum'] = 1;
			$code['actionNo'] = $actionNo['actionNo'];
			$code['type'] = intval($code['type']);
			$code['playedId'] = intval($code['playedId']);
			$code['playedGroup'] = $plays[$code['playedId']]['groupId'];
			//$codes[$key]['mode'] = $code['mode']; //信用玩法模式1元起
			$code['actionName'] = $play_group[$code['playedGroup']]['groupName'];
			$code['actionData'] = $plays[$code['playedId']]['name'];
			$code['beiShu'] = 1;
			$code['isGuan'] = 0;

			if ($code['actionAmount'] < 0) core::json_err('单笔投注金额不得小于0元');
			//检查玩法组合是否正确
			if(!$this->checkGamePlayed($code['type'],$code['playedGroup'],$code['playedId'])) core::json_err('您提交的游戏玩法不存在或已被禁用');
            /* check playedid */
            if (!array_key_exists($code['playedId'], $plays)) core::__403();
            $played = $plays[$code['playedId']];

			//$played['maxBalls'] = (int)$played['maxBalls'];
			/* 取得六合彩赔率 */
			// if($played['type']==12)
			// {
			// 	$played['bonusProp']=$this->getLHC_BaseRte($code['actionData'],$code['playedId']);
			// 	$played['bonusPropBase']=$this->getLHC_BaseRte($code['actionData'],$code['playedId']);				
			// }
			//单式不算球数
			// if (strpos($code['actionData'],'|') === false && $played['maxBalls'] > 0) {
			// 	$actonDataArray = explode(',',$code['actionData']);

			// 	foreach ($actonDataArray as $rows){
			// 		if($played['type'] == 1 || $played['type'] == 3) {
			// 			$balls = str_split($rows);
			// 		}
			// 		else {
			// 			$balls = explode(' ',$rows);
			// 		}

			// 		if (count($balls) > $played['maxBalls']) core::json_err('投注号码超过<span class="btn btn-red">'.$played['maxBalls'].'</span>个上限');
			// 	}
			// }
			
			$diff_fanDian = $this->config['fanDianMax'] - $this->user['fanDian'];
			if ($diff_fanDian < 0) core::__403();
			//返点强制设0
            $proportion = 1 - $diff_fanDian / 100;
			$bonusProp = $this->floor_dec($played['bonusProp'] * $proportion, 2);
			$code['bonusProp'] = $bonusProp;

            //$bonusPropBase = number_format($played['bonusPropBase'] * $proportion, 2, '.', '');

            // if (!array_key_exists('bonusProp', $code) || !core::lib('validate')->floor_dec($code['bonusProp'], 2)) core::__403();
			// if ($code['bonusProp'] > $bonusProp) core::json_err('提交奖金大于最大奖金，请重新投注 - '.$code['bonusProp']." - ".$bonusProp);
            //if ($code['bonusProp'] < $bonusPropBase) core::json_err('提交奖金小于最小奖金，请重新投注 - '.$code['bonusProp']." - ".$bonusPropBase);
            //if (!array_key_exists('fanDian', $code) || !core::lib('validate')->floor_dec($code['fanDian'], 1)) core::__403();

			//柱数检查 没做连码,柱数检查注解
            // if (!array_key_exists('actionNum', $code) || !core::lib('validate')->number($code['actionNum'])) core::__403();
            // if ($betCountFun = $played['betCountFun']) {
            // 	switch(true)
			// 	{
			// 		case $played['type']==11:
			// 			if($zhuihao)core::json_err('此彩种禁止追号');
			// 			if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'],$played['example'])) core::json_err('下单失败，您投注号码不符合投注规则，请重新投注');
			// 			break;
			// 		case $played['type']==1 && $played['groupId']==88:
			// 			if($zhuihao)core::json_err('此彩种禁止追号');
			// 			if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'],$played['example'])) core::json_err('下单失败，您投注号码不符合投注规则，请重新投注');
			// 			$codes[$key]['weiShu']=$played['weiShu'];
			// 			break;
			// 		case $played['type']==12:
			// 			// 六合彩不做注数检查
			// 			break;
			// 		default:
			// 			if ($code['actionNum'] != core::lib('bet')->$betCountFun($code['actionData'])) core::json_err('下单失败，您投注号码不符合投注规则，请重新投注');
			// 			break;
			// 	}
            // }
			// if ($code['actionNum'] > $played['maxcount']) core::json_err('[' . $played['name'] . ']投注上限为<span class="btn btn-red">' . $played['maxcount'] . '</span>注，请重新投注');

			//中奖金额检查
			if (( $bonusProp *$code['actionAmount'] ) > $this->config['betMaxZjAmount'] && $this->config['betMaxZjAmount']) {
				core::json_err('中奖金额超过' . $this->config['betMaxZjAmount'] . '元上限，请重新投注!');
			}
			//$this_amount = abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
			// $this_amount = abs($code['actionNum'] * $code['actionAmount']);
			// if ($this_amount < 0.01) core::json_err('单笔投注金额不得小于0.01元');
			// if ($actionNo['actionNo'] != $code['actionNo']) {
			// 	core::json_err('投注失败，您投注的第' . $actionNo['actionNo'] .' - '.$code['actionNo']. '期已经过购买时间');
			// }
			$codes[$key] = array_merge($code, $para);
			$amount += $code['actionAmount'];
		}
		// $limit_per_actionNO = $this->user['limitPreActionNo'];//小于等于0==>无限制
		// if ($limit_per_actionNO > 0) {//小于等于0==>无限制
		// 	$history_amount = $this->db->query("SELECT sum(actionAmount) as sumary FROM `{$this->db_prefix}bets_repl` WHERE `uid`={$this->user['uid']} AND `actionNo`=" . $actionNo['actionNo'] . " AND `type`=" . $para['type'] . " AND `isDelete`=0", 2);
		// 	$history_amount = $history_amount['sumary'];
		// 	if ($history_amount == null) $history_amount = 0;
		// 	$total_amount = $amount + $history_amount;
		// 	if ($total_amount > $limit_per_actionNO) core::json_err('超过当期投注金额上限' . $limit_per_actionNO . '元');
		// }
		if ($amount < $mincoin) core::json_err('您的投注金额小于最低消费金额' . $mincoin . '元，请重新投注');
		$liqType = 101;
		$info = '投注';
        $this->db->transaction('begin');
        try {
			$uid = $this->user['uid'];
			$userAmount = $this->db->query("SELECT `coin` FROM `{$this->db_prefix}guest_members` WHERE `uid`={$uid} LIMIT 1", 2);
			$userAmount = $userAmount['coin'];
			if ($userAmount < $amount) core::json_err('您的可用资金不足，请先充值');
            foreach ($codes as $code)
            {
				//返点强制设0
				$code['fanDian'] = 0;
                unset($code['playedName']);
				$code['wjorderId'] = $code['type'] . $code['playedId'] . $this->randomkeys(8);
                $code['actionNum'] = abs($code['actionNum']);
				//$amount = abs($code['actionNum'] * $code['mode'] * $code['beiShu']);
				$amount = abs($code['actionAmount']);
				$zj = abs($code['actionAmount'] * $code['bonusProp']);
				$code['md5'] = md5($code['actionData'].$code['wjorderId'].$code['serializeId'].$amount.uniqid());
                $id = $this->db->insert($this->db_prefix . 'guest_bets', $code);
                $this->set_coin_guest(array(
                    'uid' => $this->user['uid'],
                    'type' => $code['type'],
                    'liqType' => $liqType,
                    'info' => $info,
                    'extfield0' => $id,
                    'extfield1' => $para['serializeId'],
                    'coin' => -$amount,
				));
			}
			$this->db->transaction('commit');
			core::json([
				"status" =>1,
				"msg"=>'投注成功'
				]);
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            core::json_err($e->getMessage());
        }
	}
	
    private function randomkeys($length)
    {
        $key = '';
        $pattern = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $pattern1 = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pattern2 = '0123456789';
        for ($i = 0; $i < $length; $i++) $key .= $pattern{mt_rand(0, 35)};
        return $key;
    }

    // private function get_trend($type_id)
    // {
    //     $sql = "SELECT `data1`,`data2`,`data3`,`data4`,`data5`,`data6`,`data7`,`data8`,`data9`,`data10` FROM `{$this->db_prefix}trend` WHERE `type`={$type_id} order by id desc LIMIT 1";
    //     return $this->db->query($sql, 2);
    // }

    // private function get_recent_bets($type_id)
    // {
    //     $recentNo = core::lib('game')->get_game_recent_no($type_id, 5);
    //     $actionNo = $recentNo['actionNo'];
    //     $uid = $this->user['uid'];
    //     $sql = "SELECT * FROM `{$this->db_prefix}bets_repl` WHERE `isDelete`=0 AND `type`={$type_id} AND `uid`=$uid AND `actionNo`>='{$actionNo}' ORDER BY `id` DESC, `actionTime` DESC LIMIT 0, 50";
    //     return $this->db->query($sql, 3);
    // }


}

?>