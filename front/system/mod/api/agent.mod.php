<?php

class mod_agent extends mod
{
    public function __construct()
    {
        parent::__construct();
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
    }

    public function get_recent_open_code()
    {
        $this->check_post();
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $_type = core::lib('validate')->number($_POST['type_id']) ? ($_POST['type_id']) : 0;
        
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $where="";
        if(!empty($_type))
        {
            $where.=" where type = ".$_type;
        }
        if(isset($_POST['fromTime']) && isset($_POST['toTime']))
        {
            $this->get_time(false);
            $where.=$this->build_where_time('`time`');
        }
        $table="{$this->db_prefix}data";
        // $last_issue = core::lib('game')->get_game_last_no_api($_type);
        // $where.=" and number <='".$last_issue['actionNo']."'";
        $sql="select id,number as actionNo,data as openCode,time as OpenTime
                from `{$table}` {$where} order by time desc LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);

        $last_issue = core::lib('game')->get_game_last_no_api($_type);
        $openCode=null;
        $sql = "select id,number as actionNo,data as openCode,time as OpenTime FROM `{$table}` WHERE `type`={$_type} AND `number`='{$last_issue['actionNo']}' LIMIT 1";
        $last_lottery = $this->db->query($sql, 2);
        // if($lottery)
        // {
        //     $openCode=$lottery['data'];
        // }
        // $last=[
        //     'actionNo'=>$last_issue['actionNo'],
        //     'actionTime'=>strtotime($last_issue['actionTime']),
        //     'openCode'=>$openCode,
        // ];
        core::json([
            "status" =>1,
            "data" =>
            [
                "all" => $data,
                "last" => $last_lottery,
            ],
        ]);
        return;
    }

    public function search()
    {
        $this->check_post();
        $uid = $this->user['uid'];
        $username=trim($_POST['username']);
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $where="";

        if(!empty($username))
        {
            if (!core::lib('validate')->username($username)) core::json_err('用户名格式错误');
            $where.=" and username='{$username}'";
        }

        $this->get_time(false);
        $time=$this->build_where_time('l.actionTime');
        // $sql="select
        //     ifnull(sum(l.zj) ,0) as zj,
        //     ifnull(sum(l.real_bet) ,0) as real_bet,
        //     ifnull(sum(l.fandian) ,0) as fandian
        //     from
        //     {$this->db_prefix}member_report l  
        //     where l.uid={$uid} {$time} limit 1";
        // $u_info=$this->db->query($sql, 2);

        $sql="select uid,username from {$this->db_prefix}members where parentId={$uid} {$where}";
        $data = $this->db->query($sql, 3);
        $ch_id=[];
        $ch_map=[];
        $team_info=[];
        $team_info_res=[];
        $u_info=[
            "zj"=>0,
            "real_bet"=>0,
            "fandian"=>0,
            "pfandian"=>0,
        ];
        foreach($data as $k=>$v)
        {
            $ch_id[]=$v['uid'];
            $ch_map[$v['uid']]=$v['username'];
        }
        if(count($ch_id)>0)
        {
            $sql="select
            ifnull(sum(l.zj),0) as zj,
            ifnull(sum(l.real_bet),0) as real_bet,
            ifnull(sum(l.fandian),0) as fandian
            from
            {$this->db_prefix}member_report l  
            where l.uid in (".implode(",", $ch_id).") {$time}  LIMIT 1";
            $u_info=$this->db->query($sql, 2);

            $sql="select
            l.uid,
            sum(l.zj) as zj,
            sum(l.real_bet) as real_bet,
            sum(l.fandian) as fandian,
            sum(l.team_bet) as team_bet
            from
            {$this->db_prefix}member_report l  
            where l.uid in (".implode(",", $ch_id).") {$time} GROUP BY l.uid LIMIT {$skip},{$this->pagesize}";
            $team_info_res=$this->db->query($sql, 3);
        }

        foreach($team_info_res as $k=>$v)
        {
            $team_info[]=[
                "uid"=>$v['uid'],
                "username"=>$ch_map[$v['uid']],
                "zj"=>$v['zj'],
                "real_bet"=>$v['real_bet'],
                "fandian"=>$v['fandian'],
                "team_bet"=>$v['team_bet'],
            ];
        }

        $sql="select
        ifnull(sum(l.fandian),0) as fandian
        from
        {$this->db_prefix}member_report l  
        where l.uid={$uid} {$time}  LIMIT 1";
        $p_info=$this->db->query($sql, 2);
        $u_info['pfandian']=$p_info['fandian'];
        core::json([
            "status" =>1,
            "u_info" => $u_info,
            "team_info" => $team_info,
        ]);
    }

    public function add()
    {
        $this->check_post();
        $username = array_key_exists('username', $_POST) ? trim($_POST['username']) : '';
        $password = array_key_exists('password', $_POST) ? trim($_POST['password']) : '';
        //$qq = array_key_exists('qq', $_POST) ? trim($_POST['qq']) : '';
        //$type = array_key_exists('type', $_POST) ? intval($_POST['type']) : -1;
        $fanDian = array_key_exists('fanDian', $_POST) ? floatval($_POST['fanDian']) : -1;
        //$gongZi = array_key_exists('gongZi', $_POST) ? floatval($_POST['gongZi']) : -1;
        //$fenHong = array_key_exists('fenHong', $_POST) ? floatval($_POST['fenHong']) : -1;
        $memo = array_key_exists('memo', $_POST) ? trim($_POST['memo']) : '';
		$memo = htmlspecialchars(strip_tags(addslashes($memo)));
        $type = 1; //强制改成代理
        //if($this->user['forTest']==1 && isset($this->user['parentId']))core::json_err('测试帐号无法添加用户');
        if(strpos($username,"guest_")!==false)
        {
            core::json_err('禁止使用guest_帐户名');
        }
		if (empty($username)) core::json_err('用户名不能为空');
        if (!core::lib('validate')->username($username)) core::json_err('用户名格式错误');
        if ($this->db->query("SELECT `uid` FROM `{$this->db_prefix}members` WHERE `username`='{$username}' LIMIT 1", 2)) core::json_err('账户名已存在');
        if (empty($password)) core::json_err('登录密码不能为空');
		$qq = '';
		//if (empty($qq)) core::json_err('腾讯QQ不能为空');
		//if (!core::lib('validate')->qq($qq)) core::json_err('您输入的QQ有误');
        $max = $this->user['fanDian'] - $this->config['fanDianDiff'];
        $max = $max < 0 ? 0 : $max;
        if (!in_array($type, array(0, 1))) core::json_err('会员类型错误');
        if ($fanDian > $max) core::json_err('用户返点不得超过'.$max);
        if ($fanDian < 0) core::json_err('用户返点不得小于0');

		// if($this->user['gongZi'] != 0) {
		// 	if ($gongZi > $this->user['gongZi']) core::json_err('日工资不得超过'.$this->user['gongZi']);
		// 	if ($gongZi < 0) core::json_err('日工资不得小于0');
		// }
		// else{
		// 	$gongZi = 0;
		// }

		// if($this->user['fenHong'] != 0) {
		// 	if ($fenHong > $this->user['fenHong']) core::json_err('分红不得超过'. $this->user['fenHong']);
		// 	if ($fenHong < 0) core::json_err('分红不得小于0');
		// }
		// else {
		// 	$fenHong = 0;
		// }

		// $sql = "SELECT `userCount`, (SELECT COUNT(*) FROM `{$this->db_prefix}members` m WHERE m.parentId={$this->user['uid']} AND m.fanDian=s.fanDian) registerCount FROM `{$this->db_prefix}params_fandianset` s WHERE s.fanDian={$fanDian}";
        // $count = $this->db->query($sql, 2);
        // if ($count && $count['registerCount'] >= $count['userCount']) {
        //     core::json_err('对不起返点为<span class="btn btn-red">' . $fanDian . '</span>的下级人数已经达到上限');
        // }
		$ip=$this->ip(true);
		require(SYSTEM.'/lib/IP.class.php');
        $regArea=IP::find(long2ip($ip));
        $rebate = floatval($this->config['rebate']);
        $para = array(
            'source' => 1,
            'username' => $username,
            'type' => $type,
            'password' => md5($password.$this->password_key),
            'parentId' => $this->user['uid'],
            'parents' => $this->user['parents'] ? $this->user['parents'] : $this->user['uid'],
            'fanDian' => $fanDian,
            'rebate' => $rebate,
            // 'gongZi' => $gongZi,
            // 'fenHong' => $fenHong,
            'regIP' => $ip,
            'regTime' => $this->time,
            'qq' => $qq,
            'coin' => 0,
            'fcoin' => 0,
            'score' => 0,
            'scoreTotal' => 0,
            'forTest' => $this->user['forTest'],
            'regArea'=>$regArea[1],
            'memo' => $memo,
            'reg_type'=>$this->client_type,
        );
        $this->db->transaction('begin');
        try {
            $id = $this->db->insert($this->db_prefix . 'members', $para);
            if ($id) {
                $sql = "UPDATE `{$this->db_prefix}members` SET `parents`=CONCAT(parents, ',', $id) WHERE `uid`=$id LIMIT 1";
                $this->db->query($sql, 0);
                // $zczs = intval($this->config['zczs']);
                // if ($zczs > 0) {
                //     $this->set_coin(array(
                //         'uid' => $id,
                //         'liqType' => 55,
                //         'info' => '注册奖励',
                //         'coin' => $zczs,
                //     ));
                // }
                $this->db->transaction('commit');
                // $msg = $zczs !== 0 ? '添加成功，系统赠送给会员[' . $username . '] ' . $zczs . ' 元' : '添加成功';
                // $this->dialogue(array(
                //     'type' => 'success',
                //     'text' => $msg,
                //     'auto' => true,
                //     'yes' => array(
                //         'text' => '我知道了',
                //         'func' => '$.reload();',
                //     ),
                // ));
                $msg="添加成功";
                $sp = $this->db->query("SELECT lid,usedTimes,fanDian FROM `{$this->db_prefix}links` WHERE `uid`={$this->user['uid']}", 2);
                if(!$sp)
                {
                    $para=[
                        "uid"=>$uid,
                        "usedTimes"=>1,
                        "fanDian"=>$this->user['fanDian'],
                        "type"=>1,
                        "updateTime"=>$this->time,
                    ];
                    $id = $this->db->insert($this->db_prefix . 'links', $para);
                }else
                {
                    $sql = "UPDATE `{$this->db_prefix}links` SET usedTimes=usedTimes+1 WHERE `uid`={$this->user['uid']} LIMIT 1";
                    $this->db->query($sql, 0);
                }
                core::json([
                    "status" =>1,
                    "msg" => $msg,
                ]);
            } else {
                throw new Exception('添加用户信息到数据库失败');
            }
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            core::json_err($e->getMessage());
        }
    }

    public function qr()
    {
        $uid = $this->user['uid'];
        $sp = $this->db->query("SELECT lid,usedTimes,fanDian FROM `{$this->db_prefix}links` WHERE `uid`={$uid}", 2);
        if(!$sp)
        {
            $para=[
                "uid"=>$uid,
                "usedTimes"=>0,
                "fanDian"=>$this->user['fanDian'],
                "type"=>1,
                "updateTime"=>$this->time,
            ];
            $id = $this->db->insert($this->db_prefix . 'links', $para);
            $sp=[
                "usedTimes"=>0,
                "fanDian"=>$this->user['fanDian'],
                "lid"=>$id,
            ];
        }
        core::json([
            "status" =>1,
            "sp" =>$sp,
        ]);
        return;
    }

    public function last7()
    {
        $uid = $this->user['uid'];
        $today = date("Y-m-d");
        $sp = $this->db->query("SELECT actionDate,fandian FROM `{$this->db_prefix}member_report` WHERE `uid`={$uid} and actionDate ='".$today."' limit 1", 2);
        $todayFd=0;
        if($sp)
        {
            $todayFd=$sp['fandian'];
        }
        core::json([
            "status" =>1,
            "todayFd" =>$todayFd,
        ]);
        return;
    }

    public function team_last7()
    {
        $uid = $this->user['uid'];
        $today = date("Y-m-d");
        $last7 = strtotime("-14 day", strtotime($today));
        $fd = $this->db->query("SELECT actionDate,fandian FROM `{$this->db_prefix}member_report` WHERE `uid`={$uid} and actionTime > {$last7} order by  actionTime asc limit 7", 3);
        $links = $this->db->query("SELECT actionDate,members FROM `{$this->db_prefix}report_links` WHERE `uid`={$uid} and actionTime > {$last7} order by  actionTime asc limit 7", 3);
        $bets = $this->db->query("SELECT actionDate,team_bet as real_bet FROM `{$this->db_prefix}member_report` WHERE `uid`={$uid} and actionTime > {$last7}  order by  actionTime asc limit 7", 3);
        $last7=[];
        $todayFd=0;
        $todayReg=0;
        $todayBet=0;
        $today = date("Y-m-d");
        for($i=13;$i>=0;$i--)
        {
            $day = date("Y-m-d", strtotime(($i*-1)." day", strtotime($today)));
            $last7[$day]=[];
        }
        foreach($last7 as $k=>$v)
        {
            $last7[$k]['actionDate']=$k;
            $last7[$k]['fandian']=0;
            $last7[$k]['members']=0;
            $last7[$k]['real_bet']=0;
            foreach($fd as $k1=>$v1)
            {
                if($v1['actionDate']==$k)
                {
                    $last7[$k]['fandian']=$v1['fandian'];
                }
                if($v1['actionDate']==$today)
                {
                    $todayFd=$v1['fandian'];
                }
            }
            foreach($links as $k1=>$v1)
            {
                if($v1['actionDate']==$k)
                {
                    $last7[$k]['members']=$v1['members'];
                }
                if($v1['actionDate']==$today)
                {
                    $todayReg=$v1['members'];
                }
            }
            foreach($bets as $k1=>$v1)
            {
                if($v1['actionDate']==$k)
                {
                    $last7[$k]['real_bet']=$v1['real_bet'];
                }
                if($v1['actionDate']==$today)
                {
                    $todayReg=$v1['real_bet'];
                }
            }
        }
        core::json([
            "status" =>1,
            "last7" =>$last7,
            "fd" =>$todayFd,
            "reg" =>$todayReg,
            "bet" =>$todayBet,
        ]);
        return;
    }

    public function qrup()
    {
        $uid = $this->user['uid'];
        if (!core::lib('validate')->number_float($_POST['fandian'], 1)) core::json_err('您输入的[返点]格式错误');
        $fanDian = floatval($_POST['fandian']);
        $fanDian = sprintf('%.1f',$fanDian);
        if ($fanDian < 0) core::json_err('[返点]不能小于0');
        if ($fanDian > $this->user['fanDian']) core::json_err('[返点]不能大于'.$this->user['fanDian']);

        $sql="update `{$this->db_prefix}links` set  `fanDian`={$fanDian}  WHERE `uid`={$uid} LIMIT 1";
        $this->db->query($sql,0);
        core::json([
            "status" =>1,
            "msg" =>"修改成功",
        ]);
        return;
    }
    
    public function list()
    {
        $uid=$this->user['uid'];
        $username = array_key_exists('username', $_POST) ? trim($_POST['username']) : '';
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $where = "";
        if(!empty($username))
        {
            if (!core::lib('validate')->username($username)) core::json_err('用户名格式错误');
            $where.=" and username='{$username}'";
        }
        if(isset($_POST['fromTime']) && isset($_POST['toTime']))
        {
            $this->get_time(false);
            $where .= $this->build_where_time('`regTime`');    
        }
        $sql="select uid,username,FROM_UNIXTIME(regTime, '%Y-%m-%d') as regTime,DATE_FORMAT(updateTime, '%Y-%m-%d') as updateTime,fanDian,memo
                from `{$this->db_prefix}members` u where u.parentId={$uid} {$where} order by u.updateTime desc LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        $uid=[];
        $member_data=[];
        foreach($data as $k=>$v)
        {
            $uid[] = $v["uid"];
            $data[$k]['members']=0;
            //$member_data[$v["uid"]]=array_merge($v,['members'=>0]);
        }
        if(count($uid) > 0)
        {
            $sql="select uid,usedTimes from `{$this->db_prefix}links` where uid in (".implode(",",$uid).")";
            $links_data = $this->db->query($sql, 3);
            foreach($links_data as $k=>$v)
            {
                //$member_data[$v["uid"]]['members']=$v['usedTimes'];
                foreach($data as $k1=>$v1)
                {
                    if($v['uid']==$v1['uid'])
                    {
                        $data[$k1]['members']=$v['usedTimes'];
                    }
                }
            }
        }
        core::json([
            "status" =>1,
            "data" =>$data,
        ]);
        return;
    }
    public function up()
    {
        $this->check_post();

		if (!is_numeric($_POST['uid'])) core::__403();

		if (!array_key_exists('uid', $_POST) || !core::lib('validate')->number($_POST['uid']) || !array_key_exists('fandian', $_POST)) core::__403();
        if (!core::lib('validate')->number_float($_POST['fandian'], 1)) core::json_err('您输入的[用户返点]格式错误');
        $uid = intval($_POST['uid']);
        $fanDian = floatval($_POST['fandian']);
		$fanDian = sprintf('%.1f',$fanDian);
        $memo = array_key_exists('memo', $_POST) ? trim($_POST['memo']) : '';
		$memo = htmlspecialchars(strip_tags(addslashes($memo)));

		//if (!array_key_exists('memo', $_POST)) $_POST['memo'] = '';
		//$memo = htmlspecialchars(strip_tags(addslashes($_POST['memo'])));

		// if($this->user['gongZi'] != 0) {
	    //     if (!core::lib('validate')->number_float($_POST['gongZi'], 1)) core::json_err('您输入的[日工资]格式错误');
		// 	$gongZi = floatval($_POST['gongZi']);
		// 	$gongZi = sprintf('%.1f',$gongZi);
		// 	if ($gongZi < 0) core::json_err('[日工资]不能小于0');
		// 	if ($gongZi > $this->user['gongZi']) core::json_err('[日工资]不能大于' .  $this->user['gongZi']);
		// 	$result = $this->db->query("SELECT max(gongZi) as gongZi FROM `{$this->db_prefix}members` WHERE `uid`!={$uid} AND CONCAT(',', parents, ',') LIKE '%,{$uid},%' LIMIT 1", 2);
		// 	if ($gongZi < $result['gongZi']) core::json_err('[日工资]不能小于' .  $result['gongZi']);
		
		// }
		// else {
		// 	$gongZi = 0;
		// }

		// if($this->user['fenHong'] != 0) {
	    //     if (!core::lib('validate')->number_float($_POST['fenHong'], 1)) core::json_err('您输入的[分红]格式错误');
		// 	$fenHong = floatval($_POST['fenHong']);
		// 	$fenHong = sprintf('%.1f',$fenHong);
		// 	if ($fenHong < 0) core::json_err('[分红]不能小于0');
		// 	if ($fenHong > $this->user['fenHong']) core::json_err('[分红]不能大于' .  $this->user['fenHong']);
		// 	$result = $this->db->query("SELECT max(fenHong) as fenHong FROM `{$this->db_prefix}members` WHERE `uid`!={$uid} AND CONCAT(',', parents, ',') LIKE '%,{$uid},%' LIMIT 1", 2);
		// 	if ($fenHong < $result['fenHong']) core::json_err('[分红]不能小于' .  $result['fenHong']);
		// }
		// else {
		// 	$fenHong = 0;
		// }
        $sql="select uid,parentId from `{$this->db_prefix}members` WHERE uid={$uid} and parentId=".$this->user['uid']."  LIMIT 1";
        $check_parent=$this->db->query($sql, 2);
        if(!$check_parent)
        {
            core::json_err('无权修改');
        }

        if ($fanDian < 0) core::json_err('[用户返点]不能小于0');
        if ($fanDian > $this->user['fanDian']) core::json_err('[用户返点]不能大于'.$this->user['fanDian']);

        $cur_fandian = $this->db->query("SELECT max(`fanDian`) as fanDian FROM `{$this->db_prefix}members` WHERE parents LIKE '%,{$uid},%' and fanDian > {$fanDian} LIMIT 1", 2);
        if($cur_fandian)
        {
            if(!empty($cur_fandian['fanDian']))
            {
                core::json_err('[用户返点]不能小于' . $cur_fandian['fanDian']);
            }
        }

		$temp_fanDian = intval(str_replace('.', '', $fanDian));
        $temp_fanDianDiff = intval(str_replace('.', '', $this->config['fanDianDiff']));

		if ($temp_fanDianDiff != 0) { 
			if ($temp_fanDian % $temp_fanDianDiff) core::json_err(sprintf('返点只能是%.1f%%的倍数', $this->config['fanDianDiff']));
		}
        $this->db->query("UPDATE `{$this->db_prefix}members` SET `fanDian`={$fanDian},memo='{$memo}'  WHERE `uid`={$uid} LIMIT 1", 0);
        $msg="修改成功";
        core::json([
            "msg" => $msg,
        ]);        

    }    
}
?>