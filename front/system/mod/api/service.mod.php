<?php
//@session_start();
class mod_service extends mod
{
    public function __construct()
    {
        $this->user_check = false;
        parent::__construct();
    }

	//BC000001 ==> D59494651AAF76EC4169D3548A45DB9F
	//代理商識別碼
	private $KEY = SERVICE_CODE;
	private $TimeLimit = 1800;
	private $CacheTime = 1;
	//private $AgentKey = 'ABC@123456#';

	private $allowIPS = [
		'125.227.59.91',
		'125.227.59.92',
		'1.161.135.91',
	];

	//private $ApiUrl = 'http://www.bclottery.wa';
	/*
	public function __construct() {
		parent::__construct();
		$this->Crypt = (new Crypt('TEGameXM001'));
	}*/

	private function check_sign($data) {
		if(!isset($data['sign']) || !isset($data['agentID']) || !isset($data['time']))
		{
			$this->output(100,'缺少必要參數');
		}
		$sign=md5($data['agentID'].$this->KEY.$data['time']);
		if($sign!=$data['sign'])
		{
			$this->output(99,'加密錯誤');
		}
		if($data['time']+$this->TimeLimit < time())
		//if(false)
		{
			$this->output(103,'請求過期');
		}
		return;
	}

	private function checkAgent($data) {
		$sql="select uid,username from {$this->db_prefix}members where username='".$data['agentID']."' limit 1";
		$agent=$this->db->query($sql,2);
		if (!$agent) {
			$this->output(101,'代理商識別碼錯誤');
		}
		return $agent;
	}

	private function output($code = 0,$message = '',$values = null) {
		$returnData = [
			'Code' => $code,
			'Message' => $message,
		];
		if(isset($values))
		{
			$returnData['values'] = $values;
		}
		//if(strlen($values) || is_array($values)) $returnData['values'] = $values;
		die(json_encode($returnData, JSON_UNESCAPED_UNICODE));
	}

	private function checkIP(){
		return true;
		$touch = 0;
		foreach ($this->allowIPS as $ip){
			if (strpos($ip, $_SERVER['REMOTE_ADDR']) !== false) {
				$touch++;
				break;
			}		
		}
		if (!$touch) {
			$this->output(901,'無效的IP');
		}	
	}
	/**
	* showdoc
	* @catalog 遊戲
	* @title 獲取注單
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/GetBetRecords
	* @param agentID 必選 string 代理ID  
	* @param sign 必選 string md5加密串
	* @param time 必選 int 時間搓  
	* @param startTime 選填 DateTime 查詢開始時間,若沒有填入查詢時間,預設回傳15分鐘內數據
	* @param endTime 選填 DateTime 查詢結束時間,若沒有填入查詢時間,預設回傳15分鐘內數據
	* @return {"Code":0,"Message":"SUCCESS","values":[{"orderID":"721005AWrzS0q1QG","username":"testuser","type":"極速賽車","actionNo":"201910050985","lotteryNo":"5,7,2,6,1,3,4,8,9,10","actionName":"冠、亞軍和","actionData":"冠亞大","actionTime":"2019-10-05 16:2312:12","updateTime":"2019-10-05 16:3134:34","state":"1","amount":1,"bonus":"2.20","winlost":1.2},{"orderID":"721005qtshLj9f53","username":"testuser","type":"極速賽車","actionNo":"201910050985","lotteryNo":"5,7,2,6,1,3,4,8,9,10","actionName":"冠軍","actionData":"大","actionTime":"2019-10-05 16:2312:12","updateTime":"2019-10-05 16:3134:34","state":"1","amount":1,"bonus":"0.00","winlost":-1},{"orderID":"721005wY8SgxQbTm","username":"testuser","type":"極速賽車","actionNo":"201910050985","lotteryNo":"5,7,2,6,1,3,4,8,9,10","actionName":"冠軍","actionData":"單","actionTime":"2019-10-05 16:2312:12","updateTime":"2019-10-05 16:3134:34","state":"1","amount":1,"bonus":"1.98","winlost":0.98}]}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON物件
	* @return_param values.orderID string 注單編號
	* @return_param values.username string 玩家帳號
	* @return_param values.type string 彩種
	* @return_param values.actionNo string 獎期
	* @return_param values.lotteryNo string 開獎號碼
	* @return_param values.actionName string 玩法
	* @return_param values.actionData string 投注內容
	* @return_param values.actionTime DateTime 投注時間
	* @return_param values.updateTime DateTime 結算時間,注單最後更新時間
	* @return_param values.state int 注單狀態 , 0:未開獎 1:已結算已開獎 2:撤單
	* @return_param values.amount int 投注金額 
	* @return_param values.bonus decimal(10,2) 中獎金額
	* @return_param values.winlost decimal(10,2) 輸贏
	* @remark 查詢時間是以最後更新時間, 舉例: 時時彩每20分鐘開獎一次,玩家現在投注,介面回傳該筆注單(未開獎). 20分鐘介面回傳同筆注單(已結算已開獎)
	* @number 
	*/
	//投注記錄
	public final function GetBetRecords(){
		$json = $_POST;
		$this->checkIP();
		$this->check_sign($json);
		$agent=$this->checkAgent($json);
		$file=__FUNCTION__.$json['agentID'];
        $file=md5($file);
        $file= $this->getCacheDir().$file;
		if($this->CacheTime && is_file($file) && filemtime($file)+$this->CacheTime>$this->time)
		//if(false)
		{
			$BetData= unserialize(file_get_contents($file));
		}else
		{
			$startTime=time();
			$endTime=time()-900; //預設取15分鐘內注單
			//$endTime=time()-86400; //預設取15分鐘內注單
			if(!empty($json['startTime']) && !empty($json['endTime']))
			{
				$startTime=strtotime($json['startTime']);
				$endTime=strtotime($json['endTime']);
			}
			$sql="select id,title from {$this->db_prefix}type";
			$data=$this->db->getRows($sql);
			$TypeData=[];
			foreach($data as $k=>$v)
			{
				$TypeData[$v['id']]=$v['title'];
			}
	
			$table="bets_repl8";
			$time=time()-(7*86400); // 查詢日期超過7天
			if($startTime <= $time || $endTime <= $time)
			{
				$table="bets_repl";
			}
			$sql="select id,wjorderId,username,type,actionName,actionNo,actionData,actionTime,updateTime
					,lotteryNo,bonus,isDelete,actionAmount
					from {$this->db_prefix}{$table}
					where parentId={$agent['uid']} and updateTime BETWEEN {$startTime} and  {$endTime} limit 20000
			";
			$data=$this->db->getRows($sql);
			$BetData=[];
			foreach($data as $k=>$v)
			{
				//$bet_amount=$v['money']*$v['totalNums'];
				// if($v['betInfo'] && $v['Groupname'] != '連肖連尾')
				// {
				// 	$bet_amount=$v['totalMoney'];
				// }
				$bet_state="0"; //未開獎
				if($v['isDelete']==1)
				{
					$bet_state="2"; //撤單
				}
				if($v['lotteryNo']!='')
				{
					$bet_state="1"; //已結算
				}
				$winlost=0;
				if($bet_state=="1")
				{
					$winlost=number_format($v['bonus']-$v['actionAmount'],2);
				}
				
				$BetData[]=[
					"orderID"=>$v['wjorderId'],
					"username"=>$v['username'],
					"type"=>$TypeData[$v['type']],
					"actionNo"=>$v['actionNo'],
					"lotteryNo"=>$v['lotteryNo'],
					"actionName"=>$v['actionName'],
					"actionData"=>$v['actionData'],
					"actionTime"=>date('Y-m-d H:i:s',$v['actionTime']),
					"updateTime"=>date('Y-m-d H:i:s',$v['updateTime']),
					"state"=>$bet_state,
					"amount"=>$v['actionAmount'],
					"bonus"=>$v['bonus'],
					"winlost"=>$winlost
				];
			}
			file_put_contents($file, serialize($BetData));			
		}

		$this->output(0,'SUCCESS', $BetData);
	}
	/**
	* showdoc
	* @catalog 帳號
	* @title 玩家是否線上
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/CheckPlayerIsOnline
	* @param agentID 必選 string 代理ID  
	* @param account 必選 string 玩家帳號
	* @param sign 必選 string md5加密串, 加密規則為md5(agentID+KEY+time)
	* @param time 必選 int 時間搓  
	* @return {"Code":0,"Message":"SUCCESS","values":{"account":"testuser","IsOnline":1}}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON物件
	* @return_param values.account string 使用者帳號
	* @return_param values.IsOnline int 1線上 0離線
	* @remark 
	* @number 
	*/
	//檢查玩家是否線上
	public final function CheckPlayerIsOnline(){
		$data = $_POST;
		$this->checkIP();
		$this->check_sign($data);
		$sql="select uid,username from {$this->db_prefix}members where username='".$data['account']."' limit 1";
		$user=$this->db->query($sql,2);
		if(!$user)
		{
			$this->output(105,'無此使用者');	
		}
		$checktime=time()-MEMBER_SESSIONTIME;
		$sql="select * from {$this->db_prefix}member_session where accessTime >=".$checktime." and uid=".$user['uid']." and isOnLine=1";
		$session=$this->db->query($sql,2);
		$array = [
			'account' => $data['account'],
			'IsOnline' => ($session) ? 1 : 0,
		];
		$this->output(0,'SUCCESS', $array);
	}
	


	/**
	* showdoc
	* @catalog 帳號
	* @title 查詢玩家餘額
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/GetPlayerBalance
	* @param agentID 必選 string 代理ID  
	* @param account 必選 string 玩家帳號
	* @param sign 必選 string md5加密串
	* @param time 必選 int 時間搓  
	* @return {"Code":0,"Message":"SUCCESS","values":{"account":"testuser","balance":"8000.00"}}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON物件
	* @return_param values.account string 使用者帳號
	* @return_param values.balance decimal(10,2) 餘額
	* @remark 
	* @number 
	*/
	//查詢玩家餘額
	public final function GetPlayerBalance(){
		$data = $_POST;
		$this->checkIP();
		$this->check_sign($data);
		$username = htmlspecialchars($data['account'],ENT_QUOTES);
		$user = $this->getUserInfo($username);
		$array = [
			'account' => $username,
			//'balance' => sprintf('%.02f',$user['coin'])
			'balance' => $this->floor_dec($user['coin'],2)	
		];
		$this->output(0,'SUCCESS', $array);
	}
	private function checkAccount($data,$agent){
		if (empty($data['account']) || strlen($data['account']) < 3) {
			$this->output(102,'Account小於3個字元');
		}
		$sql="select uid,username from {$this->db_prefix}members where username='".$data['account']."' limit 1";
		$user=$this->db->query($sql,2);
		if (!$user) {
			//default password
			$defaultFandian = floatval($this->config['defaultFandian']);
			$rebate = floatval($this->config['rebate']);
			$password = md5($this->password_key.'q1w2e3r4');
			$coinpwd = md5($this->password_key.'q1w2e3r4t5');
			$username = $data['account'];
			$this->db->transaction('begin');
			$para=array(
				'username'=>$username,
				'password'=>$password,
				'parentId'=>$agent['uid'],
				'coinPassword'=>$coinpwd,
				'coin'=>0,
				'regIP'=>$this->ip(true),
				'fanDian'=>$defaultFandian,
				'rebate'=>$rebate,
				'parents'=>$agent['uid'],
				'regTime'=>$this->time,
				'type'=>0,
			);
			$id = $this->db->insert($this->db_prefix .'members', $para);
			if($id){
				//$id = $this->lastInsertId();
				if(empty($para['parents'])){
					$sql="update {$this->db_prefix}members set parents='$id' where `uid`=$id";
				}
				else{
					$sql="update {$this->db_prefix}members set parents=concat(parents, ',', $id) where `uid`=$id";
				}
				$this->db->query($sql, 0);
				$this->db->transaction('commit');
			}
			else {
				$this->db->transaction('rollBack');
				$this->output(999,'註冊會員失敗，請連絡管理員');
			}
			return $para;
		}else
		{
			return $user;
		}
		
	}

	/**
	* showdoc
	* @catalog 帳號
	* @title 玩家儲值
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/PlayerDeposit
	* @param agentID 必選 string 代理ID  
	* @param account 必選 string 玩家帳號
	* @param sign 必選 string md5加密串
	* @param time 必選 int 時間搓  
	* @param amount 必選 int 金額  
	* @param order_no 必選 string 客戶交易編號  				
	* @return {"Code":0,"Message":"SUCCESS","values":{"account":"testuser","BeforeTransactionBalance":"8100.00","AfterTransactionBalance":8200,"order_no":"20191005123457","TransactionNo":"201910050000002"}}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON物件
	* @return_param values.account string 使用者帳號
	* @return_param values.BeforeTransactionBalance decimal(10,2) 轉入前餘額
	* @return_param values.AfterTransactionBalance decimal(10,2) 轉入後餘額
	* @return_param values.order_no string 客戶交易編號
	* @return_param values.TransactionNo string 平臺交易編號
	* @remark order_no不能重複,提款存款交易編號也不能重複
	* @number 
	*/
	//玩家儲值
	public final function PlayerDeposit(){
		$json = $_POST;
		$this->checkIP();
		$this->check_sign($json);
		$username = htmlspecialchars($json['account'],ENT_QUOTES);
		$order_no = htmlspecialchars($json['order_no'],ENT_QUOTES);
		
		$user = $this->getUserInfo($username);
		$amount=floatval($json['amount']);
		//if (!is_numeric($amount)) {
		//	$this->output(103,'Amount只能是數字');
		//}
		if ($amount <=0) {
			$this->output(106,'amount只能大於0');
		}
		if(!strlen($order_no)) {
			$this->output(107,'order_no必需有值');
		}

		$result = $this->db->query("select count(*) as total from {$this->db_prefix}member_recharge where comment = '{$order_no}'", 2);
		if ($result['total']) {
			$this->output(108,$order_no."交易編號已存在");
		}
		/*
		$result = $this->db->query("select count(*) as total from {$this->db_prefix}member_cash where order_no = ?", $order_no);
		if ($result['total']) {
			$this->output(108,$order_no."交易編號已存在");
		}
		*/

		$this->time = time();
		$data=array(
			'amount'=>$amount,
			'actionUid'=>0,
			'actionIP'=>$this->ip(true),
			'actionTime'=>$this->time,
			'rechargeTime'=>$this->time,
			'comment'=>$order_no
		);

        $liqType = 88;
        $info = '點數轉入平臺';
        $result = $this->db->getRow("call pre_make_key(@orederKey)");
        $data['rechargeId'] = $result['_key'];	
		$this->db->transaction('begin');
		try{
		
			$data['uid']=$user['uid'];
			$data['coin']=$user['coin'];
			//$data['fcoin']=$user['fcoin'];
			$data['username']=$user['username'];
			$data['info']=$info;
			$data['state']=1;

			$id=$this->db->insert($this->db_prefix . 'member_recharge', $data);
			if($id)
			{
	            $this->set_coin(array(
	                'uid' => $user['uid'],
	                'liqType' => $liqType,
					'coin' => $data['amount'],
					'extfield0' => $id,
	                'extfield1' => $data['rechargeId'],
	                'extfield2' => $data['comment'],
	                'info' => $info
	            ));
			}

	        // if ($this->db->_insertRow($this->db_prefix . 'member_recharge', $data)) {
	        //     $dataId = $this->db->lastInsertId();
	        //     $this->addCoin(array(
	        //         'uid' => $user['uid'],
	        //         'liqType' => $liqType,
	        //         'coin' => $data['amount'],
	        //         'extfield0' => $dataId,
	        //         'extfield1' => $data['rechargeId'],
	        //         'extfield2' => $data['comment'],
	        //         'info' => $info
	        //     ));
	        // }
	
			$this->db->transaction('commit');
		}catch(Exception $e){
			$this->db->transaction('rollBack');
			$this->output(109,'點數轉入平臺失敗');
		}

		$array = [
			'account' => $username,
			'BeforeTransactionBalance' => $data['coin'],
			'AfterTransactionBalance' => $data['coin'] + $amount,
			'order_no' => $order_no,
			'TransactionNo' => $data['rechargeId'],
		];		

		$this->output(0,'SUCCESS',$array);
	}

	/**
	* showdoc
	* @catalog 帳號
	* @title 玩家提款
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/PlayerWithdrawal
	* @param agentID 必選 string 代理ID  
	* @param account 必選 string 玩家帳號
	* @param sign 必選 string md5加密串
	* @param time 必選 int 時間搓  
	* @param amount 必選 int 金額  
	* @param order_no 必選 string 客戶交易編號  		
	* @return {"Code":0,"Message":"SUCCESS","values":{"account":"testuser","BeforeTransactionBalance":"8100.00","AfterTransactionBalance":"7900.00","order_no":"20191005123459","TransactionNo":"123"}}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON物件
	* @return_param values.account string 使用者帳號
	* @return_param values.BeforeTransactionBalance decimal(10,2) 轉入前餘額
	* @return_param values.AfterTransactionBalance decimal(10,2) 轉入後餘額
	* @return_param values.order_no string 客戶交易編號
	* @return_param values.TransactionNo string 平臺交易編號
	* @remark order_no不能重複,提款存款交易編號也不能重複
	* @number 
	*/
	//玩家提款
	public final function PlayerWithdrawal(){
		$json = $_POST;
		$this->checkIP();
		$this->check_sign($json);
		$username = htmlspecialchars($json['account'],ENT_QUOTES);
		$order_no = htmlspecialchars($json['order_no'],ENT_QUOTES);

		$user = $this->getUserInfo($username);
		$amount=floatval($json['amount']);
		if ($amount <=0) {
			$this->output(104,'Amount只能大於0');
		}
		if(!strlen($order_no)) {
			$this->output(107,'order_no必需有值');
		}
		/*
		$result = $this->getRow("select count(*) as total from {$this->db_prefix}member_recharge where comment = ?", $order_no);
		if ($result['total']) {
			$this->output(108,$order_no."交易編號已存在");
		}
		*/
		$result = $this->db->getRow("select count(*) as total from {$this->db_prefix}member_cash where order_no = ?", $order_no);
		if ($result['total']) {
			$this->output(108,$order_no."交易編號已存在");
		}

		$this->time = time();
		$para['amount']=$amount;
		$para['username']=$username;
		$para['musername']=$username;
		$para['account']='1234567890';
		$para['order_no']=$order_no;
		$para['bankId']=1;
		if($para['amount'] > $user['coin']) {
			$this->output(150,'提款金額大於可用餘額，無法提款');
		}
		if($user['coin']<=0){
			$this->output(151,'可用餘額為零，無法提款');
		}
		
		$this->db->transaction('begin');
		try{
			// 插入提現請求表
			$para['actionTime']=$this->time;
			$para['uid']=$user['uid'];
			$para['state']=0;
			$id = $this->db->insert($this->db_prefix . 'member_cash', $para);
			if(!$id){
				$this->output(152,'提交提現請求出錯');
			}
			
			$log=array(
				'coin'=>0-$para['amount'],
				'uid'=>$para['uid'],
			);
			$log['info'] = "平臺轉出點數";
			$log['liqType']=89;
			$log['extfield0']=$id;
			$log['extfield2']=$order_no;

			$this->set_coin($log);

			$this->db->transaction('commit');
		}catch(Exception $e){
			$this->db->transaction('rollBack');
			$this->output(110,'平臺轉出點數失敗，請連絡管理員');
		}

		$array = [
			'account' => $username,
			'BeforeTransactionBalance' => $user['coin'],
			'AfterTransactionBalance' => sprintf('%.02f',$user['coin'] - $amount),
			'order_no' => $order_no,
			'TransactionNo' => $id,
		];		

		$this->output(0,'SUCCESS',$array);	
	}

	/**
	* showdoc
	* @catalog 帳號
	* @title 檢查轉入交易狀態
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/CheckTransactionInStatus
	* @param agentID 必選 string 代理ID  
	* @param sign 必選 string md5加密串
	* @param time 必選 int 時間搓  
	* @param order_no 必選 string 客戶交易編號  		
	* @return {"Code":0,"Message":"SUCCESS","values":{"TransactionStatus":"SUCCESSED","TransactionDateTime":"2019-10-05 11:56:31","order_no":"20191005123459","TransactionNo":"123"}}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON物件
	* @return_param values.TransactionStatus string 交易狀態
	* @return_param values.TransactionDateTime string 交易時間
	* @return_param values.order_no string 客戶交易編號
	* @return_param values.TransactionNo string 平臺交易編號
	* @remark 交易編號不存在時,返回{'Code':112,'Message':'交易編號不存在' }
	* @number 
	*/
	//檢查轉入交易狀態
	public final function CheckTransactionInStatus(){
		$json = $_POST;
		$this->checkIP();
		$this->check_sign($json);
		$order_no = htmlspecialchars($json['order_no'],ENT_QUOTES);
		$sql = "select * from {$this->db_prefix}member_recharge where comment=? limit 0,1";
		$recharge = $this->db->getRow($sql, [$order_no]);
		if (!$recharge ) {
			$this->output(112,'交易編號不存在');
		}
		if ($recharge) {
			$array = [
				'TransactionStatus' => 'SUCCESSED',
				'TransactionDateTime' => date('Y-m-d H:i:s',$recharge['rechargeTime']),
				// 'BeforeTransactionBalance' => $recharge['coin'],
				// 'AfterTransactionBalance' => $recharge['coin'] + $recharge['amount'],
				'order_no' => $order_no,
				'TransactionNo' => $recharge['rechargeId'],
			];
		}
		$this->output(0,'SUCCESS',$array);	
	}

	/**
	* showdoc
	* @catalog 帳號
	* @title 檢查轉出交易狀態
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/CheckTransactionOutStatus
	* @param agentID 必選 string 代理ID  
	* @param sign 必選 string md5加密串
	* @param time 必選 int 時間搓  
	* @param order_no 必選 string 客戶交易編號  		
	* @return {"Code":0,"Message":"SUCCESS","values":{"TransactionStatus":"SUCCESSED","TransactionDateTime":"2019-10-05 11:56:31","order_no":"20191005123459","TransactionNo":"123"}}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON物件
	* @return_param values.TransactionStatus string 交易狀態
	* @return_param values.TransactionDateTime string 交易時間
	* @return_param values.order_no string 客戶交易編號
	* @return_param values.TransactionNo string 平臺交易編號
	* @remark 交易編號不存在時,返回{'Code':112,'Message':'交易編號不存在' }
	* @number 
	*/
	//檢查轉出交易狀態
	public final function CheckTransactionOutStatus(){
		$json = $_POST;
		$this->checkIP();
		$this->check_sign($json);
		if(!isset($json['order_no']))
		{
			$this->output(112,'交易編號不存在');
		}
		$order_no = htmlspecialchars($json['order_no'],ENT_QUOTES);
		$sql = "select * from {$this->db_prefix}member_cash where order_no=? limit 0,1";
		$cash = $this->db->getRow($sql, [$order_no]);
		if ( !$cash) {
			$this->output(112,'交易編號不存在');
		}
		if ($cash) {
			// $sql = "select * from {$this->db_prefix}coin_log where liqType=89 and uid=? and extfield0=? and extfield2=?";
			// $coinlog = $this->getRow($sql, [$user['uid'],$cash['id'],$ReferenceNo]);
			$array = [
				'TransactionStatus' => 'SUCCESSED',
				'TransactionDateTime' => date('Y-m-d H:i:s',$cash['actionTime']),
				// 'BeforeTransactionBalance' => sprintf('%.02f',$coinlog['userCoin'] + $cash['amount']),
				// 'AfterTransactionBalance' => $coinlog['userCoin'] ,
				'order_no' => $order_no,
				'TransactionNo' => $cash['id'],
			];
		
		}
		$this->output(0,'SUCCESS',$array);	
	}

	/**
	* showdoc
	* @catalog 帳號
	* @title 註冊帳號
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/PlayerRegister
	* @param agentID 必選 string 代理ID  
	* @param account 必選 string 玩家帳號
	* @param sign 必選 string md5加密串
	* @param time 必選 int 時間搓  
	* @return {"Code":0,"Message":"SUCCESS","values":{"account":"testuser02"}}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON物件
	* @return_param values.account string 玩家帳號
	* @remark 若帳號不存在,自動註冊並返回帳號. 若帳號已存在直接返回帳號
	* @number 
	*/
	//註冊帳號
	public final function PlayerRegister(){
		global $conf;
		$json = $_POST;
		$this->checkIP();
		$this->check_sign($json);
		$agent = $this->checkAgent($json);
		$user = $this->checkAccount($json,$agent);
		$time = time();
		$data=[
			'account' =>$user['username'],
		];
		$this->output(0,'SUCCESS',$data);
	}

	private function getUserInfo($username) {
		$sql = "select * from {$this->db_prefix}members where isDelete=0 and username='{$username}' limit 0,1";
		$user = $this->db->query($sql, 2);
		if (!$user) {
			$this->output(105,'帳號不存在');
		}
		return $user;
	}

	/**
	* showdoc
	* @catalog 帳號
	* @title 獲取登錄URL
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/GetLoginUrl
	* @param agentID 必選 string 代理ID  
	* @param account 必選 string 玩家帳號
	* @param sign 必選 string md5加密串
	* @param time 必選 int 時間搓  
	* @param isMobile 選填 int 1:回傳手機版網址,0:回傳PC版網址.不輸入預設為PC版網址
	* @return {"Code":0,"Message":"SUCCESS","values":{"url":"google.com"}}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON
	* @return_param values.url string 登錄URL
	* @remark 若使用者第一次登錄自動註冊
	* @number 
	*/
	public final function GetLoginUrl(){
		global $conf;
		$json = $_POST;
		$this->checkIP();
		$this->check_sign($json);
		$agent=$this->checkAgent($json);
		$this->checkAccount($json,$agent);
		$time = time();
		$ApiUrl=$conf['ApiUrl_Login']."?";
		// if(isset($json['isMobile']))
		// {
		// 	if($json['isMobile']==1)
		// 	{
		// 		$ApiUrl=$conf['ApiUrl_MobileLogin']."?isMobile=1&";
		// 	}
		// }
		$sign=md5($json['agentID'].$this->KEY.$time);
		$url = $ApiUrl.'account='.$json['account'].'&agentID='.$json['agentID'].'&sign='.$sign."&time=".$time;
		/*
		if(isset($json['isMobile']))
		{
			if($json['isMobile']==1)
			{
				$url.="&isMobile=1";
			}
		}
		*/
		$data=[
			"url"=>$url
		];
		$this->output(0,'SUCCESS',$data);
		exit;
	}

	public final function login(){
		global $conf;
		if(!isset($_GET['account']) || !isset($_GET['agentID']) || !isset($_GET['sign']) || !isset($_GET['time']))
		{
			$this->output(100,'缺少必要參數');
		}
		$data=[
			"account"=>$_GET['account'],
			"agentID"=>$_GET['agentID'],
			"sign"=>$_GET['sign'],
			"time"=>$_GET['time'],
		];
		$this->check_sign($data);

		$username =htmlspecialchars($data['account'],ENT_QUOTES);
		//default password
		$password = md5($this->password_key.'q1w2e3r4');
		$coinpwd = md5($this->password_key.'q1w2e3r4t5');
		
		//if(!ctype_alnum($username)) $this->output(999,'使用者名稱包含非法字元,請重新登陸');

		$sql = "select * from {$this->db_prefix}members where enable=1 and isDelete=0 and username='{$username}' limit 0,1";
		$user = $this->db->query($sql, 2);
		if (!$user) {
			$this->output(104,'請重新登錄');
		}

		$session = array(
			'uid' => $user['uid'],
			'username' => $user['username'],
			'session_key' => session_id(),
			'loginTime' => $this->time,
			'accessTime' => $this->time,
			'loginIP' => $this->ip(true)
		);

		$session=array_merge($session, $this->getBrowser());
		$session_id = $this->db->insert($this->db_prefix . 'member_session', $session);
		if ($session_id) $user['sessionId'] = $session_id;
		$_SESSION[$this->user_session] = serialize($user);
		$uid = $user['uid'];
		$this->db->query("UPDATE `{$this->db_prefix}member_session` SET `isOnLine`=0,`state`=1 WHERE `uid`={$uid} AND `id`<{$session_id}", 0);
		$this->db->query("UPDATE `{$this->db_prefix}members` SET `updateTime`='".date('Y-m-d H:i:s',$this->time)."' WHERE `uid`={$uid}", 0);

		// if(isset($_GET['isMobile']))
		// {
		// 	header("refresh:0;url='".$conf['ApiUrl_Mobile_Return']."'");
		// }else
		// {
		// 	header("refresh:0;url='".$conf['ApiUrl_Return']."'");
		// }
		header("refresh:0;url='".$conf['ApiUrl_Return']."'");
		//$_SESSION['GameID'] = $token['GameID'];
		exit;
	}

	/**
	* showdoc
	* @catalog 遊戲
	* @title 檢查平臺是否維修
	* @description 請求格式為 application/x-www-form-urlencoded , 返回為json
	* @method post 
	* @url /api/service/getMaintain
	* @param agentID 必選 string 代理ID  
	* @param sign 必選 string md5加密串
	* @param time 必選 int 時間搓  
	* @return {"Code":0,"Message":"SUCCESS","values":{"state":"1"}}
	* @return_param Code int 回傳編號
	* @return_param Message string 說明
	* @return_param values string JSON物件
	* @return_param values.state string 1正常,0維修
	* @remark 
	* @number 
	*/
	public final function getMaintain() {
		global $conf;
		$json = $_POST;
		$this->checkIP();
		$this->check_sign($json);
		$data=[
			"state"=>$this->config['switchWeb']
		];
		$this->output(0,'SUCCESS',$data);
	}

	private function getBrowser(){
		$flag=$_SERVER['HTTP_USER_AGENT'];
		$para=array();
		
		// 檢查操作系統
		if(preg_match('/Windows[\d\. \w]*/',$flag, $match)) $para['os']=$match[0];
		
		if(preg_match('/Chrome\/[\d\.\w]*/',$flag, $match)){
			// 檢查Chrome
			$para['browser']=$match[0];
		}elseif(preg_match('/Safari\/[\d\.\w]*/',$flag, $match)){
			// 檢查Safari
			$para['browser']=$match[0];
		}elseif(preg_match('/MSIE [\d\.\w]*/',$flag, $match)){
			// IE
			$para['browser']=$match[0];
		}elseif(preg_match('/Opera\/[\d\.\w]*/',$flag, $match)){
			// opera
			$para['browser']=$match[0];
		}elseif(preg_match('/Firefox\/[\d\.\w]*/',$flag, $match)){
			// Firefox
			$para['browser']=$match[0];
		}elseif(preg_match('/OmniWeb\/(v*)([^\s|;]+)/i',$flag, $match)){
			//OmniWeb
			$para['browser']=$match[2];
		}elseif(preg_match('/Netscape([\d]*)\/([^\s]+)/i',$flag, $match)){
			//Netscape
			$para['browser']=$match[2];
		}elseif(preg_match('/Lynx\/([^\s]+)/i',$flag, $match)){
			//Lynx
			$para['browser']=$match[1];
		}elseif(preg_match('/360SE/i',$flag, $match)){
			//360SE
			$para['browser']='360安全瀏覽器';
		}elseif(preg_match('/SE 2.x/i',$flag, $match)) {
			//搜狗
			$para['browser']='搜狗瀏覽器';
		}else{
			$para['browser']='unkown';
		}
		//print_r($para);exit;
		return $para;
	}
}

?>