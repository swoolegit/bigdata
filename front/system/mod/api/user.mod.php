<?php

class mod_user extends mod
{
    public function __construct()
    {
        //$this->user_check = false;
        parent::__construct();
    }
    public function egame_start()
    {
        $id=array_key_exists('id', $_POST) ? $_POST['id'] : '';
        $vendor=array_key_exists('vendor', $_POST) ? $_POST['vendor'] : '';
        if(empty($id))
        {
            core::json_err('请选择游戏');
        }
        $url=core::lib('vendor')->StartGame($vendor,$id,$this->user['username']);
        core::json([
            "status" =>1,
            "data" =>$url,
        ]);
        return;
    }
    public function egame_coin()
    {
        $id=array_key_exists('id', $_POST) ? $_POST['id'] : '';
        $vendor=array_key_exists('vendor', $_POST) ? $_POST['vendor'] : '';
        if(empty($id))
        {
            core::json_err('请选择游戏');
        }
        $amount=core::lib('vendor')->getBalance($vendor,$id,$this->user['username']);
        core::json([
            "status" =>1,
            "data" =>$amount,
        ]);
        return;
    }
    public function get_coin()
    {
        $this->check_post();
        //$this->user_check_func();
        $this->fresh_user_session();
        $uid = $this->user['uid'];
        $score = $this->user['scoreTotal'];
        if(!$this->user['forTest'])
        {
            $new_grade = $this->db->query("SELECT MAX(`level`) AS `value` from `{$this->db_prefix}member_level` WHERE `minScore` <= {$score} LIMIT 1", 2);
            $new_grade = $new_grade['value'];
            if ($new_grade > $this->user['grade']) {
                $sql = "UPDATE `{$this->db_prefix}members` SET `grade`={$new_grade} WHERE `uid`=$uid LIMIT 1";
                $this->db->query($sql, 0);
                $this->user['grade'] = $new_grade;
            }    
        }
        core::json([
            "data" => [
                'uid'=>$this->user['uid'],
                'username'=>$this->user['username'],
                'type'=>$this->user['type'],
                'coin'=> $this->user['coin'],
                'score'=> $this->user['score'],
                'grade'=> $this->user['grade'],
                'fanDian'=> $this->user['fanDian'],
                'forTest'=> $this->user['forTest'],
            ]
        ]);
    }

    public function coin()
    {
        $this->check_post();
        $uid=$this->user['uid'];
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $_type = core::lib('validate')->number($_POST['type']) ? ($_POST['type']) : 0;
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $where="";
        if(!empty($_type))
        {
            $where.=" and liqType = ".$_type;
        }

        $this->get_time(false);
        $where .= $this->build_where_time('`actionTime`');

        $table="{$this->db_prefix}coin_log_temp".($uid%10);
        if($this->user['forTest']==1)
        {
            $table="{$this->db_prefix}guest_coin_log";
        }
        $sql="select id,liqType,coin,userCoin,info,actionTime as actionTime
                from `{$table}`  where uid={$uid} {$where} order by actionTime desc LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        core::json([
            "status" =>1,
            "data" =>$data,
        ]);
        return;
    }

    public function bet_log()
    {
        $this->check_post();
        $uid=$this->user['uid'];
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $_type = core::lib('validate')->number($_POST['type']) ? ($_POST['type']) : 0;
        $_status = core::lib('validate')->number($_POST['status']) ? ($_POST['status']) : 0;
        
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $where="";
        if(!empty($_type))
        {
            $where.=" and type = ".$_type;
        }
        if(!empty($_status))
        {
            switch($_status)
            {
                case 1:
                    $where.=" and lotteryNo <> '' and isDelete = 0";
                break;
                case 2:
                    $where.=" and lotteryNo = '' ";
                break;
                case 3:
                    $where.=" and isDelete = 1 ";
                break;
            }
        }

        $this->get_time(false);
        $where .= $this->build_where_time('`actionTime`');
        $table="{$this->db_prefix}bets_temp".($uid%10);
        if($this->user['forTest']==1)
        {
            $table="{$this->db_prefix}guest_bets";
        }
        $sql="select id,wjorderId,type,actionNo,actionTime as actionTime
                ,actionData,bonus,actionAmount,actionName,betInfo,lotteryNo,playedId,weiShu
                ,kjTime as kjTime,isDelete
                from `{$table}`  where uid={$uid} {$where} order by actionTime desc LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        core::json([
            "status" =>1,
            "data" =>$data,
        ]);
        return;
    }

    public function ebet_log()
    {
        $this->check_post();
        $uid=$this->user['uid'];
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        // $_type = core::lib('validate')->number($_POST['type']) ? ($_POST['type']) : 0;
        // $_status = core::lib('validate')->number($_POST['status']) ? ($_POST['status']) : 0;
        
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $where="";
        // if(!empty($_type))
        // {
        //     $where.=" and type = ".$_type;
        // }
        // if(!empty($_status))
        // {
        //     switch($_status)
        //     {
        //         case 1:
        //             $where.=" and lotteryNo <> '' and isDelete = 0";
        //         break;
        //         case 2:
        //             $where.=" and lotteryNo = '' ";
        //         break;
        //         case 3:
        //             $where.=" and isDelete = 1 ";
        //         break;
        //     }
        // }

        $file=md5(__FUNCTION__);
        $file= $this->getCacheDir().$file;
		if(BPCACHE_TIME && is_file($file) && filemtime($file)+BPCACHE_TIME>$this->time)
		{
			$egame= unserialize(file_get_contents($file));
		}else
        {
            $sql = "SELECT `game_type`,`cname`,`ename` FROM `{$this->db_prefix}egame` WHERE `enable`=1 ";
            $egame=$this->db->query($sql, 3);
            $data=[];
            foreach($egame as $k=>$v)
            {
                $data[$v['game_type']] = $v['cname'];
            }
            file_put_contents($file, serialize($data));
            $egame = $data;
        }

        $this->get_time(false);
        $where .= $this->build_where_time('`actionTime`');
        //$table="{$this->db_prefix}bets_temp".($uid%10);
        $table="{$this->db_prefix}egame_bet";
        $sql="select gameType,eventTime as actionTime,roundId,amount,status
                from `{$table}`  where uid={$uid} {$where} order by actionTime desc LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        $_status=[
            0=>"结算",
            1=>"投注",
            2=>"失败",
            3=>"退款",
        ];
        foreach($data as $k=>$v)
        {
            $data[$k]['gameName'] = $egame[$v['gameType']];
            $data[$k]['status'] = $_status[$v['status']];
        }
        core::json([
            "status" =>1,
            "data" =>$data,
        ]);
        return;
    }

    public function bank()
    {
        $sql="select id,name,ename from `{$this->db_prefix}bank_list` where isDelete=0 and for_cash=1 and for_recharge=0 and area='".AREA."'";
        $data = $this->db->query($sql, 3);
        core::json([
            "status" =>1,
            "data" =>$data,
        ]);
        return;
    }

    public function recharge()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
        $sql="select id,bankselect,username,userbankid,remark,qrcode from `{$this->db_prefix}params_atr` where 
                (enable=1 and (lvmin=0 and lvmax=0)) or 
                (enable=1 and
                    ({$this->user['grade']} >=lvmin and {$this->user['grade']} <=lvmax)
                )";
        $bank = $this->db->query($sql, 3);

        $sql="select id,area from `{$this->db_prefix}params_thirdpay` where 
                (
                    payin_status=1 and ((lvmin=0 and lvmax=0)
                )
                or 
                (
                    payin_status=1 and
                    ({$this->user['grade']} >=lvmin and {$this->user['grade']} <=lvmax))
                )    
                order by lvmin desc limit 1";
        $third = $this->db->query($sql, 2);
        switch(true)
        {
            case $third['id']==1 || $third['id']==6:
                $sql="select id,name,ename as code from `{$this->db_prefix}bank_list` where ename<>'' and for_recharge=1 and area = '{$third['area']}' and isDelete = 0";
            break;
            case $third['id']==2:
                $sql="select id,name,codeid as code from `{$this->db_prefix}bank_list` where codeid<>'' and for_recharge=1 and area = '{$third['area']}' and isDelete = 0";
            break;
            case $third['id']==3:
                $sql="select id,name,codeid as code from `{$this->db_prefix}bank_list` where codeid<>'' and for_recharge=1 and area = '{$third['area']}' and isDelete = 0";
            break;
            case $third['id']==4:
                $sql="select id,name,codeid as code from `{$this->db_prefix}bank_list` where codeid<>'' and for_recharge=1 and area = '{$third['area']}' and isDelete = 0";
            break;
            case $third['id']==5:
                $sql="select id,name,ename as code from `{$this->db_prefix}bank_list` where ename<>'' and for_recharge=1 and area = '{$third['area']}' and isDelete = 0";
            break;
        }
        $third_data = $this->db->query($sql, 3);

        core::json([
            "status" =>1,
            "data" =>[
                "bank"=>$bank,
                "third"=>$third_data,
                "third_id"=>$third['id'],
            ],
        ]);
        return;
    }

    public function recharge_post()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
        $this->check_post();
        $bankid = intval(trim($_POST['bankid']));  //will checked
        $amount = intval(trim($_POST['amount']));  //will checked
        $rechargeType = intval(trim($_POST['rechargeType']));  //will checked
        $memo = trim($_POST['memo']);  //will checked
        if(empty($memo))
        {
            core::json_err('缺少附言，请重新操作');
        }
        if ($amount <= 0) core::json_err('充值金额错误，请重新操作');
        $bank = $this->db->getRow("SELECT `id` FROM `{$this->db_prefix}params_atr` WHERE `id`=? and enable=1 LIMIT 1", $bankid);
        if (!$bank) core::json_err('渠道不存在，请重新选择');

		if ($amount < $this->config['rechargeMin']) core::json_err('充值金额最低为' . $this->config['rechargeMin'] . '元');
		if ($amount > $this->config['rechargeMax']) core::json_err('充值金额最高为' . $this->config['rechargeMax'] . '元');

        $orderid = $this->get_orderid();
        $id = $this->db->_insertRow($this->db_prefix . 'member_recharge', array(
            'uid' => $this->user['uid'],
            'rechargeId' => $orderid,
            'username' => $this->user['username'],
            'amount' => $amount,
            //'bankId' => $bankid,
            'actionIP' => $this->ip(true),
            'actionTime' => $this->time,
            'rechargeModel' =>0,
            'rechargeComID' =>$bankid,
            'rechargeType'=>$rechargeType,
            'info' => $memo,
        ));
        if (!$id) core::json_err('提交记录到数据库失败，请重试');
        core::json([
            "status" =>1,
            "data" =>[
                "rechargeId"=>$orderid,
            ],
        ]);
        return;
    }
    public function third_recharge_post()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
        $this->check_post();
        $code =trim($_POST['code']);  //will checked
        $amount = intval(trim($_POST['amount']));  //will checked
        $rechargeType = intval(trim($_POST['rechargeType']));  //will checked
        $thirdID = intval(trim($_POST['thirdID']));  //will checked
        
        if ($amount <= 0) core::json_err('充值金额错误，请重新操作');
        if (empty($code)) core::json_err('渠道不存在，请重新选择');

        $sql="select id,merid,addcrykey from `{$this->db_prefix}params_thirdpay` where payin_status=1 and id=".$thirdID;
        $third = $this->db->query($sql, 2);
        switch(true)
        {
            case $third['id']==1 || $third['id']==6:
                $sql="select id,name,ename as code from `{$this->db_prefix}bank_list` where ename<>''";
            break;
            case $third['id']==2:
                $sql="select id,name,codeid as code from `{$this->db_prefix}bank_list` where codeid<>''";
            break;
            case $third['id']==3:
                $sql="select id,name,codeid as code from `{$this->db_prefix}bank_list` where codeid<>''";
            break;
            case $third['id']==4:
                $sql="select id,name,codeid as code from `{$this->db_prefix}bank_list` where codeid<>''";
            break;
            case $third['id']==5:
                $sql="select id,name,ename as code from `{$this->db_prefix}bank_list` where ename='{$code}'";
            break;
        }
        $third_data = $this->db->query($sql, 2);
        if (!$third_data) core::json_err('渠道不存在，请重新选择');
        if ($amount <= 0) core::json_err('充值金额错误，请重新操作');

		if ($amount < $this->config['rechargeMin']) core::json_err('充值金额最低为' . $this->config['rechargeMin'] . '元');
        if ($amount > $this->config['rechargeMax']) core::json_err('充值金额最高为' . $this->config['rechargeMax'] . '元');
        // $third_data=[
        //     'id'=>$third['id'],
        //     'merid'=>$third['merid'],

        // ];
        $orderid = $this->get_orderid();
        $third=array_merge($third,[
            'code'=>$code,
            'order_no'=>$orderid,
            'order_amount'=>$amount,
        ]);
        $url=$this->get_third_url($third);
        $id = $this->db->_insertRow($this->db_prefix . 'member_recharge', array(
            'uid' => $this->user['uid'],
            'rechargeId' => $orderid,
            'username' => $this->user['username'],
            'amount' => $amount,
            'actionIP' => $this->ip(true),
            'actionTime' => $this->time,
            'rechargeModel' =>1,
            'rechargeComID' =>$third['id'],
            'rechargeType'=>$rechargeType,
        ));
        if (!$id) core::json_err('提交记录到数据库失败，请重试');
        core::json([
            "status" =>1,
            "data" =>[
                "rechargeId"=>$orderid,
                "url"=>$url,
            ],
        ]);
        return;
    }
    private function get_third_url($third)
    {
        switch(true)
        {
            case $third['id']==1:
                break;
            case $third['id']==2:
                break;
            case $third['id']==3:
                break;
            case $third['id']==4:
                break;
            case $third['id']==5: //艾付接口
                $version = "v1";									//接口版本
                $merchant_no = $third['merid'];						//商户号
                $order_no = $third['order_no'];									//商户订单号
                $goods_name = "充值";								//商品名称
                $order_amount = $third['order_amount'];								//订单金额
                $backend_url = PAY_CALLBACK."ifeepay_payin_call.php";									//支付结果异步通知地址
                $frontend_url = "";									//支付结果同步通知地址
                $reserve = "";										//商户保留信息
                $pay_mode = "01";									//支付模式
                $bank_code = $third['code'];									//银行编号(需先调用获取网关银行列表取得银行编号) 范例:ABC为农业银行 . 备注:少部分银行不支持境外IP,若出现风控受限信息请更换中国境内IP
                $card_type = "0";									//允许支付的银行卡类型
                $goods_name = base64_encode($goods_name);			//Base64编码
                $key = $third['addcrykey'];		//商户接口秘钥
            
                //MD5签名
                $src = "version=" . $version . "&merchant_no=" . $merchant_no . "&order_no="
                        . $order_no . "&goods_name=" . $goods_name . "&order_amount=" . $order_amount
                        . "&backend_url=" . $backend_url . "&frontend_url="
                        . $frontend_url . "&reserve=" . $reserve
                        . "&pay_mode=" . $pay_mode . "&bank_code=" . $bank_code . "&card_type="
                        . $card_type;
                $src .= "&key=" . $key;
                $sign = md5($src);
                //接口地址
                $data=[
                    "version"=>$version,
                    "merchant_no"=>$merchant_no,
                    "order_no"=>$order_no,
                    "goods_name"=>$goods_name,
                    "order_amount"=>$order_amount,
                    "backend_url"=>$backend_url,
                    "frontend_url"=>$frontend_url,
                    "reserve"=>$reserve,
                    "pay_mode"=>$pay_mode,
                    "bank_code"=>$bank_code,
                    "card_type"=>$card_type,
                    "sign"=>$sign,
                ];
                $url = "https://pay.ifeepay.com/gateway/pay.jsp?".http_build_query($data);            
                break;
            case $third['id']==6: //台灣付款
                // 檢查碼, 計算方式為: MD5 Salt , ORDER_ID , ORDER_AMOUNT, PAY_TYPE 四個字串以 "$" 為間隔所組成的字串,
                // 做 MD5 後取 hex 值小寫格式
                //echo md5("72d63a59482f325df185b902398bef5e"."$"."201911260000024"."$"."100"."$"."7-11");
                //$sign = strtolower($this->strToHex(md5($third['addcrykey']."$".$third['order_no']."$".$third['order_amount']."$".$third['code'])));
                $sign = md5($third['addcrykey']."$".$third['order_no']."$".$third['order_amount']."$".$third['code']);
                $data=[
                    "ORDER_ID"=>$third['order_no'],
                    //"ORDER_ID"=>"201911260000024",
                    "ORDER_AMOUNT"=>$third['order_amount'],
                    "ORDER_ITEM"=>"遊戲幣點數",
                    "PAY_TYPE"=>$third['code'],
                    "CALLBACK_URL"=>PAY_CALLBACK."usdt_callback.php",
                    "CHECK_CODE"=>$sign,
                    //"CHECK_CODE"=>"edb401942a9344969fdf576344095ad1",
                    "INV_DONATE"=>1,
                    "INV_NAME"=>'',
                    "INV_ADDR"=>'',
                    "INV_EMAIL"=>'hrj@hrj101.com',
                ];
                //print_r($data);
                $url = "https://www.buyusdt.cx/payment/ppredirect?".http_build_query($data);
                $data = http_build_query($data);
                $url = "https://www.buyusdt.cx/payment/ppredirect";
                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); //执行以后返回文件流，而不直接输出
                curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded","Payment-Token:".$third['merid']));
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                //curl_setopt($curl, CURLOPT_TIMEOUT,5);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                $res = curl_exec($curl); //执行请求
                curl_close($curl); //关闭资源
                //echo $res."<br>";
                $r=json_decode($res,true);
                //print_r($r);
                $file= $this->getCacheDir().$sign;
                file_put_contents($file, $r['Data']);
                $url = $sign;
                //$url = "c8d6f067548c469d3fd0f65093c2cf5b";
                break;
        }
        return $url;
    }
    public function recharge_log()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
        $this->check_post();
        $uid=$this->user['uid'];
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $where="";

        $this->get_time(false);
        $where .= $this->build_where_time('`actionTime`');

        $table="{$this->db_prefix}member_recharge";
        $sql="select id,amount,actionTime as actionTime,state
                ,rechargeId,rechargeModel,rechargeComID,info,rechargeType
                ,rechargeTime
                from `{$table}` where uid={$uid} {$where} order by actionTime desc LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        $data_array=[];
        if($data)
        {
            $sql="select id,bankselect from {$this->db_prefix}params_atr";
            $atr= $this->db->query($sql, 3);
            $atr_array=[];
            foreach($atr as $k=>$v)
            {
                $atr_array[$v['id']]=$v['bankselect'];
            }
            $sql="select id,name from {$this->db_prefix}params_thirdpay";
            $thirdpay= $this->db->query($sql, 3);
            $thirdpay_array=[];
            foreach($thirdpay as $k=>$v)
            {
                $thirdpay_array[$v['id']]=$v['name'];
            }
            foreach($data as $k=>$v)
            {
                $rechargeModel="";
                switch($v['rechargeModel'])
                {
                    case 0:
                        $rechargeModel="公司汇款";
                    break;
                    case 1:
                        $rechargeModel="三方充值";
                    break;
                    case 2:
                        //$rechargeModel="管理员充值";
                        $rechargeModel="在线充值";
                    break;
                }
                $rechargeComID="";
                switch($v['rechargeModel'])
                {
                    case 0:
                        $rechargeComID=$atr_array[$v['rechargeComID']];
                    break;
                    case 1:
                        $rechargeComID=$thirdpay_array[$v['rechargeComID']];
                    break;
                    case 2:
                        //$rechargeComID="管理员充值";
                        $rechargeComID="在线充值";
                    break;
                }

                $data_array[]=[
                    'id'=>$v['id'],
                    'amount'=>$v['amount'],
                    'actionTime'=>$v['actionTime'],
                    'state'=>$v['state'],
                    'rechargeId'=>$v['rechargeId'],
                    'rechargeModel'=>$rechargeModel,
                    'rechargeComID'=>$rechargeComID,
                    'info'=>$v['info'],
                    'clientType'=>$v['rechargeType'],
                    'rechargeTime'=>(!empty($v['rechargeTime']))?$v['rechargeTime']:'',
                ];
            }
        }
        core::json([
            "status" =>1,
            "data" =>$data_array,
        ]);
        return;
    }

    private function get_orderid()
    {
		$result = $this->db->getRow("call pre_make_key(@orederKey)");
		return $result['_key'];
    }

    public function safe()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }        
        $uid=$this->user['uid'];
        $sql="select coinPassword,phone,email from `{$this->db_prefix}members` where uid={$uid} and enable=1";
        $data = $this->db->query($sql, 2);
        if(!$data)core::json_err('请注册');

        $safe=[
            'coinPassword'=>empty($data['coinPassword'])?false:true,
            'phone'=>empty($data['phone'])?'':$data['phone'],
            'email'=>empty($data['email'])?'':$data['email'],
            'bank'=>'',
            'bank_username'=>'',
            'bank_account'=>'',
            'bank_id'=>'',
        ];

        $sql="select bankId,account,username,countname from `{$this->db_prefix}member_bank` where uid={$uid} and enable=1";
        $data = $this->db->query($sql, 2);
        if($data)
        {
            $safe['bank_username']=$data['username'];
            $account=substr($data['account'],-5);
            $safe['bank_account']=$account;
            $safe['bank_id']=$data['bankId'];
            // $sql="select name from `{$this->db_prefix}bank_list` where id={$data['bankId']}";
            // $data = $this->db->query($sql, 2);
            $safe['bank']=$data['countname']."(".$account.")";
        }
        if(!empty($safe['phone']))
        {
            $safe['phone']=substr($safe['phone'],0,3)."****".substr($safe['phone'],-4);
        }
        core::json([
            "status" =>1,
            "data" =>$safe,
        ]);
        return;
    }

    public function safe_up()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
        $uid=$this->user['uid'];
        if(empty($_POST['type']))core::json_err('错误操作');
        $_type = core::lib('validate')->number($_POST['type']) ? ($_POST['type']) : 0;
        switch($_type)
        {
            case 1:
                if(empty($_POST['password']))core::json_err('请输入登录密码');
                if(empty($_POST['new_password']))core::json_err('请输入新登录密码');
                if(strlen($_POST['password'])<4)core::json_err('密码不能小于4位');
                if(strlen($_POST['new_password'])<4)core::json_err('密码不能小于4位');
                $sql="select password from `{$this->db_prefix}members` where uid={$uid} and enable=1";
                $data = $this->db->query($sql, 2);
                if(!$data)core::json_err('请注册');
                if($data['password']!=md5($_POST['password'].$this->password_key))
                {
                    core::json_err('原密码错误');
                }
                $password=md5($_POST['new_password'].$this->password_key);
                $sql="update `{$this->db_prefix}members` set password='".$password."' where uid={$uid}";
                $this->db->query($sql,0);
                core::json([
                    "status" =>1,
                    "msg" =>"修改成功",
                ]);
                return;
            break;
            case 2:
                if(empty($_POST['coinpassword']))core::json_err('请输入提款密码');
                if(strlen($_POST['coinpassword'])<4)core::json_err('密码不能小于4位');
                if(strlen($_POST['confirm_coinpassword'])<4)core::json_err('密码不能小于4位');
                $sql="select coinpassword from `{$this->db_prefix}members` where uid={$uid} and enable=1";
                $data = $this->db->query($sql, 2);
                if(!$data)core::json_err('请注册');
                $password=md5($_POST['coinpassword'].$this->password_key);
                if(empty($data['coinpassword']))
                {
                    if($_POST['confirm_coinpassword']!=$_POST['coinpassword'])core::json_err('密码与确认密码不一致');
                    $sql="update `{$this->db_prefix}members` set coinpassword='".$password."' where uid={$uid}";
                    $this->db->query($sql,0);
                    core::json([
                        "status" =>1,
                        "msg" =>"修改成功",
                    ]);
                    return;
                }
                if(empty($_POST['confirm_coinpassword']))core::json_err('请输入新提款密码');
                if($data['coinpassword']!=md5($_POST['coinpassword'].$this->password_key))
                {
                    core::json_err('原密码错误');
                }
                $password=md5($_POST['confirm_coinpassword'].$this->password_key);
                $sql="update `{$this->db_prefix}members` set coinpassword='".$password."' where uid={$uid}";
                $this->db->query($sql,0);
                core::json([
                    "status" =>1,
                    "msg" =>"修改成功",
                ]);
                return;
            break;
            case 3:
                if(empty($_POST['phone']))core::json_err('请输入手机号');
                $phone=$_POST['phone'];
                if(!preg_match("/^1[3456789]\d{9}$/", $phone)){
                    core::json_err('手机号码格式不正确');
                }

                $sql="select phone from `{$this->db_prefix}members` where uid={$uid} and enable=1";
                $data = $this->db->query($sql, 2);
                if(!$data)core::json_err('请注册');
                if(!empty($data['phone']))core::json_err('请联系客服重置手机号');
                $sql="update `{$this->db_prefix}members` set phone='".$phone."' where uid={$uid}";
                $this->db->query($sql,0);
                core::json([
                    "status" =>1,
                    "msg" =>"修改成功",
                ]);
                return;
            break;
            case 4:
                if(empty($_POST['email']))core::json_err('请输入邮箱');
                $email=$_POST['email'];
                if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email)) {
                    core::json_err('邮箱格式不正确');
                }
                $sql="select email from `{$this->db_prefix}members` where uid={$uid} and enable=1";
                $data = $this->db->query($sql, 2);
                if(!$data)core::json_err('请注册');
                if(!empty($data['email']))core::json_err('请联系客服重置邮箱');
                $sql="update `{$this->db_prefix}members` set email='".$email."' where uid={$uid}";
                $this->db->query($sql,0);
                core::json([
                    "status" =>1,
                    "msg" =>"修改成功",
                ]);
                return;
            break;
            case 5:
                $bank_id = core::lib('validate')->number($_POST['bank']) ? ($_POST['bank']) : 0;
                if(empty($_POST['username']))core::json_err('请输入姓名');
                if(empty($_POST['account']))core::json_err('请输入卡号');
                if(empty($bank_id))core::json_err('请输入开户行');

                $account = htmlspecialchars($_POST['account'], ENT_QUOTES);
                $username = htmlspecialchars($_POST['username'], ENT_QUOTES);

                $uid = $this->user['uid'];
                $bank_me = $this->db->query("SELECT `id` FROM `{$this->db_prefix}member_bank` WHERE `uid`=$uid and enable=1  LIMIT 1", 2);
                if($bank_me)core::json_err('请联系客服重置银行卡');
    
                $bank_same = $this->db->query("SELECT `id` FROM `{$this->db_prefix}member_bank` WHERE `bankId`={$bank_id} AND `account`='{$account}'", 2);
                if($bank_me)core::json_err('[银行账户]已存在');

                $bank_name = $this->db->query("SELECT `name` FROM `{$this->db_prefix}bank_list` WHERE `id`={$bank_id} and isDelete=0 and for_cash=1", 2);
                if(!$bank_name)core::json_err('[银行]不存在');

                $id = $this->db->insert($this->db_prefix . 'member_bank', array(
                    'uid' => $uid,
                    'enable' => 1,
                    'bankId' => $bank_id,
                    'username' => $username,
                    'account' => $account,
                    'countname' => $bank_name['name'],
                    'bdtime' => time(),
                ));
                if (!$id) core::json_err('更新银行账户到数据库失败，请重试');
                $text = '设置银行账户成功';
                if ($this->config['huoDongRegister']) {
                    $bank_check = $this->db->query("SELECT `id` FROM `{$this->db_prefix}member_bank` WHERE `uid`=$uid and enable=0  LIMIT 1", 2);
                    if(!$bank_check)
                    {
                        $this->db->transaction('begin');
                        try {
                            $this->set_coin(array(
                                'uid' => $this->user['uid'],
                                'type' => 0,
                                'liqType' => 51,
                                'info' => '绑定银行奖励',
                                'extfield0' => 0,
                                'extfield1' => 0,
                                'coin' => $this->config['huoDongRegister'],
                            ));
                            $this->db->transaction('commit');
                            $text = '设置银行账户成功，系统赠送您' . $this->config['huoDongRegister'] . '元';
                        } catch (Exception $e) {
                            $this->db->transaction('rollBack');
                            core::json_err($e->getMessage());
                        }
                    }
                }
                core::json([
                    "status" =>1,
                    "msg" =>$text,
                ]);
                return;
            break;
        }
    }
    public function logout()
    {
         unset($_SESSION[$this->user_session]);
         if ($this->user && array_key_exists('uid', $this->user)) {
             $uid = $this->user['uid'];
             $sql="UPDATE `{$this->db_prefix}member_session` SET `isOnLine`=0 WHERE `uid`={$uid}";
             $this->db->query($sql, 0);
        }
        core::json([
            "status" =>1,
            "msg" =>"您已安全退出，欢迎再次光临",
        ]);
        return;
    }
    public function check_letter()
    {
        if($this->user['forTest']==0)
        {
            //core::json_err('试玩帐号禁止使用');
            $uid=$this->user['uid'];
            $sql = "SELECT count(id) as id FROM `{$this->db_prefix}message_receiver` WHERE `is_deleted`=0 and to_uid={$uid} and is_readed=0  ";
            $data = $this->db->query($sql, 2);
            $return=false;
            if($data)
            {
                $return=$data['id'];
            }else
            {
                $return=0;
            }
            core::json([
                "status" =>1,
                "data" =>$return,
            ]);
        }
        return;
    }
    public function read_letter()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
        $uid=$this->user['uid'];
        $id = intval(trim($_POST['id']));  //will checked
        $sql = "update `{$this->db_prefix}message_receiver` set is_readed=1  WHERE id={$id}";
        $this->db->query($sql,0);
        core::json([
            "status" =>1,
        ]);
        return;
    }
    public function letter()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
        $uid=$this->user['uid'];
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $sql = "SELECT id,mid,is_readed FROM `{$this->db_prefix}message_receiver` WHERE `is_deleted`=0 and to_uid={$uid}  ORDER BY  `id` DESC LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        $mid_array=[];
        $letter=[];
        $letter_data=[];
        foreach($data as $k=>$v)
        {
            $mid_array[]=$v['mid'];
            $letter[$v['mid']]=[
                'id'=>$v['id'],
                'mid'=>$v['mid'],
                'is_readed'=>$v['is_readed'],
            ];
        }
        if($mid_array)
        {
            $sql = "select mid,title,content,time  FROM `{$this->db_prefix}message_sender` where mid in (".implode(",",$mid_array).")  ORDER BY mid DESC";
            $mdata = $this->db->query($sql, 3);
            foreach($mdata as $k=>$v)
            {
                $letter_data[]=array_merge($letter[$v['mid']],
                [
                    'title'=>$v['title'],
                    'content'=>$v['content'],
                    'addTime'=>$v['time'],
                ]);
            }    
        }
        core::json([
            "status" =>1,
            "data" =>$letter_data,
        ]);
        return;
    }

    public function cash_log()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
        $this->check_post();
        $uid=$this->user['uid'];
        $_page = core::lib('validate')->number($_POST['page']) ? ($_POST['page']) : 1;
        $page_current = $_page>0?$_page:1;
		$skip = ($page_current - 1) * $this->pagesize;
        $where="";

        $this->get_time(false);
        $where .= $this->build_where_time('`actionTime`');

        $table="{$this->db_prefix}member_cash";
        $sql="select id,amount,actionTime as actionTime,state
                from `{$table}`  where uid={$uid} {$where} order by actionTime desc LIMIT {$skip},{$this->pagesize}";
        $data = $this->db->query($sql, 3);
        core::json([
            "status" =>1,
            "data" =>$data,
        ]);
        return;
    }
    public function cash()
    {
        if($this->user['forTest']==1)
        {
            core::json_err('试玩帐号禁止使用');
        }
        $this->check_post();
        if (
            !array_key_exists('money', $_POST) ||
            !is_string($_POST['money']) ||
            !preg_match('/^[1-9]{1}[0-9]{0,}(\.[0-9]+)?$/', $_POST['money']) ||
            !array_key_exists('password', $_POST) ||
            !is_string($_POST['password']) 
        ) core::__403();
        $money = floatval($_POST['money']);
        $password = md5($_POST['password'].$this->password_key);
        //$info = $this->cash_data();
        //$enable = $this->cash_is_enable($info);
        $cashFromTimeTS = strtotime(date('Y-m-d ') . $this->config['cashFromTime']);
        $cashToTimeTS = strtotime(date('Y-m-d ') . $this->config['cashToTime']);
        $today = strtotime('today');
        $uid = $this->user['uid'];
        $grade = $this->user['grade'];

        $cash_min = intval($this->config['cashMin']);
        $cash_max = intval($this->config['cashMax']);
        if ($money < $cash_min || $money > $cash_max)
        {
            core::json_err('提款范围'.$cash_min." ~ ".$cash_max);
        }

        //检查单日提款次数 begin
        $cash_times = $this->db->getRow(
            "SELECT COUNT(*) AS total FROM {$this->db_prefix}member_cash WHERE actionTime >=:actionTime AND uid =:uid AND state =0",
            array(
                'actionTime' => $today,
                'uid' => $uid
            )
        );
        $limit_times = $this->db->getRow(
            "SELECT maxToCashCount FROM {$this->db_prefix}member_level WHERE level =:level LIMIT 1",
            array('level' => $grade)
        );
        if ($cash_times['total'] >= $limit_times['maxToCashCount']) {
            core::json_err('今日您的提款次数已达到上限');
        }
        //检查单日提款次数 end
        //检查单日提款时间 begin
        if ($this->time < $cashFromTimeTS || $this->time > $cashToTimeTS) {
            $reason = '系统受理提款的时间范围为每天的' . $this->config['cashFromTime'] . ' ~ ' . $this->config['cashToTime'] . '请在该时间段内提交提款申请';
            core::json_err($reason);
        }
        //检查单日提款时间 end
        //检查打码量 begin
        if($this->user['cashLimit'])
        {
            core::json_err('需打码超过'.$this->user['cashLimit'].'才能提款');
        }
        //检查打码量 end

        //if (!$enable['result']) core::json_err($enable['reason']);
        $this->fresh_user_session();
		if (!$this->user['cashEnable']) core::json_err('禁止提款，请联系客服!');
		if ($money > $this->user['coin']) core::json_err('可用余额不足，申请提款失败');
        if ($this->user['coinPassword'] !== $password) core::json_err('资金密码错误');
        if ($this->user['forTest']) core::json_err('测试帐号无法申请提款');
        
        $bank = $this->db->query("SELECT `username`,`account`,`bankId`,countname FROM `{$this->db_prefix}member_bank` WHERE `uid`={$uid} and `enable`=1 LIMIT 1", 2);
        if (!$bank)
        {
            core::json_err('您尚未设置银行卡');
        }

        $insert_data = array(
            'amount' => $money,
            'username' => $bank['username'],
            'account' => $bank['account'],
            'bankId' => $bank['bankId'],
            'countname' => $bank['countname'],
            'actionTime' => $this->time,
            'uid' => $uid,
            'musername' => $this->user['username'],
        );
        $this->db->transaction('begin');
        try {
            $insert_id = $this->db->insert($this->db_prefix . 'member_cash', $insert_data);
            if (!$insert_id) throw new Exception('提交提款请求出错');
            $this->set_coin(array(
                'coin' => 0 - $insert_data['amount'],
                'fcoin' => 0,  // 取消冻结资金帐务类型 by robert 2019-9-13
                'uid' => $insert_data['uid'],
                //'liqType' => 106,
                'liqType' => 107,
                'info' => "提款[$insert_id]",
                'extfield0' => $insert_id
            ));
            $this->db->transaction('commit');
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            core::json_err($e->getMessage());
        }
        core::json([
            "status" =>1
        ]);
        return;        
    }
    private function strToHex($string){
        $hex = '';
        for ($i=0; $i<strlen($string); $i++){
            $ord = ord($string[$i]);
            $hexCode = dechex($ord);
            $hex .= substr('0'.$hexCode, -2);
        }
        return $hex;
    }
}
?>