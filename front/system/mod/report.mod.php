<?php

class mod_report extends mod
{
    public function index()
    {
		if ($this->post) {
			$uid = $this->user['uid'];
			$file= $this->getCacheDir().md5(__CLASS__.__FUNCTION__.$uid);
			$args=array();
			if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
			{
				$args= unserialize(file_get_contents($file));
			}else{
				$sql="select coin,fcoin from `{$this->db_prefix}members` where uid=".$uid." limit 1";
				$member = $this->db->query($sql, 2);
				$data=$this->money_data();
				$args['member']=$member;
				$args['data']=$data;
				file_put_contents($file, serialize($args));
			}
			$this->display('report/index', $args);
		}else
		{
			$this->ajax();
		}
    }
	public function teamfd()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('r.actionTime');
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{			
			$sql="SELECT 
					IFNULL(SUM(r.fandian),0) as fanDian ,
					IFNULL(SUM(r.gongzi),0) as gongzi,
					IFNULL(SUM(r.bonus),0) as bonus ,
					IFNULL(SUM(r.fandianCo),0) as fanDianCo ,
					IFNULL(SUM(r.gongziCo),0) as gongziCo,
					IFNULL(SUM(r.bonusCo),0) as bonusCo
					from {$this->db_prefix}member_report r,{$this->db_prefix}members m  
					where CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid {$where_time} ";
			$data	= $this->db->query($sql, 2);
			$args['total']['fanDian']=number_format($data['fanDian'], 3, '.', '');
			$args['total']['fanDianCo']=$data['fanDianCo'];
			$args['total']['gongzi']=number_format($data['gongzi'], 3, '.', '');
			$args['total']['gongziCo']=$data['gongziCo'];
			$args['total']['bonus']=number_format($data['bonus'], 3, '.', '');
			$args['total']['bonusCo']=$data['bonusCo'];
		
			$base = strtotime("+0 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-1 week Sunday");
			$end	= $end-1;
			/*
			$base = strtotime(date('Y-m',time()) . '-01 00:00:00');        
	        $end	= $base;
	        $start	= strtotime('-1 month', $base);
			$end	= $end-1;
			 */
			$sql="SELECT 
					IFNULL(SUM(r.fandian),0) as fanDian ,
					IFNULL(SUM(r.gongzi),0) as gongzi,
					IFNULL(SUM(r.bonus),0) as bonus ,
					IFNULL(SUM(r.fandianCo),0) as fanDianCo ,
					IFNULL(SUM(r.gongziCo),0) as gongziCo,
					IFNULL(SUM(r.bonusCo),0) as bonusCo  
					from {$this->db_prefix}member_report r,{$this->db_prefix}members m  
					where CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end ";
			$data	= $this->db->query($sql, 2);
			$args['month1']['fanDian']=number_format($data['fanDian'], 3, '.', '');
			$args['month1']['fanDianCo']=$data['fanDianCo'];
			$args['month1']['gongzi']=number_format($data['gongzi'], 3, '.', '');
			$args['month1']['gongziCo']=$data['gongziCo'];
			$args['month1']['bonus']=number_format($data['bonus'], 3, '.', '');
			$args['month1']['bonusCo']=$data['bonusCo'];
			
			$base = strtotime("-1 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-2 week Sunday");
			$end	= $end-1;
	        /*		
	        $end	= strtotime('-1 month', $base);
	        $start	= strtotime('-2 month', $base);
	        $end	= $end-1;
			 */
			$sql="SELECT 
					IFNULL(SUM(r.fandian),0) as fanDian ,
					IFNULL(SUM(r.gongzi),0) as gongzi,
					IFNULL(SUM(r.bonus),0) as bonus ,
					IFNULL(SUM(r.fandianCo),0) as fanDianCo ,
					IFNULL(SUM(r.gongziCo),0) as gongziCo,
					IFNULL(SUM(r.bonusCo),0) as bonusCo  
					from {$this->db_prefix}member_report r,{$this->db_prefix}members m  
					where CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end ";
			$data	= $this->db->query($sql, 2);
			$args['month2']['fanDian']=number_format($data['fanDian'], 3, '.', '');
			$args['month2']['fanDianCo']=$data['fanDianCo'];
			$args['month2']['gongzi']=number_format($data['gongzi'], 3, '.', '');
			$args['month2']['gongziCo']=$data['gongziCo'];
			$args['month2']['bonus']=number_format($data['bonus'], 3, '.', '');
			$args['month2']['bonusCo']=$data['bonusCo'];
	                		
			$base = strtotime("-2 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-3 week Sunday");
			$end	= $end-1;
			/*
	        $end	= strtotime('-2 month', $base);
	        $start	= strtotime('-3 month', $base);
	        $end	= $end-1;
			 */
			$sql="SELECT 
					IFNULL(SUM(r.fandian),0) as fanDian ,
					IFNULL(SUM(r.gongzi),0) as gongzi,
					IFNULL(SUM(r.bonus),0) as bonus ,
					IFNULL(SUM(r.fandianCo),0) as fanDianCo ,
					IFNULL(SUM(r.gongziCo),0) as gongziCo,
					IFNULL(SUM(r.bonusCo),0) as bonusCo  
					from {$this->db_prefix}member_report r,{$this->db_prefix}members m  
					where CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end ";
			$data	= $this->db->query($sql, 2);
			$args['month3']['fanDian']=number_format($data['fanDian'], 3, '.', '');
			$args['month3']['fanDianCo']=$data['fanDianCo'];
			$args['month3']['gongzi']=number_format($data['gongzi'], 3, '.', '');
			$args['month3']['gongziCo']=$data['gongziCo'];
			$args['month3']['bonus']=number_format($data['bonus'], 3, '.', '');
			$args['month3']['bonusCo']=$data['bonusCo'];
			
			file_put_contents($file, serialize($args));
		}
        $this->display('report/teamfd', $args);
   	}

	public function fd()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('actionTime');
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{			
			$sql="SELECT IFNULL(SUM(fandian),0) as fanDian ,IFNULL(SUM(gongzi),0) as gongzi,IFNULL(SUM(bonus),0) as bonus ,IFNULL(SUM(fandianCo),0) as fanDianCo ,IFNULL(SUM(gongziCo),0) as gongziCo,IFNULL(SUM(bonusCo),0) as bonusCo  FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid {$where_time}";
			$data	= $this->db->query($sql, 2);
			$args['total']['fanDian']=number_format($data['fanDian'], 3, '.', '');
			$args['total']['fanDianCo']=$data['fanDianCo'];
			$args['total']['gongzi']=number_format($data['gongzi'], 3, '.', '');
			$args['total']['gongziCo']=$data['gongziCo'];
			$args['total']['bonus']=number_format($data['bonus'], 3, '.', '');
			$args['total']['bonusCo']=$data['bonusCo'];
		
			$base = strtotime("+0 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-1 week Sunday");
			$end	= $end-1;
			/*
			$base = strtotime(date('Y-m',time()) . '-01 00:00:00');        
	        $end	= $base;
	        $start	= strtotime('-1 month', $base);
			$end	= $end-1;
			 */
			$sql="SELECT IFNULL(SUM(fandian),0) as fanDian ,IFNULL(SUM(gongzi),0) as gongzi,IFNULL(SUM(bonus),0) as bonus ,IFNULL(SUM(fandianCo),0) as fanDianCo ,IFNULL(SUM(gongziCo),0) as gongziCo,IFNULL(SUM(bonusCo),0) as bonusCo  FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end";
			$data	= $this->db->query($sql, 2);
			$args['month1']['fanDian']=number_format($data['fanDian'], 3, '.', '');
			$args['month1']['fanDianCo']=$data['fanDianCo'];
			$args['month1']['gongzi']=number_format($data['gongzi'], 3, '.', '');
			$args['month1']['gongziCo']=$data['gongziCo'];
			$args['month1']['bonus']=number_format($data['bonus'], 3, '.', '');
			$args['month1']['bonusCo']=$data['bonusCo'];
			
			$base = strtotime("-1 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-2 week Sunday");
			$end	= $end-1;
	        /*		
	        $end	= strtotime('-1 month', $base);
	        $start	= strtotime('-2 month', $base);
	        $end	= $end-1;
			 */
			$sql="SELECT IFNULL(SUM(fandian),0) as fanDian ,IFNULL(SUM(gongzi),0) as gongzi,IFNULL(SUM(bonus),0) as bonus ,IFNULL(SUM(fandianCo),0) as fanDianCo ,IFNULL(SUM(gongziCo),0) as gongziCo,IFNULL(SUM(bonusCo),0) as bonusCo  FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end";
			$data	= $this->db->query($sql, 2);
			$args['month2']['fanDian']=number_format($data['fanDian'], 3, '.', '');
			$args['month2']['fanDianCo']=$data['fanDianCo'];
			$args['month2']['gongzi']=number_format($data['gongzi'], 3, '.', '');
			$args['month2']['gongziCo']=$data['gongziCo'];
			$args['month2']['bonus']=number_format($data['bonus'], 3, '.', '');
			$args['month2']['bonusCo']=$data['bonusCo'];
	                		
			$base = strtotime("-2 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-3 week Sunday");
			$end	= $end-1;
			/*
	        $end	= strtotime('-2 month', $base);
	        $start	= strtotime('-3 month', $base);
	        $end	= $end-1;
			 */
			$sql="SELECT IFNULL(SUM(fandian),0) as fanDian ,IFNULL(SUM(gongzi),0) as gongzi,IFNULL(SUM(bonus),0) as bonus ,IFNULL(SUM(fandianCo),0) as fanDianCo ,IFNULL(SUM(gongziCo),0) as gongziCo,IFNULL(SUM(bonusCo),0) as bonusCo  FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end";
			$data	= $this->db->query($sql, 2);
			$args['month3']['fanDian']=number_format($data['fanDian'], 3, '.', '');
			$args['month3']['fanDianCo']=$data['fanDianCo'];
			$args['month3']['gongzi']=number_format($data['gongzi'], 3, '.', '');
			$args['month3']['gongziCo']=$data['gongziCo'];
			$args['month3']['bonus']=number_format($data['bonus'], 3, '.', '');
			$args['month3']['bonusCo']=$data['bonusCo'];
			
			file_put_contents($file, serialize($args));
		}
        $this->display('report/fd', $args);
   	}
	public function teamtransfer()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('r.actionTime');
		
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid."55");
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{			
			$args['total']['transfer']=0;
			$args['total']['transferCo']=0;
			$args['total']['headTransfer']=0;
			$args['total']['headTransferCo']=0;
			$args['total']['sonTransfer']=0;
			$args['total']['sonTransferCo']=0;
			$sql="select 
						IFNULL(SUM(transfer),0) as transfer,
						IFNULL(SUM(Stransfer),0) as headTransfer,IFNULL(SUM(StransferCo),0) as headTransferCo ,
						IFNULL(SUM(Ctransfer),0) as sonTransfer,IFNULL(SUM(CtransferCo),0) as sonTransferCo
						from {$this->db_prefix}member_report r,{$this->db_prefix}members m  
						where CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid {$where_time} ";
			$data	= $this->db->query($sql, 2);
			$args['total']['transferCo']		=$data['headTransferCo']+$data['sonTransferCo'];
			$args['total']['headTransferCo']	=$data['headTransferCo'];
			$args['total']['sonTransferCo']		=$data['sonTransferCo'];
			$args['total']['transfer']=number_format($data['transfer'], 3, '.', '');
			$args['total']['headTransfer']=number_format($data['headTransfer'], 3, '.', '');
			$args['total']['sonTransfer']=number_format($data['sonTransfer'], 3, '.', '');
	
			$base = strtotime("+0 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-1 week Sunday");
			$end	= $end-1;
			/*
			$base = strtotime(date('Y-m',time()) . '-01 00:00:00');
	        $end	= $base;
	        $start	= strtotime('-1 month', $base);
			$end	= $end-1;
			 */
			$sql="select 
						IFNULL(SUM(transfer),0) as transfer,
						IFNULL(SUM(Stransfer),0) as headTransfer,
						IFNULL(SUM(Ctransfer),0) as sonTransfer
						from {$this->db_prefix}member_report r,{$this->db_prefix}members m  
						where CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end ";
			$data	= $this->db->query($sql, 2);
	        $args['month1']['transfer']=number_format($data['transfer'], 3, '.', '');
	        $args['month1']['headTransfer']=number_format($data['headTransfer'], 3, '.', '');
	        $args['month1']['sonTransfer']=number_format($data['sonTransfer'], 3, '.', '');
			
			$base = strtotime("-1 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-2 week Sunday");
			$end	= $end-1;
			/*
	        $end	= strtotime('-1 month', $base);
	        $start	= strtotime('-2 month', $base);
	        $end	= $end-1;
			 */
			$sql="select 
						IFNULL(SUM(transfer),0) as transfer,
						IFNULL(SUM(Stransfer),0) as headTransfer,
						IFNULL(SUM(Ctransfer),0) as sonTransfer
						from {$this->db_prefix}member_report r,{$this->db_prefix}members m  
						where CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end ";
			$data	= $this->db->query($sql, 2);
	        $args['month2']['transfer']=number_format($data['transfer'], 3, '.', '');
	        $args['month2']['headTransfer']=number_format($data['headTransfer'], 3, '.', '');
	        $args['month2']['sonTransfer']=number_format($data['sonTransfer'], 3, '.', '');

			$base = strtotime("-2 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-3 week Sunday");
			$end	= $end-1;
			/*
	        $end	= strtotime('-2 month', $base);
	        $start	= strtotime('-3 month', $base);
	        $end	= $end-1;
			 */
			$sql="select 
						IFNULL(SUM(transfer),0) as transfer,
						IFNULL(SUM(Stransfer),0) as headTransfer,
						IFNULL(SUM(Ctransfer),0) as sonTransfer
						from {$this->db_prefix}member_report r,{$this->db_prefix}members m  
						where CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end ";
			$data	= $this->db->query($sql, 2);
	        $args['month3']['transfer']=number_format($data['transfer'], 3, '.', '');
	        $args['month3']['headTransfer']=number_format($data['headTransfer'], 3, '.', '');
	        $args['month3']['sonTransfer']=number_format($data['sonTransfer'], 3, '.', '');
			
			file_put_contents($file, serialize($args));
		}
		$this->display('report/teamtransfer', $args);
	}
	public function transfer()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('actionTime');
		
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		$args=array();
		
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{			
			$args['total']['transfer']=0;
			$args['total']['transferCo']=0;
			$args['total']['headTransfer']=0;
			$args['total']['headTransferCo']=0;
			$args['total']['sonTransfer']=0;
			$args['total']['sonTransferCo']=0;
			$sql="select 
						IFNULL(SUM(transfer),0) as transfer,
						IFNULL(SUM(Stransfer),0) as headTransfer,IFNULL(SUM(StransferCo),0) as headTransferCo ,
						IFNULL(SUM(Ctransfer),0) as sonTransfer,IFNULL(SUM(CtransferCo),0) as sonTransferCo
						from {$this->db_prefix}member_report where `uid`=$uid {$where_time} ";
			$data	= $this->db->query($sql, 2);
			$args['total']['transferCo']		=$data['headTransferCo']+$data['sonTransferCo'];
			$args['total']['headTransferCo']	=$data['headTransferCo'];
			$args['total']['sonTransferCo']		=$data['sonTransferCo'];
			$args['total']['transfer']=number_format($data['transfer'], 3, '.', '');
			$args['total']['headTransfer']=number_format($data['headTransfer'], 3, '.', '');
			$args['total']['sonTransfer']=number_format($data['sonTransfer'], 3, '.', '');
	
			$base = strtotime("+0 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-1 week Sunday");
			$end	= $end-1;
			/*
			$base = strtotime(date('Y-m',time()) . '-01 00:00:00');
	        $end	= $base;
	        $start	= strtotime('-1 month', $base);
			$end	= $end-1;
			 */
			$sql="select 
						IFNULL(SUM(transfer),0) as transfer,
						IFNULL(SUM(Stransfer),0) as headTransfer,
						IFNULL(SUM(Ctransfer),0) as sonTransfer
						from {$this->db_prefix}member_report where `uid`=$uid and `actionTime` BETWEEN $start AND $end";
			$data	= $this->db->query($sql, 2);
	        $args['month1']['transfer']=number_format($data['transfer'], 3, '.', '');
	        $args['month1']['headTransfer']=number_format($data['headTransfer'], 3, '.', '');
	        $args['month1']['sonTransfer']=number_format($data['sonTransfer'], 3, '.', '');
			
			$base = strtotime("-1 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-2 week Sunday");
			$end	= $end-1;
			/*
	        $end	= strtotime('-1 month', $base);
	        $start	= strtotime('-2 month', $base);
	        $end	= $end-1;
			 */
			$sql="select 
						IFNULL(SUM(transfer),0) as transfer,
						IFNULL(SUM(Stransfer),0) as headTransfer,
						IFNULL(SUM(Ctransfer),0) as sonTransfer
						from {$this->db_prefix}member_report where `uid`=$uid and `actionTime` BETWEEN $start AND $end";
			$data	= $this->db->query($sql, 2);
	        $args['month2']['transfer']=number_format($data['transfer'], 3, '.', '');
	        $args['month2']['headTransfer']=number_format($data['headTransfer'], 3, '.', '');
	        $args['month2']['sonTransfer']=number_format($data['sonTransfer'], 3, '.', '');

			$base = strtotime("-2 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-3 week Sunday");
			$end	= $end-1;
			/*
	        $end	= strtotime('-2 month', $base);
	        $start	= strtotime('-3 month', $base);
	        $end	= $end-1;
			 */
			$sql="select 
						IFNULL(SUM(transfer),0) as transfer,
						IFNULL(SUM(Stransfer),0) as headTransfer,
						IFNULL(SUM(Ctransfer),0) as sonTransfer
						from {$this->db_prefix}member_report where `uid`=$uid and `actionTime` BETWEEN $start AND $end";
			$data	= $this->db->query($sql, 2);
	        $args['month3']['transfer']=number_format($data['transfer'], 3, '.', '');
	        $args['month3']['headTransfer']=number_format($data['headTransfer'], 3, '.', '');
	        $args['month3']['sonTransfer']=number_format($data['sonTransfer'], 3, '.', '');
			
			file_put_contents($file, serialize($args));
		}
		$this->display('report/transfer', $args);
	}
	public function teamcash()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('r.actionTime');
		$args=array();
		$args['tcharge']['amount']=0;
		$args['tcharge']['co']=0;
		$args['tcharge']['del']=0;
		$args['tcharge']['ok']=0;
		for($i=0;$i<20;$i++)
		{
			$args['bank']['data'][$i]['amount']=0;
			$args['bank']['data'][$i]['co']=0;
			$args['bank']['data'][$i]['del']=0;
			$args['bank']['data'][$i]['ok']=0;
		}

        $sql="select
        			r.bankId ,
        			IFNULL(sum(r.num),0) as num ,
        			IFNULL(sum(r.snum),0) as snum ,
        			IFNULL(sum(r.amount),0) as amount
        			FROM `{$this->db_prefix}report_cash` r , `{$this->db_prefix}members` m 
        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid $where_time
        			group by r.bankId
        	";
		$data	= $this->db->query($sql, 3);
		foreach($data as $v){
			$args['tcharge']['co']+=$v['num'];
			$args['tcharge']['amount']+=$v['amount'];
			$args['tcharge']['ok']+=$v['snum'];
			$args['bank']['data'][$v['bankId']]['co']+=$v['num'];
			$args['bank']['data'][$v['bankId']]['amount']+=$v['amount'];
			$args['bank']['data'][$v['bankId']]['ok']+=$v['snum'];
		}
		$this->display('report/teamcash', $args);
	}
	public function cash()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('actionTime');
		$args=array();
		$args['tcharge']['amount']=0;
		$args['tcharge']['co']=0;
		$args['tcharge']['del']=0;
		$args['tcharge']['ok']=0;
		for($i=0;$i<20;$i++)
		{
			$args['bank']['data'][$i]['amount']=0;
			$args['bank']['data'][$i]['co']=0;
			$args['bank']['data'][$i]['del']=0;
			$args['bank']['data'][$i]['ok']=0;
		}
		for($i=1;$i<7;$i++)
		{
			$args['ct'][$i]['amount']=0;
			$args['ct'][$i]['co']=0;
		}
		$sql="select id,amount,bankId,actionTime,state from {$this->db_prefix}member_cash WHERE uid={$uid} {$where_time}";
		$data	= $this->db->query($sql, 3);
		foreach($data as $v){
			$args['tcharge']['co']++;
			$args['bank']['data'][$v['bankId']]['co']++;
			switch(true)
			{
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("00:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("03:59:59"):
					$args['ct'][1]['amount']+=$v['amount'];
					$args['ct'][1]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("04:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("07:59:59"):
					$args['ct'][2]['amount']+=$v['amount'];
					$args['ct'][2]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("08:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("11:59:59"):
					$args['ct'][3]['amount']+=$v['amount'];
					$args['ct'][3]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("12:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("15:59:59"):
					$args['ct'][4]['amount']+=$v['amount'];
					$args['ct'][4]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("16:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("19:59:59"):
					$args['ct'][5]['amount']+=$v['amount'];
					$args['ct'][5]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("20:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("23:59:59"):
					$args['ct'][6]['amount']+=$v['amount'];
					$args['ct'][6]['co']++;
				break;
			}
			switch(true)
			{
				case $v['state']==3 || $v['state']==0:
					$args['tcharge']['amount']+=$v['amount'];
					$args['tcharge']['ok']++;
					$args['bank']['data'][$v['bankId']]['amount']+=$v['amount'];
					$args['bank']['data'][$v['bankId']]['ok']++;
				break;
				case $v['state']==2 || $v['state']==4:
					$args['tcharge']['del']++;
					$args['bank']['data'][$v['bankId']]['del']++;
				break;
			}
		}
		$this->display('report/cash', $args);
	}
	public function recharge()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('actionTime');
		$args=array();
		$args['tcharge']['amount']=0;
		$args['tcharge']['co']=0;
		$args['tcharge']['del']=0;
		$args['tcharge']['ok']=0;
		$args['wc']['amount']=0;
		$args['wc']['co']=0;
		$args['wc']['del']=0;
		$args['wc']['ok']=0;
		$args['ali']['amount']=0;
		$args['ali']['co']=0;
		$args['ali']['del']=0;
		$args['ali']['ok']=0;
		$args['qq']['amount']=0;
		$args['qq']['co']=0;
		$args['qq']['del']=0;
		$args['qq']['ok']=0;
		$args['qp']['amount']=0;
		$args['qp']['co']=0;
		$args['qp']['del']=0;
		$args['qp']['ok']=0;		
		$args['jd']['amount']=0;
		$args['jd']['co']=0;
		$args['jd']['del']=0;
		$args['jd']['ok']=0;		

		$args['bank']['amount']=0;
		$args['bank']['co']=0;
		$args['bank']['del']=0;
		$args['bank']['ok']=0;
		for($i=0;$i<30;$i++)
		{
			$args['bank']['data'][$i]['amount']=0;
			$args['bank']['data'][$i]['co']=0;
			$args['bank']['data'][$i]['del']=0;
			$args['bank']['data'][$i]['ok']=0;
		}
		for($i=1;$i<7;$i++)
		{
			$args['ct'][$i]['amount']=0;
			$args['ct'][$i]['co']=0;
		}
		$sql="select id,amount,bankId,isDelete,actionTime,state from {$this->db_prefix}member_recharge WHERE uid={$uid} and state in (1,2,9) {$where_time}";
		$data	= $this->db->query($sql, 3);
		foreach($data as $v){
			$args['tcharge']['co']++;
			switch(true)
			{
				case $v['bankId']==19:
					$args['wc']['co']++;
				break;
				case $v['bankId']==1:
					$args['ali']['co']++;
				break;
				case $v['bankId']==18:
					$args['qp']['co']++;
				break;
				case $v['bankId']==21:
					$args['jd']['co']++;
				break;
				case $v['bankId']==20:
					$args['qq']['co']++;
				break;
				default:
					$args['bank']['co']++;
					$args['bank']['data'][$v['bankId']]['co']++;
				break;
			}
			switch(true)
			{
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("00:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("03:59:59"):
					$args['ct'][1]['amount']+=$v['amount'];
					$args['ct'][1]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("04:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("07:59:59"):
					$args['ct'][2]['amount']+=$v['amount'];
					$args['ct'][2]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("08:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("11:59:59"):
					$args['ct'][3]['amount']+=$v['amount'];
					$args['ct'][3]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("12:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("15:59:59"):
					$args['ct'][4]['amount']+=$v['amount'];
					$args['ct'][4]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("16:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("19:59:59"):
					$args['ct'][5]['amount']+=$v['amount'];
					$args['ct'][5]['co']++;
				break;
				case strtotime(date("H:i:s",$v['actionTime'])) >= strtotime("20:00:00") && strtotime(date("H:i:s",$v['actionTime'])) <= strtotime("23:59:59"):
					$args['ct'][6]['amount']+=$v['amount'];
					$args['ct'][6]['co']++;
				break;
			}
			if($v['state']==1 || $v['state']==9)
			{
				$args['tcharge']['amount']+=$v['amount'];
				switch(true)
				{
					case $v['bankId']==19:
						$args['wc']['amount']+=$v['amount'];
						$args['wc']['ok']++;
					break;
					case $v['bankId']==1:
						$args['ali']['amount']+=$v['amount'];
						$args['ali']['ok']++;
					break;
					case $v['bankId']==18:
						$args['qp']['amount']+=$v['amount'];
						$args['qp']['ok']++;
					break;
					case $v['bankId']==20:
						$args['qq']['amount']+=$v['amount'];
						$args['qq']['ok']++;
					break;					
					case $v['bankId']==21:
						$args['jd']['amount']+=$v['amount'];
						$args['jd']['ok']++;
					break;					
					default:
						$args['bank']['amount']+=$v['amount'];
						$args['bank']['data'][$v['bankId']]['amount']+=$v['amount'];
						$args['bank']['ok']++;
						$args['bank']['data'][$v['bankId']]['ok']++;
					break;
				}
			}
			if($v['isDelete']==1)
			{
				$args['tcharge']['del']++;
				switch(true)
				{
					case $v['bankId']==19:
						$args['wc']['del']++;
					break;
					case $v['bankId']==1:
						$args['ali']['del']++;
					break;
					case $v['bankId']==18:
						$args['qp']['del']++;
					break;
					case $v['bankId']==20:
						$args['qq']['del']++;
					break;					
					default:
						$args['bank']['del']++;
						$args['bank']['data'][$v['bankId']]['del']++;
					break;
				}
			}
		}
		$this->display('report/recharge', $args);
	}
	public function teamrecharge()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('r.actionTime');
		$args=array();
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{			
			$args['tcharge']['amount']=0;
			$args['tcharge']['co']=0;
			$args['tcharge']['del']=0;
			$args['tcharge']['ok']=0;
			$args['wc']['amount']=0;
			$args['wc']['co']=0;
			$args['wc']['del']=0;
			$args['wc']['ok']=0;
			$args['ali']['amount']=0;
			$args['ali']['co']=0;
			$args['ali']['del']=0;
			$args['ali']['ok']=0;
			$args['qq']['amount']=0;
			$args['qq']['co']=0;
			$args['qq']['del']=0;
			$args['qq']['ok']=0;
			$args['qp']['amount']=0;
			$args['qp']['co']=0;
			$args['qp']['del']=0;
			$args['qp']['ok']=0;
			$args['jd']['amount']=0;
			$args['jd']['co']=0;
			$args['jd']['del']=0;
			$args['jd']['ok']=0;
			$args['bank']['amount']=0;
			$args['bank']['co']=0;
			$args['bank']['del']=0;
			$args['bank']['ok']=0;
			for($i=0;$i<30;$i++)
			{
				$args['bank']['data'][$i]['amount']=0;
				$args['bank']['data'][$i]['co']=0;
				$args['bank']['data'][$i]['del']=0;
				$args['bank']['data'][$i]['ok']=0;
			}
	        $sql="select
	        			r.bankId ,
	        			IFNULL(sum(r.num),0) as num ,
	        			IFNULL(sum(r.snum),0) as snum ,
	        			IFNULL(sum(r.real_amount),0) as real_amount
	        			FROM `{$this->db_prefix}report_recharge` r , `{$this->db_prefix}members` m 
	        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid $where_time
	        			group by r.bankId
	        	"; 
			$data	= $this->db->query($sql, 3);
			foreach($data as $v){
				$args['tcharge']['co']+=$v['num'];
				$args['tcharge']['amount']+=$v['real_amount'];
				$args['tcharge']['ok']+=$v['snum'];
				switch(true)
				{
					case $v['bankId']==19:
						$args['wc']['co']+=$v['num'];
						$args['wc']['amount']+=$v['real_amount'];
						$args['wc']['ok']+=$v['snum'];
					break;
					case $v['bankId']==1:
						$args['ali']['co']+=$v['num'];
						$args['ali']['amount']+=$v['real_amount'];
						$args['ali']['ok']+=$v['snum'];
					break;
					case $v['bankId']==20:
						$args['qq']['co']+=$v['num'];
						$args['qq']['amount']+=$v['real_amount'];
						$args['qq']['ok']+=$v['snum'];
					break;
					case $v['bankId']==21:
						$args['jd']['co']+=$v['num'];
						$args['jd']['amount']+=$v['real_amount'];
						$args['jd']['ok']+=$v['snum'];
					break;
					case $v['bankId']==18:
						$args['qp']['co']+=$v['num'];
						$args['qp']['amount']+=$v['real_amount'];
						$args['qp']['ok']+=$v['snum'];
					break;
					default:
						$args['bank']['co']+=$v['num'];
						$args['bank']['amount']+=$v['real_amount'];
						$args['bank']['ok']+=$v['snum'];
	
						$args['bank']['data'][$v['bankId']]['amount']+=$v['real_amount'];
						$args['bank']['data'][$v['bankId']]['co']+=$v['num'];
						$args['bank']['data'][$v['bankId']]['ok']+=$v['snum'];
					break;
				}
			}
			file_put_contents($file, serialize($args));
		}
		$this->display('report/teamrecharge', $args);
	}

	public function play()
	{
		$uid = $this->user['uid'];
		$args=array();
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('actionTime');
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
			$sql="select
					playId as playedId,
					type,
					IFNULL(sum(amount),0) as betAmount ,
					IFNULL(sum(actionNum),0) as actionNum ,
					IFNULL(sum(zjAmount),0) as bonus ,
					count(orders) as norder 
					FROM `{$this->db_prefix}report_play`
					WHERE uid={$uid} {$where_time} group by playId
				";
			$data	= $this->db->query($sql, 3);
			foreach($data as $var){
				$args['total'][$var['playedId']]=$var;
				$args['today'][$var['playedId']]=$var;
			}
/*
	        $start	= strtotime('today');
	        $end	= $this->time;
			$sql="select
					playId as playedId,
					type,
					IFNULL(sum(amount),0) as betAmount ,
					IFNULL(sum(actionNum),0) as actionNum ,
					IFNULL(sum(zjAmount),0) as bonus ,
					count(orders) as norder 
					FROM `{$this->db_prefix}report_play`
					WHERE uid={$uid} and actionTime between {$start} AND {$end} group by playId
				";
			$data	= $this->db->query($sql, 3);
			foreach($data as $var){
				$args['today'][$var['playedId']]=$var;
			}
			
	        $end	= strtotime('today');
	        $start	= $end - 86400;
	        $end	= $end-1;
			$sql="select
					playId as playedId,
					type,
					IFNULL(sum(amount),0) as betAmount ,
					IFNULL(sum(actionNum),0) as actionNum ,
					IFNULL(sum(zjAmount),0) as bonus ,
					count(orders) as norder 
					FROM `{$this->db_prefix}report_play`
					WHERE uid={$uid} and actionTime between {$start} AND {$end} group by playId
				";
			$data	= $this->db->query($sql, 3);
			foreach($data as $var){
				$args['yestoday'][$var['playedId']]=$var;
			}
*/ 
			file_put_contents($file, serialize($args));
		}
		$this->display('report/play', $args);
	}
	public function lottery()
	{
		$uid = $this->user['uid'];
		$args=array();
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('actionTime');
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
			$sql="select
					type,
					IFNULL(sum(amount),0) as betAmount,
					IFNULL(sum(actionNum),0) as actionNum,
					IFNULL(sum(zjAmount),0) as bonus,
					IFNULL(sum(orders),0) as norder
					FROM `{$this->db_prefix}report_type` 
					WHERE uid={$uid} {$where_time} group by type
				";
			$data	= $this->db->query($sql, 3);
			foreach($data as $var){
				$args['total'][$var['type']]=$var;
				$args['today'][$var['type']]=$var;
			}
			/*
			//$start	= strtotime('today');
	        //$end	= $this->time;
			$sql="select
					type,
					IFNULL(sum(amount),0) as betAmount,
					IFNULL(sum(actionNum),0) as actionNum,
					IFNULL(sum(zjAmount),0) as bonus,
					IFNULL(sum(orders),0) as norder
					FROM `{$this->db_prefix}report_type` 
					WHERE uid={$uid} {$where_time} group by type
				";
			$data	= $this->db->query($sql, 3);
			foreach($data as $var){
				$args['today'][$var['type']]=$var;
			}
			*/
			/*
	        //$end	= strtotime('today');
	        //$start	= $end - 86400;
	        //$end	= $end-1;
			$sql="select
					type,
					IFNULL(sum(amount),0) as betAmount,
					IFNULL(sum(actionNum),0) as actionNum,
					IFNULL(sum(zjAmount),0) as bonus,
					IFNULL(sum(orders),0) as norder
					FROM `{$this->db_prefix}report_type` 
					WHERE uid={$uid} {$where_time} group by type
				";
			$data	= $this->db->query($sql, 3);
			foreach($data as $var){
				$args['yestoday'][$var['type']]=$var;
			}
			*/
			file_put_contents($file, serialize($args));
		}
		$this->display('report/lottery', $args);
	}
	public function game()
	{
		$uid = $this->user['uid'];
		$args=array();
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('actionTime');
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
			
			$sql="select 
					IFNULL(sum(amount),0) as betAmount,							
					IFNULL(sum(actionNum),0) as actionNum,
					IFNULL(sum(orders),0) as norder,
					IFNULL(sum(zjAmount),0) as zjAmount,
					IFNULL(sum(zjActionNum),0) as zjActionNum,
					IFNULL(sum(zjOrders),0) as zjOrder,
					IFNULL(sum(cAmount),0) as delAmount,
					IFNULL(sum(cActionNum),0) as delActionNum,
					IFNULL(sum(cOrders),0) as delOrder,
					IFNULL(sum(zActionNum),0) as zhActionNum,
					IFNULL(sum(zOrders),0) as zhOrder
					FROM `{$this->db_prefix}report_game` WHERE uid={$uid} {$where_time}
				";
			$data	= $this->db->query($sql, 2);
			$args['total']['betAmount']		=$data['betAmount'];
			$args['total']['actionNum']		=$data['actionNum'];
			$args['total']['norder']		=$data['norder'];
			$args['total']['zjAmount']		=$data['zjAmount'];
			$args['total']['zjActionNum']	=$data['zjActionNum'];
			$args['total']['zjOrder']		=$data['zjOrder'];
			$args['total']['delAmount']		=$data['delAmount'];
			$args['total']['delActionNum']	=$data['delActionNum'];
			$args['total']['delOrder']		=$data['delOrder'];
			$args['total']['zhActionNum']	=$data['zhActionNum'];
			$args['total']['zhOrder']		=$data['zhOrder'];

	        $start	= strtotime('today');
	        $end	= $this->time;
	        
	        $sql="select 
					IFNULL(sum(amount),0) as betAmount,							
					IFNULL(sum(actionNum),0) as actionNum,
					IFNULL(sum(orders),0) as norder,
					IFNULL(sum(zjAmount),0) as zjAmount,
					IFNULL(sum(zjActionNum),0) as zjActionNum,
					IFNULL(sum(zjOrders),0) as zjOrder,
					IFNULL(sum(cAmount),0) as delAmount,
					IFNULL(sum(cActionNum),0) as delActionNum,
					IFNULL(sum(cOrders),0) as delOrder,
					IFNULL(sum(zActionNum),0) as zhActionNum,
					IFNULL(sum(zOrders),0) as zhOrder
					FROM `{$this->db_prefix}report_game` WHERE uid={$uid} and actionTime between {$start} AND {$end}
				";				
			$data	= $this->db->query($sql, 2);
			$args['today']['betAmount']		=$data['betAmount'];
			$args['today']['actionNum']		=$data['actionNum'];
			$args['today']['norder']		=$data['norder'];
			$args['today']['zjAmount']		=$data['zjAmount'];
			$args['today']['zjActionNum']	=$data['zjActionNum'];
			$args['today']['zjOrder']		=$data['zjOrder'];
			$args['today']['delAmount']		=$data['delAmount'];
			$args['today']['delActionNum']	=$data['delActionNum'];
			$args['today']['delOrder']		=$data['delOrder'];
			$args['today']['zhActionNum']	=$data['zhActionNum'];
			$args['today']['zhOrder']		=$data['zhOrder'];

	        $end	= strtotime('today');
	        $start	= $end - 86400;
	        $end	= $end-1;
	        $sql="select 
					IFNULL(sum(amount),0) as betAmount,							
					IFNULL(sum(actionNum),0) as actionNum,
					IFNULL(sum(orders),0) as norder,
					IFNULL(sum(zjAmount),0) as zjAmount,
					IFNULL(sum(zjActionNum),0) as zjActionNum,
					IFNULL(sum(zjOrders),0) as zjOrder,
					IFNULL(sum(cAmount),0) as delAmount,
					IFNULL(sum(cActionNum),0) as delActionNum,
					IFNULL(sum(cOrders),0) as delOrder,
					IFNULL(sum(zActionNum),0) as zhActionNum,
					IFNULL(sum(zOrders),0) as zhOrder
					FROM `{$this->db_prefix}report_game` WHERE uid={$uid} and actionTime between {$start} AND {$end}
				";				
			$data	= $this->db->query($sql, 2);
			$args['yestoday']['betAmount']		=$data['betAmount'];
			$args['yestoday']['actionNum']		=$data['actionNum'];
			$args['yestoday']['norder']			=$data['norder'];
			$args['yestoday']['zjAmount']		=$data['zjAmount'];
			$args['yestoday']['zjActionNum']	=$data['zjActionNum'];
			$args['yestoday']['zjOrder']		=$data['zjOrder'];
			$args['yestoday']['delAmount']		=$data['delAmount'];
			$args['yestoday']['delActionNum']	=$data['delActionNum'];
			$args['yestoday']['delOrder']		=$data['delOrder'];
			$args['yestoday']['zhActionNum']	=$data['zhActionNum'];
			$args['yestoday']['zhOrder']		=$data['zhOrder'];
			
			file_put_contents($file, serialize($args));
		}
		$this->display('report/game', $args);
	}
	public function money()
	{
		$uid = $this->user['uid'];
		$file= $this->getCacheDir().md5(__CLASS__.__FUNCTION__.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
			$sql="select coin,fcoin from `{$this->db_prefix}members` where uid=".$uid." limit 1";
			$member = $this->db->query($sql, 2);
			$data=$this->money_data();
			$args['member']=$member;
			$args['data']=$data;
			file_put_contents($file, serialize($args));
		}
		$this->display('report/money', $args);
	}
	
	public function teammoney()
	{
		$uid = $this->user['uid'];
		$file= $this->getCacheDir().md5(__CLASS__.__FUNCTION__.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
			$month1Arrry=array();
			$month2Arrry=array();
			$month3Arrry=array();

	        // 上周統計
			$base = strtotime("+0 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-1 week Sunday");
			$end	= $end-1;
			/*
	        // 上月統計
			$base = strtotime(date('Y-m',time()) . '-01 00:00:00');        
	        $end	= $base;
	        $start	= strtotime('-1 month', $base);
			$end	= $end-1;
			 *
			 */
			 /*
	        $sql="select
	        			IFNULL(sum(r.recharge),0) as rechargeAmount,
	        			IFNULL(sum(r.cash),0) as cashAmount ,
	        			IFNULL(sum(r.real_bet),0) as betAmount,
	        			IFNULL(sum(r.zj+r.fandian+r.broker+r.bonus+r.gongzi-r.real_bet),0) as total_coin
	        			FROM `{$this->db_prefix}member_report` r , `{$this->db_prefix}members` m 
	        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end
	        	";
			*/
	        $sql="select
	        			IFNULL(sum(r.recharge),0) as rechargeAmount,
	        			IFNULL(sum(r.cash),0) as cashAmount ,
	        			IFNULL(sum(r.real_bet),0) as betAmount,
	        			IFNULL(sum(r.zj+r.fandian+r.broker+r.gongzi-r.real_bet),0) as total_coin
	        			FROM `{$this->db_prefix}member_report` r , `{$this->db_prefix}members` m 
	        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end
	        	";
			$data	= $this->db->query($sql, 2); 
	        $month1Arrry['money']=number_format($data['total_coin'], 3, '.', ''); // 盈餘
	        $month1Arrry['betAmount']=number_format($data['betAmount'], 3, '.', ''); // 投注
	        $month1Arrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ''); // 充值
	        $month1Arrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ''); // 提現
	
	        // 上上周統計
			$base = strtotime("-1 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-2 week Sunday");
			$end	= $end-1;
			/*
	        // 上上月統計
	        $end	= strtotime('-1 month', $base);
	        $start	= strtotime('-2 month', $base);
	        $end	= $end-1;
			 * 
			 */
			/*
			$sql="select
	        			IFNULL(sum(r.recharge),0) as rechargeAmount,
	        			IFNULL(sum(r.cash),0) as cashAmount ,
	        			IFNULL(sum(r.real_bet),0) as betAmount,
	        			IFNULL(sum(r.zj+r.fandian+r.broker+r.bonus+r.gongzi-r.real_bet),0) as total_coin
	        			FROM `{$this->db_prefix}member_report` r , `{$this->db_prefix}members` m 
	        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end
	        	";*/
	        $sql="select
	        			IFNULL(sum(r.recharge),0) as rechargeAmount,
	        			IFNULL(sum(r.cash),0) as cashAmount ,
	        			IFNULL(sum(r.real_bet),0) as betAmount,
	        			IFNULL(sum(r.zj+r.fandian+r.broker+r.gongzi-r.real_bet),0) as total_coin
	        			FROM `{$this->db_prefix}member_report` r , `{$this->db_prefix}members` m 
	        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end
	        	";						

			$data	= $this->db->query($sql, 2); 
	        $month2Arrry['money']=number_format($data['total_coin'], 3, '.', ''); // 盈餘
	        $month2Arrry['betAmount']=number_format($data['betAmount'], 3, '.', ''); // 投注
	        $month2Arrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ''); // 充值
	        $month2Arrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ''); // 提現
	
	        // 上上上周統計
			$base = strtotime("-2 week Sunday");        
	        $end	= $base;
	        $start	= strtotime("-3 week Sunday");
			$end	= $end-1;
			/*
	        // 上上上月統計
	        $end	= strtotime('-2 month', $base);
	        $start	= strtotime('-3 month', $base);
	        $end	= $end-1;
			 * 
			 */
			 /*
	        $sql="select
	        			IFNULL(sum(r.recharge),0) as rechargeAmount,
	        			IFNULL(sum(r.cash),0) as cashAmount ,
	        			IFNULL(sum(r.real_bet),0) as betAmount,
	        			IFNULL(sum(r.zj+r.fandian+r.broker+r.bonus+r.gongzi-r.real_bet),0) as total_coin
	        			FROM `{$this->db_prefix}member_report` r , `{$this->db_prefix}members` m 
	        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end
	        	";*/
	        $sql="select
	        			IFNULL(sum(r.recharge),0) as rechargeAmount,
	        			IFNULL(sum(r.cash),0) as cashAmount ,
	        			IFNULL(sum(r.real_bet),0) as betAmount,
	        			IFNULL(sum(r.zj+r.fandian+r.broker+r.gongzi-r.real_bet),0) as total_coin
	        			FROM `{$this->db_prefix}member_report` r , `{$this->db_prefix}members` m 
	        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end
	        	";
			$data	= $this->db->query($sql, 2); 
	        $month3Arrry['money']=number_format($data['total_coin'], 3, '.', ''); // 盈餘
	        $month3Arrry['betAmount']=number_format($data['betAmount'], 3, '.', ''); // 投注
	        $month3Arrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ''); // 充值
	        $month3Arrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ''); // 提現        
	        $args['data']=array(
	            'month1' => $month1Arrry,
	            'month2' => $month2Arrry,
	            'month3' => $month3Arrry
	            );
			file_put_contents($file, serialize($args));
		}
		$this->display('report/teammoney', $args);
	}

	public function money2()
	{
		$uid = $this->user['uid'];
		$file= $this->getCacheDir().md5(__FUNCTION__.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
			/*
			$sql="select coin,fcoin from `{$this->db_prefix}members` where uid=".$uid." limit 1";
			$member = $this->db->query($sql, 2);
			*/
			$data=$this->money_data2();
			//$args['member']=$member;
			$args['data']=$data;
			file_put_contents($file, serialize($args));
		}
		$this->display('report/money2', $args);
	}
	
	public function teamorder()
	{
		$uid = $this->user['uid'];
		$args=array();
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");
		}
		$where_time = $this->build_where_time('a.actionTime');
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME+300>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
			$sql = "SELECT 
						a.city as betCity,
						IFNULL(sum(a.betAmount),0) as betAmount 
						 FROM `{$this->db_prefix}report_betcity` a , `{$this->db_prefix}members` b 
						WHERE CONCAT(',',b.parents,',') LIKE '%,{$uid},%' and a.uid=b.uid {$where_time} group by a.city
						";
			$data	= $this->db->query($sql, 3);
			foreach($data as $var){
				$args['lottery'][$var['betCity']]=$var;
			}
			file_put_contents($file, serialize($args));
		}
		$this->display('report/teamorder', $args);
	}
	 
	/*
	public function teamlottery()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('a.actionTime');
		$args=array();
		$sql = "SELECT 
					a.type,
					IFNULL(sum(a.mode * a.beiShu * a.actionNum),0) as betAmount ,
					IFNULL(sum(a.actionNum),0) as actionNum ,
					count(a.id) as norder ,
					count(distinct a.uid) as nuid 
					 FROM `{$this->db_prefix}bets_repl` a , `{$this->db_prefix}members` b 
					WHERE CONCAT(',',b.parents,',') LIKE '%,{$uid},%' and a.uid=b.uid and a.isDelete=0 {$where_time} group by a.type
					";
		$data	= $this->db->query($sql, 3);
		foreach($data as $var){
			$args['lottery'][$var['type']]=$var;
		}
		$this->display('report/teamlottery', $args);
	}
	 * 
	 */
	public function teamlottery_chart()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('a.actionTime');
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME+300>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
			$sql="select 
						a.type,
						IFNULL(sum(a.amount),0) as betAmount ,
						IFNULL(sum(a.actionNum),0) as actionNum ,
						IFNULL(sum(a.orders),0) as norder ,
						count(distinct a.uid) as nuid 
						 FROM `{$this->db_prefix}report_type` a , `{$this->db_prefix}members` b 
						WHERE CONCAT(',',b.parents,',') LIKE '%,{$uid},%' and a.uid=b.uid  {$where_time} group by a.type
						";
			$data	= $this->db->query($sql, 3);
			foreach($data as $var){
				$args['lottery'][$var['type']]=$var;
			}
			file_put_contents($file, serialize($args));
		}
		$this->display('report/teamlottery_chart', $args);
	}
	public function teamplay()
	{
		$uid = $this->user['uid'];
		$this->get_time(false);
		if(!isset($_POST['fromTime']))
		{
			$this->request_time_from=strtotime("-7 day");;
		}
		$where_time = $this->build_where_time('a.actionTime');
		$file= $this->getCacheDir().md5(__FUNCTION__.$where_time.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME+300>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
			$sql="select
						a.playId as playedId,
						IFNULL(sum(a.amount),0) as betAmount ,
						IFNULL(sum(a.actionNum),0) as actionNum ,
						IFNULL(sum(a.zjAmount),0) as bonus ,
						count(a.orders) as norder, 
						count(distinct a.uid) as nuid
						 FROM `{$this->db_prefix}report_play` a , `{$this->db_prefix}members` b 
						WHERE CONCAT(',',b.parents,',') LIKE '%,{$uid},%' and a.uid=b.uid {$where_time} group by a.playId				
				";
			$data	= $this->db->query($sql, 3);
			foreach($data as $var){
				$args['teamplay'][$var['playedId']]=$var;
			}
			file_put_contents($file, serialize($args));
		}
		$this->display('report/teamplay', $args);
	}
	public function team()
	{
		$uid = $this->user['uid'];
		$file= $this->getCacheDir().md5(__CLASS__.__FUNCTION__.$uid);
		$args=array();
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME>$this->time)
		{
			$args= unserialize(file_get_contents($file));
		}else{
		
			$start	= strtotime('today');
	        $end	= $this->time;
			$sql = "SELECT count(if( type=1,uid,null))as agent,count(if(type=0,uid,null))as member,count(uid) as total,sum(coin) as total_coin, 
						(select count(uid) FROM `{$this->db_prefix}members` WHERE parentId={$uid} and type=0 AND `regTime` BETWEEN {$start} AND {$end}) as new_member ,
						(select count(b.uid) FROM `{$this->db_prefix}members` a , {$this->db_prefix}member_session b  WHERE a.parentId={$uid} and a.uid=b.uid and b.isOnLine=1 and b.accessTime > ".(time()-MEMBER_SESSIONTIME)." )  as online_member
						from `{$this->db_prefix}members` 		
					where parentId={$uid}";
			$fdata	= $this->db->query($sql, 2);		
	
			$sql = "SELECT count(if( type=1,uid,null))as agent,count(if(type=0,uid,null))as member,count(uid) as total,sum(coin) as total_coin, 
						(select CONCAT(count(if( type=1,uid,null)),',',count(if(type=0,uid,null))) FROM `{$this->db_prefix}members` WHERE CONCAT(',',parents,',') LIKE '%,{$uid},%' AND `regTime` BETWEEN {$start} AND {$end}) as new ,
						(select count(b.uid) FROM `{$this->db_prefix}members` a , {$this->db_prefix}member_session b  WHERE a.parentId={$uid} and a.uid=b.uid and b.isOnLine=1 and b.accessTime > ".(time()-MEMBER_SESSIONTIME)." )  as online_member
						from `{$this->db_prefix}members` 		
					where CONCAT(',',parents,',') LIKE '%,{$uid},%'";
			$tdata	= $this->db->query($sql, 2);
			$temp=explode(",", $tdata['new']) ;
			$tdata['new_agent']	=$temp[0];
			$tdata['new_member']	=$temp[1];
	
			$sql = "select regArea,count(regArea) as co FROM `{$this->db_prefix}members` WHERE CONCAT(',',parents,',') LIKE '%,{$uid},%' and type=1 group by regArea";
			$areaAgent	= $this->db->query($sql, 3);
			
			$sql = "select regArea,count(regArea) as co FROM `{$this->db_prefix}members` WHERE CONCAT(',',parents,',') LIKE '%,{$uid},%' and type=0 group by regArea";
			$areaMember	= $this->db->query($sql, 3);
	
		        $args=array(
			        'fmember'=>$fdata,
					'tmember'=>$tdata,
					'areaAgent'=>$areaAgent,
					'areaMember'=>$areaMember
	        );
	        file_put_contents($file, serialize($args));
	    }	    
		$this->display('report/team',$args);
	}

    private function money_data2()
    {
        $uid	= $this->user['uid'];
        $start	= strtotime('today');
        $end	= $this->time;
		$todayArrry=array();
		$yestodayArrry=array();
		$month1Arrry=array();
		$month2Arrry=array();
		$month3Arrry=array();
/*
        $sql="select 
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(bet),0) as betAmount,
        			IFNULL(sum(zj),0) as zjAmount,
        			IFNULL(sum(fandian),0) as fanDianAmount,
        			IFNULL(sum(broker),0) as brokerageAmount,
        			IFNULL(sum(gongzi),0) as gongzi,
        			IFNULL(sum(bonus),0) as bonusAmount,
        			IFNULL(sum(transfer),0) as transfer,
        			IFNULL(sum(cancelOrder),0) as cancelOrderAmount 
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";
        $data	= $this->db->query($sql, 2); 
        $total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['bonusAmount']+$data['cancelOrderAmount']+$data['gongzi']+$data['transfer'] - $data['betAmount'];
        $todayArrry['money']=number_format($total, 3, '.', ''); // 今日盈餘
        $todayArrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ''); // 今日充值
        $todayArrry['bonusAmount']=number_format($data['bonusAmount'], 3, '.', ''); // 今日分紅
        $todayArrry['transfer']=number_format($data['transfer'], 3, '.', ''); // 今日轉帳(上級+下級)
        $todayArrry['gongzi']=number_format($data['gongzi'], 3, '.', ''); // 今日工資
        $todayArrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ''); // 今日提款
        $todayArrry['brokerageAmount']=number_format($data['brokerageAmount'], 3, '.', ''); // 今日活动奖金
        $todayArrry['fanDianAmount']=number_format($data['fanDianAmount'], 3, '.', ''); // 今日返点
		
        $end	= strtotime('today');
        $start	= $end - 86400;
        $end	= $end-1;
        $sql="select 
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(bet),0) as betAmount,
        			IFNULL(sum(zj),0) as zjAmount,
        			IFNULL(sum(fandian),0) as fanDianAmount,
        			IFNULL(sum(broker),0) as brokerageAmount,
        			IFNULL(sum(gongzi),0) as gongzi,
        			IFNULL(sum(bonus),0) as bonusAmount,
        			IFNULL(sum(transfer),0) as transfer,
        			IFNULL(sum(cancelOrder),0) as cancelOrderAmount 
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";
        $data	= $this->db->query($sql, 2); 
		$total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['bonusAmount']+$data['cancelOrderAmount']+$data['gongzi']+$data['transfer'] - $data['betAmount'];
        $yestodayArrry['money']=number_format($total, 3, '.', ''); // 昨日盈餘
        $yestodayArrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ''); // 昨日充值
        $yestodayArrry['bonusAmount']=number_format($data['bonusAmount'], 3, '.', ''); // 昨日分紅
        $yestodayArrry['transfer']=number_format($data['transfer'], 3, '.', ''); // 昨日轉帳(上級+下級)
        $yestodayArrry['gongzi']=number_format($data['gongzi'], 3, '.', ''); // 昨日工資
        $yestodayArrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ''); // 昨日提款
        $yestodayArrry['brokerageAmount']=number_format($data['brokerageAmount'], 3, '.', ''); // 昨日活动奖金
        $yestodayArrry['fanDianAmount']=number_format($data['fanDianAmount'], 3, '.', ''); // 昨日返点
*/        
        // 上周統計
		$base = strtotime("+0 week Sunday");        
        $end	= $base;
        $start	= strtotime("-1 week Sunday");
		$end	= $end-1;
		/*
        // 上月統計
		$base = strtotime(date('Y-m',time()) . '-01 00:00:00');        
        $end	= $base;
        $start	= strtotime('-1 month', $base);
		$end	= $end-1;
		 *
		 */
		 /*
        $sql="select
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(real_bet),0) as betAmount,
        			IFNULL(sum(zj+fandian+broker+bonus+gongzi-real_bet),0) as total_coin
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";*/
        $sql="select
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(real_bet),0) as betAmount,
        			IFNULL(sum(zj+fandian+broker+gongzi-real_bet),0) as total_coin
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";
		$data	= $this->db->query($sql, 2); 
        $month1Arrry['money']=number_format($data['total_coin'], 3, '.', ''); // 盈餘
        $month1Arrry['betAmount']=number_format($data['betAmount'], 3, '.', ''); // 投注
        $month1Arrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ''); // 充值
        $month1Arrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ''); // 提現

        // 上上周統計
		$base = strtotime("-1 week Sunday");        
        $end	= $base;
        $start	= strtotime("-2 week Sunday");
		$end	= $end-1;
		/*
        // 上上月統計
        $end	= strtotime('-1 month', $base);
        $start	= strtotime('-2 month', $base);
        $end	= $end-1;
		 * 
		 */
		 /*
        $sql="select
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(real_bet),0) as betAmount,
        			IFNULL(sum(zj+fandian+broker+bonus+gongzi-real_bet),0) as total_coin
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";*/
        $sql="select
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(real_bet),0) as betAmount,
        			IFNULL(sum(zj+fandian+broker+gongzi-real_bet),0) as total_coin
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";
		$data	= $this->db->query($sql, 2); 
        $month2Arrry['money']=number_format($data['total_coin'], 3, '.', ''); // 盈餘
        $month2Arrry['betAmount']=number_format($data['betAmount'], 3, '.', ''); // 投注
        $month2Arrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ''); // 充值
        $month2Arrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ''); // 提現

        // 上上上周統計
		$base = strtotime("-2 week Sunday");        
        $end	= $base;
        $start	= strtotime("-3 week Sunday");
		$end	= $end-1;
		/*
        // 上上上月統計
        $end	= strtotime('-2 month', $base);
        $start	= strtotime('-3 month', $base);
        $end	= $end-1;
		 * 
		 */
		 /*
        $sql="select
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(real_bet),0) as betAmount,
        			IFNULL(sum(zj+fandian+broker+bonus+gongzi-real_bet),0) as total_coin
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";*/
        $sql="select
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(real_bet),0) as betAmount,
        			IFNULL(sum(zj+fandian+broker+gongzi-real_bet),0) as total_coin
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";
        $data	= $this->db->query($sql, 2); 
        $month3Arrry['money']=number_format($data['total_coin'], 3, '.', ''); // 盈餘
        $month3Arrry['betAmount']=number_format($data['betAmount'], 3, '.', ''); // 投注
        $month3Arrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ''); // 充值
        $month3Arrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ''); // 提現        
         return array(
            'today' => $todayArrry,
            'yestoday' => $yestodayArrry,
            'month1' => $month1Arrry,
            'month2' => $month2Arrry,
            'month3' => $month3Arrry
            );
    }
    private function money_data()
    {
        $uid	= $this->user['uid'];
        $start	= strtotime('today');
        $end	= $this->time;
		$todayArrry=array();
		$yestodayArrry=array();
		$TeamTD=array();
		$TeamYE=array();
        $sql="select 
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(real_bet),0) as betAmount,
        			IFNULL(sum(zj),0) as zjAmount,
        			IFNULL(sum(fandian),0) as fanDianAmount,
        			IFNULL(sum(broker),0) as brokerageAmount,
        			IFNULL(sum(gongzi),0) as gongzi,
        			IFNULL(sum(bonus),0) as bonusAmount,
        			IFNULL(sum(transfer),0) as transfer,
        			IFNULL(sum(cancelOrder),0) as cancelOrderAmount, 
        			IFNULL(sum(recharge_wc),0) as recharge_wc,
        			IFNULL(sum(recharge_ali),0) as recharge_ali,
        			IFNULL(sum(recharge_qq),0) as recharge_qq,
        			IFNULL(sum(recharge_qp),0) as recharge_qp,
        			IFNULL(sum(recharge_bank),0) as recharge_bank
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";
        $data	= $this->db->query($sql, 2); 
		//$total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['bonusAmount']+$data['cancelOrderAmount']+$data['gongzi'] - $data['betAmount'];
        $total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['gongzi'] - $data['betAmount'];
        $todayArrry['money']=number_format($total, 3, '.', ','); // 今日盈餘
        $todayArrry['betAmount']=number_format($data['betAmount'], 3, '.', ','); // 今日投注
        $todayArrry['zjAmount']=number_format($data['zjAmount'], 3, '.', ','); // 今日中獎
        $todayArrry['fanDianAmount']=number_format($data['fanDianAmount'], 3, '.', ','); // 今日反點
        $todayArrry['brokerageAmount']=number_format($data['brokerageAmount'], 3, '.', ','); // 今日活动奖金
        $todayArrry['transfer']=number_format($data['transfer'], 3, '.', ','); // 今日轉帳(上級+下級)
        $todayArrry['gongzi']=number_format($data['gongzi'], 3, '.', ','); // 今日工資
        $todayArrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ','); // 今日充值
        $todayArrry['recharge_wc']=number_format($data['recharge_wc'], 3, '.', ','); // 今日微信
        $todayArrry['recharge_ali']=number_format($data['recharge_ali'], 3, '.', ','); // 今日支付寶
        $todayArrry['recharge_qq']=number_format($data['recharge_qq'], 3, '.', ','); // 今日QQ錢包
        $todayArrry['recharge_qp']=number_format($data['recharge_qp'], 3, '.', ','); // 今日快捷支付
        $todayArrry['recharge_bank']=number_format($data['recharge_bank'], 3, '.', ','); // 今日網銀
        $todayArrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ','); // 今日提款
        
        $sql="select 
        			IFNULL(sum(bonus),0) as bonusAmount
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid 
        	";
        $data	= $this->db->query($sql, 2);
		$todayArrry['bonusAmount']=number_format($data['bonusAmount'], 3, '.', ','); // 今日累計分紅 
        
        $sql="select 
        			IFNULL(sum(r.recharge),0) as rechargeAmount,
        			IFNULL(sum(r.cash),0) as cashAmount ,
        			IFNULL(sum(r.real_bet),0) as betAmount,
        			IFNULL(sum(r.zj),0) as zjAmount,
        			IFNULL(sum(r.fandian),0) as fanDianAmount,
        			IFNULL(sum(r.broker),0) as brokerageAmount,
        			IFNULL(sum(r.gongzi),0) as gongzi,
        			IFNULL(sum(r.bonus),0) as bonusAmount,
        			IFNULL(sum(r.transfer),0) as transfer,
        			IFNULL(sum(r.cancelOrder),0) as cancelOrderAmount,
        			IFNULL(sum(r.recharge_wc),0) as recharge_wc,
        			IFNULL(sum(r.recharge_ali),0) as recharge_ali,
        			IFNULL(sum(recharge_qq),0) as recharge_qq,
        			IFNULL(sum(recharge_qp),0) as recharge_qp,
        			IFNULL(sum(r.recharge_bank),0) as recharge_bank
        			FROM `{$this->db_prefix}member_report` r , `{$this->db_prefix}members` m 
        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end
        	";
        $data	= $this->db->query($sql, 2); 
        //$total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['bonusAmount']+$data['cancelOrderAmount']+$data['gongzi'] - $data['betAmount'];
        $total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['gongzi'] - $data['betAmount'];
        $TeamTD['money']=number_format($total, 3, '.', ','); // 今日盈餘
        $TeamTD['betAmount']=number_format($data['betAmount'], 3, '.', ','); // 今日投注
        $TeamTD['zjAmount']=number_format($data['zjAmount'], 3, '.', ','); // 今日中獎
        $TeamTD['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ','); // 今日充值
        $TeamTD['cashAmount']=number_format($data['cashAmount'], 3, '.', ','); // 今日提款
        $TeamTD['recharge_wc']=number_format($data['recharge_wc'], 3, '.', ','); // 今日微信
        $TeamTD['recharge_ali']=number_format($data['recharge_ali'], 3, '.', ','); // 今日支付寶
        $TeamTD['recharge_qq']=number_format($data['recharge_qq'], 3, '.', ','); // 今日QQ錢包
        $TeamTD['recharge_qp']=number_format($data['recharge_qp'], 3, '.', ','); // 今日快捷支付
        $TeamTD['recharge_bank']=number_format($data['recharge_bank'], 3, '.', ','); // 今日網銀
        
		$end	= strtotime('today');
        $start	= $end - 86400;
        $end	= $end-1;
        $sql="select 
        			IFNULL(sum(recharge),0) as rechargeAmount,
        			IFNULL(sum(cash),0) as cashAmount ,
        			IFNULL(sum(real_bet),0) as betAmount,
        			IFNULL(sum(zj),0) as zjAmount,
        			IFNULL(sum(fandian),0) as fanDianAmount,
        			IFNULL(sum(broker),0) as brokerageAmount,
        			IFNULL(sum(gongzi),0) as gongzi,
        			IFNULL(sum(bonus),0) as bonusAmount,
        			IFNULL(sum(transfer),0) as transfer,
        			IFNULL(sum(cancelOrder),0) as cancelOrderAmount, 
        			IFNULL(sum(recharge_wc),0) as recharge_wc,
        			IFNULL(sum(recharge_ali),0) as recharge_ali,
        			IFNULL(sum(recharge_qq),0) as recharge_qq,
        			IFNULL(sum(recharge_qp),0) as recharge_qp,
					IFNULL(sum(recharge_bank),0) as recharge_bank
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
        	";
        $data	= $this->db->query($sql, 2); 
        //$total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['bonusAmount']+$data['cancelOrderAmount']+$data['gongzi'] - $data['betAmount'];
        $total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['gongzi'] - $data['betAmount'];
        $yestodayArrry['money']=number_format($total, 3, '.', ','); // 昨日盈餘
        $yestodayArrry['betAmount']=number_format($data['betAmount'], 3, '.', ','); // 昨日投注
        $yestodayArrry['zjAmount']=number_format($data['zjAmount'], 3, '.', ','); // 昨日中獎
        $yestodayArrry['fanDianAmount']=number_format($data['fanDianAmount'], 3, '.', ','); // 昨日反點
        $yestodayArrry['brokerageAmount']=number_format($data['brokerageAmount'], 3, '.', ','); // 昨日活动奖金
        $yestodayArrry['transfer']=number_format($data['transfer'], 3, '.', ','); // 昨日轉帳(上級+下級)
        $yestodayArrry['gongzi']=number_format($data['gongzi'], 3, '.', ','); // 昨日工資
        $yestodayArrry['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ','); // 昨日充值
        $yestodayArrry['recharge_wc']=number_format($data['recharge_wc'], 3, '.', ','); // 昨日微信
        $yestodayArrry['recharge_ali']=number_format($data['recharge_ali'], 3, '.', ','); // 昨日支付寶
        $yestodayArrry['recharge_qq']=number_format($data['recharge_qq'], 3, '.', ','); // 今日QQ錢包
        $yestodayArrry['recharge_qp']=number_format($data['recharge_qp'], 3, '.', ','); // 今日快捷支付
        $yestodayArrry['recharge_bank']=number_format($data['recharge_bank'], 3, '.', ','); // 昨日網銀
        $yestodayArrry['cashAmount']=number_format($data['cashAmount'], 3, '.', ','); // 昨日提款
        
        $sql="select 
        			IFNULL(sum(r.recharge),0) as rechargeAmount,
        			IFNULL(sum(r.cash),0) as cashAmount ,
        			IFNULL(sum(r.real_bet),0) as betAmount,
        			IFNULL(sum(r.zj),0) as zjAmount,
        			IFNULL(sum(r.fandian),0) as fanDianAmount,
        			IFNULL(sum(r.broker),0) as brokerageAmount,
        			IFNULL(sum(r.gongzi),0) as gongzi,
        			IFNULL(sum(r.bonus),0) as bonusAmount,
        			IFNULL(sum(r.transfer),0) as transfer,
        			IFNULL(sum(r.cancelOrder),0) as cancelOrderAmount,
        			IFNULL(sum(r.recharge_wc),0) as recharge_wc,
        			IFNULL(sum(r.recharge_ali),0) as recharge_ali,
        			IFNULL(sum(recharge_qq),0) as recharge_qq,
        			IFNULL(sum(recharge_qp),0) as recharge_qp,
        			IFNULL(sum(r.recharge_bank),0) as recharge_bank
        			FROM `{$this->db_prefix}member_report` r , `{$this->db_prefix}members` m 
        			WHERE CONCAT(',',m.parents,',') LIKE '%,{$uid},%' and r.`uid`=m.uid and r.`actionTime` BETWEEN $start AND $end
        	";
        $data	= $this->db->query($sql, 2); 
        //$total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['bonusAmount']+$data['cancelOrderAmount']+$data['gongzi'] - $data['betAmount'];
        $total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['gongzi'] - $data['betAmount'];
        $TeamYE['money']=number_format($total, 3, '.', ','); // 昨日盈餘
        $TeamYE['betAmount']=number_format($data['betAmount'], 3, '.', ','); // 昨日投注
        $TeamYE['zjAmount']=number_format($data['zjAmount'], 3, '.', ','); // 昨日中獎
        $TeamYE['rechargeAmount']=number_format($data['rechargeAmount'], 3, '.', ','); // 昨日充值
        $TeamYE['cashAmount']=number_format($data['cashAmount'], 3, '.', ','); // 昨日提款
        $TeamYE['recharge_wc']=number_format($data['recharge_wc'], 3, '.', ','); // 昨日微信
        $TeamYE['recharge_ali']=number_format($data['recharge_ali'], 3, '.', ','); // 昨日支付寶
        $TeamYE['recharge_qq']=number_format($data['recharge_qq'], 3, '.', ','); // 今日QQ錢包
        $TeamYE['recharge_qp']=number_format($data['recharge_qp'], 3, '.', ','); // 今日快捷支付
      	$TeamYE['recharge_bank']=number_format($data['recharge_bank'], 3, '.', ','); // 昨日網銀
                
        $sql="select 
        			IFNULL(sum(bonus),0) as bonusAmount
        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` <= $end
        	";
        $data	= $this->db->query($sql, 2);
		$yestodayArrry['bonusAmount']=number_format($data['bonusAmount'], 3, '.', ','); // 昨日累計分紅
		
		$start	= strtotime('today');
        $end	= $this->time;	

		$endY	= strtotime('today');
        $startY	= $endY - 86400;
		$endY = $endY - 1;

		$sql = "SELECT count(if( type=1,uid,null))as agent,count(if(type=0,uid,null))as member,count(uid) as total,sum(coin) as total_coin, 
					(select count(uid) FROM `{$this->db_prefix}members` WHERE CONCAT(',',parents,',') LIKE '%,{$uid},%' AND `regTime` BETWEEN {$start} AND {$end}) as new ,
					(select count(uid) FROM `{$this->db_prefix}members` WHERE CONCAT(',',parents,',') LIKE '%,{$uid},%' AND `regTime` BETWEEN {$startY} AND {$endY}) as newY 
					from `{$this->db_prefix}members` 		
				where CONCAT(',',parents,',') LIKE '%,{$uid},%'";
		$tdata	= $this->db->query($sql, 2);
		$tdata['total_coin']=number_format($tdata['total_coin'], 3, '.', ',');
		$tdata['agent']=number_format($tdata['agent'], 3, '.', ',');
		$tdata['member']=number_format($tdata['member'], 3, '.', ',');
		$sql = "SELECT
					(select count(b.uid) FROM `{$this->db_prefix}members` a , {$this->db_prefix}member_session b  WHERE a.parentId={$uid} and a.uid=b.uid and b.isOnLine=1 and os in ('ipod','ipad','android','iphone') and b.accessTime > ".(time()-MEMBER_SESSIONTIME)." )  as online_mobile,
					(select count(b.uid) FROM `{$this->db_prefix}members` a , {$this->db_prefix}member_session b  WHERE a.parentId={$uid} and a.uid=b.uid and b.isOnLine=1 and os in ('windows nt','windows','macintosh','unix','linux','other') and b.accessTime > ".(time()-MEMBER_SESSIONTIME)." )  as online_pc
					from `{$this->db_prefix}members` 		
				where CONCAT(',',parents,',') LIKE '%,{$uid},%'";
		$data	= $this->db->query($sql, 2);
		$tdata['online_mobile']=$data['online_mobile'];
		$tdata['online_pc']=$data['online_pc'];
		$tdata['TeamYE']=$TeamYE;
		$tdata['TeamTD']=$TeamTD;
         return array(
            'today' => $todayArrry,
            'yestoday' => $yestodayArrry,
            'tdata' => $tdata
            );
    }
    
}
