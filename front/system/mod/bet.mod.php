<?php

class mod_bet extends mod
{
    private function remove($id)
    {
        $this->db->transaction('begin');
        try {
            $data = $this->db->query("SELECT * FROM `{$this->db_prefix}bets` WHERE `id`=$id LIMIT 1", 2);
            if (!$data)
                core::__403();
            if ($data['isDelete'])
                return;
            if ($data['uid'] != $this->user['uid'])
                core::__403();
            if ($data['kjTime'] <= $this->time || $data['lotteryNo'])
                core::error('您提交的下注已经开奖，不能撤单');
            $types = core::lib('game')->get_types();
            $ftime = core::lib('game')->get_type_ftime($data['type']);
            if ($data['kjTime'] - $ftime < $this->time)
                core::error('您提交的下注正在开奖，不能撤单');
            $amount = $data['beiShu'] * $data['mode'] * $data['actionNum'];
            $amount = abs($amount);
			$zikaiZj=abs(($data['mode']/2) * $data['beiShu'] * $data['bonusProp']);
            $this->set_coin(array(
                'uid' => $data['uid'],
                'type' => $data['type'],
                'playedId' => $data['playedId'],
                'liqType' => 7,
                'info' => '撤单',
                'extfield0' => $id,
                'coin' => $amount
            ));
            $this->db->query("UPDATE `{$this->db_prefix}bets` SET `isDelete`=1 WHERE `id`=$id LIMIT 1", 0);

			$zikiArray=array(
				0=>array(0,0,0,0,0,0,0,0,0,0),
				1=>array(0,0,0,0,0,0,0,0,0,0),
				2=>array(0,0,0,0,0,0,0,0,0,0),
				3=>array(0,0,0,0,0,0,0,0,0,0),
				4=>array(0,0,0,0,0,0,0,0,0,0)
			);
			$zikipk10Array=array(
				0=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				1=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				2=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				3=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				4=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				5=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				6=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				7=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				8=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0),
				9=>array('01'=>0,'02'=>0,'03'=>0,'04'=>0,'05'=>0,'06'=>0,'07'=>0,'08'=>0,'09'=>0,'10'=>0)
			);
			$zikaipk10_type=array(75);
			$zikai_type=array(5,71,72,73,74,77,76,79,80,81,82,83,84,85,86,87,88,89);
			if(in_array($data['type'],$zikai_type)&& $data['playedGroup']!=87 && $this->user['forTest']==0 )
			{
				$sql="delete from {$this->db_prefix}ziki_number where md5='".$data['md5']."'";
				$this->db->query($sql, 0);
				$sql="delete from {$this->db_prefix}ziki_zhuihao where md5='".$data['md5']."'";
				$this->db->query($sql, 0);
				$sql="delete from {$this->db_prefix}ziki_sprule where md5='".$data['md5']."'";
				$this->db->query($sql, 0);

				$keyZcID=array(268,269,270,271,272); //組選
				$keyZhID=array(2,3,4,5,6,7,8,9,10,11,12,13,14,15,25,26,27,28,29,30); //時時彩直選
				switch(true)
				{
					case in_array($data['playedId'],$keyZcID):
						$sql="insert {$this->db_prefix}ziki_sprule(type,actionNo,bet,zj,rule_type)values(".$data['type'].",".$data['actionNo'].",".($amount*-1).",".($zikaiZj*-1).",1)";
						$this->db->query($sql, 0);
						break;
					case in_array($data['playedId'],$keyZhID):
						$sql="delete from {$this->db_prefix}ziki_sprule where md5='".$data['md5']."'";
						$this->db->query($sql, 0);						
						break;
				}
			}
			if(in_array($data['type'],$zikaipk10_type) && $data['playedGroup']!=68 && $data['playedGroup']!=70 && $this->user['forTest']==0)
			{
				$this->build_zikipk10_number($zikipk10Array,$data);
				$sql="insert {$this->db_prefix}zikipk10_cnumber";
				$asql=array();
				$bsql=array();
				foreach($zikipk10Array as $k=>$v)
				{
					foreach($v as $k1=>$v1)
					{
						$asql[]="`".$k.$k1."`";
						$bsql[]=$v1;
					}
				}
				$sql=$sql."(`type`,`actionNo`,`serializeId`,".implode(",", $asql).")values(".$data['type'].",".$data['actionNo'].",'".$data['serializeId']."',".implode(",", $bsql).")";
				$this->db->query($sql, 0);
			}
            $this->db->transaction('commit');
        }
        catch (Exception $e) {
            $this->db->transaction('rollBack');
            core::error($e->getMessage());
        }
    }
    public function remove_batch()
    {
        $this->check_post();
        if (!array_key_exists('ids', $_POST) || !is_array($_POST['ids']))
            core::__403();
        foreach ($_POST['ids'] as $id) {
            if (!core::lib('validate')->number($id))
                core::__403();
            $id = intval($id);
            $this->remove($id);
        }
    }
    public function remove_single()
    {
        $this->check_post();
        $id = $this->get_id();
        $this->remove($id);
    }
    private function build_ziki_number(&$zikiArray,$code)
    {
    	$key8q2dxdsID=array(42); //大小單雙前2
    	$key8h2dxdsID=array(43); //大小單雙前2
    	$key8q3dxdsID=array(265); //大小單雙前2
    	$key8h3dxdsID=array(266); //大小單雙前2

    	$key5ID=array(258,268,269,270,271,272); //5星组3组6
		$key4zqID=array(4,5);
		$key4zhID=array(6,7);
		
		$key3zqID=array(10,11);
		$key3zcID=array(287,288);
		$key3zhID=array(12,13);
		$key3z36ID=array(22,23); //任选任3组3组6
		$key3zr36ID=array(24); //任选任三混合组选,对写法不放心关闭玩法
		
		$key2zqID=array(25,26);
		$key2zhID=array(27,28);
		$key2zrID=array(35,36);
		
		$key4hID=array(273,275,276,277);
		$key3qID=array(16,17,18);
		$key3cID=array(289,290,307);
		$key3hID=array(19,20,21);
		
		$keybdwz3ID=array(39,142); //不定位前三1码2码
		$keybdwc3ID=array(40);
		$keybdwh3ID=array(38,143); //不定位后三1码2码
		$keybdw5ID=array(261,262); //不定位5星3码2码
		$keybdw4ID=array(263,264); //不定位后4星3码2码
		$rule=0;
		$a1=0;
		$a2=0;
		$weiShuKey=array();
		//$value=abs($code['mode'] * $code['beiShu']);
		$value=abs(($code['mode']/2) * $code['beiShu']*$code['bonusProp']);
		if($code['weiShu']>0)
		{
            $w = array(
                16 => '万',
                8 => '千',
                4 => '百',
                2 => '十',
                1 => '个'
            );
            foreach ($w as $p => $v) {
                if ($code['weiShu'] & $p)
				{
					switch($p)
					{
						case 16:
							$weiShuKey[]=0;
							break;
						case 8:
							$weiShuKey[]=1;
							break;
						case 4:
							$weiShuKey[]=2;
							break;
						case 2:
							$weiShuKey[]=3;
							break;
						case 1:
							$weiShuKey[]=4;
							break;
					}
				}
            }
		}
    	switch($code['playedId'])
		{
			case in_array($code['playedId'],$keybdw4ID):
				$rule=1;
				$a1=1;
				$a2=4;
				break;
			case in_array($code['playedId'],$keybdw5ID):
				$rule=1;
				$a1=0;
				$a2=4;
				break;
			case in_array($code['playedId'],$keybdwh3ID):
				$rule=1;
				$a1=2;
				$a2=4;
				break;
			case in_array($code['playedId'],$keybdwc3ID):
				$rule=1;
				$a1=1;
				$a2=3;
				break;
			case in_array($code['playedId'],$keybdwz3ID):
				$rule=1;
				$a1=0;
				$a2=2;
				break;
			case in_array($code['playedId'],$key3zr36ID):
				$rule=4;
				break;	
			case in_array($code['playedId'],$key3z36ID):
				$rule=3;
				break;	
			case in_array($code['playedId'],$key2zrID):
				$rule=3;
				break;	
			case in_array($code['playedId'],$key3zqID):
				$rule=2;
				$a1=0;
				break;	
			case in_array($code['playedId'],$key3zcID):
				$rule=2;
				$a1=1;
				break;	
			case in_array($code['playedId'],$key3zhID):
				$rule=2;
				$a1=2;
				break;
			case in_array($code['playedId'],$key2zhID):
				$rule=2;
				$a1=3;
				break;
			case in_array($code['playedId'],$key2zqID):
				$rule=2;
				$a1=0;
				break;
			case in_array($code['playedId'],$key4zhID):
				$rule=2;
				$a1=1;
				break;
			case in_array($code['playedId'],$key4zqID):
				$rule=2;
				$a1=0;
				break;
			case in_array($code['playedId'],$key3hID):
				$rule=1;
				$a1=2;
				$a2=4;
				break;
			case in_array($code['playedId'],$key3cID):
				$rule=1;
				$a1=1;
				$a2=3;
				break;
			case in_array($code['playedId'],$key3qID):
				$rule=1;
				$a1=0;
				$a2=2;
				break;
			case in_array($code['playedId'],$key5ID):
				$rule=1;
				$a1=0;
				$a2=4;
				break;
			case in_array($code['playedId'],$key4hID):
				$rule=1;
				$a1=1;
				$a2=4;
				break;		
			case in_array($code['playedId'],$key8q2dxdsID):
				$rule=5;
				$a1=0;
				break;		
			case in_array($code['playedId'],$key8h2dxdsID):
				$rule=5;
				$a1=3;
				break;		
			case in_array($code['playedId'],$key8q3dxdsID):
				$rule=5;
				$a1=0;
				break;		
			case in_array($code['playedId'],$key8h3dxdsID):
				$rule=5;
				$a1=2;
		}
		switch(true)
		{
			case $rule==0:
		    	$s=explode("|", $code['actionData']);
				foreach($s as $sv)
				{
					$a=explode(",", $sv);
					foreach ($a as $k => $v) {
						if($v!='-')
						{
							$b=str_split($v);
							foreach($b as $k1=>$v1)
							{
								$zikiArray[$k][$v1]+=$value;
							}
						}
					}			
				}
				break;
			case $rule==1:
				$s=str_replace(",","", $code['actionData']);
				$s=str_replace(" ","", $s);
				$b=str_split($s);
				for ($k=$a1;$k<=$a2;$k++) {
					foreach($b as $k1=>$v1)
					{
						$zikiArray[$k][$v1]+=$value;
					}
				}
				break;
			case $rule==2:
		    	$s=explode("|", $code['actionData']);
				foreach($s as $sv)
				{
					$a=explode(",", $sv);
					foreach ($a as $k => $v) {
						$b=str_split($v);
						foreach($b as $k1=>$v1)
						{
							$zikiArray[$k+$a1][$v1]+=$value;
						}
					}			
				}				
				break;
			case $rule==3:
		    	$s=explode("|", $code['actionData']);
				foreach($s as $sv)
				{
		            foreach ($weiShuKey as $p => $v) {
						$b=str_split($sv);
						foreach($b as $k1=>$v1)
						{
							$zikiArray[$v][$v1]+=$value;
						}
		            }
				}				
				break;
			case $rule==4:
		    	$s=explode(",", $code['actionData']);
				foreach($s as $sv)
				{
		            foreach ($weiShuKey as $p => $v) {
						$b=str_split($sv);
						foreach($b as $k1=>$v1)
						{
							$zikiArray[$v][$v1]+=$value;
						}
		            }
				}				
				break;
			case $rule==5:
		    	$s=explode(",", $code['actionData']);
				foreach($s as $k=>$sv)
				{
					$b=array();
					switch($sv)
					{
						case "单":
							$b=array(1,3,5,7,9);
						break;
						case "小":
							$b=array(0,1,2,3,4);
						break;
						case "大":
							$b=array(5,6,7,8,9);
						break;
						case "双":
							$b=array(0,2,4,6,8);
						break;

					}
					foreach($b as $k1=>$v1)
					{
						$zikiArray[$k+$a1][$v1]+=$value;
					}
				}				
				break;
		}
    }

    private function build_zikipk10_number(&$zikipk10Array,$code)
    {
    	$key93ID=array(93,94,95,96); //猜冠军,猜冠亚军,猜前三名,定位胆选
    	$key225ID=array(225,226,227,228,229,230,231,232,233,234); //两面
    	$key244ID=array(244); //冠亚季选一
    	$key247ID=array(247); //冠亚组合
		$rule=0;
		$a1=0;
		$a2=0;
		$weiShuKey=array();
		//$value=abs($code['mode'] * $code['beiShu']);
		$value=abs(($code['mode']/2) * $code['beiShu']*$code['bonusProp']);
    	switch($code['playedId'])
		{
			case in_array($code['playedId'],$key93ID):
				$rule=0;
				break;	
			case in_array($code['playedId'],$key225ID):
				$rule=1;
				break;	
			case in_array($code['playedId'],$key244ID):
				$rule=2;
				break;	
			case in_array($code['playedId'],$key247ID):
				$rule=3;
				break;
		}
		switch(true)
		{
			case $rule==0:
		    	$s=explode(",", $code['actionData']);
				foreach($s as $sk=>$sv)
				{
					if($sv=="-")continue;
					$a=explode(" ", $sv);
					foreach ($a as $k => $v) {
						$zikipk10Array[$sk][$v]+=$value;
					}			
				}
				break;
			case $rule==1:
				$sk=0;
				switch($code['playedId'])
				{
					case 225;
						$sk=0;
					break;
					case 226;
						$sk=1;
					break;
					case 227;
						$sk=2;
					break;
					case 228;
						$sk=3;
					break;
					case 229;
						$sk=4;
					break;
					case 230;
						$sk=5;
					break;
					case 231;
						$sk=6;
					break;
					case 232;
						$sk=7;
					break;
					case 233;
						$sk=8;
					break;					
					case 234;
						$sk=9;
					break;
				}
				$a=preg_split('/(?<!^)(?!$)/u', $code['actionData']); 
				foreach ($a as $k => $v) {
					switch($v)
					{
						case '大';
							$number=array('06','07','08','09','10');
						break;
						case '小';
							$number=array('01','02','03','04','05');
						break;
						case '单';
							$number=array('01','03','05','07','09');
						break;
						case '双';
							$number=array('02','04','06','08','10');
						break;
					}
					foreach($number as $kk => $vv)
					{
						$zikipk10Array[$sk][$vv]+=$value;
					}
				}
				break;
			case $rule==2:
				$pos=array(0,1,2);
		    	$s=explode(" ", $code['actionData']);
				foreach($pos as $kk => $vv)
				{
					foreach($s as $sk=>$sv)
					{
						$zikipk10Array[$vv][$sv]+=$value;			
					}
				}
				break;
			case $rule==3:
				$pos=array(0,1);
		    	$s=explode("-", $code['actionData']);
				print_r($s);
				foreach($pos as $kk => $vv)
				{
					foreach($s as $sk=>$sv)
					{
						if(strlen($sv)<2)
						{
							$sv="0".$sv;
						}
						$zikipk10Array[$vv][$sv]+=$value;			
					}
				}
				break;
		}
    }
    public function info()
    {
        $this->check_post();

		$AES = core::lib('aes');

		if (isset($_GET['uid'])) {
			$uid = $AES->decrypt($AES->urlsafe_b64decode($_GET['uid']));
			if (!is_numeric($uid)) core::__403();
		}
        $id  = $this->get_id();
		$table="{$this->db_prefix}bets_temp".((isset($uid) ? $uid : $this->user['uid'] )%10);
        $bet = $this->db->query("SELECT * FROM `{$table}` WHERE `id`=$id LIMIT 1", 2);
        if (!$bet)
            core::__403();

		//var_dump((isset($uid) ? $uid : $id ));

		$weiShu = $bet['weiShu'];
        $wei    = '';
        if ($weiShu) {
            $w = array(
                16 => '万',
                8 => '千',
                4 => '百',
                2 => '十',
                1 => '个'
            );
            foreach ($w as $p => $v) {
                if ($weiShu & $p)
                    $wei .= $v;
            }
            $wei .= '：';
        }

        $betCont = $bet['mode'] * $bet['beiShu'] * $bet['actionNum'];
        $types   = core::lib('game')->get_types();
        $plays   = $this->get_plays(0,'all');
        $html    = '<div class="detail">';
        $html .= '<table cellpadding="0" cellspacing="0" width="100%">';
        $html .= '<tr>';
        $html .= '<td class="k" width="14%">所属彩种</td>';
        $html .= '<td class="v" width="20%">' . ($types[$bet['type']]['shortName'] ? $types[$bet['type']]['shortName'] : $types[$bet['type']]['title']) . '</td>';
        $html .= '<td class="k" width="13%">订单玩法</td>';
        $html .= '<td class="v" width="20%">' . $plays[$bet['playedId']]['name'] . '</td>';
        $html .= '<td class="k" width="13%">订单状态</td>';
        if ($bet['isDelete'] == 1) {
            $status = '<span class="gray">已撤单</span>';
        } else if (!$bet['lotteryNo']) {
            $status = '<span class="green">未开奖</span>';
        } else if ($bet['zjCount']) {
            $status = '<span class="red">已中奖</span>';
        } else {
            $status = '未中奖';
        }
        $html .= '<td class="v" width="20%">' . $status . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="k">订单编号</td>';
        $html .= '<td class="v">' . $bet['wjorderId'] . '</td>';
        $html .= '<td class="k">倍数模式</td>';
        $html .= '<td class="v">' . $bet['beiShu'] . ' [' . $this->modes[$bet['mode']] . ']</td>';
        $html .= '<td class="k">奖金返点</td>';
        $html .= '<td class="v">' . number_format($bet['bonusProp'], 2, '.', '') . ' - ' . number_format($bet['fanDian'], 1, '.', '') . '%</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="k">开奖号码</td>';
		$_openNum='';
		if($bet['lotteryNo'])
		{
			$_openNum=$bet['lotteryNo'];
			if($types[$bet['type']]['type']==11)
			{
				$_temp=explode(',', $bet['lotteryNo']);
				$_openNum=$_temp[0].'+'.$_temp[1].'+'.$_temp[2].'='.$_temp[3];
			}			
		}
        $html .= '<td class="v">' . $_openNum . '</td>';
        $html .= '<td class="k">投注时间</td>';
        $html .= '<td class="v">' . date('Y-m-d H:i:s', $bet['actionTime']) . '</td>';
        $html .= '<td class="k">投注期号</td>';
        $html .= '<td class="v">' . $bet['actionNo'] . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="k">开奖时间</td>';
        $html .= '<td class="v">' . ($bet['lotteryNo'] ? date('m-d H:i:s', $bet['kjTime']) : '--') . '</td>';
        $html .= '<td class="k">购买注数</td>';
        $html .= '<td class="v">' . $bet['actionNum'] . ' 注</td>';
        $html .= '<td class="k">购买金额</td>';
        $html .= '<td class="v">' . number_format($betCont, 3, '.', '') . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td class="k">返点金额</td>';

        //20161118 自身返點金額減半 george
        //$html .= '<td class="v">'.($bet['fanDian'] ?number_format(($bet['fanDian'] / 100) * $betCont,3,'.','') : '0').' 元</td>';
        $html .= '<td class="v">' . ($bet['fanDian'] ? number_format(($bet['fanDian'] / 200) * $betCont, 3, '.', '') : '0') . ' 元</td>';

        $html .= '<td class="k">中奖金额</td>';
        $html .= '<td class="v">' . ($bet['lotteryNo'] ? number_format($bet['bonus'], 3, '.', '') . ' 元' : '--') . '</td>';
        $html .= '<td class="k">购买盈亏</td>';
        if ($bet['lotteryNo']) {

            //20161118 自身返點金額減半 george
            //$money = number_format($bet['bonus'] -$betCont +($bet['fanDian'] / 100) * $betCont,3,'.','');
            $money = number_format($bet['bonus'] - $betCont + ($bet['fanDian'] / 200) * $betCont, 3, '.', '');

            $loss_gain = ($money > 0 ? '赢' : '亏') . abs($money) . '元';
        } else {
            $loss_gain = '---';
        }
        $html .= '<td class="v">' . $loss_gain . '</td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="6" style="padding: 0px;">';
        $html .= '<div class="actionData">投注号码 : ' . $wei . $bet['actionData'] . '</div>';
        $html .= '</td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';
        $this->dialogue(array(
            'class' => 'big',
            'body' => $html,
            'yes' => array(
                'text' => '确定'
            )
        ));
    }
    public function log()
    {
        if ($this->post) {
            $tpl = $this->ispage ? '/bet/log_body' : '/bet/log';
            $this->get_time();
            $args         = $this->log_get_args();
            $page_current = $this->get_page();
            $game_log     = $this->log_search_func($args, $page_current);
            $page_max     = $this->get_page_max($game_log['total']);
            if ($page_current > $page_max)
                core::__403();
            $page_args = $this->log_page_args($args);
            $this->display($tpl, array(
                'args' => $args,
                '_types' => $this->get_types(),
                'types' => core::lib('game')->get_types(),
                'plays' => $this->get_plays(0,'all'),
                'state' => array(
                    0 => '所有状态',
                    1 => '已中奖',
                    2 => '未中奖',
                    3 => '未开奖',
                    4 => '追号',
                    6 => '撤单'
                ),
                'data' => $game_log['data'],
                'page_current' => $page_current,
                'page_max' => $page_max,
                'page_url' => '/bet/log?' . http_build_query($page_args),
                'page_container' => '#bet-log-dom .body'
            ));
        } else {
            $this->ajax();
        }
    }
    public function log_search()
    {
        $this->check_post();
        $this->get_time(false);
        $args      = $this->log_get_args(false);
        $game_log  = $this->log_search_func($args, 1);
        $page_max  = $this->get_page_max($game_log['total']);
        $page_args = $this->log_page_args($args);
        $this->display('/bet/log_body', array(
            'types' => core::lib('game')->get_types(),
            'plays' => $this->get_plays(0,'all'),
            'data' => $game_log['data'],
            'page_current' => 1,
            'page_max' => $page_max,
            'page_url' => '/bet/log?' . http_build_query($page_args),
            'page_container' => '#bet-log-dom .body'
        ));
    }
    private function log_search_func($args, $page_current)
    {
        $uid      = $this->user['uid'];
        $pagesize = $this->pagesize;
        $skip     = ($page_current - 1) * $pagesize;
		$table="{$this->db_prefix}bets_temp".($uid%10);
        $sql      = "SELECT ~field~ FROM `{$table}` WHERE";
        $where    = " `uid`={$uid}";
        if ($args['type'])
            $where .= " AND `type`={$args['type']}";
        if ($args['state']) {
            switch ($args['state']) {
                case 1:
                    $where .= ' AND `zjCount`>0';
                    break;
                case 2:
                    $where .= " AND `zjCount`=0 AND `lotteryNo`!='' AND `isDelete`=0";
                    break;
                case 3:
                    $where .= " AND `lotteryNo`=''";
                    break;
                case 4:
                    $where .= ' AND `zhuiHao`=1';
                    break;
                case 5:
                    $where .= ' AND `hmEnable`=1';
                    break;
                case 6:
                    $where .= ' AND `isDelete`=1';
                    break;
            }
        }
        if ($args['mode'] !== '0.000')
            $where .= " AND `mode`={$args['mode']}";
        if ($args['betId'])
            $where .= " AND `wjorderId`='{$args['betId']}'";
        $where .= $this->build_where_time('`actionTime`');
        if (substr($where, 0, 5) === ' AND ')
            $where = substr($where, 5);
        $sql .= $where;
        $sql .= ' ~order~ ~limit~';
        $sql_total = str_replace('~field~', 'COUNT(1) AS __total', $sql);
        $sql_total = str_replace('~limit~', '', $sql_total);
        $sql_total = str_replace('~order~', '', $sql_total);
		/* 考虑到用户可能马上撤单,投注记录不使用缓存
		$file= $this->getCacheDir().md5(__FUNCTION__.$where.$uid.$page_current);
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME-240>$this->time)
		{
			$yt= unserialize(file_get_contents($file));
		}else
		{
		*/
	        $total     = $this->db->query($sql_total, 2);
	        $total     = $total['__total'];
	        $sql_data  = str_replace('~field~', '*', $sql);
	        $sql_data  = str_replace('~limit~', "LIMIT $skip,$pagesize", $sql_data);
	        $sql_data  = str_replace('~order~', 'ORDER BY `id` DESC', $sql_data);
	        $data      = $this->db->query($sql_data, 3);
			$yt = array(
	            'data' => $data,
	            'total' => $total
	        ); 
			//file_put_contents($file, serialize($yt));
		//}
		return $yt;
    }
    private function log_get_args($get = true)
    {
        $data          = $get ? $_GET : $_POST;
        $args          = array();
        $args['type']  = (array_key_exists('type', $data) && core::lib('validate')->number($data['type'])) ? intval($data['type']) : 0;
        $args['state'] = (array_key_exists('state', $data) && in_array($data['state'], array(
            0,
            1,
            2,
            3,
            4,
            5,
            6
        ))) ? intval($data['state']) : 0;
        $args['mode']  = (array_key_exists('mode', $data) && array_key_exists($data['mode'], $this->modes)) ? strval($data['mode']) : '0.000';
        $args['betId'] = (array_key_exists('betId', $data) && preg_match('/^[a-zA-Z0-9]{8}$/', $data['betId'])) ? $data['betId'] : '';
        return $args;
    }
    private function log_page_args($args)
    {
        $page_args = array_filter($args);
        if ($this->request_time_from)
            $page_args['fromTime'] = date('Y-m-d 00:00:00', $this->request_time_from);
        if ($this->request_time_to)
            $page_args['toTime'] = date('Y-m-d 23:59:59', $this->request_time_to);
        $page_args['page'] = '{page}';
        return $page_args;
    }
}
?>