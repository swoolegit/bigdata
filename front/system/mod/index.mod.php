<?php

class mod_index extends mod
{
    public function home()
    {
    	if ($this->post) {
		    $this->display('home',array(
		        'recent_bonus_data' => $this->recent_bonus_data(),
		        'yt_bonus_data' => $this->yt_bonus_data(),
		        'agent_data' => $this->user['type'] ? $this->agent_data() : array(),
		        'plays' => $this->get_plays(0, 'all'),
		        'gtypes' => core::lib('game')->get_types(),
		        'bet_data' => $this->bet_data()
		    ));
		}
		else {
			$this->ajax();
		}
    }

    public function software()
    {
        $this->client_type = 'software';
        $this->display('index');
    }
    public function web()
    {
        $this->client_type = 'web';
        $this->display('index', array(
            'recent_bonus_data' => $this->recent_bonus_data(),
            'yt_bonus_data' => $this->yt_bonus_data(),
            'agent_data' => $this->user['type'] ? $this->agent_data() : array(),
            'plays' => $this->get_plays(0, 'all'),
            'gtypes' => core::lib('game')->get_types(),
            'bet_data' => $this->bet_data()
        ));
    }
    public function mobile()
    {
        $this->client_type = 'mobile';
        $this->display('index');
    }
    private function recent_bonus_data()
    {
        $xAxis = $series = '';
        $uid   = $this->user['uid'];
		$file= $this->getCacheDir().md5(__FUNCTION__.$uid);
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME+300>$this->time)
		{
			$yt= unserialize(file_get_contents($file));
		}else
		{		
            $sql  = "SELECT actionDate,(recharge+fandian+gongzi+bonus+cancelOrder+zj+broker-bet) AS `total_coin` FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid order by actionDate desc limit 7";
            $data = $this->db->query($sql, 3);
			$data=array_reverse($data);
			foreach($data as $v){
	            $series .= number_format($v['total_coin'], 3, '.', '') . ',';
	            $xAxis .= '"' . date('m-d', strtotime($v['actionDate'])) . '",';
			}
			$yt=array(
	            'xAxis' => substr($xAxis, 0, -1),
	            'series' => substr($series, 0, -1)
        	);
			file_put_contents($file, serialize($yt));
		}
        return $yt;
    }
    private function yt_bonus_data()
    {
        $uid = $this->user['uid'];
		$file= $this->getCacheDir().md5(__FUNCTION__.$uid);
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME+300>$this->time)
		{
			$yt= unserialize(file_get_contents($file));
		}else
		{
	        $start               = strtotime('today');
	        $end                 = $this->time;
	        $sql="select 
	        			IFNULL(sum(recharge),0) as rechargeAmount,
	        			IFNULL(sum(cash),0) as cashAmount ,
	        			IFNULL(sum(bet),0) as betAmount,
	        			IFNULL(sum(zj),0) as zjAmount,
	        			IFNULL(sum(fandian),0) as fanDianAmount,
	        			IFNULL(sum(broker),0) as brokerageAmount,
	        			IFNULL(sum(gongzi),0) as gongzi,
	        			IFNULL(sum(bonus),0) as bonusAmount,
	        			IFNULL(sum(transfer),0) as transfer,
	        			IFNULL(sum(cancelOrder),0) as cancelOrderAmount 
	        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
	        	";
	        $data	= $this->db->query($sql, 2); 
	        $today_total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['bonusAmount']+$data['cancelOrderAmount']+$data['gongzi']- $data['betAmount'];
			$today_bet=$data['betAmount'];
	        /*
	        $sql_today_money     = "SELECT SUM(coin) AS `total_coin` FROM `{$this->db_prefix}coin_log_repl` WHERE `uid`=$uid AND `liqType` NOT IN(1,8,9,106,107) AND `actionTime` BETWEEN $start AND $end";
	        $data_today_money    = $this->db->query($sql_today_money, 2);
	        $sql_today_bets      = "SELECT SUM(beiShu*mode*actionNum*(fpEnable+1)) betAmount FROM `{$this->db_prefix}bets` WHERE `uid`=$uid AND `isDelete`=0 AND `actionTime` BETWEEN $start AND $end";
	        $data_today_bets     = $this->db->query($sql_today_bets, 2);
			 *
			 */
			 
	        $end                 = strtotime('today');
	        $start               = $end - 86400-1;
	        $sql="select 
	        			IFNULL(sum(recharge),0) as rechargeAmount,
	        			IFNULL(sum(cash),0) as cashAmount ,
	        			IFNULL(sum(bet),0) as betAmount,
	        			IFNULL(sum(zj),0) as zjAmount,
	        			IFNULL(sum(fandian),0) as fanDianAmount,
	        			IFNULL(sum(broker),0) as brokerageAmount,
	        			IFNULL(sum(gongzi),0) as gongzi,
	        			IFNULL(sum(bonus),0) as bonusAmount,
	        			IFNULL(sum(transfer),0) as transfer,
	        			IFNULL(sum(cancelOrder),0) as cancelOrderAmount 
	        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid and `actionTime` BETWEEN $start AND $end
	        	";
	        $data	= $this->db->query($sql, 2); 
	        $yestoday_total=$data['zjAmount']+$data['fanDianAmount']+$data['brokerageAmount']+$data['bonusAmount']+$data['cancelOrderAmount']+$data['gongzi'] - $data['betAmount'];
			$yestoday_bet=$data['betAmount'];
	        /*
	        $sql_yestoday_money  = "SELECT SUM(coin) AS `total_coin` FROM `{$this->db_prefix}coin_log_repl` WHERE `uid`=$uid AND `liqType` NOT IN(1,8,9,106,107) AND `actionTime` BETWEEN $start AND $end";
	        $data_yestoday_money = $this->db->query($sql_yestoday_money, 2);
	        $sql_yestoday_bets   = "SELECT SUM(beiShu*mode*actionNum*(fpEnable+1)) betAmount FROM `{$this->db_prefix}bets` WHERE `uid`=$uid AND `actionTime` BETWEEN $start AND $end AND `isDelete`=0";
	        $data_yestoday_bets  = $this->db->query($sql_yestoday_bets, 2);
			 *
			 */
	        $yt=array(
	            'today' => array(
	                'money' => number_format($today_total, 3, '.', ''),
	                'bets' => number_format($today_bet, 3, '.', '')
	            ),
	            'yestoday' => array(
	                'money' => number_format($yestoday_total, 3, '.', ''),
	                'bets' => number_format($yestoday_bet, 3, '.', '')
	            )
			);
			file_put_contents($file, serialize($yt));
		}		
        return $yt;
    }
    private function agent_data()
    {
        $uid    = $this->user['uid'];
		$file= $this->getCacheDir().md5('agent_data'.$uid);
		if(CACHE_TIME && is_file($file) && filemtime($file)+CACHE_TIME+300>$this->time)
		{
			$yt= unserialize(file_get_contents($file));
		}else
		{
			$sql="select 
	        			IFNULL(sum(fandian+broker+gongzi+bonus),0) as total_coin
	        			FROM `{$this->db_prefix}member_report` WHERE `uid`=$uid 
	        	";
	        $money	= $this->db->query($sql, 2); 
	        //$money  = $this->db->query("SELECT SUM(coin) AS `total_coin` FROM `{$this->db_prefix}coin_log_repl` WHERE `uid`=$uid AND `liqType` IN(2,3,52,53,56)", 2);
	        $child  = $this->db->query("SELECT COUNT(1) count FROM `{$this->db_prefix}members` WHERE `isDelete`=0 AND `parentId`={$uid}", 2);
	        $childs = $this->db->query("SELECT COUNT(1) count FROM `{$this->db_prefix}members` WHERE `isDelete`=0 AND CONCAT(',',parents,',') LIKE '%,{$uid},%'", 2);
	        $yt=array(
	            'money' => number_format($money['total_coin'], 3, '.', ''),
	            'child' => $child['count'],
	            'childs' => $childs['count'] - 1
	        );
			file_put_contents($file, serialize($yt));
		}
		return $yt;
    }
    private function bet_data()
    {
        $uid = $this->user['uid'];
        return $this->db->query("SELECT * FROM `{$this->db_prefix}bets_repl` WHERE `uid`=$uid ORDER BY `id` DESC LIMIT 7", 3);
    }
}
?>