<?php

// 支付回调处理接口
class pay
{

    private $db; // 数据库连接
    private $db_prefix; // 数据库表前缀
    private $config = array(); // 系统配置
    private $time; // 当前时间

    public function __construct()
    {
        try {
            $this->db = new pay_db(DB2_HOST, DB2_PORT, DB2_NAME, DB2_USER, DB2_PASS);
            $this->db_prefix = DB2_PREFIX;
            $config_data = $this->db->query("SELECT * FROM `{$this->db_prefix}params`", 3);
            foreach ($config_data as $v) $this->config[$v['name']] = $v['value'];
            $this->time = time();

        } catch (Exception $e) {
            exit($e->getMessage());
        }
    }


    public function apply_memo($order_no, $memo = '')
    {
        try {
            // 开始事务
            $this->db->transaction('begin');

            // record 订单处理状态
            $this->set_memo($order_no, json_encode($memo));

            $this->db->transaction('commit');
            echo '充值成功';
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            exit($e->getMessage());
        }
    }


    /**
     * @name 支付回调接口
     * @param int order_amount 充值金额
     * @param string order_no 充值订单号
     */
    public function call($order_amount, $order_no, $memo = '')
    {
        try {
            $order_amount = floatval($order_amount);
            $order_no = intval($order_no);

            // 开始事务
            $this->db->transaction('begin');

            // record 订单处理状态
            $this->set_memo($order_no, json_encode($memo));


            // 充值金额强制校验
            //$this->check_amount($order_amount);
            // 获取充值信息
            $info_recharge = $this->get_recharge_info($order_no);
            
			// 充值金额强制校验
			if (in_array($info_recharge['bankId'],array(1,19,20,21,24,25))) {
				$rechargeMinQR = floatval($this->config['rechargeMinQR']); //最小充值金额
				$rechargeMaxQR = floatval($this->config['rechargeMaxQR']); //最大充值金额		
				if ($order_amount < $rechargeMinQR || $order_amount > $rechargeMaxQR) throw new Exception('充值失败[扫码]: 您的充值金额不符合系统设置');			
			}
			else {
			
				$rechargeMin = floatval($this->config['rechargeMin']); //最小充值金额
				$rechargeMax = floatval($this->config['rechargeMax']); //最大充值金额
				if ($order_amount < $rechargeMin || $order_amount > $rechargeMax) throw new Exception('充值失败[网银]: 您的充值金额不符合系统设置');

	
			}			
			// 订单处理状态校验
            $this->check_state($info_recharge);
            // 获取用户信息
            $info_user = $this->get_user_info($info_recharge['uid']);
            // 根据系统配置的充值赠送比例计算赠送金额
            $recharge_complimentary = $this->get_recharge_complimentary($order_amount);
            // 执行充值操作
            $this->set_amount($info_user['uid'], $recharge_complimentary, $info_user['coin'], $order_amount, $order_no, $info_recharge['id'], $info_recharge['bankId']);
            // 每天首次充值的金额达到系统设置则返回佣金
            $this->set_rebate($info_user['uid'], $order_amount, $order_no, $info_recharge['id']);
            // 上家、上上家充值赠送
            $this->doCzzsUpstream($info_user['parentId'], $order_amount, $order_no, $info_recharge['id']);
            // 提交事务
            $this->db->transaction('commit');
            echo '充值成功';
        } catch (Exception $e) {
            $this->db->transaction('rollBack');
            exit($e->getMessage());
        }
    }

    private function set_rebate($uid, $order_amount, $order_no, $recharge_id)
    {
        // 查检是否当天首次充值
        $time = strtotime('00:00');
        $sql = "SELECT `id` FROM `{$this->db_prefix}member_recharge` WHERE `rechargeTime`>=$time AND `uid`=$uid limit 1,1";
        $data = $this->db->query($sql, 2);
        if (!$data && $order_amount > floatval($this->config['rechargeCommissionAmount'])) {
            $log = array(
                'type' => 0,
                'fcoin' => 0,
                'liqType' => 52,
                'info' => '充值佣金',
                'extfield0' => $order_no,
                'extfield1' => $recharge_id,
                'extfield2' => '',
            );
            // 返回上家佣金
            $parent_id = $this->set_rebate_func($uid, floatval($this->config['rechargeCommission']), $log);
            if ($parent_id) $this->set_rebate_func($parent_id, floatval($this->config['rechargeCommission2']), $log);
        }
    }

    private function set_rebate_func($uid, $recharge_commission, $log)
    {
        // 获取账户的上家
        $sql = "SELECT `parentId` FROM `{$this->db_prefix}members` WHERE `uid`=$uid LIMIT 1";
        $data = $this->db->query($sql, 2);
        if (!$data) return null;
        $parent_id = $data['parentId'];
        // 存在上家并且系统设置返回佣金则执行返回佣金操作
        if ($parent_id && $recharge_commission) {
            $log['coin'] = $recharge_commission;
            $log['uid'] = $parent_id;
            $this->set_coin($log);
        }
        // 返回上家用户ID
        return $parent_id;
    }

    public function set_memo($order_no, $memo)
    {
        // 更新充值信息
        $sql = "UPDATE `{$this->db_prefix}member_recharge` SET `memo`=? WHERE `rechargeId`=?";
        $this->db->_update($sql, [$memo, $order_no]);
    }


    private function set_amount($uid, $recharge_complimentary, $old_coin, $order_amount, $order_no, $recharge_id,$bank_id=0)
    {
        // 更新充值信息
        $new_amount = $order_amount + $recharge_complimentary;
        $sql = "UPDATE `{$this->db_prefix}member_recharge` SET `state`=1,`rechargeAmount`=$new_amount,`coin`=$old_coin,rechargeTime=UNIX_TIMESTAMP() WHERE `rechargeId`='$order_no' LIMIT 1";
        $this->db->query($sql, 0);
        // 添加充值帐变
        $log = array(
            'coin' => $order_amount,
            'fcoin' => 0,
            'uid' => $uid,
            'liqType' => 1,
            'type' => 0,
            'info' => '充值',
            'extfield0' => $recharge_id,
            'extfield1' => $order_no,
            'extfield2' => $bank_id,
        );
        $this->set_coin($log);
        // 添加充值赠送帐变
        if ($recharge_complimentary) {
            $log = array(
                'coin' => $recharge_complimentary,
                'fcoin' => 0,
                'uid' => $uid,
                'liqType' => 54,
                'type' => 0,
                'info' => '充值赠送',
                'extfield0' => $recharge_id,
                'extfield1' => $order_no,
                'extfield2' => '',
            );
            $this->set_coin($log);
        }
    }

    // 上家、上上家充值赠送
    private function doCzzsUpstream($parent_id, $order_amount, $order_no, $recharge_id)
    {
        if (! $parent_id) {
            return;
        }

        // 上家
        $czzs_parent = intval($this->config['czzsParent']);
        if ($czzs_parent) {
            $recharge_complimentary = number_format($order_amount * $czzs_parent / 100.00, 2, '.', '');
            if ($recharge_complimentary) {
                // 添加充值赠送帐变
                $this->set_coin(array(
                    'coin' => $recharge_complimentary,
                    'fcoin' => 0,
                    'uid' => $parent_id,
                    'liqType' => 54,
                    'type' => 0,
                    'info' => '充值赠送',
                    'extfield0' => $recharge_id,
                    'extfield1' => $order_no,
                    'extfield2' => '上家'
                ));
            }
        }

        // 上上家
        $czzs_top = intval($this->config['czzsTop']);
        if ($czzs_top) {
            $sql = "SELECT parentId FROM {$this->db_prefix}members WHERE uid =:uid";
            $parent = $this->db->getRow($sql, array('uid' => $parent_id));
            if (!$parent || !$parent['parentId']) {
                return;
            }

            $recharge_complimentary = number_format($order_amount * $czzs_top / 100.00, 2, '.', '');
            if ($recharge_complimentary) {
                // 添加充值赠送帐变
                $this->set_coin(array(
                    'coin' => $recharge_complimentary,
                    'fcoin' => 0,
                    'uid' => $parent['parentId'],
                    'liqType' => 54,
                    'type' => 0,
                    'info' => '充值赠送',
                    'extfield0' => $recharge_id,
                    'extfield1' => $order_no,
                    'extfield2' => '上上家'
                ));
            }
        }
    }

    private function set_coin($log)
    {
        static $default = array(
            'coin' => 0,
            'fcoin' => 0,
            'uid' => 0,
            'liqType' => 0,
            'type' => 0,
            'info' => '',
            'extfield0' => 0,
            'extfield1' => '',
            'extfield2' => '',
        );
        $sql = 'call setCoin(';
        foreach ($default as $k => $v) {
            $val = (array_key_exists($k, $log) && $log[$k]) ? $log[$k] : $v;
            if ($v !== 0) $val = "'$val'";
            $sql .= $val . ',';
        }
        $sql = substr($sql, 0, -1) . ')';
        $this->db->query($sql, 0);
    }

    private function get_recharge_complimentary($order_amount)
    {
        $czzs = intval($this->config['czzs']);
        return $czzs > 0 ? number_format($order_amount * $czzs / 100.00, 2, '.', '') : 0;
    }

    private function get_user_info($uid)
    {
        $info = $this->db->query("SELECT * FROM `{$this->db_prefix}members` WHERE `uid`=$uid LIMIT 1", 2);
        if (!$info) throw new Exception('充值失败: 充值用户已被删除或不存在');
        return $info;
    }

    private function get_recharge_info($id)
    {
        $info = $this->db->query("SELECT * FROM `{$this->db_prefix}member_recharge` WHERE `rechargeId`='$id' LIMIT 1", 2);
        if (!$info) throw new Exception('充值失败: 订单号不存在');
        return $info;
    }

    private function check_state($info)
    {
        if ($info['state'] != 0 || !$info['uid']) throw new Exception('充值失败: 订单已被处理');
    }

    private function check_amount($amount)
    {
        $rechargeMin = floatval($this->config['rechargeMin']); //最小充值金额
        $rechargeMax = floatval($this->config['rechargeMax']); //最大充值金额
        if ($amount < $rechargeMin || $amount > $rechargeMax) throw new Exception('充值失败: 您的充值金额不符合系统设置');
    }
}

class pay_db
{

    private $db;

    public function __construct($host, $port, $name, $user, $pass)
    {
        $dsn = 'mysql:host=' . $host . ';dbname=' . $name . ';port=' . $port;
        try {
            $this->db = new PDO($dsn, $user, $pass, array(
                PDO::ATTR_PERSISTENT => false,
                PDO::ATTR_CASE => PDO::CASE_NATURAL,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_AUTOCOMMIT => false,
                PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            ));
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function query($sql, $return)
    {
        try {
            switch ($return) {
                case 0:
                    $result = $this->db->exec($sql);
                    break;

                case 1:
                    $this->db->exec($sql);
                    $result = $this->db->lastInsertId();
                    break;

                case 2:
                case 3:
                    $query = $this->db->query($sql);
                    $action = $return === 2 ? 'fetch' : 'fetchAll';
                    $result = call_user_func_array(array($query, $action), array(PDO::FETCH_ASSOC));
                    $query->closeCursor();
                    break;

                default:
            }
            return $result;
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function transaction($command)
    {
        try {
            switch ($command) {
                case 'begin':
                    $this->db->beginTransaction();
                    break;
                case 'commit':
                    $this->db->commit();
                    break;
                case 'rollBack':
                    $this->db->rollBack();
                    break;
                default:
            }
        } catch (Exception $e) {
            throw $e;
        }
    }


    /* my addon */
    public function getRow($sql, $params = null)
    {
        $stmt = $this->db->prepare($sql);

        if (!is_array($params)) {
            // 如果传入的是一个值
            $params = array($params);
        }
        $stmt->execute($params);

        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $return = $stmt->fetch();
        $stmt = null;

        return $return;
    }

    public function getRows($sql, $params = null)
    {
        $stmt = $this->db->prepare($sql);
        if (!is_array($params)) {
            // 如果传入的是一个值
            $params = array($params);
        }
        //$this->setCharset(utf8);
        $stmt->db->execute($params);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $return = $stmt->fetchAll();
        $stmt = null;
        return $return;
    }

    public function _update($query, $params = null)
    {
        return $this->_insert($query, $params);
    }

    public function _delete($query, $params = null)
    {
        return $this->_update($query, $params);
    }

    public function setCharset($charset)
    {
        if ($charset && $this->charset != $charset) {
            $this->db->charset = $charset;
            $this->db->query('set names ' . $charset);
        }
    }

    public function _insert($query, $params = null)
    {
        if ($params && !is_array($params)) $params = array($params);
        if ($params) {
            if (!$stmt = $this->db->prepare($query)) {
                throw new Exception('解析查询语句出错，SQL语句：' . $query);
            }

            if (!$return = $stmt->execute($params)) {
                $err = $stmt->errorInfo();
                throw new Exception(end($err));
            }
            return $return;
        } else {
            if ($this->db->exec($query)) {
                return true;
            } else {
                $err = $this->db->errorInfo();
                throw new Exception(end($err));
            }
        }
    }

    public function _insertRow($table, $data){
        $sql="insert into $table(";
        $values='';
        foreach($data as $key=>$val){
            if($values){
                $sql.=', ';
                $values.=', ';
            }
            $sql.="`$key`";
            $values.=":$key";
        }
        $sql.=") values($values)";

        return $this->_insert($sql, $data);
    }

    public function _updateRows($table, $data, $where)
    {
        $sql = "update $table set";
        foreach ($data as $key => $_v) $sql .= " $key=:$key,";
        $sql = rtrim($sql, ',') . " where $where";
        return $this->_update($sql, $data);
    }
    /* end:my addon */

    public function __destruct()
    {
        $this->db = null;
    }

}