<?php

class lib_KaiJiang {
	// 总和值大小单双
	public function ssczhdxds($data) {
		$data = explode(',', $data);
		$val=$data[0]+$data[1]+$data[2]+$data[3]+$data[4];
		$r=array();
		switch(true)
		{
			case $val>=23:
				$r[]=$val."大";
				break;
			case $val<=22:
				$r[]=$val."小";
				break;
			case $val%2==0:
				$r[]="双";
				break;
			case $val%2==1:
				$r[]="单";
				break;
		}
		return $r;
	}
	
	// 龙虎和
	public function ssclhhz($data) {
		$data = explode(',', $data);
		$val=$data[0]+$data[1]+$data[2]+$data[3]+$data[4];
		$r=array();
		switch(true)
		{
			case $data[0]>$data[4]:
				$r[]="龙";
				break;
			case $data[0]<$data[4]:
				$r[]="虎";
				break;
			case $data[0]==$data[4]:
				$r[]="和";
				break;
		}
		return $r;
	}

	// 牛牛
	public function nini($data) {
		$val = explode(',', $data);
		$c=$this->check_nini($val);
		if(count($c)==0) return array("没牛");
		$v=$this->re2num($val,$c);
		$v2=($v[0]+$v[1])%10;
		$r=array(); 
		switch(true)
		{
			case $v2==1:
				$r[]="牛1";
				$r[]="牛小";
				$r[]="牛单";
				break;
			case $v2==2:
				$r[]="牛2";
				$r[]="牛小";
				$r[]="牛双";
				break;
			case $v2==3:
				$r[]="牛3";
				$r[]="牛小";
				$r[]="牛单";
				break;
			case $v2==4:
				$r[]="牛4";
				$r[]="牛小";
				$r[]="牛双";
				break;
			case $v2==5:
				$r[]="牛5";
				$r[]="牛小";
				$r[]="牛单";
				break;
			case $v2==6:
				$r[]="牛6";
				$r[]="牛大";
				$r[]="牛双";
				break;
			case $v2==7:
				$r[]="牛7";
				$r[]="牛大";
				$r[]="牛单";
				break;
			case $v2==8:
				$r[]="牛8";
				$r[]="牛大";
				$r[]="牛双";
				break;
			case $v2==9:
				$r[]="牛9";
				$r[]="牛大";
				$r[]="牛单";
				break;
			case $v2==0:
				$r[]="牛牛";
				$r[]="牛大";
				$r[]="牛双";
				break;
		}
		return $r;
	}
	public function check_nini($val) {
		foreach ($val as $k => $v) {
			array_shift($val);
			if(count($val)<=1)break;
			$val2=$val;
			foreach ($val as $k1 => $v1) {
				array_shift($val2);
				foreach($val2 as $k2=>$v2)
				{
					if(($v+$v1+$v2)%10===0)
					{
						return array($v,$v1,$v2);
					}
				}
			}
		}
		return array();
	}
	public function re2num($a,$b) {
		$key = array_search($b[0], $a);
		array_splice($a, $key, 1);
		$key = array_search($b[1], $a);
		array_splice($a, $key, 1);
		$key = array_search($b[2], $a);
		array_splice($a, $key, 1);
		return $a;
	}
}