<?php

class lib_game {

	private $db; // 数据库连接
	private $db_prefix; // 数据库表前缀
	private $time; // 当前时间
	private $types = array(); // 彩种列表
	private $ftimes = array(); // 彩种延迟时间列表

	public function __construct() {
		$this->db = core::lib('db');
		$this->db_prefix = DB_PREFIX;
		$this->time = time();
	}

	// 获取彩种列表
	public function get_types() {
		if($this->types) return $this->types;
		$sql = "SELECT * FROM `{$this->db_prefix}type` WHERE `isDelete`=0 ORDER BY `sort` ASC";
		$data = $this->db->query($sql, 3);
		foreach ($data as $v) $this->types[$v['id']] = $v;
		return $this->types;
	}
	// 取得參數
	public function getParams($name = '') {
		$sql = "SELECT * FROM `{$this->db_prefix}params` WHERE `name`='{$name}'";
		$data = $this->db->query($sql, 2);
		return $data;
	}

	 //获取当期时间
	public function get_game_current_time($type_id, $old = 0) {
		$current = $this->get_game_no($type_id);
    	$actionTime = strtotime($current['actionTimeFull']);
		if (!$actionTime) $actionTime = $old;
		return $actionTime;
	}

	public function get_game_type_all_time($type_id) {
		$type_id = intval($type_id);
		$data = $this->db->query("SELECT * FROM `{$this->db_prefix}data_time` WHERE type= $type_id ORDER BY actionNo", 3);
        return $data;
	}
	/**
	 * @name 获取延迟时间
	 * @param int type_id 彩种ID
	 */
	public function get_type_ftime($type_id) {
		if (!array_key_exists($type_id, $this->ftimes)) {
			$ftime = $this->db->query("SELECT `data_ftime` FROM `{$this->db_prefix}type` WHERE `id`=$type_id LIMIT 1", 2);
			$ftime = $ftime ? $ftime['data_ftime'] : 0;
			$this->ftimes[$type_id] = $ftime;
		}
		return $this->ftimes[$type_id];
	}

	// 期号格式化
	private function no_format($no) {
		$no = str_replace('-', '', $no);
		//$no = preg_replace('/[0]{2,}(\d{1,})$/', '0$1', $no);
		return $no;
	}

    //獲取當日最後一期獎期
    public function get_today_last($type_id){
        $atimedb=$this->db_prefix.'data_time';
        $time=time();
		$sql="select actionNo, actionTime from {$atimedb} where type=$type_id  order by actionNo desc limit 1";
		$return = $this->db->getRow($sql);
        $types=$this->get_types();
		if(($fun=$types[$type_id]['onGetNoed']) && method_exists($this, $fun)){
			$this->$fun($return['actionNo'], $return['actionTime'], $time);
		}
		return $return['actionNo'];
	}
	
	/**
	 * @name API读取下期期号
	 * @param int type_id 彩种ID
	 * @param int time 时间，默认为当前时间
	 */
	public function get_game_no_api($type_id){
		$type=intval($type_id);
		$ftime = $this->get_type_ftime($type_id);
		//$time=$this->time + $ftime;
		$time=time() + $ftime;
		if($type==90){ //六合彩
			$atime=date('Y-m-d H:i:s', $time);
			$atimedb=$this->db_prefix.'lhc_time';
		}else{
			$atime=date('H:i:s', $time);
			$atimedb=$this->db_prefix.'data_time';
		}
		$sql="select actionNo, actionTime, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', actionTime)  AS actionTimeFull from {$atimedb} where type=$type and actionTime>? order by actionTime limit 1";
		$return = $this->db->getRow($sql, $atime);
		if(!$return){
			$sql="select actionNo, actionTime, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL +1 DAY), '%Y-%m-%d'), ' ', actionTime)   AS actionTimeFull from {$atimedb} where type=$type order by actionTime limit 1";
			$return =$this->db->getRow($sql, $atime);
			$time=$time+24*3600;
		}
		$types=$this->get_types();
		if(($fun=$types[$type]['onGetNoed']) && method_exists($this, $fun)){
			$this->$fun($return['actionNo'], $return['actionTime'], $time);
		}
		if($type==90){ //六合彩
			$return['actionTimeFull']=$return['actionTime'];
		}
		return $return;
	}

	/**
	 * @name 读取上期(当前期)期号
	 * @param int type_id 彩种ID
	 * @param int time 时间，默认为当前时间
	 */
	public function get_game_last_no_api($type_id) {
		$type=intval($type_id);
		$ftime = $this->get_type_ftime($type_id);
		$time=$this->time + $ftime;
		if($type==90){ //六合彩
			$atime=date('Y-m-d H:i:s', $time);
			$atimedb=$this->db_prefix.'lhc_time';
		}else{
			$atime=date('H:i:s', $time);
			$atimedb=$this->db_prefix.'data_time';
		}

		$sql="select actionNo, actionTime,CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', actionTime)  AS actionTimeFull from {$atimedb} where type=$type and actionTime<=? order by actionTime desc limit 1";
		$return = $this->db->getRow($sql, $atime);				
		if(!$return){
			$sql="select actionNo, actionTime,CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', actionTime)  AS actionTimeFull from {$atimedb} where type=$type order by actionNo desc limit 1";
			$return =$this->db->getRow($sql, $atime);
			//$return['actionTime']=date('Y-m-d ', $time-24*3600).$return['actionTime'];
			$time=$time-24*3600;
		}
		$types=$this->get_types();
		if(($fun=$types[$type]['onGetNoed']) && method_exists($this, $fun)){
			$this->$fun($return['actionNo'], $return['actionTime'], $time);
		}
		return $return;
	}

	/**
	 * @name 读取下期期号
	 * @param int type_id 彩种ID
	 * @param int time 时间，默认为当前时间
	 */
	public function get_game_no($type_id, $time=null){
		/*
		$type_id = intval($type_id);
		if($time===null) $time = $this->time;
		$ftime = $this->get_type_ftime($type_id);

        $aFulltime = date('Y-m-d H:i:s', $time + $ftime);

        $sql = <<<EOT
select * from (
select *, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), '%Y-%m-%d'), ' ', actionTime)   AS actionTimeFull,
	CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), '%Y-%m-%d'), ' ', stopTime)   AS stopTimeFull
from {$this->db_prefix}data_time
where type=$type_id

UNION DISTINCT

select *, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 1 DAY), '%Y-%m-%d'), ' ', actionTime)   AS actionTimeFull,
	CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 1 DAY), '%Y-%m-%d'), ' ', stopTime)   AS stopTimeFull
from {$this->db_prefix}data_time
where type=$type_id

UNION DISTINCT

select *, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', actionTime)   AS actionTimeFull,
	CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', stopTime)   AS stopTimeFull
from {$this->db_prefix}data_time
where type=$type_id
) as t
where actionTimeFull>?
order by actionTimeFull ASC limit 1
EOT;

        $result = $this->db->getRow($sql, $aFulltime);
//var_dump($result);
		$types = $this->get_types();
		if(($func = $types[$type_id]['onGetNoed']) && method_exists($this, $func)) {
			$this->$func($result['actionNo'], $result['actionTimeFull'], $time);
		}
//var_dump($result);
		$result['actionNo'] = $this->no_format($result['actionNo']);
		//print_r($result);
        return $result;
		*/

		$type=intval($type_id);
		if($time===null) $time=$this->time;
		if($type==90){ //六合彩
			$atime=date('Y-m-d H:i:s', $time);
			$atimedb=$this->db_prefix.'lhc_time';
		}else{
			$atime=date('H:i:s', $time);
			$atimedb=$this->db_prefix.'data_time';
		}

		$sql="select actionNo, actionTime, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', actionTime)   AS actionTimeFull from {$atimedb} where type=$type and actionTime>? order by actionTime limit 1";
		$return = $this->db->getRow($sql, $atime);
		if(!$return){
			$sql="select actionNo, actionTime, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL +1 DAY), '%Y-%m-%d'), ' ', actionTime)   AS actionTimeFull from {$atimedb} where type=$type order by actionTime limit 1";
			$return =$this->db->getRow($sql, $atime);
			$time=$time+24*3600;
		}
		$types=$this->get_types();
		if(($fun=$types[$type]['onGetNoed']) && method_exists($this, $fun)){
			$this->$fun($return['actionNo'], $return['actionTime'], $time);
		}
		if($type==90){ //六合彩
			$return['actionTimeFull']=$return['actionTime'];
		}
		return $return;
	}

	/**
	 * @name 读取上期(当前期)期号
	 * @param int type_id 彩种ID
	 * @param int time 时间，默认为当前时间
	 */
	public function get_game_last_no($type_id, $time = null) {
		/*
		 * 看不懂這SQL在寫什麼
		$type_id = intval($type_id);
		if($time===null) $time = $this->time;
		$ftime = $this->get_type_ftime($type_id);

        $aFulltime = date('Y-m-d H:i:s', $time + $ftime);
        $sql = <<<EOT
select * from (
select *, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), '%Y-%m-%d'), ' ', actionTime)   AS actionTimeFull,
	CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 DAY), '%Y-%m-%d'), ' ', stopTime)   AS stopTimeFull
from {$this->db_prefix}data_time
where type=$type_id

UNION DISTINCT

select *, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 1 DAY), '%Y-%m-%d'), ' ', actionTime)   AS actionTimeFull,
	CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 1 DAY), '%Y-%m-%d'), ' ', stopTime)   AS stopTimeFull
from {$this->db_prefix}data_time
where type=$type_id

UNION DISTINCT

select *, CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', actionTime)   AS actionTimeFull,
	CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', stopTime)   AS stopTimeFull
from {$this->db_prefix}data_time
where type=$type_id
) as t
where actionTimeFull<=?
order by actionTimeFull DESC limit 1
EOT;


        $result = $this->db->getRow($sql, $aFulltime);
//var_dump($result);
		$types = $this->get_types();
//var_dump($types[$type_id]['onGetNoed']);
		if (($func = $types[$type_id]['onGetNoed']) && method_exists($this, $func)) {
			$this->$func($result['actionNo'], $result['actionTimeFull'], $time);
		}
//var_dump($result);
		$result['actionNo'] = $this->no_format($result['actionNo']);

		return $result;
		*/
		
		$type=intval($type_id);
		if($time===null) $time=$this->time;
		if($type==90){ //六合彩
			$atime=date('Y-m-d H:i:s', $time);
			$atimedb=$this->db_prefix.'lhc_time';
		}else{
			$atime=date('H:i:s', $time);
			$atimedb=$this->db_prefix.'data_time';
		}

		$sql="select actionNo, actionTime,CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', actionTime)  AS actionTimeFull from {$atimedb} where type=$type and actionTime<=? order by actionTime desc limit 1";
		$return = $this->db->getRow($sql, $atime);		
		if(!$return){
			$sql="select actionNo, actionTime,CONCAT(DATE_FORMAT(DATE_ADD(NOW(), INTERVAL 0 DAY), '%Y-%m-%d'), ' ', actionTime)  AS actionTimeFull from {$atimedb} where type=$type order by actionNo desc limit 1";
			$return =$this->db->getRow($sql, $atime);
			//$return['actionTime']=date('Y-m-d ', $time-24*3600).$return['actionTime'];
			$time=$time-24*3600;
		}
		$types=$this->get_types();
		if(($fun=$types[$type]['onGetNoed']) && method_exists($this, $fun)){
			$this->$fun($return['actionNo'], $return['actionTime'], $time);
		}
		return $return;
	}

	// 获取近期期号
	public function get_game_recent_no($type_id, $num) {
		$type_id = intval($type_id);
		$time = $this->time;
		$ftime = $this->get_type_ftime($type_id);
		$action_time = date('H:i:s', $time + $ftime);

		$where = "WHERE `type`=$type_id AND `actionTime`<='$action_time'";
		$data = $this->db->query("SELECT COUNT(1) AS `__total` FROM `{$this->db_prefix}data_time` $where", 2);
		$total = $data['__total'] ? $data['__total'] : 1;
		$skip = $total > $num ? $num : $total - 1;
		$sql = "SELECT `actionNo`,`actionTime` FROM `{$this->db_prefix}data_time` $where ORDER BY `actionTime` DESC LIMIT $skip,1";
		$result = $this->db->query($sql, 2);

		if (!$result) {
			$sql = "SELECT `actionNo`,`actionTime` FROM `{$this->db_prefix}data_time` WHERE `type`=$type_id ORDER BY `actionNo` DESC LIMIT 1";
			$result = $this->db->query($sql, 2);
			$time = $time - 24*3600;
		}

		$types = $this->get_types();
		if (($func = $types[$type_id]['onGetNoed']) && method_exists($this, $func)) {
			$this->$func($result['actionNo'], $result['actionTime'], $time);
		}

		$result['actionNo'] = $this->no_format($result['actionNo']);


        return $result;
	}

	// 获取近期期号
	public function get_game_next_nos($type_id, $num) {
		$type_id = intval($type_id);
		$time = $this->time;
		$ftime = $this->get_type_ftime($type_id);
		$action_time = date('H:i:s', $time + $ftime);

		$where = "WHERE `type`=$type_id AND `actionTime`>='$action_time'";
		$data = $this->db->query("SELECT COUNT(1) AS `__total` FROM `{$this->db_prefix}data_time` $where", 2);
		$total = $data['__total'] ? $data['__total'] : 1;
		$limit = $num ? ($total > $num ? $num : $total) : $total;
		$sql = "SELECT `actionNo`,`actionTime` FROM `{$this->db_prefix}data_time` $where ORDER BY `actionTime` ASC LIMIT {$limit}";
		$result = $this->db->query($sql, 3);

		if (!$result) {
			$sql = "SELECT `actionNo`,`actionTime` FROM `{$this->db_prefix}data_time` WHERE `type`=$type_id ORDER BY `actionNo` ASC LIMIT {$num}";
			$result = $this->db->query($sql, 3);
			$time = $time - 24*3600;
		}

		$types = $this->get_types();
		if (($func = $types[$type_id]['onGetNoed']) && method_exists($this, $func)) {
			foreach ($result as &$r) {
				$this->$func($r['actionNo'], $r['actionTime'], $time);
				$r['actionNo'] = $this->no_format($r['actionNo']);
			}
		}

		return $result;
	}

	private function setTimeNo(&$actionTime, &$time=null) {
		if (!preg_match('/^(\d{1,2}\:){2}\d{1,2}$/', $actionTime)) {
		    //core::error('开奖时间表中时间数据错误');
        } else {
            //相容舊格式用!!!
			if (!$time) $time = $this->time;
            $actionTime = date('Y-m-d ', $time) . $actionTime;
        }
	}

    //重庆时时彩
	private function noHdCQSSC(&$actionNo, &$actionTime, $time=null) {
		if (!is_numeric($actionNo)) core::error('开奖时间表中期号数据错误');
        $this->setTimeNo($actionTime, $time);
        if ($actionNo >= 120) {
            $_t = strtotime($actionTime) - 24 * 3600;
        } else {
            $_t = strtotime($actionTime);
        }

        $actionNo = date('Ymd', $_t) . str_pad(intval($actionNo), 3, '0', STR_PAD_LEFT);
	}

    //江西11选5 
	private function no0Hd(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		$no = substr(1000 + $actionNo, 1);
		if (substr($no, 0, 1) === '0') $no = substr($no, 1);
		$actionNo = date('Ymd', strtotime($actionTime)).$no;
	}

    //山东11选5 广东11选5 重慶快樂10分
	private function no0Hd_1(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		if($actionNo>84){
			$time-=24*3600;
		}
		//$actionNo=date('ymd', $time).substr(100+$actionNo,1);
		// $this->setTimeNo($actionTime, $time);
		$actionNo = date('Ymd', strtotime($actionTime)).substr(100 + $actionNo, 1);
	}

    //江苏快3 
	private function no0Hd_2(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		$actionNo=date('ymd', $time).substr(100+$actionNo,1);
		//$actionNo = date('Ymd', strtotime($actionTime)).substr(1000 + $actionNo, 1);
	}
	//廣西快3 廣東快樂10分
	private function no0Hd_3(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		$actionNo = date('Ymd', $time).substr(1000 + $actionNo, 1);
	}

    //福彩3D
	private function pai3(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		$result = $this->getParams('pai3dDiffDay');
		$actionNo = date('Yz', strtotime($actionTime)) + intval($result['value']);

		$actionNo = substr($actionNo, 0, 4).substr(substr($actionNo, 4) + 1001, 1);
	}

	private function pai3x(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		$result = $this->getParams('pai3xDiffDay');
		$actionNo = date('Yz', strtotime($actionTime)) + intval($result['value']);
		$actionNo = substr($actionNo, 0, 4).substr(substr($actionNo, 4) + 1001, 1);
		if($actionTime < date('Y-m-d H:i:s', $time)) $actionTime = date('Y-m-d 20:30', $time);
	}

    //新疆时时彩
	private function noxHd(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);

        if ($actionNo >= 84) {
            $_t = strtotime($actionTime) - 24 * 3600;
        } else {
            $_t = strtotime($actionTime);
        }

		$actionNo = date('Ymd', $_t).substr(1000 + $actionNo, 1);

	}

    //北京PK拾
	private function BJpk10(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		//$actionNo = 179*(strtotime(date('Y-m-d', $time))-strtotime('2007-11-18'))/3600/24+$actionNo-1267-1273+$this->settings['bjpk10DiffDay'];
		$result = $this->getParams('bjpk10DiffDay');
		$actionNo = 729391+ 44*(strtotime(date('Y-m-d', $time))-strtotime('2019-02-11'))/3600/24+$actionNo+intval($result['value']);

		// $this->setTimeNo($actionTime, $time);
		// $result = $this->getParams('bjpk10DiffDay');
		// if (!$result['value']) $result['value'] = -1267;
		// $actionNo = 179 * (strtotime(date('Y-m-d', strtotime($actionTime))) - strtotime('2007-11-18')) / 3600 / 24 + $actionNo + intval($result['value']);
	}

    //澳洲分分彩
    //西班牙3分彩
    //匈牙利5分彩
    private function no0Hdx(&$actionNo, &$actionTime, $time=null) {
        $this->setTimeNo($actionTime, $time);
        $_t = strtotime($actionTime);
        $actionNo = date('Ymd', $_t).substr(10000 + $actionNo, 1);
    }

	private function Kuai8(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
        //$actionNo = 179 * (strtotime(date('Y-m-d', $time)) - strtotime('2004-09-19')) / 3600 / 24 + $actionNo - 77 - 1253;
        //$actionNo = 179 * (strtotime(date('Y-m-d', $time)) - strtotime('2004-09-19')) / 3600 / 24 + $actionNo - 77 - 1253-2527;
		//$TimeCheck=((strtotime(date('Y-m-d', $time)) - strtotime('2017-07-17'))/86400);
        //$TimeCheck=((strtotime(date('Y-m-d', strtotime($actionTime))) - strtotime('2017-07-17'))/86400);
		/*
        if($TimeCheck<=1)
		{
			$TimeCheck=0;
		}
		 * 
		 */
		//$actionNo=834579+($TimeCheck*179)+ $actionNo-703;
		$this->setTimeNo($actionTime, $time);
		$result = $this->getParams('bjkl8DiffDay');
		//$actionNo = 935368 + 179 *(strtotime(date('Y-m-d'), $time) - strtotime('2019-02-11'))/3600/24+$actionNo+$this->settings['bjkl8DiffDay'];
		$actionNo = 935368+ 179*(strtotime(date('Y-m-d', $time))-strtotime('2019-02-11'))/3600/24+$actionNo+intval($result['value']);
		
	}

	private function noHd(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		$actionNo = date('Ymd', $time).substr(100 + $actionNo, 1);
	}

    //PK拾 幸運飛艇
	private function FTpk10(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);

        if ($actionNo >= 132) {
            $_t = strtotime($actionTime) - 24 * 3600;
        } else {
            $_t = strtotime($actionTime);
        }

		$actionNo = date('Ymd', $_t).substr(1000 + $actionNo, 1);

	}

    //東京1.5分彩
	private function JPFFC(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);

        if ($actionNo <= 39) {
            $_t = strtotime($actionTime) + 24 * 3600;
        } else {
            $_t = strtotime($actionTime);
        }
		$actionNo = date('Ymd', $_t).substr(1000 + $actionNo, 1);

	}

    //韓國1.5分彩
	private function KRFFC(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		$actionNo = floor((strtotime(date('Y-m-d',time())) - strtotime('2012-01-30')) / 86400 )  * 960 +117+ $actionNo;
	}

	//台灣賓果twbingo
	private function TWFFC(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		$_t = strtotime($actionTime);
		$actionNo=(date('Y', $_t)-1911)*1000000+$actionNo;
		$actionNo+=(date('z', $_t))*203;
	}

    //菲律賓1.5分彩
	private function PHFFC(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);

        if ($actionNo == 920) {
            $_t = strtotime($actionTime) - (24 * 3600);
        } else {
            $_t = strtotime($actionTime);
        }
		$actionNo = date('Ymd', $_t).substr(1000 + $actionNo, 1);
	}
	// 六合彩
	public function no6Hd(&$actionNo, &$actionTime, $time=null){
		if(!$time) $time=$this->time;
		$actionNo=substr(date('Yz', $time),0,4).substr(1000+$actionNo,1);
	}
	// 今彩539
	public function no5Hd(&$actionNo, &$actionTime, $time=null){
		$this->setTimeNo($actionTime, $time);
		$_t = strtotime($actionTime);
		$actionNo=(date('Y', $_t)-1911)*1000000+$this->get_weekend_days(date('Y', $_t)."-01-01",date('Y-m-d', $_t),true);
		//$actionNo+=(date('z', $_t))*203;
	}

	// 计算一段日期内的周末天数 (是否返回工作日，默认是返回休息日)
	public function get_weekend_days($start_date,$end_date,$is_workday = false){
		if (strtotime($start_date) > strtotime($end_date)) list($start_date, $end_date) = array($end_date, $start_date);
		$start_reduce = $end_add = 0;
		$start_N = date('N',strtotime($start_date));
		$start_reduce = ($start_N == 7) ? 1 : 0;
		$end_N = date('N',strtotime($end_date));
		in_array($end_N,array(6,7)) && $end_add = ($end_N == 7) ? 2 : 1;
		$alldays = abs(strtotime($end_date) - strtotime($start_date))/86400 + 1;
		$weekend_days = floor(($alldays + $start_N - 1 - $end_N) / 7) * 2 - $start_reduce + $end_add;
		if ($is_workday){
		$workday_days = $alldays - $weekend_days;
		return $workday_days;
		}
		return $weekend_days;
	}
}