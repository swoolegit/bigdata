<?php

class lib_vendor {
	public function StartGame($vendor,$game,$username){
		global $conf;
		if($conf["vendor"][$vendor]["enable"]=="0")
		{
			core::json_err('游戏尚未开放');
		}
		$vendor_file = SYSTEM . '/lib/vendor/' .$vendor . '.php';
		if (!is_file($vendor_file)) throw new Exception('错误厂商');
		require($vendor_file);
		$API = new $vendor;
		return $result = $API->StartGame($game,$username);
	}
	public function getBalance($vendor,$game,$username){
		global $conf;
		if($conf["vendor"][$vendor]["enable"]=="0")
		{
			core::json_err('游戏尚未开放');
		}
		$vendor_file = SYSTEM . '/lib/vendor/' .$vendor . '.php';
		if (!is_file($vendor_file)) throw new Exception('错误厂商');
		require($vendor_file);
		$API = new $vendor;
		return $result = $API->getBalance($game,$username);
	}
}