<?php
class lib_middle {
    public function __construct(){
    }

	public function bankPay($json,$middleUrl) {
		$AES = core::lib('aes');
		$encode = $AES->urlsafe_b64encode($AES->encrypt(json_encode($json)));
		$url = $middleUrl.'?j='.urlencode($encode);
		header("Location:{$url}");
		exit;
	}

	public function onlinePay($json,$middleUrl) {

		$AES = core::lib('aes');
		$encode = $AES->urlsafe_b64encode($AES->encrypt(json_encode($json)));
		$url = $middleUrl.'?j='.urlencode($encode);		
		$result = file_get_contents($url);

		return $result;
	}

}


