<?php
class CG {
	private $key = '';
    private $url = '';
    private $client = '';
	private $iv = '';
    public $prefix = '';
    private $channelId = '';
    
	function __construct()
    {
		global $conf;
        $this->prefix = $conf['prefix']."_";
        $this->key = $conf['vendor']['CG']['KEY'];
        $this->iv = $conf['vendor']['CG']['IV'];
        $this->url = $conf['vendor']['CG']['API_URL'];
        $this->client = $conf['vendor']['CG']['CLIENT_URL'];
        $this->channelId = $conf['vendor']['CG']['channelId'];
	}

    function aes256CbcDecrypt($data)
    {
        $raw_key = base64_decode($this->key);
        $raw_iv = base64_decode($this->iv);
        $raw_data = base64_decode($data);

        if (32 !== strlen($raw_key)) {
            return false;
        }
        if (16 !== strlen($raw_iv)) {
            return false;
        }

        //	PHP 6 以前的版本用 mcrypt_decrypt
        //$data = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $raw_key, $raw_data, MCRYPT_MODE_CBC, $raw_iv);
        
        //	PHP 7 以後的版本用 openssl_encrypt
        $data = openssl_decrypt($raw_data, "AES-256-CBC", $raw_key, 0, $raw_iv);
        //$padding = ord($data[strlen($data) - 1]);
        //return substr($data, 0, -$padding);
        return $data;
    }

    function aes256CbcEncrypt($data)
    {
        $raw_key = base64_decode($this->key);
        $raw_iv = base64_decode($this->iv);
        $raw_data = base64_decode($data);
    
        if (32 !== strlen($raw_key)) {
            return false;
        }
        if (16 !== strlen($raw_iv)) {
            return false;
        }
    
        //$padding = 16 - (strlen($raw_data) % 16);
        //$raw_data .= str_repeat(chr($padding), $padding);
        
        //	PHP 6 以前的版本用 mcrypt_decrypt
        // return mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $raw_key, $raw_data, MCRYPT_MODE_CBC, $raw_iv);
        //	PHP 7 以後的版本用 openssl_encrypt
        return openssl_encrypt($raw_data, "AES-256-CBC", $raw_key, 0, $raw_iv);
    }

    public function curl($url,$params)
    {
        $data=$this->aes256CbcEncrypt(base64_encode(json_encode($params)));
        $params = "version=1.0&channelId=".$this->channelId."&data=".$data;
        $command = 'curl --max-time 10 -X POST "'.$this->url.$url.'" -H "Content-Type: application/x-www-form-urlencoded" -d "'.$params.'"';
        //echo $command;
        $result = shell_exec($command);
        $result = json_decode($this->aes256CbcDecrypt(base64_encode($result)),true);
        //echo $decrypted_data;
        return $result;
    }

	public function CreateAcount($userId){
        global $conf;
		$params = [
			'currency' => $conf["vendor"]["CG"]["currency"],
			'accountId' => $this->prefix.$userId,
        ];
        $result = $this->curl('/td_create_account',$params);
        // 使用openssl_encrypt加密的字串會有特殊符號,特殊符號使用http_build_query會破壞原本加密串
		// $params = [
        //     "q" => $q ,
        //     "s"=>$s,
		// ];
        //$command = 'curl -X POST "'.$this->url.'" -H "Content-Type: application/x-www-form-urlencoded" -d "'.http_build_query($params).'"';
        //$xml = simplexml_load_string(shell_exec($command));
        //$result = json_decode($json,true);  
		return $result;
    }

	public function StartGame($gameId,$userId){
        // if($this->getMaintain())
        // {
        //     return false;
        // }
        $result=$this->CreateAcount($userId);
        if(!$result)
        {
            core::json_err('启动游戏失败');
        }
        if($result["errorCode"]!="0" && $result["errorCode"]!="13")
        {
            core::json_err('启动游戏失败 , CODE = '.$result["errorCode"]);
        }
        $url=$this->client."/".$gameId."/?version=1.0&language=cn&channelId=".$this->channelId."&uiType=1&data=".urlencode($this->aes256CbcEncrypt(base64_encode($userId)));
		return $url;
	}

}
?>