<?php
class BLG {
    private $url = '';
    public $prefix = '';
    
	function __construct()
    {
		global $conf;
        $this->prefix = $conf['prefix']."_";
        $this->url = $conf['vendor']['BLG']['API_URL'];
        $this->GAME_url = $conf['vendor']['BLG']['GAME_URL'];
	}


    public function curl($url,$params)
    {
        $command = 'curl --max-time 10 -X POST "'.$this->url.$url.'" -H "Content-Type: application/x-www-form-urlencoded" -d "'.http_build_query($params).'"';
        $result = shell_exec($command);
        $result = json_decode($result,true);
        return $result;
    }

    public function curl_get($url,$params)
    {
        $command = 'curl --max-time 10 -X GET "'.$this->url.$url.'" -H "Content-Type: application/x-www-form-urlencoded" -d "'.http_build_query($params).'"';
        $result = shell_exec($command);
        $result = json_decode($result,true);
        return $result;
    }

    public function curl_game_get($url,$params)
    {
        $command = 'curl --max-time 10 -X GET "'.$this->GAME_url.$url.'" -H "Content-Type: application/x-www-form-urlencoded" -d "'.http_build_query($params).'"';
        $result = shell_exec($command);
        $result = json_decode($result,true);
        return $result;
    }

	public function StartGame($gameId,$userId){
        // if($this->getMaintain())
        // {
        //     return false;
        // }
		$params = [
			'account' => $userId,
            'gameCode' => 'nil',
            'accountPlatform' => $this->prefix,
            'category' => '',
        ];
        //$gameId='ky';
        $result = $this->curl('games/login/'.$gameId,$params);
        if(!$result)
        {
            core::json_err('启动游戏失败');
        }
        if($result["code"]!=200 && !isset($result["data"]["GameUrl"]))
        {
            core::json_err('启动游戏失败 , CODE = '.$result["message"]."(".$result["code"].")");
        }
        $url=$result["data"]["GameUrl"];
		return $url;
    }

	public function getBalance($gameId,$userId){
		$params = [
        ];
        $result = $this->curl_get('wallet/'.$gameId."/".$userId."?accountPlatform=".$this->prefix,$params);
        if(!$result)
        {
            core::json_err('连线失败');
        }
        if($result["code"]!=200 && !isset($result["data"]["Balance"]))
        {
            core::json_err('连线失败 , CODE = '.$result["message"]."(".$result["code"].")");
        }
        $amount=$result["data"]["Balance"];
		return $amount;
	}

	public function transferIn($gameId,$userId,$amount){
		$params = [
			'account' => $userId,
            'amount' => $amount,
            'accountPlatform' => $this->prefix,
        ];
        $result = $this->curl('wallet/deposit/'.$gameId,$params);
        if(!$result)
        {
            core::json_err('连线失败');
        }
        if($result["code"]!=200 && !isset($result["data"]["TransferId"]))
        {
            core::json_err('连线失败 , CODE = '.$result["message"]."(".$result["code"].")");
        }
		return $result["data"]["TransferId"];
    }
    
	public function transferOut($gameId,$userId,$amount){
		$params = [
			'account' => $userId,
            'amount' => $amount,
            'accountPlatform' => $this->prefix,
        ];
        $result = $this->curl('wallet/withdraw/'.$gameId,$params);
        if(!$result)
        {
            core::json_err('连线失败');
        }
        if($result["code"]!=200 && !isset($result["data"]["TransferId"]))
        {
            core::json_err('连线失败 , CODE = '.$result["message"]."(".$result["code"].")");
        }
		return $result["data"]["TransferId"];
    }

	public function GameHistory($args = []){
		$params = [
			'startTime' => $args['startTime'],
            'endTime' => $args['endTime'],
        ];
        $result = $this->curl_game_get($args['game'].'/'.$args['account'].'/betorder?'.http_build_query($params),[]);
        if(!$result)
        {
            return false;
        }
        if($result["code"]!=200 && !isset($result["data"]))
        {
            return false;
        }
		return $result["data"];
	}

}
?>