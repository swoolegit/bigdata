<?php
	$para=$_GET;
	
	// 用户限制
	if($para['username'] && $para['username']!="用户名"){
		$userWhere=" and username like '%{$para['username']}%'";
	}

    if ($para['fromTime'] && $para['toTime']) {
        $fromTime = strtotime($para['fromTime']);
        $toTime = strtotime($para['toTime']) ;
        $betTimeWhere = "and r.actionTime between $fromTime and $toTime";
    } elseif ($para['fromTime']) {
        $fromTime = strtotime($para['fromTime']);
        $betTimeWhere = "and r.actionTime >=$fromTime";
    } elseif ($para['toTime']) {
        $toTime = strtotime($para['toTime']);
        $betTimeWhere = "and r.actionTime < $toTime";
    } else {
        $toTime = strtotime('00:00');
        $fromTime = strtotime('00:00 -7 day');
        $betTimeWhere = "and r.actionTime > $toTime";
    } 
?>

<article class="module width_full">
  <header>
    <h3 class="tabs_involved">分成统计（默认查询的最近七天,可分红总额为(团队盈亏总额)*分成比例）
      <form action="bonus/bonuslists" target="ajax" call="defaultList" dataType="html" class="submit_link wz">
        用户名：
        <input type="text" class="alt_btn" name="username" >
        &nbsp;&nbsp;
        时间：从
        <input type="date" class="alt_btn" name="fromTime" value="<?=date("Y-m-d",$fromTime)?>">
        到
        <input type="date" class="alt_btn" name="toTime" value="<?=date("Y-m-d",$toTime)?>" >
        &nbsp;&nbsp;
        <input type="submit" value="查找" class="alt_btn">
      </form>
    </h3>
  </header>
    <div class="tab_content">
        <?php $this->display("bonus/bonus-lists.php"); ?>
    </div>
</article>
