<?php
	$para=$_GET;
	// 用户限制
	if($para['username'] && $para['username']!="用户名"){
		$userWhere=" and username like '%{$para['username']}%'";
	}

    if ($para['fromTime'] && $para['toTime']) {
        $fromTime = strtotime($para['fromTime']);
        $toTime = strtotime($para['toTime']) ;
        $betTimeWhere = "and r.actionTime between $fromTime and $toTime";
    } elseif ($para['fromTime']) {
        $fromTime = strtotime($para['fromTime']);
        $betTimeWhere = "and r.actionTime >=$fromTime";
    } elseif ($para['toTime']) {
        $toTime = strtotime($para['toTime']);
        $betTimeWhere = "and r.actionTime < $toTime";
    } else {
        $toTime = strtotime('00:00');
        $fromTime = strtotime('00:00 -7 day');
        $betTimeWhere = "and r.actionTime > $toTime";
    } 

	$sql="select * from {$this->prename}members where type=1 and isDelete=0 and enable=1 and fenHong > 0 and  parentId = 0 {$userWhere}";
    $data=$this->getPage($sql, $this->page, $this->pageSize);
    $SonArray=[];
    foreach($data['data'] as $var)
    {
        $sql="select uid from {$this->prename}members where isDelete=0 and enable=1 and CONCAT(',',parents,',') LIKE '%,{$var['uid']},%'";
        $SonData=$this->getRows($sql);
        $temp=[];
        foreach($SonData as $k=>$v)
        {
            $temp[]=$v['uid'];
        }
        $SonArray[$var['uid']]=$temp;
    }

?>
  <table class="tablesorter" cellspacing="0">
    <thead>
      <tr>
        <td>代理账号</td>
        <td>分成比例</td>
        <td>团队投注总额</td>
        <td>团队中奖总额</td>
        <td>团队退水总额</td>
        <td>团队返点总额</td>
        <td>盈亏总额(投注-中奖-退水-返点)</td>
        <td>可分成总额</td>
        <td>上次分红</td>
        <td>操作</td>
        <td>备注</td>
      </tr>
    </thead>
    <tbody>
      <?php  
      foreach($data['data'] as $var){ 
		$fromTime2=$fromTime;
		$toTime2=$toTime;
        $sql="select endTime from {$this->prename}bonus_log where uid=".$var['uid']." order by endTime desc limit 1";
        $endTime=$this->getRow($sql);
		$var['endTime']=0;
		if($endTime)
		{
			$var['endTime']=$endTime['endTime'];
		}
        if($var['endTime'] > $fromTime2)
        {
            $fromTime2 = $var['endTime']+86400;
        }
        $sql="select 
                    IFNULL(sum(r.fandian),0) as fanDianAmount,
                    IFNULL(sum(r.broker),0) as brokerageAmount,
                    IFNULL(sum(r.rebate),0) as rebateAmount,
                    IFNULL(sum(r.bonus),0) as bonusAmount,
                    IFNULL(sum(r.real_bet),0) as betAmount,
                    IFNULL(sum(r.zj),0) as zjAmount
                from  
                     {$this->prename}member_report r 
                where r.uid in (".implode(",",$SonArray[$var['uid']]).") and r.actionTime >= {$fromTime2} and r.actionTime <= {$toTime2}  ";
        $list = $this->getRow($sql);
        $teamwin=($list['zjAmount']+$list['rebateAmount']+$list['fanDianAmount'])-($list['betAmount']);
        $betTotal=$list['betAmount'];
        $zjTotal=$list['zjAmount'];
        $rebateTotal=$list['rebateAmount'];
        $faTotal=$list['fanDianAmount'];
        $fenHongTotal=0;
        if($teamwin<0)
        {
            $fenHongTotal=($teamwin * $var['fenHong'])/100*-1;
        }
	  ?>
      <tr>
            <td><?=$var['username']?></td>
            <td><?=$var['fenHong']?>%</td>
            <td><?=$this->ifs($betTotal,'--')?></td>
            <td><?=$this->ifs($zjTotal,'--')?></td>
            <td><?=$this->ifs($rebateTotal,'--')?></td>
            <td><?=$this->ifs($faTotal,'--')?></td>
            <td><?=$this->ifs(number_format($teamwin,2), '--')?></td>
            <td>
            <?php
            if($fenHongTotal > 0)
            {
                $winmoney=number_format($fenHongTotal,2);
                echo $winmoney;
            }else
            {
                echo "--";
            }
            ?>              
    		</td>
            <td>
            <?php
            if($var['endTime'])
            {
                echo date('Y-m-d', $var['endTime']);
            }else
            {
                echo '--';
            }
            ?>
            </td>
            <td>
        <?php
        if($fenHongTotal > 0)
        {
            ?>
            <a href="<?= "bonus/bonuscheck/?uid={$var['uid']}&fromTime={$fromTime}&toTime={$toTime}" ?>" nopjax="1" target="ajax" call="defaultAjaxLink">发放分红</a>  
            <?php
            ?>     
        <?php
        }else
        {
            echo "--";
        }
        ?>              
            </td>
            <td><?=$var['memo']?></td>
      </tr>
      <?php
       }
      ?>
    </tbody>
  </table>
  <footer>
    <?php
		$rel=get_class($this).'/bonuslist-{page}?'.http_build_query($_GET,'','&');
		$this->display('inc/page.php', 0, $data['total'], $rel, 'defaultReplacePageAction');
?>
  </footer>
