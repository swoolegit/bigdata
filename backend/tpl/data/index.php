<?php
$para = $_GET;
if (!array_key_exists('actionNo', $para)) $para['actionNo'] = '';
//print_r($para);
// 默认取今天的数据
if (isset($para['date']) && $para['date']) {
    $date = strtotime($para['date']);
	$start_date = "00:00:00";
	$end_date = "23:59:59";
	if(strtotime('today') == strtotime($para['date']))
	{
		$start_date = "00:00:00";
		$end_date = date('H:i:s',time());
	}
} else {
    $date = strtotime('today');
	$start_date = "00:00:00";
	$end_date = date('H:i:s',time()+3600);	
}
// 取彩种信息
$sql = "select * from {$this->prename}type where id=?";
$typeInfo = $this->getRow($sql, $this->type);

// 取当前彩种开奖时间表
$table="data_time";
$now_date = date('H:i:s',time());
$whereTime="and actionTime >='{$start_date}' and actionTime <='{$end_date}'";
if($this->type=="90")
{
    $table="lhc_time";
    $now_date = date('Y-m-d H:i:s',time());
    $whereTime="";
}
//$sql = "select * from {$this->prename}{$table} where type={$this->type} {$whereTime} order by actionTime desc";
$this->pageSize=50;
if($para['actionNo'])
{
    $sql = "select * from {$this->prename}{$table} where type={$this->type} order by actionTime desc";
    $times['data'] = $this->getRows($sql);
    $times['total'] = 1;
}else
{
    $sql = "select * from {$this->prename}{$table} where type={$this->type} order by actionTime desc";
    $times = $this->getPage($sql, $this->page, $this->pageSize);
}
//$sql = "select * from {$this->prename}data where type={$this->type} and ";

//$sql = "select * from {$this->prename}{$table} where actionTime <='{$now_date}' and type={$this->type} order by actionTime desc limit 1";
$sql = "select * from {$this->prename}{$table} where actionTime <='{$now_date}' and type={$this->type} order by actionTime desc limit 1";
$issueinfo = $this->getRow($sql, $this->type);
$now_issue=$this->getGameLastNo($this->type, $issueinfo['actionNo'], $issueinfo['actionTime'], $date);
?>
<article class="module width_full">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <header>
        <h3 class="tabs_involved"><?= $typeInfo['title'] ?>开奖数据
            <form class="submit_link wz" action="/data/index/<?= $this->type ?>" target="ajax"
                  call="defaultSearch" dataType="html">
                期数：<input name="actionNo" type="text" value="<?php if ($para['actionNo']) echo $para['actionNo']; ?>"/>
                <label style="margin-left:30px;"><a class="item"
                                                    href="data/index/<?= $this->type ?>?date=<?= date('Y-m-d', $date - 24 * 3600) ?>">前一天</a></label>
                <label><a class="item"
                          href="data/index/<?= $this->type ?>?date=<?= date('Y-m-d', $this->time) ?>">今天</a></label>
                <label><a class="item"
                          href="data/index/<?= $this->type ?>?date=<?= date('Y-m-d', $date + 24 * 3600) ?>">后一天</a></label>
                <label>日期：<input name="date" type="date"/></label>
                <input type="submit" value="查找" class="alt_btn">
                <input type="reset" value="重置条件">
<?php
                if($typeInfo['zikai']=="1" )
                {
?>
    			    <label><a href="/data/batch_number/<?=$this->type?>" class="item" target="modal" width="730" title="新增開獎號碼" modal="true" button="確定:dataAddCode|取消:defaultCloseModal">[批次錄號]</a></label>
<?php
			    }
?>
            </form>
        </h3>
    </header>

    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
            <th>彩种</th>
            <th>场次</th>
            <th>期数</th>
            <!--th>日期</th-->
            <th>开奖数据</th>
            <th>状态</th>
            <th>开奖时间</th>
            <th>手动开奖</th>
            <th>手动退款</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $count = array();
        $dateString = date('Y-m-d ', $date);
        $search_result = false;
        foreach ($times['data'] as $var) {
            $actionData = $this->getGameLastNo($this->type, $var['actionNo'], $var['actionTime'], $date);
            if ($para['actionNo']) {
                if ($actionData['actionNo'] == $para['actionNo']) {
                    $search_result = true;
                } else {
                    continue;
                }
            }
            $number = $actionData['actionNo'];
			$sql="select data from {$this->prename}data where type={$this->type} and number='{$number}'";
			$data = $this->getRow($sql);
            ?>
            <tr 
                <?php
                if($number==$now_issue['actionNo']){echo 'style="background: #f4ecd7;"';}
                ?>
            >
                <td><?= $typeInfo['title'] ?></td>
                <td><?= $var['actionNo'] ?></td>
                <td><?=$this->ifs($number, '--' )?></td>
                <!--td><?= date('Y-m-d', $date) ?></td-->
                <td><?= $this->ifs($data['data'], '--') ?></td>
                <td><?= $this->iff($data['data'], '已开奖', '未开奖') ?></td>
                <td><?= $actionData['actionTime'] ?></td>
                <td>
                    <?php if ($data && $data['data']) { ?>
                        <a href="/data/udata/<?= $this->type ?>/<?= $var['actionNo'] ?>/<?= $dateString . $var['actionTime'] ?>/<?=$data['data']?>"
                           target="modal" width="340" title="添加开奖号码" modal="true"
                           button="确定:dataAddCode|取消:defaultCloseModal">修改</a>
                        <!--a href="/data/kj" target="ajax" data-type="<?= $typeInfo['id'] ?>"
                           data-number="<?= $data['number'] ?>" data-time="<?= $dateString . $var['actionTime'] ?>"
                           data-data="<?= $data['data'] ?>" onajax="setKjData" call="setKj" title="重新对没有开奖的投注开奖">开奖</a-->
                    <?php } else { ?>
                        <?php
                if($typeInfo['zikai']=="1" )
                {
                ?>
                        <a href="/data/auto_number?type=<?=$this->type?>&actionNo=<?=$number?>" target="ajax" call="defaultAjaxLink">[產生號碼] </a>
                        |
                <?php
                }
                ?>
                        <a href="/data/add/<?= $this->type ?>/<?= $number ?>/<?= $dateString . $var['actionTime'] ?>"
                           target="modal" width="340" title="添加开奖号码" modal="true"
                           button="确定:dataAddCode|取消:defaultCloseModal">添加</a>
                    <?php } ?>

                </td>
                <td>
                    <a href="/data/back/<?= $this->type ?>/<?= $var['actionNo'] ?>/<?= $dateString . $var['actionTime'] ?>"
                       target="modal" width="340" title="对未开奖期号进行投注退款" modal="true"
                       button="确定:dataAddCode|取消:defaultCloseModal" style="color:#F00;">撤单</a>
                </td>
            </tr>
            <?php
        }
        if ($para['actionNo'] && !$search_result) {
            echo '<tr>';
            echo '<td colspan="14">在您选择的日期当中没有查询到对应的期号，请选择正确的日期</td>';
            echo '</tr>';
        }
        ?>
        </tbody>
    </table>
    <footer>
        <?php
        $rel = $this->type;
        if ($para) {
            $rel .= '?' . http_build_query($para, '', '&');
        }
        $rel = $this->controller . '/' . $this->action . '-{page}/' . $this->type . '?' . http_build_query($_GET, '', '&');
        $this->display('inc/page.php', 0, $times['total'], $rel, 'dataPageAction');
        ?>
    </footer>
</article>