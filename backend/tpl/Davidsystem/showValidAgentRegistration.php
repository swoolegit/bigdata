<?php
$fromTime = isset($_GET['date']) ? strtotime($_GET['date']) : '';
if (! $fromTime) {
	echo '<h3>资料错误</h3>';
	return;
}
$toTime = strtotime('+1 day', $fromTime) - 1;

$sql ="
	SELECT
		m.uid,
		m.username,
		m.regTime
	FROM {$this->prename}members m
	JOIN (
		SELECT uid FROM {$this->prename}member_recharge WHERE rechargeAmount > 0 GROUP BY uid HAVING MIN(rechargeTime) BETWEEN :fromTime AND :toTime
	) mr ON mr.uid = m.uid
	AND m.type = 1";
$rows = $this->getRows($sql, array(
	'fromTime' => $fromTime,
	'toTime' => $toTime
));
?>

<article class="module width_full">
	<div class="tab_content">
		<table class="tablesorter" cellspacing="0">
			<thead>
				<tr>
					<th>用户名</th>
					<th>注册日期</th>
				</tr>
			</thead>
			<tbody>
				<?php
				if ($rows) {
					foreach ($rows as $row) { ?>
						<tr>
							<td><?php echo $row['username'] ?></td>
							<td><?php echo date('Y-m-d H:i:s', $row['regTime']) ?></td>
						</tr>
				<?php
					}
				} else { ?>
					<tr>
						<td colspan="2">无资料</td>
					</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>
</article>
