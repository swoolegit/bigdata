<?php
#时间这个应该要每一天都捞吧 ? 只能这样做了 ... 不用 DB 自己做的换页
if (isset($_GET)) {
    extract($_GET);
}
if (!isset($s)) {
    $s = date("Y-m-d");
}
if (!isset($e)) {
    $e = date("Y-m-d", strtotime("-3 month", strtotime($s)));
}

$hidedata=" and forTest=0";
if(isset($_GET['sethide']) && $_GET['sethide']=="1")
{
	$hidedata=" and forTest=1";
}

?>
        <table class="tablesorter" cellspacing="0">
            <thead>
            <tr>
                <th>日期</th>
                <th>代理总注册</th>
                <th>会员总注册</th>
            </tr>
            </thead>
            <tbody>
            <?php
			$this->pageSize = 30;
			$startNumber = ($this->page-1)*$this->pageSize;
			$sql="SELECT COUNT(1) as total FROM {$this->prename}report_day where date >='{$e}' and date <='{$s}' {$hidedata}";
			$total = $this->getValue($sql);			
            $sql="select date,newAgent,newUser from {$this->prename}report_day where date >='{$e}' and date <='{$s}' {$hidedata} order by id desc LIMIT {$startNumber},{$this->pageSize}";
			$result = $this->getRows($sql);
			foreach ($result as $rows){
				echo "<tr><td>{$rows['date']}</td><td>{$rows['newAgent']}</td><td>{$rows['newUser']}</td></tr>";
			}
            ?>
            </tbody>
        </table>
		<footer>
		    <?php
		    $rel = get_class($this) . '/proxy-{page}?' . http_build_query($_GET, '', '&');
		    $this->display('inc/page.php', 0, $total, $rel, 'defaultReplacePageAction');
		    ?>
		</footer>		
