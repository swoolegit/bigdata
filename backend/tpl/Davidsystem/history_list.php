<?php
if (isset($_GET)) {
    extract($_GET);
}
if (!isset($s)) {
    $s = date("Y-m-d");
}
if (!isset($e)) {
    $e = date("Y-m-d", strtotime("-3 month", strtotime($s)));
}
?>
        <table class="tablesorter" cellspacing="0">
            <thead>
            <tr>
                <th>日期</th>
                <th>存款</th>
                <th>取款</th>
                <th>注额</th>
                <th>派奖</th>
                <th>返点</th>
                <th>分红</th>
                <th>退水</th>
                <th>活动</th>
                <th>人工扣减</th>
                <th>盈亏</th>
                <th>注册人数</th>
                <th>投注人数</th>
            </tr>
            </thead>
            <tbody>
            <?php            
			$this->pageSize = 30;
			$startNumber = ($this->page-1)*$this->pageSize;
			$sql="SELECT COUNT(1) as total FROM {$this->prename}report_day where date >='{$e}' and date <='{$s}' ";
			$total = $this->getValue($sql);			
            $sql="select * from {$this->prename}report_day where date >='{$e}' and date <='{$s}' order by date desc LIMIT {$startNumber},{$this->pageSize}";
			$data = $this->getRows($sql);
			foreach ($data as $k => $v) {
				$profit=$v['real_bet']-$v['zjAmount']-$v['fanDianAmount']-$v['rebateAmount']-$v['brokerageAmount']-$v['bonusAmount'];
			?>
			<tr>
				<td><?=$v['date']?></td>
				<td><?=$this->nformat($v['rechargeAmount'])?></td>
				<td><?=$this->nformat($v['cashAmount'])?></td>
				<td><?=$this->nformat($v['real_bet'])?></td>
				<td><?=$this->nformat($v['zjAmount'])?></td>
				<td><?=$this->nformat($v['fanDianAmount'])?></td>
				<td><?=$this->nformat($v['bonusAmount'])?></td>
				<td><?=$this->nformat($v['rebateAmount'])?></td>
				<td><?=$this->nformat($v['brokerageAmount'])?></td>
				<td><?=$this->nformat($v['deduction'])?></td>
				<td><?=$this->nformat($profit)?>
					<?php 
						$temp=0;
						if($profit!=0)
						{
							$temp=number_format(($profit/$v['real_bet'])*100,2);
						}
						if($profit > 0)
						{
							echo " (<font color=red>".$temp."%</font>)";
						}else
						{
							echo " (<font color=green>".$temp."%</font>)";
						}
						
					?>					
				</td>
				<td><?=$v['newUser']?></td>
				<td><?=$v['betUser']?></td>
			</tr>
			<?php
			}
            ?>
            </tbody>
        </table>
		<footer>
		    <?php
		    $rel = get_class($this) . '/history-{page}?' . http_build_query($_GET, '', '&');
		    $this->display('inc/page.php', 0, $total, $rel, 'defaultReplacePageAction');
		    ?>
		</footer>