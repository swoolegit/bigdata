<?php
if (isset($_GET)) {
    extract($_GET);
}
if (!isset($s)) {
    $s = date("Y-m-d");
}
if (!isset($e)) {
    $e = date("Y-m-d", strtotime("-3 month", strtotime($s)));
}
?>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved">历史统计 (一天生成一次)
            <div class="submit_link wz">
                <form id="query-form" action="Davidsystem/history_list" target="ajax" call="defaultList" dataType="html">
                    时间：从 <input type="text" style="width:75px;" name="e" value="<?php echo $e; ?>" />
                    到 <input type="text" style="width:75px;" name="s" value="<?php echo $s; ?>" />
                    <input type="submit" value="查找" class="alt_btn">
                </form>
            </div>
        </h3>
    </header>
    <div class="tab_content">
    	<?php $this->display("Davidsystem/history_list.php"); ?>
    </div><!-- end of .tab_container -->
</article><!-- end of content manager article -->

<script>
$(function() {
    var f = document.getElementById("query-form");
    $([f.s, f.e]).datepicker();
});
</script>