<div class="system-info">
    <ul>
        <li>盈亏公式 : 注额-派奖-返点-退水-活动-分红</li>
    </ul>    
</div>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved">全局统计</h3>
    </header>
    <div class="tab_content">
        <table class="tablesorter" cellspacing="0">
            <thead>
            <tr>
                <th>资金统计</th>
                <th>存款</th>
                <th>取款</th>
                <th>注额</th>
                <th>派奖</th>
                <th>返点</th>
                <th>分红</th>
                <th>退水</th>
                <th>活动</th>
                <th>撤单</th>
                <th>人工扣减</th>
                <!--<th>其他</th>
                <th>所有彩金</th>
                <th>赠送彩金</th>-->
                <th>平台盈亏</th>
                <th>获利率</th>
            </tr>
            </thead>
            <?php
            $today = date("Y-m-d");
            $yesterday = date("Y-m-d", strtotime("-1 day", strtotime($today)));
            $thisWeekS = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - date("w") + 1));
            $thisWeekE = date("Y-m-d", mktime(0, 0, 0, date("m"), date("d") - date("w") + 7));
            $thisMonth = date("Y-m");
            $nextMonth = date("Y-m", strtotime("+1 month", strtotime($today))) . '-01';
            $preMonth = date("Y-m", strtotime("-1 month", strtotime($today))) . '-01';
            $month = (int)date("m");
            switch ($month) {
                case 1:
                case 2:
                case 3:
                    $thisSeasonS = date("Y") . '-01-01';
                    $thisSeasonE = date("Y") . '-03-31';
                    $preYear = date("Y") - 1;
                    $preSeasonS = $preYear . '-10-01';
                    $preSeasonE = $preYear . '-12-31';
                    break;
                case 4:
                case 5:
                case 6:
                    $thisSeasonS = date("Y") . '-04-01';
                    $thisSeasonE = date("Y") . '-06-30';
                    $preSeasonS = date("Y") . '-01-01';
                    $preSeasonE = date("Y") . '-03-31';
                    break;
                case 7:
                case 8:
                case 9:
                    $thisSeasonS = date("Y") . '-07-01';
                    $thisSeasonE = date("Y") . '-09-30';
                    $preSeasonS = date("Y") . '-04-01';
                    $preSeasonE = date("Y") . '-06-30';
                    break;
                case 10:
                case 11:
                case 12:
                    $thisSeasonS = date("Y") . '-10-01';
                    $thisSeasonE = date("Y") . '-12-31';
                    $preSeasonS = date("Y") . '-07-01';
                    $preSeasonE = date("Y") . '-09-30';
                    break;
            }
            ?>
            <tbody>
            <?php
            $array = array();
            $s = strtotime($today . ' 00:00:00');
            $e = strtotime($today . ' 23:59:59');
            $array['今日统计'] = array($s, $e);
            $s = $s - 86400;
            $e = $e - 86400;
            $array['昨日统计'] = array($s, $e);
            $s = strtotime($thisWeekS);
            $e = strtotime($thisWeekE);
            $array['本周统计'] = array($s, $e);
            $s = $s - (86400 * 7);
            $e = $e - (86400 * 7);
            $array['上周统计'] = array($s, $e);
            $s = strtotime($thisMonth . '-01 00:00:00');
            $e = strtotime($nextMonth . '-01 00:00:00');
            #echo $thisMonth.'-01 00:00:00'.'<hr>'.$nextMonth.'-01 00:00:00';
            $array['本月统计'] = array($s, $e);
            $s = strtotime($preMonth . '-01 00:00:00');
            $e = strtotime($thisMonth . '-01 00:00:00');
            #echo $preMonth.'-01 00:00:00'.'<hr>'.$thisMonth.'-01 00:00:00';
            $array['上月统计'] = array($s, $e);
            $s = strtotime($thisSeasonS . ' 00:00:00');
            $e = strtotime($thisSeasonE . ' 23:59:59');
            $array['本季统计'] = array($s, $e);
            $s = strtotime($preSeasonS . ' 00:00:00');
            $e = strtotime($preSeasonE . ' 23:59:59');
            $array['上季统计'] = array($s, $e);
            $s = strtotime(date("Y") . '-01-01 00:00:00');
            $e = strtotime(date("Y") . '-12-31 23:59:59');
            $array['今年统计'] = array($s, $e);
            foreach ($array as $key => $arr) {
                $s = $arr[0];
                $e = $arr[1];
                ?>
                <tr>
                    <td><?php echo $key; ?></td>
                    <?php
                    if($key=='今日统计')
                    {
                        $sql="
                        select sum(r.recharge) as rechargeAmount,
                        sum(r.cash) as cashAmount , 
                        sum(r.real_bet) as betAmount,
                        sum(r.zj) as zjAmount,
                        sum(r.fandian) as fanDianAmount,
                        sum(r.broker) as brokerageAmount,
                        sum(r.rebate) as rebateAmount,
                        sum(r.bonus) as bonusAmount,
                        sum(r.deduction)*-1 as deduction,
                        sum(r.cancelOrder) as cancelOrderAmount 
                        FROM `{$this->prename}member_report` r
                        WHERE r.`actionTime` >= {$s} AND r.`actionTime` <= {$e} ";
                    }else
                    {
                        $sdate=date("Y-m-d",$s);
                        $edate=date("Y-m-d",$e);
                        $sql="
                        select sum(r.rechargeAmount) as rechargeAmount,
                        sum(r.cashAmount) as cashAmount , 
                        sum(r.real_bet) as betAmount,
                        sum(r.zjAmount) as zjAmount,
                        sum(r.fanDianAmount) as fanDianAmount,
                        sum(r.brokerageAmount) as brokerageAmount,
                        sum(r.rebateAmount) as rebateAmount,
                        sum(r.bonusAmount) as bonusAmount,
                        sum(r.deduction)*-1 as deduction,
                        sum(r.cancelOrderAmount) as cancelOrderAmount 
                        FROM `{$this->prename}report_day` r
                        WHERE r.`date` >= '{$sdate}' AND r.`date` <= '{$edate}' ";
                    }

					$data = $this->getRow($sql);
                    #存款
                    echo "<td>" . $this->nformat($data['rechargeAmount']). "</td>";
                    #取款
                    echo "<td>" . $this->nformat($data['cashAmount']) . "</td>";
                    #注额
                    echo "<td>" . $this->nformat($data['betAmount']) . "</td>";
                    #派奖
                    echo "<td>" . $this->nformat($data['zjAmount']) . "</td>";
                    #返点
                    echo "<td>" . $this->nformat($data['fanDianAmount']) . "</td>";
                    #分红
                    echo "<td>" . $this->nformat($data['bonusAmount']) . "</td>";
                    #退水
                    echo "<td>" . $this->nformat($data['rebateAmount']) . "</td>";
                    #活动
                    echo "<td>" . $this->nformat($data['brokerageAmount']) . "</td>";
                    #撤单
                    echo "<td>" . $this->nformat($data['cancelOrderAmount']) . "</td>";                    
                    #人工扣减
                    echo "<td>" . $this->nformat($data['deduction']) . "</td>";                    
					#盈亏
                    $profit=$data['betAmount']-$data['zjAmount']-$data['fanDianAmount']-$data['rebateAmount']-$data['brokerageAmount']-$data['bonusAmount'];
                    echo "<td>" . $this->nformat($profit)."</td>";
                    #其他
                    //echo '<td>?</td>';
                    #所有彩金
                    //echo '<td>?</td>';
                    #赠送彩金
                    //echo '<td>?</td>';
                    #利率
                    if($data['betAmount']==0)
                    {
                        echo "<td>0%</td>";
                    }else
                    {
                        echo "<td>".(number_format(($profit/$data['betAmount'])*100,2))."%</td>";
                    }
                    ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <footer>
        </footer>
    </div><!-- end of .tab_container -->
</article><!-- end of content manager article -->

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">排名统计(中奖金额前10)</h3>
    </header>
    <div class="tab_content">
        <table class="tablesorter" cellspacing="0">
            <thead>
            <tr>
                <th>今日中奖</th>
                <th>会员帐户</th>
                <th>存款</th>
                <th>取款</th>
                <th>注额</th>
                <th>派奖</th>
                <th>返点</th>
                <th>分红</th>
                <th>工资</th>
                <th>活动</th>
                <th>撤单</th>
                <th>人工扣减</th>
                <!--<th>其他</th>-->
                <th>平台盈亏</th>
                <th>当前余额</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $n = 1;
            $today = date("Y-m-d");
            $s = strtotime("{$today} 00:00:00");
            $e = strtotime("{$today} 23:59:59");
			$sql="select sum(a.recharge) as rechargeAmount,sum(a.cash) as cashAmount , sum(a.real_bet) as betAmount,
						sum(a.zj) as zjAmount,sum(a.fandian) as fanDianAmount,sum(a.broker) as brokerageAmount,
						sum(a.rebate) as rebateAmount,sum(a.bonus) as bonusAmount,sum(a.cancelOrder) as cancelOrderAmount,
						sum(a.deduction)*-1 as deduction,
						b.uid,b.username,b.coin from `{$this->prename}member_report` a,`{$this->prename}members` b where a.actionTime >= '{$s}' AND a.actionTime <= '{$e}' and a.uid=b.uid group by uid order by zjAmount desc LIMIT 10";
            //$q = "SELECT `uid`,SUM(bonus) AS TOTAL FROM `{$this->prename}bets_repl` WHERE `actionTime` >= '{$s}' AND `actionTime` <= '{$e}' GROUP BY `uid` ORDER BY TOTAL DESC LIMIT 10";
            $qrow = $this->getRows($sql);
            foreach ($qrow as $data) {
                echo "<tr><td>第{$n}名</td>";
                #会员姓名
                echo "<td>{$data["username"]}</td>";
                #存款
                echo "<td>" . $this->nformat($data['rechargeAmount']). "</td>";
                #取款
                echo "<td>" . $this->nformat($data['cashAmount']). "</td>";
                #注额
                echo "<td>" . $this->nformat($data['betAmount']). "</td>";
                #派奖(用这个排)
                echo "<td>".$this->nformat($data['zjAmount'])."</td>";
                #返点
                echo "<td>".$this->nformat($data['fanDianAmount'])."</td>";
                #分红
                echo "<td>".$this->nformat($data['bonusAmount'])."</td>";
				#工资
                echo "<td>".$this->nformat($data['rebateAmount'])."</td>";
				#活动
                echo "<td>".$this->nformat($data['brokerageAmount'])."</td>";
				#撤单
                echo "<td>".$this->nformat($data['cancelOrderAmount'])."</td>";                
                #人工扣减
                echo "<td>" . $this->nformat($data['deduction']). "</td>";
                #其他
                //echo "<td>?</td>";
                #盈亏（注额 - 派奖 - 派点 - 其他）
                $profit=$data['betAmount']-$data['zjAmount']-$data['fanDianAmount']-$data['rebateAmount']-$data['brokerageAmount']-$data['bonusAmount'];
                echo "<td>" . $this->nformat($profit)."</td>";
                #余额
                echo "<td>" . $this->nformat($data['coin'])."</td>";
                $n++;
            }
            ?>
            </tbody>
        </table>
        <footer>
        </footer>
    </div><!-- end of .tab_container -->
</article><!-- end of content manager article -->
