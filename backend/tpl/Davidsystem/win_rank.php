<article class="module width_full">
    <header>
        <h3 class="tabs_involved">盈利排行前100位</h3>
    </header>
    <div class="tab_content">
        <table class="tablesorter" cellspacing="0">
            <thead>
            <tr>
                <th>用户名</th>
                <th>注额</th>
                <th>派奖</th>
                <th>盈亏</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $this->pageSize=30;
            //$sql = "SELECT `uid`,SUM( (`actionNum`*`mode`*`beiShu`) - `fanDian` - `bonus` ) AS TOTAL,SUM(`actionNum`*`mode`*`beiShu`) AS bets , SUM(`bonus`) AS bonus FROM `{$this->prename}bets_repl` GROUP BY `uid` HAVING TOTAL > 0 ORDER BY TOTAL DESC";
            $sql = "SELECT m.username,r.uid,SUM( r.zj-r.real_bet ) AS TOTAL , SUM(r.zj) AS zj , SUM(r.real_bet) AS real_bet FROM `{$this->prename}member_report` r , `{$this->prename}members` m 
            where r.uid=m.uid {$setfortest}
            GROUP BY r.uid HAVING TOTAL > 0 ORDER BY TOTAL DESC limit 100";
            //$data = $this->getPage($sql, $this->page, $this->pageSize);
            $data = $this->getRows($sql);
            foreach ($data as $var) {
                    echo "<tr><td>{$var['username']}</td>";
                    echo "<td>{$this->nformat($var['real_bet'])}</td>";
                    echo "<td>{$this->nformat($var['zj'])}</td>";
                    echo "<td>{$this->nformat($var['TOTAL'])}</td>";
                }
            ?>
            </tbody>
        </table>
        <footer>
            <?php
            $rel = get_class($this) . '/win_rank-{page}?' . http_build_query($_GET, '', '&');
            $this->display('inc/page.php', 0, 0, $rel, 'defaultReplacePageAction');
            ?>
        </footer>
    </div><!-- end of .tab_container -->
</article><!-- end of content manager article -->
