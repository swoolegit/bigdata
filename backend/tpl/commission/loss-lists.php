<?php
//前一天日期
$yesterday = date("Y-m-d", strtotime("-1 day"));
$fromTime = strtotime($yesterday . ' 00:00:00');
$toTime = strtotime($yesterday . ' 23:59:59');
// 用户限制
$para=$_GET;
$userWhere="";
if($para['username'] && $para['username']!="用户名"){
    $userWhere=" where u.username like '%{$para['username']}%'";
}
$this->getSystemSettings();
$lossCommissionBase = floatval($this->settings['lossCommissionBase']);
$lossCommissionBase2 = floatval($this->settings['lossCommissionBase2']);
$sql =
    "SELECT
        u.uid,
        u.username,
        u.type,
        u.parentId,
        SUM(b.real_bet) AS betAmount,
        SUM(b.zj) AS zjAmount,
        SUM(b.fandian) AS fanDianAmount,
        SUM(b.broker) AS brokerageAmount,
        SUM(b.rebate) AS rebate,
        u.lossCommStatus
    FROM {$this->prename}members AS u
        LEFT JOIN {$this->prename}member_report AS b ON
            b.uid =u.uid
            AND b.actionTime BETWEEN {$fromTime} AND {$toTime}
    {$userWhere}
    GROUP BY u.uid
    HAVING betAmount-zjAmount-fanDianAmount-brokerageAmount-rebate > " . ($lossCommissionBase < $lossCommissionBase2 ? $lossCommissionBase : $lossCommissionBase2);
$users = $this->getRows($sql);
?>

    <div class="tab_content">
        <table class="tablesorter" cellspacing="0">
            <thead>
            <tr>
                <th>用户名</th>
                <th>UserId</th>
                <th>类型</th>
                <th>昨日亏损金额</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php
            if ($users) {

                $lossCommission = $lossCommissionBase < $lossCommissionBase2 ? $lossCommissionBase : $lossCommissionBase2;
                foreach ($users as $user) {
                 	$loss = $user['zjAmount'] - $user['betAmount'] + $user['fanDianAmount'] + $user['brokerageAmount'];
                    if ($loss < 0 && abs($loss) > $lossCommission) {
                        ?>
                        <tr>
                            <td><?= $user['username'] ?></td>
                            <td><?= $user['uid'] ?></td>
                            <td><?php if ($user['type']) {
                                    echo '代理';
                                } else {
                                    echo '会员';
                                } ?></td>
                            <td><?= abs($loss) ?></td>
                            <td><?php 
                                if($user['lossCommStatus'] )
                                {
                                    echo "已发送";
                                }else
                                {
                                    echo "未处理";
                                }
                            ?></td>
                            <td>
                            <?php
                            if($user['lossCommStatus'] )
                            {
                                echo "--";
                            }else
                            {
                            ?>                                
                                <a href="Commission/lossComSingle/<?= $user['uid'] ?>" target="ajax"
                                   call="lossCommHandle" dataType="html">发放佣金</a>
                            <?php
                            }
                            ?>
                            </td>
                        </tr>
                    <?php }
                }
            } else { ?>
                <tr>
                    <td colspan="5" align="center">已没有可发放亏损佣金的用户。</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <footer>
        <?php
            $rel = get_class($this) . '/lossCommissionLists-{page}?' . http_build_query($_GET, '', '&');
            $this->display('inc/page.php', 0, $data['total'], $rel, 'betLogSearchPageAction');
            ?>
        </footer>