<?php
//前一天日期
$yesterday = date("Y-m-d", strtotime("-1 day"));
$fromTime = strtotime($yesterday . ' 00:00:00');
$toTime = strtotime($yesterday . ' 23:59:59');
// 用户限制
$para=$_GET;
$userWhere="";
if($para['username'] && $para['username']!="用户名"){
    $userWhere=" where u.username like '%{$para['username']}%'";
}
$this->getSystemSettings();

$conCommissionBase = floatval($this->settings['conCommissionBase']);
$conCommissionBase2 = floatval($this->settings['conCommissionBase2']);
$sql =
    "SELECT
        u.uid,
        u.username,
        u.type,
        u.parentId,
        SUM(b.real_bet) AS betAmount,
        u.conCommStatus
    FROM {$this->prename}members AS u
        LEFT JOIN {$this->prename}member_report AS b ON
            b.uid =u.uid
            AND b.actionTime BETWEEN {$fromTime} AND {$toTime}
    {$userWhere}
    GROUP BY u.uid
    HAVING betAmount > " . ($conCommissionBase < $conCommissionBase2 ? $conCommissionBase : $conCommissionBase2);
$data = $this->getPage($sql, $this->page, $this->pageSize);
?>
        <table class="tablesorter" cellspacing="0">
            <thead>
            <tr>
                <th>用户名</th>
                <th>UserId</th>
                <th>类型</th>
                <th>昨日消费金额</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php if ($data['data']) foreach ($data['data'] as $var) { ?>
                <tr>
                    <td><?= $var['username'] ?></td>
                    <td><?= $var['uid'] ?></td>
                    <td><?php if ($var['type']) {
                            echo '代理';
                        } else {
                            echo '会员';
                        } ?></td>
                    <td><?= $var['betAmount'] ?></td>
                    <td><?php 
                        if($var['conCommStatus'] )
                        {
                            echo "已发送";
                        }else
                        {
                            echo "未处理";
                        }
                    ?></td>
                    <td>
                        <?php
                        if($var['conCommStatus'] )
                        {
                            echo "--";
                        }else
                        {
                        ?>
                        <a href="Commission/conComSingle/<?= $var['uid'] ?>" target="ajax"
                           call="conCommHandle" dataType="html">发放佣金</a>
                        <?php
                        }
                        ?>
                    </td>
                </tr>
            <?php } else { ?>
                <tr>
                    <td colspan="9" align="center">已没有可发放消费佣金的用户。</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        <footer>
            <?php
            $rel = get_class($this) . '/conCommissionLists-{page}?' . http_build_query($_GET, '', '&');
            $this->display('inc/page.php', 0, $data['total'], $rel, 'betLogSearchPageAction');
            ?>
        </footer>
