<?php
$para = $_GET;

// 时间限制
if ($para['fromTime'] && $para['toTime']) {
    $fromTime = strtotime($para['fromTime']."00:00:00");
    $toTime = strtotime($para['toTime']."23:59:59");
    $betTimeWhere = "and r.actionTime between $fromTime and $toTime";
} elseif ($para['fromTime']) {
    $fromTime = strtotime($para['fromTime']);
    $betTimeWhere = "and r.actionTime >=$fromTime";
} elseif ($para['toTime']) {
    $toTime = strtotime($para['toTime']) + 24 * 3600;
    $betTimeWhere = "and r.actionTime < $toTime";
} else {
    $toTime = strtotime('00:00')-1;
    $betTimeWhere = "and r.actionTime > $toTime";
}
// 用户限制
$amountTitle = '全部总结';
$userWhere=1;
if ($para['parentId'] = intval($para['parentId'])) {
    // 用户ID限制
    $userWhere = "parentId={$para['parentId']}";
    $uid = $para['parentId'];
}
if ($para['uid'] = intval($para['uid'])) {
    // 用户ID限制
    $uParentId = $this->getValue("select parentId from {$this->prename}members where uid=?", $para['uid']);
    $userWhere = "u.uid=$uParentId";
}
if ($para['username'] && $para['username'] != '用户名') {
    $para['username'] = wjStrFilter($para['username']);
    if (!preg_match('/^\w{4,16}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    // 用户名限制
    $userWhere = "u.username='{$para['username']}'";
}
$startNumber = ($this->page-1)*$this->pageSize;
//$list = $this->getPage($sql . ' ', $this->page, $this->pageSize, null, 180);	// TODO: 先用 3 分钟缓存改善性能
$sql="select 
            r.uid as uid,
            IFNULL(sum(r.recharge),0) as rechargeAmount,
            IFNULL(sum(r.cash),0) as cashAmount , 
            IFNULL(sum(r.real_bet),0) as betAmount,
            IFNULL(sum(r.zj-real_bet),0) as zjAmount,
            IFNULL(sum(r.fandian),0) as fanDianAmount,
            IFNULL(sum(r.broker),0) as brokerageAmount,
            IFNULL(sum(r.rebate),0) as rebateAmount,
            IFNULL(sum(r.bonus),0) as bonusAmount,
            IFNULL(sum(r.poker_bet),0) as poker_bet,
            IFNULL(sum(r.poker_winlost),0) as poker_winlost
        from {$this->prename}member_report r where {$userWhere}  {$betTimeWhere}
         group by r.uid LIMIT {$startNumber},{$this->pageSize}
        ";

$list['data'] = $this->getRows($sql);
$sql="SELECT count(DISTINCT r.uid) as total FROM {$this->prename}member_report r where {$userWhere}  {$betTimeWhere}";
$list['total'] = $this->getValue($sql);
$nameArray=[];
foreach($list['data'] as $k=>$v)
{
    $nameArray[]=$v['uid'];
}
//print_r($nameArray);
if(count($nameArray) > 0)
{
    $sql="select uid,username,fanDian,fenHong,rebate from {$this->prename}members where uid in (".implode(",",$nameArray).")";
    $temp= $this->getRows($sql);
    $nameArray=[];
    foreach($temp as $k=>$v)
    {
        $nameArray[$v['uid']]=
        [
            'username'=>$v['username'],
            'fanDian'=>$v['fanDian'],
            'fenHong'=>$v['fenHong'],
            'rebate'=>$v['rebate'],
        ];
    }
}

$params = http_build_query($_REQUEST, '', '&');
$count = array();
$sql="select 
            IFNULL(sum(r.recharge),0) as rechargeAmount,
            IFNULL(sum(r.cash),0) as cashAmount , 
            IFNULL(sum(r.real_bet),0) as betAmount,
            IFNULL(sum(r.zj-real_bet),0) as zjAmount,
            IFNULL(sum(r.fandian),0) as fanDianAmount,
            IFNULL(sum(r.broker),0) as brokerageAmount,
            IFNULL(sum(r.rebate),0) as rebateAmount,
            IFNULL(sum(r.bonus),0) as bonusAmount,
            IFNULL(sum(r.poker_bet),0) as poker_bet,
            IFNULL(sum(r.poker_winlost),0) as poker_winlost
		from {$this->prename}member_report r where {$userWhere}  {$betTimeWhere}
        ";
$all = $this->getRow($sql);
?>

<table class="tablesorter" cellspacing="0">
    <thead>
    <tr>
        <td>用户名</td>
        <td>充值</td>
        <td>提现</td>
        <td>彩票投注</td>
        <td>彩票输赢</td>
        <td>棋牌投注</td>
        <td>棋牌输赢</td>
        <!-- <td>电子投注</td>
        <td>电子输赢</td> -->
        <td>返点 (%)</td>
		<td>分红 (%)</td>
		<td>退水 (%)</td>
        <!--td>撤单</td-->
        <!--td title="包括充值佣金，注册佣金，亏损佣金，消费佣金，签到，活动赠送奖金">佣金及活动</td-->
        <!--td>转账</td-->
        <!--td>余额</td-->
        <!--td>团队盈亏</td-->
        <td>盈亏</td>
        <!-- <td>查看</td> -->
    </tr>
    </thead>
    <tbody id="nav01">
    <?php
    if ($list['data']) foreach ($list['data'] as $var) {
        // if ($var['username'] != '没有下级了') {
        //     //$var['fanDianAmount'] = $this->getValue($sql, $var['uid']);
        //     $pId = $var['uid'];
		// 	$sql="select sum(r.zj-r.real_bet+r.fandian+r.broker+r.gongzi) as teamwin from
		// 	 {$this->prename}member_report r , {$this->prename}members u 
		// 	 where r.uid=u.uid and concat(',', u.parents, ',') like '%,$pId,%' {$betTimeWhere}";
		// 	$var['teamwin'] = $this->getValue($sql);
        // }
        $count['betAmount'] += $var['betAmount'];
        $count['zjAmount'] += $var['zjAmount'];
        $count['fanDianAmount'] += $var['fanDianAmount'];
        $count['cashAmount'] += $var['cashAmount'];
        $count['rechargeAmount'] += $var['rechargeAmount'];
		$count['bonusAmount'] += $var['bonusAmount'];
		$count['rebateAmount'] += $var['rebateAmount'];
        $count['poker_bet'] += $var['poker_bet'];
        $count['poker_winlost'] += $var['poker_winlost'];
        ?>
        <tr>
            <td><?= $this->ifs($nameArray[$var['uid']]['username'], '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['rechargeAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['cashAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['betAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['zjAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['poker_bet']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['poker_winlost']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['fanDianAmount']), '--') ?> (<?=$nameArray[$var['uid']]['fanDian']?>)</td>
            <td><?= $this->ifs($this->nformat($var['bonusAmount']), '--') ?> (<?=$nameArray[$var['uid']]['fenHong']?>)</td>
            <td><?= $this->ifs($this->nformat($var['rebateAmount']), '--') ?> (<?=$nameArray[$var['uid']]['rebate']?>)</td>
            <td><?= $this->ifs($this->nformat($var['zjAmount'] - $var['betAmount'] + $var['fanDianAmount'] + $var['brokerageAmount'] + $var['rebateAmount']+$var['bonusAmount'] ), '--') ?></td>
            <!--<td>
                <?php if (!$noChildren) { ?>
                    <a target="ajax" dataType="html" call="defaultList"
                       href="<?= "countData/betDateSearch/?parentId={$var['uid']}&fromTime={$para['fromTime']}&toTime={$para['toTime']}" ?>">下级</a>
                <?php } ?>
                <?php if ($var['parentId']) { ?>
                    <a target="ajax" dataType="html" call="defaultList"
                       href="<?= "countData/betDateSearch/?uid={$var['uid']} &fromTime={$para['fromTime']}&toTime={$para['toTime']}" ?>">上级</a>
                <?php } ?>
            </td>-->
        </tr>
    <?php } ?>
    <tr>
        <td><span class="spn9">本页总结</span></td>
        <td><?= $this->ifs($this->nformat($count['rechargeAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['cashAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['betAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['zjAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['poker_bet']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['poker_winlost']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['fanDianAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['bonusAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['rebateAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['zjAmount'] + $count['poker_winlost'] + $count['fanDianAmount'] + $count['rebateAmount']+ $count['bonusAmount']), '--') ?></td>
    </tr>
    <tr>
        <td><span class="spn9"><?= $amountTitle ?></span></td>
        <td><?= $this->ifs($this->nformat($all['rechargeAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($all['cashAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($all['betAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($all['zjAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($all['poker_bet']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($all['poker_winlost']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($all['fanDianAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($all['bonusAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($all['rebateAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($all['zjAmount'] + $all['poker_winlost'] + $all['fanDianAmount'] + $all['brokerageAmount']+ $all['rebateAmount']+ $all['bonusAmount']), '--') ?></td>
    </tr>
    </tbody>
</table>
<footer>
    <?php
    $rel = get_class($this) . '/betDateSearch-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $list['total'], $rel, 'betLogSearchPageAction');
    ?>
</footer>
<script type="text/javascript">
    ghhs("nav01", "tr");
</script>