<script type="text/javascript">
    $(function () {
        $('.tabs_involved form input[name=username]')
            .focus(function () {
                if (this.value == '用户名') this.value = '';
            })
            .blur(function () {
                if (this.value == '') this.value = '用户名';
            })
            .keypress(function (e) {
                if (e.keyCode == 13) $(this).closest('form').submit();
            });

    });

</script>
<div class="system-info">
    <ul>
        <li>此表为团队资金纪录(含自身及所以下级)</li>
        <li>盈亏公式 : 注额-派奖-返点-退水-活动-分红</li>
    </ul>    
</div>
<article class="module width_full">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <header>
        <h3 class="tabs_involved">综合统计
            <form action="countData/betTDateSearch" target="ajax" dataType="html" call="defaultList"
                  class="submit_link wz">
                	&nbsp;&nbsp;时间：从 <input type="date" class="alt_btn" name="fromTime" value="<?php echo date("Y-m-d",time()); ?>" /> 到 <input type="date" class="alt_btn" value="<?php echo date("Y-m-d",time()); ?>" name="toTime"/>&nbsp;&nbsp;
                <span>
	                <input type="text" class="fqr-in" height="20" autocomplete="off" value="用户名" id="username" name="username"/>
	                <div class="auto-screening auto-hidden mbox" id="autoScreening"></div>
                </span>
                &nbsp;&nbsp;
                <input type="submit" value="查找" class="alt_btn">
            </form>
        </h3>
    </header>

    <div class="tab_content">
        <?php $this->display("count/tdate-list.php"); ?>
    </div>
</article>