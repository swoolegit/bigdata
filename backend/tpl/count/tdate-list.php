<?php
$para = $_GET;


// 时间限制
if ($para['fromTime'] && $para['toTime']) {
    $fromTime = strtotime($para['fromTime']."00:00:00");
    $toTime = strtotime($para['toTime']."23:59:59");
    $betTimeWhere = " r.actionTime between $fromTime and $toTime";
} elseif ($para['fromTime']) {
    $fromTime = strtotime($para['fromTime']);
    $betTimeWhere = " r.actionTime >=$fromTime";
} elseif ($para['toTime']) {
    $toTime = strtotime($para['toTime']) + 24 * 3600;
    $betTimeWhere = " r.actionTime < $toTime";
} else {
    $toTime = strtotime('00:00')-1;
    $betTimeWhere = " r.actionTime > $toTime";
}
// 用户限制
$amountTitle = '全部总结';
$userWhere=1;
if ($para['parentId'] = intval($para['parentId'])) {
    // 用户ID限制
    $userWhere = "u.parentId={$para['parentId']}";
    $uid = $para['parentId'];
}
if ($para['uid'] = intval($para['uid'])) {
    // 用户ID限制
    $uParentId = $this->getValue("select parentId from {$this->prename}members where uid=?", $para['uid']);
    $userWhere = "u.uid=$uParentId";
}
if ($para['username'] && $para['username'] != '用户名') {
    $para['username'] = wjStrFilter($para['username']);
    if (!preg_match('/^\w{4,16}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    // 用户名限制
    $userWhere = "u.username='{$para['username']}'";
}
$this->pageSize = 20;
$startNumber = ($this->page-1)*$this->pageSize;
//$list = $this->getPage($sql . ' ', $this->page, $this->pageSize, null, 180);	// TODO: 先用 3 分钟缓存改善性能
$mainUser=$userWhere;
if($userWhere==1)
{
    $mainUser=" u.parentId =0";
    //$mainUser="";
}
$sql="select u.username,u.coin,u.uid,u.parentId,u.parents,u.fanDian,u.rebate,u.fenHong
		from  {$this->prename}members u  where {$mainUser}  LIMIT {$startNumber},{$this->pageSize}
		";

$list['data'] = $this->getRows($sql);
$sql="SELECT count(u.uid) as total FROM  {$this->prename}members u where {$mainUser}  ";

$list['total'] = $this->getValue($sql);

if (!$list['total']) {
    $uParentId2 = $this->getValue("select parentId from {$this->prename}members u ", $para['parentId']);
    $list = array(
        'total' => 1,
        'data' => array(array(
            'parentId' => $uParentId2,
            'uid' => $para['parentId'],
            'username' => '没有下级了'
        ))
    );
    $noChildren = true;
}
$params = http_build_query($_REQUEST, '', '&');
$count = array();
/*
$sql2="";
if($para['parentId'])
{
	$sql2="or concat(',', u.parents, ',') like '%,$uid,%'";
}
$sql="select sum(u.coin) as coin,
		IFNULL(sum(r.recharge),0) as rechargeAmount,
				IFNULL(sum(r.cash),0) as cashAmount , 
				IFNULL(sum(r.real_bet),0) as betAmount,
				IFNULL(sum(r.zj),0) as zjAmount,
				IFNULL(sum(r.fandian),0) as fanDianAmount,
				IFNULL(sum(r.broker),0) as brokerageAmount,
				IFNULL(sum(r.gongzi),0) as gongziAmount,
				IFNULL(sum(r.bonus),0) as bonusAmount,
				IFNULL(sum(r.cancelOrder),0) as cancelOrderAmount,
				IFNULL(sum(r.transfer),0) as transfer
		from {$this->prename}members u left join {$this->prename}member_report r on u.uid=r.uid {$betTimeWhere} where {$mainUser} {$sql2} {$sethide} 
		";
echo $sql;
$all = $this->getRow($sql);
 *
 */
?>

<table class="tablesorter" cellspacing="0">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <thead>
    <tr>
        <td>用户名</td>
        <td>团队充值</td>
        <td>团队提现</td>
        <td>团队投注</td>
        <td>团队中奖</td>
        <td>团队返点 (%)</td>
		<td>团队分红 (%)</td>
		<td>团队退水 (%)</td>
        <td>团队撤单</td>
        <td title="包括充值佣金，注册佣金，亏损佣金，消费佣金，签到，活动赠送奖金">佣金及活动</td>
        <!--td>团队转账</td-->
        <td>团队余额</td>
        <td>团队盈亏</td>
        <td>查看</td>
    </tr>
    </thead>
    <tbody id="nav01">
    <?php
    if ($list['data']) foreach ($list['data'] as $var) {
        if ($var['username'] != '没有下级了') {
            //$var['fanDianAmount'] = $this->getValue($sql, $var['uid']);
            $pId = $var['uid'];
			$sql="select 
						sum(u.coin) as coin,
						sum(r.zj) as zjAmount,
						sum(r.real_bet) as betAmount,
						sum(r.fandian) as fanDianAmount,
						sum(r.broker) as brokerageAmount,
						sum(r.rebate) as rebateAmount,
						sum(r.bonus) as bonusAmount,
						sum(r.recharge) as rechargeAmount,
						sum(r.cancelOrder) as cancelOrderAmount,
						sum(r.transfer) as transfer,
						sum(r.cash) as cashAmount,
						sum(r.zj-r.real_bet+r.fandian+r.broker+r.rebate+r.bonus) as teamwin 
				from
			 {$this->prename}member_report r , {$this->prename}members u 
             where r.uid=u.uid and concat(',', u.parents, ',') like '%,$pId,%' and {$betTimeWhere}";

			 $team = $this->getRow($sql);
			$var['teamwin'] = $team['teamwin'];
        }
        $count['betAmount'] += $team['betAmount'];
        $count['zjAmount'] += $team['zjAmount'];
        $count['fanDianAmount'] += $team['fanDianAmount'];
        $count['brokerageAmount'] += $team['brokerageAmount'];
        $count['cashAmount'] += $team['cashAmount'];
        $count['coin'] += $var['coin']+$team['coin'];
        $count['rechargeAmount'] += $team['rechargeAmount'];
        $count['teamwin'] += $team['teamwin'];
		$count['bonusAmount'] += $team['bonusAmount'];
		$count['rebateAmount'] += $team['rebateAmount'];
		$count['cancelOrderAmount'] += $team['cancelOrderAmount'];
		$count['transfer'] += $team['transfer'];
        ?>
        <tr>
            <td><?= $this->ifs($var['username'], '--') ?></td>
            <td><?= $this->ifs($this->nformat($team['rechargeAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($team['cashAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($team['betAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($team['zjAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($team['fanDianAmount']), '--') ?> (<?=$var['fanDian']?>)</td>
            <td><?= $this->ifs($this->nformat($team['bonusAmount']), '--') ?> (<?=$var['fenHong']?>)</td>
            <td><?= $this->ifs($this->nformat($team['rebateAmount']), '--') ?> (<?=$var['rebate']?>)</td>
            <td><?= $this->ifs($this->nformat($team['cancelOrderAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($team['brokerageAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($team['coin']+$var['coin']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($team['teamwin']), '--') ?></td>
            <td>
                <?php if (!$noChildren) { ?>
                    <a target="ajax" dataType="html" call="defaultList"
                       href="<?= "countData/betTDateSearch/?parentId={$var['uid']}&fromTime={$para['fromTime']}&toTime={$para['toTime']}" ?>">下级</a>
                <?php } ?>
                <?php if ($var['parentId']) { ?>
                    <a target="ajax" dataType="html" call="defaultList"
                       href="<?= "countData/betTDateSearch/?uid={$var['uid']} &fromTime={$para['fromTime']}&toTime={$para['toTime']}" ?>">上级</a>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td><span class="spn9">本页总结</span></td>
        <td><?= $this->ifs($this->nformat($count['rechargeAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['cashAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['betAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['zjAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['fanDianAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['bonusAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['rebateAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['cancelOrderAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['brokerageAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['coin']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['teamwin']), '--') ?></td>
        <td></td>
    </tr>
    </tbody>
</table>
<footer>
    <?php
    $rel = get_class($this) . '/betTDateSearch-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $list['total'], $rel, 'defaultReplacePageAction');
    ?>
</footer>
<script type="text/javascript">
    ghhs("nav01", "tr");
</script>