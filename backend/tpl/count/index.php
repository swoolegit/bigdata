<article class="module width_full">
    <header>
    	<h3 class="tabs_involved">盈亏统计</h3>
    </header>
        <?php
        $data_array=[];
        $data = $this->getOldDateCount7();
        //print_r($data);
        foreach($data as $k=>$v)
        {
            $YKCount = $v['betAmount'] - $v['zjAmount'];
            $TCount = $v['betAmount'];
            $data_array[]=[
                'date'=>$v['date'],
                'TCount'=>$TCount,
                'YKCount'=>$YKCount,
            ];
        }
        asort($data_array);
        $chart=array();
        $chart['tooltip']['show']=true;
        $chart['legend']['data']=array("投注额","盈亏");
        $chart['yAxis'][]=array('type'=>"value",'axisLabel'=>array('formatter'=>'{value}'));
        $title_data=array();
        $tcount_data=array();
        $ykcount_data=array();
        foreach ($data_array as $t) {
            $title_data[]=$t['date'];
            $tcount_data[]=intval($t['TCount']);
            $ykcount_data[]=intval($t['YKCount']);
        }
        $chart['xAxis'][]=array('type'=>"category",'boundaryGap'=>"false",'data'=>$title_data);
        $chart['series'][]=array('name'=>"投注额",'type'=>"bar",'data'=>$tcount_data);
        $chart['series'][]=array('name'=>"盈亏",'type'=>"bar",'data'=>$ykcount_data);
        //echo json_encode($chart);        
        ?>
        <div id="main_chart" style="height:400px;width:1200px"></div>
    <div class="clear"></div>
</article>

<?php
$today = strtotime('today');
$sql =
    "SELECT
        COUNT(*) AS allUser,
        COUNT(IF(regTime >=:regTime, 1, NULL)) AS todayReg,
        SUM(coin ) AS amountCount
    FROM {$this->prename}members
    WHERE isDelete =0 and enable=1";
$data = $this->getRow($sql, array('regTime' => $today));
?>
<article class="module width_full">
    <header><h3 class="tabs_involved">用户统计</h3></header>
    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
            <th>用户总数</th>
            <th>今日注册人数</th>
            <!--th>代理人数</th>
            <th>会员人数</th-->
            <th>当前在线人数</th>
            <th>余额总数</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?= $data['allUser'] ?></td>
            <td><?= $data['todayReg'] ?></td>
            <!--td><?= $data['dlCount'] ?></td>
            <td><?= $data['memberCount'] ?></td-->
            <td><?= $this->getValue("select count(distinct uid) from {$this->prename}member_session where isOnLine=1 and accessTime >= ".($this->time-900)) ?></td>
            <td><?= number_format($data['amountCount']) ?></td>
        </tr>
        </tbody>
    </table>
</article>

<article class="module width_full">
    <header><h3 class="tabs_involved">彩种投注金额统计<span class="spn1">（彩种名称：投注金额）</span></h3></header>
    <div class="module_content">
        <?php
        $sql="select r.type,sum(r.amount) as amount from {$this->prename}report_type r group by r.type";
        $data = $this->getObject($sql, 'type');
        $this->getTypes();
        if ($this->types) foreach ($this->types as $var) {
            if ($var['isDelete'] == 0 && $var['enable'] == 1) {
                ?>
                <div class="cztz"><span class="title"><?= $var['title'] ?></span><span
                        class="spn2">￥<?= number_format($this->ifs($data[$var['id']]['amount'], 0), 2) ?></span></div>
            <?php }
        } ?>
    </div>
</article>

<script>
$(function() {
    //{"tooltip":{"show":true},"legend":{"data":["\u6295\u6ce8\u984d","\u76c8\u8667"]},"yAxis":[{"type":"value","axisLabel":{"formatter":"{value}"}}],"xAxis":[{"type":"category","boundaryGap":"false","data":["9","5","1","1","6","2","0","2019-09-25"]}],"series":[{"name":"\u6295\u6ce8\u984d","type":"line","data":["9","5","1","1","6","2","0","927.30"]},{"name":"\u76c8\u8667","type":"line","data":["9","5","1","1","6","2","0","9000.00"]}]}
    var myChart = echarts.init(document.getElementById('main_chart'),'macarons');
    var option =JSON.parse('<?=json_encode($chart)?>');
    // 为echarts对象加载数据 
    myChart.setOption(option);	
});
</script>