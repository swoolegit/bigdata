<?php
$para = $_GET;
// 时间限制
if ($para['fromTime'] && $para['toTime']) {
    $fromTime = strtotime($para['fromTime']);
    $toTime = strtotime($para['toTime']) + 24 * 3600;
    $betTimeWhere = "and r.actionTime between $fromTime and $toTime";
} elseif ($para['fromTime']) {
    $fromTime = strtotime($para['fromTime']);
    $betTimeWhere = "and r.actionTime >=$fromTime";
} elseif ($para['toTime']) {
    $toTime = strtotime($para['toTime']) + 24 * 3600;
    $betTimeWhere = "and r.actionTime < $toTime";
} else {
    $toTime = strtotime('00:00')-1;
    $betTimeWhere = "and r.actionTime > $toTime";
}
// 用户限制
$amountTitle = '全部总结';
$userWhere=1;

if ($para['username'] && $para['username'] != '用户名') {
    $para['username'] = wjStrFilter($para['username']);
    if (!preg_match('/^\w{4,16}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    // 用户名限制
    $userWhere = "u.username='{$para['username']}'";
}else
{
	exit;
}

$sql = "
	select u.uid,u.parents,u.fanDian,u.rebate,u.fenHong FROM `{$this->prename}members` u WHERE {$userWhere} limit 1 
	";
$u = $this->getRow($sql);

$diff=date_diff(date_create($para['fromTime']),date_create($para['toTime']));
$countDay=array();
for($i=0;$i<=$diff->format("%a");$i++)
{
	$countDay[$i]=$fromTime+(86400*$i);
}
$sql="select
 		u.username,u.uid,u.parentId,u.parents,
		r.actionDate, 
		r.actionTime,
		IFNULL((r.recharge),0) as rechargeAmount,
				IFNULL((r.cash),0) as cashAmount , 
				IFNULL((r.real_bet),0) as betAmount,
				IFNULL((r.zj),0) as zjAmount,
				IFNULL((r.fandian),0) as fanDianAmount,
				IFNULL((r.broker),0) as brokerageAmount,
				IFNULL((r.rebate),0) as rebateAmount,
				IFNULL((r.bonus),0) as bonusAmount,
				IFNULL((r.transfer),0) as transfer,
				IFNULL((r.cancelOrder),0) as cancelOrderAmount
		from  {$this->prename}members u , {$this->prename}member_report r where {$userWhere} and r.uid=u.uid {$betTimeWhere} order by r.id asc;
		";
$list['data1'] = $this->getRows($sql);

foreach($countDay as $k=>$v)
{
	foreach($list['data1'] as $k1=>$v1)
	{
		if($v1['actionTime']==$v)
		{
			$list['data'][$k]=$list['data1'][$k1];
			break;
		}
	}
	if(!isset($list['data'][$k]))
	{
		$list['data'][$k]=array(
            'uid' => $u['uid'],
            'actionDate' => date("Y-m-d",$v),
            'actionTime' => $v,
            'rechargeAmount' => 0.000,
            'cashAmount' => 0.000,
            'betAmount' => 0.000,
            'zjAmount' => 0.000,
            'fanDianAmount' => 0.000,
            'brokerageAmount' => 0.000,
            'gongziAmount' => 0.000,
            'bonusAmount' => 0.000,
            'transfer' => 0.000,
            'cancelOrderAmount' => 0.000		
		);		
	}
}
	
$sql="SELECT COUNT(1) as total FROM {$this->prename}members u , {$this->prename}member_report r where {$userWhere} and u.uid=r.uid {$betTimeWhere} ";
$list['total'] = $this->getValue($sql);

if (!$list['total']) {
    $uParentId2 = $this->getValue("select parentId from {$this->prename}members u ", $para['parentId']);
	if(!$uParentId2)
	{
		$noChildren = true;
	}    
}

$params = http_build_query($_REQUEST, '', '&');
$count = array();
/*
$sql="select 
		IFNULL(sum(r.recharge),0) as rechargeAmount,
				IFNULL(sum(r.cash),0) as cashAmount , 
				IFNULL(sum(r.bet),0) as betAmount,
				IFNULL(sum(r.zj),0) as zjAmount,
				IFNULL(sum(r.fandian),0) as fanDianAmount,
				IFNULL(sum(r.broker),0) as brokerageAmount,
				IFNULL(sum(r.gongzi),0) as gongziAmount,
				IFNULL(sum(r.bonus),0) as bonusAmount,
				IFNULL(sum(r.cancelOrder),0) as cancelOrderAmount
		from {$this->prename}members u ,{$this->prename}member_report r where {$userWhere} and u.uid=r.uid {$betTimeWhere}
		";		
$all = $this->getRow($sql);
 *
 */
?>

<table class="tablesorter" cellspacing="0">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <thead>
    <tr>
        <td>日期</td>
        <td>投注</td>
        <td>中奖</td>
        <td>返点 (<?=$u['fanDian']?>)</td>
		<td>分红 (<?=$u['fenHong']?>)</td>
		<td>退水 (<?=$u['rebate']?>)</td>
        <td>撤单</td>
        <td title="包括充值佣金，注册佣金，亏损佣金，消费佣金，签到，活动赠送奖金">佣金及活动</td>
        <td>充值</td>
        <td>提现</td>
        <td>盈亏</td>
    </tr>
    </thead>
    <tbody id="nav01">
    <?php
		
    if ($list['data']) foreach ($list['data'] as $var) {
        if (!$noChildren) {

		    // $fromTime = $var['actionDate'];
		    // $betTimeWhereDay = "and r.actionDate='{$fromTime}'";
            // $pId = $var['uid'];
			// $sql="select sum(r.zj-r.real_bet+r.fandian+r.broker+r.gongzi) as teamwin from
			//  {$this->prename}member_report r , {$this->prename}members u 
			//  where r.uid=u.uid and concat(',', u.parents, ',') like '%,$pId,%' {$betTimeWhereDay}";
			// $var['teamwin'] = $this->getValue($sql);
        }
        $count['betAmount'] += $var['betAmount'];
        $count['zjAmount'] += $var['zjAmount'];
        $count['fanDianAmount'] += $var['fanDianAmount'];
        $count['brokerageAmount'] += $var['brokerageAmount'];
        $count['cashAmount'] += $var['cashAmount'];
        $count['rechargeAmount'] += $var['rechargeAmount'];
        $count['teamwin'] += $var['teamwin'];
		$count['bonusAmount'] += $var['bonusAmount'];
		$count['rebateAmount'] += $var['rebateAmount'];
		$count['cancelOrderAmount'] += $var['cancelOrderAmount'];
		$count['transfer'] += $var['transfer'];
        ?>
        <tr>
            <td><?= $this->ifs($var['actionDate'], '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['betAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['zjAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['fanDianAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['bonusAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['rebateAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['cancelOrderAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['brokerageAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['rechargeAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['cashAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['zjAmount'] - $var['betAmount'] + $var['fanDianAmount'] + $var['brokerageAmount'] + $var['rebateAmount']+$var['bonusAmount']), '--') ?></td>
        </tr>
    <?php } 
    ?>
    <tr>
        <td><span class="spn9">本页总结</span></td>
        <td><?= $this->ifs($this->nformat($count['betAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['zjAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['fanDianAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['bonusAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['rebateAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['cancelOrderAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['brokerageAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['rechargeAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['cashAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['zjAmount'] - $count['betAmount'] + $count['fanDianAmount'] + $count['brokerageAmount'] + $count['rebateAmount']+$var['bonusAmount']), '--') ?></td>
    </tr>
    </tbody>
</table>


    <header>
        <h3 class="tabs_involved">上级统计
        </h3>
    </header>
<?php
$p_array=explode(",",$u['parents']);
$key=array_search($u["uid"], $p_array);
array_splice($p_array,$key);
if(count($p_array)>0)
{
	$userWhere=" u.uid in (".implode(",", $p_array).")";
	$sql="select u.username,u.coin,u.uid,u.fanDian,u.gongZi,u.fenHong,
			IFNULL(sum(r.recharge),0) as rechargeAmount,
					IFNULL(sum(r.cash),0) as cashAmount , 
					IFNULL(sum(r.real_bet),0) as betAmount,
					IFNULL(sum(r.zj),0) as zjAmount,
					IFNULL(sum(r.fandian),0) as fanDianAmount,
					IFNULL(sum(r.broker),0) as brokerageAmount,
					IFNULL(sum(r.rebate),0) as rebateAmount,
					IFNULL(sum(r.bonus),0) as bonusAmount,
					IFNULL((r.transfer),0) as transfer,
					IFNULL(sum(r.cancelOrder),0) as cancelOrderAmount
			from {$this->prename}members u , {$this->prename}member_report r where {$userWhere} and u.uid=r.uid {$betTimeWhere} group by r.uid 
			";
	$list['data'] = $this->getRows($sql);	
}
else {
	$list['data']=array();
}
?>
<table class="tablesorter" cellspacing="0">
    <thead>
    <tr>
        <td>帐号</td>
        <td>投注</td>
        <td>中奖</td>
        <td>返点(%)</td>
		<td>分红(%)</td>
		<td>退水(%)</td>
        <td>撤单</td>
        <td title="包括充值佣金，注册佣金，亏损佣金，消费佣金，签到，活动赠送奖金">佣金及活动</td>
        <td>充值</td>
        <td>提现</td>
        <td>盈亏</td>
        <td>操作</td>
    </tr>
    </thead>
    <tbody id="nav01">
    <?php
		
    if ($list['data']) foreach ($list['data'] as $var) {
        if (!$noChildren) {
		    // $fromTime = $var['actionDate'];
		    // $betTimeWhere = "and r.actionDate='{$fromTime}'";
            // $pId = $var['uid'];
			// $sql="select sum(r.zj-r.real_bet+r.fandian+r.broker+r.gongzi) as teamwin from
			//  {$this->prename}member_report r , {$this->prename}members u 
			//  where r.uid=u.uid and concat(',', u.parents, ',') like '%,$pId,%' {$betTimeWhere}";
			// $var['teamwin'] = $this->getValue($sql);
        }
        $count['betAmount'] += $var['betAmount'];
        $count['zjAmount'] += $var['zjAmount'];
        $count['fanDianAmount'] += $var['fanDianAmount'];
        $count['brokerageAmount'] += $var['brokerageAmount'];
        $count['cashAmount'] += $var['cashAmount'];
        $count['rechargeAmount'] += $var['rechargeAmount'];
        //$count['teamwin'] += $var['teamwin'];
		$count['bonusAmount'] += $var['bonusAmount'];
		$count['rebateAmount'] += $var['rebateAmount'];
		$count['cancelOrderAmount'] += $var['cancelOrderAmount'];
		$count['transfer'] += $var['transfer'];
        ?>
        <tr>
            <td><?= $this->ifs($var['username'], '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['betAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['zjAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['fanDianAmount']), '--') ?> (<?=$var['fanDian']?>)</td>
            <td><?= $this->ifs($this->nformat($var['bonusAmount']), '--') ?> (<?=$var['fenHong']?>)</td>
            <td><?= $this->ifs($this->nformat($var['rebateAmount']), '--') ?> (<?=$var['rebate']?>)</td>
            <td><?= $this->ifs($this->nformat($var['cancelOrderAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['brokerageAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['rechargeAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['cashAmount']), '--') ?></td>
            <td><?= $this->ifs($this->nformat($var['zjAmount'] - $var['betAmount'] + $var['fanDianAmount'] + $var['brokerageAmount'] + $var['rebateAmount'] + $var['bonusAmount']), '--') ?></td>
            <td>
            	<a href="business/coinLog?username=<?= $var['username'] ?>&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>">帐变</a> |
            	<a href="member/index?type=2&uid=<?= $var['uid'] ?>">下级</a> |
            	<a href="business/betLog?username=<?= $var['username'] ?>&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>">投注</a> 
            </td>
        </tr>
    <?php } 
    ?>
    <tr>
        <td><span class="spn9">本页总结</span></td>
        <td><?= $this->ifs($this->nformat($count['betAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['zjAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['fanDianAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['bonusAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['rebateAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['cancelOrderAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['brokerageAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['rechargeAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['cashAmount']), '--') ?></td>
        <td><?= $this->ifs($this->nformat($count['zjAmount'] - $count['betAmount'] + $count['fanDianAmount'] + $count['brokerageAmount'] + $count['rebateAmount'] + $var['bonusAmount']), '--') ?></td>
    </tr>
    </tbody>
</table>

<script type="text/javascript">
    ghhs("nav01", "tr");
</script>