<?php
$para = $_GET;
//$dateDay = date('d', time());
if(empty($para['fromTime'])) $para['fromTime']=0;
//$dayBeforeTen = date("Y-m-d", strtotime("+1 day",$para['fromTime']));

$fromTime = $para['fromTime'];
//$dayBeforeOne = date("Y-m-d", strtotime("-1 day"));
$toTime = $para['toTime'];

$this->getSystemSettings();
$sql = "select uid,username,fanDian from {$this->prename}members WHERE uid=?";
$data = $this->getRows($sql, $para['uid']);
$bonusUser = $data[0];
?>
<div class="manager-edit">
    <form action="/index.php/Agent/shareBonusSingle/<?= $para['uid'] ?>" target="ajax" method="post" call="agentshareBonusHandle"
          dataType="html" >
        <table class="tablesorter left" cellspacing="0">
            <tbody>
            <tr>
                <td>用户名</td>
                <td><?= $bonusUser['username'] ?></td>
            </tr>
            <tr>
                <td>本期分红起始时间</td>
                <td><input type="date" class="alt_btn" name="startTime" value="<?= date('Y-m-d', $fromTime) ?>" name="fromTime"/> 到 <input type="date" value="<?= date('Y-m-d', $toTime) ?>" class="alt_btn"
                                                                                   name="endTime"/></td>
            </tr>
            <tr>
                <td>注意</td>
                <td>
                	<ul>
					  <li>发放分红包含此用户及此用户所有下级</li>
					  <li>系统会自动过滤已经发送过的会员,不会重复发送</li>
					  <li>系统会自动补时分秒,请勿输入时分秒</li>
					  <li>起始时间从00:00:00开始算,结束时间为23:59:59。例如起始时间2017/01/01 00:00:00,结束时间2017/01/15 23:59:59</li>
					</ul>  
                	
                </td>
            </tr>

            </tbody>
        </table>
    </form>
</div>