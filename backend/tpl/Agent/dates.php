<?php
$para = $_GET;
// 时间限制
if ($para['fromTime'] && $para['toTime']) {
    $fromTime = strtotime($para['fromTime']);
    $toTime = strtotime($para['toTime']) + 24 * 3600;
    $betTimeWhere = "and r.actionTime between $fromTime and $toTime";
} elseif ($para['fromTime']) {
    $fromTime = strtotime($para['fromTime']);
    $betTimeWhere = "and r.actionTime >=$fromTime";
} elseif ($para['toTime']) {
    $toTime = strtotime($para['toTime']) + 24 * 3600;
    $betTimeWhere = "and r.actionTime < $toTime";
} else {
    $toTime = strtotime('00:00');
	$fromTime = strtotime('00:00');
    $betTimeWhere = "and r.actionTime > $toTime";
} 

?>
<script type="text/javascript">
    $(function () {
        $('.tabs_involved form input[name=username]')
            .focus(function () {
                if (this.value == '用户名') this.value = '';
            })
            .blur(function () {
                if (this.value == '') this.value = '用户名';
            })
            .keypress(function (e) {
                if (e.keyCode == 13) $(this).closest('form').submit();
            });

    });

</script>

<article class="module width_full">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <header>
        <h3 class="tabs_involved">综合统计
            <form action="/index.php/Agent/betDateSearchs" target="ajax" dataType="html" call="defaultList"
                  class="submit_link wz">
                时间：从 <input type="date" class="alt_btn" name="fromTime" value="<?=date("Y-m-d",$fromTime)?>"/> 
                到 <input type="date" class="alt_btn" name="toTime" value="<?=date("Y-m-d",$toTime)?>" />&nbsp;&nbsp;
                <input type="text" class="fqr-in" height="20" value="用户名" name="username"/>&nbsp;&nbsp;
                <input type="submit" value="查找" class="alt_btn">
            </form>
        </h3>
    </header>

    <div class="tab_content">
        <?php $this->display("Agent/date-lists.php"); ?>
    </div>
</article>