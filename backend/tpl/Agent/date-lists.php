<?php
$para = $_GET;
// 时间限制
if ($para['fromTime'] && $para['toTime']) {
    $fromTime = strtotime($para['fromTime']);
    $toTime = strtotime($para['toTime']) ;
    $betTimeWhere = "and r.actionTime between $fromTime and $toTime";
} elseif ($para['fromTime']) {
    $fromTime = strtotime($para['fromTime']);
    $betTimeWhere = "and r.actionTime >=$fromTime";
} elseif ($para['toTime']) {
    $toTime = strtotime($para['toTime']);
    $betTimeWhere = "and r.actionTime < $toTime";
} else {
    $toTime = strtotime('00:00');
	$fromTime = strtotime('00:00');
    $betTimeWhere = "and r.actionTime > $toTime";
} 
$sql="select u.username,u.coin,u.uid,u.parentId,u.parents,u.fenHong,u.fanDian,u.gongZi,
			u.forTest
		from {$this->prename}members u  
		where u.business_agent=1
		";
$list['data'] = $this->getRows($sql);
?>

<table class="tablesorter" cellspacing="0">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <thead>
    <tr>
        <td>用户名</td>
        <td>返点比(%)</td>
        <td>已发返点</td>
		<td>工资比(%)</td>
		<td>已发工资</td>        
        <td>分红比(%)</td>
		<td>未发分红</td>
		<td>团队投注亏损(中奖-有效投注-返点-工资-活动)</td>
        <td>上次分红</td>
        <td>操作</td>
    </tr>
    </thead>
    <tbody id="nav01">
<?php
	foreach ($list['data'] as $var) {
		$sql="select endTime from {$this->prename}bonus_log where uid=".$var['uid']." order by endTime desc limit 1";
		$endTime=$this->getRow($sql);
		$var['endTime']=$endTime['endTime'];
		if(!$var['endTime'])
		{
			$var['endTime']=0;
		}
		
		$sql="select r.uid,
					IFNULL(sum(r.fandian),0) as fanDianAmount,
					IFNULL(sum(r.broker),0) as brokerageAmount,
					IFNULL(sum(r.gongzi),0) as gongziAmount,
					IFNULL(sum(r.bonus),0) as bonusAmount
				from  
					 {$this->prename}member_report r 
				where r.uid={$var['uid']} and r.actionTime >= {$fromTime} and r.actionTime <= {$toTime} group by r.uid 
				";
		$all = $this->getRow($sql);
		$sql="select sum(r.zj+r.fandian+r.broker+r.gongzi-r.real_bet) as teamwin from
		 {$this->prename}member_report r , {$this->prename}members u 
		 where r.uid=u.uid and concat(',', u.parents, ',') like '%,{$var['uid']},%' and r.actionTime >= {$fromTime} and  r.actionTime <= {$toTime}";
		$var['teamwin'] = $this->getValue($sql);
			/*
			 * begin
			 * 下级投注亏损分红计算
			 */
		
		if($var['teamwin'] < 0)
		{
            $childUidarr = $this->getRows("select uid,fenHong from {$this->prename}members where parentId={$var['uid']}");
			foreach ($childUidarr as $child) {
				$sql="select sum(r.zj+r.fandian+r.broker+r.gongzi-r.real_bet) as teamwin 
				 from {$this->prename}member_report r , {$this->prename}members u 
				 where r.uid=u.uid and concat(',', u.parents, ',') like '%,{$child['uid']},%' and r.actionTime > {$var['endTime']} ";
				$bet_total=$this->getValue($sql);
				
				if($bet_total < 0)
				{
					$bet_total*=-1;
					$var['fenHongAmount'] += $bet_total*($var['fenHong']-$child['fenHong'])/100;					
				}
			}
			$var['fenHongAmount']=$var['teamwin']*$var['fenHong']/100*-1;
		}
		
			/*
			 * end
			 * 下级投注亏损分红计算 
			 */
			/*
			 * begin
			 * 自己投注亏损分红计算
			 * 招商号不投注
			 */
			/*$bet_total=$this->getValue("select sum(b.bonus-b.mode * b.beiShu * b.actionNum) betZjAmount from {$this->prename}members u ,{$this->prename}bets_repl b where b.isDelete=0 and u.uid=b.uid and u.uid=$pId and b.lotteryNo<>'' $betTimeWhere");
			$bet_total=$var['zjAmount']-$var['betAmount'];
			if($bet_total < 0)
			{
				$bet_total*=-1;
				$var['fenHongAmount'] += $bet_total*$var['fenHong']/100;
			}
			 */
			/*
			 * end
			 * 自己投注亏损分红计算 
			 */
        
        $count['fanDianAmount'] += $all['fanDianAmount'];
        $count['brokerageAmount'] += $all['brokerageAmount'];
		$count['fenHongAmount'] += $var['fenHongAmount'];
		$count['gongziAmount'] += $all['gongziAmount'];
		$count['teamwin'] += $var['gongZi'];
        ?>
        <tr>
            <td><?= $this->ifs($var['username'], '--') ?></td>
            <td><?= $this->ifs($var['fanDian'], '--') ?>%</td>
            <td><?= $this->ifs($all['fanDianAmount'], '--') ?></td>
            <td><?= $this->ifs($var['gongZi'], '--') ?>%</td>
            <td><?= $this->ifs($all['gongziAmount'], '--') ?></td>
            <td><?= $this->ifs($var['fenHong'], '--') ?>%</td>
            <td><?= $this->ifs($var['fenHongAmount'], '--') ?></td>
            <td><?= $this->ifs($var['teamwin'], '--') ?></td>
            <td>
            <?php
            if($var['endTime'])
			{
				echo date('Y-m-d H:i:s', $var['endTime']);
			}else
			{
				echo '--';
			}
            ?>
            </td>
            <td>
		<?php
		if($var['teamwin'] < 0)
		{
		?>
                      <a href="<?= "/index.php/Agent/shareBonusModal/?uid={$var['uid']}&fromTime={$fromTime}&toTime={$toTime}" ?>" target="ajax"
                      call="shareBonusModal">发放分红</a>		
		<?php
		}else
		{
			echo "--";
		}
		?>            	
            </td>
        </tr>
    <?php } ?>
    <tr>
        <td><span class="spn9">本页总结</span></td>
            <td></td>
            <td><?= $this->ifs($count['fanDianAmount'], '--') ?></td>
            <td></td>
            <td><?= $this->ifs($count['gongziAmount'], '--') ?></td>
            <td></td>
            <td><?= $this->ifs($count['fenHongAmount'], '--') ?></td>
            <td><?= $this->ifs($count['teamwin'], '--') ?></td>
        <td></td>
        <td></td>
    </tr>
    </tbody>
</table>
<script type="text/javascript">
    ghhs("nav01", "tr");
</script>