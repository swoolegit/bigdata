<?php
$al_id = isset($_GET['al_id']) ? intval($_GET['al_id']) : '';
if (! $al_id) {
	echo '<h3>资料错误</h3>';
	return;
}

$sql =
	"SELECT operationRecord
	FROM {$this->prename}admin_log
	WHERE id =:id";
$row = $this->getRow($sql, array('id' => $al_id));

if ($row['operationRecord']) {
	ob_start();
	print_r(json_decode($row['operationRecord'], true));
	$result = '<pre>' . ob_get_contents() . '</pre>';
	ob_end_clean();
} else {
	$result = '<h3>无记录</h3>';
}
?>

<article class="module width_full">
	<div class="tab_content">
		<?php echo $result; ?>
	</div>
</article>
