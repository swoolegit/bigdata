<?php
$para = $_GET;

// 用户限制
if ($para['username']) {
    $para['username'] = wjStrFilter($para['username']);
    if (!preg_match('/^\w{4,16}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    $userWhere = "and u.username like '%{$para['username']}%'";
}

// IP限制
if ($para['ip'] = ip2long($para['ip'])) {
    $para['ip'] = wjStrFilter($para['ip']);
    $ipWhere = " and l.actionIp='{$para['ip']}'";
}

// 日志类型限制
if ($para['type'] = intval($para['type'])) {
    $typeWhere = "and l.type={$para['type']}";
}

// 时间限制
if ($_REQUEST['fromTime'] && $_REQUEST['toTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $toTime = strtotime($_REQUEST['toTime']) + 24 * 3600;
    $timeWhere = "l.actionTime between {$fromTime} and {$toTime}";
} elseif ($_REQUEST['fromTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $timeWhere = "l.actionTime >={$fromTime}";
} elseif ($_REQUEST['toTime']) {
    $toTime = strtotime($_REQUEST['toTime']) + 24 * 3600;
    $timeWhere = "l.actionTime <{$toTime}";
} else {
    $timeWhere = 'l.actionTime >' . strtotime('00:00');
}

$sql =
    "SELECT
        l.id,
        l.type,
        l.actionTime,
        l.actionIP,
        l.action,
        l.extfield0,
        l.extfield1,
        IF(l.operationRecord = '' OR l.operationRecord IS NULL, 0, 1) AS recorded,
        u.username
    FROM {$this->prename}admin_log AS l
        LEFT JOIN {$this->prename}admin_members AS u ON u.uid = l.uid
    WHERE
        {$timeWhere}
        {$typeWhere}
        {$userWhere}
        {$ipWhere}
    ORDER BY l.id DESC";
$data = $this->getPage($sql, $this->page, $this->pageSize);
?>
<table class="tablesorter" cellspacing="0">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <thead>
    <tr>
        <th>时间</th>
        <th>管理员</th>
        <th>操作类型</th>
        <th>登录IP</th>
        <th>操作描述</th>
        <th>对应ID</th>
        <th>操作对象</th>
    </tr>
    </thead>
    <tbody id="nav01">
    <?php if ($data['data']) foreach ($data['data'] as $var) { ?>
        <tr>
            <td><?= date('m-d H:i:s', $var['actionTime']) ?></td>
            <td><?= $var['username'] ?></td>
            <td>
                <?= $this->adminLogType[$var['type']] ?>
                <?php if (in_array($var['type'], array(10, 12, 13)) && $var['recorded']) {
                    echo ' (<span class="show-op-record-btn clickable spn6" data-al-id="' . $var['id'] . '" data-title="' . $var['username'] . ' - ' . date('m-d H:i:s', $var['actionTime']) . '">查看</span>)';
                } ?>
            </td>
            <td><?= long2ip($var['actionIP']) ?></td>
            <td><?= $this->ifs($var['action'], '--') ?></td>
            <td><?= $this->ifs($var['extfield0'], '--') ?></td>
            <td><?= $this->ifs($var['extfield1'], '--') ?></td>
        </tr>
    <?php } else { ?>
        <tr>
            <td colspan="7">暂时没有Log</td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<footer>
    <?php
    $rel = get_class($this) . '/controlLog-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $data['total'], $rel, 'betLogSearchPageAction');
    ?>
</footer>
<script type="text/javascript">
ghhs("nav01", "tr");

$(function() {
    $(".show-op-record-btn").click(function() {
        var al_id = this.getAttribute("data-al-id");
        var title = this.getAttribute("data-title");

        $.ajax({
            url: "/manage/showOperationRecord",
            data: {al_id: al_id},
            method: "GET",
            dataType: "html",
            success: function(data) {
                $(data).dialog({
                    title: "操作记录 (" + title + ")",
                    width: 400,
                    //resizable: false,
                    buttons: [
                        {
                            text: "关闭",
                            click: function() {
                                $(this).dialog("destroy");
                            }
                        }
                    ]
                });
            },
            error: function() {
                alert("发生错误!!");
            }
        });
    });
});
</script>