<article class="module width_full">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <header>
        <h3 class="tabs_involved">操作日志
            <form id="query-form" class="submit_link wz" action="/manage/controlLogList" target="ajax" dataType="html" onajax="adminLogBeforeSubmit" call="adminLogList">
                管理员：<input type="text" class="alt_btn" style="width:100px;" name="username" placeholder="管理员" />　
                IP：<input type="text" class="alt_btn" style="width:100px;" name="ip" />　
                类型：<select style="width:100px" name="type">
                    <option value="">所有类型</option>
                    <?php foreach ($this->adminLogType as $k => $v) {
                        echo '<option value="' . $k . '">' . $v . '</option>';
                    } ?>
                </select>　
                时间：
                    从 <input type="text" class="alt_btn" name="fromTime" value="<?= date('Y-m-d', $this->time) ?>" />
                    到 <input type="text" class="alt_btn" name="toTime" />　
                <input type="submit" value="查找" class="alt_btn">
                <input type="reset" value="重置条件">
            </form>
        </h3>
    </header>

    <div class="tab_content">
        <?php $this->display("manage/control-log-list.php") ?>
    </div>
</article>


<script type="text/javascript">
$(function () {
    $('.tabs_involved input[name=username]')
        .keypress(function (e) {
            if (e.keyCode == 13) $(this).closest('form').submit();
        });

    var f = document.getElementById("query-form"),
        $fromTime = $(f.fromTime),
        $toTime = $(f.toTime);

    $fromTime.datepicker({
        onSelect: function(dateText, inst) {
            $toTime.datepicker("option", "minDate", dateText);
        }
    });
    $toTime.datepicker({
        onSelect: function(dateText, inst) {
            $fromTime.datepicker("option", "maxDate", dateText);
        }
    });
});

function adminLogBeforeSubmit() {
}

function adminLogList(err, data) {
    if (err) {
        alert(err);
    } else {
        $('.tab_content').html(data);
    }
}
</script>
