<?php
$sql = "select * from {$this->prename}params_thirdpay ";
$data = $this->getPage($sql, $this->page, $this->pageSize);
?>
<div class="system-info"> 
    <ul>
        <li>渠道配置会员等级,会员满足等级後只能看到此充值渠道(<font color=red>不含所有等级渠道</font>)</li>
        <li>会员等级满足多渠道时,系统默认以等级最低的渠道优先</li>
        <li>同时段代付只能激活一个</li>
    </ul>
    
</div>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved" >收款设置
            <div class="tabs_btn" >
                <a href="/system/thirdpayModal/0" width="510" 
                        title="添加三方"
                        target="modal" modal="true" button="确定:dataAddCode|取消:defaultCloseModal"> 
                    添加三方 
                </a> 
            </div>
        </h3>
    </header>
    <table class="tablesorter" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>ID</td>
            <td>名称</td>
            <td>商户号</td>
            <td>密钥</td>
            <td>支付状态</td>
            <td>代付状态</td>
            <td>会员等级</td>
            <td>备注</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <?php if ($data['data']) foreach ($data['data'] as $var) { ?>
            <tr>
                <td><?= $var['id'] ?></td>
                <td><?= $var['name'] ?></td>
                <td><?= $var['merid'] ?></td>
                <td><?= $var['addcrykey'] ?></td>
                <td><?= $this->iff($var['payin_status'], '开', '关') ?></td>
                <td><?= $this->iff($var['payout_status'], '开', '关') ?></td>
                <td><?php
                if($var['lvmin']==0 && $var['lvmax']==0)
                {
                    echo "所有等级";
                }else
                {
                    echo "须求等级 >= ".$var['lvmin']." AND "."须求等级 <= ".$var['lvmax'];
                }
                ?></td>
                <td><?= $var['memo'] ?></td>
                <td><a href="/system/switchPayinStatus/<?= $var['id'] ?>" target="ajax"
                       call="rechargeSubmitCode"><?= $this->iff($var['payin_status'], '支付关闭', '支付开启') ?></a> | 
                       <a href="/system/switchPayoutStatus/<?= $var['id'] ?>" target="ajax"
                       call="rechargeSubmitCode"><?= $this->iff($var['payout_status'], '代付关闭', '代付开启') ?></a> |                        
                       <a href="/system/thirdpayModal/<?= $var['id'] ?>" width="510" 
                       title="收款设置"
                       target="modal" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">修改</a>
                </td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="8">暂时没有三方信息，请点右上角按钮添加三方</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <footer>
    <?php
    $rel = get_class($this) . '/thirdpay?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, 1, $rel, 'defaultReplacePageAction');
    ?>
</footer>    
</article>