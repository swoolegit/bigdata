<div>
<form name="system_addBank"  id="my_form" action="/system/upthirdpay" enctype="multipart/form-data" 
    target="ajax" method="post" call="rechargeSubmitCode"
           dataType="html"
>
    <?php
    $id = intval($args[0]);
    $sql = "select * from {$this->prename}params_thirdpay where id={$id}";
    $bank = $this->getRow($sql);

    $sql = "select * from {$this->prename}member_level order by level";
    $level = $this->getRows($sql);

    $check=true;
    if(!$bank)
    {
        $check=false;
        $bank=[
            'name'=>'',
            'merid'=>'',
            'addcrykey'=>'',
            'memo'=>'',
            'payout_status'=>0,
            'payin_status'=>0,
        ];
    }
    if ($bank) {
        ?>
        <?php
            if($check)
            {
        ?>
                <input type="hidden" name="id" value="<?php echo $bank['id']; ?>">
        <?php
            }
        ?>
        <table class="tablesorter left" cellspacing="0" width="100%">
            <tbody>
            <tr>
                <td>三方名称</td>
                <td>
                    <input type="text" name="name" value="<?= $bank['name'] ?>"/>
                </td>
            </tr>
            <tr>
                <td>商户号</td>
                <td>
                    <input type="text" name="merid" value="<?= $bank['merid'] ?>"/>
                </td>
            </tr>
            <tr>
                <td>密钥</td>
                <td><input type="text" name="addcrykey" value="<?= $bank['addcrykey'] ?>"/></td>
            </tr>
            <tr>
                <td>支付状态</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="payin_status"<?php if ($bank['payin_status']) echo ' checked="checked"'; ?>>开启</label>
                    <label><input type="radio" value="0"
                                  name="payin_status"<?php if (!$bank['payin_status']) echo ' checked="checked"'; ?>>关闭</label>
                </td>
            <tr>
            <tr>
                <td>代付状态</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="payout_status"<?php if ($bank['payout_status']) echo ' checked="checked"'; ?>>开启</label>
                    <label><input type="radio" value="0"
                                  name="payout_status"<?php if (!$bank['payout_status']) echo ' checked="checked"'; ?>>关闭</label>
                </td>
            <tr>
            <tr>
                <td>会员等级 >= </td>
                <td>
                    <select style="width:100px;" name="lvmin">
                        <option value="0">0</option>
                        <?php
                        foreach($level as $k=>$v)
                        {
                            if($v['level']==$bank['lvmin'])
                            {
                                echo "<option value='{$v['level']}' selected>{$v['level']}</option>";
                            }else
                            {
                                echo "<option value='{$v['level']}'>{$v['level']}</option>";
                            }
                        }
                        ?>                     
                    </select>(大于) 0则不限
                </td>
            </tr>
            <tr>
                <td>会员等级 <= </td>
                <td>
                    <select style="width:100px;" name="lvmax">
                        <option value="0">0</option>
                        <?php
                        foreach($level as $k=>$v)
                        {
                            if($v['level']==$bank['lvmax'])
                            {
                                echo "<option value='{$v['level']}' selected>{$v['level']}</option>";
                            }else
                            {
                                echo "<option value='{$v['level']}'>{$v['level']}</option>";
                            }
                        }
                        ?>                     
                    </select>(小于) 0则不限
                </td>
            </tr>
            <tr>
                <td>备注</td>
                <td>
                <textarea cols="30" rows="3" name="memo"><?= $bank['memo'] ?></textarea>
            </tr>
            <tr><td colspan="2">说明: 渠道配置会员等级,会员满足等级後只能看到此充值渠道(<font color=red>不含所有等级渠道</font>)</td></tr>
            </tbody>
        </table>
    <?php } ?>
</form>
</div>