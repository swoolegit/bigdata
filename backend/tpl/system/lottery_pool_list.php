<?php
$sql = "select a.id,b.title,b.killrule,a.bet,a.zj from {$this->prename}ziki_type a,{$this->prename}type b where a.type=b.id and b.enable=1 ";
$data = $this->getRows($sql);
?>
<div class="system-info">
<pre>
说明: 
1. 满足期望输赢时,系统会自动停止杀数. 反之低于期望输赢时,系统自动开启杀数 
2. 奖池每周日零时自动重置
</pre>
</div>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved" >奖池设置
        </h3>
    </header>
    <form name="system_install" action="/system/PoolSettings" method="post" target="ajax" call="sysSettings"
          onajax="sysSettingsBefor">
    <table class="tablesorter" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>彩种</td>
            <td>总投注额</td>
            <td>总派奖</td>
            <td>目前输赢(%)</td>
            <td>期望输赢(杀数%)</td>
        </tr>
        </thead>
        <tbody>
        <?php if ($data) foreach ($data as $var) { ?>
            <tr>
                <input type="hidden" name="dataarr[]" value="<?=$var['id']?>" />
                <td><?= $var['title'] ?></td>
                <td><input type="text" class="textWid1" name="bet[]" value="<?= $var['bet'] ?>"></td>
                <td><input type="text" class="textWid1" name="zj[]" value="<?= $var['zj'] ?>"></td>
                <td><?= ($var['bet'] - $var['zj']) ?> (<?= round((($var['bet'] - $var['zj'])/$var['bet'])*100,2) ?>%)</td>
                <td><?= round(($var['bet']*$var['killrule']/100),2) ?> (<?=$var['killrule']?>%)</td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="9">暂时没有信息</td>
            </tr>
        <?php } ?>
            <tr>
                <td colspan="5"><input type="submit" value="保存修改设置" title="保存设置" class="alt_btn"></td>
            </tr>
        </tbody>
    </table>
    </form>
    <footer>
</footer>    
</article>