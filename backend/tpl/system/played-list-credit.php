<?php
if (!$args[0]) $args[0] = 1;        // 默认查看时时彩玩法
$chiTypes = array(
    1 => '时时彩',
    2 => '11选5',
    3 => '3D/P3/',
    4 => '快乐十分',
    //5=>'系统彩',
    6 => 'PK10',
    8 => '快乐八',
    9 => '快三',
    11 => 'PC蛋蛋',
    12 => '六合彩',
);
$groups = $this->getRows("select * from {$this->prename}played_group where type =? and isGuan=0 order by sort", $args[0]);
$sql = "select * from {$this->prename}played where groupId =? order by sort";
?>


<article class="module width_full">
    <header>
        <h3 class="tabs_involved">玩法设置
            <ul class="tabs" style="margin-right:25px;">
                <?php foreach ($chiTypes as $key => $var) { ?>
                    <li <?= $this->iff($args[0] == $key, 'class="active"') ?>><a
                            href="system/played_credit/<?= $key ?>"><?= $var ?></a></li>
                <?php } ?>
            </ul>
        </h3>
    </header>
    <?php if ($groups) foreach ($groups as $group) { ?>
        <form name="buy" method="post" id="buy">
        <table class="tablesorter" cellspacing="0">
            <thead>
            <tr>
                <th colspan="6" style="text-align:left;">

                    <span style="float:right; margin-right:20px"><a
                            href="system/switchPlayedGroupStatus/<?= $group['id'] ?>" target="ajax"
                            call="reloadPlayed"><?= $this->iff($group['enable'], '关闭', '开启') ?></a></span>
                    <?= $group['groupName'] ?>&nbsp;&nbsp;&nbsp;&nbsp;
                    <span style="float:right; margin-right:20px"><a onclick="plxg(this)" class="plxg btn btn-red" style="color: #77BACE;" >批量保存</a></span>
                    <span class="spn1">[状态：<span class="state1"><?= $this->iff($group['enable'], '开启', '关闭') ?></span>]</span>
                    <div style="margin-left: 455px;margin-top:-20px">预设<input type="text" class="mzinput" style="margin-left: 20px"></div>

                </th>
            </tr>
            </thead>
            <tbody>
            <?php if ($playeds = $this->getRows($sql, $group['id'])) foreach ($playeds as $played) { ?>
                <tr>
                    <input type="hidden" name="dataarr[]" value="<?=$played['id']?>" />
                    <td width="10%"><?= $played['name'] ?></td>
                    <td width="40%">最高奖金：<input type="text" class="textWid1" name="bonusProp[]"
                                                value="<?= $played['bonusProp'] ?>"> (填入玩法最高奖金,奖金依据用户返点浮动)</td>
                    <!--td width="12%">最低奖金：<input type="text" class="textWid1" name="bonusPropBase"
                                                value="<?= $played['bonusPropBase'] ?>"></td>
                    <td width="11%">总注数：<?= $played['allCount'] ?></td>
                    <td width="10%">最高注数：<input type="text" class="textWid1" name="maxCount"
                                                value="<?= $played['maxCount'] ?>"></td-->
                    <td width="20%">投注限额：<input type="text" class="textWid1" name="maxCharge[]"
                                                value="<?= $played['maxCharge'] ?>">元 (0则不限)
                    </td>
                    <td width="120">排序：<input type="text" class="" name="sort[]" value="<?= $played['sort'] ?>" style="width:50px">
                    </td>
                    <!--td width="120">限球数：<input type="text" class="" name="maxBalls" value="<?= $played['maxBalls'] ?>" style="width:50px"-->
                    </td>
                    <td width="10%"><span class="state2"><?= $this->iff($played['enable'], '开启', '关闭') ?></span></td>
                    <td><a href="system/switchPlayedStatus/<?= $played['id'] ?>" target="ajax"
                           call="reloadPlayed"><?= $this->iff($played['enable'], '关闭', '开启') ?></a> | <a
                            href="system/upPlayed/<?= $played['id'] ?>" target="ajax" method="post"
                            onajax="sysBeforeupPlayed" call="reloadPlayed">保存修改</a> <!--| <a
                            href="system/betPlayedInfoUpdate/<?= $played['id'] ?>"
                            button="修改:dataAddCode|取消:defaultCloseModal" title="修改信息" width="510" target="modal"
                            modal="true">修改信息</a-->
                    </td>
                </tr>
            <?php } else { ?>
                <tr>
                    <td colspan="9">暂时没有玩法</td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
        </form>
    <?php } else { ?>
        暂时没有玩法
    <?php } ?>
</article>
<script>
	
$(".textWid1").focus(function(){
    
    var k = $(this).closest("tbody").prev().find("tr").find("th").find(".mzinput").val();

   $(this).val(k);
})

function plxg(obj){
        /*
       $.getJSON("/admin778899.php/system/updateAll",$(obj).closest("#buy").serialize(),function(data){
       
       });
       */
      
       $.post("system/upAll",$(obj).closest("#buy").serializeObject(),function(data){
           if(data) success(data);
           $('#main .tabs .active a').trigger('click');		
       },"html");
   }
   </script>
