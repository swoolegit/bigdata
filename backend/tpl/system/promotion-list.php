<?php
$sql = "select * from {$this->prename}promotion ";
$data = $this->getPage($sql, $this->page, $this->pageSize);
?>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved" >优惠活动设置
            <div class="tabs_btn" >
                <a href="/system/PromotiomModal/0" width="560" 
                        title="添加优惠活动"
                        target="modal" modal="true" button="确定:dataAddCode|取消:defaultCloseModal"
                        style="
                        "
                        > 
                    添加优惠活动                   
                </a> 
            </div>
        </h3>
    </header>
    <table class="tablesorter" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>主题</td>
            <td>副标题</td>
            <td>到期时间</td>
            <td>状态</td>
            <td>备注</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <?php if ($data['data']) foreach ($data['data'] as $var) { ?>
            <tr>
                <td><?= $var['title'] ?></td>
                <td><?= $var['subtitle'] ?></td>
                <td><?= $var['endTime'] ?></td>
                <td><?= $this->iff($var['enable'], '开', '关') ?></td>
                <td><?= $var['memo'] ?></td>
                <td><a href="/system/switchPromotion/<?= $var['id'] ?>" target="ajax"
                       call="sysReloadPromotion"><?= $this->iff($var['enable'], '关闭', '开启') ?></a> | 
                       <a href="/system/PromotiomModal/<?= $var['id'] ?>" width="560" 
                       title="收款设置"
                       target="modal" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">修改</a>
                </td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="8">暂时没有优惠信息，请点右上角按钮添加</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <footer>
    <?php
    $rel = get_class($this) . '/promotion?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $data['total'], $rel, 'defaultReplacePageAction');
    ?>
</footer>    
</article>