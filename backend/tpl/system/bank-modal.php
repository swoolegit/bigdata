<div>
<form name="system_addBank"  id="my_form" action="/system/upBank" enctype="multipart/form-data" 
    target="ajax" method="post" call="rechargeSubmitCode"
           dataType="html"
>
    <?php
    $id = intval($args[0]);
    $sql = "select * from {$this->prename}params_atr where id={$id}";
    $bank = $this->getRow($sql);

    $sql = "select * from {$this->prename}member_level order by level";
    $level = $this->getRows($sql);
    
    $check=true;
    if(!$bank)
    {
        $check=false;
        $bank=[
            'bankselect'=>'',
            'username'=>'',
            'userbankid'=>'',
            'remark'=>'',
            'enable'=>0,
        ];
    }
    if ($bank) {
        ?>
        <?php
            if($check)
            {
        ?>
                <input type="hidden" name="id" value="<?php echo $bank['id']; ?>">
        <?php
            }
        ?>
        <table class="tablesorter left" cellspacing="0" width="100%">
            <tbody>
            <tr>
                <td>渠道名称</td>
                <td>
                    <input type="text" name="bankselect" value="<?= $bank['bankselect'] ?>"/>
                </td>
            </tr>
            <tr>
                <td>收款人</td>
                <td>
                    <input type="text" name="username" value="<?= $bank['username'] ?>"/>
                </td>
            </tr>
            <tr>
                <td>账号</td>
                <td><input type="text" name="userbankid" value="<?= $bank['userbankid'] ?>"/></td>
            </tr>
            <tr>
                <td>客户说明</td>
                <td>
                <textarea cols="30" rows="3" name="remark"><?= $bank['remark'] ?></textarea>
                </td>
            </tr>
            <tr>
                <td>2维码</td>
                <td>
                    <input type="file" name="fileToUpload"/>
                </td>
            </tr>
            <tr>
                <td>会员等级 >= </td>
                <td>
                    <select style="width:100px;" name="lvmin">
                        <option value="0">0</option>
                        <?php
                        foreach($level as $k=>$v)
                        {
                            if($v['level']==$bank['lvmin'])
                            {
                                echo "<option value='{$v['level']}' selected>{$v['level']}</option>";
                            }else
                            {
                                echo "<option value='{$v['level']}'>{$v['level']}</option>";
                            }
                        }
                        ?>                     
                    </select>(大于) 0则不限
                </td>
            </tr>
            <tr>
                <td>会员等级 <= </td>
                <td>
                    <select style="width:100px;" name="lvmax">
                        <option value="0">0</option>
                        <?php
                        foreach($level as $k=>$v)
                        {
                            if($v['level']==$bank['lvmax'])
                            {
                                echo "<option value='{$v['level']}' selected>{$v['level']}</option>";
                            }else
                            {
                                echo "<option value='{$v['level']}'>{$v['level']}</option>";
                            }
                        }
                        ?>                     
                    </select>(小于) 0则不限
                </td>
            </tr>

            <tr>
                <td>状态</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="enable"<?php if ($bank['enable']) echo ' checked="checked"'; ?>>开启</label>
                    <label><input type="radio" value="0"
                                  name="enable"<?php if (!$bank['enable']) echo ' checked="checked"'; ?>>关闭</label>
                </td>
            <tr>
            <tr>
                <td>后端备注</td>
                <td>
                <textarea cols="30" rows="3" name="memo"><?= $bank['memo'] ?></textarea>
                </td>
            </tr>
            <tr><td colspan="2">说明: 渠道配置会员等级,会员须满足等级才能看到此充值渠道(<font color=red>含所有等级渠道</font>)</td></tr>
            </tbody>
        </table>
    <?php } ?>
</form>
</div>