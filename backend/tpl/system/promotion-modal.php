<div>
<form name="system_addBank" id="query-form"  action="/system/upPromotion" enctype="multipart/form-data" 
    target="ajax" method="post" call="rechargeSubmitCode"
           dataType="html"
>
    <?php
    $id = intval($args[0]);
    $sql = "select * from {$this->prename}promotion where id={$id}";
    $bank = $this->getRow($sql);
    $check=true;
    if(!$bank)
    {
        $check=false;
        $bank=[
            'title'=>'',
            'subtitle'=>'',
            'enable'=>0,
            'endTime'=>'',
            'memo'=>'',
        ];
    }
    if ($bank) {
        ?>
        <?php
            if($check)
            {
        ?>
                <input type="hidden" name="id" value="<?php echo $bank['id']; ?>">
        <?php
            }
        ?>
        <table class="tablesorter left" cellspacing="0" width="100%">
            <tbody>
            <tr>
                <td>主题</td>
                <td>
                    <input type="text" name="title" value="<?= $bank['title'] ?>"/>
                </td>
            </tr>
            <tr>
                <td>副标题</td>
                <td>
                    <input type="text" name="subtitle" value="<?= $bank['subtitle'] ?>"/>
                </td>
            </tr>
            <tr>
                <td>到期时间</td>
                <td><input type="text" name="endTime" value="<?= $bank['endTime'] ?>"/> (格式:2019/01/01 18:00:00)</td>
            </tr>
            <tr>
                <td>手机端标题图</td>
                <td>
                    <input type="file" name="fileToUpload_MBanner"/>
            </tr>
            <tr>
                <td>手机端内文图</td>
                <td>
                    <input type="file" name="fileToUpload_MContent"/>
            </tr>
            <tr>
                <td>PC端标题图</td>
                <td>
                    <input type="file" name="fileToUpload_PBanner"/>
            </tr>
            <tr>
                <td>PC端内文图</td>
                <td>
                    <input type="file" name="fileToUpload_PContent"/>
            </tr>
            <tr>
                <td>状态</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="enable"<?php if ($bank['enable']) echo ' checked="checked"'; ?>>开启</label>
                    <label><input type="radio" value="0"
                                  name="enable"<?php if (!$bank['enable']) echo ' checked="checked"'; ?>>关闭</label>
                </td>
            <tr>
            <tr>
                <td>备注</td>
                <td>
                <textarea cols="30" rows="3" name="memo"><?= $bank['memo'] ?></textarea>
            </tr>
            </tbody>
        </table>
    <?php } ?>
</form>
</div>