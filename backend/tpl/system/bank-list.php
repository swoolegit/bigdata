<?php
$sql = "select * from {$this->prename}params_atr ";
$data = $this->getPage($sql, $this->page, $this->pageSize);
?>
<div class="system-info">说明: 渠道配置会员等级,会员须满足等级才能看到此充值渠道(<font color=red>含所有等级渠道</font>)</div>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved" >收款设置
            <div class="tabs_btn" >
                <a href="/system/bankModal/0" width="510" 
                        title="收款设置"
                        target="modal" modal="true" button="确定:dataAddCode|取消:defaultCloseModal"> 
                    添加收款渠道                   
                </a> 
            </div>
        </h3>
    </header>
    <table class="tablesorter" cellspacing="0" width="100%">
        <thead>
        <tr>
            <td>渠道</td>
            <td>帐户名</td>
            <td>帐号</td>
            <td width='15%'>客户说明</td>
            <td>2维码</td>
            <td>状态</td>
            <td>会员等级</td>
            <td width='10%'>后端备注</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody>
        <?php if ($data['data']) foreach ($data['data'] as $var) { ?>
            <tr>
                <td><?= $var['bankselect'] ?></td>
                <td><?= $var['username'] ?></td>
                <td><?= $var['userbankid'] ?></td>
                <td style="word-break: break-all;"><?= $var['remark'] ?></td>
                <td><?= $var['qrcode'] ?></td>
                <td><?= $this->iff($var['enable'], '开', '关') ?></td>
                <td><?php
                if($var['lvmin']==0 && $var['lvmax']==0)
                {
                    echo "所有等级";
                }else
                {
                    echo "须求等级 >= ".$var['lvmin']." AND "."须求等级 <= ".$var['lvmax'];
                }
                ?></td>
                <td><?= $var['memo'] ?></td>
                <td><a href="/system/switchBankStatus/<?= $var['id'] ?>" target="ajax"
                       call="sysReloadBank"><?= $this->iff($var['enable'], '关闭', '开启') ?></a> | 
                       <a href="/system/bankModal/<?= $var['id'] ?>" width="510" 
                       title="收款设置"
                       target="modal" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">修改</a>
                </td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="9">暂时没有银行信息，请点右上角按钮添加银行</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <footer>
    <?php
    $rel = get_class($this) . '/bank?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, 1, $rel, 'defaultReplacePageAction');
    ?>
</footer>    
</article>