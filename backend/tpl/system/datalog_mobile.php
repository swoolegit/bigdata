<?php
//$sql = "select * from {$this->prename}content order by id desc";
//$data = $this->getPage($sql, $this->page, $this->pageSize);

$filesArray = array();
foreach (glob(ADMIN_ROOT."/../m/system/data/log/*.txt") as $filename) {
	$filesArray[] = $filename;
}
rsort($filesArray);
$filesTotal = count($filesArray);
$filesArray = array_splice($filesArray,($this->page-1)*$this->pageSize,$this->pageSize);
$AES = new lib_aes();
//print_r($this->page);
?>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">front - log
        </h3>
    </header>
    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
            <td>No.</td>
            <td>file</td>
            <td>size</td>
        </tr>
        </thead>
        <tbody id="nav01">
        <?php 
		$i = ($this->page-1)*$this->pageSize;
		if ($filesArray) foreach ($filesArray as $var) { ?>
            <tr>
                <td align="left" width="50"><?= ++$i ?></td>
                <td align="left">
                     <a href="/index.php/system/datalogModal/<?= $AES->urlsafe_b64encode($AES->encrypt($var)) ?>" target="modal" width="800"
                               title="front-log <?= $var ?>" modal="true" button="取消:defaultCloseModal"><?= $var ?></a>				
				
				
				</td>
                <td align="left"><?= filesize($var) ?></td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="2">no data</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <footer>
        <?php
        $rel = get_class($this) . '/datalog-{page}?' . http_build_query($_GET, '', '&');
        $this->display('inc/page.php', 0, $filesTotal, $rel, 'betLogSearchPageAction');
        ?>
    </footer>
</article>
<script type="text/javascript">
    ghhs("nav01", "tr");
</script>
