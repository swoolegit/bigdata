<article class="module width_full">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <header><h3 class="tabs_involved">系统设置</h3></header>
    <form name="system_install" action="/system/upSettings" method="post" target="ajax" call="sysSettings"
          onajax="sysSettingsBefor">
        <table class="tablesorter left" cellspacing="0" width="100%">
            <thead>
            <tr>
                <td width="160" style="text-align:left;">配置项目</td>
                <td style="text-align:left;">配置值</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>平台名称</td>
                <td><input type="text" value="<?= $this->settings['webName'] ?>" name="webName"/></td>
            </tr>
            <tr>
                <td>网站开关</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="switchWeb" <?= $this->iff($this->settings['switchWeb'], 'checked="checked"') ?>/>开启</label>
                    <label><input type="radio" value="0"
                                  name="switchWeb" <?= $this->iff(!$this->settings['switchWeb'], 'checked="checked"') ?>/>关闭</label>
                </td>
            </tr>
            <tr>
                <td>网站关闭公告</td>
                <td>
                    <textarea name="webCloseServiceResult" cols="56"
                              rows="5"><?= $this->settings['webCloseServiceResult'] ?></textarea>
                </td>
            </tr>
            <tr>
                <td>总投注开关</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="switchBuy" <?= $this->iff($this->settings['switchBuy'], 'checked="checked"') ?>/>开启</label>
                    <label><input type="radio" value="0"
                                  name="switchBuy" <?= $this->iff(!$this->settings['switchBuy'], 'checked="checked"') ?>/>关闭</label>
                </td>
            </tr>
            <tr>
                <td>代理投注开关</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="switchDLBuy" <?= $this->iff($this->settings['switchDLBuy'], 'checked="checked"') ?>/>开启</label>
                    <label><input type="radio" value="0"
                                  name="switchDLBuy" <?= $this->iff(!$this->settings['switchDLBuy'], 'checked="checked"') ?>/>关闭</label>
                </td>
            </tr>
            <tr>
                <td>总代投注开关</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="switchZDLBuy" <?= $this->iff($this->settings['switchZDLBuy'], 'checked="checked"') ?>/>开启</label>
                    <label><input type="radio" value="0"
                                  name="switchZDLBuy" <?= $this->iff(!$this->settings['switchZDLBuy'], 'checked="checked"') ?>/>关闭</label>
                </td>
            </tr>
            <!--tr>
                <td>上级充值开关</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="recharge" <?= $this->iff($this->settings['recharge'], 'checked="checked"') ?>/>开启</label>
                    <label><input type="radio" value="0"
                                  name="recharge" <?= $this->iff(!$this->settings['recharge'], 'checked="checked"') ?>/>关闭</label>
                </td>
            </tr-->
            <tr>
                <td rowspan="2">自开采杀数开关</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="zikaikill" <?= $this->iff($this->settings['zikaikill'], 'checked="checked"') ?>/>开启</label>
                    <label><input type="radio" value="0"
                                  name="zikaikill" <?= $this->iff(!$this->settings['zikaikill'], 'checked="checked"') ?>/>关闭</label>
                </td>
            </tr>
            <tr>
                <td>
					彩池控制 <input type="number" value="<?= $this->settings['zikaiPool'] ?>" class="textWid1" name="zikaiPool" />  彩池总投注额超过时启动杀数 (每周系统自动重置)
                </td>
            </tr>
            <tr>
                <td>
                    提早封盤
                </td>
                <td>
					 <input type="number" value="<?= $this->settings['closebet'] ?>" class="textWid1" name="closebet" />  被標記用戶提前封盤(單位:秒)
                </td>
            </tr>
            <tr>
            </tr>
            <tr>
                <td>投注模式</td>
                <td>
                    <label><input onchange="setValue(this)" type="checkbox" name="yuanmosi"
                                  value="<?= $this->settings['yuanmosi'] ?>" <?= $this->iff($this->settings['yuanmosi'] == 1, 'checked="checked"') ?>/>元</label>
                    <label><input onchange="setValue(this)" type="checkbox" name="jiaomosi"
                                  value="<?= $this->settings['jiaomosi'] ?>" <?= $this->iff($this->settings['jiaomosi'] == 1, 'checked="checked"') ?>/>角</label>
                    <label><input onchange="setValue(this)" type="checkbox" name="fenmosi"
                                  value="<?= $this->settings['fenmosi'] ?>" <?= $this->iff($this->settings['fenmosi'] == 1, 'checked="checked"') ?>/>分</label>
                    <label><input onchange="setValue(this)" type="checkbox" name="limosi"
                                  value="<?= $this->settings['limosi'] ?>" <?= $this->iff($this->settings['limosi'] == 1, 'checked="checked"') ?>/>厘</label>
                    <br/>
                </td>
            </tr>
            <script type="text/javascript">
                /*
                 $("input[name=yuanmosi]").click(function(){if($(this).attr("checked")==true){$(this).val(1);}else{if(<? echo $this->settings['yuanmosi'];?>==0){$(this).val(1);}else{$(this).val(0);}}})
                 $("input[name=jiaomosi]").click(function(){if($(this).attr("checked")==true){$(this).val(1);}else{if(<? echo $this->settings['jiaomosi'];?>==0){$(this).val(1);}else{$(this).val(0);}}})
                 $("input[name=fenmosi]").click(function(){if($(this).attr("checked")==true){$(this).val(1);}else{if(<? echo $this->settings['fenmosi'];?>==0){$(this).val(1);}else{$(this).val(0);}}})
                 $("input[name=limosi]").click(function(){if($(this).attr("checked")==true){$(this).val(1);}else{if(<? echo $this->settings['limosi'];?>==0){$(this).val(1);}else{$(this).val(0);}}})
                 */
                function setValue(checkboxElem) {
                    if (checkboxElem.value == 1) {
                        if (checkboxElem.name == "yuanmosi") $("input[name=yuanmosi]").val(0);
                        else if (checkboxElem.name == "jiaomosi") $("input[name=jiaomosi]").val(0);
                        else if (checkboxElem.name == "fenmosi")  $("input[name=fenmosi]").val(0);
                        else if (checkboxElem.name == "limosi")   $("input[name=limosi]").val(0);
                    }
                    else {
                        if (checkboxElem.name == "yuanmosi") $("input[name=yuanmosi]").val(1);
                        else if (checkboxElem.name == "jiaomosi") $("input[name=jiaomosi]").val(1);
                        else if (checkboxElem.name == "fenmosi")  $("input[name=fenmosi]").val(1);
                        else if (checkboxElem.name == "limosi")   $("input[name=limosi]").val(1);
                    }
                }


            </script>
            <tr>
                <td>最低投注金额</td>
                <td>
					<input type="number" value="<?= $this->settings['min_bet'] ?>" class="textWid1" name="min_bet" />  为0则不限
                </td>
            </tr>
            <tr>
                <td>返点最大值</td>
                <td><input type="text" class="textWid1" value="<?= $this->settings['fanDianMax'] ?>" name="fanDianMax"/>%
                </td>
            </tr>
            <tr>
                <td>非推广注册用户默认返点比例(直客)</td>
                <td>
                    <input type="text" class="textWid1" value="<?= $this->settings['defaultFandian'] ?>"
                           name="defaultFandian"/>%
                </td>
            </tr>
            <tr>
                <td>上下级返点最小差值</td>
                <td><input type="text" class="textWid1" value="<?= $this->settings['fanDianDiff'] ?>"
                           name="fanDianDiff"/>%
                </td>
            </tr>
            <tr>
                <td>彩票退水</td>
                <td><input type="text" class="textWid1" value="<?= $this->settings['rebate'] ?>" name="rebate"/>% (会员投注不计输赢,依比例返回. 例: 退水1% 投注100元,固定返回1元不计输赢)
                    <br><span style="color:red">更新此数值,将重置所有会员退水</span>
                </td>
            </tr>
            <tr>
                <td>分红最大值</td>
                <td><input type="text" class="textWid1" value="<?= $this->settings['fenHongMax'] ?>" name="fenHongMax"/>%
                </td>
            </tr>
            <!--tr>
                <td>最大返点限制</td>
                <td>
                    元模式：<input type="text" class="textWid1" value="<?= $this->settings['betModeMaxFanDian0'] ?>"
                               name="betModeMaxFanDian0"/>%
                    　角模式：<input type="text" class="textWid1" value="<?= $this->settings['betModeMaxFanDian1'] ?>"
                                name="betModeMaxFanDian1"/>%
                    　分模式：<input type="text" class="textWid1" value="<?= $this->settings['betModeMaxFanDian2'] ?>"
                                name="betModeMaxFanDian2"/>%
                    厘模式：<input type="text" class="textWid1" value="<?= $this->settings['betModeMaxFanDian3'] ?>"
                               name="betModeMaxFanDian3"/>%
                </td>
            </tr-->
            <tr>
                <td>充值限制</td>
                <td>
                    最低金额：<input type="text" class="textWid1" value="<?= $this->settings['rechargeMin'] ?>"
                                name="rechargeMin"/>元&nbsp;&nbsp;
                    最高金额：<input type="text" class="textWid1" value="<?= $this->settings['rechargeMax'] ?>"
                                name="rechargeMax"/>元
                </td>
            </tr>
			<tr>
                <td>提现限制</td>
                <td>
                    消费比例：<input type="number" class="textWid1" value="<?= $this->settings['cashMinAmount'] ?>" name="cashMinAmount" min="0" max="1000"/>%
                    最低金额：<input type="text" class="textWid1" value="<?= $this->settings['cashMin'] ?>" name="cashMin"/>元
                    最高金额：<input type="text" class="textWid1" value="<?= $this->settings['cashMax'] ?>" name="cashMax"/>元
                    时间段：
                        从 <input type="time" value="<?= $this->settings['cashFromTime'] ?>" name="cashFromTime"/>
                        到 <input type="time" value="<?= $this->settings['cashToTime'] ?>" name="cashToTime"/>
                    <br>
                    (消费比例公式为:充值 * 百分比为需要投注金额,举例 充100  填入300%,投注须满300才能提款. 0则关闭)
                </td>
            </tr>
            <tr>
                <td>赠送活动</td>
                <td>首次设置银行账户送<input class="textWid1" type="text" value="<?= $this->settings['huoDongRegister'] ?>"
                                    name="huoDongRegister"/>元 &nbsp;&nbsp;
                                    <!--                                    
                                    每天签到每次送<input type="text" class="textWid1" value="<?= $this->settings['huoDongSign'] ?>" name="huoDongSign"/>元，如果为0则关闭活动-->
                </td>
            </tr>
            <tr>
                <td>充值赠送 活动</td>
                <td>
                    每次充值赠送本人 <input type="number" class="textWid1" value="<?= $this->settings['czzs'] ?>" name="czzs" min="0" max="100" />%，
                    上家送 <input type="number" class="textWid1" value="<?= $this->settings['czzsParent'] ?>" name="czzsParent" min="0" max="100" />%，
                    上上家送 <input type="number" class="textWid1" value="<?= $this->settings['czzsTop'] ?>" name="czzsTop" min="0" max="100" />%，
                    如果为0则关闭活动
                </td>
            </tr>
            <tr>
                <td>注册赠送 活动</td>
                <td>
                    新注册用户赠送本人：<input type="text" class="textWid1" value="<?= $this->settings['zczs'] ?>" name="zczs"/>元，如果为0则关闭活动
                </td>
            </tr>
            <tr>
                <td>充值佣金 活动</td>
                <td>
                    每天首次充值金额<input type="number" class="textWid1" value="<?= $this->settings['rechargeCommissionAmount'] ?>" name="rechargeCommissionAmount" min="0" />元以上，
                    自身送<input type="number" class="textWid1" value="<?= $this->settings['rechargeSelfCommission'] ?>" name="rechargeSelfCommission" min="0" />元佣金，
                    上家送<input type="number" class="textWid1" value="<?= $this->settings['rechargeCommission'] ?>" name="rechargeCommission" min="0" />元佣金，
                    上上家送<input type="number" class="textWid1" value="<?= $this->settings['rechargeCommission2'] ?>" name="rechargeCommission2" min="0" />元佣金，
                    如果为0则关闭活动
                </td>
            </tr>

            <tr>
                <td rowspan="2">消费佣金活动</td>
                <td>
                    <p>
                        每天消费达<input class="textWid1" type="text" value="<?= $this->settings['conCommissionBase'] ?>" name="conCommissionBase" />元时，
                        自身送<input class="textWid1" type="text" value="<?= $this->settings['conCommissionSelfAmount'] ?>" name="conCommissionSelfAmount"/>元佣金，
                        上家送<input class="textWid1" type="text" value="<?= $this->settings['conCommissionParentAmount'] ?>" name="conCommissionParentAmount"/>元佣金，
                        上上家送<input class="textWid1" type="text" value="<?= $this->settings['conCommissionParentAmount2'] ?>" name="conCommissionParentAmount2"/>元佣金，
                        如果为0则关闭活动
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        每天消费达<input class="textWid1" type="text" value="<?= $this->settings['conCommissionBase2'] ?>" name="conCommissionBase2"/>元时，
                        自身送<input class="textWid1" type="text" value="<?= $this->settings['conCommissionSelfAmount2'] ?>" name="conCommissionSelfAmount2"/>元佣金，
                        上家送<input class="textWid1" type="text" value="<?= $this->settings['conCommissionParentAmount3'] ?>" name="conCommissionParentAmount3"/>元佣金，
                        上上家送<input class="textWid1" type="text" value="<?= $this->settings['conCommissionParentAmount4'] ?>" name="conCommissionParentAmount4"/>元佣金，
                        如果为0则关闭活动
                    </p>
                </td>
            </tr>

            <tr>
                <td rowspan="2">亏损佣金活动</td>
                <td>
                    <p>
                        每天亏损达<input class="textWid1" type="text" value="<?= $this->settings['lossCommissionBase'] ?>" name="lossCommissionBase"/>元时，
                        自身送<input class="textWid1" type="text" value="<?= $this->settings['lossCommissionSelfAmount'] ?>" name="lossCommissionSelfAmount"/>元佣金，
                        上家送<input class="textWid1" type="text" value="<?= $this->settings['lossCommissionParentAmount'] ?>" name="lossCommissionParentAmount"/>元佣金，
                        上上家送<input class="textWid1" type="text" value="<?= $this->settings['lossCommissionParentAmount2'] ?>" name="lossCommissionParentAmount2"/>元佣金，
                        如果为0则关闭活动
                    </p>
                </td>
            </tr>
            <tr>
                <td>
                    <p>
                        每天亏损达<input class="textWid1" type="text" value="<?= $this->settings['lossCommissionBase2'] ?>" name="lossCommissionBase2"/>元时，
                        自身送<input class="textWid1" type="text" value="<?= $this->settings['lossCommissionSelfAmount2'] ?>" name="lossCommissionSelfAmount2"/>元佣金，
                        上家送<input class="textWid1" type="text" value="<?= $this->settings['lossCommissionParentAmount3'] ?>" name="lossCommissionParentAmount3"/>元佣金，
                        上上家送<input class="textWid1" type="text" value="<?= $this->settings['lossCommissionParentAmount4'] ?>" name="lossCommissionParentAmount4"/>元佣金，
                        如果为0则关闭活动
                    </p>
                </td>
            </tr>
            <!--
            <tr>
                <td>分红发放比例</td>
                <td>
                    <p>
                        返点比例为：
                        <input name="bonusScale_fandian1_min" class="textWid1" type="text"
                               value="<?= $this->settings['bonusScale_fandian1_min'] ?>"/>%&nbsp;~&nbsp;
                        <input name="bonusScale_fandian1_max" class="textWid1" type="text"
                               value="<?= $this->settings['bonusScale_fandian1_max'] ?>"/>%&nbsp;~&nbsp;
                        时，账户分红比例为：<input name="bonusScale1" class="textWid1" type="text"
                                         value="<?= $this->settings['bonusScale1'] ?>"/>%
                    </p>
                    <p>
                        返点比例为：
                        <input name="bonusScale_fandian2_min" class="textWid1" type="text"
                               value="<?= $this->settings['bonusScale_fandian2_min'] ?>"/>%&nbsp;~&nbsp;
                        <input name="bonusScale_fandian2_max" class="textWid1" type="text"
                               value="<?= $this->settings['bonusScale_fandian2_max'] ?>"/>%&nbsp;~&nbsp;
                        时，账户分红比例为：<input name="bonusScale2" class="textWid1" type="text"
                                         value="<?= $this->settings['bonusScale2'] ?>"/>%
                    </p>
                    <p>其他返点比例不分红</p>
                </td>
            </tr>
            -->
            <!--tr>
                <td>滚动公告</td>
                <td>
                    <textarea name="webGG" cols="56" rows="5"><?= $this->settings['webGG'] ?></textarea>
                </td>
            </tr-->
            <tr>
                <td>积分比例</td>
                <td>
                    <input type="text" class="textWid1" value="<?= $this->settings['scoreProp'] ?>" name="scoreProp"/>
                    每消费1元积的分数
                </td>
            </tr>
            <tr>
                <td>积分规则</td>
                <td>
                    <textarea name="scoreRule" cols="30" rows="3"><?= $this->settings['scoreRule'] ?></textarea>
                </td>
            </tr>
            <tr>
                <td>客服状态</td>
                <td>
                    <label><input type="radio" value="1"
                                  name="kefuStatus" <?= $this->iff($this->settings['kefuStatus'], 'checked="checked"') ?>/>开启</label>
                    <label><input type="radio" value="0"
                                  name="kefuStatus" <?= $this->iff(!$this->settings['kefuStatus'], 'checked="checked"') ?>/>关闭</label>

                </td>
            </tr>
            <tr>
                <td>客服链接</td>
                <td>
                    <textarea name="kefuGG" cols="56" rows="5"><?= $this->settings['kefuGG'] ?></textarea>
                </td>
            </tr>
			<tr>
				<td>提示</td>
				<td>
					以下设置进入下一年度才需要修改
				</td>
			</tr>
			<tr>
				<td>香港彩五行金</td>
				<td>
					<input name="lhcWxJin"  type="text" value="<?=$this->settings['lhcWxJin']?>">
                    &nbsp;&nbsp;香港彩五行木&nbsp;&nbsp;<input name="lhcWxMu"  type="text" value="<?=$this->settings['lhcWxMu']?>">
					&nbsp;&nbsp;香港彩五行水&nbsp;&nbsp;<input name="lhcWxShui"  type="text" value="<?=$this->settings['lhcWxShui']?>">
				</td>
			</tr>
			<tr>
				<td>香港彩五行土</td>
				<td>
					<input name="lhcWxTu"  type="text" value="<?=$this->settings['lhcWxTu']?>">
                    &nbsp;&nbsp;香港彩五行火&nbsp;&nbsp;<input name="lhcWxHuo"  type="text" value="<?=$this->settings['lhcWxHuo']?>">
                      &nbsp;&nbsp;本年生肖&nbsp;&nbsp;<input name="animalsYear"  type="text" value="<?=$this->settings['animalsYear']?>">
				</td>
			</tr>
            <tr>
                <td>排列五相差期数</td>
                <td><input type="number" class="textWid1" value="<?= $this->settings['pai3xDiffDay'] ?>" name="pai3xDiffDay"/> 每年(12-31 -> 01-01)跨年时需归0，过年后输入未开奖天数，例:-6天
                </td>
            </tr>
            <tr>
                <td>福彩3D相差期数</td>
                <td><input type="number" class="textWid1" value="<?= $this->settings['pai3dDiffDay'] ?>" name="pai3dDiffDay"/> 每年跨年时需归0，过年后输入未开奖天数，例:-7天
                </td>
            </tr>
            <tr>
                <td>北京PK10相差期数</td>
                <td><input type="number" class="textWid1" value="<?= $this->settings['bjpk10DiffDay'] ?>" name="bjpk10DiffDay"/> 过年后输入未开奖天数*179期，例:过年休7天，就是7*179 = 1253，请在左方累加-1253天
                </td>
            </tr>
            <tr>
                <td>北京快乐8相差期数</td>
                <td><input type="number" class="textWid1" value="<?= $this->settings['bjkl8DiffDay'] ?>" name="bjkl8DiffDay"/> 过年后输入未开奖天数*179期，例:过年休7天，就是7*179 = 1253，请在左方累加-1253天
                </td>
            </tr>            
            </tbody>
        </table>
        <footer>
            <div class="submit_link">
                <input type="submit" value="保存修改设置" title="保存设置" class="alt_btn">&nbsp;&nbsp;
                <input type="button" onclick="load('system/settings')" value="重置" title="重置原来的设置">
            </div>
        </footer>
    </form>
</article>


<script>
function clearData3Before() {
    var $this = $(this),
        date = $("#clearData3Date").val(),
        $items = $("input.clearData3Item:checked");
    if (! date) {
        throw("日期未填");
    } else if (! $items.length) {
        throw("至少勾选一个要删除的项目");
    }
    $this.removeData();
    $items.each(function() {
        $this.data(this.name, this.value);
    });
    $this.data("date", date);
}
</script>
