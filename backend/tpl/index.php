<!doctype html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8" />
<meta charset="utf-8"/>
<title>后台管理系统</title>
<link rel="stylesheet" href="/skin/admin/layout.css" type="text/css" />
<link rel="stylesheet" href="/skin/admin/fontawesome/css/all.min.css">
<link type="text/css" rel="stylesheet" href="/skin/js/jqueryui/skin/smoothness/jquery-ui-1.8.23.custom.css" />
<link type="text/css" rel="stylesheet" href="/skin/plugins/jQuery-Timepicker-Addon-v1.6.3/jquery-ui-timepicker-addon.min.css" />
<link type="text/css" rel="stylesheet" href="/skin/css/css-loader.css" />
<!--[if IE]>
	<link rel="stylesheet" href="/skin/admin/ie.css" type="text/css" />
	<script src="/skin/js/html5.js"></script>
<![endif]-->
<script src="/skin/js/jquery-1.8.0.min.js"></script>
<script src="/skin/admin/onload.js"></script>
<script src="/skin/admin/wjevent.js?v=1"></script>
<script src="/skin/admin/function.js?v=2"></script>
<script src="/skin/js/Array.ext.js"></script>
<script src="/skin/js/jqueryui/jquery-ui-1.8.23.custom.min.js"></script>
<script src="/skin/js/jqueryui/i18n/jquery.ui.datepicker-zh-CN.js"></script>
<script src="/skin/plugins/jQuery-Timepicker-Addon-v1.6.3/jquery-ui-timepicker-addon.js"></script>
<script src="/skin/plugins/jQuery-Timepicker-Addon-v1.6.3/i18n/jquery-ui-timepicker-zh-CN.js"></script>
<script type="text/javascript" src="/skin/js/jquery.messager.js"></script>
<script src="/skin/js/jquery.cookie.js"></script>
<script src="/skin/js/jquery.blockUI.js"></script>
<script src="/skin/js/util.js"></script>
<script type="text/javascript">
$(function(){
	$('.quick_search input[name=username]')
	.focus(function(){
		if(this.value=='查找会员') this.value='';
	})
	.blur(function(){
		if(this.value=='') this.value='查找会员';
	})
	.keypress(function(e){
		if(e.keyCode==13) $(this).closest('form').submit();
	});
});
function searchUserSubmit(err,data){
	if(err){
		error(err);
	}else{
		$('#main').html(data);
	}
}
var TIP=true;

/**add	znx**/
function sysAddBox(){
	load('Box/addbox');
}

function sysReloadBox(err, data){
	if(err){
		error(err);
	}else{
		success(data);
		load('Box/receivebox');
	}
}

function copy_code(copyText){
    if (window.clipboardData)
    {
        window.clipboardData.setData("Text", copyText)
    }
    else
    {
        var flashcopier = 'flashcopier';
        if(!document.getElementById(flashcopier))
        {
          var divholder = document.createElement('div');
          divholder.id = flashcopier;
          document.body.appendChild(divholder);
        }
        document.getElementById(flashcopier).innerHTML = '';
        var divinfo = '<embed src="../_clipboard.swf" FlashVars="clipboard='+encodeURIComponent(copyText)+'" width="0" height="0" type="application/x-shockwave-flash"></embed>';
        document.getElementById(flashcopier).innerHTML = divinfo;
    }
  alert('copy成功！');
}
</script>
<!-- ECharts单文档引入 -->
    <script src="/skin/js/echarts-all.js"></script>
</head>

<body>
	<header id="header">
		<hgroup>
			<h1 class="site_title">管理系统</h1>
			<h2 class="section_title">
				<a class="tjgk  load" href="business/betLog"><i class="icon fas fa-clipboard-list bet"></i> 普通投注</a>
				<a class="tjgk load" href="member/index"><i class="icon fas fa-clipboard-list user"></i> 用户列表</a>
				<a class="tjgk  load" href="#" onclick="rechargModal()" value="充值"><i class="icon fas fa-donate recharg"></i> 账号充值</a>
				<a class="tjgk  load" href="#" onclick="sysAddBox()"><i class="icon fas fa-envelope mail"></i> 发布消息</a>
				<a class="tjgk  load" method="post" target="ajax" call="clearDataSuccess" title="即将清空进程缓存，是否继续！" dataType="json" href="/clear/rmfile"><i class="icon fas fa-sync sync"></i> 清空缓存</a>
            </h2>
            <div class="btn_view_site"><a href="/user/logout">安全退出</a></div>
		</hgroup>
	</header>

	<section id="secondary_bar">
		<div class="user">
			<p>欢迎：<?=$this->user['username']?></p>
		</div>
		<div class="breadcrumbs_container">
			<article class="breadcrumbs"><a>当前位置：<strong>首页</strong></a> <div class="breadcrumb_divider"></div> <span id="position"><a class="current">统计概况</a></span></article>
		</div>
	</section>
	<aside id="sidebar" class="column">
		<!--form action="/member/listUser" class="quick_search" call="searchUserSubmit" dataType="html" target="ajax">
		  <input name="username" type="search" placeholder="查找会员" value="查找会员"/>
	    </form-->
		<hr/>
		<h3>业务流水<a class='showa'>＋</a></h3>
		<ul class="toggle" style="display:none;">
			<!--li><a class="bq yw_b_1" href="business/cash">提现请求</a></li-->
			<li><a class="bq yw_b_2" href="business/cashLog">提现记录</a></li>
			<li><a class="bq yw_b_3" href="business/rechargeLog">充值记录</a></li>
			<li><a class="bq yw_b_4" href="business/betLog">彩票投注</a></li>
			<li><a class="bq yw_b_4" href="business/ebetLog">电子投注</a></li>
			<li><a class="bq yw_b_4" href="business/ecardLog">棋牌投注</a></li>
			<li><a class="bq yw_b_7" href="business/coinLog">资金纪录</a></li>
		</ul>
		<h3>开奖数据<a class='showa'>＋</a></h3>
		<ul class="toggle" style="display:none;">
		<?php foreach($this->getRows("select id,title from {$this->prename}type where enable=1 and isDelete=0 order by title,sort") as $type){ ?>
			<li><a href="data/index/<?=$type['id']?>" class="k_b_1 bq"><?=$type['title']?></a></li>
		<?php } ?>
		</ul>
        <h3>时间管理<a class='showa'>＋</a></h3>
		<ul class="toggle" style="display:none;">
			<?php foreach($this->getRows("select id,title from {$this->prename}type where enable=1 and isDelete=0 order by title,sort") as $type){ ?>
			<li><a href="time/index/<?=$type['id']?>" class="k_b_1 bq"><?=$type['title']?></a></li>
			<?php } ?>
		</ul>
		<h3>数据统计<a class='showa'>＋</a></h3>
		<ul class="toggle" style="display:none;">
			<li><a href="countData/index" class="s_b_1 bq current">统计概况</a></li>
			<li><a href="countData/betDate" class="s_b_2 bq">个人综合统计</a></li>
			<li><a href="countData/betTDate" class="s_b_2 bq">团队综合统计</a></li>
			<li><a href="countData/betDate_Day" class="s_b_2 bq">用户日报表</a></li>
			<!--li><a href="kjdatas/tests" class="s_b_2 bq">开奖检测</a></li-->
			<!--li><a href="report/index" class="s_b_2 bq">每小时报表</a></li-->
			<!--li><a href="report/gameClass" class="s_b_2 bq">彩种报表 (旧)</a></li-->
			<li><a href="report/incomeOfType" class="s_b_2 bq">彩种报表</a></li>
			<li><a href="report/incomeOfecharts" class="s_b_2 bq">彩种报表</a></li>
			<li><a href="report/incomeOfUser" class="s_b_2 bq">用户投注排名</a></li>
			<!--li><a href="report/registrationRecharge" class="s_b_2 bq">注册充值报表</a></li-->
			<!--li><a href="report/usersOfecharts" class="s_b_2 bq">用户报表 (图)</a></li-->
			<li><a href="report/profitOfecharts" class="s_b_2 bq">盈亏报表</a></li>
			<li><a href="report/clientOfecharts" class="s_b_2 bq">载具分析表</a></li>
			<li><a href="report/coin" class="s_b_2 bq">出入帐汇总</a></li>
			<!--li><a href="report/betsRepl" class="s_b_2 bq">特异人士</a></li-->
		</ul>
		<h3>用户管理<a class='showa'>＋</a></h3>
		<ul class="toggle" style="display:none;">
			<li><a href="member/add" class="yh_b_1 bq">增加会员</a></li>
			<li><a href="member/index" class="yh_b_2 bq">用户列表</a></li>
            <li><a href="member/bank" class="yh_b_4 bq">银行信息</a></li>
            <li><a href="member/loginLog" class="yh_b_5 bq">登录日志</a></li>
            <!--li><a href="member/userCountSetting" class="yh_b_5 bq">用户限额设置</a></li-->

		</ul>
		<h3>管理人员<a class='showa'>＋</a></h3>
		<ul class="toggle" style="display:none;">
			<li><a href="manage/index" class="g_b_1 bq">管理员列表</a></li>
            <li><a href="manage/controlLog" class="g_b_3 bq">操作日志</a></li>
            <li><a href="manage/loginLog" class="g_b_4 bq">登录日志</a></li>
		</ul>
        <h3><span>佣金管理</span><a class='showa'>＋</a></h3>
		<ul class="toggle" style="display:none;">
        	<li><a href="commission/conCommissionList" class="bq jf_b_2">消费佣金发放</a></li>
        	<li><a href="commission/lossCommissionList" class="bq jf_b_2">亏损佣金发放</a></li>
		</ul>
		<h3><span>分红管理</span><a class='showa'>＋</a></h3>
		<ul class="toggle" style="display:none;">
        	<li><a href="bonus/bonuslist" class="bq jf_b_2">分红发放</a></li>
        	<li><a href="bonus/bonusLog" class="bq jf_b_2">分红发放记录</a></li>
        	<!--li><a href="Agent/betDates" class="bq jf_b_2">招商号统计</a></li-->
		</ul>
		<h3>系统设置<a class='showa'>＋</a></h3>
		<ul class="toggle" style="display:none;">
			<li><a href="system/settings" class="bq t_b_4">系统设置</a></li>
			<li><a href="system/notice" class="bq t_b_3">公告中心</a></li>
			<li><a href="system/bank" class="bq t_b_5">公司汇款设置</a></li>
			<li><a href="system/thirdpay" class="bq t_b_5">三方支付设置</a></li>
			<li><a href="system/promotion" class="bq t_b_5">优惠活动管理</a></li>
			<li><a href="system/pool" class="bq t_b_5">獎池管理</a></li>
			<!--li><a href="system/sysbanklist" class="bq t_b_5">银行管理</a></li-->
			<li><a href="system/type" class="bq t_b_8">彩种管理</a></li>
			<li><a href="system/played_credit" class="bq t_b_9">信用玩法管理</a></li>
			<li><a href="system/played_official" class="bq t_b_9">官方玩法管理</a></li>
			<li><a href="member/level" class="yh_b_6 bq">等级设置</a></li>
			<!--li><a href="database/backup" class="g_b_4 bq">数据备份</a></li-->
			<!--li><a href="system/datalog" class="g_b_4 bq">Front-Log</a></li-->
			<!--li><a href="system/datalog_mobile" class="g_b_4 bq">Mobile-Log</a></li-->
			<!--li><a href="pays/index" class="bq jf_b_2">接口管理</a></li-->
		</ul>
		<h3><span>站内信管理</span><a class='showa'>＋</a></h3>
		<?php
		    $sql="select count(id) from {$this->prename}message_receiver where to_uid=? and is_readed=0 and is_deleted=0";
			$num=$this->getValue($sql,0);
		?>
		<ul class="toggle"  style="display:none;">
		    <li><a href="#" onclick="sysAddBox()" class="bq jf_b_2">发消息</a></li>
		    <!--li><a href="Box/receivebox" class="bq jf_b_2">收件箱</a>(<strong style="color:red"> <?=$num?> </strong>)</li-->
        	<!--li><a href="Box/sendbox" class="bq jf_b_2">发件箱</a></li-->
			<li><a href="Box/all" class="bq jf_b_1">发信记录</a></li>
		</ul>
		<h3><span>积分兑换管理</span><a class='showa'>＋</a></h3>
		<ul class="toggle"  style="display:none;">
        	<li><a href="Score/settings" class="bq jf_b_2">兑换管理</a></li>
			<li><a href="Score/pointList" class="bq jf_b_1">兑换记录</a></li>
		</ul>
		<!--<h3><span>电子银行管理</span><a>＋</a></h3>
		<ul class="toggle"  style="display:none;">
        	<li><a href="dzyh/dzyhsettings" class="bq jf_b_2">电子银行配置</a></li>
			<li><a href="dzyh/ckpointList" class="bq jf_b_1">存款管理</a></li>
			<li><a href="dzyh/tkpointList" class="bq jf_b_1">取款管理</a></li>
		</ul>
		<h3><span>夺宝奇兵管理</span><a>＋</a></h3>
		<ul class="toggle"  style="display:none;">
        	<li><a href="dbqb/dbqbsettings" class="bq jf_b_2">夺宝配置</a></li>
			<li><a href="dbqb/pointList" class="bq jf_b_1">夺宝记录</a></li>
		</ul>-->
		<h3><span>大转盘管理</span><a class='showa'>＋</a></h3>
		<ul class="toggle"  style="display:none;">
        	<li><a href="Dzp/dzpsettings" class="bq jf_b_2">大转盘配置</a></li>
			<li><a href="Dzp/pointList" class="bq jf_b_1">中奖记录</a></li>
		</ul>
        <h3><span>系统统计</span><a class='showa'>＋</a></h3>
        <ul class="toggle"  style="display:none;">
            <li><a href="Davidsystem/all" class="s_b_2 bq">全局统计</a></li>
            <li><a href="Davidsystem/history" class="s_b_2 bq">历史统计</a></li>
            <!--li><a href="Davidsystem/proxy" class="s_b_2 bq">代理统计</a></li-->
            <li><a href="Davidsystem/win_rank" class="s_b_2 bq">盈利排行</a></li>
            <li><a href="Davidsystem/lose_rank" class="s_b_2 bq">亏损排行</a></li>
        </ul>

		<ul class="toggle"></ul>

		<footer>
			<hr />
			<p><strong>Copyright &copy; 管理系统</strong></p>
		</footer>
	</aside><!-- end of sidebar -->
	<div class="right-box" style="position: absolute;    left: 120px;    top: 90px;    display: inline-block;    width: 90%;">
	<section id="message-tip"></section>
	<section id="main" class="column"></section>
	<div style="clear: both"></div>
	</div>
</body>
</html>
