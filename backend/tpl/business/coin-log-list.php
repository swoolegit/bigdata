<?php
$this->getTypes();
$this->getPlayeds();

$para = $_GET;

$hidedata=" and u.forTest=0";
if(isset($_GET['sethide']) && $_GET['sethide']=="1")
{
	$hidedata=" and u.forTest=1";
}

$where_cond = array();
$binds = array();

// 时间限制
if (!empty($para['fromTime']) && !empty($para['toTime'])) {
    $where_cond[] = "l.actionTime BETWEEN :fromTime AND :toTime";
    $binds['fromTime'] = strtotime($para['fromTime']);
    $binds['toTime'] = strtotime($para['toTime']);
} elseif (! empty($para['fromTime'])) {
    $where_cond[] = "l.actionTime >= :fromTime";
    $binds['fromTime'] = strtotime($para['fromTime']);
} elseif (! empty($para['toTime'])) {
    $where_cond[] = "l.actionTime < :toTime";
    $binds['toTime'] = strtotime($para['toTime']);
} else {
    $where_cond[] = "l.actionTime > :today";
    $binds['today'] = strtotime('00:00');
}

// 帐变类型限制
$para['liqType'] = isset($para['liqType']) ? intval($para['liqType']) : 0;
if ($para['liqType']) {
    if ($para['liqType'] == 2) {
        $where_cond[] = "l.liqType IN (2, 3)";
    } elseif($para['liqType'] ==99 ) {
        $where_cond[] = "l.liqType IN (50,51,52,53,56,57,120)";
    } else {
        $where_cond[] = "l.liqType =:liqType";
        $binds['liqType'] = $para['liqType'];
    }
}

// 彩种限制
$para['type'] = isset($para['type']) ? intval($para['type']) : 0;
if ($para['type']) {
    $where_cond[] = "l.type =:type";
    $binds['type'] = $para['type'];
}

// 用户限制
if (isset($para['username']) && $para['username']!=='') {
    if (!preg_match('/^\w{2,32}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    $where_cond[] = "u.username = :like_username";
    $binds['like_username'] = "{$para['username']}";
}

//$where_cond[] = "l.liqType NOT IN (4, 11, 104)";    // Deprecated
$where_cond = $where_cond ? 'WHERE ' . implode(' AND ', $where_cond) : '';

$table="coin_log";
$fromTime=$para['fromTime'];
$toTime=$para['toTime'];
if(!empty($fromTime) || !empty($toTime))
{
    $time=time()-(7*86400);
    if(strtotime($fromTime) <= $time || strtotime($toTime) <= $time)
    {
        $table="coin_log_repl";
    }
}

$sql =
    "SELECT
        l.*,
        u.username,
        u.fanDian,
        u.fenHong
    FROM {$this->prename}{$table} AS l
        JOIN {$this->prename}members AS u ON u.uid = l.uid {$hidedata}
    {$where_cond}
    ORDER BY l.id DESC";
$data = $this->getPage($sql, $this->page, $this->pageSize, $binds);
$mname = array(
    '2.000' => '元',
    '0.200' => '角',
    '0.020' => '分',
    '0.002' => '厘'
);
?>

<table class="table-sorter text-center" cellspacing="0">
    <thead>
        <tr>
            <th>用户名</th>
            <th>帐变类型</th>
            <th>单号</th>
            <!--
            <th>游戏</th>
            <th>玩法</th>
            <th>期号</th>
            <th>模式</th>
            <th>返点</th>
            <th>工资</th>
            <th>分红</th>
            -->
            <th>资金</th>
            <th>余额</th>
            <th>时间</th>
        </tr>
    </thead>
    <tbody id="nav01">
    <?php if ($data['data']) foreach ($data['data'] as $var) { ?>
        <tr>
            <td><?= $var['username'] ?></td>
            <td><?= $var['info'] ?></td>
            <?php
            if ($var['extfield0'] && in_array($var['liqType'], array(2,5,6,7,9,101))) {
            ?>
                <td>
                    <a href="business/betInfo/<?= $var['extfield0'] ?>" target="modal" width="510" title="投注信息"
                       button="关闭:defaultCloseModal"><?= $this->ifs($var['extfield0'], '--') ?></a>
                </td>
            <?php } elseif (in_array($var['liqType'], array(1, 52))) { ?>
                <td>
                    <a href="business/rechargeInfo/<?= $var['extfield0'] ?>" target="modal" width="500" title="充值信息"
                       button="关闭:defaultCloseModal"><?= $var['extfield1'] ?></a>
                </td>
            <?php } elseif (in_array($var['liqType'], array(8, 107))) { ?>
                <td>
                    <a href="business/cashInfo/<?= $var['extfield0'] ?>" target="modal" width="500" title="提现信息"
                       button="关闭:defaultCloseModal"><?= $var['extfield0'] ?></a>
                </td>
            <?php } else { ?>
                <td>--</td>

            <?php } ?>
            <td><?= $var['coin'] ?></td>
            <td><?= $var['userCoin'] ?></td>
            <td><?= date('m-d H:i:s', $var['actionTime']) ?></td>
        </tr>
    <?php } else { ?>
        <tr>
            <td colspan="13">暂时没有帐变记录</td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<footer>
    <?php
    $rel = get_class($this) . '/coinLogList-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $data['total'], $rel, 'betLogSearchPageAction');
    ?>
</footer>
