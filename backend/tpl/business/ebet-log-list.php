<?php
$this->getETypes();
//$this->getPlayeds();
//print_r($this->etypes);
// 帐号限制
if ($_REQUEST['username']) {
    $_REQUEST['username'] = wjStrFilter($_REQUEST['username']);
    if (!preg_match('/^\w{2,32}$/', $_REQUEST['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    $userWhere = "and b.username = '{$_REQUEST['username']}'";
}

// $hidedata=" and b.forTest=0";
// if(isset($_GET['sethide']) && $_GET['sethide']=="1")
// {
// 	$hidedata=" and b.forTest=1";
// }

// 遊戲限制
if ($_REQUEST['type'] = intval($_REQUEST['type'])) {
    $typeWhere = " and b.gameType={$_REQUEST['type']}";
}

// 时间限制
if ($_REQUEST['fromTime'] && $_REQUEST['toTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $toTime = strtotime($_REQUEST['toTime']);
    $timeWhere = "and actionTime between {$fromTime} and {$toTime}";
} elseif ($_REQUEST['fromTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $timeWhere = "and actionTime >= {$fromTime}";
} elseif ($_REQUEST['toTime']) {
    $toTime = strtotime($_REQUEST['toTime']);
    $timeWhere = "and actionTime < {$toTime}";
} else {
    $timeWhere = ' and actionTime >' . strtotime('00:00');;
}

$table="egame_bet";
if(!empty($fromTime) || !empty($toTime))
{
    $time=time()-(7*86400);
    if($fromTime <= $time || $toTime <= $time)
    {
        $table="egame_bet";
    }
}

// if ($_REQUEST['id']) {
//     $_REQUEST['id'] = wjStrFilter($_REQUEST['id']);
//     if (!preg_match('/^\w{4,16}$/', $_REQUEST['id'])) {
// 		throw new Exception('单号包含非法字符,请重新输入');
// 	}
//     $sql = "SELECT * FROM {$this->prename}{$table} AS b WHERE b.wjorderId ='{$_REQUEST['id']}'";
//     $totalSql = "SELECT SUM(actionAmount) AS TOTAL,SUM(bonus) AS zjTOTAL FROM {$this->prename}{$table} AS b WHERE b.wjorderId ='{$_REQUEST['id']}' AND b.isDelete =0 AND b.lotteryNo !=''";
// } else {
// 	$sql = "SELECT * FROM {$this->prename}{$table} AS b WHERE 1 {$timeWhere} {$actionNoWhere} {$typeWhere} {$betTypeWhere} {$userWhere}  ORDER BY b.id DESC";
// 	$totalSql = "SELECT SUM(actionAmount) AS TOTAL,SUM(bonus) AS zjTOTAL FROM {$this->prename}{$table} AS b WHERE 1 {$timeWhere} {$actionNoWhere} {$typeWhere} {$betTypeWhere} {$userWhere}  AND b.isDelete =0 AND b.lotteryNo !='' ORDER BY b.id DESC";
// }

$sql = "SELECT * FROM {$this->prename}{$table} AS b WHERE 1 {$timeWhere} {$typeWhere} {$userWhere}  ORDER BY b.id DESC";
$totalSql = "SELECT 
                    (select sum(amount) from {$this->prename}{$table} where 1 {$timeWhere} {$typeWhere} {$userWhere} and status = 1) AS TOTAL,
                    (select sum(amount) from {$this->prename}{$table} where 1 {$timeWhere} {$typeWhere} {$userWhere} and status = 0) AS zjTOTAL 
                    ";
$time1=time();
$data = $this->getPage($sql, $this->page, $this->pageSize);
$time2=time();

$mname = array(
    '2.000' => '元',
    '0.200' => '角',
    '0.020' => '分',
    '0.002' => '厘'
);
$page_total=0;
$page_zjtotal=0;
$_status=[
    0=>"结算",
    1=>"投注",
    2=>"失败",
    3=>"退款",
];
?>

    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
            <th>局号</th>
            <th>用户名</th>
            <th>更新日期</th>
            <th>游戏</th>
            <th>状态</th>
            <th>金额</th>
            <th>mtcode</th>
        </tr>
        </thead>
        <tbody id="nav01">
        <?php if ($data['data']) foreach ($data['data'] as $var) { 
            if( $var['status']==1)
            {
                $page_total+=$var['amount'];
            }
            if( $var['status']==0)
            {
                $page_zjtotal+=$var['amount'];
            }
            ?>
            <tr >
                <td><?= $var['roundId'] ?></td>
                <td><?= $var['accountId'] ?></td>
                <td><?= $var['eventTime'] ?></td>
                <td><?= $this->etypes[$var['gameType']]['cname'] ?></td>
                <td><?= $_status[$var['status']] ?></td>
                <td><?= $var['amount'] ?></td>
                <td><?= $var['mtcode'] ?></td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="15" align="center">暂时没有投注记录。</td>
            </tr>
        <?php } ?>
			<tr>
				<td colspan="15" align="center" >
					<?php
					$totalData = $this->getRow($totalSql);
                    $total = (float) $totalData['TOTAL'];
                    $zjtotal = (float) $totalData['zjTOTAL'];
                    $page_winlost=$page_zjtotal - $page_total;
                    $winlost=$zjtotal - $total;
					if ($toTime == '' && $fromTime == '') {
                        echo "本页有效投注总额：<font color=red>". $this->nformat($page_total,2) . '</font> 元 , 中奖金额 : <font color=red>'.$this->nformat($page_zjtotal,2).'</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($page_winlost,2).'</font> 元<br>';
						echo '有效投注总额：<font color=red>' . $this->nformat($total,2) . '</font> 元 , 中奖金额 : <font color=red>'.$this->nformat($zjtotal,2).'</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($winlost,2).'</font> 元';
					} else {
                        echo ($fromTime ? date("Y-m-d H:i:s",$fromTime) : '以往') . ' ~ ' . ($toTime ? date("Y-m-d H:i:s",$toTime) : '迄今') ;
                        echo "<br>本页有效投注总额：<font color=red>". $this->nformat($page_total,2) . '</font> 元 , 中奖金额 : <font color=red>'.$this->nformat($page_zjtotal,2).'</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($page_winlost,2).'</font> 元';
                        echo '<br>有效投注总额：<font color=red>' . $this->nformat($total,2) . '</font> 元 , 中奖金额 : <font color=red>'.$this->nformat($zjtotal,2).'</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($winlost,2).'</font> 元';
					}
					?>
				</td>
			</tr>
        </tbody>
    </table>
    <footer>
        <?php

        $rel = get_class($this) . '/ebetLog_list-{page}?' . http_build_query($_GET, '', '&');
        $this->display('inc/page.php', 0, $data['total'], $rel, 'betLogSearchPageAction');
        ?>
    </footer>