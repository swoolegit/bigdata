<?php
$username="";
if (isset($_GET['username']) && $_GET['username']!=='')
{
	$username=$_GET['username'];
}
$fromTime=strtotime('00:00:00');
if(isset($_GET['fromTime']))
{
	$fromTime=strtotime($_GET['fromTime']."00:00:00");
}
$toTime=strtotime('23:59:59');
if(isset($_GET['toTime']))
{
	$toTime=strtotime($_GET['toTime']."23:59:59");
}
$types=$this->getTypes();
?>
<article class="module width_full">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <header>
        <h3 class="tabs_involved">普通投注
            <div class="submit_link wz">
                <form id="query-form" action="business/betLog_list" target="ajax" call="defaultList" dataType="html">
                    期号<input type="text" class="alt_btn" name="actionNo" style="width:90px;" value="<?= $_REQUEST['actionNo'] ?>" />
                    单号<input type="text" class="alt_btn" name="id" style="width:90px;" />
                    会员<input type="text" class="alt_btn" name="username" autocomplete="off" id="username" style="width:90px;" value="<?=$username?>" />
                    <div class="auto-screening auto-hidden mbox" id="autoScreening"></div>
                    时间从 <input type="text" class="alt_btn" name="fromTime" value="<?=date("Y-m-d H:i:s",$fromTime)?>" />
                    到 <input type="text" name="toTime" class="alt_btn" value="<?=date("Y-m-d H:i:s",$toTime)?>" />
                    <select style="width:100px;" name="type">
                        <option value="">全部彩种</option>
                        <?php
                        if ($types) foreach ($this->types as $var) {
                            if ($var['enable'] && !$var['isDelete']) {
                        ?>
                            <option value="<?= $var['id'] ?>" title="<?= $var['title'] ?>"><?= $this->ifs($var['shortName'], $var['title']) ?></option>
                        <?php
                            }
                        }
                        ?>
                    </select>
                    <input type="submit" value="查找" class="alt_btn">
                    <input type="reset" value="重置条件">
                </form>
            </div>
        </h3>
    </header>
    <div class="tab_content">
        <?php $this->display('business/bet-log-list.php'); ?>
    </div>
</article>
<script type="text/javascript">
$(function() {
    ghhs("nav01", "tr");

    var f = document.getElementById("query-form"),
        $fromTime = $(f.fromTime);
        $toTime = $(f.toTime);

    $fromTime.datetimepicker({
        timeFormat: "HH:mm:ss",
        controlType: "select",
        oneLine: true,
        onSelect: function(selectedDateTime) {
            $toTime.datetimepicker("option", "minDate", selectedDateTime.split(" ")[0]);
        }
    });
    $toTime.datetimepicker({
        timeFormat: "HH:mm:ss",
        controlType: "select",
        oneLine: true,
        hour: 23,
        minute: 59,
        second: 59,
        onSelect: function(selectedDateTime) {
            $fromTime.datetimepicker("option", "maxDate",  selectedDateTime.split(" ")[0]);
        }
    });
});
</script>