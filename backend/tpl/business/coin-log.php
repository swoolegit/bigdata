<?php
//$this->getTypes();

$liqTypeName = array(
			1   => '充值',
			2   => '返点',
			3   => '代理分红',
			4  => '人工扣减',
			55  => '注册奖励',
			//9   => '管理员充值',
			54  => '充值奖励',
			//106 => '提现冻结',
			12  => '上级转款',
			8   => '提现失败返还',
			107 => '提现扣除',
            125 => '重新结算',
            9 => '退水',

			101 => '投注扣款',
			//108 => '开奖扣除',
			6   => '中奖奖金',
			7   => '撤单返款',
			//102 => '追号投注',
			5   => '追号撤单',
			//11  => '合买收单',
            //255 => '未开奖返还',
            51  => '绑定银行奖励',
			52  => '充值佣金',
			53  => '消费佣金',
			56  => '亏损佣金',
			13  => '转款给下级',
			//20  => '日工资',
            //50  => '签到赠送',
            57  => '活动赠送',
            120 => '幸运大转盘',
            88 => '点数转入平台',
            89 => '点数转出平台',
			//121 => '积分兑换',
);
$username="";
if (isset($_GET['username']) && $_GET['username']!=='')
{
	$username=$_GET['username'];
}
$fromTime=strtotime('00:00:00');
if(isset($_GET['fromTime']))
{
	$fromTime=strtotime($_GET['fromTime']."00:00:00");
}
$toTime=strtotime('23:59:59');
if(isset($_GET['toTime']))
{
	$toTime=strtotime($_GET['toTime']."23:59:59");
}
?>

<div style="margin-left: 30px;margin-top: 10px">
    返点计算: 下级投注额*(自身返点-下级返点) , 每次开奖计算 <br>
    退水计算: 投注额*自身退水 , 每次开奖计算 <br>
	分红计算: 团队亏损金额*(自身分红-下级分红) ,人工在后台派发  <br>
</div>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved">帐变明细
            <form id="query-form" class="submit_link wz" action="business/coinLogList" target="ajax" dataType="html" call="coinLogList">
                用户名：<input type="text" name="username" style="width:90px;" autocomplete="off" placeholder="用户名" id="username" value="<?=$username?>" />
                <div class="auto-screening auto-hidden mbox" id="autoScreening"></div>
                帐变类型：<select style="width:100px" name="liqType">
                    <option value="">所有帐变类型</option>
                    <?php
                    foreach ($liqTypeName as $k => $v) {
                        echo '<option value="' . $k . '">' . $v . '</option>';
                    }
                    ?>
                </select>
                时间：
                    从 <input type="text" class="text-center" name="fromTime" autocomplete="off" value="<?=date("Y-m-d H:i:s",$fromTime)?>" />
                    到 <input type="text" class="text-center" name="toTime" autocomplete="off" value="<?=date("Y-m-d H:i:s",$toTime)?>"/>
                <input type="submit" value="查找" class="alt_btn">
                <input type="reset" value="重置条件">
            </form>
        </h3>
    </header>

    <div class="tab_content">
        <?php $this->display('business/coin-log-list.php'); ?>
    </div>
</article>


<script type="text/javascript">
$(function () {
    var f = document.getElementById("query-form"),
        $fromTime = $(f.fromTime),
        $toTime = $(f.toTime);
    $fromTime.datetimepicker({
        timeFormat: "HH:mm:ss",
        controlType: "select",
        oneLine: true,
        onSelect: function(selectedDateTime) {
            $toTime.datetimepicker("option", "minDate", selectedDateTime.split(" ")[0]);
        }
    });
    $toTime.datetimepicker({
        timeFormat: "HH:mm:ss",
        controlType: "select",
        oneLine: true,
        hour: 23,
        minute: 59,
        second: 59,
        onSelect: function(selectedDateTime) {
            $fromTime.datetimepicker("option", "maxDate",  selectedDateTime.split(" ")[0]);
        }
    });
});

function coinLogList(err, data) {
    if (err) {
        alert(err);
    } else {
        $('.tab_content').html(data);
    }
}
</script>