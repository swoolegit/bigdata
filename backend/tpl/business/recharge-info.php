<?php
//$sql ="select r.*, mb.username accountName, mb.account account, b.name bankName from {$this->prename}member_recharge r,{$this->prename}members u,{$this->prename}member_bank mb, {$this->prename}bank_list b where r.uid=u.uid and mb.id=r.mBankId and mb.bankId=b.id and b.isDelete=0 and r.id={$args[0]}";
$sql = "select r.* from {$this->prename}member_recharge r where r.id={$args[0]}";
$rechargeInfo = $this->getRow($sql, $args[0]);
// if ($rechargeInfo['mBankId']) {
//     $sql = "select mb.username accountName, mb.account account, b.name bankName from {$this->prename}members u,{$this->prename}member_bank mb, {$this->prename}bank_list b where b.isDelete=0 and u.uid={$rechargeInfo['uid']} and mb.id={$rechargeInfo['mBankId']} and mb.bankId=b.id";
//     $bankInfo = $this->getRow($sql);

//     $sql = "select b.* from {$this->prename}bank_list b where b.isDelete=0 and b.id={$rechargeInfo['bankId']}";
//     $rechargeBank = $this->getRow($sql);
// }

//$memo = json_decode($rechargeInfo['memo'] ? $rechargeInfo['memo'] : '');

$sql="select id,bankselect from {$this->prename}params_atr";
$atr= $this->getRows($sql);
$atr_array=[];
foreach($atr as $k=>$v)
{
    $atr_array[$v['id']]=$v['bankselect'];
}
$sql="select id,name from {$this->prename}params_thirdpay";
$thirdpay= $this->getRows($sql);
$thirdpay_array=[];
foreach($thirdpay as $k=>$v)
{
    $thirdpay_array[$v['id']]=$v['name'];
}

//$this->ifs($bankInfo['bankName'], '--')
?>
<div class="recharge-modal popupModal">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <table width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td class="title">用户</td>
            <td><?= $rechargeInfo['username'] ?></td>
        </tr>
        <tr>
            <td class="title">充值金额</td>
            <td><?= $rechargeInfo['amount'] ?>元</td>
        </tr>
        <tr>
            <td class="title">充值前资金</td>
            <td><?= number_format($rechargeInfo['coin'], 2) ?>元</td>
        </tr>
        <tr>
            <td class="title">充值方式</td>
            <td>
            <?php
                    switch($rechargeInfo['rechargeModel'])
                    {
                        case 0:
                            echo "公司汇款";
                        break;
                        case 1:
                            echo "三方充值";
                        break;
                        case 2:
                            echo "管理员充值";
                        break;
                    }
                    ?>                
            </td>
        </tr>
        <tr>
            <td class="title">充值渠道</td>
            <td>
            <?php
                    switch($var['rechargeModel'])
                    {
                        case 0:
                            echo $atr_array[$var['rechargeComID']];
                        break;
                        case 1:
                            echo $thirdpay_array[$var['rechargeComID']];
                        break;
                        case 2:
                            echo "管理员充值";
                        break;
                    }
            ?>               
            </td>
        </tr>        
        <tr>
            <td class="title">MEMO</td>
            <td><?= $rechargeInfo['memo'] ?></td>
        </tr>
        <tr>
            <td class="title">充值时间</td>
            <td><?= date("Y-m-d H:i:s", $rechargeInfo['actionTime']) ?></td>
        </tr>
        <tr>
            <td class="title">到帐时间</td>
            <td><?= date("Y-m-d H:i:s", $rechargeInfo['rechargeTime']) ?></td>
        </tr>

    </table>
</div>
