<?php
$para = $_GET;

// $hidedata=" and u.forTest=0";
// if(isset($_GET['sethide']) && $_GET['sethide']=="1")
// {
// 	$hidedata=" and u.forTest=1";
// }
$where_cond = array();
$binds = array();

// 时间限制
switch ((isset($para['timeType']) ? $para['timeType'] : '')) {
    case 'rechargeTime':        // 到帐时间
        $timeType = 'rechargeTime';
        break;
    default:   // 申请时间
        $timeType = 'actionTime';
        break;
}
if (!empty($para['fromTime']) && !empty($para['toTime'])) {
    $where_cond[] = "c.{$timeType} BETWEEN :fromTime AND :toTime";
    $binds['fromTime'] = strtotime($para['fromTime']);
    $binds['toTime'] = strtotime($para['toTime']);
} elseif (! empty($para['fromTime'])) {
    $where_cond[] = "c.{$timeType} >= :fromTime";
    $binds['fromTime'] = strtotime($para['fromTime']);
} elseif (! empty($para['toTime'])) {
    $where_cond[] = "c.{$timeType} < :toTime";
    $binds['toTime'] = strtotime($para['toTime']);
}

// 状态
if (isset($para['state'])) {
    switch ($para['state']) {
        case '0':
        case '1':
        case '2':
            $where_cond[] = "c.state =:state";
            $binds['state'] = $para['state'];
            break;
    }
}

// 渠道
if (isset($para['rechargeModel'])) {
    switch ($para['rechargeModel']) {
        case '0':
        case '1':
        case '2':
            $where_cond[] = "c.rechargeModel =:rechargeModel";
            $binds['rechargeModel'] = $para['rechargeModel'];
            break;
    }
}

// 用户限制
if (isset($para['username']) && $para['username']!=='') {
    $para['username'] = wjStrFilter($para['username']);
    if (!preg_match('/^\w{2,32}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    $where_cond[] = "c.username LIKE :like_username";
    $binds['like_username'] = "%{$para['username']}%";
}

// 充值编号限制
if (isset($para['rechargeId']) && $para['rechargeId']!=='') {
    $para['rechargeId'] = wjStrFilter($para['rechargeId'], 0, 0);
    if (!ctype_digit($para['rechargeId'])) throw new Exception('充值编号包含非法字符');
    $where_cond[] = "c.rechargeId =:rechargeId";
    $binds['rechargeId'] = $para['rechargeId'];
}

// 银行代号
// $bank_id = !empty($para['bank_id']) ? intval($para['bank_id']) : 0;
// if ($bank_id) {
//     $where_cond[] = "c.bankId =:bankId";
//     $binds['bankId'] = $bank_id;
// }

$where_cond[] = "c.isDelete =0";
$where_cond = $where_cond ? 'WHERE ' . implode(' AND ', $where_cond) : '';

$sql =
    "SELECT
        c.id,
        c.uid,
        c.rechargeModel,
        c.rechargeComID,
        c.state,
        c.rechargeId,
        c.amount,
        c.rechargeAmount,
        c.coin,
        c.info,
        c.actionTime,
        c.rechargeTime,
        c.memo,
        c.username
    FROM {$this->prename}member_recharge AS c
    {$where_cond}
    ORDER BY c.id DESC";
$data = $this->getPage($sql, $this->page, $this->pageSize, $binds);

$totalSql =
    "SELECT SUM(c.amount) AS TOTAL
    FROM {$this->prename}member_recharge AS c 
    {$where_cond} and c.state=1";
// 银行资料
// $sql = "SELECT * FROM {$this->prename}bank_list WHERE isDelete =0";
// $bank_data = $this->getRows($sql);
// $banks = array();
// foreach ($bank_data as $v) $banks[$v['id']] = $v;

// $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b');

$sql="select id,bankselect from {$this->prename}params_atr";
$atr= $this->getRows($sql);
$atr_array=[];
foreach($atr as $k=>$v)
{
    $atr_array[$v['id']]=$v['bankselect'];
}
$sql="select id,name from {$this->prename}params_thirdpay";
$thirdpay= $this->getRows($sql);
$thirdpay_array=[];
foreach($thirdpay as $k=>$v)
{
    $thirdpay_array[$v['id']]=$v['name'];
}

?>

<table class="table-sorter text-center" cellspacing="0">
    <thead>
    <tr>
        <th>用户名</th>
        <th>充值金额</th>
        <th>充值前资金</th>
        <th>充值编号</th>
        <th>充值方式</th>
        <th>充值渠道</th>
        <th>状态</th>
        <th>备注</th>
        <th>申请时间</th>
        <th>到帐时间</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody id="nav01">
    <?php
    if ($data['data']) {
        $amount = 0;
        foreach ($data['data'] as $var) {
            if ($var['state']==1) $amount += $var['amount'];
            //$var['parents'] = trim($var['parents'], ',');

            // if (!isset($_SESSION['randColor'][$bank_name])) {
            //     $_SESSION['randColor'][$bank_name] = $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)];;
            // }
            // $bank_name = "<font color='#{$_SESSION['randColor'][$bank_name]}'>{$bank_name}</font>";
            $info = $var['info'];
            if (!empty($var['memo']) && strlen($var['memo']) <= 20) {
                $info .= '-' . $var['memo'];
            }
            ?>
            <tr>
                <td><?= $var['username'] ?></td>
                <td><?= $var['amount'] ?></td>
                <td><?= $this->iff($var['state'], $var['coin'], '--') ?></td>
                <td><?= $var['rechargeId'] ?></td>
                <td><?php
                    switch($var['rechargeModel'])
                    {
                        case 0:
                            echo "公司汇款";
                        break;
                        case 1:
                            echo "三方充值";
                        break;
                        case 2:
                            echo "管理员充值";
                        break;
                    }
                    ?>
                </td>
                <td><?php
                    switch($var['rechargeModel'])
                    {
                        case 0:
                            echo $atr_array[$var['rechargeComID']];
                        break;
                        case 1:
                            echo $thirdpay_array[$var['rechargeComID']];
                        break;
                        case 2:
                            echo "管理员充值";
                        break;
                    }
                 ?></td>
                <td><?php 
                    switch($var['state'])
                    {
                        case 0:
                            echo '<font color=brown>正在申请</font>';
                        break;
                        case 1:
                            echo '<font color=green>充值成功</font>';
                        break;
                        case 2:
                            echo '<font color=gray>失败</font>';
                        break;
                    }
                ?></td>
                <td><?= $info ?></td>
                <td><?= date('Y-m-d H:i:s', $var['actionTime']) ?></td>
                <td><?= $var['rechargeTime'] ? date('Y-m-d H:i:s', $var['rechargeTime']) : '' ?></td>
                <td>
                    <?php if (!$var['state']) { ?>
                        <a href="/business/rechargeActionModal/<?= $var['id'] ?>" target="modal" width="420"
                           title="到帐处理" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">到帐处理</a>
                        <a href="/business/rechargeDelete/<?= $var['id'] ?>" target="ajax" dataType="json"
                           call="defaultAjaxLink" title="确认取消?">取消</a>
                    <?php } else { ?>
                        <span>--</span>
                    <?php } ?>
                </td>
            </tr>
        <?php }
    } else { ?>
        <tr>
            <td colspan="13">暂时没有充值记录。</td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="13" class="black font-larger">
                <?php
                $totalData = $this->getRow($totalSql, $binds);
                $total = (float)$totalData['TOTAL'];
                if (!empty($para['toTime']) || !empty($para['fromTime'])) {
                    echo '<span class="red">';
                    echo (empty($para['fromTime']) ? '以往' : htmlspecialchars($para['fromTime'])) . ' ~ ' . (empty($para['toTime']) ? '迄今' : htmlspecialchars($para['toTime']));
                    echo '</span><br>';
                }
                ?>
                充值成功总额: <?php echo number_format($total, 3) ?> 元<br>
                本页充值成功总额: <?php echo number_format($amount, 3) ?> 元
            </td>
        </tr>
    </tfoot>
</table>

<footer>
    <?php
    $rel = get_class($this) . '/rechargeLog-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $data['total'], $rel, 'defaultReplacePageAction');
    ?>
</footer>
