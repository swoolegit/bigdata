<?php
$this->getETypes();
//$this->getPlayeds();
//print_r($this->etypes);
// 帐号限制
if ($_REQUEST['username']) {
    $_REQUEST['username'] = wjStrFilter($_REQUEST['username']);
    if (!preg_match('/^\w{2,32}$/', $_REQUEST['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    $userWhere = "and b.username = '{$_REQUEST['username']}'";
}

// $hidedata=" and b.forTest=0";
// if(isset($_GET['sethide']) && $_GET['sethide']=="1")
// {
// 	$hidedata=" and b.forTest=1";
// }


// 时间限制
if ($_REQUEST['fromTime'] && $_REQUEST['toTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $toTime = strtotime($_REQUEST['toTime']);
    $timeWhere = "and actionTime between {$fromTime} and {$toTime}";
} elseif ($_REQUEST['fromTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $timeWhere = "and actionTime >= {$fromTime}";
} elseif ($_REQUEST['toTime']) {
    $toTime = strtotime($_REQUEST['toTime']);
    $timeWhere = "and actionTime < {$toTime}";
} else {
    $timeWhere = ' and actionTime >' . strtotime('00:00');;
}

$table="egame_history";
if(!empty($fromTime) || !empty($toTime))
{
    $time=time()-(7*86400);
    if($fromTime <= $time || $toTime <= $time)
    {
        $table="egame_history";
    }
}

$sql = "SELECT * FROM {$this->prename}{$table} AS b WHERE 1 {$timeWhere} {$typeWhere} {$userWhere}  ORDER BY b.id DESC";
$totalSql = " 
            select sum(ValidBetAmount) as TOTAL,sum(winloss) as WINLOST from {$this->prename}{$table} where 1 {$timeWhere} {$typeWhere} {$userWhere} 
            ";
$time1=time();
$data = $this->getPage($sql, $this->page, $this->pageSize);
$time2=time();

$mname = array(
    '2.000' => '元',
    '0.200' => '角',
    '0.020' => '分',
    '0.002' => '厘'
);
$page_total=0;
$page_zjtotal=0;
$_status=[
    0=>"结算",
    1=>"投注",
    2=>"失败",
    3=>"退款",
];
?>

    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
            <th>游戏局号</th>
            <th>用户名</th>
            <th>更新日期</th>
            <th>游戏</th>
            <th>下注金额</th>
            <th>输赢金额</th>
        </tr>
        </thead>
        <tbody id="nav01">
        <?php if ($data['data']) foreach ($data['data'] as $var) { 
            $page_winlost+= $var['winloss'];
            ?>
            <tr >
                <td><?= $var['transaction_id'] ?></td>
                <td><?= $var['UserName'] ?></td>
                <td><?= $var['TranssctionTime'] ?></td>
                <td><?= $var['Groupname'] ?></td>
                <td><?= $var['ValidBetAmount'] ?></td>
                <td><?= $var['winloss'] ?></td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="15" align="center">暂时没有投注记录。</td>
            </tr>
        <?php } ?>
			<tr>
				<td colspan="15" align="center" >
					<?php
					$totalData = $this->getRow($totalSql);
                    $total = (float) $totalData['TOTAL'];
                    $WINLOST = (float) $totalData['WINLOST'];
					if ($toTime == '' && $fromTime == '') {
                        echo "本页有效投注总额：<font color=red>". $this->nformat($page_total,2) . '</font> 元 ,  会员输赢总计 : <font color=red>'.$this->nformat($page_winlost,2).'</font> 元<br>';
						echo '有效投注总额：<font color=red>' . $this->nformat($total,2) . '</font> 元 ,  会员输赢总计 : <font color=red>'.$this->nformat($WINLOST,2).'</font> 元';
					} else {
                        echo ($fromTime ? date("Y-m-d H:i:s",$fromTime) : '以往') . ' ~ ' . ($toTime ? date("Y-m-d H:i:s",$toTime) : '迄今') ;
                        echo "<br>本页有效投注总额：<font color=red>". $this->nformat($page_total,2) . '</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($page_winlost,2).'</font> 元';
                        echo '<br>有效投注总额：<font color=red>' . $this->nformat($total,2) . '</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($WINLOST,2).'</font> 元';
					}
					?>
				</td>
			</tr>
        </tbody>
    </table>
    <footer>
        <?php

        $rel = get_class($this) . '/ebetLog_list-{page}?' . http_build_query($_GET, '', '&');
        $this->display('inc/page.php', 0, $data['total'], $rel, 'betLogSearchPageAction');
        ?>
    </footer>