<?php
$para = $_GET;

$where_cond = array();
$binds = array();

// 用户限制
if (isset($para['username']) && $para['username']!=='') {
    $para['username'] = wjStrFilter($para['username']);
    if (!preg_match('/^\w{2,32}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    $where_cond[] = "c.musername LIKE :like_username";
    $binds['like_username'] = "%{$para['username']}%";
}

if (!isset($para['toTime'])) {
    $_SESSION['toTime'] = '';
} else {
    $_SESSION['toTime'] = $para['toTime'];
}
if (!isset($para['fromTime'])) {
    $_SESSION['fromTime'] = '';
} else {
    $_SESSION['fromTime'] = $para['fromTime'];
}

// 时间限制
switch ((isset($para['timeType']) ? $para['timeType'] : '')) {
    case 'cashTime':        // 到帐时间
        $timeType = 'cashTime';
        break;
    default:   // 申请时间
        $timeType = 'actionTime';
        break;
}
if ((isset($para['fromTime']) && $para['fromTime']!=='') && (isset($para['toTime']) && $para['toTime']!=='')) {
    $where_cond[] = "c.{$timeType} BETWEEN :fromTime AND :toTime";
    $binds['fromTime'] = strtotime($para['fromTime']);
    $binds['toTime'] = strtotime($para['toTime']) + 24 * 3600;
} else if (isset($para['fromTime']) && $para['fromTime']!=='') {
    $where_cond[] = "c.{$timeType} >= :fromTime";
    $binds['fromTime'] = strtotime($para['fromTime']);
} elseif (isset($para['toTime']) && $para['toTime']!=='') {
    $where_cond[] = "c.{$timeType} < :toTime";
    $binds['toTime'] = strtotime($para['toTime']) + 24 * 3600;
}

// 渠道
if (isset($para['state'])) {
    switch ($para['state']) {
        case '1':
        case '4':
            $where_cond[] = "c.state =:state";
            $binds['state'] = $para['state'];
            break;
        case '3':
            $where_cond[] = "c.state in (0,3)";
            break;
        default:
            break;
    }
}

$where_cond[] = "c.isDelete =0";
$total_where_cond = array_merge($where_cond, array('c.state < 5', 'c.state !=4', 'c.flag =1'));

$where_cond = $where_cond ? 'WHERE ' . implode(' AND ', $where_cond) : '';
$total_where_cond = $total_where_cond ? 'WHERE ' . implode(' AND ', $total_where_cond) : '';

$sql =
    "SELECT
        c.*
    FROM {$this->prename}member_cash AS c
    {$where_cond}
    ORDER BY c.id DESC";

$totalSql =
    "SELECT SUM(c.amount) AS TOTAL
    FROM {$this->prename}member_cash AS c
    {$total_where_cond}";

$data = $this->getPage($sql, $this->page, $this->pageSize, $binds);

$stateName = array('已出款', '申请中', '已取消', '已支付', '已失败', '已删除');
$stateColor = array('00008b', 'b8860b', 'dc143c', 'ff7f50', '5f9ea0', 'd8d8bf');
$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b');
?>


<table class="table-sorter text-center" cellspacing="0">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <thead>
    <tr>
    	<th>订单编号</th>
        <!--th>UserID</th-->
        <th>用户名</th>
        <!--th>上级关系</th-->
        <th>提现金额</th>
        <th>开户姓名</th>
        <th>银行账号</th>
        <th>开户行</th>
        <th>申请时间</th>
        <th>处理时间</th>
        <th>状态</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody id="nav01">
    <?php
    $amount = 0;
    if ($data['data']) {
        foreach ($data['data'] as $var) {
            if ($var['state'] == 0 || $var['state'] == 3) $amount += $var['amount'];
            $bankName = htmlspecialchars($var['bankName']);
            if (!isset($_SESSION['randColor'][$bankName])) {
                $_SESSION['randColor'][$bankName] = $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)] . $rand[rand(0, 15)];;
            }
            $bankName = "<font color='#{$_SESSION['randColor'][$bankName]}'>{$bankName}</font>";
            ?>
            <tr>
            	<td><?= $var['id'] ?></td>
                <td><?= htmlspecialchars($var['musername']) ?></td>
                <td><?= $var['amount'] ?></td>
                <td><?= htmlspecialchars($var['username']) ?></td>
                <td><?= htmlspecialchars($var['account']) ?></td>
                <td><?= htmlspecialchars($var['countname']) ?></td>
                <td><?= date('Y-m-d H:i', $var['actionTime']) ?></td>
                <td><?= $var['cashTime'] ? date('Y-m-d H:i', $var['cashTime']) : '' ?></td>
                <td>
                    <?php
                    if ($var['state'] >= 0) {
                        echo "<font color='#{$stateColor[$var['state']]}'>{$stateName[$var['state']]}</font>";
                    } else {
                        switch ($var['state']) {
                            case -1:
                                echo "<font color='#EA7500'>已审核</font>";
                                break;
                        }
                    }
                    ?>
                </td>
                <td align="center">
                    <?php if ($var['state'] == 1 ) { ?>
                        <a href="/business/cashActionModal2/<?= $var['id'] ?>" target="modal" width="420"
                           title="代付处理" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">代付处理</a>                        
                        | <a href="/business/cashActionModal/<?= $var['id'] ?>" target="modal" width="420"
                           title="人工处理" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">人工处理</a>
                    <?php
                    }
                    ?>
                </td>
            </tr>
        <?php }
    } else { ?>
        <tr>
            <td colspan="12" align="center">暂时没有提现记录。</td>
        </tr>
    <?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="12" class="black font-larger">
                <?php
                $totalData = $this->getRow($totalSql, $binds);
                $total = (float)$totalData['TOTAL'];
                if (!empty($para['toTime']) || !empty($para['fromTime'])) {
                    echo '<span class="red">';
                    echo (empty($para['fromTime']) ? '以往' : htmlspecialchars($para['fromTime'])) . ' ~ ' . (empty($para['toTime']) ? '迄今' : htmlspecialchars($para['toTime']));
                    echo '</span><br>';
                }
                ?>
                提款总额: <?php echo number_format($total) ?> 元<br>
                本页提款总额: <?php echo number_format($amount) ?> 元
            </td>
        </tr>
    </tfoot>
</table>

<footer>
    <?php
    $rel = get_class($this) . '/cashLog-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $data['total'], $rel, 'defaultReplacePageAction');
    ?>
</footer>


<script type="text/javascript">
    ghhs("nav01", "tr");
</script>