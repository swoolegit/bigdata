
<script language="javascript">
    function cashFalse() {
        $('.cashFalseSM').css('display', 'block');
    }
    function cashTrue() {
        $('.cashFalseSM').css('display', 'none');
        $('.cashFalseSM').val() = false;
    }
</script>
<?php



$sql = "select b.name bankName, c.*, u.username userAccount,u.parents, d.countname from {$this->prename}bank_list b, {$this->prename}member_cash c, {$this->prename}members u, {$this->prename}member_bank d where u.forTest=0 and c.state=1 and c.bankId=b.id and c.uid=u.uid and d.uid=u.uid";
//echo $sql;
$data = $this->getPage($sql, $this->page, $this->pageSize);
//print_r($this);
//echo get_class($this);
?>
<article class="module width_full">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <header><h3 class="tabs_involved">提现请求
    	</h3></header>

    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
        	<th>订单编号</th>
            <th>UserID</th>
            <th>用户名</th>
            <th>上级关系</th>
            <th>提现金额</th>
            <th>银行类型</th>
            <th>开户姓名</th>
            <th>银行账号</th>
            <th>开户行</th>
            <th>时间</th>
            <th>操作</th>
        </tr>
        </thead>

        <tbody>
        <?php
        if ($data['data']) {
            foreach ($data['data'] as $var) {
                $var['parents'] = trim($var['parents'], ',');
                ?>
                <tr>
                	<td><?= $var['id'] ?></td>
                    <td><?= $var['uid'] ?></td>
                    <td><?= htmlspecialchars($var['userAccount']) ?></td>
                    <td><?= $var['parents']?implode('> ', $this->getCol("select username from {$this->prename}members where uid in ({$var['parents']})")):'' ?></td>
                    <td><?= $var['amount'] ?></td>
                    <td><?= htmlspecialchars($var['bankName']) ?></td>
                    <td><?= htmlspecialchars($var['username']) ?></td>
                    <td><?= htmlspecialchars($var['account']) ?></td>
                    <td><?= htmlspecialchars($var['countname']) ?></td>
                    <td><?= date('Y-m-d H:i:s', $var['actionTime']) ?></td>
                    <td align="center">
                        <?php if ($var['state'] == 0 || $var['state'] == 2 || $var['state'] == 4) { ?>
                            <a href="/index.php/business/cashLogDelete/<?= $var['id'] ?>">删除</a>
                        <?php } elseif ($var['state'] == 1) { ?>
                            <a href="/index.php/business/cashActionModal/<?= $var['id'] ?>" target="modal" width="420"
                               title="提款处理" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">处理</a>
                        <?php } elseif ($var['state'] >= 3) { ?>
                            --
                        <?php } ?>
                    </td>
                </tr>
            <?php }
        } else { ?>
            <tr>
                <td colspan="8" align="center">暂时没有人申请提现。</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <footer>
        <?php
        $rel = get_class($this) . '/cash-{page}?' . http_build_query($_GET, '', '&');
        $this->display('inc/page.php', 0, $data['total'], $rel, 'defaultReplacePageAction');
        ?>
    </footer>
</article>

<script>
<?php if($this->settings['cashFlow'] == 'niufu') { ?>
//==/*牛付*/============================================================================================================================================================================================================================================================================

var getCashAmount = function (_this){

	util.loading();
	$.ajax({
		type: "get",
		url: "/index.php/business/getCashAmount",
		data: {},
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(data){
			util.unloading();
		  if (data.retCode != 0) {
			  alert('检查异常');
		  }
		  else {
			$(_this).hide();
			$('.niufuAmount').html(data.balance);
		  }
		}
	});
};

var goPayment = function (target,username,account,amount,user_account){
	var $bank = $(target).parent().find('.banklist');

	//console.log($bank.text());
	if (!$bank.val())
	{
		alert('请选择银行代码');
		return false;
	}
	util.loading();

	//console.log($bank.text());
	$.ajax({
		type: "get",
		url: "/index.php/business/goPayment",
		data: {'bankId':$bank.val(),'abname':$bank.text(),'username':username,'account':account,'amount':amount,'user_account':user_account},
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		complete: function(data){
			util.unloading();
			var res = $.parseJSON(data.responseText);
			if (res.retCode != 0)
			{
				alert('代付失败:'+res.retCode);
			}
			else{
				alert('代付成功');

			}
		}
	});

};
var bankSearch = function (target){
	var banklist = {"ICBC":"\u5de5\u5546\u94f6\u884c","ABC":"\u519c\u4e1a\u94f6\u884c","BOC":"\u4e2d\u56fd\u94f6\u884c","BOCM":"\u4ea4\u901a\u94f6\u884c","COMB":"\u4e0a\u6d77\u4ea4\u901a\u94f6\u884c","PAB":"\u5e73\u5b89\u94f6\u884c","CMB":"\u62db\u5546\u94f6\u884c","CNCB":"\u4e2d\u4fe1\u94f6\u884c","NMGYH":"\u5185\u8499\u53e4\u94f6\u884c","FJHXYH":"\u798f\u5efa\u6d77\u5ce1\u94f6\u884c","JZYH":"\u9526\u5dde\u94f6\u884c","HZYH":"\u6e56\u5dde\u94f6\u884c","TZYH":"\u53f0\u5dde\u94f6\u884c","WSYH":"\u5fbd\u5546\u94f6\u884c","DLYH":"\u5927\u8fde\u94f6\u884c","YTYH":"\u70df\u53f0\u94f6\u884c","SRYH":"\u4e0a\u9976\u94f6\u884c","EEDSYH":"\u9102\u5c14\u591a\u65af\u94f6\u884c","DZYH":"\u5fb7\u5dde\u94f6\u884c","BHYH":"\u6e24\u6d77\u94f6\u884c","XTYH":"\u90a2\u53f0\u94f6\u884c","HBYH":"\u6cb3\u5317\u94f6\u884c","LZYH":"\u67f3\u5dde\u94f6\u884c","WFYH":"\u6f4d\u574a\u94f6\u884c","YLYH":"\u53cb\u5229\u94f6\u884c","HBNX":"\u6e56\u5317\u519c\u4fe1","ZZYH":"\u90d1\u5dde\u94f6\u884c","JSYH":"\u664b\u5546\u94f6\u884c","GZYH":"\u8d63\u5dde\u94f6\u884c","JLYH":"\u5409\u6797\u94f6\u884c","HYYH":"\u97e9\u4e9a\u94f6\u884c","QDYH":"\u9752\u5c9b\u94f6\u884c","WZYH":"\u6e29\u5dde\u94f6\u884c","SZYH":"\u82cf\u5dde\u94f6\u884c","JNYH":"\u6d4e\u5b81\u94f6\u884c","YKYH":"\u8425\u53e3\u94f6\u884c","QHYH":"\u9752\u6d77\u94f6\u884c","SHYH":"\u4e0a\u6d77\u94f6\u884c","NYYH":"\u5357\u9633\u94f6\u884c","LYYH":"\u6d1b\u9633\u94f6\u884c","HFYH":"\u6052\u4e30\u94f6\u884c","TJNSYH":"\u5929\u6d25\u519c\u5546\u94f6\u884c","NCYH":"\u5357\u660c\u94f6\u884c","CQYH":"\u91cd\u5e86\u94f6\u884c","HKYH":"\u6c49\u53e3\u94f6\u884c","NXYH":"\u5b81\u590f\u94f6\u884c","SHNSYH":"\u4e0a\u6d77\u519c\u5546\u94f6\u884c","JXYH":"\u5609\u5174\u94f6\u884c","SZNSH":"\u6df1\u5733\u519c\u5546\u884c","CDYH":"\u627f\u5fb7\u94f6\u884c","GYYH":"\u8d35\u9633\u94f6\u884c","SXYH":"\u7ecd\u5174\u94f6\u884c","GDNYYH":"\u5e7f\u4e1c\u5357\u7ca4\u94f6\u884c","HLDYH":"\u846b\u82a6\u5c9b\u94f6\u884c","YZYH":"\u911e\u5dde\u94f6\u884c","KLYH":"\u6606\u4ed1\u94f6\u884c","SDSNLS":"\u5c71\u4e1c\u7701\u519c\u8054\u793e","XHYHZG":"\u65b0\u97e9\u94f6\u884c\u4e2d\u56fd","CSYH":"\u957f\u6c99\u94f6\u884c","DYYH":"\u5fb7\u9633\u94f6\u884c","DGYH":"\u4e1c\u839e\u94f6\u884c","NJYH":"\u9f99\u6c5f\u94f6\u884c","BSYH":"\u5305\u5546\u94f6\u884c","ZHHRYH":"\u73e0\u6d77\u534e\u6da6\u94f6\u884c","TJYH":"\u5929\u6d25\u94f6\u884c","RZYH":"\u65e5\u7167\u94f6\u884c","FXYH":"\u961c\u65b0\u94f6\u884c","FZYH":"\u5bcc\u6ec7\u94f6\u884c","HEBYH":"\u54c8\u5c14\u6ee8\u94f6\u884c","CAYH":"\u957f\u5b89\u94f6\u884c","QYYH":"\u4f01\u4e1a\u94f6\u884c","LFYH":"\u5eca\u574a\u94f6\u884c","LSYH":"\u83b1\u5546\u94f6\u884c","UNION":"\u94f6\u8054","CCB":"\u5efa\u8bbe\u94f6\u884c","GDB":"\u5e7f\u53d1\u94f6\u884c","HXB":"\u534e\u590f\u94f6\u884c","CEB":"\u4e2d\u56fd\u5149\u5927\u94f6\u884c","CMBC":"\u4e2d\u56fd\u6c11\u751f\u94f6\u884c","PSBC":"\u90ae\u653f\u50a8\u84c4","SPDB":"\u6d66\u53d1\u94f6\u884c","CIB":"\u5174\u4e1a\u94f6\u884c","SDB":"\u6df1\u5733\u53d1\u5c55\u94f6\u884c","NJB":"\u5357\u4eac\u94f6\u884c","CZB":"\u6d59\u5546\u94f6\u884c","HZB":"\u676d\u5dde\u94f6\u884c","NBB":"\u5b81\u6ce2\u94f6\u884c","XMYH":"\u53a6\u95e8\u94f6\u884c","CZYH":"\u6ca7\u5dde\u94f6\u884c","ALIPAY3":"\u652f\u4ed8\u5b9d","HDSSYYH":"\u90af\u90f8\u5e02\u5546\u4e1a\u94f6\u884c","WHSSYYH":"\u5a01\u6d77\u5e02\u5546\u4e1a\u94f6\u884c","HNSNCXYS":"\u6d77\u5357\u7701\u519c\u6751\u4fe1\u7528\u793e","MYSSYYH":"\u7ef5\u9633\u5e02\u5546\u4e1a\u94f6\u884c","ZJCZSYYH":"\u6d59\u6c5f\u7a20\u5dde\u5546\u4e1a\u94f6\u884c","YNSNCXYS":"\u4e91\u5357\u7701\u519c\u6751\u4fe1\u7528\u793e","KFSSYYH":"\u5f00\u5c01\u5e02\u5546\u4e1a\u94f6\u884c","CQNCSYYH":"\u91cd\u5e86\u519c\u6751\u5546\u4e1a\u94f6\u884c","CSNCSYYH":"\u5e38\u719f\u519c\u6751\u5546\u4e1a\u94f6\u884c","ZJSNCXYS":"\u6d59\u6c5f\u7701\u519c\u6751\u4fe1\u7528\u793e","WHYHYXGS":"\u5916\u6362\u94f6\u884c\uff08\u4e2d\u56fd\uff09\u6709\u9650\u516c\u53f8","KSNCSYYH":"\u6606\u5c71\u519c\u6751\u5546\u4e1a\u94f6\u884c","PZHSSYYH":"\u6500\u679d\u82b1\u5e02\u5546\u4e1a\u94f6\u884c","GXBBWYH":"\u5e7f\u897f\u5317\u90e8\u6e7e\u94f6\u884c","ZJKSSYYH":"\u5f20\u5bb6\u53e3\u5e02\u5546\u4e1a\u94f6\u884c","ZGSSYYH":"\u81ea\u8d21\u5e02\u5546\u4e1a\u94f6\u884c","QSYH":"\u9f50\u5546\u94f6\u884c","JSNCXYS":"\u6c5f\u82cf\u7701\u519c\u6751\u4fe1\u7528\u793e\u8054\u5408\u793e","FJSNCXYS":"\u798f\u5efa\u7701\u519c\u6751\u4fe1\u7528\u793e","JCSSYYH":"\u664b\u57ce\u5e02\u5546\u4e1a\u94f6\u884c","AHSNCXYS":"\u5b89\u5fbd\u7701\u519c\u6751\u4fe1\u7528\u793e\u8054\u5408\u793e","GXNCXYS":"\u5e7f\u897f\u519c\u6751\u4fe1\u7528\u793e\uff08\u5408\u4f5c\u94f6\u884c\uff09","ZJMTYH":"\u6d59\u6c5f\u6c11\u6cf0\u5546\u4e1a\u94f6\u884c","JLNCXYS":"\u5409\u6797\u519c\u6751\u4fe1\u7528\u793e","LHSSYYH":"\u6f2f\u6cb3\u5e02\u5546\u4e1a\u94f6\u884c","HHNCSYYH":"\u9ec4\u6cb3\u519c\u6751\u5546\u4e1a\u94f6\u884c","SDNCSYYH":"\u987a\u5fb7\u519c\u6751\u5546\u4e1a\u94f6\u884c","TASSYYH":"\u6cf0\u5b89\u5e02\u5546\u4e1a\u94f6\u884c","ZJGNCSYYH":"\u5f20\u5bb6\u6e2f\u519c\u6751\u5546\u4e1a\u94f6\u884c","ZJTNSYYH":"\u6d59\u6c5f\u6cf0\u9686\u5546\u4e1a\u94f6\u884c","WJNCSYYH":"\u5434\u6c5f\u519c\u6751\u5546\u4e1a\u94f6\u884c","GZNCSYYH":"\u5e7f\u5dde\u519c\u6751\u5546\u4e1a\u94f6\u884c","DGNCSYYH":"\u4e1c\u839e\u519c\u6751\u5546\u4e1a\u94f6\u884c","DGSSYYH":"\u4e1c\u8425\u5e02\u5546\u4e1a\u94f6\u884c","WLMQSSYYH":"\u4e4c\u9c81\u6728\u9f50\u5e02\u5546\u4e1a\u94f6\u884c","SQSSYYH":"\u5546\u4e18\u5e02\u5546\u4e1a\u94f6\u884c","ASYH":"\u978d\u5c71\u5e02\u5546\u4e1a\u94f6\u884c","BOBJ":"\u5317\u4eac\u94f6\u884c","TF":"\u901a\u4ed8","GhBank":"\u534e\u5174\u94f6\u884c","BOJJ":"\u4e5d\u6c5f\u94f6\u884c"};
	var sword = $(target).parent().find('.sword').val();
	if (!sword) return;
	var $bank = $(target).parent().find('.banklist');
	$bank.find('option').remove().end();
	$.each(banklist,function (val,text){
		if (text.indexOf(sword) != -1) {
			$bank.append('<option value='+val+'>'+text+'</option>');
		}
	});
};
<?php }?>

//==/*艾付*/============================================================================================================================================================================================================================================================================
<?php if($this->settings['cashFlow'] == 'aifu') { ?>
//==/*艾付*/============================================================================================================================================================================================================================================================================

var getCashAmount = function (_this){

	util.loading();
	$.ajax({
		type: "get",
		url: "/index.php/business/getCashAmountAifu",
		data: {},
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function(data){
			console.log(data);
			util.unloading();
		  if (data.result_code != '000000') {
			  alert('检查异常');
		  }
		  else {
			$(_this).hide();
			$('.aifuAmount').html(data.balance);
		  }
		}
	});
};

var goPayment = function (target,username,account,amount,user_account,id){

	$(target).attr('disabled',true);

	var $bank = $(target).parent().find('.banklist');

	//console.log($bank.text());
	if (!$bank.val())
	{
		alert('请选择银行代码');
		return false;
	}


	
	
	util.loading();

	//console.log($bank.text());
	$.ajax({
		type: "get",
		url: "/index.php/business/goPaymentAifu",
		data: {'bankId':$bank.val(),'abname':$bank.text(),'username':username,'account':account,'amount':amount,'user_account':user_account,'id':id},
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		complete: function(data){
			$(target).attr('disabled',false);
			util.unloading();
			var res = $.parseJSON(data.responseText);
			if (res.result_code != '000000')
			{
				alert('代付失败:'+res.result_code);
			}
			else{
				
				alert('代付成功');
				$('.ui-dialog-titlebar-close').click();
				$('.alt_btn').click();
			}


		}
	});

};
var bankSearch = function (target){
				   
	//var banklist = {"CMB":"\u62db\u5546\u94f6\u884c","CCB":"\u5efa\u8bbe\u94f6\u884c","ABC":"\u519c\u4e1a\u94f6\u884c","BOC":"\u4e2d\u56fd\u94f6\u884c","GDB":"\u5e7f\u53d1\u94f6\u884c","HXB":"\u534e\u590f\u94f6\u884c","NBB":"\u5b81\u6ce2\u94f6\u884c","CIB":"\u5174\u4e1a\u94f6\u884c","SDB":"\u6df1\u5733\u53d1\u5c55\u94f6\u884c","PAB":"\u5e73\u5b89\u94f6\u884c","NJB":"\u5357\u4eac\u94f6\u884c","CZB":"\u6d59\u5546\u94f6\u884c","HZB":"\u676d\u5dde\u94f6\u884c","CEB":"\u4e2d\u56fd\u5149\u5927\u94f6\u884c","JZYH":"\u9526\u5dde\u94f6\u884c","HZYH":"\u6e56\u5dde\u94f6\u884c","TZYH":"\u53f0\u5dde\u94f6\u884c","WSYH":"\u5fbd\u5546\u94f6\u884c","DLYH":"\u5927\u8fde\u94f6\u884c","YTYH":"\u70df\u53f0\u94f6\u884c","SRYH":"\u4e0a\u9976\u94f6\u884c","DZYH":"\u5fb7\u5dde\u94f6\u884c","BHYH":"\u6e24\u6d77\u94f6\u884c","XTYH":"\u90a2\u53f0\u94f6\u884c","HBYH":"\u6cb3\u5317\u94f6\u884c","LZYH":"\u67f3\u5dde\u94f6\u884c","WFYH":"\u6f4d\u574a\u94f6\u884c","YLYH":"\u53cb\u5229\u94f6\u884c","HBNX":"\u6e56\u5317\u519c\u4fe1","ZZYH":"\u90d1\u5dde\u94f6\u884c","JSYH":"\u664b\u5546\u94f6\u884c","GZYH":"\u8d63\u5dde\u94f6\u884c","JLYH":"\u5409\u6797\u94f6\u884c","HYYH":"\u97e9\u4e9a\u94f6\u884c","QDYH":"\u9752\u5c9b\u94f6\u884c","WZYH":"\u6e29\u5dde\u94f6\u884c","SZYH":"\u82cf\u5dde\u94f6\u884c","JNYH":"\u6d4e\u5b81\u94f6\u884c","YKYH":"\u8425\u53e3\u94f6\u884c","QHYH":"\u9752\u6d77\u94f6\u884c","SHYH":"\u4e0a\u6d77\u94f6\u884c","NYYH":"\u5357\u9633\u94f6\u884c","LYYH":"\u6d1b\u9633\u94f6\u884c","HFYH":"\u6052\u4e30\u94f6\u884c","NCYH":"\u5357\u660c\u94f6\u884c","CQYH":"\u91cd\u5e86\u94f6\u884c","HKYH":"\u6c49\u53e3\u94f6\u884c","NXYH":"\u5b81\u590f\u94f6\u884c","JXYH":"\u5609\u5174\u94f6\u884c","CDYH":"\u627f\u5fb7\u94f6\u884c","GYYH":"\u8d35\u9633\u94f6\u884c","SXYH":"\u7ecd\u5174\u94f6\u884c","YZYH":"\u911e\u5dde\u94f6\u884c","KLYH":"\u6606\u4ed1\u94f6\u884c","CSYH":"\u957f\u6c99\u94f6\u884c","DYYH":"\u5fb7\u9633\u94f6\u884c","DGYH":"\u4e1c\u839e\u94f6\u884c","NJYH":"\u9f99\u6c5f\u94f6\u884c","BSYH":"\u5305\u5546\u94f6\u884c","TJYH":"\u5929\u6d25\u94f6\u884c","RZYH":"\u65e5\u7167\u94f6\u884c","FXYH":"\u961c\u65b0\u94f6\u884c","FZYH":"\u5bcc\u6ec7\u94f6\u884c","CAYH":"\u957f\u5b89\u94f6\u884c","LFYH":"\u5eca\u574a\u94f6\u884c","LSYH":"\u83b1\u5546\u94f6\u884c","ICBC":"\u5de5\u5546\u94f6\u884c","BOCM":"\u4ea4\u901a\u94f6\u884c","CMBC":"\u4e2d\u56fd\u6c11\u751f\u94f6\u884c","PSBC":"\u90ae\u653f\u50a8\u84c4","CNCB":"\u4e2d\u4fe1\u94f6\u884c","SPDB":"\u6d66\u53d1\u94f6\u884c","COMB":"\u4e0a\u6d77\u4ea4\u901a\u94f6\u884c","XMYH":"\u53a6\u95e8\u94f6\u884c","CZYH":"\u6ca7\u5dde\u94f6\u884c","QSYH":"\u9f50\u5546\u94f6\u884c","ASYH":"\u978d\u5c71\u5e02\u5546\u4e1a\u94f6\u884c","BOBJ":"\u5317\u4eac\u94f6\u884c","BOJJ":"\u4e5d\u6c5f\u94f6\u884c","HLDYH":"\u846b\u82a6\u5c9b\u94f6\u884c","NMGYH":"\u5185\u8499\u53e4\u94f6\u884c","HEBYH":"\u54c8\u5c14\u6ee8\u94f6\u884c","SZNSH":"\u6df1\u5733\u519c\u5546\u884c","ZHHRYH":"\u73e0\u6d77\u534e\u6da6\u94f6\u884c","GDNYYH":"\u5e7f\u4e1c\u5357\u7ca4\u94f6\u884c","FJHXYH":"\u798f\u5efa\u6d77\u5ce1\u94f6\u884c","SDSNLS":"\u5c71\u4e1c\u7701\u519c\u8054\u793e","ZJMTYH":"\u6d59\u6c5f\u6c11\u6cf0\u5546\u4e1a\u94f6\u884c","SHNSYH":"\u4e0a\u6d77\u519c\u5546\u94f6\u884c","EEDSYH":"\u9102\u5c14\u591a\u65af\u94f6\u884c","GhBank":"\u534e\u5174\u94f6\u884c","TJNSYH":"\u5929\u6d25\u519c\u5546\u94f6\u884c","XHYHZG":"\u65b0\u97e9\u94f6\u884c\u4e2d\u56fd","MYSSYYH":"\u7ef5\u9633\u5e02\u5546\u4e1a\u94f6\u884c","WHSSYYH":"\u5a01\u6d77\u5e02\u5546\u4e1a\u94f6\u884c","HDSSYYH":"\u90af\u90f8\u5e02\u5546\u4e1a\u94f6\u884c","SQSSYYH":"\u5546\u4e18\u5e02\u5546\u4e1a\u94f6\u884c","DGSSYYH":"\u4e1c\u8425\u5e02\u5546\u4e1a\u94f6\u884c","TASSYYH":"\u6cf0\u5b89\u5e02\u5546\u4e1a\u94f6\u884c","LHSSYYH":"\u6f2f\u6cb3\u5e02\u5546\u4e1a\u94f6\u884c","JLNCXYS":"\u5409\u6797\u519c\u6751\u4fe1\u7528\u793e","GXNCXYS":"\u5e7f\u897f\u519c\u6751\u4fe1\u7528\u793e\uff08\u5408\u4f5c\u94f6\u884c\uff09","JCSSYYH":"\u664b\u57ce\u5e02\u5546\u4e1a\u94f6\u884c","JSNCXYS":"\u6c5f\u82cf\u7701\u519c\u6751\u4fe1\u7528\u793e\u8054\u5408\u793e","ZGSSYYH":"\u81ea\u8d21\u5e02\u5546\u4e1a\u94f6\u884c","GXBBWYH":"\u5e7f\u897f\u5317\u90e8\u6e7e\u94f6\u884c","KFSSYYH":"\u5f00\u5c01\u5e02\u5546\u4e1a\u94f6\u884c","ZJTNSYYH":"\u6d59\u6c5f\u6cf0\u9686\u5546\u4e1a\u94f6\u884c","YNSNCXYS":"\u4e91\u5357\u7701\u519c\u6751\u4fe1\u7528\u793e","WJNCSYYH":"\u5434\u6c5f\u519c\u6751\u5546\u4e1a\u94f6\u884c","AHSNCXYS":"\u5b89\u5fbd\u7701\u519c\u6751\u4fe1\u7528\u793e\u8054\u5408\u793e","GZNCSYYH":"\u5e7f\u5dde\u519c\u6751\u5546\u4e1a\u94f6\u884c","ZJCZSYYH":"\u6d59\u6c5f\u7a20\u5dde\u5546\u4e1a\u94f6\u884c","DGNCSYYH":"\u4e1c\u839e\u519c\u6751\u5546\u4e1a\u94f6\u884c","HNSNCXYS":"\u6d77\u5357\u7701\u519c\u6751\u4fe1\u7528\u793e","HHNCSYYH":"\u9ec4\u6cb3\u519c\u6751\u5546\u4e1a\u94f6\u884c","FJSNCXYS":"\u798f\u5efa\u7701\u519c\u6751\u4fe1\u7528\u793e","SDNCSYYH":"\u987a\u5fb7\u519c\u6751\u5546\u4e1a\u94f6\u884c","ZJKSSYYH":"\u5f20\u5bb6\u53e3\u5e02\u5546\u4e1a\u94f6\u884c","PZHSSYYH":"\u6500\u679d\u82b1\u5e02\u5546\u4e1a\u94f6\u884c","KSNCSYYH":"\u6606\u5c71\u519c\u6751\u5546\u4e1a\u94f6\u884c","ZJSNCXYS":"\u6d59\u6c5f\u7701\u519c\u6751\u4fe1\u7528\u793e","CSNCSYYH":"\u5e38\u719f\u519c\u6751\u5546\u4e1a\u94f6\u884c","CQNCSYYH":"\u91cd\u5e86\u519c\u6751\u5546\u4e1a\u94f6\u884c","WLMQSSYYH":"\u4e4c\u9c81\u6728\u9f50\u5e02\u5546\u4e1a\u94f6\u884c","ZJGNCSYYH":"\u5f20\u5bb6\u6e2f\u519c\u6751\u5546\u4e1a\u94f6\u884c"};
	var banklist = {"ICBC":"\u4e2d\u56fd\u5de5\u5546\u94f6\u884c","ABC":"\u4e2d\u56fd\u519c\u4e1a\u94f6\u884c","CCB":"\u4e2d\u56fd\u5efa\u8bbe\u94f6\u884c","BOC":"\u4e2d\u56fd\u94f6\u884c","PSBC":"\u4e2d\u56fd\u90ae\u653f\u50a8\u84c4\u94f6\u884c","CEB":"\u4e2d\u56fd\u5149\u5927\u94f6\u884c","CMBC":"\u4e2d\u56fd\u6c11\u751f\u94f6\u884c","CMB":"\u62db\u5546\u94f6\u884c","BOCOM":"\u4ea4\u901a\u94f6\u884c","CNCB":"\u4e2d\u4fe1\u94f6\u884c","SPDB":"\u6d66\u53d1\u94f6\u884c","GDB":"\u5e7f\u53d1\u94f6\u884c","HXB":"\u534e\u590f\u94f6\u884c","CIB":"\u5174\u4e1a\u94f6\u884c","PAB":"\u5e73\u5b89\u94f6\u884c","BCCB":"\u5317\u4eac\u94f6\u884c","NJB":"\u5357\u4eac\u94f6\u884c","HZB":"\u676d\u5dde\u94f6\u884c","NBB":"\u5b81\u6ce2\u94f6\u884c"};
	var sword = $(target).parent().find('.sword').val();
	if (!sword) return;
	var $bank = $(target).parent().find('.banklist');
	$bank.find('option').remove().end();
	$.each(banklist,function (val,text){
		if (text.indexOf(sword) != -1) {
			$bank.append('<option value='+val+'>'+text+'</option>');
		}
	});
};
<?php }?>


//==/*天合宝*/============================================================================================================================================================================================================================================================================

<?php if($this->settings['cashFlow'] == 'tianhebao') { /*天合宝*/ ?>


var goPayment = function (target,username,account,amount,user_account){

	var $bank = $(target).parent().find('.banklist');

	//console.log($bank.text());
	if (!$bank.val())
	{
		alert('请选择银行代码');
		return false;
	}
	util.loading();

	//console.log($bank.text());
	$.ajax({
		type: "get",
		url: "/index.php/business/goPaymentTianhebao",
		data: {'bankId':$bank.val(),'abname':$bank.text(),'username':username,'account':account,'amount':amount,'user_account':user_account},
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		complete: function(data){
			util.unloading();
			var res = $.parseJSON(data.responseText);
			if (res.code != 0)
			{
				alert('代付失败:'+res.msg);
			}
			else{
				alert('代付成功');

			}
		}
	});

};

/*
array(
'1002'=>'中国工商银行',
'1005'=>'中国农业银行',
'1003'=>'中国建设银行',
'1026'=>'中国银行',
'1001'=>'招商银行',
'1006'=>'民生银行',
'1020'=>'交通银行',
'1025'=>'华夏银行',
'1009'=>'兴业银行',
'1027'=>'广发银行',
'1004'=>'浦发银行',
'1022'=>'光大银行',
'1021'=>'中信银行',
'1010'=>'平安银行',
'1066'=>'中国邮政储蓄银行'
)
*/
var bankSearch = function (target){
	var banklist = {"1002":"\u4e2d\u56fd\u5de5\u5546\u94f6\u884c","1005":"\u4e2d\u56fd\u519c\u4e1a\u94f6\u884c","1003":"\u4e2d\u56fd\u5efa\u8bbe\u94f6\u884c","1026":"\u4e2d\u56fd\u94f6\u884c","1001":"\u62db\u5546\u94f6\u884c","1006":"\u6c11\u751f\u94f6\u884c","1020":"\u4ea4\u901a\u94f6\u884c","1025":"\u534e\u590f\u94f6\u884c","1009":"\u5174\u4e1a\u94f6\u884c","1027":"\u5e7f\u53d1\u94f6\u884c","1004":"\u6d66\u53d1\u94f6\u884c","1022":"\u5149\u5927\u94f6\u884c","1021":"\u4e2d\u4fe1\u94f6\u884c","1010":"\u5e73\u5b89\u94f6\u884c","1066":"\u4e2d\u56fd\u90ae\u653f\u50a8\u84c4\u94f6\u884c"};
	var sword = $(target).parent().find('.sword').val();
	if (!sword) return;
	var $bank = $(target).parent().find('.banklist');
	$bank.find('option').remove().end();
	$.each(banklist,function (val,text){
		if (text.indexOf(sword) != -1) {
			$bank.append('<option value='+val+'>'+text+'</option>');
		}
	});
};
<?php }?>

function cashLogList(err, data) {
    if (err) {
        alert(err);
    } else {
        $('.tab_content').html(data);
    }
}
</script>
