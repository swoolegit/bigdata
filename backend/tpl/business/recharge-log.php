<?php
if (isset($_GET['excel']) && $_GET['excel'] == '1') {
    $date = date('Ymd');
    header('Content-type:application/vnd.ms-excel');  //声明网页格式
    header('Content-Disposition: attachment; filename=' . $date . '_cash_log.xls');  //设置文件名称

    $para = $_GET;

	$hidedata=" and u.forTest=0";
	if(isset($_GET['sethide']) && $_GET['sethide']=="1")
	{
		$hidedata=" and u.forTest=1";
	}

    $where_cond = array();
    $binds = array();

    // 时间限制
    if (!empty($para['fromTime']) && !empty($para['toTime'])) {
        $where_cond[] = "c.actionTime BETWEEN :fromTime AND :toTime";
        $binds['fromTime'] = strtotime($para['fromTime']);
        $binds['toTime'] = strtotime($para['toTime']);
    } elseif (! empty($para['fromTime'])) {
        $where_cond[] = "c.actionTime >= :fromTime";
        $binds['fromTime'] = strtotime($para['fromTime']);
    } elseif (! empty($para['toTime'])) {
        $where_cond[] = "c.actionTime < :toTime";
        $binds['toTime'] = strtotime($para['toTime']);
    }

    // 状态
    if (isset($para['state'])) {
        switch ($para['state']) {
            case '0':
            case '1':
            case '2':
            case '9':
                $where_cond[] = "c.state =:state";
                $binds['state'] = $para['state'];
                break;
            case '-9':
                $where_cond[] = "c.state !=9";
                break;
        }
    }

    // 用户限制
    if (isset($para['username']) && $para['username']!=='') {
        $para['username'] = wjStrFilter($para['username']);
        if (!preg_match('/^\w{2,32}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
        $where_cond[] = "u.username LIKE :like_username";
        $binds['like_username'] = "%{$para['username']}%";
    }

    // 充值编号限制
    if (isset($para['rechargeId']) && $para['rechargeId']!=='') {
        $para['rechargeId'] = wjStrFilter($para['rechargeId'], 0, 0);
        if (!ctype_digit($para['rechargeId'])) throw new Exception('充值编号包含非法字符');
        $where_cond[] = "c.rechargeId =:rechargeId";
        $binds['rechargeId'] = $para['rechargeId'];
    }

    // 银行代号
    $bank_id = !empty($para['bank_id']) ? intval($para['bank_id']) : 0;
    if ($bank_id) {
        $where_cond[] = "c.bankId =:bankId";
        $binds['bankId'] = $bank_id;
    }

    $where_cond[] = "c.isDelete =0";
    $where_cond = $where_cond ? 'WHERE ' . implode(' AND ', $where_cond) : '';

    $sql =
        "SELECT
            c.id,
            c.uid,
            c.bankId,
            c.state,
            c.rechargeId,
            c.amount,
            c.rechargeAmount,
            c.coin,
            c.info,
            c.actionTime,
            c.memo,
            u.username,
            u.parents
        FROM {$this->prename}member_recharge AS c
            JOIN {$this->prename}members AS u ON u.uid = c.uid {$hidedata}
        {$where_cond}
        ORDER BY c.id DESC";
    $data = $this->getRows($sql, $binds);
    ?>

    <html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
    <table border="1">
        <tr>
            <th>UserID</th>
            <th>用户名</th>
            <th>上级关系</th>
            <th>充值金额</th>
            <th>实际到账</th>
            <th>充值前资金</th>
            <th>充值编号</th>
            <th>充值银行</th>
            <th>状态</th>
            <th>备注</th>
            <th>时间</th>
        </tr>
        <?php
        $amount = 0;
        foreach ($data as $var) {
            if ($var['state'] == 0 || $var['state'] == 3) $amount += $var['amount'];
            $var['parents'] = trim($var['parents'], ',');
            ?>
            <tr>
                <td><?= $var['uid'] ?></td>
                <td><?= $var['username'] ?></td>
                <td><?= implode(' &gt; ', $this->getCol("select username from {$this->prename}members where uid in ({$var['parents']})")) ?></td>
                <td><?= $var['amount'] ?></td>
                <td><?= $var['rechargeAmount'] ?></td>
                <td><?= $this->iff($var['state'], $var['coin'], '--') ?></td>
                <td><?= $var['rechargeId'] ?></td>
                <td><?= $bank_name ?></td>
                <td><?= $this->iff($var['state'], '充值成功', '正在申请') ?></td>
                <td><?= $var['info'] ?></td>
                <td><?= date('Y-m-d H:i:s', $var['actionTime']) ?></td>
            </tr>
        <?php } ?>
    </table>
    </body>
    </html>
    <?php
    // 输出 excel 程序应该执行到此为止而已
    exit;
} else {
    // $banks = array();
    // $sql = "SELECT id, name FROM {$this->prename}bank_list WHERE isDelete =0 ORDER BY sort DESC, id ASC";
    // $rows = $this->getRows($sql);
    // foreach ($rows as $v) {
    //     $banks[$v['id']] = $v;
    // }
}
?>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved">充值记录
            <form id="query-form" class="submit_link wz" action="/business/rechargeLogList" method="get" target="ajax" dataType="html" call="rechargeLogList">
                用户名：<input type="text" class="alt_btn" style="width:80px;" autocomplete="off" id="username" name="username" placeholder="用户名" />
                <div class="auto-screening auto-hidden mbox" id="autoScreening"></div>　
                充值编号：<input type="text" class="alt_btn" autocomplete="off" style="width:80px;" name="rechargeId" placeholder="充值编号" />　
                <select name="timeType" style="width:80px">
                    <option value="actionTime">申请时间</option>
                    <option value="rechargeTime">到帐时间</option>
                </select>：
                    从 <input type="text" class="text-center" autocomplete="off"  name="fromTime" />
                    到 <input type="text" class="text-center"  autocomplete="off" name="toTime" />　
                渠道：<select name="rechargeModel" style="width:100px">
                    <option value="">所有渠道</option>
                    <option value="0">公司汇款</option>
                    <option value="1">三方充值</option>
                    <option value="2">管理员充值</option>
                    </select>　
                状态：<select name="state" style="width:100px">
                    <option value="">所有状态</option>
                    <option value="0">正在申请</option>
                    <option value="1">充值成功</option>
                    <option value="2">失败</option>
                </select>　
                <input type="submit" value="查找" class="alt_btn">
                <input type="reset" value="重置条件">
            </form>
        </h3>
    </header>

    <div class="tab_content">
        <?php $this->display("business/recharge-log-list.php"); ?>
    </div>
</article>


<script type="text/javascript">
$(function () {
    $('.tabs_involved input[name=username]')
        .keypress(function (e) {
            if (e.keyCode == 13) $(this).closest('form').submit();
        });
    $('.tabs_involved input[name=rechargeId]')
        .keypress(function (e) {
            if (e.keyCode == 13) $(this).closest('form').submit();
        });

    var f = document.getElementById("query-form"),
        $fromTime = $(f.fromTime),
        $toTime = $(f.toTime);
    $fromTime.datetimepicker({
        timeFormat: "HH:mm:ss",
        controlType: "select",
        oneLine: true,
        onSelect: function(selectedDateTime) {
            $toTime.datetimepicker("option", "minDate", selectedDateTime.split(" ")[0]);
        }
    });
    $toTime.datetimepicker({
        timeFormat: "HH:mm:ss",
        controlType: "select",
        oneLine: true,
        hour: 23,
        minute: 59,
        second: 59,
        onSelect: function(selectedDateTime) {
            $fromTime.datetimepicker("option", "maxDate",  selectedDateTime.split(" ")[0]);
        }
    });

    $("#output-excel-btn").click(function() {
        var qs = $(f).serialize();
        console.log(qs);
            url = "/business/rechargeLog?excel=1" + (qs ? "&" + qs : "");
            win = window.open(url, "_blank");
    });
});

function rechargeLogList(err, data) {
    if (err) {
        alert(err);
    } else {
        $('.tab_content').html(data);
    }
}
</script>
