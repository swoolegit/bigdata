<table cellpadding="0" cellspacing="0" width="320" class="layout">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <tr>
        <th>选择充值：</th>
        <td><label>用户名<input type="radio" name="user" value="2" checked="checked"/></label> <label>UserID<input
                    type="radio" name="user" value="1"/></label></td>
    </tr>
	<tr>
		<th>用户名或UID:</th>
		<td>
			<textarea rows="5" name="uid"></textarea>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<font color="red">使用符号,分隔用户名<br>
			充值三个用户 例: 用户名1,用户名2,用户名3
			</font>
		</td>
	</tr>
    <tr>
        <th>充值金额：</th>
        <td><input type="text" name="amount" min="100"/></td>
    </tr>
    <tr>
        <th>充值备注：</th>
        <td><select style="width:100px;" name="addType" id="addType">
                <!--
                    <option value="0" selected>管理员充值</option>
                    <option value="1">活动赠送</option>
                    <option value="2">支付宝补充</option>
                    <option value="3">网银补充</option>
                -->
                <option value="1">人工扣减</option>
                <option value="2">管理员充值</option>
                <option value="3">活动赠送</option>
                <!--
                <option value="支付宝补充">支付宝补充</option>
                <option value="微信补充">微信补充</option>
                -->
                <!--
                <option value="网银(易宝)补充">网银(易宝)补充</option>
                <option value="支付宝">支付宝</option>
                <option value="微信">微信</option>
                <option value="易宝充值">易宝充值</option>
                <option value="微信补充">微信补充</option>
                -->
            </select>
        </td>
    </tr>
	<tr>
		<th>备注：</th>
		<td><input type="text" name="comment" /></td>
	</tr>
    <tr>
        <th><span class="spn9">提示：</span></th>
        <td><span class="spn9">第一栏填写用户名 或 用户ID均可充值</td>
        </span>
    </tr>
</table>
