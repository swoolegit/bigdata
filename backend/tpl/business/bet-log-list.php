<?php
$this->getTypes();
//$this->getPlayeds();

// 帐号限制
if ($_REQUEST['username']) {
    $_REQUEST['username'] = wjStrFilter($_REQUEST['username']);
    if (!preg_match('/^\w{2,32}$/', $_REQUEST['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    $userWhere = "and b.username = '{$_REQUEST['username']}'";
}

// $hidedata=" and b.forTest=0";
// if(isset($_GET['sethide']) && $_GET['sethide']=="1")
// {
// 	$hidedata=" and b.forTest=1";
// }

//期号
if ($_REQUEST['actionNo']) {
    $_REQUEST['actionNo'] = wjStrFilter($_REQUEST['actionNo']);
    $actionNoWhere = " and b.actionNo='{$_REQUEST['actionNo']}'";
}

// 彩种限制
if ($_REQUEST['type'] = intval($_REQUEST['type'])) {
    $typeWhere = " and b.type={$_REQUEST['type']}";
}

// 时间限制
if ($_REQUEST['fromTime'] && $_REQUEST['toTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $toTime = strtotime($_REQUEST['toTime']);
    $timeWhere = "and b.actionTime between {$fromTime} and {$toTime}";
} elseif ($_REQUEST['fromTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $timeWhere = "and b.actionTime >= {$fromTime}";
} elseif ($_REQUEST['toTime']) {
    $toTime = strtotime($_REQUEST['toTime']);
    $timeWhere = "and b.actionTime < {$toTime}";
} else {
    $timeWhere = ' and b.actionTime >' . strtotime('00:00');;
}

$table="bets_repl8";
if(!empty($fromTime) || !empty($toTime))
{
    $time=time()-(7*86400);
    if($fromTime <= $time || $toTime <= $time)
    {
        $table="bets_repl";
    }
}

if ($_REQUEST['id']) {
    $_REQUEST['id'] = wjStrFilter($_REQUEST['id']);
    if (!preg_match('/^\w{4,16}$/', $_REQUEST['id'])) {
		throw new Exception('单号包含非法字符,请重新输入');
	}
    $sql = "SELECT * FROM {$this->prename}{$table} AS b WHERE b.wjorderId ='{$_REQUEST['id']}'";
    $totalSql = "SELECT SUM(actionAmount) AS TOTAL,SUM(bonus) AS zjTOTAL FROM {$this->prename}{$table} AS b WHERE b.wjorderId ='{$_REQUEST['id']}' AND b.isDelete =0 AND b.lotteryNo !=''";
} else {
	$sql = "SELECT * FROM {$this->prename}{$table} AS b WHERE 1 {$timeWhere} {$actionNoWhere} {$typeWhere} {$betTypeWhere} {$userWhere}  ORDER BY b.id DESC";
	$totalSql = "SELECT SUM(actionAmount) AS TOTAL,SUM(bonus) AS zjTOTAL FROM {$this->prename}{$table} AS b WHERE 1 {$timeWhere} {$actionNoWhere} {$typeWhere} {$betTypeWhere} {$userWhere}  AND b.isDelete =0 AND b.lotteryNo !='' ORDER BY b.id DESC";
}
$time1=time();
$data = $this->getPage($sql, $this->page, $this->pageSize);
$time2=time();

$mname = array(
    '2.000' => '元',
    '0.200' => '角',
    '0.020' => '分',
    '0.002' => '厘'
);
$page_total=0;
$page_zjtotal=0;
$prefix = "BCITY";
$prefix_len = strlen($prefix);
?>

    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
            <th>单号</th>
            <th>用户名</th>
            <th>投注时间</th>
            <th>彩种</th>
            <th>玩法</th>
            <th>期号</th>
            <th>倍数</th>
            <th>注数</th>
            <!--th>模式</th-->
            <th>开奖时间</th>
            <th>开奖号码</th>
            <th>投注号码</th>
            <th>投注金额</th>
            <th>中奖金额</th>
            <th>返点</th>
            <th>操作</th>
        </tr>
        </thead>
        <tbody id="nav01">
        <?php if ($data['data']) foreach ($data['data'] as $var) { 
            if( $var['lotteryNo']!='' && $var['isDelete']==0)
            {
                $page_total+=$var['actionAmount'];
                $page_zjtotal+=$var['bonus'];
            }
            ?>
            <tr >
                <td><a href="/business/betInfo/<?= $var['id'] ?>" button="确定:defaultCloseModal" title="投注信息"
                       width="510" target="modal"><?= $var['wjorderId'] ?></a></td>
                <td>(<?=$prefix?>) <?= str_replace($prefix,"",$var['username'])?></td>
                <td><?= date('m-d H:i', $var['actionTime']) ?></td>
                <td><?= $this->types[$var['type']]['title'] ?></td>
                <td><?= $var['actionName'] ?></td>
                <td><a href="/business/betIssueInfo?actionNo=<?= $var['actionNo'] ?>&username=<?= $var['username'] ?>&type=<?= $this->types[$var['type']]['title'] ?>" button="确定:defaultCloseModal" title="投注信息"
                       width="580" target="modal"><?= $var['actionNo'] ?></a></td>
                <td><?= $var['beiShu'] ?></td>
                <td><?= $var['actionNum'] ?></td>
                <!--td><?= $mname[$var['mode']] ?></td-->
                <td><?= date('m-d H:i', $var['kjTime']) ?></td>
                <td style="color: green"><b><?= $var['lotteryNo'] ?></b></td>
                <td ><?= $this->CsubStr($var['actionData'], 0, 20) ?></td>
                <td><?= $this->nformat($var['actionAmount'], 2) ?></td>
                <td>
                    <?php
                    if ($var['isDelete'] == 1) {
                        echo '已撤单';
                    } else {
                        if ($var['lotteryNo']) {
                            echo $this->nformat($var['bonus'], 2);
                        } else {
                            echo '未开奖';
                        }
                    }
                    ?>
                </td>
                <td><?= $var['fanDianAmount'] ?></td>
                <td>
                <!--a href="business/betInfoCheck/<?=$var['id']?>" button="取消:defaultCloseModal" title="确认" width="800" target="modal" modal="true">注单比对</a-->
                    <!--a
                        href="/business/betInfoUpdate/<?= $var['id'] ?>"
                        button="修改:dataAddCode|取消:defaultCloseModal" title="修改投注信息" width="510" target="modal"
                        modal="true">修改</a--></td>
            </tr>
        <?php } else { ?>
            <tr>
                <td colspan="15" align="center">暂时没有投注记录。</td>
            </tr>
        <?php } ?>
			<tr>
				<td colspan="15" align="center" >
					<?php
					$totalData = $this->getRow($totalSql);
                    $total = (float) $totalData['TOTAL'];
                    $zjtotal = (float) $totalData['zjTOTAL'];
                    $page_winlost=$page_zjtotal - $page_total;
                    $winlost=$zjtotal - $total;
					if ($toTime == '' && $fromTime == '') {
                        echo "本页有效投注总额：<font color=red>". $this->nformat($page_total,2) . '</font> 元 , 中奖金额 : <font color=red>'.$this->nformat($page_zjtotal,2).'</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($page_winlost,2).'</font> 元<br>';
						echo '有效投注总额：<font color=red>' . $this->nformat($total,2) . '</font> 元 , 中奖金额 : <font color=red>'.$this->nformat($zjtotal,2).'</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($winlost,2).'</font> 元';
					} else {
                        echo ($fromTime ? date("Y-m-d H:i:s",$fromTime) : '以往') . ' ~ ' . ($toTime ? date("Y-m-d H:i:s",$toTime) : '迄今') ;
                        echo "<br>本页有效投注总额：<font color=red>". $this->nformat($page_total,2) . '</font> 元 , 中奖金额 : <font color=red>'.$this->nformat($page_zjtotal,2).'</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($page_winlost,2).'</font> 元';
                        echo '<br>有效投注总额：<font color=red>' . $this->nformat($total,2) . '</font> 元 , 中奖金额 : <font color=red>'.$this->nformat($zjtotal,2).'</font> 元 , 会员输赢总计 : <font color=red>'.$this->nformat($winlost,2).'</font> 元';
					}
					?>
				</td>
			</tr>
        </tbody>
    </table>
    <footer>
        <?php

        $rel = get_class($this) . '/betLog_list-{page}?' . http_build_query($_GET, '', '&');
        $this->display('inc/page.php', 0, $data['total'], $rel, 'betLogSearchPageAction');
        ?>
    </footer>