<?php
$sql = "select * from {$this->prename}members where uid=?";
$userData = $this->getRow($sql, $args[0]);

$sp = $this->getRow("SELECT lid,usedTimes,fanDian FROM `{$this->prename}links` WHERE `uid`={$args[0]}");
if(!$sp)
{
    $para=[
        "uid"=>$args[0],
        "usedTimes"=>0,
        "fanDian"=>$userData['fanDian'],
        "type"=>1,
        "updateTime"=>$this->time,
    ];
    $this->insertRow($this->prename . 'links', $para);
    $lid = $this->lastInsertId();
    $sp=[
        "usedTimes"=>0,
        "fanDian"=>$userData['fanDian'],
        "lid"=>$lid,
    ];
}


if ($userData['parentId']) {
    $parentData = $this->getRow("select fanDian, fanDianBdw,rebate,fenHong from {$this->prename}members where uid=?", $userData['parentId']);
} else {
    $this->getSystemSettings();
    $parentData['fanDian'] = $this->settings['fanDianMax'];
    $parentData['fanDianBdw'] = $this->settings['fanDianBdwMax'];
    $parentData['fenHong'] = $this->settings['fenHongMax'];

}
$sonFanDianMax = $this->getRow("select max(fanDian) sonFanDian, max(fanDianBdw) sonFanDianBdw, max(fenHong) sonFenHong from {$this->prename}members where isDelete=0 and parentId=?", $args[0]);
?>

<div>
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <form action="member/userUped" target="ajax" method="post" call="userDataSubmitCode"
          onajax="userDataBeforeSubmitCode" dataType="html">
        <input type="hidden" name="uid" value="<?= $args[0] ?>"/>
        <!--uid isDelete  enable  parentId 会员从属关系 parents 上级系列 admin  username  coinPassword  type 是否代理：0会员，1代理 subCount 人数配额 sex  regIP  regTime  updateTime  province  city  address  password  qq  msn  mobile  email  idCard 身分证号码 grade 等级 score 积分 coin 个人财产 fcoin 冻结资产 fanDian 用户设置的返点数 fanDianBdw 不定位返点 safepwd 交易密码，请区别于登录密码 safeEmail 密保邮箱，与邮箱分开 -->
        <table cellpadding="2" cellspacing="2" class="popupModal">
            <tr>
                <td class="title" width="180">上级关系：</td>
                <td><?= $userData['parents'] ? implode('> ', $this->getCol("select username from {$this->prename}members where uid in ({$userData['parents']})")):'' ?></td>
            </tr>
            <tr>
                <td class="title" width="180">用户名：</td>
                <td>
                    <lable><?= $userData['username'] ?></lable>
                </td>
            </tr>
            <tr>
                <td class="title">密码：</td>
                <td><input type="text" name="password" value=""/>&nbsp;<span class="spn9">置空为不修改</span></td>
            </tr>
            <tr>
                <td class="title">资金密码：</td>
                <td><input type="text" name="coinPassword" value=""/>&nbsp;<span class="spn9">置空为不修改</span></td>
            </tr>
            <tr>
                <td class="title" width="180">下级注册网址：</td>
                <td>
                    <lable><?=QR_URL."/spreg/".$sp['lid']?></lable>
                </td>
            </tr>
            <tr>
                <td class="title" width="180">下级注册人数：</td>
                <td>
                    <lable><?=$sp['usedTimes']?></lable>
                </td>
            </tr>
            <!--20161120 移除 george
            <tr>
                <td class="title">可用金额：</td>
                <td><input type="text" name="coin" value="<?= $userData['coin'] ?>"/>&nbsp;</td>
            </tr>
            -->
            <tr>
                <td class="title">QQ：</td>
                <td><input type="text" name="qq" value="<?= $userData['qq'] ?>"/>&nbsp;</td>
            </tr>
            <tr>
                <td class="title">手机号：</td>
                <td><input type="text" name="phone" value="<?= $userData['phone'] ?>"/>&nbsp;</td>
            </tr>
            <tr>
                <td class="title">LINE：</td>
                <td><input type="text" name="line" value="<?= $userData['line'] ?>"/>&nbsp;</td>
            </tr>
            <tr>
                <td class="title">邮箱：</td>
                <td><input type="text" name="email" value="<?= $userData['email'] ?>"/>&nbsp;</td>
            </tr>
            <tr>
                <td class="title">提款限额：</td>
                <td><input type="text" name="cashLimit" value="<?= $userData['cashLimit'] ?>"/><span class="spn9">提款须打码量</span></td>
            </tr>
            <tr>
                <td class="title">积分：</td>
                <td><input type="text" name="score" value="<?= $userData['score'] ?>"/>&nbsp;</td>
            </tr>
            <tr>
                <td class="title">等级：</td>
                <td><input type="text" name="grade" value="<?= $userData['grade'] ?>"/>&nbsp;</td>
            </tr>
            <tr>
                <td class="title">返点：</td>
                <td><input type="text" name="fanDian" value="<?= $userData['fanDian'] ?>"
                           max="<?= $parentData['fanDian'] ?>" min="<?= $sonFanDianMax['sonFanDian'] ?>"
                           fanDianDiff=<?= $this->settings['fanDianDiff'] ?>>%&nbsp;<span
                        style="color:#999"><?= $this->ifs($sonFanDianMax['sonFanDian'], '0') ?>
                        -<?= $parentData['fanDian'] ?>%</span></td>
            </tr>
            <tr>
                <td class="title">退水：</td>
                <td><input type="text" name="rebate" value="<?= $userData['rebate'] ?>" />%
                </td>
            </tr>
            <tr>
                <td class="title">分红：</td>
                <td><input type="text" name="fenHong" value="<?= $userData['fenHong'] ?>"
                           max="<?= $parentData['fenHong'] ?>" min="<?= $sonFanDianMax['sonFenHong'] ?>"
                            ?>%&nbsp;<span
                        style="color:#999"><?= $this->ifs($sonFanDianMax['sonFenHong'], '0') ?>
                        -<?= $parentData['fenHong'] ?>%</span></td>
            </tr>
            <tr>
                <td class="title">单期注额限制：</td>
                <td><input type="text" name="limitPreActionNo" value="<?= $userData['limitPreActionNo'] ?>"/>&nbsp;<span
                        class="spn9">小于等于0，无限制</span></td>
            </tr>

            <tr>
                <td class="title">重置银行：</td>
                <td><label><input type="radio" name="resetBank" value="1"/>重置</label> <label><input type="radio" name="resetBank" value="" checked />不重置</label></td>
            </tr>
            <tr>
                <td class="title">账号冻结：</td>
                <td><label><input type="radio" value="1"
                                  name="enable" <?php if ($userData['enable']) echo 'checked="checked"' ?>/>开启</label>&nbsp;&nbsp;<label><input
                            type="radio" value="0"
                            name="enable" <?php if (!$userData['enable']) echo 'checked="checked"' ?>/>冻结</label></td>
            </tr>
            <tr>
                <td class="title">提现权限：</td>
                <td><label><input type="radio" value="1"
                                  name="cashEnable" <?php if ($userData['cashEnable']) echo 'checked="checked"' ?>/>开启</label>&nbsp;&nbsp;<label><input
                            type="radio" value="0"
                            name="cashEnable" <?php if (!$userData['cashEnable']) echo 'checked="checked"' ?>/>冻结</label></td>
            </tr>
			
			<tr>
                <td class="title">注册IP：</td>
                <td><label><?= long2ip($userData['regIP']) ?></label></td>
            </tr>
            <tr>
                <td class="title">加入时间：</td>
                <td><?= date("Y-m-d H:i:s", $userData['regTime']) ?></td>
            </tr>
            <tr>
                <td class="title">备注：</td>
                <td><textarea name="info"><?=$userData['info']?></textarea></td>
            </tr>
        </table>
    </form>
</div>