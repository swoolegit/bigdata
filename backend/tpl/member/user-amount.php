<?php
/*
$sql = "select u.username, u.coin, u.uid, u.parentId, sum(b.mode * b.beiShu * b.actionNum) betAmount, sum(b.bonus) zjAmount,(select sum(c.amount) from {$this->prename}member_cash c where c.`uid`=u.`uid` and c.state=0) cashAmount,(select sum(r.amount) from {$this->prename}member_recharge r where r.`uid`=u.`uid` and r.state in(1,2,9)) rechargeAmount, (select sum(l.coin) from {$this->prename}coin_log_repl l where l.`uid`=u.`uid` and l.liqType in(50,51,52,53)) brokerageAmount from {$this->prename}members u left join {$this->prename}bets_repl b on u.uid=b.uid and b.isDelete=0 where u.uid=?";
$var = $this->getRow($sql, $args[0]);

$var['fanDianAmount'] = $this->getValue("select sum(coin) from {$this->prename}coin_log_repl where uid=? and liqType between 2 and 3", $args[0]);
$var['rechargeAmount'] = $this->getValue("select sum(coin) from {$this->prename}coin_log_repl where uid=? and liqType=1", $args[0]);
$var['cashAmount'] = $this->getValue("select sum(abs(fcoin)) from {$this->prename}coin_log_repl where uid=? and liqType=107", $args[0]);
 * 
 */
//print_r($parentData);
$sql="select
			u.username,u.coin,
			IFNULL(sum(r.real_bet),0) as RbetAmount,
			IFNULL(sum(r.zj),0) as zjAmount,
			IFNULL(sum(r.fandian),0) as fanDianAmount,
			IFNULL(sum(r.broker),0) as brokerageAmount,
			IFNULL(sum(r.gongzi),0) as gongziAmount,
			IFNULL(sum(r.bonus),0) as bonusAmount,
			IFNULL(sum(r.cancelOrder),0) as cancelOrderAmount,
			IFNULL(sum(r.recharge),0) as rechargeAmount,
            IFNULL(sum(r.cash),0) as cashAmount,	
            IFNULL(sum(r.rebate),0) as rebate	
			from {$this->prename}members u , {$this->prename}member_report r 
			where u.uid=r.uid and u.uid={$args[0]}		
";
$var = $this->getRow($sql);
?>
<div>
    <table cellpadding="2" cellspacing="2" class="popupModal">
        <input type="hidden" value="<?= $this->user['username'] ?>"/>
        <tr>
            <td class="title" width="180">用户名：</td>
            <td><input type="text" readonly="readonly" value="<?= $var['username'] ?>"/></td>
        </tr>
	        <td class="title">有效投注额</td>
	        <td><input type="text" readonly="readonly" value="<?= $this->ifs($this->nformat($var['RbetAmount']), '--') ?>"/></td>
        </tr>        
        <tr>
            <td class="title">中奖总额</td>
            <td><input type="text" readonly="readonly" value="<?= $this->ifs($this->nformat($var['zjAmount']), '--') ?>"/></td>
        </tr>
        <tr>
            <td class="title">总返点</td>
            <td><input type="text" readonly="readonly" value="<?= $this->ifs($this->nformat($var['fanDianAmount']), '--') ?>"/></td>
        </tr>
        <tr>
            <td class="title">总分红</td>
            <td><input type="text" readonly="readonly" value="<?= $this->ifs($this->nformat($var['bonusAmount']), '--') ?>"/></td>
        </tr>
        <tr>
            <td class="title">总退水</td>
            <td><input type="text" readonly="readonly" value="<?= $this->ifs($this->nformat($var['rebate']), '--') ?>"/></td>
        </tr>        
        <tr>
            <td class="title">佣金,活动</td>
            <td><input type="text" readonly="readonly" value="<?= $this->ifs($this->nformat($var['brokerageAmount']), '--') ?>"/></td>
        </tr>
        <tr>
            <td class="title">充值</td>
            <td><input type="text" readonly="readonly" value="<?= $this->ifs($this->nformat($var['rechargeAmount']), '--') ?>"/></td>
        </tr>
        <tr>
            <td class="title">提现</td>
            <td><input type="text" readonly="readonly" value="<?= $this->ifs($this->nformat($var['cashAmount']), '--') ?>"/></td>
        </tr>
        <tr>
            <td class="title">余额</td>
            <td><input type="text" readonly="readonly" value="<?= $this->ifs($this->nformat($var['coin']), '--') ?>"/></td>
        </tr>
        <tr>
            <td class="title">个人总结算</td>
            <td><input type="text" readonly="readonly"
                       value="<?= $this->ifs($this->nformat($var['zjAmount'] - $var['RbetAmount'] + $var['fanDianAmount'] + $var['brokerageAmount']+$var['rebate']+$var['bonusAmount']), '--') ?>"/>
            </td>
        </tr>
    </table>
</div>