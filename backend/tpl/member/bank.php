<?php
$para = $_GET;

$where_cond = array();
// 用户限制
if ($para['username']) {
    $para['username'] = wjStrFilter($para['username']);
    if (!preg_match('/^\w{4,16}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    $where_cond[] = "(u.username LIKE '%{$para['username']}%')";
}
if ($para['bank_account']) {
    $where_cond[] = "(mb.account LIKE '%" . $para['bank_account'] . "%')";
}
if ($para['account_name']) {
    $where_cond[] = "(mb.username LIKE '%" . $para['account_name'] . "%')";
}
$where_cond = $where_cond ? 'WHERE ' . implode(' AND ', $where_cond) : '';

$sql =
    "SELECT
        u.uid,
        u.username AS u_username,
        mb.account AS bank_account,
        mb.username AS bank_username,
        mb.countname
    FROM {$this->prename}member_bank AS mb
        LEFT JOIN {$this->prename}members AS u  ON mb.uid = u.uid
    {$where_cond}
    ORDER BY u.uid DESC";
$data = $this->getPage($sql, $this->page, $this->pageSize);
?>

<article class="module width_full">
    <header>
        <h3 class="tabs_involved">银行信息
            <form action="member/bank" target="ajax" dataType="html" class="submit_link wz" call="defaultSearch">
                银行帐号：<input type="text" class="alt_btn" name="bank_account" placeholder="银行帐号"/>　
                开户姓名：<input type="text" class="alt_btn" name="account_name" placeholder="开户姓名"/>　
                用户名：<input type="text" class="alt_btn" name="username" placeholder="用户名"/>　
                <input type="submit" value="查找" class="alt_btn">
            </form>
        </h3>
    </header>
    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
            <td>会员编号</td>
            <td>用户名</td>
            <td>银行名称</td>
            <td>银行账号</td>
            <td>开户姓名</td>
        </tr>
        </thead>
        <tbody id="nav01">
        <?php if ($data['data']) foreach ($data['data'] as $var) { ?>
            <tr>
                <td><?= $var['uid'] ?></td>
                <td><?= $var['u_username'] ?></td>
                <td><?= $var['countname'] ?></td>
                <td><?= $var['bank_account'] ?></td>
                <td><?= $var['bank_username'] ?></td>                
            </tr>
        <?php } ?>

        </tbody>
    </table>
    <footer>
        <?php
        $rel = get_class($this) . '/bank-{page}?' . http_build_query($_GET, '', '&');
        $this->display('inc/page.php', 0, $data['total'], $rel, 'defaultReplacePageAction');
        ?>
    </footer>
</article>
<script type="text/javascript">
    ghhs("nav01", "tr");
</script>