<div>
    <?php
    $uid=intval($args[0]);
    $sql="select * from {$this->prename}member_bank where uid={$uid} order by id desc";
    $banks = $this->getRows($sql);
    ?>
    <table class="tablesorter" cellspacing="0" width="100%">
        <thead>
        <tr style="text-indent: 0px;">
            <td>帐户名</td>
            <td>开户行</td>
            <td>帐号</td>
            <td>绑定日期</td>
            <td>状态</td>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach($banks as $k=>$v)
        {
        ?>
        <tr>
            <td><?=$v['username']?></td>
            <td><?=$v['countname']?></td>
            <td><?=$v['account']?></td>
            <td><?=date('Y-m-d H:i:s',$v['bdtime'])?></td>
            <td><?php
                if($v['enable'])
                {
                    echo "开启";
                }else
                {
                    echo "关闭";
                }
            ?></td>
        </tr>
        <?php
        }
        ?>
		</tbody>
    </table>
</div>
