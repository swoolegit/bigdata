<?php
$para = $_GET;
// 用户名限制
if ($para['username']) {
    $para['username'] = wjStrFilter($para['username']);
    if (!preg_match('/^\w{4,16}$/', $para['username'])) throw new Exception('用户名包含非法字符,请重新输入');
    $userWhere = " and username='{$para['username']}'";
}
// IP限制
if ($para['ip'] = ip2long($para['ip'])) {
    $para['ip'] = wjStrFilter($para['ip']);
    $ipWhere = " and loginIp='{$para['ip']}'";
}

// 时间限制
if ($_REQUEST['fromTime'] && $_REQUEST['toTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $toTime = strtotime($_REQUEST['toTime']) + 24 * 3600;
    $timeWhere = "and loginTime between $fromTime and $toTime";
} elseif ($_REQUEST['fromTime']) {
    $fromTime = strtotime($_REQUEST['fromTime']);
    $timeWhere = "and loginTime>=$fromTime";
} elseif ($_REQUEST['toTime']) {
    $toTime = strtotime($_REQUEST['toTime']) + 24 * 3600;
    $timeWhere = "and loginTime<$fromTime";
}

// 栏位排序
switch ($para['order_by']) {
    case 'id':
        $order_by = 'id';
        break;
    case 'username':
        $order_by = 'username';
        break;
    case 'loginIP':
        $order_by = 'loginIP';
        break;
    default:
        $order_by = 'loginTime';
        break;
}
if ($para['sort'] == 'ASC') {
    $sort = 'ASC';
} else {
    $sort = 'DESC';
}

$sql =
    "SELECT *
    FROM {$this->prename}member_session
    WHERE
        1
        {$timeWhere}
        {$userWhere}
        {$ipWhere}
    ORDER BY {$order_by} {$sort}";
$data = $this->getPage($sql, $this->page, $this->pageSize);
?>


<article class="module width_full">
    <input type="hidden" value="<?= $this->user['username'] ?>" />
    <header>
        <h3 class="tabs_involved">
            登录日志
            <form id="query-form" class="submit_link wz" action="member/loginLog" target="ajax" dataType="html" call="defaultSearch">
                会员名：<input type="text" class="alt_btn" style="width:100px;" name="username" />　
                IP：<input type="text" class="alt_btn" style="width:100px;" name="ip" />　
                时间：从 <input type="date" class="alt_btn" name="fromTime" /> 到 <input type="date" class="alt_btn" name="toTime" />　
                <input type="submit" id="qf-submit-btn" class="alt_btn" value="查找" />

                <input type="hidden" name="order_by" value="<?= $order_by ?>" />
                <input type="hidden" name="sort" value="<?= $sort ?>" />
            </form>
        </h3>
    </header>
    <table class="tablesorter" cellspacing="0">
        <thead>
        <tr>
            <td class="clickable sort<?php if ($order_by == 'id') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('id')">ID</td>
            <td class="clickable sort<?php if ($order_by == 'username') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('username')">用户名</td>
            <td class="clickable sort<?php if ($order_by == 'loginIP') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('loginIP')">IP</td>
            <td>浏览器</td>
            <td>操作系统</td>
            <td>移动设备</td>
            <td class="clickable sort<?php if ($order_by == 'loginTime') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('loginTime')">登录时间</td>
            <td>操作</td>
        </tr>
        </thead>
        <tbody id="nav01">
        <?php if ($data['data']) foreach ($data['data'] as $var) { ?>
            <tr>
                <td><?= $var['id'] ?></td>
                <td><?= $var['username'] ?></td>
                <td><?= long2ip($var['loginIP']) ?></td>
                <td><?= $var['browser'] ?></td>
                <td><?= $var['os'] ?></td>
                <td><?= $this->iff($var['isMobileDevices'], '是', '否') ?></td>
                <td><?= date('Y-m-d H:i:s', $var['loginTime']) ?></td>
                <td><a href="member/loginLog?username=<?= $var['username'] ?>">只看此人</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <footer>
        <?php
        $rel = get_class($this) . '/loginLog-{page}?' . http_build_query($_GET, '', '&');
        $this->display('inc/page.php', 0, $data['total'], $rel, 'defaultReplacePageAction');
        ?>
    </footer>
</article>


<script type="text/javascript">
ghhs("nav01", "tr");

function orderBy(column) {
    var f = document.getElementById("query-form");
    if (f.order_by.value == column) {
        f.sort.value = (f.sort.value == "ASC" ? "DESC" : "ASC");
    } else {
        f.order_by.value = column;
        f.sort.value = "DESC";
    }
    document.getElementById("qf-submit-btn").click();
}
</script>
