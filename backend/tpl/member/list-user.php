<?php
$where_cond = array();
$binds = array();
// 用户名限制
if (isset($_GET['username']) && $_GET['username']!=='') {
    $_GET['username'] = wjStrFilter($_GET['username']);
    if (! preg_match('/^\w{2,64}$/', $_GET['username'])) {
        throw new Exception('用户名包含非法字符,请重新输入');
    }
    $where_cond[] = "u.username = '{$_GET['username']}'";
    $binds['like_username'] = "'{$_GET['username']}'";
	$_GET['type']=0;
}

// QQ 限制
if (isset($_GET['userqq']) && $_GET['userqq']!=='') {
    if (!ctype_digit($_GET['userqq'])) {
        throw new Exception('QQ包含非法字符');
    }
    if (strlen($_GET['userqq']) < 4 || strlen($_GET['userqq']) > 12) {
        throw new Exception('QQ号为4-12位,请重新输入');
    }
    $where_cond[] = "qq LIKE :like_qq";
    $binds['like_qq'] = "%{$_GET['userqq']}%";
}

// 从属关系限制
if (isset($_GET['type'])) {
    switch ($_GET['type']) {
        case 1:
            //我自己
            $where_cond[] = "uid =:uid";
            $binds['uid'] = $this->user['uid'];
            break;
        case 2:
            //直属下线
            $where_cond[] = "parentId =:parentId";
            $binds['parentId'] = empty($_GET['uid']) ? $this->user['uid'] : $_GET['uid'];
            break;
        case 3:
            // 所有下级
            $where_cond[] = "CONCAT(',', parents, ',') LIKE :like_parents AND uid !=:uid";
            $binds['like_parents'] = "%,{$this->user['uid']},%";
            $binds['uid'] = $this->user['uid'];
            break;
        default:
            // 所有人
            //$sql.=" and concat(',', parents, ',') like '%,{$this->user['uid']},%'";
            break;
    }
}else
{
    // 層級顯示
	// $where_cond[] = "parentId = :parentId";
	// $binds['parentId'] = 0;
}

switch ((isset($_GET['sort']) ? $_GET['sort'] : '')) {
    case 'ASC':
        $sort = 'ASC';
        break;
    default:
        $sort = 'DESC';
}

$order_by = empty($_GET['order_by']) ? 'type' : htmlspecialchars($_GET['order_by']);
switch ($order_by) {
    case 'coin':
        $order_by_clause = "ORDER BY coin {$sort}"; break;
    case 'score':
        $order_by_clause = "ORDER BY score {$sort}"; break;
    case 'grade':
        $order_by_clause = "ORDER BY grade {$sort}"; break;
    case 'zjAmount':
        $order_by_clause = "ORDER BY zjAmount {$sort}"; break;
    case 'fanDianAmount':
        $order_by_clause = "ORDER BY fanDianAmount {$sort}"; break;
    case 'betAmount':
        $order_by_clause = "ORDER BY betAmount {$sort}"; break;
    default:    // 默认 (代理 > 会员 顺序)
        $order_by_clause = "ORDER BY u.type {$sort}, u.uid ASC"; break;
}
$where_cond = $where_cond ? ' where ' . implode(' AND ', $where_cond) : '';
$sql="select u.username,u.coin,u.uid,u.parentId,u.parents,u.score,u.grade,u.fenHong,u.rebate,u.fanDian,u.type,info,delay,
				IFNULL(sum(r.real_bet),0) as betAmount,
				IFNULL(sum(r.zj),0) as zjAmount,
				IFNULL(sum(r.fandian),0) as fanDianAmount,
				IFNULL(sum(r.broker),0) as brokerageAmount,
				IFNULL(sum(r.gongzi),0) as gongziAmount,
				IFNULL(sum(r.bonus),0) as bonusAmount,
                IFNULL(sum(r.cancelOrder),0) as cancelOrderAmount,
                IFNULL(sum(r.rebate),0) as rebateAmount
		from {$this->prename}members u LEFT JOIN {$this->prename}member_report r on u.uid=r.uid
		 {$where_cond} group by u.uid {$order_by_clause}
		";
$data = $this->getPage($sql, $this->page, $this->pageSize, $binds);
if($data['data'])
{
    foreach($data['data'] as $k=>$v)
    {
        $pid[]=$v['parentId'];
    }
    $sql="select uid,parentId,username from {$this->prename}members where uid in (".implode(",",$pid).")";
    $pdata = $this->getRows($sql);
    $pid=[];
    foreach($pdata as $k=>$v)
    {
        $pid[$v['uid']]=[
            'uid'=>$v['uid'],
            'username'=>$v['username'],
        ];
    }
}
if(isset($_GET['uid']) && $_GET['uid']!=='')
{
	$tree=$this->get_tree($_GET['uid']);
}
if(isset($_GET['username']) && $_GET['username']!=='')
{
	$sql="select uid from {$this->prename}members u where username='".$_GET['username']."'";
	$u=$this->getRow($sql);
	$tree=$this->get_tree($u['uid']);
}
?>
<div style="padding: 5px;">
	<a href="member/index">从属关系</a> : <?=$tree?>
</div>
<table class="table-sorter text-center" cellspacing="0">
    <thead>
        <tr>
            <th>上級</th>
            <th>用户名</th>
            <!--th>ID</th-->
            <th class="clickable sort<?php if ($order_by == 'type') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" data-column="type">类型</th>
            <th class="clickable sort<?php if ($order_by == 'coin') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" data-column="coin">个人财产</th>
            <th>
                <span class="clickable sort<?php if ($order_by == 'score') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" data-column="score">积分</span> |
                <span class="clickable sort<?php if ($order_by == 'grade') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" data-column="grade">等级</span>
            </th>
            <th>
                <span class="clickable sort<?php if ($order_by == 'zjAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" data-column="zjAmount">中奖</span> |
                <span class="clickable sort<?php if ($order_by == 'fanDianAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" data-column="fanDianAmount">返点</span>
            </th>
            <th>
                <span class="clickable sort<?php if ($order_by == 'betAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" data-column="betAmount">有效投注</span> | 盈亏
            </th>
            <th>返点</th>
            <th>分红</th>
            <th>退水</th>
            <th>状态</th>
            <th>提早封盤</th>
            <th>最后登录</th>
            <th>登录IP</th>
            <th>操作</th>
            <th>备注</th>
        </tr>
    </thead>
    <tbody id="nav01">
        <?php
        if ($data['data']) {
            foreach ($data['data'] as $v) {
                $loss = $v['zjAmount'] - $v['betAmount'] + $v['fanDianAmount'] + $v['brokerageAmount'] + $v['gongziAmount'] + $v['bonusAmount']+ $v['rebateAmount'];
        ?>
                <tr>
                    <td><a href="member/userUp/<?= $pid[$v['parentId']]['uid'] ?>" target="modal" width="520" title="编辑用户" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">
                        <?= $pid[$v['parentId']]['username'] ?>
                        </a>
                    </td>
                    <td><?= $v['username'] ?></td>
                    <!--td><?= $v['uid'] ?></td-->
                    <td><?php if ($v['type']) {echo '代理';} else {echo '会员';} ?>
                    	<?php
                    	if ($v['forTest']) {echo '(测试号)';}
                    	?>
                    </td>
                    <td class="text-right"><?= $this->nformat($v['coin']) ?></td>
                    <td><?= $v['score'] ?><span class='spn10'> | </span><?= $v['grade'] ?></td>
                    <td>
                        <?= ($v['zjAmount']!=0 ? $this->nformat($v['zjAmount']) : '--') ?><span class='spn10'> | </span>
                        <?= ($v['fanDianAmount']!=0 ? $this->nformat($v['fanDianAmount']) : '--') ?>
                    </td>
                    <td>
                        <?= ($v['betAmount']!=0 ? $this->nformat($v['betAmount']) : '--') ?><span class='spn10'> | </span>
                        <span class="<?php if ($loss < 0) {echo 'red';} ?>"><?= ($loss!=0 ? $this->nformat($loss) : '--') ?></span>
                    </td>
                    <td><?= $v['fanDian'] ?>%</td>
                    <td><?= $v['fenHong'] ?>%</td>
                    <td><?= $v['rebate'] ?>%</td>
                    <?php
                    if ($v['isDelete'] == 0) {    // 是否删除
                        $check_online_sql = "SELECT * FROM {$this->prename}member_session WHERE uid =? ORDER BY id DESC LIMIT 1";
                        $login = $this->getRow($check_online_sql, $v['uid']);
                    ?>
                        <td><?= $this->iff($login['isOnLine'] && ceil(strtotime(date('Y-m-d H:i:s', time())) - strtotime(date('Y-m-d H:i:s', $login['accessTime']))) < $GLOBALS['conf']['member']['sessionTime'], '<font color="#FF0000">在线</font>', '离线') ?></td>
                        <td><?php
                         if($v['delay']==1)
                         {
                             echo "開啟";
                         }else
                         {
                            echo "關閉";
                         }
                         ?></td>
                        <td><?= $this->iff($login['loginTime'], date('m-d H:i', $login['loginTime']), '--') ?></td>
                        <td><?= long2ip($login['loginIP']) ?></td>
                        <td>
                            <a href="member/userAmount/<?= $v['uid'] ?>" target="modal" width="420" title="用户统计" modal="true">统计</a> |
                            <a href="business/coinLog?username=<?= $v['username'] ?>">帐变</a> |
                            <a href="member/userUp/<?= $v['uid'] ?>" target="modal" width="520" title="编辑用户" modal="true" button="确定:dataAddCode|取消:defaultCloseModal">编辑</a> |
                            <a href="member/index?type=2&uid=<?= $v['uid'] ?>">下级</a> |
                            <!--a href="member/delete/<?= $v['uid'] ?>" target="modal" width="320" title="删除用户" modal="true" button="确定:dataAddCode">删</a> |-->
                            <!--a href="member/removeEdge/<?= $v['uid'] ?>" target="modal" width="320" title="将下线转移到其他用户名下" modal="true" button="确定:dataAddCode">转</a> |!-->
                            <a href="system/T/<?= $v['uid'] ?>" method="post" target="ajax" call="TSuccess" title="是否踢该会员下线?">踢下线</a> |
							<a href="member/bank_modal/<?= $v['uid'] ?>" target="modal" width="680" title="银行信息" modal="true" >银行信息</a> |
							<a href="member/delay/<?= $v['uid'] ?>" target="ajax" call="reload" title="是否設定提早封盤"  >提早封盤</a>
						</td>
                    <?php
                    } else {
                    ?>
                        <td>已删除</td>
                        <td><?= $v['updateTime'] ?></td>
                        <td>
                            <a href="member/userAmount/<?= $v['uid'] ?>" target="modal" width="420" title="用户统计" modal="true">统计</a> |
                            <a href="business/coinLog?username=<?= $v['username'] ?>">帐变</a> |
                            <a href="Member/index?type=2&uid=<?= $v['uid'] ?>" call="">下级</a>
                        </td>
                    <?php
                    }
                    ?>
                    <td><?= $v['info'] ?></td>
                </tr>
        <?php
            }
        } else {
        ?>
            <tr>
                <td colspan="16">没用用户或该用户还没有下级</td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>

<footer>
    <?php
    $rel = get_class($this) . '/index-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $data['total'], $rel, 'defaultReplacePageAction');
    ?>
</footer>

<script type="text/javascript">
$(function() {
    $(".clickable.sort").click(function() {
        var f = document.getElementById("query-form");
        if (! f) {
            return;
        }
        var column = this.getAttribute("data-column");
        if (f.order_by.value == column) {
            f.sort.value = (f.sort.value == "ASC" ? "DESC" : "ASC");
        } else {
            f.order_by.value = column;
            f.sort.value = "DESC";
        }
        $("#submit_btn").click();
    });
});
// function reload()
// {
//     $("#submit_btn").click();
// }
</script>
