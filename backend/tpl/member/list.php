<?php
if (isset($_GET['excel']) && $_GET['excel'] == '1') {
    $date = date("Ymd");
    header('Content-type:application/vnd.ms-excel');  //声明网页格式
    header('Content-Disposition: attachment; filename=' . $date . '_member.xls');  //设置文件名称

    $sql = "SELECT * FROM `{$this->prename}members` ORDER BY `uid` DESC";
    $data = $this->getRows($sql);
    $n = 1;
    $csv = $csvData = '';
    $csvHTML = '<tr>';
    $csvHTMLData = '';
    foreach ($data as $arr) {
        $csvHTMLData .= '<tr>';
        foreach ($arr as $key => $value) {
            if ($n == 1) {
                $key = str_replace(',', ' ', $key);
                $csv .= $key . ',';
                $csvHTML .= "<th>{$key}</th>";
            }
            $value = str_replace(',', ' ', $value);
            $csvData .= '="' . $value . '",';
            $csvHTMLData .= "<td>{$value}</td>";
        }
        if ($n == 1) {
            $csv .= "\r\n";
            $csvHTML .= '</tr>';
        }
        $csvData .= "\r\n";
        $csvHTMLData .= '</tr>';
        $n++;
    }
    $csv .= $csvData;
    $csv = mb_convert_encoding($csv, 'BIG-5', 'UTF-8');
?>
    <html>
        <head>
            <meta charset="utf-8">
        </head>
        <body>
            <table border="1">
                <?php echo $csvHTML; ?>
                <?php echo $csvHTMLData; ?>
            </table>
        </body>
    </html>
<?php
    // 输出 excel 程序应该执行到此为止而已
    exit;
}
$username = isset($_GET['username']) ? htmlspecialchars($_GET['username']) : '';
$userqq = isset($_GET['userqq']) ? htmlspecialchars($_GET['userqq']) : '';
$order_by = isset($_GET['order_by']) ? htmlspecialchars($_GET['order_by']) : 'type';
$sort = isset($_GET['sort']) ? htmlspecialchars($_GET['sort']) : 'DESC';
?>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved">用户列表
            <button type="button" id="output-excel-btn">Excel</button>
            <form id="query-form" class="submit_link wz" action="Member/listUser" target="ajax" dataType="html"  call="memberUserList">
			用户名：<input type="text" id="username" name="username" style="width:100px;" autocomplete="off" placeholder="用户名" value="<?php echo $username ?>" />
                <div class="auto-screening auto-hidden mbox" id="autoScreening"></div>
                <input type="submit" id="submit_btn" value="查找" class="alt_btn">
                <input type="hidden" name="order_by" value="<?php echo $order_by ?>" />
                <input type="hidden" name="sort" value="<?php echo $sort ?>" />
            </form>
        </h3>
    </header>
    <div class="tab_content">
        <?php $this->display('member/list-user.php') ?>
    </div>
</article>


<script type="text/javascript">
$(function() {
    $("#output-excel-btn").click(function() {
        var f = document.getElementById("query-form"),
            qs = $(f).serialize(),
            url = "member/index?excel=1" + (qs ? "&" + qs : ""),
            win = window.open(url, "_blank");
    });
});
function memberUserList(err, data){
    if (err) {
        alert(err);
    } else {
        $(".tab_content").html(data);
    }
}

</script>
