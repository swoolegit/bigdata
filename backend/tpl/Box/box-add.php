<script type="text/javascript">
    function boxadd(err, data) {
        if (err) {
            error(err);
        } else {
            success(data);
        }
    }
</script>
<article class="module width_full">
    <input type="hidden" value="<?= $this->user['username'] ?>"/>
    <header><h3 class="tabs_involved">发消息</h3></header>
    <div class="writeinfo2">
        <form action="box/dowrite" method="post"  target="ajax" dataType="html" onajax="boxBeforadd" call="boxadd">
            <div class="writeinfo2_l">
            	<table>
            		<tr>
            			<td>收件人：</td>
            			<td>
            				<div style="line-height: 32px;">
	                    	<select name="touser">
	                    		<option value="1">部分会员</option>
	                    		<option value="0">所有会员</option>
	                    	</select> (选择部分会员,请在下方输入会员名,若发送多位会员请用,符号分隔,例: 会员1,会员2,会员3)
	                    	</div>
	                    	<div>
	                    	<textarea rows="2" cols="30" name="userlist"></textarea>
	                    	</div>            				
            			</td>
            		</tr>
            		<tr>
            			<td>主题：</td>
            			<td><input name="title" value="" class="txt" type="text"></td>
            		</tr>
            		<tr>
            			<td>内容：</td>
            			<td><textarea name="content" class="txt2" rows="8" cols="50"></textarea></td>
            		</tr>
            		<tr>
            			<td></td>
            			<td><input class="bnt" value="发 送" type="submit"></td>
            		</tr>
            		
            	</table>
            </div>
            <div class="clear"></div>
        </form>
    </div>
    <div class="clear"></div>
</article>
