<?php

$where_cond = array();
$binds = array();

// 时间限制
$fromTime = empty($_GET['fromTime']) ? '' : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? '' : strtotime($_GET['toTime']);
if ($fromTime && $toTime) {
    $recharge_time_cond = "AND rechargeTime BETWEEN {$fromTime} AND {$toTime}";
    $cash_time_cond = "AND cashTime BETWEEN {$fromTime} AND {$toTime}";
    $binds['fromTime'] = $fromTime;
    $binds['toTime'] = $toTime;
} else if ($fromTime) {
    $recharge_time_cond = "AND rechargeTime >= {$fromTime}";
    $cash_time_cond = "AND cashTime >= {$fromTime}";
    $binds['fromTime'] = $fromTime;
} else if ($toTime) {
    $recharge_time_cond = "AND rechargeTime < {$toTime}";
    $cash_time_cond = "AND cashTime < {$toTime}";
    $binds['toTime'] = $toTime;
} else {
    $recharge_time_cond = '';
    $cash_time_cond = '';
}
$sethide=" where u.fortest=0";
if(isset($_GET['sethide']) && $_GET['sethide']=="1")
{
	$sethide=" where u.forTest=1";
}

// 充值状态限制
switch ((isset($_GET['recharge_state']) ? $_GET['recharge_state'] : '')) {
    case 'recharged':
        $having_clause = "HAVING LEFT(rechargeTotalInfo, 2) !='0|'";
        break;
    case 'unrecharge':
        $having_clause = "HAVING LEFT(rechargeTotalInfo, 2) ='0|'";
        break;
    default:
        $having_clause = '';
}

$sql =
    "SELECT
        u.uid,
        u.username,                                                 # 用户名
        u.regTime,                                                  # 注册日期
        (
            SELECT
                CONCAT(
                    COUNT(*), '|',
                    IFNULL(SUM(rechargeAmount), 0)
                )
            FROM {$this->prename}member_recharge
            WHERE
                uid =u.uid
                {$recharge_time_cond}
                AND rechargeAmount > 0
                AND isDelete =0
        ) AS rechargeInfo,                                          # 充值资讯 (次数|金额)
        (
            SELECT
                CONCAT(
                    COUNT(*), '|',
                    IFNULL(SUM(rechargeAmount), 0), '|',
                    IFNULL(MIN(rechargeTime), 0)
                )
            FROM {$this->prename}member_recharge
            WHERE
                uid =u.uid
                AND rechargeAmount > 0
                AND isDelete =0
        ) AS rechargeTotalInfo,                                     # 总充值资讯 (次数|金额|首充日期)
        (
            SELECT
                CONCAT(
                    COUNT(*), '|',
                    IFNULL(SUM(amount), 0)
                )
            FROM {$this->prename}member_cash
            WHERE
                uid =u.uid
                {$cash_time_cond}
                AND state IN (0, 3)                                 # [0:确认到帐, 3:已支付]
                AND isDelete =0
        ) AS cashInfo,                                              # 提现资讯 (次数|金额)
        (
            SELECT
                CONCAT(
                    COUNT(*), '|',
                    IFNULL(SUM(amount), 0)
                )
            FROM {$this->prename}member_cash
            WHERE
                uid =u.uid
                AND state IN (0, 3)                                 # [0:确认到帐, 3:已支付]
                AND isDelete =0
        ) AS cashTotalInfo                                          # 总提现资讯 (次数|金额)
    FROM {$this->prename}members AS u
    {$sethide}
    {$having_clause}
    ORDER BY u.regTime ASC";

if (isset($_GET['excel']) && $_GET['excel']=='1') {
    include __DIR__ . '/registration-recharge-excel.php';
    exit;
}
$page = $this->getPage($sql, $this->page, $this->pageSize, $binds);
?>


<table class="table-sorter text-center" cellspacing="0">
    <thead>
        <tr>
            <td>注册日期</td>
            <td>首充日期</td>
            <td>用户名</td>
            <td>充值次数</td>
            <td>充值金额</td>
            <td>提现次数</td>
            <td>提现金额</td>
            <td>总充值次数</td>
            <td>总充值金额</td>
            <td>总提现次数</td>
            <td>总提现金额</td>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($page['data'] as $v) {
            list($czCount, $rechargeAmount) = explode('|', $v['rechargeInfo']);
            list($zczCount, $rechargeAmountTotal, $firstRechargeTime) = explode('|', $v['rechargeTotalInfo']);
            list($txCount, $cashAmount) = explode('|', $v['cashInfo']);
            list($ztxCount, $cashAmountTotal) = explode('|', $v['cashTotalInfo']);
        ?>
            <tr>
                <td><?php echo date('Y-m-d H:i:s', $v['regTime']); ?></td>
                <td><?php echo $firstRechargeTime ? date('Y-m-d H:i:s', $firstRechargeTime) : '--'; ?></td>
                <td title="uid: <?php echo $v['uid']; ?>"><?php echo htmlspecialchars($v['username']); ?></td>
                <td><?php echo $czCount ? $czCount : '--'; ?></td>
                <td><?php echo $rechargeAmount ? $rechargeAmount : '--'; ?></td>
                <td><?php echo $txCount ? $txCount : '--'; ?></td>
                <td><?php echo $cashAmount ? $cashAmount : '--'; ?></td>
                <td><?php echo $zczCount ? $zczCount : '--'; ?></td>
                <td><?php echo $rechargeAmountTotal ? $rechargeAmountTotal : '--'; ?></td>
                <td><?php echo $ztxCount ? $ztxCount : '--'; ?></td>
                <td><?php echo $cashAmountTotal ? $cashAmountTotal : '--'; ?></td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>

<footer>
    <?php
    $rel = get_class($this) . '/registrationRecharge-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $page['total'], $rel, 'defaultReplacePageAction');
    ?>
</footer>
