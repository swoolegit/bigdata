<?php
$this->getTypes();
$this->getPlayeds();

$where_cond = array();
$binds = array();
/*
// 时间限制
$fromTime = empty($_GET['fromTime']) ? '' : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? '' : strtotime($_GET['toTime']);
if ($fromTime && $toTime) {
    $recharge_time_cond = "AND rechargeTime BETWEEN :fromTime AND :toTime";
    $cash_time_cond = "AND cashTime BETWEEN :fromTime AND :toTime";
    $binds['fromTime'] = $fromTime;
    $binds['toTime'] = $toTime;
} else if ($fromTime) {
    $recharge_time_cond = "AND rechargeTime >= :fromTime";
    $cash_time_cond = "AND cashTime >= :fromTime";
    $binds['fromTime'] = $fromTime;
} else if ($toTime) {
    $recharge_time_cond = "AND rechargeTime < :toTime";
    $cash_time_cond = "AND cashTime < :toTime";
    $binds['toTime'] = $toTime;
} else {
    $recharge_time_cond = '';
    $cash_time_cond = '';
}

// 充值状态限制
switch ((isset($_GET['recharge_state']) ? $_GET['recharge_state'] : '')) {
    case 'recharged':
        $having_clause = "HAVING LEFT(rechargeTotalInfo, 2) !='0|'";
        break;
    case 'unrecharge':
        $having_clause = "HAVING LEFT(rechargeTotalInfo, 2) ='0|'";
        break;
    default:
        $having_clause = '';
}

$where_cond = $where_cond ? 'WHERE ' . implode(' AND ', $where_cond) : '';
*/
$sql =
    "
     SELECT
      *
     FROM {$this->prename}bets_repl
	 WHERE actionName != ''
   ";
$page = $this->getPage($sql, $this->page, $this->pageSize, $binds);
?>


<table class="table-sorter text-center" cellspacing="0">
    <thead>
        <tr>
            <td>投注日期</td>
            <td>uid</td>
            <td>用户名</td>
            <td>金额</td>
            <td>期数</td>
            <td>彩种</td>
            <td>玩法</td>
            <td>投注</td>
            <td>之后</td>
        </tr>
    </thead>
    <tbody>
        <?php

        foreach ($page['data'] as $v) {
            list($czCount, $rechargeAmount) = explode('|', $v['rechargeInfo']);
            list($zczCount, $rechargeAmountTotal, $firstRechargeTime) = explode('|', $v['rechargeTotalInfo']);
            list($txCount, $cashAmount) = explode('|', $v['cashInfo']);
            list($ztxCount, $cashAmountTotal) = explode('|', $v['cashTotalInfo']);
        ?>
            <tr>
                <td><?php echo date('Y-m-d H:i:s', $v['actionTime']); ?></td>
                <td><?php echo $v['uid']; ?></td>
                <td><?php echo $v['username']; ?></td>
                <td><?php echo $v['actionNum'] * $v['mode'] * $v['beiShu']; ?></td>
                <td><?php echo $v['actionNo']; ?></td>
                <td><?php echo $this->types[$v['type']]['shortName']; ?></td>
                <td><?php echo $this->playeds[$v['playedId']]['name']; ?></td>
                <td><?php echo $v['actionData']; ?></td>
                <td><?php echo $v['actionName']; ?></td>

            </tr>
        <?php
        }
        ?>
    </tbody>
</table>

<footer>
    <?php
    $rel = get_class($this) . '/betsRepl-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $page['total'], $rel, 'defaultReplacePageAction');
    ?>
</footer>
