<?php
	$this->fromTime = $_GET['fromTime'] ? strtotime($_GET['fromTime'].' 00:00:00') : strtotime(date('Y-m-d 00:00:00'));
	$this->toTime = $_GET['toTime'] ? strtotime($_GET['toTime'].' 23:59:59') : strtotime(date('Y-m-d 23:59:59'));

	$gameClass = $this->getRows("select id,title from {$this->prename}type where enable=1 and isDelete=0 order by sort");
	$sql = sprintf("
		SELECT (
			IFNULL (
				(
					SELECT
						SUM(coin)
					FROM {$this->prename}coin_log_repl

					WHERE liqType BETWEEN 50 AND 55
						AND actionTime BETWEEN %d and %d

				)
			,0 ) +
			IFNULL (
				(
					SELECT
						SUM(c.coin)
					FROM {$this->prename}coin_log_repl c LEFT JOIN {$this->prename}member_recharge r
						ON r.id = c.extfield0
					WHERE c.liqType = 1
						AND c.actionTime BETWEEN %d and %d
						AND r.memo = '活动赠送'
				)
			,0 )
		) otherAmount
		",
		$this->fromTime,
		$this->toTime,
		$this->fromTime,
		$this->toTime
	);

	$total = array();
	$total = $this->GetRow($sql);

?>
<table class="tablesorter" cellspacing="0">
	<thead>
		<tr>
			<td width="120">彩种</td>
			<td width="90">未开奖数</td>
			<td width="90">未开奖额</td>
			<td width="90">有效笔数</td>
			<td width="120">有效投注额</td>
			<td width="120">中奖金额</td>
			<td width="120">公司返点</td>
			<td width="120">赠送、活动、佣金</td>
			<td width="120">结果</td>
			<td>最后投注时间</td>

		</tr>
	</thead>
	<tbody id="nav01">
	<?php

		foreach ($gameClass as $rows) {

			$sql = sprintf("
				SELECT
					count(1) as orderCount,
					(
						SELECT
							SUM(c.coin)
						FROM `{$this->prename}coin_log_repl` c RIGHT JOIN `{$this->prename}bets_repl` b
							ON b.id = c.extfield0
						WHERE c.liqType = 2
							AND b.type = %d
							AND b.actionTime BETWEEN %d and %d
					) fanDianAmount,
					SUM(actionNum*mode*beiShu) betsAmount,
					SUM(bonus) bonus

				FROM `{$this->prename}bets_repl`
				WHERE
					type = %d
					AND isDelete=0
					AND actionTime BETWEEN %d AND %d
					AND lotteryNo != ''
				",
				$rows['id'],
				$this->fromTime,
				$this->toTime,
				$rows['id'],
				$this->fromTime,
				$this->toTime
			);


			$result = $this->GetRow($sql);

			//未开奖
			$sql = sprintf("
				SELECT
					count(1) as orderCount,
					SUM(actionNum*mode*beiShu) betsAmount

				FROM `{$this->prename}bets_repl`
				WHERE
					type = %d
					AND isDelete=0
					AND actionTime BETWEEN %d AND %d
					AND (lotteryNo = '' OR lotteryNo IS NULL)
				",
				$rows['id'],
				$this->fromTime,
				$this->toTime
			);

			$noOpen = $this->GetRow($sql);


			$result['winLose'] = $noOpen['betsAmount'] + $result['betsAmount'] - $result['bonus'] - $result['fanDianAmount'];
			$total['fanDianAmount'] += sprintf('%.02f',$result['fanDianAmount']);
			$total['betsAmount'] += sprintf('%.02f',$result['betsAmount']);
			$total['bonus'] += sprintf('%.03f',$result['bonus']);

			$total['winLose'] +=  sprintf('%.03f',$result['winLose']);
			$total['orderCount'] += $result['orderCount'];
			$total['noOpenOrderCount'] += $noOpen['orderCount'];
			$total['noOpenBetsAmount'] += sprintf('%.02f',$noOpen['betsAmount']);
			//最后一笔下注时间
			$sql = sprintf("
				SELECT
					MAX(actionTime) actionTime
				FROM `{$this->prename}bets_repl`
				WHERE
					type = %d
					AND isDelete=0
				",
				$rows['id']
			);
			$result['actionTime'] = $this->getValue($sql);



	?>
		<tr>
			<td><?=$rows['title']?></td>
			<td><?=(int)$noOpen['orderCount']?></td>
			<td><?=(float)$noOpen['betsAmount']?></td>
			<td><?=(int)$result['orderCount']?></td>
			<td><?=(float)$result['betsAmount']?></td>
			<td><?=(float)$result['bonus']?></td>
			<td><?=(float)$result['fanDianAmount']?></td>
			<td class="text-right"></td>
			<td><span class="<?=($result['winLose']<0? 'spn2':'')?>"><?=sprintf('%.03f',$result['winLose']);?></span></td>
			<td class="text-center"><?=date('Y-m-d H:i:s',$result['actionTime'])?></td>

		</tr>
	<?php } ?>
	<thead>
		<tr>
			<td class="text-right">总计：</td>
			<td class="text-right"><?=$total['noOpenOrderCount'];?></td>
			<td class="text-right"><?=number_format($total['noOpenBetsAmount'], 2, '.', ',');?></td>
			<td class="text-right"><?=$total['orderCount'];?></td>
			<td class="text-right"><?=number_format($total['betsAmount'], 2, '.', ',');?></td>
			<td class="text-right"><?=number_format($total['bonus'], 3, '.', ',');?></td>
			<td class="text-right"><?=number_format($total['fanDianAmount'], 2, '.', ',');?></td>
			<td class="text-right"><?=number_format($total['otherAmount'], 2, '.', ',');?></td>
			<td class="text-right"><span class="<?=($total['winLose']<0? 'spn2':'')?>"><?=number_format($total['winLose'] , 3, '.', ',');?></span></td>
			<td class="text-right">盈亏：<span class="<?=($total['winLose']<0? 'spn2':'')?>"><?=number_format($total['winLose']-$total['otherAmount'], 3, '.', ',');?></span></td>
		</tr>
	</thead>

	</tbody>
</table>

<footer>

</footer>
<script type="text/javascript">
ghhs("nav01","tr");

</script>