<?php
$fromTime = empty($_GET['fromTime']) ? strtotime(date('Y-m-d')) : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? strtotime(date('Y-m-d')) : strtotime($_GET['toTime']);

$sql =
	"SELECT
		r.date, r.real_bet as betAmount, r.zjAmount, r.fanDianAmount, r.rebateAmount, r.bonusAmount, r.brokerageAmount, r.rechargeAmount, r.cashAmount, 
		r.deduction
	FROM {$this->prename}report_day r
		WHERE r.date BETWEEN '{$_GET['fromTime']}' and '{$_GET['toTime']}' 
	order by date asc";
$types = $this->getRows($sql);

$chart=array();
$chart['tooltip']['show']=true;
//$chart['calculable']=true;
$chart['legend']['data']=array("有效投注","中奖金额","返点","退水","分红","活动赠送","充值","提现","人工扣减","盈亏");
//$chart['xAxis'][]=array('type'=>"category",'data'=>array("衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"));
//$chart['xAxis'][]=;
$chart['yAxis'][]=array('type'=>"value",'axisLabel'=>array('formatter'=>'{value}'));
//$chart['series'][]=array('name'=>"量",'type'=>"bar",'data'=>array(5, 20, 40, 10, 10, 20));
//$chart['series']['name']="销量";
//$chart['series']['type']="bar";
//$chart['series']['data']=array(5, 20, 40, 10, 10, 20);
$title_data=array();
$betAmount_data=array();
$zjAmount_data=array();
$fanDianAmount_data=array();
$rebateAmount_data=array();
$bonusAmount_data=array();
$brokerageAmount_data=array();
$rechargeAmount_data=array();
$cashAmount_data=array();
$profitAmount_data=array();
foreach ($types as $t) {
	$title_data[]=$t['date'];
	$betAmount_data[]=$t['betAmount'];
	$zjAmount_data[]=$t['zjAmount'];
	$fanDianAmount_data[]=$t['fanDianAmount'];
	$rebateAmount_data[]=$t['rebateAmount'];
	$bonusAmount_data[]=$t['bonusAmount'];
	$brokerageAmount_data[]=$t['brokerageAmount'];
	$rechargeAmount_data[]=$t['rechargeAmount'];
	$cashAmount_data[]=$t['cashAmount'];
	$deductionAmount_data[]=$t['deduction'];
	$profitAmount_data[]=$t['betAmount']-$t['zjAmount']-$t['fanDianAmount']-$t['rebateAmount']-$t['bonusAmount']-$t['brokerageAmount'];
	
}
$chart['xAxis'][]=array('type'=>"category",'boundaryGap'=>"false",'data'=>$title_data);
$chart['series'][]=array('name'=>"有效投注",'type'=>"line",'data'=>$betAmount_data);
$chart['series'][]=array('name'=>"中奖金额",'type'=>"line",'data'=>$zjAmount_data);
$chart['series'][]=array('name'=>"返点",'type'=>"line",'data'=>$fanDianAmount_data);
$chart['series'][]=array('name'=>"退水",'type'=>"line",'data'=>$rebateAmount_data);
$chart['series'][]=array('name'=>"分红",'type'=>"line",'data'=>$bonusAmount_data);
$chart['series'][]=array('name'=>"活动赠送",'type'=>"line",'data'=>$brokerageAmount_data);
$chart['series'][]=array('name'=>"充值",'type'=>"line",'data'=>$rechargeAmount_data);
$chart['series'][]=array('name'=>"提现",'type'=>"line",'data'=>$cashAmount_data);
$chart['series'][]=array('name'=>"人工扣减",'type'=>"line",'data'=>$deductionAmount_data);
$chart['series'][]=array('name'=>"盈亏",'type'=>"line",'data'=>$profitAmount_data);
echo json_encode($chart);
?>
