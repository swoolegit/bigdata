<?php
$played_group_id = intval($this->tpl_vars['played_group_id']);
if (! $played_group_id) {
	echo '<h3>资料错误</h3>';
	return;
}

$played_group = $this->getRow("SELECT groupName FROM {$this->prename}played_group WHERE id =:id", array('id' => $played_group_id));
if (! $played_group) {
	echo '<h3>无此玩法组</h3>';
	return;
}

$type_id = isset($_GET['type_id']) ? intval($_GET['type_id']) : 0;

$fromTime = empty($_GET['fromTime']) ? strtotime(date('Y-m-d 00:00:00')) : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? strtotime(date('Y-m-d 23:59:59')) : strtotime($_GET['toTime']);

$right_join_where = '';
if (empty($_GET['show_all'])) {
	$right_join_where = 'AND enable = 1';
}

$sql =
	"SELECT
		b.*,
		c.fanDian,
		(betAmount - zjAmount - IF(fanDian IS NULL, 0, fanDian)) AS income,				# 盈亏
		p.name
	FROM (
		SELECT
			COUNT(IF(lotteryNo ='', 1, NULL)) AS unopenedBetCount,						# 未开奖数
			SUM(IF(lotteryNo ='', mode * beiShu * actionNum, 0)) AS unopenedBetAmount,	# 未开奖额
			COUNT(*) AS betCount,														# 有效笔数
			SUM(mode * beiShu * actionNum) AS betAmount,								# 有效投注额
			SUM(bonus) AS zjAmount,														# 中奖金额
			#SUM(fanDian) AS fanDian,													# 公司返点 (改捞 coin_log 比较准确)
			playedId
		FROM {$this->prename}bets_repl
		WHERE
			actionTime BETWEEN :fromTime AND :toTime
			AND type =:type_id
			AND playedGroup =:playedGroupId
			AND isDelete =0
		GROUP BY playedId
	) AS b
		LEFT JOIN (
			SELECT
				b.playedId,
				SUM(c.coin) AS fanDian													# 公司返点
			FROM {$this->prename}coin_log_repl AS c
				LEFT JOIN {$this->prename}bets_repl AS b ON b.id = c.extfield0
			WHERE
				b.actionTime BETWEEN :fromTime and :toTime
				AND c.liqType =2
				AND b.type =:type_id
				AND b.playedGroup =:playedGroupId
			GROUP BY b.playedId
		) AS c ON c.playedId = b.playedId
		RIGHT JOIN (
			SELECT
				id,
				name
			FROM {$this->prename}played
			WHERE
				groupId =:playedGroupId
				{$right_join_where}
		) AS p ON p.id = b.playedId";
$playeds = $this->getRows(
	$sql,
	array(
		'type_id' => $type_id,
		'playedGroupId' => $played_group_id,
		'fromTime' => $fromTime,
		'toTime' => $toTime
	),
	1800,
	(!empty($_GET['flush_cache']) ? 1 : 0)
);

$order_by = isset($_GET['order_by']) ? $_GET['order_by'] : '';
switch ($order_by) {
	case 'unopenedBetCount':
		$column = 'unopenedBetCount';
		break;
	case 'unopenedBetAmount':
		$column = 'unopenedBetAmount';
		break;
	case 'betCount':
		$column = 'betCount';
		break;
	case 'betAmount':
		$column = 'betAmount';
		break;
	case 'zjAmount':
		$column = 'zjAmount';
		break;
	case 'fanDian':
		$column = 'fanDian';
		break;
	default:
		$column = 'income';
}
switch ((isset($_GET['sort']) ? $_GET['sort'] : '')) {
	case 'ASC':
		$sort = 'ASC';
		break;
	default:
		$sort = 'DESC';
}
$this->orderBy($playeds, $column, $sort);
?>


<table class="table-sorter" cellspacing="0">
	<thead class="text-center">
		<tr>
			<td>玩法</td>
			<td class="clickable sort<?php if ($order_by == 'unopenedBetCount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('unopenedBetCount')">未开奖数</td>
			<td class="clickable sort<?php if ($order_by == 'unopenedBetAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('unopenedBetAmount')">未开奖额</td>
			<td class="clickable sort<?php if ($order_by == 'betCount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('betCount')">有效笔数</td>
			<td class="clickable sort<?php if ($order_by == 'betAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('betAmount')">有效投注额</td>
			<td class="clickable sort<?php if ($order_by == 'zjAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('zjAmount')">中奖金额</td>
			<td class="clickable sort<?php if ($order_by == 'fanDian') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('fanDian')">公司返点</td>
			<td class="clickable sort<?php if ($order_by == 'income') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('income')">盈亏</td>
		</tr>
	</thead>
	<tbody class="text-right">
		<?php
		$total = array(
			'unopenedBetCount' => 0,
			'unopenedBetAmount' => 0,
			'betCount' => 0,
			'betAmount' => 0,
			'zjAmount' => 0,
			'fanDian' => 0,
			'income' => 0
		);
		foreach ($playeds as $p) {
			$unopenedBetCount = $p['unopenedBetCount'] ? $p['unopenedBetCount'] : 0;
			$unopenedBetAmount = $p['unopenedBetAmount'] ? $p['unopenedBetAmount'] : 0;
			$betCount = $p['betCount'] ? $p['betCount'] : 0;
			$betAmount = $p['betAmount'] ? $p['betAmount'] : 0;
			$zjAmount = $p['zjAmount'] ? $p['zjAmount'] : 0;
			$fanDian = $p['fanDian'] ? $p['fanDian'] : 0;
			$income = $p['income'] ? $p['income'] : 0;

			$total['unopenedBetCount'] += $unopenedBetCount;
			$total['unopenedBetAmount'] += $unopenedBetAmount;
			$total['betCount'] += $betCount;
			$total['betAmount'] += $betAmount;
			$total['zjAmount'] += $zjAmount;
			$total['fanDian'] += $fanDian;
			$total['income'] += $income;
		?>
			<tr>
				<td><?php echo $p['name'] ?></td>
				<td><?php echo number_format($unopenedBetCount) ?></td>
				<td><?php echo number_format($unopenedBetAmount, 3) ?></td>
				<td><?php echo number_format($betCount) ?></td>
				<td><?php echo number_format($betAmount, 3) ?></td>
				<td><?php echo number_format($zjAmount, 3) ?></td>
				<td><?php echo number_format($fanDian, 3) ?></td>
				<td class="<?php echo $income < 0 ? 'red' : '' ?>"><?php echo number_format($income, 3) ?></td>
			</tr>
		<?php
		}
		?>
	</tbody>
	<tfoot class="text-right">
		<tr>
			<td>总计</td>
			<td><?php echo number_format($total['unopenedBetCount']) ?></td>
			<td><?php echo number_format($total['unopenedBetAmount'], 3) ?></td>
			<td><?php echo number_format($total['betCount']) ?></td>
			<td><?php echo number_format($total['betAmount'], 3) ?></td>
			<td><?php echo number_format($total['zjAmount'], 3) ?></td>
			<td><?php echo number_format($total['fanDian'], 3) ?></td>
			<td class="<?php echo $total['income'] < 0 ? 'red' : '' ?>"><?php echo number_format($total['income'], 3) ?></td>
		</tr>
	</tfoot>
</table>
