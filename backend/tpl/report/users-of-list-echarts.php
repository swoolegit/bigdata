<?php
$fromTime = empty($_GET['fromTime']) ? strtotime(date('Y-m-d')) : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? strtotime(date('Y-m-d')) : strtotime($_GET['toTime']);

$sql =
	"SELECT
		r.date, r.newUser, r.betUser, r.LoginUser
	FROM {$this->prename}report_day r
		WHERE r.date BETWEEN '{$_GET['fromTime']}' and '{$_GET['toTime']}'
	order by date asc";
$types = $this->getRows($sql);

$chart=array();
$chart['tooltip']['show']=true;
//$chart['calculable']=true;
$chart['legend']['data']=array("注册人数","投注人数","登录人数");
//$chart['xAxis'][]=array('type'=>"category",'data'=>array("衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"));
//$chart['xAxis'][]=;
$chart['yAxis'][]=array('type'=>"value",'axisLabel'=>array('formatter'=>'{value} 人'));
//$chart['series'][]=array('name'=>"量",'type'=>"bar",'data'=>array(5, 20, 40, 10, 10, 20));
//$chart['series']['name']="销量";
//$chart['series']['type']="bar";
//$chart['series']['data']=array(5, 20, 40, 10, 10, 20);
$title_data=array();
$newUser_data=array();
$betUser_data=array();
$loginUser_data=array();
foreach ($types as $t) {
	$title_data[]=$t['date'];
	$newUser_data[]=$t['newUser'];
	$betUser_data[]=$t['betUser'];
	$loginUser_data[]=$t['LoginUser'];
}
$chart['xAxis'][]=array('type'=>"category",'boundaryGap'=>"false",'data'=>$title_data);
$chart['series'][]=array('name'=>"注册人数",'type'=>"line",'data'=>$newUser_data);
$chart['series'][]=array('name'=>"投注人数",'type'=>"line",'data'=>$betUser_data);
$chart['series'][]=array('name'=>"登录人数",'type'=>"line",'data'=>$loginUser_data);
echo json_encode($chart);
?>
