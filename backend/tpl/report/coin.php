<?php

if(empty($_GET['fromTime']))
{
	$_GET['fromTime']=date("Y-m-d H:i:s",strtotime('00:00:00'));
}
if(empty($_GET['toTime']))
{
	$_GET['toTime']=date("Y-m-d H:i:s",strtotime('23:59:59'));
}

$fromTime = empty($_GET['fromTime']) ? '' : htmlspecialchars($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? '' : htmlspecialchars($_GET['toTime']);
$recharge_state = empty($_GET['recharge_state']) ? '' : htmlspecialchars($_GET['recharge_state']);
?>


<article class="module width_full">
    <header>
        <h3 class="tabs_involved">出入帐汇总
            <form id="query-form" class="submit_link wz" action="report/coinList" target="ajax" dataType="html"  call="defaultList">
<label></label>
<select name="utype">
	<option value="1">用户查询</option>
	<option value="2">代理线查询</option>
</select>
：<input class="alt_btn" style="width:100px;" value="" placeholder="" name="username" type="text">&nbsp;&nbsp;
<span style="cursor:pointer;color: #77BACE;" id="fday" >上</span>&nbsp;&nbsp;<span style="cursor:pointer;color: #77BACE;"  id="bday">下</span>
                时间：
                    从 <input type="text" class="text-center" name="fromTime" id="fromTime" value="<?php echo $fromTime ?>" />
                    到 <input type="text" class="text-center" name="toTime" id="toTime" value="<?php echo $toTime ?>" />　
                <input type="submit" id="submit_btn" class="alt_btn" value="查找">
            </form>
        </h3>
    </header>
    <div class="tab_content" id="tab_content">
        <?php $this->display('report/coin-list.php') ?>
    </div>
</article>


<script>
$(function() {
    var f = document.getElementById("query-form");
	$('#fday').die('click');
	$('#bday').die('click');
	$('#fday').live('click', function(){
		var date=$('#fromTime').val();
		$('#fromTime').val(addDate(date,-1)+" 00:00:00");
		var date=$('#toTime').val();
		$('#toTime').val(addDate(date,-1)+" 23:59:59");
		$('#submit_btn').click();
		return false
	});    
	$('#bday').live('click', function(){
		var date=$('#fromTime').val();
		$('#fromTime').val(addDate(date,1)+" 00:00:00");
		var date=$('#toTime').val();
		$('#toTime').val(addDate(date,1)+" 23:59:59");
		$('#submit_btn').click();
		return false
	});    

        $fromTime = $(f.fromTime);
        $toTime = $(f.toTime);
    $fromTime.datetimepicker({
        timeFormat: "HH:mm:ss",
        controlType: "select",
        oneLine: true,
        onSelect: function(selectedDateTime) {
            $toTime.datetimepicker("option", "minDate", selectedDateTime.split(" ")[0]);
        }
    });
    $toTime.datetimepicker({
        timeFormat: "HH:mm:ss",
        controlType: "select",
        oneLine: true,
        hour: 23,
        minute: 59,
        second: 59,
        onSelect: function(selectedDateTime) {
            $fromTime.datetimepicker("option", "maxDate",  selectedDateTime.split(" ")[0]);
        }
    });

});
</script>
