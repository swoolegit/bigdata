<?php

if (!isset($this) || !($this instanceof DBAccess)) {
    die('请勿直接使用本程序');
} else if (! isset($sql, $binds)) {
    die('资料有误');
}

header('Content-Type: application/vnd.ms-excel');   // 声明网页格式
header('Content-Disposition: attachment; filename=注册充值报表.xls'); // 设置文件名称

if (isset($_GET['all']) && $_GET['all']=='1') {
    $rows = $this->getRows($sql, $binds);
} else {
    $p = isset($_GET['page']) ? intval($_GET['page']) : 1;
    if ($p < 1) {
        $p = 1;
    }
    $page = $this->getPage($sql, $p, $this->pageSize, $binds);
    $rows = $page['data'];
}

?>


<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <table border="1">
            <tr>
                <td>注册日期</td>
                <td>首充日期</td>
                <td>用户名</td>
                <td>充值次数</td>
                <td>充值金额</td>
                <td>提现次数</td>
                <td>提现金额</td>
                <td>总充值次数</td>
                <td>总充值金额</td>
                <td>总提现次数</td>
                <td>总提现金额</td>
            </tr>
            <?php
            foreach ($rows as $v) {
                list($czCount, $rechargeAmount) = explode('|', $v['rechargeInfo']);
                list($zczCount, $rechargeAmountTotal, $firstRechargeTime) = explode('|', $v['rechargeTotalInfo']);
                list($txCount, $cashAmount) = explode('|', $v['cashInfo']);
                list($ztxCount, $cashAmountTotal) = explode('|', $v['cashTotalInfo']);
            ?>
                <tr>
                    <td><?php echo date('Y-m-d H:i:s', $v['regTime']); ?></td>
                    <td><?php echo $firstRechargeTime ? date('Y-m-d H:i:s', $firstRechargeTime) : '--'; ?></td>
                    <td title="uid: <?php echo $v['uid']; ?>"><?php echo htmlspecialchars($v['username']); ?></td>
                    <td><?php echo $czCount ? $czCount : '--'; ?></td>
                    <td><?php echo $rechargeAmount ? $rechargeAmount : '--'; ?></td>
                    <td><?php echo $txCount ? $txCount : '--'; ?></td>
                    <td><?php echo $cashAmount ? $cashAmount : '--'; ?></td>
                    <td><?php echo $zczCount ? $zczCount : '--'; ?></td>
                    <td><?php echo $rechargeAmountTotal ? $rechargeAmountTotal : '--'; ?></td>
                    <td><?php echo $ztxCount ? $ztxCount : '--'; ?></td>
                    <td><?php echo $cashAmountTotal ? $cashAmountTotal : '--'; ?></td>
                </tr>
            <?php
            }
            ?>
        </table>
    </body>
</html>
