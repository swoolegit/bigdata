<?php
$fromTime = empty($_GET['fromTime']) ? strtotime(date('Y-m-d 00:00:00')) : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? strtotime(date('Y-m-d 23:59:59')) : strtotime($_GET['toTime']);
$type = empty($_GET['type']) ? '' : " and rt.type=".$_GET['type'];

$order_by = isset($_GET['order_by']) ? $_GET['order_by'] : '';
switch ($order_by) {
	case 'actionNum':
		$column = 'actionNum';
		break;
	case 'betCount':
		$column = 'betCount';
		break;
	case 'betAmount':
		$column = 'betAmount';
		break;
	case 'zjAmount':
		$column = 'zjAmount';
		break;
	case 'fanDian':
		$column = 'fanDian';
		break;
	case 'rebate':
		$column = 'rebate';
		break;
	default:
		$column = 'income';
		break;
}
switch ((isset($_GET['sort']) ? $_GET['sort'] : '')) {
	case 'ASC':
		$sort = 'ASC';
		break;
	default:
		$sort = 'DESC';
}
$orderBy="";
if(isset($_GET['order_by']))
{
	$orderBy=" order by ".$column." ".$sort;
}
//$this->orderBy($types, $column, $sort);
$sql="select
		sum(rt.amount) as betAmount, 
		sum(rt.orders) as betCount, 
		sum(rt.zjAmount) as zjAmount,
		sum(rt.rebate) as rebate,
		sum(rt.fanDian) as fanDian,
		sum(rt.amount - rt.zjAmount- rt.rebate- rt.fanDian) as income,
		m.username, 
		m.uid,
		m.fanDian as user_fanDian,
		m.rebate as user_rebate
		from {$this->prename}report_type rt,{$this->prename}members m 
		where rt.actionTime BETWEEN {$fromTime} AND {$toTime}  {$type} and rt.uid=m.uid group by m.uid {$orderBy} limit 50;
";
$types = $this->getRows($sql);
?>

<table class="table-sorter" cellspacing="0">
	<thead class="text-center">
		<tr>
		 	<td>用户名</td>
			<td class="clickable sort<?php if ($order_by == 'betCount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('betCount')">注单</td>
			<td class="clickable sort<?php if ($order_by == 'betAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('betAmount')">有效投注额</td>
			<td class="clickable sort<?php if ($order_by == 'zjAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('zjAmount')">中奖金额</td>
			<td class="clickable sort<?php if ($order_by == 'zjAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('fanDian')">返点</td>
			<td class="clickable sort<?php if ($order_by == 'zjAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('rebate')">退水</td>
			<td class="clickable sort<?php if ($order_by == 'income') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('income')">盈亏 (%)</td>
		</tr>
	</thead>
	<tbody class="text-right">
		<?php
		$total = array(
			'betCount' => 0,
			'betAmount' => 0,
			'zjAmount' => 0,
			'income' => 0,
			'fanDian' => 0,
			'rebate' => 0,
		);
		foreach ($types as $t) {
			$betCount = $t['betCount'] ? $t['betCount'] : 0;
			$betAmount = $t['betAmount'] ? $t['betAmount'] : 0;
			$zjAmount = $t['zjAmount'] ? $t['zjAmount'] : 0;
			$income = $t['income'] ? $t['income'] : 0;
			$fanDian = $t['fanDian'] ? $t['fanDian'] : 0;
			$rebate = $t['rebate'] ? $t['rebate'] : 0;

			$total['betCount'] += $betCount;
			$total['betAmount'] += $betAmount;
			$total['zjAmount'] += $zjAmount;
			$total['fanDian'] += $fanDian;
			$total['rebate'] += $rebate;
			$total['income'] += $income;

		?>
			<tr>
				<td>
					<?php echo $t['username'] ?>
				</td>				
				<td><?php echo number_format($betCount) ?></td>
				<td><?php echo number_format($betAmount, 3) ?></td>
				<td><?php echo number_format($zjAmount, 3) ?></td>
				<td><?php echo number_format($fanDian, 3) ?>(<?=$t['user_fanDian']?>%)</td>
				<td><?php echo number_format($rebate, 3) ?>(<?=$t['user_rebate']?>%)</td>
				<td class="<?php echo $income < 0 ? 'red' : '' ?>">
					<?php 
						echo number_format($income, 3);
						$temp=round((($income/$betAmount)*100),2);
						echo " (".$temp."%)";
					?>
				</td>
				
			</tr>
	<?php } ?>
	</tbody>
	<tfoot class="text-right">
		<tr>
			<td>总计</td>
			<td><?php echo number_format($total['betCount']) ?></td>
			<td><?php echo number_format($total['betAmount'], 3) ?></td>
			<td><?php echo number_format($total['zjAmount'], 3) ?></td>
			<td><?php echo number_format($total['fanDian'], 3) ?></td>
			<td><?php echo number_format($total['rebate'], 3) ?></td>
			<td class="<?php echo $total['income'] < 0 ? 'red' : '' ?>"><?php echo number_format($total['income'], 3) ?></td>
		</tr>
	</tfoot>
</table>
