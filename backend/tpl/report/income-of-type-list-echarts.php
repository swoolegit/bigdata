<?php
$fromTime = empty($_GET['fromTime']) ? strtotime(date('Y-m-d 00:00:00')) : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? strtotime(date('Y-m-d 23:59:59')) : strtotime($_GET['toTime']);

$right_join_where = '';
if (empty($_GET['show_all'])) {
	$right_join_where = 'WHERE enable = 1 AND isDelete = 0';
}

/*
$sql =
	"SELECT
		b.*,
		c.fanDian,
		(betAmount - zjAmount - IF(fanDian IS NULL, 0, fanDian)) AS income,				# 盈亏
		t.id,
		t.title
	FROM (
		SELECT
			COUNT(IF(lotteryNo ='', 1, NULL)) AS unopenedBetCount,						# 未开奖数
			SUM(IF(lotteryNo ='', mode * beiShu * actionNum, 0)) AS unopenedBetAmount,	# 未开奖额
			COUNT(*) AS betCount,														# 有效笔数
			SUM(mode * beiShu * actionNum) AS betAmount,								# 有效投注额
			SUM(bonus) AS zjAmount,														# 中奖金额
			#SUM(fanDian) AS fanDian,													# 公司返点 (改捞 coin_log 比较准确)
			type
		FROM {$this->prename}bets_repl
		WHERE
			actionTime BETWEEN :fromTime AND :toTime
			AND isDelete =0
		GROUP BY type
	) AS b
		LEFT JOIN (
			SELECT
				b.type,
				SUM(c.coin) AS fanDian													# 公司返点
			FROM {$this->prename}coin_log_repl AS c
				LEFT JOIN {$this->prename}bets_repl AS b ON b.id = c.extfield0
			WHERE
				b.actionTime BETWEEN :fromTime and :toTime
				AND c.liqType =2
			GROUP BY b.type
		) AS c ON c.type = b.type
		RIGHT JOIN (
			SELECT
				id,
				title
			FROM {$this->prename}type
			{$right_join_where}
		) AS t ON t.id = b.type";
*/
	$sql="select
		t.title,
		t.type,
		st.betAmount,
		st.actionNum,
		st.orders as betCount,
		st.zjAmount,
		st.fandian as fanDian,
		st.rebate as rebate,
		(st.betAmount - st.fandian - st.zjAmount  - st.rebate) as income	
		from 
		{$this->prename}type t left join 
		(
			select 
			rt.type,
			sum(rt.amount) as betAmount, 
			sum(rt.actionNum) as actionNum, 
			sum(rt.orders) as orders, 
			sum(rt.zjAmount) as zjAmount, 
			sum(rt.fandian) as fandian ,
			sum(rt.rebate) as rebate 
			from {$this->prename}admin_report_type rt where rt.actionTime BETWEEN {$fromTime} AND {$toTime}  group by rt.type 
		) as st on t.id=st.type where t.enable=1
		";
//echo $sql;
$types = $this->getRows($sql);
$chart=array();
$chart['tooltip']['show']=true;
$chart['legend']['data']=array("有效投注额","中奖金额","盈亏");
//$chart['xAxis'][]=array('type'=>"category",'data'=>array("衬衫","羊毛衫","雪纺衫","裤子","高跟鞋","袜子"));
//$chart['xAxis'][]=;
$chart['yAxis'][]=array('type'=>"value");
//$chart['series'][]=array('name'=>"量",'type'=>"bar",'data'=>array(5, 20, 40, 10, 10, 20));
//$chart['series']['name']="销量";
//$chart['series']['type']="bar";
//$chart['series']['data']=array(5, 20, 40, 10, 10, 20);
$title_data=array();
$betAmount_data=array();
$zjAmount_data=array();
$income_data=array();
$fanDian_data=array();
$rebate_data=array();
foreach ($types as $t) {
	$betAmount = $t['betAmount'] ? $t['betAmount'] : 0;
	$zjAmount = $t['zjAmount'] ? $t['zjAmount'] : 0;
	$income = $t['income'] ? $t['income'] : 0;
	$fanDian = $t['fanDian'] ? $t['fanDian'] : 0;
	$rebate = $t['rebate'] ? $t['rebate'] : 0;
	$title_data[]=$t['title'];
	$betAmount_data[]=$betAmount;
	$zjAmount_data[]=$zjAmount;
	$income_data[]=$income;
	//$fanDian_data[]=$fanDian;
	//$rebate_data[]=$rebate;
}
$chart['xAxis'][]=array('type'=>"category",'data'=>$title_data);
$chart['series'][]=array('name'=>"有效投注额",'type'=>"bar",'data'=>$betAmount_data);
$chart['series'][]=array('name'=>"中奖金额",'type'=>"bar",'data'=>$zjAmount_data);
$chart['series'][]=array('name'=>"盈亏",'type'=>"bar",'data'=>$income_data);
// $chart['series'][]=array('name'=>"返点",'type'=>"bar",'data'=>$fanDian_data);
// $chart['series'][]=array('name'=>"退水",'type'=>"bar",'data'=>$rebate_data);
echo json_encode($chart);
?>
