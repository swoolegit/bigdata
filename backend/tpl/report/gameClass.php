
<style>
.tablesorter td{text-align:right;}
.text-right {text-align:right !important;}
.text-center {text-align:center !important;}
</style>
<article class="module width_full">
    <header>
        <h3 class="tabs_involved">彩种报表

            <form action="report/gameClassSearch" target="ajax" dataType="html" call="defaultList" class="submit_link wz">
				<input type="submit" onclick="changeMonth(-1)" value="上月">&nbsp;
				<input type="submit" onclick="changeMonth(0)" value="本月">&nbsp;
				<input type="submit" onclick="changeWeek(-1)" value="上周">&nbsp;
				<input type="submit" onclick="changeWeek(0)" value="本周">&nbsp;
				<input type="submit" onclick="changeDate(-2)" value="前天">&nbsp;
				<input type="submit" onclick="changeDate(-1)" value="昨天">&nbsp;
				<input type="submit" onclick="changeDate(0)" value="今天">&nbsp;

				时间：从 <input type="date" class="alt_btn" name="fromTime" value="<?=date('Y-m-d')?>"/> 到 <input type="date" class="alt_btn" name="toTime" value="<?=date('Y-m-d')?>"/>&nbsp;&nbsp;
                <input type="submit" value="查找" class="alt_btn">
            </form>
        </h3>
    </header>

    <div class="tab_content">
        <?php $this->display("report/gameClass-list.php"); ?>
	</div>
</article>
<script>
function changeWeek(num) {
	weekDate.getThisWeekDate();
	var data = num == 0 ? weekDate.getThisWeekDate() :  weekDate.getPrevWeekDate();
	$('input[name=fromTime]').val(data[0]);
	$('input[name=toTime]').val(data[1]);

}
function changeMonth(num) {

	var data = getMonthStr(num);
	$('input[name=fromTime]').val(data[0]);
	$('input[name=toTime]').val(data[1]);
}
function changeDate(num) {

	$('input[name=fromTime]').val(getDateStr(num));
	$('input[name=toTime]').val(getDateStr(num));
}
function getDateStr(number){
	var dd = new Date();
	dd.setDate(dd.getDate()+number);//获取AddDayCount天后的日期
	var y = dd.getFullYear();
	var m = dd.getMonth()+1;//获取当前月份的日期
	var d = dd.getDate();
	return y+"-"+m+"-"+d;
}
function getMonthStr(number){
	var dd = new Date();
	dd.setMonth(dd.getMonth()+number);
	var y = dd.getFullYear();
	var m = dd.getMonth()+1;
	var d = dd.getDate();
	return [y+"-"+m+"-01",y+"-"+m+"-"+new Date(y,m,0).getDate()];
}

function WeekDate() {

    /**
     * 基准时间，所有计算以此为基础
     */
    var _calcDate = new Date();

    /**
     * 一天的豪秒数
     */
    var _day = 1000 * 60 * 60 * 24;

    this.getThisWeekDate = getThisWeekDate;
    this.getPrevWeekDate = getPrevWeekDate;
    this.getNextWeekDate = getNextWeekDate;
    this.wrapDate = wrapDate;

    this.getDayMillisecond = getDayMillisecond;

    /**
     * 取上周开始至上周结束日期
     *
     * @return Array [0]上周第一天 [1]上周最后一天
     */
    function getPrevWeekDate() {
        // 取上周结束日期
        var lastDay = new Date(_calcDate - (_calcDate.getDay()) * _day);
        // 取上周开始日期
        var firstDay = new Date((lastDay * 1) - 6 * _day);
        // 更新基准时间
        _calcDate = firstDay;

        return [wrapDate(firstDay), wrapDate(lastDay)];
    }

    /**
     * 取下周开始至下周结束日期
     *
     * @return Array [0]上周第一天 [1]上周最后一天
     */
    function getNextWeekDate() {
        // 取下周开始日期
        var firstDay = new Date((_calcDate * 1) + (6 - _calcDate.getDay() + 2) * _day);
        // 取下周结束日期
        var lastDay = new Date((firstDay * 1) + 6 * _day);
        // 更新基准时间
        _calcDate = firstDay;

        return [wrapDate(firstDay), wrapDate(lastDay)];
    }

    /**
     * 取本周开始至本周结束日期
     *
     * @return Array [0]本周第一天 [1]本周最后一天
     */
    function getThisWeekDate() {
        _calcDate = new Date();
        // 第一天日期
        var firstDay = new Date(_calcDate - (_calcDate.getDay() - 1) * _day);
        // 最后一天日期
        var lastDay = new Date((firstDay * 1) + 6 * _day);

        return [wrapDate(firstDay), wrapDate(lastDay)];
    }

    function wrapDate($date) {
        var m = $date.getMonth() + 1;
        m = m < 10 ? "0" + m : m;

        var d = $date.getDate();
        d = d < 10 ? "0" + d : d;

        return $date.getFullYear() + "-" + m + "-" + d;
    }

    function getDayMillisecond() {
        return _day;
    }
}
var weekDate = new WeekDate();

</script>
