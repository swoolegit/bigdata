<?php
$fromTime = empty($_GET['fromTime']) ? date('Y-m-d') : htmlspecialchars($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? date('Y-m-d') : htmlspecialchars($_GET['toTime']);

$show_all = isset($_GET['show_all']) ? 1 : 0;
$order_by = empty($_GET['order_by']) ? 'income' : htmlspecialchars($_GET['order_by']);
$sort = empty($_GET['sort']) ? 'DESC' : htmlspecialchars($_GET['sort']);
?>
<div class="system-info">
    <ul>
        <li>此表每日凌晨更新一次</li>
        <li>盈亏公式 : 注额-派奖-返点-退水</li>
        <li>由于返点及退水数据较小,故图表省略显示. 但盈亏有扣减这两项数值</li>
    </ul>    
</div>


<article class="module width_full">
    <header>
        <h3 class="tabs_involved">彩种报表
            <form id="query-form" class="submit_link wz" action="report/incomeOfTypeListEcharts" target="ajax" dataType="html" call="defaultListEcharts">

				<input type="submit" onclick="changeMonth(-1)" value="上月">&nbsp;
				<input type="submit" onclick="changeMonth(0)" value="本月">&nbsp;
				<input type="submit" onclick="changeWeek(-1)" value="上周">&nbsp;
				<input type="submit" onclick="changeWeek(0)" value="本周">&nbsp;
				<input type="submit" onclick="changeDate(-2)" value="前天">&nbsp;
				<input type="submit" onclick="changeDate(-1)" value="昨天">&nbsp;
				<input type="submit" onclick="changeDate(0)" value="今天">&nbsp;
				时间：
					从 <input type="date" class="alt_btn" name="fromTime" id="date1"  value="<?php echo $fromTime ?>" />
					到 <input type="date" class="alt_btn" name="toTime"  id="date2" value="<?php echo $toTime ?>" />　
                <input type="submit" id="submit_btn" class="alt_btn" value="查找">
				<input type="hidden" name="order_by" value="<?php echo $order_by ?>" />
				<input type="hidden" name="sort" value="<?php echo $sort ?>" />
            </form>
        </h3>
    </header>

    <div class="tab_content"></div>
<div id="main_chart" style="height:500px;width:1500px"></div>
</article>
<script type="text/javascript">
var myChart =null;

</script>
<script>
$(function() {
	var f = document.getElementById("query-form");
	$("#date1").datepicker({
		  //minView: "month",//设置只显示到月份
		  format : "Y-m-d",//日期格式
		  autoclose:true,//选中关闭
		  todayBtn: true
	});
	$("#date2").datepicker({
		  format : "Y-m-d",//日期格式
		  autoclose:true,//选中关闭
		  todayBtn: true
	});

	$("#submit_btn").click();
});
function prepareQueryString(a) {
	var e = window.event;
	e.preventDefault && e.preventDefault();
	e.returnValue = false;	// IE8

	var qs = $("#query-form").serialize(),
		href = a.getAttribute("href"),
		url = href + (qs ? "?" + qs : "");
	load(url);
}
function orderBy(column) {
    var f = document.getElementById("query-form");
    if (f.order_by.value == column) {
        f.sort.value = (f.sort.value == "ASC" ? "DESC" : "ASC");
    } else {
        f.order_by.value = column;
        f.sort.value = "DESC";
    }
    alert("test");
	$("#submit_btn").click();
}

function changeWeek(num) {
	weekDate.getThisWeekDate();
	var data = num == 0 ? weekDate.getThisWeekDate() : weekDate.getPrevWeekDate();
	$('input[name=fromTime]').val(data[0] );
	$('input[name=toTime]').val(data[1] );
}
function changeMonth(num) {
	var data = getMonthStr(num);
	$('input[name=fromTime]').val(data[0] );
	$('input[name=toTime]').val(data[1] );
}
function changeDate(num) {
	$('input[name=fromTime]').val(getDateStr(num) );
	$('input[name=toTime]').val(getDateStr(num) );
}

function getDateStr(number){
	var dd = new Date();
	dd.setDate(dd.getDate()+number);//获取AddDayCount天后的日期
	var y = dd.getFullYear();
	var m = dd.getMonth()+1;//获取当前月份的日期
	var d = dd.getDate();
	m = m < 10 ? "0" + m : m;
	d = d < 10 ? "0" + d : d;
	return y+"-"+m+"-"+d;
}
function getMonthStr(number){
	var dd = new Date();
	dd.setMonth(dd.getMonth()+number);
	var y = dd.getFullYear();
	var m = dd.getMonth()+1;
	m = m < 10 ? "0" + m : m;
	return [y+"-"+m+"-01",y+"-"+m+"-"+new Date(y,m,0).getDate()];
}

function WeekDate() {

    /**
     * 基准时间，所有计算以此为基础
     */
    var _calcDate = new Date();

    /**
     * 一天的豪秒数
     */
    var _day = 1000 * 60 * 60 * 24;

    this.getThisWeekDate = getThisWeekDate;
    this.getPrevWeekDate = getPrevWeekDate;
    this.getNextWeekDate = getNextWeekDate;
    this.wrapDate = wrapDate;

    this.getDayMillisecond = getDayMillisecond;

    /**
     * 取上周开始至上周结束日期
     *
     * @return Array [0]上周第一天 [1]上周最后一天
     */
    function getPrevWeekDate() {
        // 取上周结束日期
        var lastDay = new Date(_calcDate - (_calcDate.getDay()) * _day);
        // 取上周开始日期
        var firstDay = new Date((lastDay * 1) - 6 * _day);
        // 更新基准时间
        _calcDate = firstDay;

        return [wrapDate(firstDay), wrapDate(lastDay)];
    }

    /**
     * 取下周开始至下周结束日期
     *
     * @return Array [0]上周第一天 [1]上周最后一天
     */
    function getNextWeekDate() {
        // 取下周开始日期
        var firstDay = new Date((_calcDate * 1) + (6 - _calcDate.getDay() + 2) * _day);
        // 取下周结束日期
        var lastDay = new Date((firstDay * 1) + 6 * _day);
        // 更新基准时间
        _calcDate = firstDay;

        return [wrapDate(firstDay), wrapDate(lastDay)];
    }

    /**
     * 取本周开始至本周结束日期
     *
     * @return Array [0]本周第一天 [1]本周最后一天
     */
    function getThisWeekDate() {
        _calcDate = new Date();
        // 第一天日期
        var firstDay = new Date(_calcDate - (_calcDate.getDay() - 1) * _day);
        // 最后一天日期
        var lastDay = new Date((firstDay * 1) + 6 * _day);

        return [wrapDate(firstDay), wrapDate(lastDay)];
    }

    function wrapDate($date) {
        var m = $date.getMonth() + 1;
        m = m < 10 ? "0" + m : m;

        var d = $date.getDate();
        d = d < 10 ? "0" + d : d;

        return $date.getFullYear() + "-" + m + "-" + d;
    }

    function getDayMillisecond() {
        return _day;
    }
}
var weekDate = new WeekDate();
</script>
