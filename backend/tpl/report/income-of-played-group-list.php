<?php
$type_id = intval($this->tpl_vars['type_id']);
if (! $type_id) {
	echo '<h3>资料错误</h3>';
	return;
}

$type = $this->getRow("SELECT type, title FROM {$this->prename}type WHERE id =:id", array('id' => $type_id));
if (! $type) {
	echo '<h3>无此彩种</h3>';
	return;
}

$fromTime = empty($_GET['fromTime']) ? strtotime(date('Y-m-d 00:00:00')) : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? strtotime(date('Y-m-d 23:59:59')) : strtotime($_GET['toTime']);

$right_join_where = '';
if (empty($_GET['show_all'])) {
	$right_join_where = 'AND enable = 1';
}

$sql =
	"SELECT
		b.*,
		c.fanDian,
		(betAmount - zjAmount - IF(fanDian IS NULL, 0, fanDian)) AS income,				# 盈亏
		pg.id,
		pg.groupName
	FROM (
		SELECT
			COUNT(IF(lotteryNo ='', 1, NULL)) AS unopenedBetCount,						# 未开奖数
			SUM(IF(lotteryNo ='', mode * beiShu * actionNum, 0)) AS unopenedBetAmount,	# 未开奖额
			COUNT(*) AS betCount,														# 有效笔数
			SUM(mode * beiShu * actionNum) AS betAmount,								# 有效投注额
			SUM(bonus) AS zjAmount,														# 中奖金额
			#SUM(fanDian) AS fanDian,													# 公司返点 (改捞 coin_log 比较准确)
			playedGroup
		FROM {$this->prename}bets_repl
		WHERE
			actionTime BETWEEN :fromTime AND :toTime
			AND type =:type_id
			AND isDelete =0
		GROUP BY playedGroup
	) AS b
		LEFT JOIN (
			SELECT
				b.playedGroup,
				SUM(c.coin) AS fanDian													# 公司返点
			FROM {$this->prename}coin_log_repl AS c
				LEFT JOIN {$this->prename}bets_repl AS b ON b.id = c.extfield0
			WHERE
				b.actionTime BETWEEN :fromTime and :toTime
				AND c.liqType =2
				AND b.type =:type_id
			GROUP BY b.playedGroup
		) AS c ON c.playedGroup = b.playedGroup
		RIGHT JOIN (
			SELECT
				id,
				groupName
			FROM {$this->prename}played_group
			WHERE
				type =:type_type
				{$right_join_where}
		) AS pg ON pg.id = b.playedGroup";
$played_groups = $this->getRows(
	$sql,
	array(
		'type_id' => $type_id,
		'type_type' => $type['type'],
		'fromTime' => $fromTime,
		'toTime' => $toTime
	),
	1800,
	(!empty($_GET['flush_cache']) ? 1 : 0)
);

$order_by = isset($_GET['order_by']) ? $_GET['order_by'] : '';
switch ($order_by) {
	case 'unopenedBetCount':
		$column = 'unopenedBetCount';
		break;
	case 'unopenedBetAmount':
		$column = 'unopenedBetAmount';
		break;
	case 'betCount':
		$column = 'betCount';
		break;
	case 'betAmount':
		$column = 'betAmount';
		break;
	case 'zjAmount':
		$column = 'zjAmount';
		break;
	case 'fanDian':
		$column = 'fanDian';
		break;
	default:
		$column = 'income';
}
switch ((isset($_GET['sort']) ? $_GET['sort'] : '')) {
	case 'ASC':
		$sort = 'ASC';
		break;
	default:
		$sort = 'DESC';
}
$this->orderBy($played_groups, $column, $sort);
?>


<table class="table-sorter" cellspacing="0">
	<thead class="text-center">
		<tr>
			<td>玩法组</td>
			<td class="clickable sort<?php if ($order_by == 'unopenedBetCount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('unopenedBetCount')">未开奖数</td>
			<td class="clickable sort<?php if ($order_by == 'unopenedBetAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('unopenedBetAmount')">未开奖额</td>
			<td class="clickable sort<?php if ($order_by == 'betCount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('betCount')">有效笔数</td>
			<td class="clickable sort<?php if ($order_by == 'betAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('betAmount')">有效投注额</td>
			<td class="clickable sort<?php if ($order_by == 'zjAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('zjAmount')">中奖金额</td>
			<td class="clickable sort<?php if ($order_by == 'fanDian') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('fanDian')">公司返点</td>
			<td class="clickable sort<?php if ($order_by == 'income') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('income')">盈亏</td>
		</tr>
	</thead>
	<tbody class="text-right">
		<?php
		$total = array(
			'unopenedBetCount' => 0,
			'unopenedBetAmount' => 0,
			'betCount' => 0,
			'betAmount' => 0,
			'zjAmount' => 0,
			'fanDian' => 0,
			'income' => 0
		);
		foreach ($played_groups as $pg) {
			$unopenedBetCount = $pg['unopenedBetCount'] ? $pg['unopenedBetCount'] : 0;
			$unopenedBetAmount = $pg['unopenedBetAmount'] ? $pg['unopenedBetAmount'] : 0;
			$betCount = $pg['betCount'] ? $pg['betCount'] : 0;
			$betAmount = $pg['betAmount'] ? $pg['betAmount'] : 0;
			$zjAmount = $pg['zjAmount'] ? $pg['zjAmount'] : 0;
			$fanDian = $pg['fanDian'] ? $pg['fanDian'] : 0;
			$income = $pg['income'] ? $pg['income'] : 0;

			$total['unopenedBetCount'] += $unopenedBetCount;
			$total['unopenedBetAmount'] += $unopenedBetAmount;
			$total['betCount'] += $betCount;
			$total['betAmount'] += $betAmount;
			$total['zjAmount'] += $zjAmount;
			$total['fanDian'] += $fanDian;
			$total['income'] += $income;
		?>
			<tr>
				<td>
					<a href="report/incomeOfPlayed/<?php echo $pg['id']?>" onclick="prepareQueryString(this)"><?php echo $pg['groupName'] ?></a>
				</td>
				<td><?php echo number_format($unopenedBetCount) ?></td>
				<td><?php echo number_format($unopenedBetAmount, 3) ?></td>
				<td><?php echo number_format($betCount) ?></td>
				<td><?php echo number_format($betAmount, 3) ?></td>
				<td><?php echo number_format($zjAmount, 3) ?></td>
				<td><?php echo number_format($fanDian, 3) ?></td>
				<td class="<?php echo $income < 0 ? 'red' : '' ?>"><?php echo number_format($income, 3) ?></td>
			</tr>
		<?php
		}
		?>
	</tbody>
	<tfoot class="text-right">
		<tr>
			<td>总计</td>
			<td><?php echo number_format($total['unopenedBetCount']) ?></td>
			<td><?php echo number_format($total['unopenedBetAmount'], 3) ?></td>
			<td><?php echo number_format($total['betCount']) ?></td>
			<td><?php echo number_format($total['betAmount'], 3) ?></td>
			<td><?php echo number_format($total['zjAmount'], 3) ?></td>
			<td><?php echo number_format($total['fanDian'], 3) ?></td>
			<td class="<?php echo $total['income'] < 0 ? 'red' : '' ?>"><?php echo number_format($total['income'], 3) ?></td>
		</tr>
	</tfoot>
</table>
