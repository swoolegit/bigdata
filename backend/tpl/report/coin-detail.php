<?php
$where="";
$limit="";
$username="";
$userid="";
$url="";
// 时间限制
if(empty($_GET['fromTime']))
{
	$_GET['fromTime']=date("Y-m-d",strtotime("+0 day"))." 00:00:00";
}else
{
	$_GET['fromTime']=$_GET['fromTime'];
}
if(empty($_GET['toTime']))
{
	$_GET['toTime']=date("Y-m-d",strtotime("+0 day"))." 23:59:59";
}else
{
	$_GET['toTime']=$_GET['toTime'];
}

$fromTime = strtotime($_GET['fromTime']);
$toTime = strtotime($_GET['toTime']);
if ($fromTime && $toTime) {
    $where = " where u.regTime BETWEEN {$fromTime} AND {$toTime}";
} else {
	
	$limit=" limit 100 ";
}
$order1="amount desc";
$setsort="desc";
if(!empty($_GET['order']) && !empty($_GET['sort']))
{
	switch(true)
	{
		case $_GET['order']=="amount":
			$order1="amount ".$_GET['sort'];
			break;
		case $_GET['order']=="datetime":
			$order1="rdate ".$_GET['sort'];			
			break;
		case $_GET['order']=="name":
			$order1="username ".$_GET['sort'];
			break;
	}
	if($_GET['sort']=="desc")
	{
		$setsort="asc";
	}
}
if(!empty($_GET['username']))
{
	switch(true)
	{
		case $_GET['utype']=="1":
				$sql="select uid from {$this->prename}members where username='{$_GET['username']}'";
				$u=$this->getRow($sql);
				$username = " and r.username='{$_GET['username']}'";
				$userid = " and r.uid='{$u['uid']}'";
			break;
		case $_GET['utype']=="2":
				$sql="select uid from {$this->prename}members where username='{$_GET['username']}'";
				$u=$this->getRow($sql);
				
				$sql="select uid,username from {$this->prename}members where CONCAT(',', parents, ',') like '%,{$u['uid']},%'";
				$u=$this->getRows($sql);
				$uid_array=array();
				$uname_array=array();
				foreach($u as $k=>$v)
				{
					$uid_array[]=$v['uid'];
					$uname_array[]="'".$v['username']."'";
				} 
				$username = " and r.username in (".implode($uname_array, ",").")";
				$userid = " and r.uid in (".implode($uid_array, ",").")";
			break;
	}
}


switch(true)
{
	case $_GET['type']=="coin_atr":
		$sql="select
			r.rechargeTime as rdate,
			r.username as username,
			r.amount as amount
			FROM {$this->prename}member_recharge r where r.state =1 and rechargeModel=0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$username}
			order by {$order1}";
		$url="business/rechargeLogList?rechargeModel=0&state=1";
		break;
	case $_GET['type']=="coin_pay":
		$sql="select
			r.rechargeTime as rdate,
			r.username as username,
			r.amount as amount
			FROM {$this->prename}member_recharge r where r.state =1 and r.rechargeModel=1 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid} order by {$order1}";
		$url="business/rechargeLogList?rechargeModel=1&state=1";
		break;
	case $_GET['type']=="coin_admin":
		$sql="select
			r.rechargeTime as rdate,
			r.username as username,
			r.amount as amount
			FROM {$this->prename}member_recharge r where r.state=1 and r.rechargeModel=2 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid} order by {$order1}";
		$url="business/rechargeLogList?rechargeModel=2&state=1";
		break;
	case $_GET['type']=="coin_deduction":
		$sql="select
			r.actionTime as rdate,
			u.username as username,
			r.deduction*-1 as amount
			FROM {$this->prename}member_report r,{$this->prename}members u where r.deduction<0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} and u.uid=r.uid {$userid} order by {$order1}";
		$url="business/coinLogList?liqType=4";
		break;
	case $_GET['type']=="coout_cash":
		$sql="select
			r.actionTime as rdate,
			u.username as username,
			r.amount as amount
			FROM {$this->prename}member_cash r,{$this->prename}members u where r.state in (0,3) and r.actionTime BETWEEN {$fromTime} AND {$toTime} and u.uid=r.uid {$userid} order by {$order1}";
			$url="business/cashLogList?state=3";			
		break;
	case $_GET['type']=="coout_broker":
		$sql="select
			r.actionTime as rdate,
			u.username as username,
			r.broker as amount
			FROM {$this->prename}member_report r,{$this->prename}members u where r.broker>0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} and u.uid=r.uid {$userid} order by {$order1}";
		$url="business/coinLogList?liqType=99";
		break;
	case $_GET['type']=="coout_rebate":
		$sql="select
			r.actionTime as rdate,
			u.username as username,
			r.rebate as amount
			FROM {$this->prename}member_report r,{$this->prename}members u where r.rebate>0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} and u.uid=r.uid {$userid} order by {$order1}";
		$url="business/coinLogList?liqType=105";
		break;
	case $_GET['type']=="coout_bonus":
		$sql="select
			r.actionTime as rdate,
			u.username as username,
			r.bonus as amount
			FROM {$this->prename}member_report r,{$this->prename}members u where r.bonus>0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} and u.uid=r.uid {$userid} order by {$order1}";
		$url="business/coinLogList?liqType=3";
		break;
	case $_GET['type']=="coout_fandian":
		$sql="select
			r.actionTime as rdate,
			u.username as username,
			r.fandian as amount
			FROM {$this->prename}member_report r,{$this->prename}members u where r.fandian>0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} and u.uid=r.uid {$userid} order by {$order1}";
		$url="business/coinLogList?liqType=2";
		break;
												
}
$page = $this->getPage($sql, $this->page, $this->pageSize, $binds);

$rel = '/'.get_class($this) . '/coinDetail?' . http_build_query($_GET, '', '&');
?>
<table class="tablesorter" cellspacing="0">
    <thead>
        <tr>
            <th><a href="<?=$rel?>&order=datetime&sort=<?=$setsort?>" target="ajax" call="defaultList">日期</a></th>
            <th><a href="<?=$rel?>&order=name&sort=<?=$setsort?>" target="ajax" call="defaultList">用户名</a></th>
            <th><a href="<?=$rel?>&order=amount&sort=<?=$setsort?>" target="ajax" call="defaultList">金额</a></th>
        </tr>
    </thead>
    <tbody id="nav01">
        <?php
        foreach ($page['data'] as $v) {
        ?>
            <tr>
                <td><?=date("Y-m-d H:i:s",$v['rdate'])?></td>
                <td><?=$v['username']?></td>
                <td>
                	<a href="<?=$url?>&username=<?=$v['username']?>&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=$v['amount']?>
                	</a>
                </td>
            </tr>
        <?php
        }
        ?>
    </tbody>
</table>
<footer>
    <?php
    $rel = get_class($this) . '/coinDetail-{page}?' . http_build_query($_GET, '', '&');
    $this->display('inc/page.php', 0, $page['total'], $rel, 'defaultPageActionDiv');
    ?>
</footer>

