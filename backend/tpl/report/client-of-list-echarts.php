<?php
$fromTime = empty($_GET['fromTime']) ? strtotime(date('Y-m-d')) : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? strtotime(date('Y-m-d')) : strtotime($_GET['toTime']);


$sql =
	"SELECT
		r.date, r.newUser, r.betUser, r.LoginUser
	FROM {$this->prename}report_day r
		WHERE r.date BETWEEN '{$_GET['fromTime']}' and '{$_GET['toTime']}'
	order by date asc";
$days = $this->getRows($sql);

$sql =
	"SELECT
		r.date, r.newUser_web, r.newUser_mobile, r.betUser_web, r.betUser_mobile, r.betOldUser_web, r.betOldUser_mobile, r.betAmount_web, r.betAmount_mobile,
		r.zjAmount_web,r.zjAmount_mobile,r.rechargeAmount_web,r.rechargeAmount_mobile,r.cashAmount_web,r.cashAmount_mobile
	FROM {$this->prename}report_client r
		WHERE r.date BETWEEN '{$_GET['fromTime']}' and '{$_GET['toTime']}' 
	order by date asc";
$types = $this->getRows($sql);

$chart=array();
$chart['web']['tooltip']['show']=true;
$chart['web']['title']['text']='WEB端';
$chart['mobile']['tooltip']['show']=true;
$chart['mobile']['title']['text']='手机端';
$chart['analysis']['tooltip']['show']=true;
$chart['analysis']['title']['text']='总计图';
$chart['days']['tooltip']['show']=true;
$chart['days']['title']['text']='人数走势';
//$chart['calculable']=true;
$chart['web']['legend']['data']=array("注册人数","投注人数");
$chart['mobile']['legend']['data']=array("注册人数","投注人数");
$chart['analysis']['legend']['data']=array("WEB端","手机端");
$chart['days']['legend']['data']=array("注册人数","投注人数","登录人数");
//$chart['analysis']['legend']['data']=array("WEB端");
//$chart['analysis']['xAxis'][]=array('type'=>"category",'data'=>array("添加注册用户","当日活跃用户","老用户日活跃","有效投注","中奖金额","充值","提现"));
$chart['analysis']['xAxis'][]=array('type'=>"category",'data'=>array("注册人数","投注人数"));
$chart['web']['yAxis'][]=array('type'=>"value",'axisLabel'=>array('formatter'=>'{value}'));
$chart['mobile']['yAxis'][]=array('type'=>"value",'axisLabel'=>array('formatter'=>'{value}'));
$chart['analysis']['yAxis'][]=array('type'=>"value",'axisLabel'=>array('formatter'=>'{value}'));
$chart['days']['yAxis'][]=array('type'=>"value",'axisLabel'=>array('formatter'=>'{value}'));
//$chart['analysis']['series'][]=array('name'=>"WEB端",'type'=>"bar",'data'=>array(5, 20, 40, 10, 10, 20,1));
//$chart['analysis']['series'][]=array('name'=>"手机端",'type'=>"bar",'data'=>array(5, 20, 40, 10, 10, 20,1));
//$chart['analysis']['series']['name']="销量";
//$chart['analysis']['series']['type']="bar";
//$chart['analysis']['series']['data']=array(5, 20, 40, 10, 10, 20);
$title_data=array();
$newUser_data=array();
$betUser_data=array();
$loginUser_data=array();

$sum=array();
$sum['web']['newUser_web']=0;
$sum['web']['betUser_web']=0;
$sum['mobile']['newUser_web']=0;
$sum['mobile']['betUser_web']=0;
if($types)
{
	foreach ($types as $t) {
		$title_data[]=$t['date'];
		$newUser_data['web'][]=$t['newUser_web'];
		$betUser_data['web'][]=$t['betUser_web'];
	
		$newUser_data['mobile'][]=$t['newUser_mobile'];
		$betUser_data['mobile'][]=$t['betUser_mobile'];
		
		$sum['web']['newUser']+=$t['newUser_web'];
		$sum['web']['betUser']+=$t['betUser_web'];
	
		$sum['mobile']['newUser']+=$t['newUser_mobile'];
		$sum['mobile']['betUser']+=$t['betUser_mobile'];
	}
	$chart['web']['xAxis'][]=array('type'=>"category",'boundaryGap'=>"false",'data'=>$title_data);
	$chart['web']['series'][]=array('name'=>"注册人数",'type'=>"line",'data'=>$newUser_data['web']);
	$chart['web']['series'][]=array('name'=>"投注人数",'type'=>"line",'data'=>$betUser_data['web']);
	
	$chart['mobile']['xAxis'][]=array('type'=>"category",'boundaryGap'=>"false",'data'=>$title_data);
	$chart['mobile']['series'][]=array('name'=>"注册人数",'type'=>"line",'data'=>$newUser_data['mobile']);
	$chart['mobile']['series'][]=array('name'=>"投注人数",'type'=>"line",'data'=>$betUser_data['mobile']);
	
	$chart['analysis']['series'][]=array('name'=>"WEB端",'type'=>"bar",'data'=>array($sum['web']['newUser'],$sum['web']['betUser']));
	$chart['analysis']['series'][]=array('name'=>"手机端",'type'=>"bar",'data'=>array($sum['mobile']['newUser'],$sum['mobile']['betUser']));
}

if($days)
{
	$title_data=array();
	$newUser_data=array();
	$betUser_data=array();
	$loginUser_data=array();
	foreach ($days as $t) {
		$title_data[]=$t['date'];
		$newUser_data[]=$t['newUser'];
		$betUser_data[]=$t['betUser'];
		$loginUser_data[]=$t['LoginUser'];
	}
	$chart['days']['xAxis'][]=array('type'=>"category",'boundaryGap'=>"false",'data'=>$title_data);
	$chart['days']['series'][]=array('name'=>"注册人数",'type'=>"line",'data'=>$newUser_data);
	$chart['days']['series'][]=array('name'=>"投注人数",'type'=>"line",'data'=>$betUser_data);
	$chart['days']['series'][]=array('name'=>"登录人数",'type'=>"line",'data'=>$loginUser_data);
}


echo json_encode($chart);
?>
