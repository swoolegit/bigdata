<?php
$fromTime = empty($_GET['fromTime']) ? strtotime(date('Y-m-d 00:00:00')) : strtotime($_GET['fromTime']);
$toTime = empty($_GET['toTime']) ? strtotime(date('Y-m-d 23:59:59')) : strtotime($_GET['toTime']);
$sh = empty($_GET['sh']) ? 'type' : $_GET['sh'];
$play_id = empty($_GET['play_id']) ? '' : $_GET['play_id'];
if($sh=='type')
{

	$sql="select
		t.title,
		t.type,
		st.betAmount,
		st.orders as betCount,
		st.zjAmount,
		st.fandian as fanDian,
		st.rebate as rebate,
		IFNULL((st.betAmount - st.fandian - st.zjAmount - st.rebate ),0) as income	
		from 
		{$this->prename}type t left join 
		(
			select 
			rt.type,
			sum(rt.amount) as betAmount, 
			sum(rt.orders) as orders, 
			sum(rt.zjAmount) as zjAmount, 
			sum(rt.fandian) as fandian ,
			sum(rt.rebate) as rebate 
			from {$this->prename}report_type rt where rt.actionTime BETWEEN {$fromTime} AND {$toTime}  group by rt.type 
		) as st on t.id=st.type where t.enable=1
		";
	
	/*
	$sql="
		select 
		rt.type,
		rt.title,
		sum(rt.amount) as betAmount, 
		sum(rt.actionNum) as actionNum, 
		sum(rt.orders) as orders, 
		sum(rt.zjAmount) as zjAmount, 
		sum(rt.fandian) as fandian,
		sum(rt.rebate) as rebate 
		from {$this->prename}admin_report_type rt where rt.actionTime BETWEEN {$fromTime} AND {$toTime} group by rt.type 

	";
	*/
}else
{
	$sql="select
		t.name as title,
		t.id,
		st.betAmount,
		st.orders as betCount,
		st.zjAmount,
		st.fandian as fanDian,
		(st.betAmount - st.fandian - st.zjAmount ) as income	
		from 
		{$this->prename}played t left join 
		(
			select 
			rt.playId,
			sum(rt.amount) as betAmount, sum(rt.actionNum) as actionNum, sum(rt.orders) as orders, sum(rt.zjAmount) as zjAmount, sum(rt.fandian) as fandian 
			from {$this->prename}admin_report_play rt where rt.actionTime BETWEEN {$fromTime} AND {$toTime}  group by rt.playId 
		) as st on t.id=st.playId where t.enable=1 and t.type={$play_id}
		";	
}
$types = $this->getRows($sql);
$order_by = isset($_GET['order_by']) ? $_GET['order_by'] : '';
switch ($order_by) {
	case 'actionNum':
		$column = 'actionNum';
		break;
	case 'betCount':
		$column = 'betCount';
		break;
	case 'betAmount':
		$column = 'betAmount';
		break;
	case 'zjAmount':
		$column = 'zjAmount';
		break;
	case 'fanDian':
		$column = 'fanDian';
		break;
	default:
		$column = 'income';
}
switch ((isset($_GET['sort']) ? $_GET['sort'] : '')) {
	case 'ASC':
		$sort = 'ASC';
		break;
	default:
		$sort = 'DESC';
}
$this->orderBy($types, $column, $sort);
?>

<table class="table-sorter" cellspacing="0">
	<thead class="text-center">
		<tr>
			<?php
			if($sh=='type')
			{
			 	echo "<td>彩种</td>";
			}else
			{
				echo '<td class="clickable"> <a href="javascript:;" onclick="changeType()">玩法(返回彩种)</a></td>';
			}
			?>
			<td class="clickable sort<?php if ($order_by == 'betCount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('betCount')">注单数</td>
			<td class="clickable sort<?php if ($order_by == 'betAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('betAmount')">有效投注额</td>
			<td class="clickable sort<?php if ($order_by == 'zjAmount') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('zjAmount')">中奖金额</td>
			<td class="clickable sort<?php if ($order_by == 'fanDian') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('fanDian')">返点</td>
			<td class="clickable sort<?php if ($order_by == 'rebate') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('rebate')">退水</td>
			<td class="clickable sort<?php if ($order_by == 'income') {echo ($sort == 'ASC' ? ' asc' : ' desc');} ?>" onclick="orderBy('income')">盈亏 (%)</td>
		</tr>
	</thead>
	<tbody class="text-right">
		<?php
		$total = array(
			'betCount' => 0,
			'betAmount' => 0,
			'zjAmount' => 0,
			'fanDian' => 0,
			'income' => 0,
			'rebate' => 0,
		);
		foreach ($types as $t) {
			$betCount = $t['betCount'] ? $t['betCount'] : 0;
			$betAmount = $t['betAmount'] ? $t['betAmount'] : 0;
			$zjAmount = $t['zjAmount'] ? $t['zjAmount'] : 0;
			$fanDian = $t['fanDian'] ? $t['fanDian'] : 0;
			$income = $t['income'] ? $t['income'] : 0;
			$rebate = $t['rebate'] ? $t['rebate'] : 0;

			$total['betCount'] += $betCount;
			$total['betAmount'] += $betAmount;
			$total['zjAmount'] += $zjAmount;
			$total['fanDian'] += $fanDian;
			$total['income'] += $income;
			$total['rebate'] += $rebate;
		?>
			<tr>
				<td>
					<?php echo $t['title'] ?>
				</td>				
				<td><?php echo number_format($betCount) ?></td>
				<td><?php echo number_format($betAmount, 3) ?></td>
				<td><?php echo number_format($zjAmount, 3) ?></td>
				<td><?php echo number_format($fanDian, 3) ?></td>
				<td><?php echo number_format($rebate, 3) ?></td>
				<td class="<?php echo $income < 0 ? 'red' : '' ?>">
					<?php 
						echo number_format($income, 3);
						$temp = 0;
						if($income != 0)
						{
							$temp=round(((($income)/$betAmount)*100),2);
						}						
						echo " (".$temp."%)";
					?>
				</td>
				
			</tr>
	<?php } ?>
	</tbody>
	<tfoot class="text-right">
		<tr>
			<td>总计</td>
			<td><?php echo number_format($total['betCount']) ?></td>
			<td><?php echo number_format($total['betAmount'], 3) ?></td>
			<td><?php echo number_format($total['zjAmount'], 3) ?></td>
			<td><?php echo number_format($total['fanDian'], 3) ?></td>
			<td><?php echo number_format($total['rebate'], 3) ?></td>
			<td class="<?php echo $total['income'] < 0 ? 'red' : '' ?>"><?php echo number_format($total['income'], 3) ?></td>
		</tr>
	</tfoot>
</table>
