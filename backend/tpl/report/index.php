<?php
	if(!isset($this->page)) $this->page = 1;

	//今日帐差
	$sql = sprintf("
		SELECT *
		FROM `{$this->prename}report_amount_log`
		WHERE DATE_FORMAT(dateTime,'%%Y-%%m-%%d') = '%s'
		ORDER BY dateTime DESC",
		date('Y-m-d')
	);
	$result = $this->GetRows($sql);
	//只抓00:00:00到现在时间共二笔
	$todayResult = array(
		$result[0],
		$result[count($result)-1]
	);

	$sql = sprintf("
		SELECT *
		FROM  `{$this->prename}report_amount_log`
		ORDER BY id desc
		LIMIT %d , %d
	",	($this->page-1) * $this->pageSize, $this->pageSize);

	$result = $this->GetRows($sql);

	$sql = sprintf(" SELECT count(id) FROM {$this->prename}report_amount_log ");

	$total = $this->getValue($sql);




?>
<article class="module width_full">
	<header><h3 class="tabs_involved">每小时报表</h3></header>


<table class="tablesorter" cellspacing="0">
	<thead>
		<tr>
			<td>时间</td>
			<td width="90">投注总额</td>
			<td width="90">中奖总额</td>
			<td width="90">总返点</td>
			<td width="90" title="包括充值佣金，注册佣金，亏损佣金，消费佣金，签到">佣金及活动1</td>
			<td width="90">充值</td>
			<td width="90">提现</td>
			<td width="150">余额</td>
			<td width="80">会员盈亏</td>
			<td width="80">会员充提</td>
			<td width="80">隔日帐</td>
			<td width="80">余额差值</td>
		</tr>
	</thead>
	<tbody id="nav01">
	<?php
		for ($i=0; $i<count($todayResult); $i++) {
		//foreach ($result as $rows) {
			$rows = $todayResult[$i];
			$nextCoin = $result[$i+1]['coin'];
			$diffCoin = $todayResult[$i+1]['coin'] ? (int)($rows['coin'] - $todayResult[$i+1]['coin']) : 0;

			$memberWinLose = -($rows['betAmount'] - $rows['zjAmount'] - $rows['fanDianAmount'] - $rows['brokerageAmount']);
			$memberRecharge = $rows['rechargeAmount'] - $rows['cashAmount'];
			$memberDiff = $memberWinLose + $memberRecharge;
			$totalDiff = sprintf("%.02f",$diffCoin - $memberDiff);
	?>
		<tr>
			<td><?=$rows['dateTime']?></td>
			<td><?=$rows['betAmount']?></td>
			<td><?=$rows['zjAmount']?></td>
			<td><?=$rows['fanDianAmount']?></td>

			<td><?=$rows['brokerageAmount']?></td>
			<td><?=$rows['rechargeAmount']?></td>
			<td><?=$rows['cashAmount']?></td>
			<td><?=$rows['coin']?> / <span class="<?=($diffCoin<0? 'spn2':'')?>"><?=$diffCoin?></span></td>
			<td><span class="<?=($memberWinLose<0? 'spn2':'')?>"><?=$memberWinLose?></span></td>
			<td><span class="<?=($memberRecharge<0? 'spn2':'')?>"><?=$memberRecharge?></span></td>
			<td><span class="<?=($rows['yestodayAmount']<0? 'spn2':'')?>"><?=$rows['yestodayAmount']?></span></td>
			<td><span class="<?=($totalDiff<0? 'spn2':'')?>"><?=$totalDiff?></span></td>

		</tr>
	<?php } ?>


	</tbody>
</table>



<table class="tablesorter" cellspacing="0">
	<thead>
		<tr>
			<td>时间</td>
			<td width="90">投注总额</td>
			<td width="90">中奖总额</td>
			<td width="90">总返点</td>
			<td width="90" title="包括充值佣金，注册佣金，亏损佣金，消费佣金，签到">佣金及活动1</td>
			<td width="90">充值</td>
			<td width="90">提现</td>
			<td width="150">余额</td>
			<td width="80">会员盈亏</td>
			<td width="80">会员充提</td>
			<td width="80">隔日帐</td>
			<td width="80">余额差值</td>
		</tr>
	</thead>
	<tbody id="nav02">
	<?php
		for ($i=0; $i<count($result); $i++) {
		//foreach ($result as $rows) {
			$rows = $result[$i];
			$nextCoin = $result[$i+1]['coin'];
			//$diffCoin = (int)($rows['coin'] - $result[$i+1]['coin']);
			$diffCoin = $result[$i+1]['coin'] ? (int)($rows['coin'] - $result[$i+1]['coin']) : 0;

			$memberWinLose = -($rows['betAmount'] - $rows['zjAmount'] - $rows['fanDianAmount'] - $rows['brokerageAmount']);
			$memberRecharge = $rows['rechargeAmount'] - $rows['cashAmount'];
			$memberDiff = $memberWinLose + $memberRecharge;
			$totalDiff = sprintf("%.02f",$diffCoin - $memberDiff);

	?>
		<tr>
			<td><?=$rows['dateTime']?></td>
			<td><?=$rows['betAmount']?></td>
			<td><?=$rows['zjAmount']?></td>
			<td><?=$rows['fanDianAmount']?></td>

			<td><?=$rows['brokerageAmount']?></td>
			<td><?=$rows['rechargeAmount']?></td>
			<td><?=$rows['cashAmount']?></td>
			<td><?=$rows['coin']?> / <span class="<?=($diffCoin<0? 'spn2':'')?>"><?=$diffCoin?></span></td>
			<td><span class="<?=($memberWinLose<0? 'spn2':'')?>"><?=$memberWinLose?></span></td>
			<td><span class="<?=($memberRecharge<0? 'spn2':'')?>"><?=$memberRecharge?></span></td>
			<td><span class="<?=($rows['yestodayAmount']<0? 'spn2':'')?>"><?=$rows['yestodayAmount']?></span></td>
			<td><span class="<?=($totalDiff<0? 'spn2':'')?>"><?=$totalDiff?></span></td>

		</tr>
	<?php } ?>


	</tbody>
</table>
<footer>
	<?php
		$rel= 'report/index-{page}?'.http_build_query($_GET,'','&');
		$this->display('inc/page.php', 0, $total, $rel, 'defaultReplacePageAction');
	?>
</footer>
</article>
<script type="text/javascript">
ghhs("nav01","tr");
ghhs("nav02","tr");
</script>