<?php
$where="";
$limit="";
$username="";
$userid="";
// 时间限制
if(empty($_GET['fromTime']))
{
	$_GET['fromTime']=date("Y-m-d",strtotime("+0 day"))." 00:00:00";
}else
{
	$_GET['fromTime']=$_GET['fromTime'];
}
if(empty($_GET['toTime']))
{
	$_GET['toTime']=date("Y-m-d",strtotime("+0 day"))." 23:59:59";
}else
{
	$_GET['toTime']=$_GET['toTime'];
}
$fromTime = strtotime($_GET['fromTime']);
$toTime = strtotime($_GET['toTime']);
if ($fromTime && $toTime) {
    $where = " where u.regTime BETWEEN {$fromTime} AND {$toTime}";
} else {
	
	$limit=" limit 100 ";
}
if(!empty($_GET['username']))
{
	switch(true)
	{
		case $_GET['utype']=="1":
				$sql="select uid from {$this->prename}members where username='{$_GET['username']}'";
				$u=$this->getRow($sql);
				$username = " and r.username='{$_GET['username']}'";
				$userid = " and r.uid='{$u['uid']}'";
			break;
		case $_GET['utype']=="2":
				$sql="select uid from {$this->prename}members where username='{$_GET['username']}'";
				$u=$this->getRow($sql);
				
				$sql="select uid,username from {$this->prename}members where CONCAT(',', parents, ',') like '%,{$u['uid']},%'";
				$u=$this->getRows($sql);
				$uid_array=array();
				$uname_array=array();
				foreach($u as $k=>$v)
				{
					$uid_array[]=$v['uid'];
					$uname_array[]="'".$v['username']."'";
				} 
				$username = " and r.username in (".implode($uname_array, ",").")";
				$userid = " and r.uid in (".implode($uid_array, ",").")";
			break;
	}
}

$coin=array();

$sql="select
		count(id) as t,
		ifnull(sum(r.amount),0) as recharge,
		ifnull(count(DISTINCT r.uid),0) as p
		FROM {$this->prename}member_recharge r where r.state =1 and rechargeModel=0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid}";
$sum=$this->getRow($sql);
$coin["atr"]=array(
		"t"=>$sum["t"],
		"val"=>$sum["recharge"],
		"p"=>$sum["p"]
		);

$sql="select
		count(id) as t,
		ifnull(sum(r.amount),0) as recharge,
		ifnull(count(DISTINCT r.uid),0) as p
		FROM {$this->prename}member_recharge r where r.state =1 and r.rechargeModel=1 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid}";
$sum=$this->getRow($sql);
$coin["pay"]=array(
		"t"=>$sum["t"],
		"val"=>$sum["recharge"],
		"p"=>$sum["p"]
		);

$sql="select
		count(id) as t,
		ifnull(sum(r.amount),0) as recharge,
		ifnull(count(DISTINCT r.uid),0) as p
		FROM {$this->prename}member_recharge r where r.state=1 and r.rechargeModel=2 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid}";
$sum=$this->getRow($sql);
$coin["admin_pay"]=array(
		"t"=>$sum["t"],
		"val"=>$sum["recharge"],
		"p"=>$sum["p"]
		);

$sql="select
		count(id) as t,
		ifnull(sum(r.deduction),0) as deduction,
		ifnull(count(DISTINCT r.uid),0) as p
		FROM {$this->prename}member_report r where r.deduction<0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid}";
$sum=$this->getRow($sql);
$coin["deduction"]=array(
		"t"=>$sum["t"],
		"val"=>$sum["deduction"]*-1,
		"p"=>$sum["p"]
		);
		
$sql="select
		count(id) as t,
		ifnull(sum(r.amount),0) as cash,
		ifnull(count(DISTINCT r.uid),0) as p
		FROM {$this->prename}member_cash r where r.state in (0,3) and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid}";
$sum=$this->getRow($sql);
$coout["cash"]=array(
		"t"=>$sum["t"],
		"val"=>$sum["cash"],
		"p"=>$sum["p"]
		);

$sql="select
		count(id) as t,
		ifnull(sum(r.broker),0) as broker,
		ifnull(count(DISTINCT r.uid),0) as p
		FROM {$this->prename}member_report r where broker>0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid}";
$sum=$this->getRow($sql);
$coout["broker"]=array(
		"t"=>$sum["t"],
		"val"=>$sum["broker"],
		"p"=>$sum["p"]
		);
				
$sql="select
		count(id) as t,
		ifnull(sum(r.rebate),0) as rebate,
		ifnull(count(DISTINCT r.uid),0) as p
		FROM {$this->prename}member_report r where rebate>0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid}";
$sum=$this->getRow($sql);
$coout["rebate"]=array(
		"t"=>$sum["t"],
		"val"=>$sum["rebate"],
		"p"=>$sum["p"]
		);
				
$sql="select
		count(id) as t,
		ifnull(sum(r.bonus),0) as bonus,
		ifnull(count(DISTINCT r.uid),0) as p
		FROM {$this->prename}member_report r where bonus>0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid}";
$sum=$this->getRow($sql);
$coout["bonus"]=array(
		"t"=>$sum["t"],
		"val"=>$sum["bonus"],
		"p"=>$sum["p"]
		);

$sql="select
		count(id) as t,
		ifnull(sum(r.fandian),0) as fandian,
		ifnull(count(DISTINCT r.uid),0) as p
		FROM {$this->prename}member_report r where fandian>0 and r.actionTime BETWEEN {$fromTime} AND {$toTime} {$userid}";
$sum=$this->getRow($sql);
$coout["fandian"]=array(
		"t"=>$sum["t"],
		"val"=>$sum["fandian"],
		"p"=>$sum["p"]
		);


$acoin=$coin["atr"]["val"]+$coin["pay"]["val"]+$coin["admin_pay"]["val"]+$coin["deduction"]["val"];
$acoout=$coout["cash"]["val"]+$coout["broker"]["val"]+$coout["rebate"]["val"]+$coout["bonus"]["val"]+$coout["fandian"]["val"];
$amout=array(
	"coin"=>$acoin,
	"coout"=>$acoout,
	"set"=>$acoin-$acoout,
);

$acoin2=$coin["atr"]["val"]+$coin["pay"]["val"];
$acoout2=$coout["cash"]["val"];
$amout2=array(
	"coin"=>$acoin2,
	"coout"=>$acoout2,
	"set"=>$acoin2-$acoout2,
);

?>

<table class="tablesorter" cellspacing="0">
    <thead>
        <tr>
            <th>收入</th>
            <th>收入金额</th>
            <th>支出</th>
            <th>支出金额</th>
        </tr>
    </thead>
    <tbody id="nav01">
            <tr>
                <td>公司汇款</td>
                <td>
                	<a href="report/coinDetail?username=<?=$_GET['username']?>&type=coin_atr&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>&utype=<?=$_GET['utype']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=number_format($coin["atr"]["val"],2)?> (<?=$coin["atr"]["t"]?>笔)(<?=$coin["atr"]["p"]?>人)
                	</a>
                </td>
                <td>会员出款</td>
                <td>
                	<a href="report/coinDetail?username=<?=$_GET['username']?>&type=coout_cash&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>&utype=<?=$_GET['utype']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=number_format($coout["cash"]["val"],2)?> (<?=$coout["cash"]["t"]?>笔)(<?=$coout["cash"]["p"]?>人)
                	</a>
                </td>
            </tr>
            <tr>
                <td>三方支付</td>
                <td>
                	<a href="report/coinDetail?username=<?=$_GET['username']?>&type=coin_pay&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>&utype=<?=$_GET['utype']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=number_format($coin["pay"]["val"],2)?> (<?=$coin["pay"]["t"]?>笔)(<?=$coin["pay"]["p"]?>人)
                	</a>
                </td>
                <td>活动优惠</td>
                <td>
                	<a href="report/coinDetail?username=<?=$_GET['username']?>&type=coout_broker&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>&utype=<?=$_GET['utype']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=number_format($coout["broker"]["val"],2)?> (<?=$coout["broker"]["t"]?>笔)(<?=$coout["broker"]["p"]?>人)
			        </a>
                </td>
            </tr>
            <tr>
                <td>管理员存入</td>
                <td>
                	<a href="report/coinDetail?username=<?=$_GET['username']?>&type=coin_admin&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>&utype=<?=$_GET['utype']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=number_format($coin["admin_pay"]["val"],2)?> (<?=$coin["admin_pay"]["t"]?>笔)(<?=$coin["admin_pay"]["p"]?>人)
                	</a>
                </td>
                <td>给予会员退水</td>
                <td>
                	<a href="report/coinDetail?username=<?=$_GET['username']?>&type=coout_rebate&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>&utype=<?=$_GET['utype']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=number_format($coout["rebate"]["val"],2)?> (<?=$coout["rebate"]["t"]?>笔)(<?=$coout["rebate"]["p"]?>人)
                	</a>
                </td>
            </tr>
            <tr>
                <td>人工扣除金额</td>
                <td>
                	<a href="report/coinDetail?username=<?=$_GET['username']?>&type=coin_deduction&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>&utype=<?=$_GET['utype']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=number_format($coin["deduction"]["val"],2)?> (<?=$coin["deduction"]["t"]?>笔)(<?=$coin["deduction"]["p"]?>人)
                	</a>
                </td>
                <td>给予代理分红</td>
                <td>
                	<a href="report/coinDetail?username=<?=$_GET['username']?>&type=coout_bonus&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>&utype=<?=$_GET['utype']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=number_format($coout["bonus"]["val"],2)?> (<?=$coout["bonus"]["t"]?>笔)(<?=$coout["bonus"]["p"]?>人)
                	</a>
                </td>
            </tr>
            <tr>
				<td></td>
				<td></td>
                <td>给予代理反点</td>
                <td>
                	<a href="report/coinDetail?username=<?=$_GET['username']?>&type=coout_fandian&fromTime=<?=$_GET['fromTime']?>&toTime=<?=$_GET['toTime']?>&utype=<?=$_GET['utype']?>" target="ajax" dataType="html" onajax="replaceQueryStringState" call="defaultList">
                	<?=number_format($coout["fandian"]["val"],2)?> (<?=$coout["fandian"]["t"]?>笔)(<?=$coout["fandian"]["p"]?>人)
                	</a>
                </td>
            </tr>


    </tbody>
</table>
<div>
<font><b>结算法[1]:</b></font>
<p style="text-align:left;"><span style="font-size:15px;">
	总计: 收入 <span style="color:red;"><?=number_format($amout["coin"],2)?></span> 元 , 
	支出 <span style="color:green;"><?=number_format($amout["coout"],2)?></span> 元 ,
	结算金额  
	<?php
	if($amout["coin"] >= $amout["coout"])
	{
		echo '<span style="color:red;">';
	}else
	{
		echo '<span style="color:green;">';
	}
	?>
	 <?=number_format($amout["set"],2)?></span> 元 
</p>
<p style="text-align:left;"><span style="font-size:15px;">公式: 公司入款 + 线上支付 + 人工存入+ 人工扣除金额 - 会员出款 - 活动优惠 -给予会员退水 -  给予代理分红   -  给予代理反点=  结算金额</p>
</div>

<div>
<font><b>结算法[2]:</b></font>
<p style="text-align:left;"><span style="font-size:15px;">
	总计: 收入 <span style="color:red;"><?=number_format($amout2["coin"],2)?></span> 元 , 
	支出 <span style="color:green;"><?=number_format($amout2["coout"],2)?></span> 元 ,
	结算金额  
	<?php
	if($amout2["coin"] >= $amout2["coout"])
	{
		echo '<span style="color:red;">';
	}else
	{
		echo '<span style="color:green;">';
	}
	?>
	 <?=number_format($amout2["set"],2)?></span> 元 
</p>
<p style="text-align:left;"><span style="font-size:15px;">公式: 公司入款 + 线上支付  - 会员出款   =  结算金额</p>
</div>