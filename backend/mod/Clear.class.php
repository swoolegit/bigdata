<?php

class Clear extends AdminBase
{
    public final function rmfile()
    {
        $dir = $GLOBALS['conf']['cache']['dir'];
        $dh = opendir($dir);
        while ($file = readdir($dh)) {
            if ($file != '.' && $file != '..') {
                $fullpath = $dir . '/' . $file;
                if (!is_dir($fullpath)) {
                    @unlink($fullpath);
                } else {
                    @deldir($fullpath);
                }
            }
        }
        closedir($dh);

        $dir = $GLOBALS['conf']['cache']['front_dir'];
        $dh = opendir($dir);
        while ($file = readdir($dh)) {
            if ($file != '.' && $file != '..') {
                $fullpath = $dir . '/' . $file;
                if (!is_dir($fullpath)) {
                    @unlink($fullpath);
                } else {
                    @deldir($fullpath);
                }
            }
        }
        closedir($dh);

        // if (@rmdir($dir)) {
        //     throw new Exception('清除成功');
        // } else {
        //     throw new Exception('清除失败');
        // }
    }
}

?>