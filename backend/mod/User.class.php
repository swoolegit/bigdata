<?php

class user extends db
{
    private $vcodeSessionName = 'vcode-session-name';

    public function _generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function __construct($dsn, $user = '', $password = '')
    {
        if (!session_id()) session_start();
        parent::__construct($dsn, $user, $password);

        if (!isset($_SESSION[$this->vcodeSessionName])) {
            $_SESSION[$this->vcodeSessionName] = $this->_generateRandomString();
        }
    }

    public final function login()
    {

        //verify IP
        $user_ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

		$user_ips = [];
        if (strpos($user_ip, ',') === FALSE) {
            $user_ips = [$user_ip];
        } else {
            $user_ips = explode(',', $user_ip);
        }
        $allowed_ips = ',' . $GLOBALS['ALLOWED_IPS'] . ',';
        $ip_allows_through = false;

        foreach ($user_ips as $ip) {
            if (strrpos($allowed_ips, $ip) !== FALSE) {
                $ip_allows_through = true;
                break;
            }
        }
		$ip_allows_through = true; //强制通过IP验证  by robert
        if (!$ip_allows_through) {
            throw new Exception('IP:' . $user_ip);
        }
        header('content-Type: text/html;charset=utf8');
        $this->display('login.php');
    }

    public final function logined()
    {
        //verify IP
        $user_ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        $user_ips = [];
        if (strpos($user_ip, ',') === FALSE) {
            $user_ips = [$user_ip];
        } else {
            $user_ips = explode(',', $user_ip);
        }
        $allowed_ips = ',' . $GLOBALS['ALLOWED_IPS'] . ',';
        $ip_allows_through = false;
        foreach ($user_ips as $ip) {
            if (strrpos($allowed_ips, $ip) !== FALSE) {
                $ip_allows_through = true;
                break;
            }
        }
		$ip_allows_through = true; //强制通过IP验证  by robert
        if (!$ip_allows_through) {
            throw new Exception('IP:' . $user_ip);
        }

        header('content-Type: text/html;charset=utf8');

        $_SESSION['checkLogined_csrf'] = $this->_generateRandomString();
        $this->checkLogined_csrf = $_SESSION['checkLogined_csrf'];
        $this->display('logined.php');
    }

    public final function logout()
    {
        $_SESSION = array();
        if ($this->user['uid']) {
            $this->update("update {$this->prename}admin_session set isOnLine=0 where uid={$this->user['uid']}");
        }
        header('location: /user/login');
    }

    private function getBrowser()
    {
        $flag = $_SERVER['HTTP_USER_AGENT'];
        $para = array();
        if (preg_match('/Windows[\d\. \w]*/', $flag, $match)) $para['os'] = $match[0];
        if (preg_match('/Chrome\/[\d\.\w]*/', $flag, $match)) {
            $para['browser'] = $match[0];
        } elseif (preg_match('/Safari\/[\d\.\w]*/', $flag, $match)) {
            $para['browser'] = $match[0];
        } elseif (preg_match('/MSIE [\d\.\w]*/', $flag, $match)) {
            $para['browser'] = $match[0];
        } elseif (preg_match('/Opera\/[\d\.\w]*/', $flag, $match)) {
            $para['browser'] = $match[0];
        } elseif (preg_match('/Firefox\/[\d\.\w]*/', $flag, $match)) {
            $para['browser'] = $match[0];
        } elseif (preg_match('/OmniWeb\/(v*)([^\s|;]+)/i', $flag, $match)) {
            $para['browser'] = $match[2];
        } elseif (preg_match('/Netscape([\d]*)\/([^\s]+)/i', $flag, $match)) {
            $para['browser'] = $match[2];
        } elseif (preg_match('/Lynx\/([^\s]+)/i', $flag, $match)) {
            $para['browser'] = $match[1];
        } elseif (preg_match('/360SE/i', $flag, $match)) {
            $para['browser'] = '360安全浏览器';
        } elseif (preg_match('/SE 2.x/i', $flag, $match)) {
            $para['browser'] = '搜狗浏览器';
        } else {
            $para['browser'] = 'unkown';
        }
        return $para;
    }


    public final function checkLogined()
    {
        $username = wjStrFilter($_POST['username']);
        $vcode = wjStrFilter($_POST['vcode']);
        if (!preg_match('/^\w{4,16}$/', $username)) throw new Exception('用户名包含非法字符,请重新输入');
        if (!$username) {
            throw new Exception('用户名不能为空');
        }

        if (strtolower($vcode) != $_SESSION[$this->vcodeSessionName]) {
            throw new Exception('验证码不正确。');
        }
        $_SESSION[$this->vcodeSessionName] = $this->_generateRandomString();

        $sql = "select * from {$this->prename}admin_members where isDelete=0 and username=? LIMIT 0,1";
        if (!$user = $this->getRow($sql, $username)) {
            throw new Exception('用户名不正确');
        }
        if ($username != $user['username']) {
            throw new Exception('用户名不正确。');
        }
        if (!$user['enable']) {
            throw new Exception('您的帐号被冻结，请联系管理员。');
        }
        setcookie('username', $username);
    }

    public final function checkLogin()
    {
        $safe_username = wjStrFilter($_POST['username']);
        //$safe_password = wjStrFilter($_POST['password']);
        //$safe_safepass = wjStrFilter($_POST['safepass']);
        $safe_password = $_POST['password'];
        $safe_safepass = $_POST['safepass'];
        $vcode = wjStrFilter($_POST['vcode']);
        $unsafe_csrf = $_POST['csrf'];

        if ($_SESSION['checkLogined_csrf'] != $unsafe_csrf) {
            throw new Exception('浏览器 Cookie 没启用。');
        }
        // if (strtolower($vcode) != $_SESSION[$this->vcodeSessionName]) {
        //     throw new Exception('验证码不正确。');
        // }
        $_SESSION[$this->vcodeSessionName] = $this->_generateRandomString();

        if (!$safe_username) {
            throw new Exception('用户名不能为空');
        }
        if (!$safe_password) {
            throw new Exception('不允许空密码登录');
        }
        if (md5($safe_safepass) != md5($GLOBALS['conf']['safepass'])) {
            throw new Exception('安全码不正确');
        }
        if (!preg_match('/^\w{4,16}$/', $safe_username)) throw new Exception('用户名包含非法字符,请重新输入');
        $sql = "select * from {$this->prename}admin_members where isDelete=0 and username=? LIMIT 1";
        if (!$user = $this->getRow($sql, $safe_username)) {
            throw new Exception('用户名或密码不正确');
        }
        if ($safe_username != $user['username']) {
            throw new Exception('用户名不正确。');
        }
		//echo md5($safe_password);
        if (md5($safe_password) != $user['password']) {
            throw new Exception('用户名或密码不正确');
        }
        if (!$user['enable']) {
            throw new Exception('您的帐号被冻结，请联系管理员。');
        }
        $session = array(
            'uid' => $user['uid'],
            'username' => $user['username'],
            'session_key' => session_id(),
            'loginTime' => $this->time,
            'accessTime' => $this->time,
            'loginIP' => self::ip(true)
        );
        $session = array_merge($session, $this->getBrowser());
        if ($this->insertRow($this->prename . 'admin_session', $session)) {
            $user['sessionId'] = $this->lastInsertId();
        }
        $_SESSION['_ADMIN_'] = serialize($user);
        $this->update("update {$this->prename}admin_session set isOnLine=0,state=1 where uid={$user['uid']} and id<{$user['sessionId']}");
        return $user;
    }

    public final function vcode($rmt = null)
    {
        $lottery_lib_path = ADMIN_ROOT . '/lib/';
        include_once $lottery_lib_path . 'CImage.lib.php';
         $width = 72;
         $height = 24;
        $img = new CImage($width, $height);
        $img->sessionName = $this->vcodeSessionName;
        $img->printimg('png');
    }
}

?>