<?php

class System extends AdminBase
{
    public $pageSize = 15;

    public final function settings()
    {
        $this->display('system/settings.php');
    }

    public final function upSettings()
    {
        if (!$para = $_POST) throw new Exception('参数出错');
        if (!ctype_digit($para['czzs']) || $para['czzs'] > 100 || $para['czzs'] < 0) throw new Exception('请正确输入充值赠送本人百分比!');
        if (!ctype_digit($para['czzsParent']) || $para['czzsParent'] > 100 || $para['czzsParent'] < 0) throw new Exception('请正确输入充值赠送上家百分比!');
        if (!ctype_digit($para['czzsTop']) || $para['czzsTop'] > 100 || $para['czzsTop'] < 0) throw new Exception('请正确输入充值赠送上上家百分比!');
        if (!ctype_digit($para['cashMinAmount']) || $para['cashMinAmount'] > 1000 || $para['cashMinAmount'] < 0) throw new Exception('请正确输入消费百分比!');
        // if (!ctype_digit($para['betMaxCount'])) throw new Exception('请正确设置最大注数!');
        // if (!ctype_digit($para['betMaxZjAmount'])) throw new Exception('请正确设置最大中奖金额!');
        if (!ctype_digit($para['rechargeMin'])) throw new Exception('请正确设置最低充值金额!');
        if (!ctype_digit($para['rechargeMax'])) throw new Exception('请正确设置最高充值金额!');
        if (!ctype_digit($para['cashMin'])) throw new Exception('请正确设置最低提现金额!');
        if (!ctype_digit($para['cashMax'])) throw new Exception('请正确设置最高提现金额!');
        // if (!ctype_digit($para['clearMemberCoin'])) throw new Exception('请正确设置清理账号规则金额!');
        // if (!ctype_digit($para['clearMemberDate'])) throw new Exception('请正确设置清理账号规则时间!');
        // if (!ctype_digit($para['huoDongRegister'])) throw new Exception('请正确设置绑定工行赠送金额!');
        //if (!ctype_digit($para['huoDongSign'])) throw new Exception('请正确设置每天签到赠送金额!');
        if (!ctype_digit($para['rechargeCommissionAmount'])) throw new Exception('请正确设置充值佣金金额!');
        if (!ctype_digit($para['rechargeSelfCommission'])) throw new Exception('请正确设置充值佣金自身赠送金额!');
        if (!ctype_digit($para['rechargeCommission'])) throw new Exception('请正确设置充值佣金上家赠送金额!');
        if (!ctype_digit($para['rechargeCommission2'])) throw new Exception('请正确设置充值佣金上上家赠送金额!');
        if (!ctype_digit($para['conCommissionBase'])) throw new Exception('请正确设置消费佣金金额!');
        if (!ctype_digit($para['conCommissionSelfAmount'])) throw new Exception('请正确设置消费佣金自身赠送金额!');
        if (!ctype_digit($para['conCommissionParentAmount'])) throw new Exception('请正确设置消费佣金上家赠送金额!');
        if (!ctype_digit($para['conCommissionParentAmount2'])) throw new Exception('请正确设置消费佣金上上家赠送金额!');

        $para['yuanmosi'] = isset($para['yuanmosi']) ? 1 : 0;
        $para['jiaomosi'] = isset($para['jiaomosi']) ? 1 : 0;
        $para['fenmosi'] = isset($para['fenmosi']) ? 1 : 0;
        $para['limosi'] = isset($para['limosi']) ? 1 : 0;

        if(!$para['cashFlowJson']) $para['cashFlow'] = '';


        // $para['lottoBenefit'] = intval($para['lottoBenefit']);
        // if ($para['lottoBenefit'] < -10 || $para['lottoBenefit'] > 100) throw new Exception('请正确设置自开彩获利值!');
        /*
        if (!ctype_digit($para['LiRunLv'])) throw new Exception('请正确设置系统彩利率!');
        if (!is_numeric($para['LiRunLv'])) throw new Exception('请正确设置系统彩利率!');
        */

        $values = array();
        $bind = array();
        $settings_diff = array();

        $i = 0;
        foreach ($para as $key => $var) {
            // 改用 bind 方式，SQL 不用加 slash (ht/safe.php)
            $var = stripslashes($var);
            if ($var == $this->settings[$key]) {
                continue;
            }

            $i++;
            $values[] = "(?, ?)";
            array_push($bind, $key, $var);

            // 记录差异
            $settings_diff[$key]['old'] = $this->settings[$key];
            $settings_diff[$key]['new'] = $var;
        }
        if (!$i) {
            throw new Exception('数据没有改变');
        }
        if(isset($para['rebate']))
        {
            $sql="update {$this->prename}members set rebate = ".$para['rebate'];
            $this->query($sql);
        }

        $sql = "INSERT INTO {$this->prename}params (`name`, `value`) VALUES ";
        $sql .= implode(', ', $values);
        $sql .= ' ON DUPLICATE KEY UPDATE `value` =VALUES(`value`)';

        if ($this->insert($sql, $bind)) {
            $this->addLog(10, $this->adminLogType[10], 0, '', json_encode($settings_diff));
            return $this->getSystemSettings(0);
        } else {
            throw new Exception('未知错误');
        }
    }

    public final function notice()
    {
        $this->display('system/notice.php');
    }

    public final function addNotice()
    {
        $this->display('system/notice-add.php');
    }

    public final function doAddNotice()
    {
        $para = array_merge($_POST, array(
            'addTime' => $this->time,
        ));
        if (!$para['title']) throw new Exception('公告标题不能为空');
        if (!$para['content']) throw new Exception('公告内容不能为空');
        if ($this->insertRow($this->prename . 'content', $para)) {
            return '添加公告成功';
        } else {
            throw new Exception('未知出错');
        }
    }

    public final function upNotice($id)
    {
        $this->display('system/notice-update.php', 0, $id);
    }

    public final function doupNotice($id)
    {
        $_POST['addTime'] = strtotime($_POST['addTime']);
        if (!$_POST['title']) throw new Exception('公告标题不能为空');
        if (!$_POST['content']) throw new Exception('公告内容不能为空');
        if ($this->updateRows($this->prename . 'content', $_POST, 'id=' . $id)) {
            return '修改公告成功';
        } else {
            throw new Exception('未知出错');
        }
    }

    public final function delNotice($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        $sql = "delete from {$this->prename}content where id=?";
        if ($this->delete($sql, $id)) {
            echo '公告已经删除';
        } else {
            throw new Exception('未知出错');
        }
    }

    public final function bank()
    {
        $this->display('system/bank-list.php');
    }

    public final function thirdpay()
    {
        $this->display('system/thirdpay-list.php');
    }
    public final function thirdpayModal($id)
    {
        $this->display('system/thirdpay-modal.php', 0, $id);
    }

    public final function upthirdpay()
    {
        $para = $_POST;
        $sql = "insert into {$this->prename}params_thirdpay set";
        foreach ($para as $field => $var) {
            $sql .= " `$field`='$var',";
        }
        $sql = rtrim($sql, ',');        
        $sql .= ' on duplicate key update 
        `name`=values(`name`), `merid`=values(`merid`), `addcrykey`=values(`addcrykey`), `memo`=values(`memo`)
        , `payout_status`=values(`payout_status`), `payin_status`=values(`payin_status`), `lvmin`=values(`lvmin`), `lvmax`=values(`lvmax`)';
        if ($this->insert($sql, $para)) {
            $addbankId = $this->lastInsertId();
            $this->addLog(91, $this->adminLogType[91] . '[' . $this->iff($para['id'], '修改', '添加') . 'ID:' . $addbankId . ']', $addbankId, '三方设置');
            $upstatus=[];
            if($para['payout_status']==1)
            {
                $upstatus[]="payout_status=0";
            }
            // if($para['payin_status']==1)
            // {
            //     $upstatus[]="payin_status=0";
            // }
            if($para['id']==0)
            {
                $para['id']=$addbankId;
            }
            if(count($upstatus)>0)
            {
                $sql="update {$this->prename}params_thirdpay set ".implode(",",$upstatus)." where id<>".$para['id'];
                $this->update($sql);
            }
            $fun = 'success';
            $msg = '操作成功';
        } else {
            $fun = 'error';
            $msg = '未知错误';
        }
        
    }

    public final function promotion()
    {
        $this->display('system/promotion-list.php');
    }
    
    public final function sysbanklist()
    {
        $this->display('system/sysbank-list.php');
    }

    public final function sysbankModal($id)
    {
        $this->display('system/sysbank-modal.php', 0, $id);
    }

    public final function bankModal($id)
    {
        $this->display('system/bank-modal.php', 0, $id);
    }

    public final function upBank()
    {
        if(isset($_FILES["fileToUpload"]))
        {
            $temp=explode(".",basename($_FILES["fileToUpload"]["name"]));
            $filename=md5(basename($_FILES["fileToUpload"]["name"]).time());
            $filename=$filename.".".$temp[1];
            $target_file = UPLOAD_QRCODE . "/".$filename;
            if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
            {
                $_POST['qrcode']=$filename;
            }
        }

        $para = $_POST;
        $sql = "insert into {$this->prename}params_atr set";
        foreach ($para as $field => $var) {
            $sql .= " `$field`='$var',";
        }
        $sql = rtrim($sql, ',');        
        $sql .= ' on duplicate key update 
        `bankselect`=values(`bankselect`), `username`=values(`username`), `userbankid`=values(`userbankid`)
        , `remark`=values(`remark`), `enable`=values(`enable`), `memo`=values(`memo`), `lvmin`=values(`lvmin`), `lvmax`=values(`lvmax`)';
        if(!empty($_FILES))
        {
            $sql .=', `qrcode`=values(`qrcode`)';
        }
        if ($this->insert($sql, $para)) {
            $addbankId = $this->lastInsertId();
            $this->addLog(11, $this->adminLogType[11] . '[' . $this->iff($para['id'], '修改', '添加') . 'ID:' . $addbankId . ']', $addbankId, '收款设置');
            $fun = 'success';
            $msg = '操作成功';
        } else {
            $fun = 'error';
            $msg = '未知错误';
        }
    }

    public final function upBanklist()
    {
        $para = $_POST;
        $para['uid'] = $this->user['uid'];
        $sql = "insert into {$this->prename}bank_list set";
        foreach ($para as $field => $var) {
            $sql .= " `$field`='$var',";
        }
        $sql = rtrim($sql, ',');
        $sql .= ' on duplicate key update id=values(id), `name`=values(`name`), `home`=values(`home`), `sort`=values(`sort`), `isDelete`=values(`isDelete`)';
        if ($this->insert($sql, $para)) {
            $addbankId = $this->lastInsertId();
            $this->addLog(11, $this->adminLogType[11] . '[' . $this->iff($para['id'], '修改', '添加') . 'ID:' . $addbankId . ']', $addbankId, $para['name']);
            $fun = 'success';
            $msg = '操作成功';
        } else {
            $fun = 'error';
            $msg = '未知错误';
        }
        echo '<script type="text/javascript">top.onUpdateCompile2("', $fun, '", ', json_encode($msg), ')</script>';
    }

    public final function switchBankStatus($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        $sql = "update {$this->prename}params_atr set enable=not enable where id=$id";
        if ($this->update($sql)) {
            //$chikaren = $this->getValue("select username from {$this->prename}member_bank where uid=?", $uid);
            $this->addLog(11, $this->adminLogType[11] . '[开关操作ID:' . $id . ']', $id, $chikaren);
            echo '操作成功';
        } else {
            throw new Exception('未知错误');
        }
    }
    public final function switchPayinStatus($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        $sql = "update {$this->prename}params_thirdpay set payin_status=not payin_status where id=$id";
        if ($this->update($sql)) {
            // $sql = "update {$this->prename}params_thirdpay set payin_status=0 where id<>$id";
            // $this->update($sql);
            $chikaren = $this->getValue("select name from {$this->prename}params_thirdpay where id=?", $id);
            $this->addLog(91, $this->adminLogType[91] . '[开关操作ID:' . $id . ']', $id, $chikaren);
            echo '操作成功';
        } else {
            throw new Exception('未知错误');
        }
    }
    public final function switchPayoutStatus($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        $sql = "update {$this->prename}params_thirdpay set payout_status=not payout_status where id=$id";
        if ($this->update($sql)) {
            $sql = "update {$this->prename}params_thirdpay set payout_status=0 where id<>$id";
            $this->update($sql);
            $chikaren = $this->getValue("select name from {$this->prename}params_thirdpay where id=?", $id);
            $this->addLog(91, $this->adminLogType[91] . '[开关操作ID:' . $id . ']', $id, $chikaren);
            echo '操作成功';
        } else {
            throw new Exception('未知错误');
        }
    }
    public final function PromotiomModal($id)
    {
        $this->display('system/promotion-modal.php', 0, $id);
    }
    public final function upPromotion()
    {
        if(empty($_POST['endTime']))
        {
            throw new Exception('到期时间不能为空');
        }
        if(isset($_FILES["fileToUpload_MBanner"]))
        {
            $temp=explode(".",basename($_FILES["fileToUpload_MBanner"]["name"]));
            $filename=md5(basename($_FILES["fileToUpload_MBanner"]["name"]).time());
            $filename=$filename.".".$temp[1];
            $target_file = UPLOAD_PROMOTION_M . "/".$filename;
            if(move_uploaded_file($_FILES["fileToUpload_MBanner"]["tmp_name"], $target_file))
            {
                $_POST['mobile_banner']=$filename;
            }
        }
        if(isset($_FILES["fileToUpload_MContent"]))
        {
            $temp=explode(".",basename($_FILES["fileToUpload_MContent"]["name"]));
            $filename=md5(basename($_FILES["fileToUpload_MContent"]["name"]).time());
            $filename=$filename.".".$temp[1];
            $target_file = UPLOAD_PROMOTION_M . "/".$filename;
            if(move_uploaded_file($_FILES["fileToUpload_MContent"]["tmp_name"], $target_file))
            {
                $_POST['mobile_content']=$filename;
            }
        }
        if(isset($_FILES["fileToUpload_PBanner"]))
        {
            $temp=explode(".",basename($_FILES["fileToUpload_PBanner"]["name"]));
            $filename=md5(basename($_FILES["fileToUpload_PBanner"]["name"]).time());
            $filename=$filename.".".$temp[1];

            $target_file = UPLOAD_PROMOTION_P . "/".$filename;
            if(move_uploaded_file($_FILES["fileToUpload_PBanner"]["tmp_name"], $target_file))
            {
                $_POST['pc_banner']=$filename;
            }
        }
        if(isset($_FILES["fileToUpload_PContent"]))
        {
            $temp=explode(".",basename($_FILES["fileToUpload_PContent"]["name"]));
            $filename=md5(basename($_FILES["fileToUpload_PContent"]["name"]).time());
            $filename=$filename.".".$temp[1];

            $target_file = UPLOAD_PROMOTION_P . "/".$filename;
            if(move_uploaded_file($_FILES["fileToUpload_PContent"]["tmp_name"], $target_file))
            {
                $_POST['pc_content']=$filename;
            }
        }

        $para = $_POST;
        $sql = "insert into {$this->prename}promotion set";
        foreach ($para as $field => $var) {
            $sql .= " `$field`='$var',";
        }
        $sql = rtrim($sql, ',');        
        $sql .= ' on duplicate key update 
        `title`=values(`title`), `subtitle`=values(`subtitle`), `enable`=values(`enable`), `endTime`=values(`endTime`), `memo`=values(`memo`)';
        if(!empty($_FILES["fileToUpload_MBanner"]))
        {
            $sql .=', `mobile_banner`=values(`mobile_banner`)';
        }
        if(!empty($_FILES["fileToUpload_MContent"]))
        {
            $sql .=', `mobile_content`=values(`mobile_content`)';
        }
        if(!empty($_FILES["fileToUpload_PBanner"]))
        {
            $sql .=', `pc_banner`=values(`pc_banner`)';
        }
        if(!empty($_FILES["fileToUpload_PContent"]))
        {
            $sql .=', `pc_content`=values(`pc_content`)';
        }
        if ($this->insert($sql, $para)) {
            $addbankId = $this->lastInsertId();
            $this->addLog(32, $this->adminLogType[32] . '[' . $this->iff($para['id'], '修改', '添加') . 'ID:' . $addbankId . ']', $addbankId, '优惠活动设置');
            $fun = 'success';
            $msg = '操作成功';
        } else {
            $fun = 'error';
            $msg = '未知错误';
        }
    }

    public final function switchPromotion($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        $sql = "update {$this->prename}promotion set enable=not enable where id=$id";
        if ($this->update($sql)) {
            //$chikaren = $this->getValue("select username from {$this->prename}member_bank where uid=?", $uid);
            $this->addLog(32, $this->adminLogType[32] . '[开关操作ID:' . $id . ']', $id, $chikaren);
            echo '操作成功';
        } else {
            throw new Exception('未知错误');
        }
    }
/*
    public final function switchBankStatus2($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        $sql = "update {$this->prename}admin_bank set enable=not enable where id=$id";
        if ($this->update($sql)) {
            $this->addLog(11, $this->adminLogType[11] . '[开关操作ID:' . $id . ']', $id, '收款设置');
            echo '操作成功';
        } else {
            throw new Exception('未知错误');
        }
    }

    public final function switchBankStatus3($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        $sql = "update {$this->prename}bank_list set isDelete=not isDelete where id=$id";
        if ($this->update($sql)) {
            $this->addLog(11, $this->adminLogType[11] . '[开关操作ID:' . $id . ']', $id, '银行管理');
            echo '操作成功';
        } else {
            throw new Exception('未知错误');
        }
    }
*/
    public final function delBank($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        $sql = "delete from {$this->prename}member_bank where id=$id";
        if ($this->delete($sql)) {
            $this->addLog(11, $this->adminLogType[11] . '[删除ID:' . $id . ']');
            echo '银行已经删除';
        } else {
            throw new Exception('未知错误');
        }
    }

    public final function type()
    {
        $this->display('system/type-list.php');
    }

    // 更新彩种设置
    public final function upType($id)
    {
        if (! isset($_POST['enable'])) {
            $_POST['enable'] = 0;
        }
        if (! isset($_POST['autoNumber'])) {
            $_POST['autoNumber'] = 0;
        }
        if (! isset($_POST['android'])) {
            $_POST['android'] = 0;
        }

        $type = $this->getRow("SELECT * FROM {$this->prename}type WHERE id =:id", array('id' => $id));
        if (! $type) {
            throw new Exception('查无资料');
        }

        $diff = array();
        $changed = false;
        foreach ($_POST as $k => $v) {
            // 改用 bind 方式，SQL 不用加 slash (ht/safe.php)
            $v = stripslashes($v);
            if ($v == $type[$k]) {
                unset($_POST[$k]);
                continue;
            }

            $changed = true;

            // 记录差异
            $diff[$k]['old'] = $type[$k];
            $diff[$k]['new'] = $v;
        }

        if (! $changed) {
            throw new Exception('数据没有改变');
        }
        if ($this->updateRows($this->prename . 'type', $_POST, 'id =' . $id)) {
            $shortName = $type['shortName'];
            $this->addLog(12, $this->adminLogType[12] . '[' . $shortName . ']', $id, $shortName, json_encode($diff));
            echo '修改彩种成功';
        } else {
            throw new Exception('未知出错');
        }
    }

    public final function played($cai = 1)
    {
        $this->display('system/played-list.php', 0, $cai);
    }

    public final function played_credit($cai = 1)
    {
        $this->display('system/played-list-credit.php', 0, $cai);
    }

    public final function played_official($cai = 1)
    {
        $this->display('system/played-list-official.php', 0, $cai);
    }

    public final function betPlayedInfoUpdate($id)
    {
        $this->display('system/update-play-info.php', 0, $id);
    }

    public final function playedInfoUpdateed()
    {
        $para = $_POST;
        $playedid = $para['playedid'];
        unset($para['playedid']);
        $played = $this->getRow("select * from {$this->prename}played where id={$playedid}");
        if (!$played) throw new Exception('玩法不存在');
        if ($this->updateRows($this->prename . 'played', $para, 'id=' . $playedid)) {
            $name = $this->getValue("select name from {$this->prename}played where id=?", $playedid);
            $this->addLog(13, $this->adminLogType[13] . '[玩法信息修改:' . $name . ']', $id, $name);
            echo '修改成功';
        } else {
            throw new Exception('未知出错');
        }
    }

    // 玩法组开关
    public final function switchPlayedGroupStatus($id)
    {
        $played_group = $this->getRow("SELECT * FROM {$this->prename}played_group WHERE id =:id", array('id' => $id));
        if (! $played_group) {
            throw new Exception('查无资料');
        }

        $sql = "UPDATE {$this->prename}played_group SET enable =NOT enable WHERE id =?";
        if ($this->update($sql, $id)) {
            $groupName = $played_group['groupName'];
            $this->addLog(13, $this->adminLogType[13] . '[玩法组:' . $groupName . ' ' . ($played_group['enable'] ? '关闭' : '开启') . ']', $id, $groupName);
            echo '操作成功';
        } else {
            throw new Exception('未知出错');
        }
    }

    public final function switchPlayedGroupMStatus($id)
    {
        $sql = "update {$this->prename}played_group set android=not android where id=?";
        if ($this->update($sql, $id)) {
            $name = $this->getValue("select name from {$this->prename}played where id=?", $id);
            $this->addLog(13, $this->adminLogType[13] . '[玩法组手机开关:' . $name . ']', $id, $name);
            echo '操作成功';
        } else {
            throw new Exception('未知出错');
        }
    }

    // 玩法开关
    public final function switchPlayedStatus($id)
    {
        $played = $this->getRow("SELECT * FROM {$this->prename}played WHERE id =:id", array('id' => $id));
        if (! $played) {
            throw new Exception('查无资料');
        }

        $sql = "UPDATE {$this->prename}played SET enable =NOT enable WHERE id =?";
        if ($this->update($sql, $id)) {
            $name = $played['name'];
            $this->addLog(13, $this->adminLogType[13] . '[玩法:' . $name . ' ' . ($played['enable'] ? '关闭' : '开启') . ']', $id, $name);
            echo '操作成功';
        } else {
            throw new Exception('未知出错');
        }
    }

    public final function switchPlayedMStatus($id)
    {
        $sql = "update {$this->prename}played set android=not android where id=?";
        if ($this->update($sql, $id)) {
            $playName = $this->getValue("select name from {$this->prename}played where id=?", $id);
            $this->addLog(13, $this->adminLogType[13] . '[玩法手机开关:' . $playName . ']', $id, $playName);
            echo '操作成功';
        } else {
            throw new Exception('未知出错');
        }
    }

    // 更新玩法设置
    public final function upPlayed($id)
    {
        $played = $this->getRow("SELECT * FROM {$this->prename}played WHERE id =:id", array('id' => $id));
        if (! $played) {
            throw new Exception('查无资料');
        }
        $diff = array();
        $changed = false;
        foreach ($_POST as $k => $v) {
            // 改用 bind 方式，SQL 不用加 slash (ht/safe.php)
            $v[0] = stripslashes($v[0]);
            if ($v == $played[$k]) {
                unset($_POST[$k]);
                continue;
            }
            $changed = true;

            // 记录差异
            $diff[$k]['old'] = $played[$k];
            $diff[$k]['new'] = $v;
        }

        if (! $changed) {
            throw new Exception('数据没有改变');
        }
		$data=array();
		$data['bonusProp']=$_POST['bonusProp'][0];
		$data['sort']=$_POST['sort'][0];
		$data['maxCharge']=$_POST['maxCharge'][0];
        if ($this->updateRows($this->prename . 'played', $_POST, 'id=' . $id)) {
            $name = $played['name'];
            $this->addLog(13, $this->adminLogType[13] . '[修改:' . $name . ']', $id, $name, json_encode($diff));
            echo '修改成功';
        } else {
            throw new Exception('未知出错');
        }
    }

	public final function upAll(){
		$data=array();
		$data['dataarr']=$_POST['dataarr'];
		$data['bonusProp']=$_POST['bonusProp'];
		$data['sort']=$_POST['sort'];
		$data['maxCharge']=$_POST['maxCharge'];
		for($i=0;$i<count($data['dataarr']);$i++){
            $sql="update lottery_played set bonusProp='".$data['bonusProp'][$i]."',sort='".$data['sort'][$i]."',maxCharge='".$data['maxCharge'][$i]."' where id=".$data['dataarr'][$i]."";
			$result=$this->update($sql);
			$groupName=$this->getValue("select groupName from {$this->prename}played_group where id=?", $data['dataarr'][$i]);
			$this->addLog(13,$this->adminLogType[13].'[修改:'.$groupName.']',$data['dataarr'][$i],$groupName);
		}
		echo "修改成功";
    }

	public final function pool(){
        $this->display('system/lottery_pool_list.php');
    }
    public final function PoolSettings(){
        if (!$para = $_POST) throw new Exception('参数出错');

		$data=array();
		$data['dataarr']=$_POST['dataarr'];
		$data['bet']=$_POST['bet'];
		$data['zj']=$_POST['zj'];
		for($i=0;$i<count($data['dataarr']);$i++){
            $sql="update lottery_ziki_type set bet=".$data['bet'][$i].",zj=".$data['zj'][$i]." where id=".$data['dataarr'][$i]."";
			$result=$this->update($sql);
			$this->addLog(95,$this->adminLogType[95].'[修改奖池:'.$data['dataarr'][$i].']',$data['dataarr'][$i],$data['dataarr'][$i]);
		}
		return "修改成功";
    }

    public final function service()
    {
        $this->display('system/system-service.php');
    }

    public final function serviceadd()
    {
        $this->display('system/service-add.php');
    }

    public final function clearData()
    {
        $date = strtotime($_POST['date'] . ' 00:00:00');
        $sql = "call clearData({$date})";
        $this->update($sql);
    }

    public final function clearData2()
    {
        $date = strtotime($_POST['date'] . ' 00:00:00');
        $sql = "call clearData2({$date})";
        $this->update($sql);
    }

    public final function clearData3()
    {
        $time = isset($_POST['date']) ? strtotime($_POST['date']) : 0;
        if (! $time) {
            throw new Exception('日期错误');
        }

        $cleared = array();
		$this->beginTransaction();

		if (isset($_POST['bets']) && $_POST['bets']=='1') {
            $sql = "DELETE FROM {$this->prename}bets WHERE kjTime < :time AND lotteryNo !=''";
            $this->execute($sql, array('time' => $time));
            $cleared[] = 'bets';
        }
        if (isset($_POST['coin_log']) && $_POST['coin_log']=='1') {
            $sql = "DELETE FROM {$this->prename}coin_log WHERE actionTime < :time";
            $this->execute($sql, array('time' => $time));
            $cleared[] = 'coin_log';
        }
		$this->commit();

        if ($cleared) {
            $this->addLog(29, implode(', ', $cleared) . ' (' . date('Y-m-d', $time) . ')');
        }
    }

    public final function clearUser()
    {
        $clearMemberCoin = $_POST['coin_del'];
        $clearMemberDate = $this->time - $_POST['date_del'] * 24 * 3600;
        $sql = "call delUsers({$clearMemberCoin}, {$clearMemberDate})";
        $this->update($sql);
    }

    public final function T($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        if ($this->delete("update {$this->prename}member_session set isOnLine=0 where uid={$id}")) {
            $name = $this->getValue("select username from {$this->prename}members where uid=?", $id);
            $this->addLog(21, $this->adminLogType[21] . '[被踢会员:' . $name . ']', $id, $name);
            echo '成功踢掉该会员';
        } else {
            throw new Exception('未知错误');
        }
    }

    public final function delUserCount($id)
    {
        if (!$id = intval($id)) throw new Exception('参数出错');
        $sql = "delete from {$this->prename}params_fandianset  where id=$id";
        if ($this->delete($sql, $id)) {
            echo '已经删除';
        } else {
            throw new Exception('未知出错');
        }
    }
    public final function datalog()
    {
        $this->display('system/datalog.php');
    }
    public final function datalog_mobile()
    {
        $this->display('system/datalog_mobile.php');
    }
    public final function datalogModal($path)
    {
		$this->logPath = $path;
//		print_r($path);
        $this->display('system/datalog-modal.php');
    }
}
