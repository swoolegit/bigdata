<?php

class Time extends AdminBase
{
    public $pageSize = 20;

    public final function index($type)
    {
        $this->type = $type;
        $this->display('time/index.php');
    }

    public final function upTime($type, $id,$actionNo)
    {
        if ($this->updateRows($this->prename . 'data_time', $_POST, 'id=' . $id)) {
            $shortName = $this->getValue("select shortName from {$this->prename}type where id=?", $type);
            $this->addLog(89, $this->adminLogType[89] . '[' . $shortName . '第' . $actionNo . '期]', $id, $shortName . '第' . $actionNo . '期');
            echo '修改时间成功 ';
        } else {
            throw new Exception('未知出错');
        }
    }

    public final function delTime($type, $id,$actionNo)
    {
        if ($this->update("delete from ".$this->prename . 'lhc_time where id=:id', ['id'=>$id])) {
            $shortName = $this->getValue("select shortName from {$this->prename}type where id=?", $type);
            $this->addLog(90, $this->adminLogType[90] . '[' . $shortName . '第' . $actionNo . '期]', $id, $shortName . '第' . $actionNo . '期');
            echo '删除成功 ';
        } else {
            throw new Exception('未知出错');
        }
    }

    public final function add($type)
    {
        $this->type = $type;
        echo $type;
        $this->display('time/add-modal.php');
    }

    public final function added()
    {
        $type=intval($_POST['type']);
        $actionNo=$_POST['actionNo'];
        $time=$_POST['time'];
        $data=[
            'type'=>$type,
            'actionNo'=>$actionNo,
            'actionTime'=>$time,
        ];
        if ($this->insertRow($this->prename . 'lhc_time', $data)) {
            $shortName = $this->getValue("select shortName from {$this->prename}type where id=?", $type);
            $this->addLog(90, $this->adminLogType[90] . '[' . $shortName . '第' . $actionNo . '期]', $id, $shortName . '第' . $actionNo . '期');
            echo '添加成功 ';
        } else {
            throw new Exception('未知出错');
        }
    }

}

?>