<?php

class Manage extends AdminBase
{
    public $pageSize = 15;

    public final function index()
    {
        $this->display('manage/list.php');
    }

    public final function addManagerModal()
    {
        $this->display('manage/manager-add-modal.php');
    }

    public final function addManager()
    {
        $para = array_merge($_POST, array(
            'enable' => 1,
            'regTime' => $this->time,
            'regIP' => $this->ip(true),
        ));
        $para['username'] = strtolower($para['username']);
        if ($this->getValue("select uid from {$this->prename}admin_members where username=?", $para['username']))
            throw new Exception('用户名已经存在');
        $para['password'] = md5($para['password']);
        if ($this->insertRow($this->prename . 'admin_members', $para)) {
            $id = $this->lastInsertId();
            //$sql = "update {$this->prename}admin_members set parents=concat(parents, ',', $id) where `uid`=$id";
            //$this->update($sql);
            $this->addLog(7, $this->adminLogType[7] . '[' . $para['username'] . ']', $this->lastInsertId(), $para['username']);
            return '添加管理员成功';
        } else {
            throw new Exception('未知错误');
        }
    }

    public final function delManager($uid)
    {
        if (!$uid = intval($uid)) throw new Exception('参数出错');
        $sql = "update {$this->prename}admin_members set isDelete=1 where uid=$uid";
        if ($this->delete($sql)) {
            $manageName = $this->getValue("select username from {$this->prename}admin_members where uid=?", $uid);
            $this->addLog(9, $this->adminLogType[9] . '[' . $manageName . ']', $uid, $manageName);
            return '管理员已经删除';
        } else {
            throw new Exception('未知错误');
        }
    }

    public final function pwdManagerModal($uid)
    {
        $this->display('manage/manager-pwd-modal.php', 0, $uid);
    }

    public final function clearManager($uid)
    {
        $userName = $this->getValue("select username from {$this->prename}admin_members where uid=?", $uid);
        $sql = "call delUser($uid)";
        if ($this->update($sql)) {
            $this->addLog(19, $this->adminLogType[19] . '[' . $userName . ']', $uid, $userName);
            return '删除成功';
        } else {
            throw new Exception('删除失败');
        }
    }

    public final function changeManagerPwd($uid)
    {
        if (!$uid = intval($uid)) throw new Exception('参数出错');
		$para = $_POST;
		if(isset($para['password']) && !empty($para['password']))
		{
	        $manageData = $this->getRow("select password, username from {$this->prename}admin_members where uid=$uid");
	        $password = $manageData['password'];
	        if (!$password) throw new Exception('用户不存在');
	        
	        $para['oldpwd'] = md5($para['oldpwd']);
	        if ($para['oldpwd'] != $password) throw new Exception('原密码不正确');
	        unset($para['oldpwd']);
	        $para['password'] = md5($para['password']);
	        if ($para['password'] == $password) throw new Exception('密码没有变化');
	        if ($this->updateRows($this->prename . 'admin_members', $para, 'uid=' . $uid)) {
	            $this->addLog(8, $this->adminLogType[8] . '[' . $manageData['username'] . ']', $uid, $manageData['username']);
	            return '密码已经修改';
	        } else {
	            throw new Exception('未知出错');
	        }
		}
		if(isset($para['memo']) && !empty($para['memo']))
		{
			$memo=array('memo'=>$para['memo']);
			$this->updateRows($this->prename . 'admin_members', $memo, 'uid=' . $uid);
		}
    }

    public final function loginLog()
    {
        include 'Ip.function.php';
        $this->display('manage/login-list.php');
    }

    public final function controlLog()
    {
        $this->display('manage/control-log.php');
    }

    public final function controlLogList()
    {
        $this->display('manage/control-log-list.php');
    }

    // 显示操作记录
    public final function showOperationRecord()
    {
        $this->display('manage/show-operation-record.php');
    }
}
