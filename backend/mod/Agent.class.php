<?php

class Agent extends AdminBase
{
    public $pageSize = 15;

    public final function betDates()
    {
        $this->display('Agent/dates.php');
    }
    public final function betDateSearch()
    {
        $this->display('Agent/date-list.php');
    }
    public final function betDateSearchs()
    {
        $this->display('Agent/date-lists.php');
    }
    public final function shareBonusModal()
    {
        $this->display('Agent/share-bonus-modal.php');
    }
    public final function shareBonusSingle($uid)
    {
        $para = $_POST;
        $para['uid'] = $uid;
        if (!$uid = intval($uid)) throw new Exception('参数出错');
        if (!isset($para['startTime']) || empty($para['startTime'])) throw new Exception('参数出错');
        if (!isset($para['endTime']) || empty($para['endTime'])) throw new Exception('参数出错');
		//if ($para['startTime']>$para['endTime'])throw new Exception('参数出错');
		$para['lossAmount']=0;
		$para['bonusAmount']=0;
		$para['startTime'] = strtotime($para['startTime'] . ' 00:00:00');
		$para['endTime'] = strtotime($para['endTime'] . ' 23:59:59');
		$betTimeWhere = "and b.actionTime between ".$para['startTime']." and ".$para['endTime'];
		/*
		 *begin
		 * 检查是否发送过
		 * 起始发送日必须大于上次结算日 
		 */
        //$sql = "select id from {$this->prename}bonus_log where uid=" . $uid . ' and  endTime >= '.$para['startTime'];
        //$bonusId = $this->getValue($sql);
        //if ($bonusId) throw new Exception('该用户本期分红已经发放');
		/*
		 *end
		 * 检查是否发送过
		 * 起始发送日必须大于上次结算日 
		 */
		$sql="select u.uid,u.username,u.fenHong,u.business_agent,u.parents,u.parentId,
			sum(b.zj+b.broker+b.fandian+b.gongzi - b.real_bet) as betZjAmount,l.endTime
			from {$this->prename}members u LEFT OUTER JOIN lottery_bonus_log l on l.uid=u.uid ,{$this->prename}member_report b where u.uid=b.uid 
			and (u.uid=$uid or concat(',', parents, ',') like '%,$uid,%') and (l.endTime < ".$para['startTime']." or l.endTime is null)  $betTimeWhere group by u.uid";
        $_datas= $this->getRows($sql);
		$_userdata=array();
		$_countUser=0;
		$_countUserSended=0;
		foreach ($_datas as $_data) {
			$_userdata[$_data['uid']]['uid']=$_data['uid'];
			$_userdata[$_data['uid']]['username']=$_data['username'];
			$_userdata[$_data['uid']]['fenHong']=$_data['fenHong'];
			$_userdata[$_data['uid']]['business_agent']=$_data['business_agent'];
			$_userdata[$_data['uid']]['parents']=$_data['parents'];
			$_userdata[$_data['uid']]['parentId']=$_data['parentId'];
			$_userdata[$_data['uid']]['lossAmount']=$_data['betZjAmount'];
			$_userdata[$_data['uid']]['lossAmount_self']=$_data['betZjAmount'];
			if(!empty($_data['parents']))
			{
				$_parents=explode(",",$_data['parents']);
				foreach($_parents as $_parent)
				{
					if($_parent==$_data['uid']) continue;
					$_userdata[$_parent]['lossAmount']+=$_data['betZjAmount'];
				}
			}
		}
		foreach($_userdata as $_user)
		{
			if(count(explode(",",$_user['parents']))>2) continue;
			$_countUser++;
			$para['username']=$_user['username'];
			if($_user['business_agent']==0 && $_user['lossAmount']>=0)continue;
			if(!isset($_user['uid']))continue;
			switch(TRUE)
			{
				case ($_user['business_agent']==0 && $_user['lossAmount']<0):
					$para['lossAmount'] = $_user['lossAmount'];
					$para['bonusAmount'] = $_user['lossAmount']*-1*($_user['fenHong'])/100;
					break;
				case ($_user['business_agent']==1):
					if($_user['lossAmount_self']<0)
					{
						$para['lossAmount'] = $_user['lossAmount_self'];
						$para['bonusAmount'] = $_user['lossAmount_self']*-1*($_user['fenHong'])/100;						
					}
					foreach($_userdata as $_user2)
					{
						if($_user2['parentId']==$_user['uid'])
						{
							$para['lossAmount']+=$_user2['lossAmount'];
							$para['bonusAmount'] += $_user2['lossAmount']*-1*($_user['fenHong']-$_user2['fenHong'])/100;					
						}
					}
					break;					
			}
			$para['uid'] = $_user['uid'];
	        $para['bonusTime'] = time();
			$para['bonusStatus'] = 1;
	        $this->beginTransaction();
	        try {
	            $data['uid'] = $user['uid'];
	            $data['coin'] = $user['coin'];
	            $data['username'] = $user['username'];
	            $data['info'] = '结算分红';
				$this->addCoin(array(
                    'uid' => $para['uid'],
                    'liqType' => 3,
                    'coin' => $para['bonusAmount'],
                    'extfield2' => $this->user['username'],
                    'info' => $data['info']
                ));
				$this->insertRow("{$this->prename}bonus_log", $para);
	            $this->addLog(30, $this->adminLogType[30] . '[' . $para['username'] . ']['.$para['bonusAmount'].']', $this->lastInsertId(), $para['username']);
	            $this->commit();
	            //return '充值成功';
	            $_countUserSended++;
	            //echo '分红发放成功!!。 应发送数量:'.$_countUser.'人,发送成功数:'.$_countUserSended.'人,发送失败数:'.($_countUser-$_countUserSended).'人';
	        } catch (Exception $e) {
	            $this->rollBack();
	            throw $e;
	        }
			/*
	        if ($this->insertRow("{$this->prename}bonus_log", $para)) {
	            $this->addLog(30, $this->adminLogType[30] . '[' . $para['username'] . ']', $this->lastInsertId(), $para['username']);
				$_countUserSended++;
	        } else {
	            throw new Exception('未知错误');
	        }
			*/
		}
		echo '分红发放成功!!。 应发送数量:'.$_countUser.'人,发送成功数:'.$_countUserSended.'人,发送失败数:'.($_countUser-$_countUserSended).'人';		 
		/*
		 * 招商号分红发送结束
		 */
    }
}

?>