<?php

class Commission extends AdminBase
{
    public $pageSize = 20;

    public final function conCommissionList()
    {
        $this->display('commission/con-list.php');
    }
    public final function conCommissionLists()
    {
        $this->display('commission/con-lists.php');
    }

    public final function lossCommissionList()
    {
        $this->display('commission/loss-list.php');
    }

    public final function lossCommissionLists()
    {
        $this->display('commission/loss-lists.php');
    }

    // 更新 消费佣金 or 亏损佣金 发放状态 (系统设置)
    public final function upCommStatus()
    {
        if (!isset($_GET['commStatusName'])) {
            throw new Exception('参数出错');
        }

        $commStatusName = $_GET['commStatusName'];
        $updateName = $commStatusName . '_update';

        $updateNames = array('conCommStatus_update', 'lossCommStatus_update');
        if (!in_array($updateName, $updateNames)) {
            throw new Exception('非法请求');
        }

        $updateValue = $this->settings[$updateName];
        if ($updateValue + 86400 > time()) {
            throw new Exception('您今天已经更新过发放状态，请明天再次更新');
        }

        $sql = "UPDATE {$this->prename}members SET {$commStatusName} =0";
        $this->update($sql);

        $today = strtotime('today');
        $sql = "UPDATE {$this->prename}params SET value ='{$today}' WHERE name ='{$updateName}' LIMIT 1";
        $this->update($sql);

        $systemCacheFilename = $this->cacheDir . 'systemSettings';
        if (is_file($systemCacheFilename)) {
            unlink($systemCacheFilename);
        }

        echo '更新发放状态成功！！！';
    }

    // 发放消费佣金
    public final function conComSingle($uid)
    {
        if (!$uid = intval($uid)) {
            throw new Exception('参数出错');
        }

        $this->getSystemSettings();

        $yesterday = date('Y-m-d', strtotime('-1 day'));
        $fromTime = strtotime($yesterday . ' 00:00:00');
        $toTime = strtotime($yesterday . ' 23:59:59');
        $sql =
            "SELECT
                u.uid,
                u.username,
                u.parentId,
                SUM(b.real_bet) AS betAmount
            FROM {$this->prename}members AS u
                LEFT JOIN {$this->prename}member_report AS b ON
                    b.uid =u.uid
                    AND b.actionTime BETWEEN {$fromTime} AND {$toTime}
            WHERE
                u.uid ={$uid}
            GROUP BY u.uid";
        $res = $this->getRows($sql);
        $userbets = $res[0];

        // 消费金额
        $betAmount = $userbets['betAmount'];

        $this->beginTransaction();
        try {
            $conCommissionBase = array(
                'value' => floatval($this->settings['conCommissionBase']),
                'self' => floatval($this->settings['conCommissionSelfAmount']),
                'parent' => floatval($this->settings['conCommissionParentAmount']),
                'top' => floatval($this->settings['conCommissionParentAmount2']),
            );
            $conCommissionBase2 = array(
                'value' => floatval($this->settings['conCommissionBase2']),
                'self' => floatval($this->settings['conCommissionSelfAmount2']),
                'parent' => floatval($this->settings['conCommissionParentAmount3']),
                'top' => floatval($this->settings['conCommissionParentAmount4']),
            );
            if ($conCommissionBase['value'] > $conCommissionBase2['value']) {
                $conBig = $conCommissionBase;
                $conSmall = $conCommissionBase2;
            } else {
                $conBig = $conCommissionBase2;
                $conSmall = $conCommissionBase;
            }

            $rebateValue = 0;
            $rebateSelf = 0;
            $rebateParent = 0;
            $rebateTop = 0;
            if ($betAmount > $conSmall['value'] && $betAmount <= $conBig['value']) {
                $rebateValue = $conSmall['value'];
                $rebateSelf = $conSmall['self'];
                $rebateParent = $conSmall['parent'];
                $rebateTop = $conSmall['top'];
            } else if ($betAmount > $conBig['value']) {
                $rebateValue = $conBig['value'];
                $rebateSelf = $conBig['self'];
                $rebateParent = $conBig['parent'];
                $rebateTop = $conBig['top'];
            }

            if ($rebateValue && ($rebateSelf || $rebateParent || $rebateTop)) {
                $log = array(
                    'liqType' => 53,
                    'extfield0' => $uid,
                );

                // 自身消费佣金
                if ($rebateSelf) {
                    $log['coin'] = $rebateSelf;
                    $log['uid'] = $userbets['uid'];
                    $log['info'] = '自身[' . $userbets['username'] . ']消费佣金';
                    $log['extfield1'] = $userbets['username'];
                    $this->addCoin($log);
                    $this->addLog(20, $this->adminLogType[20] . '[' . $userbets['username'] . ']', $uid, $userbets['username']);
                }

                if ($parentId = $userbets['parentId']) {
                    // 上家消费佣金
                    if ($rebateParent) {
                        $log['coin'] = $rebateParent;
                        $log['uid'] = $parentId;
                        $log['info'] = '下级[' . $userbets['username'] . ']消费佣金';
                        $log['extfield1'] = $userbets['username'];
                        $this->addCoin($log);
                        $sql = "SELECT username FROM {$this->prename}members WHERE uid =?";
                        $parentName = $this->getValue($sql, $parentId);
                        $this->addLog(20, $this->adminLogType[20] . '[' . $parentName . '<=' . $userbets['username'] . ']', $uid, $userbets['username']);
                    }

                    $sql = "SELECT parentId, username FROM {$this->prename}members WHERE uid =?";
                    $res = $this->getRows($sql, $parentId);
                    $parent = $res[0];
                    if ($parentId = $parent['parentId']) {
                        // 上上家消费佣金
                        if ($rebateTop) {
                            $log['coin'] = $rebateTop;
                            $log['uid'] = $parentId;
                            $log['info'] = '下级[' . $parent['username'] . '<=' . $userbets['username'] . ']消费佣金';
                            $log['extfield1'] = $parent['username'] . '<=' . $userbets['username'];
                            $this->addCoin($log);
                            $sql = "select username from {$this->prename}members where `uid`=?";
                            $parentName = $this->getValue($sql, $parentId);
                            $this->addLog(20, $this->adminLogType[20] . '[' . $parentName . '<=' . $parent['username'] . '<=' . $userbets['username'] . ']', $uid, $userbets['username']);
                        }
                    }
                }
            }

            // 更新会员消费佣金发放状态
            $sql = "UPDATE {$this->prename}members SET conCommStatus =1 WHERE uid ={$uid}";
            if ($this->update($sql)) {
                $this->commit();
                echo '消费佣金发放成功';
            }
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    // 发放亏损佣金
    public final function lossComSingle($uid)
    {
        if (!$uid = intval($uid)) {
            throw new Exception('参数出错');
        }

        $this->getSystemSettings();

        $yesterday = date('Y-m-d', strtotime('-1 day'));
        $fromTime = strtotime($yesterday . ' 00:00:00');
        $toTime = strtotime($yesterday . ' 23:59:59');

        $sql =
            "SELECT
                u.uid,
                u.username,
                u.parentId,
		        SUM(b.real_bet) AS betAmount,
		        SUM(b.zj) AS zjAmount,
		        SUM(b.fandian) AS fanDianAmount,
		        SUM(b.broker) AS brokerageAmount
            FROM {$this->prename}members AS u
                LEFT JOIN {$this->prename}member_report AS b ON
                    b.uid =u.uid
                    AND b.actionTime BETWEEN {$fromTime} AND {$toTime}
            WHERE
                u.uid ={$uid}
            GROUP BY u.uid";
        $res = $this->getRows($sql);
        $userloss = $res[0];

        // 亏损金额
        $lossAmount = $userloss['zjAmount'] - $userloss['betAmount'] + $userloss['fanDianAmount'] + $userloss['brokerageAmount'];

        $this->beginTransaction();
        try {
            $lossCommissionBase = array(
                'value' => floatval($this->settings['lossCommissionBase']),
                'self' => floatval($this->settings['lossCommissionSelfAmount']),
                'parent' => floatval($this->settings['lossCommissionParentAmount']),
                'top' => floatval($this->settings['lossCommissionParentAmount2']),
            );
            $lossCommissionBase2 = array(
                'value' => floatval($this->settings['lossCommissionBase2']),
                'self' => floatval($this->settings['lossCommissionSelfAmount2']),
                'parent' => floatval($this->settings['lossCommissionParentAmount3']),
                'top' => floatval($this->settings['lossCommissionParentAmount4']),
            );
            if ($lossCommissionBase['value'] > $lossCommissionBase2['value']) {
                $lossBig = $lossCommissionBase;
                $lossSmall = $lossCommissionBase2;
            } else {
                $lossBig = $lossCommissionBase2;
                $lossSmall = $lossCommissionBase;
            }

            $rebateValue = 0;
            $rebateSelf = 0;
            $rebateParent = 0;
            $rebateTop = 0;
            if (abs($lossAmount) > $lossSmall['value'] && abs($lossAmount) <= $lossBig['value']) {
                $rebateValue = $lossSmall['value'];
                $rebateSelf = $lossSmall['self'];
                $rebateParent = $lossSmall['parent'];
                $rebateTop = $lossSmall['top'];
            } else if (abs($lossAmount) > $lossBig['value']) {
                $rebateValue = $lossBig['value'];
                $rebateSelf = $lossBig['self'];
                $rebateParent = $lossBig['parent'];
                $rebateTop = $lossBig['top'];
            }

            if ($rebateValue && ($rebateSelf || $rebateParent || $rebateTop)) {
                $log = array(
                    'liqType' => 56,
                    'extfield0' => $uid
                );

                // 自身亏损佣金
                if ($rebateSelf) {
                    $log['coin'] = $rebateSelf;
                    $log['uid'] = $userloss['uid'];
                    $log['info'] = '自身[' . $userloss['username'] . ']亏损佣金';
                    $log['extfield1'] = $userloss['username'];
                    $this->addCoin($log);
                    $this->addLog(20, $this->adminLogType[20] . '[' . $userloss['username'] . ']', $uid, $userloss['username']);
                }

                if ($parentId = $userloss['parentId']) {
                    // 上家亏损佣金
                    if ($rebateParent) {
                        $log['coin'] = $rebateParent;
                        $log['uid'] = $parentId;
                        $log['info'] = '下级[' . $userloss['username'] . ']亏损佣金';
                        $log['extfield1'] = $userloss['username'];
                        $this->addCoin($log);
                        $sql = "SELECT username FROM {$this->prename}members WHERE uid =?";
                        $parentName = $this->getValue($sql, $parentId);
                        $this->addLog(20, $this->adminLogType[20] . '[' . $parentName . '<=' . $userloss['username'] . ']', $uid, $userloss['username']);
                    }

                    $sql = "SELECT parentId, username FROM {$this->prename}members WHERE uid =?";
                    $res = $this->getRows($sql, $parentId);
                    $parent = $res[0];
                    if ($parentId = $parent['parentId']) {
                        // 上上家亏损佣金
                        if ($rebateTop) {
                            $log['coin'] = $rebateTop;
                            $log['uid'] = $parentId;
                            $log['info'] = '下级[' . $parent['username'] . '<=' . $userloss['username'] . ']亏损佣金';
                            $log['extfield1'] = $parent['username'] . '<=' . $userloss['username'];
                            $this->addCoin($log);
                            $sql = "SELECT username FROM {$this->prename}members WHERE uid =?";
                            $parentName = $this->getValue($sql, $parentId);
                            $this->addLog(20, $this->adminLogType[20] . '[' . $parentName . '<=' . $parent['username'] . '<=' . $userloss['username'] . ']', $uid, $userloss['username']);
                        }
                    }
                }
            }

            // 更新会员亏损佣金发放状态
            $sql = "UPDATE {$this->prename}members SET lossCommStatus =1 WHERE uid ={$uid}";
            if ($this->update($sql)) {
                $this->commit();
                echo '亏损佣金发放成功';
            }
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }
}
