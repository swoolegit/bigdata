<?php

/*
 * 报表相关
*/
class Report extends AdminBase
{
    public $pageSize = 30;

    function __construct($dsn, $user = '', $password = '')
    {
        parent::__construct($dsn, $user, $password);
    }

    // 每小时报表
    public final function index()
    {
        $this->display('report/index.php');
    }

    // 彩种 盈收
    public final function incomeOfUser()
    {
        $this->display('report/income-of-user.php');
    }
    // 彩种 盈收列表
    public final function incomeOfUserList()
    {
        $this->display('report/income-of-user-list.php');
    }


    // 彩种 盈收
    public final function incomeOfType()
    {
        $this->display('report/income-of-type.php');
    }

    // 彩种 盈收(图) by robert
    public final function incomeOfecharts()
    {
        $this->display('report/income-of-type-echarts.php');
    }
	
    // 彩种 盈收列表
    public final function incomeOfTypeList()
    {
        $this->display('report/income-of-type-list.php');
    }

    // 彩种 盈收列表(图) by robert
    public final function incomeOfTypeListEcharts()
    {
        $this->display('report/income-of-type-list-echarts.php');
    }
    // 用户报表 (图) by robert
    public final function usersOfecharts()
    {
        $this->display('report/users-of-echarts.php');
    }
    // 用户报表列表 (图) by robert
    public final function usersOfListecharts()
    {
        $this->display('report/users-of-list-echarts.php');
    }
    // 盈亏报表 (图) by robert
    public final function profitOfecharts()
    {
        $this->display('report/profit-of-echarts.php');
    }
    // 盈亏报表列表 (图) by robert
    public final function profitOfListecharts()
    {
        $this->display('report/profit-of-list-echarts.php');
    }
    // PC及手机分析表 (图) by robert
    public final function clientOfecharts()
    {
        $this->display('report/client-of-echarts.php');
    }
    // PC及手机分析表 (图) by robert
    public final function clientOfListecharts()
    {
        $this->display('report/client-of-list-echarts.php');
    }
    // 玩法组 盈收
    public final function incomeOfPlayedGroup($type_id = 0)
    {
        $this->tpl_vars = array(
            'type_id' => $type_id
        );
        $this->display('report/income-of-played-group.php');
    }

    // 玩法组 盈收列表
    public final function incomeOfPlayedGroupList($type_id = 0)
    {
        $this->tpl_vars = array(
            'type_id' => $type_id
        );
        $this->display('report/income-of-played-group-list.php');
    }

    // 玩法 盈收
    public final function incomeOfPlayed($played_group_id = 0)
    {
        $this->tpl_vars = array(
            'played_group_id' => $played_group_id
        );
        $this->display('report/income-of-played.php');
    }

    // 玩法 盈收列表
    public final function incomeOfPlayedList($played_group_id = 0)
    {
        $this->tpl_vars = array(
            'played_group_id' => $played_group_id
        );
        $this->display('report/income-of-played-list.php');
    }

    // 注册 & 充值
    public final function registrationRecharge()
    {
        $this->display('report/registration-recharge.php');
    }

    // 注册 & 充值列表
    public final function registrationRechargeList()
    {
        $this->display('report/registration-recharge-list.php');
    }

    public final function gameClass()
    {
        $this->display('report/gameClass.php');
    }

    public final function gameClassSearch()
    {
        $this->display('report/gameClass-list.php');
    }

    public final function betsRepl()
    {
        $this->display('report/betsRepl.php');
    }

    // 出入帐
    public final function coin()
    {
        $this->display('report/coin.php');
    }

    // 出入帐
    public final function coinList()
    {
        $this->display('report/coin-list.php');
    }

    // 出入帐
    public final function coinDetail()
    {
        $this->display('report/coin-detail.php');
    }

}
