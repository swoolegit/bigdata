<?php

class Data extends AdminBase
{
    public $pageSize = 15;
    private $encrypt_key = 'lottery_running';
    private $dataPort = 65531;
	private $analysis =null;
    public final function index($type)
    {
        $this->type = $type;
        $this->display('data/index.php');
    }

	public final function batch_number($type){
		$this->type=$type;
		$this->display('data/batch-modal.php');
	}

    public final function add($type, $actionNo, $actionTime)
    {
        $para = array(
            'type' => $type,
            'actionNo' => $actionNo,
            'actionTime' => $actionTime
        );
        $this->display('data/add-modal.php', 0, $para);
    }

	public final function batch_added(){
		$code=trim($_POST['code']);
		$issues=explode("|",$code);
		foreach($issues as $k=>$v)
		{
			$data2=explode(":",$v);
			$number=$data2[0];
			$data=$data2[1];
			if(!$this->getValue("select data from {$this->prename}data where type={$_POST['type']} and number='{$number}'")){
				$para['type']=intval($_POST['type']);
				$para['number']=$number;
				$para['data']=$data;
				$issueDate = substr($para['number'],0,4)."-".substr($para['number'],4,2)."-".substr($para['number'],6,2)." 00:00:00";
				$issueTime = (int)substr($para['number'],8,4)*60-60;
				$para['time']=strtotime($issueDate)+$issueTime;
				//$para['time']=$this->time;
				try{
					$this->beginTransaction();
					$this->insertRow($this->prename .'data', $para);
					$this->addLog(93,$this->adminLogType[93].'['.$para['data'].']', 0, $this->getValue("select shortName from {$this->prename}type where id=?",$para['type']).'[期號:'.$para['number'].']');
					$this->commit();
					//return "新增成功!";
				}catch(Exception $e){
					$this->rollBack();
					throw $e;
				}
			}
		}
	}

	public final function auto_number(){
		//$para=$_GET;
		$sql="select type from {$this->prename}type where id={$_GET['type']}";
		$type=$this->getValue($sql);
		$zikiArray=[];
		$data="";
		switch(true)
		{
			case $type==6:
				$tnumber=array('1','2','3','4','5','6','7','8','9','10');
				$dnumber=array();
				for($i=0;$i<10;$i++)
				{
					$key=array_rand($tnumber,1);
					$dnumber[$i]=$tnumber[$key];
					array_splice($tnumber, $key, 1);
				}
				$data=implode(",", $dnumber);
				break;
			case $type==1:
				$zikiArray=array(
					0=>array(0,1,2,3,4,5,6,7,8,9),
					1=>array(0,1,2,3,4,5,6,7,8,9),
					2=>array(0,1,2,3,4,5,6,7,8,9),
					3=>array(0,1,2,3,4,5,6,7,8,9),
					4=>array(0,1,2,3,4,5,6,7,8,9)
				);
				$num0=$zikiArray[0][array_rand($zikiArray[0],1)];
				$num1=$zikiArray[1][array_rand($zikiArray[1],1)];
				$num2=$zikiArray[2][array_rand($zikiArray[2],1)];
				$num3=$zikiArray[3][array_rand($zikiArray[3],1)];
				$num4=$zikiArray[4][array_rand($zikiArray[4],1)];
				$data =$num0.",".$num1.",".$num2.",".$num3.",".$num4;
				break;
			case $type==12:
				$tnumber=array('1','2','3','4','5','6','7','8','9'
								,'10','11','12','13','14','15','16','17','18','19'
								,'20','21','22','23','24','25','26','27','28','29'
								,'30','31','32','33','34','35','36','37','38','39'
								,'40','41','42','43','44','45','46','47','48','49'
							);
				$dnumber=array();
				for($i=0;$i<7;$i++)
				{
					$key=array_rand($tnumber,1);
					$dnumber[$i]=$tnumber[$key];
					array_splice($tnumber, $key, 1);
				}
				$data=implode(",", $dnumber);
				break;
			case $type==9:
				$zikiArray=array(
					0=>array(1,2,3,4,5,6),
					1=>array(1,2,3,4,5,6),
					2=>array(1,2,3,4,5,6),
				);
				$num0=$zikiArray[0][array_rand($zikiArray[0],1)];
				$num1=$zikiArray[1][array_rand($zikiArray[1],1)];
				$num2=$zikiArray[2][array_rand($zikiArray[2],1)];
				$data =$num0.",".$num1.",".$num2;
				break;
		}
		if(!$this->getValue("select data from {$this->prename}data where type={$_GET['type']} and number='{$_GET['actionNo']}'")){
			$para['type']=intval($_GET['type']);
			$para['number']=$_GET['actionNo'];
			$para['data']=$data;
			$sql="select actionTime from {$this->prename}data_time where type={$_GET['type']} and actionNo = 1 limit 1";
			$time1 = $this->getValue($sql);
			$sql="select actionTime from {$this->prename}data_time where type={$_GET['type']} and actionNo = 2 limit 1";
			$time2 = $this->getValue($sql);
			$diff_time=(strtotime($time2) - strtotime($time1));
			$issueDate = substr($para['number'],0,4)."-".substr($para['number'],4,2)."-".substr($para['number'],6,2)." 00:00:00";
			$issueTime = ((int)substr($para['number'],8,4)-1)*$diff_time;
			$para['time']=strtotime($issueDate)+$issueTime;
			try{
				$this->beginTransaction();
				$this->insertRow($this->prename .'data', $para);
				$this->addLog(17,$this->adminLogType[17].'['.$para['data'].']', 0, $this->getValue("select shortName from {$this->prename}type where id=?",$para['type']).'[期號:'.$para['number'].']');
				$this->commit();
				return "新增成功!";
			}catch(Exception $e){
				$this->rollBack();
				throw $e;
			}
		}
	}

    public final function back($type, $actionNo, $actionTime)
    {
        $para = array(
            'type' => $type,
            'actionNo' => $actionNo,
            'actionTime' => $actionTime
        );
        $this->display('data/back-modal.php', 0, $para);
    }

    public final function backed()
    {
        $para = $_POST;
        $type = intval($para['type']);
        $number = $para['number'];
        $sql = "select * from {$this->prename}bets where type={$type} and actionNo='{$number}' AND lotteryNo = '' ";
        if ($data = $this->getRows($sql)) {
			$this->beginTransaction();
			try{
				foreach ($data as $var) {
					// if($var['isGuan']=0)
					// {
					// 	$amount = abs($var['actionAmount']);
					// }else{
					// 	$amount = abs($var['actionNum']) * $var['mode'] * intval($var['beiShu']);
					// }
					$amount = abs($var['actionAmount']);
		            $this->addCoin(array(
		                'uid' => $var['uid'],
		                'type' => $var['type'],
		                'liqType' => 7,
		                'info' => '撤单',
		                'extfield0' => $var['id'],
		                'coin' => $amount
		            ));
		            $this->query("UPDATE `{$this->prename}bets` SET `isDelete`=1 WHERE `id`=".$var['id']." LIMIT 1");
				}
				$this->commit();
			} catch (Exception $e) {
				$this->rollBack();
				throw new Exception($e);
			}
        }
    }
    public final function udata($type, $actionNo, $actionTime,$actionData)
    {
        $para = array(
            'type' => $type,
            'actionNo' => $actionNo,
			'actionTime' => $actionTime,
			'actionData' => $actionData
        );
        $this->display('data/update-modal.php', 0, $para);
    }

    public final function kj()
    {
        //$para = $_GET;
        //$para['key'] = $this->encrypt_key;
        //$url = $GLOBALS['conf']['node']['access'] . '/data/kj';
        //echo $this->http_post($url, $para);

		//$data_url = http_build_query($para);
		$command = '/usr/bin/php '.dirname(__DIR__).'/cron/checkOrder.php ';
		//echo $command;
		exec($command);
	}

    public final function added()
    {
		if($_POST['opencheck']!=OPENPASS )
		{
			throw new Exception('验证码错误');
		}

        $para = $_POST;
        $para['type'] = intval($para['type']);
        //$para['key'] = $this->encrypt_key;
        //$url = $GLOBALS['conf']['node']['access'] . '/data/add';
        if (!$this->getValue("select data from {$this->prename}data where type={$para['type']} and number='{$para['number']}'"))
		{
			$this->addLog(17, $this->adminLogType[17] . '[' . $para['data'] . ']', 0, $this->getValue("select shortName from {$this->prename}type where id=?", $para['type']) . '[期号:' . $para['number'] . ']');
		}         
        //echo $this->http_post($url, $para);
		$type = $para['type'];
		$number = $para['number'];
		$time = time();
		$data = $para['data'];
		//if(!$data || !$number) continue;
		$sql = "
			INSERT INTO lottery_data
			 (type, time, number, data) VALUES (?,?,?,?)
		";
		$dtime = date('m-d H:i:s');
		try{
			$result = $this->insert($sql,array($type, $time, $number, $data));
			$this->trend($type,$number,$data);
		} catch (Exception $e) {
			echo $e;
		}

		// begin 幸运28 by robert
		if($type==24 || $type==1)
		{
			switch(true)
			{
				case $type==63;
					$type=67;
					$data = array();
					$data[] = substr(strval($d[0] + $d[1] + $d[2] + $d[3]+ $d[4]+ $d[5]),-1,1);
					$data[] = substr(strval($d[6] + $d[7] + $d[8] + $d[9]+ $d[10]+ $d[11]),-1,1);
					$data[] = substr(strval($d[12] + $d[13] + $d[14] + $d[15]+ $d[16]+ $d[17]),-1,1);
					$data[] = strval($data[0] + $data[1] + $data[2]);
					$data = implode(',',$data);										
					echo "$type, $time, $number, $data";
				break;
				case $type==64;
					$type=68;
					$data = array();
					$data[] = substr(strval($d[0] + $d[1] + $d[2] + $d[3]+ $d[4]+ $d[5]),-1,1);
					$data[] = substr(strval($d[6] + $d[7] + $d[8] + $d[9]+ $d[10]+ $d[11]),-1,1);
					$data[] = substr(strval($d[12] + $d[13] + $d[14] + $d[15]+ $d[16]+ $d[17]),-1,1);
					$data[] = strval($data[0] + $data[1] + $data[2]);
					$data = implode(',',$data);
					echo "$type, $time, $number, $data";
				break;
				case $type==24;
					$type=65;
					$d = explode(',',$data);
					$data = array();
					$data[] = substr(strval($d[0] + $d[1] + $d[2] + $d[3]+ $d[4]+ $d[5]),-1,1);
					$data[] = substr(strval($d[6] + $d[7] + $d[8] + $d[9]+ $d[10]+ $d[11]),-1,1);
					$data[] = substr(strval($d[12] + $d[13] + $d[14] + $d[15]+ $d[16]+ $d[17]),-1,1);
					$data[] = strval($data[0] + $data[1] + $data[2]);
					$data = implode(',',$data);
					echo "$type, $time, $number, $data";
				break;
				case $type==1;
					$type=66;
					$d = explode(',',$data);
					$data = array();
					$data[] = $d[0];
					$data[] = $d[1];
					$data[] = $d[2];
					$data[] = strval($data[0] + $data[1] + $data[2]);
					$data = implode(',',$data);
					echo "$type, $time, $number, $data";
				break;
			}
			try{
				$result = $this->insert($sql,array($type, $time, $number, $data));
			} catch (Exception $e) {
				echo $e;
			}
		}
		// end 幸运28 by robert
		//$data_url = http_build_query($para);
		//$command = '/usr/bin/php '.dirname(__DIR__).'/cron/handKaiJiang.php '.urlencode($data_url);
		//echo $command;
		//exec($command);
	}

    public final function udataed()
    {
        // $id = intval($_POST['id']);
        // $para['data'] = $_POST['data'];
        // $sql = "update {$this->prename}data set data='{$para['data']}' where id={$id}";
        // if ($this->update($sql)) {
        //     echo '修改成功';
		// }
		if($_POST['opencheck']!=OPENPASS )
		{
			throw new Exception('验证码错误');
		}

		$para=$_POST;
		$id=intval($_POST['id']);
		$para['data']=$_POST['data'];
		$para['type']=$id;
		$item_name=$this->getValue("select title from {$this->prename}type where id=?", $para['type']);
		$this->trend($para['type'],$para['actionNo'],$para['data']);
		$this->beginTransaction();
		try
		{
			$sql="call reKanjiang({$para['type']},'{$para['actionNo']}')";
			$this->insert($sql);
			$this->addLog(31,$this->adminLogType[31].'['.$item_name.']', 0, $item_name.'[期号:'.$para['actionNo'].']');			
			$sql="update {$this->prename}data set data='{$para['data']}' where type={$para['type']} and number='{$para['actionNo']}'";
			$this->update($sql);
			$this->addLog(32,$this->adminLogType[32].'['.$item_name.']', 0, $item_name.'[期号:'.$para['actionNo'].']');
			$this->commit();
			echo "修改成功!";
		}catch(Exception $e){
			$this->rollBack();
			throw $e;
		}
		$command = '/usr/bin/php '.dirname(__DIR__).'/api/checkOrder.php'; // 本機路徑
		//$command = '/usr/bin/php /root/crown_kj/checkOrder.php'; // linux路徑
		//echo $command;
		$res = shell_exec($command);
		//$res = exec($command);
		//print_r($res);
		//exec($command);
    }

    public function http_post($url, $data)
    {

		/*
		$data_len = strlen($data_url);
        return file_get_contents($url, false, stream_context_create(array('http' => array('method' => 'POST'
        , 'header' => "Connection: close\r\nContent-Length: $data_len\r\n"
        , 'content' => $data_url
        ))));*/
    }
	
	//begin 走势图 by robert
	public function trend($type,$actionNo,$kjData) {
		include_once 'lib/analysis.php';
		$this->analysis = new analysis();
		$sql="select data from {$this->prename}data where type={$type} order by id desc  limit 49";
		$get_data =$this->getRows($sql);
		$sql="select type from {$this->prename}type where id={$type}";
		$get_type = $this->getRow($sql);
		$an_data=null;
		$clong=null;
		switch(true)
		{
			case $get_type['type']==1: //时时彩
				$an_data=$this->analysis->ssc($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],5);
				break;
			case $get_type['type']==2: //11选5
				$an_data=$this->analysis->x11x5($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],5);
				break;
			case $get_type['type']==3: //排列3 福彩3D
				$an_data=$this->analysis->p3($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],3);
				break;
			case $get_type['type']==4: //快乐十分
				$an_data=$this->analysis->kl10($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],8);
				break;
			case $get_type['type']==6: //PK10
				$an_data=$this->analysis->pk10($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],10);
				break;
			case $get_type['type']==8: //快乐8
				$an_data=$this->analysis->k8($kjData,$get_data);
				break;
			case $get_type['type']==9: //快3
				$an_data=$this->analysis->k3($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],3);
				break;
			case $get_type['type']==11: //幸运28
				$an_data=$this->analysis->lucky28($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],1);
				break;
			case $get_type['type']==12: //六合彩
				$an_data=$this->analysis->lhc($kjData,$get_data);
				break;
		}
		if($an_data)
		{
			$sql="delete from {$this->prename}trend where type={$type}";
			$this->query($sql);
			if($get_type['type']==12)
			{
				$sql="insert into {$this->prename}trend(type,number,time,lost,hot,sp_lost,sp_hot,data)
				VALUES({$type},'{$actionNo}',".time().",'". json_encode($an_data['lost'],JSON_FORCE_OBJECT)."','". json_encode($an_data['hot'],JSON_FORCE_OBJECT)."','". json_encode($an_data['sp_lost'],JSON_FORCE_OBJECT)."','". json_encode($an_data['sp_hot'],JSON_FORCE_OBJECT)."','".$kjData."')";
			}else
			{
				$sql="insert into {$this->prename}trend(type,number,time,lost,hot,clong,data)
				VALUES({$type},'{$actionNo}',".time().",'". json_encode($an_data['lost'],JSON_FORCE_OBJECT)."','". json_encode($an_data['hot'],JSON_FORCE_OBJECT)."','". json_encode($clong,JSON_FORCE_OBJECT)."','".$kjData."')";
			}
			$this->query($sql);
		}
	}
	//end 走势图 by robert
	
}

?>