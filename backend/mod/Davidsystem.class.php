<?php

class Davidsystem extends AdminBase
{
    public $title = '站内信';
    public $pageSize = 15;

    public final function all()
    {
        $this->display('Davidsystem/all.php');
    }

    public final function history()
    {
        $this->display('Davidsystem/history.php');
    }

    public final function history_list()
    {
        $this->display('Davidsystem/history_list.php');
    }

    public final function proxy()
    {
        $this->display('Davidsystem/proxy.php');
    }

    public final function proxy_list()
    {
        $this->display('Davidsystem/proxy_list.php');
    }

    public final function deldaili($id)
    {
        $id = wjStrFilter($id);
        $arr = explode('-', $id);
        $sql = "delete from {$this->prename}inagent where id=?";
        foreach ($arr as $key => $var) {
            $this->update($sql, $arr[$key]);
        }
    }

    public final function win_rank()
    {
        $this->display('Davidsystem/win_rank.php');
    }

    public final function lose_rank()
    {
        $this->display('Davidsystem/lose_rank.php');
    }

    public final function showValidRegistration()
    {
        $this->display('Davidsystem/showValidRegistration.php');
    }

    public final function showValidAgentRegistration()
    {
        $this->display('Davidsystem/showValidAgentRegistration.php');
    }
}
