<?php

class AdminBase extends db
{
	public $pageSize = 50;
    private $admin_session_name = '_ADMIN_';
    private $user;
	public $password_key;
    public $headers;
    public $page = 1;
    public $types;
    public $etypes;
    public $playeds;
    private $expire = 3600;
    //public $prename = $conf['db']['prename'];
    public $settings;
    public $adminLogType = array(
        1 => '提现处理',
        2 => '充值确认',
        3 => '管理员充值',
        4 => '增加用户',
        5 => '修改用户',
        6 => '删除用户',
        7 => '添加管理员',
        8 => '修改管理员密码',
        9 => '删除管理员',
        10 => '修改系统设置',
        11 => '收款渠道设置',
        12 => '彩种设置',
        13 => '玩法设置',
        14 => '等级设置修改',
        15 => '兑换订单处理',
        16 => '积分兑换设置',
        17 => '手动开奖',
        18 => '修改订单',
        19 => '清除管理员',
        20 => '添加支付接口',
        21 => '踢会员下线',
        22 => '大转盘中奖记录处理',
        23 => '修改大转盘设置',
        24 => '修改夺宝奇兵配置',
        25 => '夺宝记录处理',
        26 => '电子银行提款记录删除',
        27 => '电子银行存款冻结',
        28 => '电子银行存款解冻',
        29 => '清除数据库资料',
        30 => '结算分红',
        31 => '重新结算',
        32 => '修改开奖号码',
        88 => 'Payment代付',
        89 => '设置开奖时间',
        90 => '删除开奖时间',
        91 => '设置三方支付',
        92 => '提交代付',
        93 => '批次錄號',
        94 => '延遲投注',
        95 => '修改奖池',
    );

    function __construct($dsn, $user = '', $password = '')
    {
        global $conf;
        $this->prename = $conf['db']['prename'];
		$this->password_key = PASSWORD_KEY;
        if (!session_id()) session_start();
        if (isset($_SESSION[$this->admin_session_name])) {
            //verify IP
            $user_ip = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
            $user_ips = [];
            if (strpos($user_ip, ',') === FALSE) {
                $user_ips = [$user_ip];
            } else {
                $user_ips = explode(',', $user_ip);
            }
            $allowed_ips = ',' . $GLOBALS['ALLOWED_IPS'] . ',';
            $ip_allows_through = false;
            foreach ($user_ips as $ip) {
                if (strrpos($allowed_ips, $ip) !== FALSE) {
                    $ip_allows_through = true;
                    break;
                }
            }
			$ip_allows_through = true; //强制通过IP验证  by robert
            if (!$ip_allows_through) {
                throw new Exception('IP:' . $user_ip);
            }

            $this->user = unserialize($_SESSION[$this->admin_session_name]);
        } else {
            unset($_SESSION[$this->admin_session_name]);
            $headers = getallheaders();
            if (isset($headers['x-load']) || isset($headers['x-form-call'])) {
                $this->actionTemplate = $conf['action']['template'];
                $this->display('session_timeout.php');
            } else {
                header('location: /user/login');
            }
            exit();
        }
        try {

            parent::__construct($dsn, $user, $password);

            $sql = "update {$this->prename}admin_session set accessTime={$this->time} where id=?";
			$this->update($sql, $this->user['sessionId']);

            $x = $this->getRow("select isOnLine,state from {$this->prename}admin_session where uid={$this->user['uid']} and session_key=? order by id desc limit 1", session_id());
            if (!$x['isOnLine'] && $x['state'] == 1) {
                echo "<script>alert('对不起,您的账号在别处登陆,您被强迫下线!');window.location.href='/user/logout'</script>";
                exit();
            } else if (!$x['isOnLine']) {
                echo "<script>alert('对不起,登陆超时或网络不稳定,请重新登陆!');window.location.href='/user/logout'</script>";
                exit();
            }
        } catch (Exception $e) {
            exception_handler($e);
        }

    }
	public function nformat($number,$decimals=3)
	{
		return number_format($number,$decimals,".",",");
	}
    public function addLog($type, $logString, $extfield0 = 0, $extfield1 = '', $operationRecord = '')
    {
        $log = array(
            'uid' => $this->user['uid'],
            'username' => $this->user['username'],
            'type' => $type,
            'actionTime' => $this->time,
            'actionIP' => $this->ip(true),
            'action' => $logString,
            'extfield0' => $extfield0,
            'extfield1' => $extfield1,
            'operationRecord' => $operationRecord
        );
        return $this->insertRow($this->prename . 'admin_log', $log);
    }

    public function getTypes()
    {
        if ($this->types) return $this->types;
        $sql = "select * from {$this->prename}type where isDelete=0";
        return $this->types = $this->getObject($sql, 'id', null, $this->expire);
    }

    public function getETypes()
    {
        if ($this->etypes) return $this->etypes;
        $sql = "select * from {$this->prename}egame where enable=1";
        return $this->etypes = $this->getObject($sql, 'game_type', null, $this->expire);
    }

    public function getPlayeds()
    {
        if ($this->playeds) return $this->playeds;
        $sql = "select * from {$this->prename}played";
        return $this->playeds = $this->getObject($sql, 'id', null, $this->expire);
    }

    public function getSystemSettings($expire = null)
    {
        if ($expire === null) $expire = $this->expire;
        $file = $this->cacheDir . 'systemSettings';
        if ($expire && is_file($file) && filemtime($file) + $expire > $this->time) {
            return $this->settings = unserialize(file_get_contents($file));
        }
        $sql = "select * from {$this->prename}params";
        $this->settings = array();
        if ($data = $this->getRows($sql)) {
            foreach ($data as $var) {
                $this->settings[$var['name']] = $var['value'];
            }
        }
        file_put_contents($file, serialize($this->settings));
        return $this->settings;
    }

    public function getdzpSettings()
    {
        $sql = "select * from {$this->prename}dzpparams";
        $this->dzpsettings = array();
        if ($data = $this->getRows($sql)) {
            foreach ($data as $var) {
                $this->dzpsettings[$var['name']] = $var['value'];
            }
        }
        return $this->dzpsettings;
    }

    public function getdbqbSettings()
    {
        $sql = "select * from {$this->prename}dbqbparams";
        $this->dbqbsettings = array();
        if ($data = $this->getRows($sql)) {
            foreach ($data as $var) {
                $this->dbqbsettings[$var['name']] = $var['value'];
            }
        }
        return $this->dbqbsettings;
    }

    public function getdzyhSettings()
    {
        $sql = "select * from {$this->prename}dzyhparams";
        $this->dzyhsettings = array();
        if ($data = $this->getRows($sql)) {
            foreach ($data as $var) {
                $this->dzyhsettings[$var['name']] = $var['value'];
            }
        }
        return $this->dzyhsettings;
    }

    public function getUser($uid = null)
    {
        if ($uid === null) return $this->user;
        if (is_int($uid)) return $this->getRow("select * from {$this->prename}admin_members where uid='$uid'");
        if (is_string($uid)) return $this->getRow("select * from {$this->prename}admin_members where username=?", $uid);
    }

    public function setUser()
    {
        throw new Exception('这是一个只读属性');
    }

    public function checkLogin()
    {
        if ($user = unserialize($_SESSION[$this->admin_session_name])) return $user;
        echo "<script>alert('对不起,您尚未登录!');window.location.href='/index.php/user/login'</script>";
        exit();
    }

    private function setClientMessage($message, $type = 'Info', $showTime = 3000)
    {
        $message = trim(rawurlencode($message), '"');
        header("X-$type-Message: $message");
        header("X-$type-Message-Times: $showTime");
    }

    protected function info($message, $showTime = 3000)
    {
        $this->setClientMessage($message, 'Info', $showTime);
    }

    protected function success($message, $showTime = 3000)
    {
        $this->setClientMessage($message, 'Success', $showTime);
    }

    protected function warning($message, $showTime = 3000)
    {
        $this->setClientMessage($message, 'Warning', $showTime);
    }

    public function error($message, $showTime = 5000)
    {
        $this->setClientMessage($message, 'Error', $showTime);
        exit;
    }

    public function unescape($source)
    {
        $decodedStr = '';
        $pos = 0;
        $len = strlen($source);
        while ($pos < $len) {
            $charAt = substr($source, $pos, 1);
            if ($charAt == '%') {
                $pos++;
                $charAt = substr($source, $pos, 1);
                if ($charAt == 'u') {
                    $pos++;
                    $unicodeHexVal = substr($source, $pos, 4);
                    $unicode = hexdec($unicodeHexVal);
                    $decodedStr .= $this->u2utf82gb($unicode);
                    $pos += 4;
                } else {
                    $hexVal = substr($source, $pos, 2);
                    $decodedStr .= chr(hexdec($hexVal));
                    $pos += 2;
                }
            } else {
                $decodedStr .= $charAt;
                $pos++;
            }
        }
        return $decodedStr;
    }

    public function u2utf82gb($c)
    {
        $strphp = '';
        if ($c < 0x80) {
            $strphp .= $c;
        } elseif ($c < 0x800) {
            $strphp .= chr(0xC0 | $c >> 6);
            $strphp .= chr(0x80 | $c & 0x3F);
        } elseif ($c < 0x10000) {
            $strphp .= chr(0xE0 | $c >> 12);
            $strphp .= chr(0x80 | $c >> 6 & 0x3F);
            $strphp .= chr(0x80 | $c & 0x3F);
        } elseif ($c < 0x200000) {
            $strphp .= chr(0xF0 | $c >> 18);
            $strphp .= chr(0x80 | $c >> 12 & 0x3F);
            $strphp .= chr(0x80 | $c >> 6 & 0x3F);
            $strphp .= chr(0x80 | $c & 0x3F);
        }
        return $strphp;
    }

    public function addCoin($log)
    {
        if (!isset($log['uid'])) $log['info'] = $this->user['uid'];
        if (!isset($log['info'])) $log['info'] = '';
        if (!isset($log['coin'])) $log['coin'] = 0;
        if (!isset($log['type'])) $log['type'] = 0;
        if (!isset($log['fcoin'])) $log['fcoin'] = 0;
        if (!isset($log['extfield0'])) $log['extfield0'] = 0;
        if (!isset($log['extfield1'])) $log['extfield1'] = '';
        if (!isset($log['extfield2'])) $log['extfield2'] = '';
        $sql = "call setCoin({$log['coin']}, {$log['fcoin']}, {$log['uid']}, {$log['liqType']}, {$log['type']}, '{$log['info']}', {$log['extfield0']}, '{$log['extfield1']}', '{$log['extfield2']}')";
        $this->insert($sql);
    }

    public function getDateCount($date = null)
    {
    	$sql="select 
    	ifnull(sum(m.real_bet),0) as betAmount,
    	ifnull(sum(m.zj),0) as zjAmount,
    	ifnull(sum(m.fandian),0) as fanDianAmount,
    	ifnull(sum(m.broker),0) as brokerageAmount,
    	ifnull(sum(m.rebate),0) as rebateAmount,
    	ifnull(sum(m.bonus),0) as bonusAmount,
    	ifnull(sum(m.cancelOrder),0) as cancelOrderAmount 
    	from {$this->prename}member_report m  where  m.actionTime between $date and ".($date+86400);
		$all = $this->getRow($sql);
    	/*
        if (!$date) $date = strtotime(date('Y-m-d', $this->time));
        $sql = "select count(*) betCount, sum(beiShu*mode*actionNum*(fpEnable+1)) betAmount, sum(bonus) zjAmount from {$this->prename}bets_repl where kjTime between $date and $date+24*3600 and lotteryNo<>'' and isDelete=0";
        $all = $this->getRow($sql);
        $all['fanDianAmount'] = $this->getValue("select sum(coin) from {$this->prename}coin_log_repl where liqType between 2 and 3 and actionTime between $date and $date+24*3600");
        $all['brokerageAmount'] = $this->getValue("select sum(coin) from {$this->prename}coin_log_repl where liqType in(50,51,52,53,56) and actionTime between $date and $date+24*3600");
		 */
        return $all;
    }

    public function getOldDateCount($date = null)
    {
    	$sql="select 
    	ifnull(sum(m.real_bet),0) as betAmount,
    	ifnull(sum(m.zjAmount),0) as zjAmount,
    	ifnull(sum(m.fanDianAmount),0) as fanDianAmount,
    	ifnull(sum(m.brokerageAmount),0) as brokerageAmount,
    	ifnull(sum(m.rebateAmount),0) as rebateAmount,
    	ifnull(sum(m.bonusAmount),0) as bonusAmount,
    	ifnull(sum(m.cancelOrderAmount),0) as cancelOrderAmount 
    	from {$this->prename}report_day m  where  m.date = '{$date}'";
		$all = $this->getRow($sql);
    	/*
        if (!$date) $date = strtotime(date('Y-m-d', $this->time));
        $sql = "select count(*) betCount, sum(beiShu*mode*actionNum*(fpEnable+1)) betAmount, sum(bonus) zjAmount from {$this->prename}bets_repl where kjTime between $date and $date+24*3600 and lotteryNo<>'' and isDelete=0";
        $all = $this->getRow($sql);
        $all['fanDianAmount'] = $this->getValue("select sum(coin) from {$this->prename}coin_log_repl where liqType between 2 and 3 and actionTime between $date and $date+24*3600");
        $all['brokerageAmount'] = $this->getValue("select sum(coin) from {$this->prename}coin_log_repl where liqType in(50,51,52,53,56) and actionTime between $date and $date+24*3600");
		 */
        return $all;
    }

    public function getOldDateCount7()
    {
        $sql="select 
        date,
    	ifnull(sum(m.real_bet),0) as betAmount,
    	ifnull(sum(m.zjAmount),0) as zjAmount,
    	ifnull(sum(m.fanDianAmount),0) as fanDianAmount,
    	ifnull(sum(m.brokerageAmount),0) as brokerageAmount,
    	ifnull(sum(m.rebateAmount),0) as rebateAmount,
    	ifnull(sum(m.bonusAmount),0) as bonusAmount,
    	ifnull(sum(m.cancelOrderAmount),0) as cancelOrderAmount 
    	from {$this->prename}report_day m group by date order by date desc limit 7";
		$all = $this->getRows($sql);
        return $all;
    }

    public function getGameLastNo($type, $actionNo, $actionTime, $time)
    {
        $type = intval($type);
        $types = $this->getTypes();
        if (($fun = $types[$type]['onGetNoed']) && method_exists($this, $fun)) {
            $this->$fun($actionNo, $actionTime, $time);
        }
        return array(
            'actionNo' => $actionNo,
            'actionTime' => $actionTime,
        );
    }

    private function setTimeNo(&$actionTime, &$time = null)
    {
        $actionTime = wjStrFilter($actionTime);
        if (!$time) $time = $this->time;
        $actionTime = date('Y-m-d ', $time) . $actionTime;
    }

    public function noHdCQSSC(&$actionNo, &$actionTime, $time = null)
    {
        $actionNo = wjStrFilter($actionNo);
        $this->setTimeNo($actionTime, $time);
        if ($actionNo == 0 || $actionNo == 120) {
            $actionNo = date('Ymd120', $time - 24 * 3600);
            $actionTime = date('Y-m-d 00:00', $time);
        } else {
            $actionNo = date('Ymd', $time) . substr(1000 + $actionNo, 1);
        }
    }

    public function no0Hd_2(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        $actionNo = date('Ymd', $time) . substr(1000 + $actionNo, 1);
    }

    public function no0Hd(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
//20160718 13:00
//{$this->prename}typ.onGetNoed = no0Hd
//修改后台 江西11选5 开奖数据 期号格式 yyyymmddxxx --> yyyymmddxx
//$actionNo=date('Ymd',$time).substr(1000+$actionNo,1);
        $actionNo = date('Ymd', $time) . substr(100 + $actionNo, 1);
    }

    public function no0Hd_1(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        $actionNo = date('Ymd', $time) . substr(100 + $actionNo, 1);
    }

    public function pai3(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        $actionNo = date('Yz', $time) + (int)$this->settings['pai3dDiffDay'];
        $actionNo = substr($actionNo, 0, 4) . substr(substr($actionNo, 4) + 1001, 1);
        if ($actionTime >= date('Y-m-d H:i:s', $time)) {
        } else {
            $actionTime = date('Y-m-d 18:30', $time);
        }
    }

    public function noxHd(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        if ($actionNo >= 84) {
            $time -= 24 * 3600;
        }
        $actionNo = date('Ymd', $time) . substr(1000 + $actionNo, 1);
    }

    public function no0Hd_3(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        $actionNo = date('Ymd', $time).substr(1000 + $actionNo, 1);
    }

    public function BJpk10(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        $actionNo = 729391+ 44*(strtotime(date('Y-m-d', $time))-strtotime('2019-02-11'))/3600/24+$actionNo+intval($this->settings['bjpk10DiffDay']);
        //$actionNo = 179 * (strtotime(date('Y-m-d', $time)) - strtotime('2007-11-18')) / 3600 / 24 + $actionNo + intval($this->settings['bjpk10DiffDay']);
    }
    public function FTpk10(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);

		if ($actionNo >= 132) {
			$time -= 24 * 3600;
		}

        $actionNo = date('Ymd', $time) . substr(1000 + $actionNo, 1);
    }

    public function no0Hdx(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        $actionNo = date('Ymd', $time) . substr(10000 + $actionNo, 1);
    }

    public function Kuai8(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        //$actionNo = 179 * (strtotime(date('Y-m-d', $time)) - strtotime('2004-09-19')) / 3600 / 24 + $actionNo - 77 - 1253;
        //$actionNo = 179 * (strtotime(date('Y-m-d', $time)) - strtotime('2004-09-19')) / 3600 / 24 + $actionNo - 77 - 1253-2527;
        //$TimeCheck=((strtotime(date('Y-m-d', $time)) - strtotime('2017-07-17'))/86400);
		/*
        if($TimeCheck<=1)
		{
			$TimeCheck=0;
		}
		 *
		 */
        //$actionNo=834579+($TimeCheck*179)+ $actionNo-703;
		$actionNo = 935368+ 179*(strtotime(date('Y-m-d', $time))-strtotime('2019-02-11'))/3600/24+$actionNo+intval($this->settings['bjkl8DiffDay']);
    }

    public function noHd(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        $actionNo = date('Ymd', $time) . substr(100 + $actionNo, 1);
    }

    public function pai3x(&$actionNo, &$actionTime, $time = null)
    {
        $this->setTimeNo($actionTime, $time);
        $actionNo = date('Yz', $time) + (int)$this->settings['pai3dDiffDay'];
        $actionNo = substr($actionNo, 0, 4) . substr(substr($actionNo, 4) + 1001, 1);
        if ($actionTime >= date('Y-m-d H:i:s', $time)) {
        } else {
            $actionTime = date('Y-m-d 20:30', $time);
        }
    }

    //菲律宾1.5分彩
	private function PHFFC(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);

        if ($actionNo == 920) {
            $_t = strtotime($actionTime) - (24 * 3600);
        } else {
            $_t = strtotime($actionTime);
        }
		$actionNo = date('Ymd', $_t).substr(1000 + $actionNo, 1);

	}
    //东京1.5分彩
	private function JPFFC(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);

        if ($actionNo <= 39) {
            $_t = strtotime($actionTime) + 24 * 3600;
        } else {
            $_t = strtotime($actionTime);
        }
		$actionNo = date('Ymd', $_t).substr(1000 + $actionNo, 1);

	}
    //韩国1.5分彩
	private function KRFFC(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		//$actionNo = (strtotime(date('Y-m-d')) - strtotime('2012-01-29 17:00:00')) / 86400 * 960 - 3 + $actionNo;
		$actionNo = ceil((strtotime(date('Y-m-d',time())) - strtotime('2012-01-30')) / 86400)* 960 +197+ $actionNo;

	}

	//台湾宾果twbingo
	private function TWFFC(&$actionNo, &$actionTime, $time=null) {
		$this->setTimeNo($actionTime, $time);
		$_t = strtotime($actionTime);
		$actionNo=(date('Y', $_t)-1911)*1000000+$actionNo;
		$actionNo+=(date('z', $_t))*203;
    }
    
	// 六合彩
	public function no6Hd(&$actionNo, &$actionTime, $time=null){
		if(!$time) $time=$this->time;
		$actionNo=substr(date('Yz', $time),0,4).substr(1000+$actionNo,1);
	}
}

?>