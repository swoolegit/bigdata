<?php

class Business extends AdminBase
{
    public $pageSize = 50;

    public final function cash()
    {
        $this->display('business/cash.php');
    }

    public final function cashLog()
    {
        $this->display('business/cash-log.php');
    }

    public final function cashLogList()
    {
        $this->display('business/cash-log-list.php');
    }

    public final function cashActionModal($id)
    {
        $id = intval($id);
        $sql = "select  c.*, u.username userAccount 
        from  {$this->prename}member_cash c, {$this->prename}members u 
        where  c.uid=u.uid and c.id=?";
        $data = $this->getRow($sql, $id);
        if (!$data) throw new Exception('参数出错');
        switch ($data['state']) {
            case 0:
                throw new Exception('提现已经到帐');
            case 1:
            case -1:
                $this->display('business/cash-action.php', 0, $data);
                break;
            case 2:
                throw new Exception('用户已经取消提现申请');
            case 3:
                throw new Exception('提现已经支付过了');
            case 4:
                throw new Exception('提现已经失败');
            case 5:
                throw new Exception('提现己经完成');
            case 6:
                throw new Exception('删除');
            default:
                throw new Exception('未知出错');
        }
    }

    public final function cashActionModal2($id)
    {
        $id = intval($id);
        $sql = "select  c.*, u.username userAccount 
        from  {$this->prename}member_cash c, {$this->prename}members u 
        where  c.uid=u.uid and c.id=?";
        $data = $this->getRow($sql, $id);
        if (!$data) throw new Exception('参数出错');
        switch ($data['state']) {
            case 0:
                throw new Exception('提现已经到帐');
            case 1:
            case -1:
                $sql="select id,merid,addcrykey,name from `{$this->prename}params_thirdpay` where payout_status=1";
                $third = $this->getRow($sql);
                if(!$third)
                {
                    throw new Exception('没有激活代付');
                }

                $amount=$this->get_third_amount($third);
                $amount=[
                    'blance'=>100,
                    'third_name'=>$third['name'],
                ];
                $data=array_merge($data,$amount);
                $this->display('business/cash-action2.php', 0, $data);
                break;
            case 2:
                throw new Exception('用户已经取消提现申请');
            case 3:
                throw new Exception('提现已经支付过了');
            case 4:
                throw new Exception('提现已经失败');
            case 5:
                throw new Exception('提现己经完成');
            case 6:
                throw new Exception('删除');
            default:
                throw new Exception('未知出错');
        }
    }

    private final function get_third_amount($third)
    {
        switch(true)
        {
            case $third['id']==1:
            break;
            case $third['id']==2:
            break;
            case $third['id']==3:
            break;
            case $third['id']==4:
            break;
            case $third['id']==5: //艾付接口
                $merchant_no = $third['merid'];            //商户号
                $key = $third['addcrykey'];  //商户接口秘钥
                //MD5签名
                $src = "merchant_no=" . $merchant_no . "&key=" . $key;
                $sign = md5($src);
                $data=[
                    "merchant_no"=>$merchant_no,
                    "sign"=>$sign,
                ];
                //接口地址                
                $url = "https://pay.ifeepay.com/withdraw/queryBalance?".http_build_query($data); 
                $content = file_get_contents($url."?".$string);
                $result = json_decode($content,1);
            break;
        }
        return $result;
    }

    public final function betInfo($id)
    {
        $id = intval($id);
        $this->getTypes();
        $this->display('business/bet-info.php', 0, $id);
    }

    public final function betIssueInfo()
    {
        $this->display('business/betIssueInfo.php');
    }

    public final function getTip()
    {
        $sql = "select id from {$this->prename}member_cash where state=1 and actionTime>" . strtotime('00:00');
        if ($data = $this->getCol($sql)) {
            if ($cookie = $_COOKIE['cash-tip']) {
                $cookie = explode(',', $cookie);
                if (!array_diff($data, $cookie)) return array('flag' => false);
            }
            $data = implode(',', $data);
            if ($data) setcookie('cash-tip', $data);
            return array(
                'flag' => true,
                'message' => '有新的提现请求需要处理',
                'buttons' => '前往处理:goToDealWithCash|忽略:defaultCloseModal'
            );
        }
    }

    public final function getRecharge()
    {
        $sql = "select id from {$this->prename}member_recharge where state=0 and isDelete=0 order by id asc";
        if ($data = $this->getCol($sql)) {
            if (array_key_exists('recharge-tip', $_COOKIE) && ($cookie = $_COOKIE['recharge-tip'])) {
                $cookie = explode(',', $cookie);
                if (!array_diff($data, $cookie)) return array('flag' => false);
            }
            $data = implode(',', $data);
            if ($data) setcookie('recharge-tip', $data);
//<script>window.onload=setTimeout$(this).dialog("destroy"), 3000);</script>
            return array(
                'flag' => true,
                'message' => '有新的充值请求需要处理<script>setTimeout("defaultCloseModal()", 2000);</script>',
                'buttons' => '前往处理:goToDealWithRecharge|忽略:defaultCloseModal'
            );
        }
    }

    public final function getCash()
    {
        $sql = "select id from {$this->prename}member_cash where state=1 and isDelete=0 order by id asc";
        if ($data = $this->getRows($sql)) {
            $temp_data = array();
            foreach ($data as $v) array_push($temp_data, $v['id']);
            $data = $temp_data;
			/* 提现申请没处理时,必须强制提示
            if (array_key_exists('cash-tip', $_COOKIE) && ($cookie = $_COOKIE['cash-tip'])) {
                $cookie = explode(',', $cookie);
                if (!array_diff($data, $cookie)) return array('flag' => false);
            }
			*/
            $data = implode(',', $data);
            if ($data) setcookie('cash-tip', $data);
            return array(
                'flag' => true,
                'message' => '有新的提现请求需要处理',
                'buttons' => '前往处理:goToDealWithCash|忽略:defaultCloseModal'
            );
        }
    }

    public final function rechargeInfo($id)
    {
        $id = intval($id);
        //$this->getTypes();
        //$this->getPlayeds();
        $this->display('business/recharge-info.php', 0, $id);
    }

    public final function cashInfo($id)
    {
        $id = intval($id);
        $this->getTypes();
        $this->getPlayeds();
        $this->display('business/cash-info.php', 0, $id);
    }

    //代付处理
    public final function cashDealWith2(){
        //$this->getSystemSettings();
		$sql="select * from {$this->prename}params_thirdpay where payout_status=1";
		$third=$this->getRow($sql);
		$merchant_no=$third["merid"];
		$key=$third["addcrykey"];

		$id=$_POST['id'];
		$sql="select c.state,c.amount, c.account,c.username,c.countname,c.bankId
			from  {$this->prename}member_cash c 
			where c.state=1 and c.id=".$id;
		$data=$this->getRow($sql);

		$bank_name=$data['countname'];
		$username=$data['username'];
		$account=$data['account'];
        $amount=intval($data['amount']);
		switch(true)
		{
			case $third['id']==3: //随意付代付
					$order_no = "payout_".$id;
					$string="parter={$merchant_no}&orderid={$order_no}&value={$amount}&payeebank={$bank_name}&account={$account}&cardname={$username}";
					$sign=md5("{$string}{$key}");
					$string.="&sign=".$sign;
					$url = "https://transfer.easyipay.com/interface/transfer/index.aspx";
					$content = file_get_contents($url."?".$string);
					$ca=explode("&", $content);
					$order_no="";
					if($ca[0]=="error")
					{
						echo $ca[1];
						exit;
					}else
					{
						$temp=explode("=", $ca[2]);
						$order_no=$temp[1];
					}
                break;
            case $third['id']==5: //艾付代付
                $sql="select c.ename  from  {$this->prename}bank_list c where c.id=".$data['bankId'];
                $bank=$this->getRow($sql);

                $merchant_no = $merchant_no;									//商户号
                $order_no = "payout_".$id;										//商户订单号
                $card_no = $account;							        		//银行卡号
                $account_name = base64_encode($username);						//银行开户名,使用base64进行编码（UTF-8编码）
                $bank_branch = "";												//银行支行名称,对公账户需填写，使用base64进行编码（UTF-8编码）
                $cnaps_no = "";													//银行联行号,银行唯一识别编号，对公账户需填写
                $bank_code = $bank['ename'];											//银行代码,参考银行代码对照表
                $bank_name= base64_encode($bank_name);										//银行名称,参考银行代码对照表，使用base64进行编码（UTF-8编码）
                $amount=$amount;													//代付金额,最多两位小数
                $pay_pwd="F571C8FB03FE5720ED0B760B86779916524F3449674B";		//支付密码(需先在商户後台配置支付密码,获取支付密码)
                $key = $key;	                                				//商户接口秘钥
            
                //MD5签名
                $src = "merchant_no=" . $merchant_no . "&order_no="
                        . $order_no . "&card_no=" . $card_no . "&account_name=" . $account_name
                        . "&bank_branch=" . $bank_branch . "&cnaps_no="
                        . $cnaps_no . "&bank_code=" . $bank_code
                        . "&bank_name=" . $bank_name . "&amount=" . $amount . "&pay_pwd="
                        . $pay_pwd;
                $src .= "&key=" . $key;
                $sign = md5($src);
                $data=[
                    "merchant_no"=>$merchant_no,
                    "order_no"=>$order_no,
                    "card_no"=>$card_no,
                    "account_name"=>$account_name,
                    "bank_branch"=>$bank_branch,
                    "cnaps_no"=>$cnaps_no,
                    "bank_code"=>$bank_code,
                    "bank_name"=>$bank_name,
                    "amount"=>$amount,
                    "sign"=>$sign,
                ];                
                //接口地址
                $url = "https://pay.ifeepay.com/withdraw/singleWithdraw";
                $content = file_get_contents($url."?".http_build_query($data));
            break;
		}

		$this->addLog(92, "代付:{$data['account']}:{$data['amount']}:{$data['bank_code']}:{$data['username']}:{$data['bank_name']}:[{$order_no}]:回传=>".$content);
		$sql = "update {$this->prename}member_cash set order_no='{$order_no}',state=3 where id=?";
		$this->update($sql, $id);
		echo "提交代付成功!!";
		exit;
    }
    
    public final function cashDealWith($id)
    {
        $id = intval($id);
        $actionFlag = intval($_POST['type']);
        $info = $_POST['info'];
        $sql = "select b.name bankName, c.*, u.username,u.forTest userAccount 
        from {$this->prename}bank_list b, {$this->prename}member_cash c, {$this->prename}members u
         where b.isDelete=0 and c.bankId=b.id and c.uid=u.uid and c.id=?";
        $data = $this->getRow($sql, $id);
        if (!$data) throw new Exception('参数出错');
		if($data['forTest']==1)
		{
			throw new Exception('测试帐号无法提现');
		}
        switch ($data['state']) {
            case 0:
                throw new Exception('提现已经到帐');
            case 1:
            case -1:
                if ($actionFlag) {
                    $log = array(
                        'uid' => $data['uid'],
                    );
                    $row = array(
                        'info' => $info,
                        'state' => $actionFlag
                    );
                    $log['info'] = "提现[{$data['id']}]处理失败";
                    $log['coin'] = $data['amount'];
                    $log['liqType'] = 8;
                    $log['extfield0'] = $data['id'];
                } else {
                    $row = array('state' => 0, 'cashTime' => time());
                }
                if ($actionFlag != -1) {
                    $this->beginTransaction();
                    try {
                        if(isset($log))
                        {
                            $this->addCoin($log);
                        }
                        $this->updateRows($this->prename . 'member_cash', $row, 'id=' . $id);
                        $this->commit();
                        $this->addLog(1, $this->adminLogType[1] . '[ID:' . $data['id'] . ']', $data['uid'], $data['username']);
                        return '操作成功';
                    } catch (Exception $e) {
                        $this->rollBack();
                        throw $e;
                    }
                } else {
                    $this->updateRows($this->prename . 'member_cash', $row, 'id=' . $id);
                }
                break;
            case 4:
                throw new Exception('提现已经失败');
            case 6:
                throw new Exception('已删除');
            default:
                throw new Exception('未知出错');
        }
    }

    public final function cashLogDelete($id)
    {
        $id = intval($id);
        $sql = "select b.name bankName, c.*, u.username userAccount from {$this->prename}bank_list b, {$this->prename}member_cash c, {$this->prename}members u where b.isDelete=0 and c.bankId=b.id and c.uid=u.uid and c.id=?";
        $data = $this->getRow($sql, $id);
        if (!$data) throw new Exception('参数出错');
        switch ($data['state']) {
        }
        if ($this->updateRows($this->prename . 'member_cash', array('isDelete' => 5), 'id=' . $data['id'])) {
            return '操作成功';
        }
        throw new Exception('未知出错');
    }

    public final function rechargeLog()
    {
        $this->display('business/recharge-log.php');
    }

    public final function rechargeLogList()
    {
        $this->display('business/recharge-log-list.php');
    }

    public final function rechargeModal()
    {
        $this->display('business/recharge-modal.php');
    }

    // 充值佣金
    public function rechargeCommission($coin, $uid, $rechargeId, $rechargeSerialize = '')
    {
        $this->getSystemSettings();
        if (floatval($this->settings['rechargeCommissionAmount']) > $coin) {
            return;
        }

        // 检查今日充值记录，非首充便返回
        $time = strtotime('00:00');
        $sql = "SELECT id FROM {$this->prename}member_recharge WHERE rechargeTime >={$time} AND uid ={$uid} LIMIT 1, 1";
        if ($this->getValue($sql)) {
            return;
        }

        $log = array(
            'liqType' => 52,
            'info' => '充值佣金',
            'extfield0' => $rechargeId,
            'extfield1' => $rechargeSerialize
        );

        // 自身
        if ($rechargeSelfCommission = floatval($this->settings['rechargeSelfCommission'])) {
            $log['coin'] = $rechargeSelfCommission;
            $log['uid'] = $uid;
            $this->addCoin($log);
        }

        $sql = "SELECT parentId FROM {$this->prename}members WHERE uid =?";
        // 上家
        if ($parentId = $this->getValue($sql, $uid)) {
            if ($rechargeCommission = floatval($this->settings['rechargeCommission'])) {
                $log['coin'] = $rechargeCommission;
                $log['uid'] = $parentId;
                $this->addCoin($log);
            }

            // 上上家
            if ($parentId = $this->getValue($sql, $parentId)) {
                if ($rechargeCommission2 = floatval($this->settings['rechargeCommission2'])) {
                    $log['coin'] = $rechargeCommission2;
                    $log['uid'] = $parentId;
                    $this->addCoin($log);
                }
            }
        }
    }

    public final function rechargeAction()
    {
        // if ($uIN == 1) {
        //     $uid = intval($uid);
        //     if ($uid <= 0) throw new Exception('用户ID不正确');
        // }
        // http://admin.xgame.wa/index.php/business/rechargeAction/qatest1/100.00/2/管理员充值
		$amount=$_POST['amount'];
		$username=$_POST['username'];
		$uIN=$_POST['uIN'];
		$type=$_POST['type'];       
        $amount = floatval($amount);
        $data = array(
            'amount' => $amount,
            'rechargeAmount' => $amount, // 充值金额也要写入实际到帐栏位
            'actionUid' => $this->user['uid'],
            'actionIP' => $this->ip(true),
            'actionTime' => $this->time,
            'rechargeTime' => $this->time,
            'memo' => $_POST['comment'],
        );
		$liqType=1;
		$info='充值成功';
        if ($type == '3') {
            // 活动赠送不列入实际到帐
            $liqType=57;
			$info='活动赠送';
            unset($data['rechargeAmount']);
        }
        if ($type == '1') {
            $liqType=4;
			$info='人工扣减';
			$data['rechargeAmount']=$data['rechargeAmount']*-1;
			$data['amount']=$data['amount']*-1;
        }

		$u=explode(",", $username);
		$u=array_unique($u);
		foreach($u as $k=>$v)
		{
			//if($this->getValue("select uid from {$this->prename}members where username=?", $v))
			//throw new Exception("用户名已经存在。".$v);
			if($uIN==1){
				$user=$this->getRow("select uid, username, coin, fcoin from {$this->prename}members where uid=$v");
			}else{
				$user=$this->getRow("select uid, username, coin, fcoin from {$this->prename}members where username='$v'");
			} 
			if(!$user) throw new Exception('用户不存在');
			//return;
		}
		$num=0;
		foreach($u as $k=>$v)
		{

            $result = $this->getRow("call pre_make_key(@orederKey)");
            $data['rechargeId'] = $result['_key'];
            $this->beginTransaction();
            try {
                if ($uIN == 1) {
                    $user = $this->getRow("select uid, username, coin, fcoin from {$this->prename}members where uid=$v");
                } else {
                    $user = $this->getRow("select uid, username, coin, fcoin from {$this->prename}members where username='$v'");
                }
                if (!$user) throw new Exception('用户不存在');
                $data['uid'] = $user['uid'];
                $data['coin'] = $user['coin'];
                $data['username'] = $user['username'];
                $data['info'] = '充值成功';
                $data['rechargeModel'] = '2';
                $data['state'] = 1;
                switch(true)
                {
                    case $type=='2' :
                    
                        $this->rechargeCommission($data['amount'], $data['uid'], $dataId, $data['rechargeId']);
                        if ($this->insertRow($this->prename . 'member_recharge', $data)) {
                            $dataId = $this->lastInsertId();
                            $this->addCoin(array(
                                'uid' => $user['uid'],
                                'liqType' => $liqType,
                                'coin' => $data['amount'],
                                'extfield0' => $dataId,
                                'extfield1' => $data['rechargeId'],
                                'info' => $info
                            ));
                        }
                        $cashLimit=($data['amount']/100)*$this->settings['cashMinAmount'];
                        $sql = "update {$this->prename}members set cashLimit={$cashLimit} where uid=?";
                        $this->update($sql, $user['uid']);					
                    break;
                    case $type == '3' || $type == '1' :
                        $this->addCoin(array(
                            'uid' => $user['uid'],
                            'liqType' => $liqType,
                            'coin' => $data['amount'],
                            'info' => $info
                        ));
                    break;
                }
                $this->addLog(3, $this->adminLogType[3] . '[ID:' . $dataId . ']', $data['uid'], $data['username']);
                $this->commit();
                $num++;
            } catch (Exception $e) {
                $this->rollBack();
                throw $e;
            }
        }
        return "充值成功 ".$num."个帐号";
    }

    public final function rechargeDelete($id)
    {
        $id = intval($id);
        if ($this->updateRows($this->prename . 'member_recharge', array('state' => 2), 'id=' . $id)) {
            return '操作成功';
        } else {
            throw new Exception('操作失败');
        }
    }

    public final function rechargeActionModal($id)
    {
        $id = intval($id);
        $this->display('business/rechargeOn-modal.php', 0, $id);
    }

    public final function rechargeHandle()
    {
        $para = $_POST;
        $id = $para['id'];
        unset($para['username']);
        $sql = "select * from {$this->prename}member_recharge where id=?";
        $data = $this->getRow($sql, $id);

        if (!$data) throw new Exception('参数出错');
        if ($data['state']) throw new Exception('充值已经到帐，请不要重复确认');
        if ($data['isDelete']) throw new Exception('充值已经被删除');
        try {
            $this->beginTransaction();
            $para = array_merge(
                array(
                    'rechargeAmount' => $para['rechargeAmount'], 
                    'state' => 1, 
                    'memo' => '手动确认', 
                    'actionUid' => $this->user['uid'], 
                    'rechargeTime' => $this->time, 
                    'actionIP' => $this->ip(true)), 
                    $this->getRow("select coin from {$this->prename}members where uid={$data['uid']}"));
            $this->updateRows($this->prename . 'member_recharge', $para, 'id=' . $data['id']);
            if ($this->updateRows($this->prename . 'member_recharge', $para, 'id=' . $data['id'])) {
                $this->addCoin(array(
                    'uid' => $data['uid'],
                    'coin' => $data['amount'],
                    'liqType' => 1,
                    'extfield0' => $data['id'],
                    'extfield1' => $data['rechargeId'],
                    'info' => '充值'
                ));

                $bonus = $para['rechargeAmount'] - $data['amount'];
                if($bonus > 0) {
                    $this->addCoin(array(
                        'uid' => $data['uid'],
                        'coin' => $bonus,
                        'liqType' => 54,
                        'extfield0' => $data['id'],
                        'extfield1' => $data['rechargeId'],
                        'info' => '充值赠送'
                    ));
                }
                $parent = $this->getRow("select parentId from {$this->prename}members where uid={$data['uid']}");

                $sql = "SELECT parentId FROM {$this->prename}members WHERE uid =?";
                // 充值赠送 上家
                if ($parentId = $this->getValue($sql, $data['uid'])) {
                    if ($this->settings['czzsParent'] > 0) {
                        $this->addCoin(array(
                            'uid' => $parentId,
                            'coin' => number_format($data['amount'] * $this->settings['czzsParent'] / 100.00, 2, '.', ''),
                            'liqType' => 54,
                            'extfield0' => $data['id'],
                            'extfield1' => $data['rechargeId'],
                            'extfield2' => '上家',
                            'info' => '充值赠送'
                        ));
                    }

                    // 充值赠送 上上家
                    if ($this->settings['czzsTop'] > 0) {
                        if ($parentTop = $this->getValue($sql, $parentId)) {
                            $this->addCoin(array(
                                'uid' => $parentTop,
                                'coin' => number_format($data['amount'] * $this->settings['czzsTop'] / 100.00, 2, '.', ''),
                                'liqType' => 54,
                                'extfield0' => $data['id'],
                                'extfield1' => $data['rechargeId'],
                                'extfield2' => '上上家',
                                'info' => '充值赠送'
                            ));
                        }
                    }

                }

            }
            $this->rechargeCommission($data['amount'], $data['uid'], $data['id'], $data['rechargeId']);
            $this->addLog(2, $this->adminLogType[2] . '[充值编号:' . $data['rechargeId'] . ']', $data['uid'], $data['username']);
            $this->commit();
            echo '操作成功';
        } catch (Exception $e) {
            $this->rollBack();
            throw $e;
        }
    }

    public final function betInfoUpdate($id)
    {
        $id = intval($id);
        $this->display('business/update-bet-info.php', 0, $id);
    }

    public final function betinfoUpdateed()
    {
        throw new Exception('最近因滥用发生问题，系统检修当中暂不提供此服务');
        $para = $_POST;
        $betid = $para['betid'];
        $uid = $para['uid'];
        $username = $para['username'];
        unset($para['betid']);
        unset($para['uid']);
        unset($para['username']);
        $bet = $this->getRow("select * from {$this->prename}bets_repl where id={$betid}");
        if (!$bet) throw new Exception('单号不存在');
        if ($bet['lotteryNo']) throw new Exception('已开奖过，不能再修改投注信息');
        if ($this->updateRows($this->prename . 'bets', $para, 'id=' . $betid)) {
            $this->addLog(18, $this->adminLogType[18] . '[投注编号：' . $betid . ']', $uid, $username);
            echo '修改成功';
        } else {
            throw new Exception('未知出错');
        }
    }

    public final function betLog()
    {
        $this->display('business/bet-log.php');
    }

    public final function ebetLog()
    {
        $this->display('business/ebet-log.php');
    }

    public final function betLog_list()
    {
        $this->display('business/bet-log-list.php');
    }

    public final function ebetLog_list()
    {
        $this->display('business/ebet-log-list.php');
    }

    public final function ecardLog()
    {
        $this->display('business/ecard-log.php');
    }

    public final function ecardLog_list()
    {
        $this->display('business/ecard-log-list.php');
    }

    public final function betInfoCheck($id){
		$id=intval($id);
		$this->getTypes();
		$this->display('business/bet-info-check.php',0,$id);
	}
    // public final function znzLog()
    // {
    //     $this->display('business/bet-znz-log.php');
    // }

    public final function coinLog()
    {
        $this->display('business/coin-log.php');
    }

    public final function coinLogList()
    {
        $this->display('business/coin-log-list.php');
    }
	//艾付
    public final function getCashStatusAifu(){
		$json = [
			'merchant_no' => '144790007168',
			'key' => 'c0c9bc2b-979f-11e7-840d-4797948f370c'
		];

//		$id = 'ai'.$_GET['id'];
		$id = $_GET['id'];
		$string = ("merchant_no={$json['merchant_no']}&order_no={$id}&key={$json['key']}");
		#echo "https://pay.ifeepay.com/withdraw/queryBalance?merchant_no={$json['merchant_no']}&sign=".md5($string);

		$result = json_decode(file_get_contents("https://pay.ifeepay.com/withdraw/queryOrder?merchant_no={$json['merchant_no']}&order_no={$id}&sign=".md5($string)),1);
		die(json_encode($result));

	}

	
	//艾付
    public final function getCashAmountAifu(){
		$json = [
			'merchant_no' => '144790007168',
			'key' => 'c0c9bc2b-979f-11e7-840d-4797948f370c'
		];
		$string = ("merchant_no={$json['merchant_no']}&key={$json['key']}");
		#echo "https://pay.ifeepay.com/withdraw/queryBalance?merchant_no={$json['merchant_no']}&sign=".md5($string);

		$result = json_decode(file_get_contents("https://pay.ifeepay.com/withdraw/queryBalance?merchant_no={$json['merchant_no']}&sign=".md5($string)),1);
		die(json_encode($result));

	}

	//艾付
    public final function goPaymentAifu(){

		$json = [
			'merchant_no' => '144790007168',
			'key' => 'c0c9bc2b-979f-11e7-840d-4797948f370c'
		];
		$params = [
			'merchant_no' => $json['merchant_no'],
			'order_no' => 'ai'.intval($_GET['id']),
			'card_no' => $_GET['account'],
			'account_name' => (base64_encode($_GET['username'])),
			'bank_branch' => '',
			'cnaps_no' => '',
			'bank_code' => $_GET['bankId'],
			'bank_name' => (base64_encode($_GET['abname'])),
			'amount' => sprintf("%.2f", $_GET['amount']),
		];
		//print_r($params);
		$sign = '';
		foreach ($params as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$sign .= "pay_pwd=6A2FC875ED06AAB1A09FBFC9EEA7E64E745755627642&key={$json['key']}";
		$params['sign'] = md5(($sign));


		$string = http_build_query($params);
		//echo "https://pay.ifeepay.com/withdraw/singleWithdraw?".$string;
		//exit;
		$content = file_get_contents("https://pay.ifeepay.com/withdraw/singleWithdraw?".$string );
		#echo "https://pay.ifeepay.com/withdraw/singleWithdraw?".$string;
		$content = iconv("gb2312","utf-8//IGNORE",$content);

		$result = json_decode($content,1);
		header('Content-Type: application/json; charset=utf-8');

		$this->addLog(88, "艾付代付:{$_GET['user_account']}:{$_GET['amount']}:{$_GET['abname']}:{$_GET['username']}:{$_GET['account']}:[{$params['order_no']}]:回传=>".json_encode($result), 0, '', json_encode($params));

		if($result)
		{
			if($result["result_code"] == "000000")
			{
				$_GET['type']=0;
				$sql = "update {$this->prename}member_cash set order_no='{$result['order_no']}' where id=?";
				$this->update($sql, $_GET['id']);	
				//$this->cashDealWith($_GET['id']);
			}
			echo json_encode($result);
		}
		/*
			[retCode] => 0
			[order_no] => 201704131444451377
		*/
		exit;
		//die(json_encode($result));

	}

	//牛付
    public final function getCashAmount(){
		$niufu = [
			'partnerId' => '0755000040',
			'key' => '25f03fac-1096-11e7-8fb7-11c807d5574d'
		];
		$string = urlencode("partnerId={$niufu['partnerId']}&key={$niufu['key']}");
		$result = json_decode(file_get_contents("https://pay.newpaypay.com/partner/wallet/queryBalance?partnerId={$niufu['partnerId']}&sign=".md5($string)),1);
		die(json_encode($result));

	}
	//牛付
    public final function goPayment(){

		$niufu = [
			'partnerId' => '0755000040',
			'key' => '25f03fac-1096-11e7-8fb7-11c807d5574d'
		];
		$params = [
			'partnerId' => $niufu['partnerId'],
			'payeeBankAccount' => $_GET['account'],
			'payeeBankAccountName' => (base64_encode($_GET['username'])),
			'payeeBankBranch' => '',
			'cnapsNo' => '',
			'bankId' => $_GET['bankId'],
			'abname' => (base64_encode($_GET['abname'])),
			'payeeTransferAmount' => sprintf("%.2f", $_GET['amount']),
		];

		$sign = '';
		foreach ($params as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$sign .= "transferPwd=04227B4A372B74E9F0D601D6B6ABEBF3565934484754&key={$niufu['key']}";
		$params['sign'] = md5(urlencode($sign));


		$string = http_build_query($params);
		$result = json_decode(file_get_contents("https://pay.newpaypay.com/partner/wallet/singleTransfer?".$string ),1);
		header('Content-Type: application/json; charset=utf-8');

		$this->addLog(88, "牛付代付:{$_GET['user_account']}:{$_GET['amount']}:{$_GET['abname']}:{$_GET['username']}:{$_GET['account']}:回传=>".json_encode($result), 0, '', json_encode($params));


		echo json_encode($result);
		/*
			[retCode] => 0
			[order_no] => 201704131444451377
		*/
		exit;
		//die(json_encode($result));

	}
	//天合宝
    public final function goPaymentTianhebao(){

		$tian = [
			'userid' => '1565',
			'key' => '7053a66c5b924fed8896c50e42e085b0'
		];
/*
		print_r( $_GET);
		exit;
    [bankId] => 1004
    [abname] => 浦发银行
    [username] => dio001
    [account] => fdsafdsfds
    [amount] => 100.000
    [user_account] => dio001
*/

		$username = iconv("utf-8","gb2312//IGNORE",$_GET['username']);
		$params = [
			'userid' => $tian['userid'],
			'key' => $tian['key'],
			'applyMoney' => intval($_GET['amount']),
			'payeeAccount' => $_GET['account'],
			'bankCode' => $_GET['bankId'],
			'payeeUserName' => urlencode($username),
		];

		/*$sign = '';
		foreach ($params as $key => $val) {
			$sign .= $key.'='.$val.'&';
		}
		$sign .= "transferPwd=04227B4A372B74E9F0D601D6B6ABEBF3565934484754&key={$tian['key']}";
		$params['sign'] = md5(urlencode($sign));*/
		

		$string = http_build_query($params);

		$result = file_get_contents("http://pay.zhongyishentu.com/applyCost.aspx?".$string );
		header('Content-Type: application/json; charset=utf-8');

		$this->addLog(88, "代付:{$_GET['user_account']}:{$_GET['amount']}:{$_GET['abname']}:{$_GET['username']}:{$_GET['account']}:回传=>".json_encode($result), 0, '', json_encode($params));


		if (stristr($result, 'success') !== false) {
			$json = array(
				'code' => '0' //代付成功
			);		

		}
		else{
			$json = array(
				'code' => '-1', //代付失败
				'msg' => $result.'[天合宝]'
			);		
		}

		echo json_encode($json);
		exit;
		//die(json_encode($result));

	}
}
