<?php
include_once './cron.config.php';
class cg extends KJ
{
	private $key = '';
	private $url = '';
	private $iv = '';
    public $prefix = '';
    private $channelId = '';

	public function __construct() 
	{
        global $conf;
        parent::__construct();
        $this->prefix = $conf['prefix']."_";
        $this->key = $conf['vendor']['CG']['KEY'];
        $this->iv = $conf['vendor']['CG']['IV'];
        $this->url = $conf['vendor']['CG']['API_URL'];
        $this->channelId = $conf['vendor']['CG']['channelId'];
    }
    function aes256CbcDecrypt($data)
    {
        $raw_key = base64_decode($this->key);
        $raw_iv = base64_decode($this->iv);
        $raw_data = base64_decode($data);

        if (32 !== strlen($raw_key)) {
            return false;
        }
        if (16 !== strlen($raw_iv)) {
            return false;
        }        
        //	PHP 7 以後的版本用 openssl_encrypt
        $data = openssl_decrypt($raw_data, "AES-256-CBC", $raw_key, 0, $raw_iv);
        return $data;
    }

    function aes256CbcEncrypt($data)
    {
        $raw_key = base64_decode($this->key);
        $raw_iv = base64_decode($this->iv);
        $raw_data = base64_decode($data);
    
        if (32 !== strlen($raw_key)) {
            return false;
        }
        if (16 !== strlen($raw_iv)) {
            return false;
        }    
        return openssl_encrypt($raw_data, "AES-256-CBC", $raw_key, 0, $raw_iv);
    }
	public function run() 
	{
        file_put_contents('cg_callback.log',file_get_contents("php://input",'r')."\n", FILE_APPEND | LOCK_EX);
		$version=$_REQUEST['version'];
		$channelId=$_REQUEST['channelId'];
		$data=$_REQUEST['data'];
        $result = json_decode($this->aes256CbcDecrypt(base64_encode($data)),true);
        if(empty($version) || empty($channelId) || empty($data))
        {
            return;
        }
        $sql="select username from `lottery_members` where username='".$result["token"]."' and enable=1";
        $u=$this->getRow($sql);
        if($u)
        {
            //{"channelId":"66316","accountId":"UserID000001","nickName":"nick Name","errorCode":0} 
            $data=[
                "channelId" => $this->channelId,
                "accountId" => $u['username'],
                "nickName" => $u['username'],
                "errorCode" => 0
            ];
            //$data=$this->aes256CbcEncrypt(base64_encode(json_encode($data)));
            echo $this->aes256CbcEncrypt(base64_encode(json_encode($data)));
            return;
        }
        $data=[
            "channelId" => $this->channelId,
            "accountId" => $u['username'],
            "nickName" => $u['username'],
            "errorCode" => 12
        ];
        echo $this->aes256CbcEncrypt(base64_encode(json_encode($data)));
        return;
	}
}

$VendorCallback = new cg();
$VendorCallback->run();
?>

