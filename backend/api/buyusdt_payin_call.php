<?php
include_once './cron.config.php';
set_time_limit(0);
class Paycallback extends KJ
{
	public function __construct() 
	{
		parent::__construct();
	}
	public function run() 
	{
        file_put_contents('buyusdt_callback.log',file_get_contents("php://input",'r')."\n", FILE_APPEND | LOCK_EX);
		$ORDER_ID=$_REQUEST['ORDER_ID'];
		$ORDER_AMOUNT=$_REQUEST['ORDER_AMOUNT'];
		$STATUS=$_REQUEST['STATUS'];
		$CHECK_CODE=$_REQUEST['CHECK_CODE'];
		//CHECK_CODE 的計算方式為 MD5(Salt + "$" + ORDER_ID + "$" + ORDER_AMOUNT + "$" + STATUS)
        
        $sql="select id,merid,addcrykey from `lottery_params_thirdpay` where id=6";
        $third=$this->getRow($sql);
        if(!$third)
        {
            echo "無此渠道"."<br/>";
            return;
        }
        $sign = strtolower($this->strToHex(md5($third['addcrykey']."$".$ORDER_ID."$".$ORDER_AMOUNT."$".$STATUS)));
        if($CHECK_CODE != $sign)
        {
            echo "驗證錯誤"."<br/>";
            file_put_contents('buyusdt_callback.log','result=do not run '."\n", FILE_APPEND | LOCK_EX);
            return;
        }
        if($STATUS=="1")
        {
			$sql = "SELECT uid,amount FROM lottery_member_recharge where rechargeId=".$ORDER_ID." and state=0 ";
			if($member=$this->getRow($sql))
			{
				$this->beginTransaction();
				try
				{
					$uid_ok=$member['uid'];
					$amount_ok=$member['amount'];
					$sql = "update lottery_member_recharge set rechargeTime=".time().",state=1 where rechargeId=".$ORDER_ID;
					$this->update($sql);
					$this->addCoin(
					array(
						'coin'=>$amount_ok,
						'uid'=>$uid_ok,
						'liqType'=>1,
						'info'=>'充值'
						)
					);
					$this->commit();
				} catch (Exception $e) 
				{
					$this->rollBack();
					throw $e;
				}
			}
        }
        if($STATUS=="2")
        {
            $sql = "update lottery_member_recharge where rechargeId=".$ORDER_ID." and state=2 ";
            $this->update($sql);
        }
	}
	public function addCoin($log)
    {
       // if (!isset($log['uid'])) $log['info'] = $this->user['uid'];
        if (!isset($log['info'])) $log['info'] = '';
        if (!isset($log['coin'])) $log['coin'] = 0;
        if (!isset($log['type'])) $log['type'] = 0;
        if (!isset($log['fcoin'])) $log['fcoin'] = 0;
        if (!isset($log['extfield0'])) $log['extfield0'] = 0;
        if (!isset($log['extfield1'])) $log['extfield1'] = '';
        if (!isset($log['extfield2'])) $log['extfield2'] = '';
        $sql = "call setCoin({$log['coin']}, {$log['fcoin']}, {$log['uid']}, {$log['liqType']}, {$log['type']}, '{$log['info']}', {$log['extfield0']}, '{$log['extfield1']}', '{$log['extfield2']}')";
        $this->insert($sql);
    }
}

$Paycallback = new Paycallback();
$Paycallback->run();
?>

