<?php
//計算冷熱遺漏 by robert
class analysis {

    public function ssc($kjData,$get_data)
    {
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $get_data=array_merge($get_data,$kj);
        $lost=[];
        $hot=[];
        $temp=[];
        $pos_total=5;
        for($j=0;$j<10;$j++)
        {
            $temp[$j]=0;
        }
        for($i=1;$i<=$pos_total;$i++)
        {
            $lost[$i]=$temp;
            $hot[$i]=$temp;
        }
        foreach($get_data as $k=>$v)
        {
            $knum=explode(',',$v['data']);
            for($pos=0;$pos<$pos_total;$pos++)
            {
                foreach($lost[$pos+1] as $k1=>$v1)
                {
                    if($knum[$pos]!=$k1)
                    {
                        $lost[$pos+1][$k1]++;
                    }
                    if($knum[$pos]==$k1)
                    {
                        $hot[$pos+1][$k1]++;
                    }
                }
            }
        }
        return [
            'lost' => $lost,
            'hot' => $hot,
        ];
    }

    public function lhc($kjData,$get_data)
    {
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $get_data=array_merge($get_data,$kj);
        $lost=[];
        $hot=[];
        $sp_lost=[];
        $sp_hot=[];
        $temp=[];
        //$pos_total=6; //計算1-6球
        $pos_total=1; //只計算正碼
        for($j=1;$j<50;$j++)
        {
            $temp[$j]=0;
        }
        for($i=1;$i<=$pos_total;$i++)
        {
            $lost[$i]=$temp;
            $hot[$i]=$temp;
        }
        $sp_lost[7]=$temp;
        $sp_hot[7]=$temp;   
        //print_r($lost); 
        //exit;
        foreach($get_data as $k=>$v)
        {
            $knum=explode(',',$v['data']);
            // //計算正碼  計算1-6球 begin
            // for($pos=0;$pos<$pos_total;$pos++)
            // {
            //     foreach($lost[$pos+1] as $k1=>$v1)
            //     {
            //         if($knum[$pos]!=$k1)
            //         {
            //             $lost[$pos+1][$k1]++;
            //         }
            //         if($knum[$pos]==$k1)
            //         {
            //             $hot[$pos+1][$k1]++;
            //         }
            //     }
            // }
            // //計算正碼  計算1-6球 end
            //計算正碼  begin

            foreach($lost[1] as $k1=>$v1)
            {
                if (in_array($k1, $knum))
                {
                    $hot[1][$k1]++;
                }else
                {
                    $lost[1][$k1]++;
                }
            }
            //計算正碼  end
            //計算特碼 begin
            for($pos=6;$pos<7;$pos++)
            {
                foreach($sp_lost[$pos+1] as $k1=>$v1)
                {
                    if($knum[$pos]!=$k1)
                    {
                        $sp_lost[$pos+1][$k1]++;
                    }
                    if($knum[$pos]==$k1)
                    {
                        $sp_hot[$pos+1][$k1]++;
                    }
                }
            }
            //計算特碼 end
        }
        return [
            'lost' => $lost,
            'hot' => $hot,
            'sp_lost' => $sp_lost,
            'sp_hot' => $sp_hot,
        ];
    }

    public function p3($kjData,$get_data)
    {
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $get_data=array_merge($get_data,$kj);
        $lost=[];
        $hot=[];
        $temp=[];
        $pos_total=3;
        for($j=0;$j<10;$j++)
        {
            $temp[$j]=0;
        }
        for($i=1;$i<=$pos_total;$i++)
        {
            $lost[$i]=$temp;
            $hot[$i]=$temp;
        }
        foreach($get_data as $k=>$v)
        {
            $knum=explode(',',$v['data']);
            for($pos=0;$pos<$pos_total;$pos++)
            {
                foreach($lost[$pos+1] as $k1=>$v1)
                {
                    if($knum[$pos]!=$k1)
                    {
                        $lost[$pos+1][$k1]++;
                    }
                    if($knum[$pos]==$k1)
                    {
                        $hot[$pos+1][$k1]++;
                    }
                }
            }
        }
        return [
            'lost' => $lost,
            'hot' => $hot,
        ];
    }

    public function kl10($kjData,$get_data)
    {
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $get_data=array_merge($get_data,$kj);
        $lost=[];
        $hot=[];
        $temp=[];
        $pos_total=8;
        for($j=1;$j<21;$j++)
        {
            $temp[$j]=0;
        }
        for($i=1;$i<=$pos_total;$i++)
        {
            $lost[$i]=$temp;
            $hot[$i]=$temp;
        }
        foreach($get_data as $k=>$v)
        {
            
            $knum=explode(',',$v['data']);
            for($pos=0;$pos<$pos_total;$pos++)
            {
                foreach($lost[$pos+1] as $k1=>$v1)
                {
                    if($knum[$pos]!=$k1)
                    {
                        $lost[$pos+1][$k1]++;
                    }
                    if($knum[$pos]==$k1)
                    {
                        $hot[$pos+1][$k1]++;
                    }
                }
            }
        }
        return [
            'lost' => $lost,
            'hot' => $hot,
        ];
    }

    public function pk10($kjData,$get_data)
    {
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $get_data=array_merge($get_data,$kj);
        $lost=[];
        $hot=[];
        $temp=[];
        $pos_total=10;
        for($j=1;$j<11;$j++)
        {
            $temp[$j]=0;
        }
        for($i=1;$i<=$pos_total;$i++)
        {
            $lost[$i]=$temp;
            $hot[$i]=$temp;
        }
        foreach($get_data as $k=>$v)
        {
            
            $knum=explode(',',$v['data']);
            for($pos=0;$pos<$pos_total;$pos++)
            {
                foreach($lost[$pos+1] as $k1=>$v1)
                {
                    if($knum[$pos]!=$k1)
                    {
                        $lost[$pos+1][$k1]++;
                    }
                    if($knum[$pos]==$k1)
                    {
                        $hot[$pos+1][$k1]++;
                    }
                }
            }
        }
        return [
            'lost' => $lost,
            'hot' => $hot,
        ];
    }

    public function k8($kjData,$get_data)
    {
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $get_data=array_merge($get_data,$kj);
        $lost=[];
        $hot=[];
        $temp=[];
        $pos_total=20;
        for($j=1;$j<81;$j++)
        {
            $temp[$j]=0;
        }
        for($i=1;$i<=$pos_total;$i++)
        {
            $lost[$i]=$temp;
            $hot[$i]=$temp;
        }
        foreach($get_data as $k=>$v)
        {
            
            $knum=explode(',',$v['data']);
            for($pos=0;$pos<$pos_total;$pos++)
            {
                foreach($lost[$pos+1] as $k1=>$v1)
                {
                    if($knum[$pos]!=$k1)
                    {
                        $lost[$pos+1][$k1]++;
                    }
                    if($knum[$pos]==$k1)
                    {
                        $hot[$pos+1][$k1]++;
                    }
                }
            }
        }
        return [
            'lost' => $lost,
            'hot' => $hot,
        ];
    }

    public function k3($kjData,$get_data)
    {
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $get_data=array_merge($get_data,$kj);
        $lost=[];
        $hot=[];
        $temp=[];
        $pos_total=3;
        for($j=1;$j<7;$j++)
        {
            $temp[$j]=0;
        }
        for($i=1;$i<=$pos_total;$i++)
        {
            $lost[$i]=$temp;
            $hot[$i]=$temp;
        }
        foreach($get_data as $k=>$v)
        {
            
            $knum=explode(',',$v['data']);
            for($pos=0;$pos<$pos_total;$pos++)
            {
                foreach($lost[$pos+1] as $k1=>$v1)
                {
                    if($knum[$pos]!=$k1)
                    {
                        $lost[$pos+1][$k1]++;
                    }
                    if($knum[$pos]==$k1)
                    {
                        $hot[$pos+1][$k1]++;
                    }
                }
            }
        }
        return [
            'lost' => $lost,
            'hot' => $hot,
        ];
    }

    public function lucky28($kjData,$get_data)
    {
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $get_data=array_merge($get_data,$kj);
        $kj=[];
        foreach($get_data as $k=>$v)
        {
            $knum=explode(',',$v['data']);
            $number=intval($knum[0])+intval($knum[1])+intval($knum[2]);
            $kj[]=[
                'data'=>$number,
            ];
        }
        $get_data=$kj;
        $lost=[];
        $hot=[];
        $temp=[];
        $pos_total=1;
        for($j=0;$j<28;$j++)
        {
            $temp[$j]=0;
        }
        for($i=1;$i<=$pos_total;$i++)
        {
            $lost[$i]=$temp;
            $hot[$i]=$temp;
        }
        foreach($get_data as $k=>$v)
        {
            
            $knum=explode(',',$v['data']);
            for($pos=0;$pos<$pos_total;$pos++)
            {
                foreach($lost[$pos+1] as $k1=>$v1)
                {
                    if($knum[$pos]!=$k1)
                    {
                        $lost[$pos+1][$k1]++;
                    }
                    if($knum[$pos]==$k1)
                    {
                        $hot[$pos+1][$k1]++;
                    }
                }
            }
        }
        return [
            'lost' => $lost,
            'hot' => $hot,
        ];
    }
    
    public function x11x5($kjData,$get_data)
    {
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $get_data=array_merge($get_data,$kj);
        $lost=[];
        $hot=[];
        $temp=[];
        $pos_total=5;
        for($j=1;$j<12;$j++)
        {
            $temp[$j]=0;
        }
        for($i=1;$i<=$pos_total;$i++)
        {
            $lost[$i]=$temp;
            $hot[$i]=$temp;
        }
        foreach($get_data as $k=>$v)
        {
            $knum=explode(',',$v['data']);
            for($pos=0;$pos<$pos_total;$pos++)
            {
                foreach($lost[$pos+1] as $k1=>$v1)
                {
                    if($knum[$pos]!=$k1)
                    {
                        $lost[$pos+1][$k1]++;
                    }
                    if($knum[$pos]==$k1)
                    {
                        $hot[$pos+1][$k1]++;
                    }
                }
            }
        }
        return [
            'lost' => $lost,
            'hot' => $hot,
        ];
    }

    public function clong_count(&$check_cl,&$check_cl2,&$status,&$clong,$key1,$key2,$key3=null)
    {
        if($check_cl2==$check_cl)
        {
            if($status['status']==false)
            {
                $status['status']=true;
                $status['count']=2;
            }else
            {
                $status['count']++;
            }
        }else
        {
            $status['status']=false;
            if($check_cl2==$key1[1])
            {
                if($clong[$key1[0]]<$status['count'])
                {
                    $clong[$key1[0]]=$status['count'];
                }
            }
            if($check_cl2==$key2[1])
            {
                if($clong[$key2[0]]<$status['count'])
                {
                    $clong[$key2[0]]=$status['count'];
                }
            }
            if(isset($key3))
            {
                if($check_cl2==$key3[1])
                {
                    if($clong[$key3[0]]<$status['count'])
                    {
                        $clong[$key3[0]]=$status['count'];
                    }
                }    
            }
            $status['count']=0;
        } 
    }

    public function clong_finish(&$check_cl,&$check_cl2,&$status,&$clong,$key1,$key2,$key3=null)
    {
        if($check_cl2==$key1[1])
        {
            if($clong[$key1[0]]<$status['count'])
            {
                $clong[$key1[0]]=$status['count'];
            }
        }
        if($check_cl2==$key2[1])
        {
            if($clong[$key2[0]]<$status['count'])
            {
                $clong[$key2[0]]=$status['count'];
            }
        }
        if(isset($key3))
        {
            if($check_cl2==$key3[1])
            {
                if($clong[$key3[0]]<$status['count'])
                {
                    $clong[$key3[0]]=$status['count'];
                }
            }    
        }
    }

	public function clong($kjData,$get_data,$type,$balls){
        $kj=[
            [
                'data'=>$kjData,
            ]
        ];
        $data=array_merge($get_data,$kj);
        $clong=[];
        $check_cl=[];
        $check_cl2=[];
        //$balls=5;
        $reset=[
            'status'=>false,
            'count'=>0,
        ];
        for($i=0;$i<$balls;$i++)
        {
            $status[$i]['ds']=$reset;
            $status[$i]['dx']=$reset;
            $clong['ball_'.$i.'_big']=0;
            $clong['ball_'.$i.'_small']=0;
            $clong['ball_'.$i.'_double']=0;
            $clong['ball_'.$i.'_single']=0;
        }
        $clong['zh_double']=0;
        $clong['zh_single']=0;
        $clong['zh_big']=0;
        $clong['zh_small']=0;
        $clong['zh_l']=0;
        $clong['zh_h']=0;
        $clong['zh_hh']=0;
        $clong['zh_ws_big']=0;
        $clong['zh_ws_small']=0;
        $clong['gy_double']=0;
        $clong['gy_single']=0;
        $clong['gy_big']=0;
        $clong['gy_small']=0;
        $status['zhds']=$reset;
        $status['zhdx']=$reset;
        $status['gyds']=$reset;
        $status['gydx']=$reset;
        $status['lhh']=$reset;
        $status['zhwsdx']=$reset;
        $lib_clong=new lib_clong();
        foreach($data as $k=>$v)
        {
            switch(true)
            {
                case $type==1:
                    $check_cl=$lib_clong->ssc($v['data']);
                break;
                case $type==2:
                    $check_cl=$lib_clong->x11x5($v['data']);
                break;
                case $type==3:
                    $check_cl=$lib_clong->p3($v['data']);
                break;
                case $type==4:
                    $check_cl=$lib_clong->kl10($v['data']);
                break;
                case $type==6:
                    $check_cl=$lib_clong->pk10($v['data']);
                break;
                case $type==9:
                    $check_cl=$lib_clong->k3($v['data']);
                break;
                case $type==11:
                    $check_cl=$lib_clong->lucky28($v['data']);
                break;

            }
            if(!$check_cl2)
            {
                $check_cl2=$check_cl;
                continue;
            }
            for($i=0;$i<$balls;$i++)
            //for($i=0;$i<1;$i++)
            {
                if(isset($check_cl[$i]['ds']))
                {
                    $this->clong_count($check_cl[$i]['ds'],$check_cl2[$i]['ds'],$status[$i]['ds'],$clong,['ball_'.$i.'_big','大'],['ball_'.$i.'_small','小']);
                }
                if(isset($check_cl[$i]['dx']))
                {
                    $this->clong_count($check_cl[$i]['dx'],$check_cl2[$i]['dx'],$status[$i]['dx'],$clong,['ball_'.$i.'_single','单'],['ball_'.$i.'_double','双']);    
                }
            }
            
            if(isset($check_cl['zhds']))
            {
                $this->clong_count($check_cl['zhds'],$check_cl2['zhds'],$status['zhds'],$clong,['zh_big','大'],['zh_small','小']);
                // if($check_cl2['zhds']==$check_cl['zhds'])
                // {
                //     if($status['zhds']['status']==false)
                //     {
                //         $status['zhds']['status']=true;
                //         $status['zhds']['count']=2;
                //     }else
                //     {
                //         $status['zhds']['count']++;
                //     }
                // }else
                // {
                //     $status['zhds']['status']=false;
                //     if($check_cl2['zhds']=='大')
                //     {
                //         if($clong['zh_big']<$status['zhds']['count'])
                //         {
                //             $clong['zh_big']=$status['zhds']['count'];
                //         }
                //     }
                //     if($check_cl2['zhds']=='小')
                //     {
                //         if($clong['zh_small']<$status['zhds']['count'])
                //         {
                //             $clong['zh_small']=$status['zhds']['count'];
                //         }
                //     }
                //     $status['zhds']['count']=0;
                // }    
            }
            if(isset($check_cl['zhdx']))
            {
                $this->clong_count($check_cl['zhdx'],$check_cl2['zhdx'],$status['zhdx'],$clong,['zh_single','单'],['zh_double','双']); 
            }
            if(isset($check_cl['zhwsdx']))
            {
                $this->clong_count($check_cl['zhwsdx'],$check_cl2['zhwsdx'],$status['zhwsdx'],$clong,['zh_ws_big','大'],['zh_ws_small','小']); 
            }

            if(isset($check_cl['lhh']))
            {
                $this->clong_count($check_cl['lhh'],$check_cl2['lhh'],$status['lhh'],$clong,['zh_l','龙'],['zh_h','虎'],['zh_hh','和']);
            }

            if(isset($check_cl['gyds']))
            {
                $this->clong_count($check_cl['gyds'],$check_cl2['gyds'],$status['gyds'],$clong,['gy_big','大'],['gy_small','小']); 
            }

            if(isset($check_cl['gydx']))
            {
                $this->clong_count($check_cl['gydx'],$check_cl2['gydx'],$status['gydx'],$clong,['gy_single','单'],['gy_double','双']); 
            }
            $check_cl2=$check_cl;
        }
        for($i=0;$i<$balls;$i++)
        //for($i=0;$i<1;$i++)
        {
            if($status[$i]['ds']['status']=true)
            {
                $this->clong_finish($check_cl[$i]['ds'],$check_cl2[$i]['ds'],$status[$i]['ds'],$clong,['ball_'.$i.'_big','大'],['ball_'.$i.'_small','小']);
            }
            if($status[$i]['dx']['status']=true)
            {
                $this->clong_finish($check_cl[$i]['dx'],$check_cl2[$i]['dx'],$status[$i]['dx'],$clong,['ball_'.$i.'_single','单'],['ball_'.$i.'_double','双']);    
            }
        }

        if(isset($check_cl['gydx']))
        {
            if($status['gydx']['status']=true)
            {
                $this->clong_finish($check_cl['gydx'],$check_cl2['gydx'],$status['gydx'],$clong,['gy_single','单'],['gy_double','双']); 
            }    
        }

        if(isset($check_cl['gyds']))
        {
            if($status['gyds']['status']=true)
            {
                $this->clong_finish($check_cl['gyds'],$check_cl2['gyds'],$status['gyds'],$clong,['gy_big','大'],['gy_small','小']); 
            }    
        }
        if(isset($check_cl['zhds']))
        {
            $this->clong_finish($check_cl['zhds'],$check_cl2['zhds'],$status['zhds'],$clong,['zh_big','大'],['zh_small','小']);
        }
        if(isset($check_cl['zhdx']))
        {
            if($status['zhdx']['status']=true)
            {
                $this->clong_finish($check_cl['zhdx'],$check_cl2['zhdx'],$status['zhdx'],$clong,['zh_single','单'],['zh_double','双']); 
            }    
        }
        if(isset($check_cl['zhwsdx']))
        {
            if($status['zhwsdx']['status']=true)
            {
                $this->clong_finish($check_cl['zhwsdx'],$check_cl2['zhwsdx'],$status['zhwsdx'],$clong,['zh_ws_big','大'],['zh_ws_small','小']); 
            }
        }
        if(isset($check_cl['lhh']))
        {
            if($status['lhh']['status']=true)
            {
                $this->clong_finish($check_cl['lhh'],$check_cl2['lhh'],$status['lhh'],$clong,['zh_l','龙'],['zh_h','虎'],['zh_hh','和']);
            }    
        }
        //arsort($clong);
        return $clong;
    }

}

class lib_clong {
    public function lucky28($data){
        $datas=explode(',', $data);
        $total=0;
        $ssc=[];
        $total=intval($datas[0])+intval($datas[1])+intval($datas[2]);
        if($total>=14 && $total<=27 ){
            $ssc['zhds']='大';
        }elseif($total<=13){
            $ssc['zhds']='小';
        }
        if($total%2==0){
            $ssc['zhdx']='双';
        }else{
            $ssc['zhdx']='单';
        }
        return $ssc;
    }
	public function k3($data){
        $datas=explode(',', $data);
        $val=0;
        $total=0;
        $ssc=[];
        foreach($datas as $k=>$v)
        {
            $val=intval($v);
            $total+=$val;
        }
        $total=intval($datas[0])+intval($datas[1]);
        if($total>=11 && $total<=17 ){
            $ssc['zhds']='大';
        }elseif($total>=4 && $total<=10){
            $ssc['zhds']='小';
        }
        if($total%2==0){
            $ssc['zhdx']='双';
        }else{
            $ssc['zhdx']='单';
        }
        if($datas[0]==$datas[1] && $datas[1]==$datas[2])
        {
            $ssc['zhds']='豹子';
            $ssc['zhdx']='豹子';
        }
        return $ssc;
    }
        
	public function ssc($data){
        $datas=explode(',', $data);
        $val=0;
        $total=0;
        $ssc=[];
        foreach($datas as $k=>$v)
        {
            $val=intval($v);
            $total+=$val;
            if($val>=5 && $val<=9){
                $ssc[$k]['ds']='大';
            }elseif($val>=0 && $val<=4){
                $ssc[$k]['ds']='小';
            }

            if($val%2!=0){
                $ssc[$k]['dx']='单';
            }elseif($val%2==0){
                $ssc[$k]['dx']='双';
            }
        }
        if($total>=0 && $total<=23){
            $ssc['zhds']='小';
        }elseif($total>=22){
            $ssc['zhds']='大';
        }
        if($total%2==0){
            $ssc['zhdx']='双';
        }else{
            $ssc['zhdx']='单';
        }
        $val1=intval($datas[0]);
        $val5=intval($datas[4]);
        switch(true)
        {
            case $val1>$val5:
                $ssc['lhh']='龙';
            break;
            case $val1<$val5:
                $ssc['lhh']='虎';
            break;
            case $val1==$val5:
                $ssc['lhh']='和';
            break;
        }
        //$ssc['num']=$data;
        return $ssc;
    }

	public function p3($data){
        $datas=explode(',', $data);
        $val=0;
        $total=0;
        $ssc=[];
        foreach($datas as $k=>$v)
        {
            $val=intval($v);
            $total+=$val;
            if($val>=5 && $val<=9){
                $ssc[$k]['ds']='大';
            }elseif($val>=0 && $val<=4){
                $ssc[$k]['ds']='小';
            }

            if($val%2!=0){
                $ssc[$k]['dx']='单';
            }elseif($val%2==0){
                $ssc[$k]['dx']='双';
            }
        }
        if($total>=0 && $total<=13){
            $ssc['zhds']='小';
        }elseif($total>=14){
            $ssc['zhds']='大';
        }
        if($total%2==0){
            $ssc['zhdx']='双';
        }else{
            $ssc['zhdx']='单';
        }
        $val1=intval($datas[0]);
        $val5=intval($datas[2]);
        switch(true)
        {
            case $val1>$val5:
                $ssc['lhh']='龙';
            break;
            case $val1<$val5:
                $ssc['lhh']='虎';
            break;
            case $val1==$val5:
                $ssc['lhh']='和';
            break;
        }
        return $ssc;
    }

	public function x11x5($data){
        $datas=explode(',', $data);
        $val=0;
        $total=0;
        $ssc=[];
        foreach($datas as $k=>$v)
        {
            $val=intval($v);
            $total+=$val;
            if($val>=6 && $val<11){
                $ssc[$k]['ds']='大';
            }elseif($val>0 && $val<=5){
                $ssc[$k]['ds']='小';
            }elseif($val==11){
                $ssc[$k]['ds']='和';
            }
            if($val%2!=0 && $val != 11){
                $ssc[$k]['dx']='单';
            }elseif($val%2==0 && $val != 11){
                $ssc[$k]['dx']='双';
            }elseif($val== 11){
                $ssc[$k]['dx']='和';
            }
        }
        if($total>30){
            $ssc['zhds']='大';
        }elseif($total<30){
            $ssc['zhds']='小';
        }elseif($total==30){
            $ssc['zhds']='和';
        }
        if($total%2==0){
            $ssc['zhdx']='双';
        }else{
            $ssc['zhdx']='单';
        }
        if($total%10>4){
            $ssc['zhwsdx']='大';
        }else{
            $ssc['zhwsdx']='小';
        }
        $val1=intval($datas[0]);
        $val5=intval($datas[4]);
        switch(true)
        {
            case $val1>$val5:
                $ssc['lhh']='龙';
            break;
            case $val1<$val5:
                $ssc['lhh']='虎';
            break;
        }
        return $ssc;
    }
    
	public function kl10($data){
        $datas=explode(',', $data);
        $val=0;
        $total=0;
        $ssc=[];
        foreach($datas as $k=>$v)
        {
            $val=intval($v);
            $total+=$val;
            if($val>=11 && $val<21){
                $ssc[$k]['ds']='大';
            }elseif($val>0 && $val<=10){
                $ssc[$k]['ds']='小';
            }
            if($val%2!=0 ){
                $ssc[$k]['dx']='单';
            }elseif($val%2==0 ){
                $ssc[$k]['dx']='双';
            }
        }
        if($total>35 && $total<84 ){
            $ssc['zhds']='小';
        }elseif($total>84 && $total<133){
            $ssc['zhds']='大';
        }elseif($total==84){
            $ssc['zhds']='和';
        }
        if($total%2==0){
            $ssc['zhdx']='双';
        }else{
            $ssc['zhdx']='单';
        }
        if($total%10>4){
            $ssc['zhwsdx']='大';
        }else{
            $ssc['zhwsdx']='小';
        }
        // $val1=intval($datas[0]);
        // $val5=intval($datas[4]);
        // switch(true)
        // {
        //     case $val1>$val5:
        //         $ssc['lhh']='龙';
        //     break;
        //     case $val1<$val5:
        //         $ssc['lhh']='虎';
        //     break;
        // }
        return $ssc;
    }
    
	public function pk10($data){
        $datas=explode(',', $data);
        $val=0;
        $total=0;
        $ssc=[];
        foreach($datas as $k=>$v)
        {
            $val=intval($v);
            //$total+=$val;
            if($val>=6 && $val<11){
                $ssc[$k]['ds']='大';
            }elseif($val>0 && $val<=5){
                $ssc[$k]['ds']='小';
            }
            if($val%2!=0 ){
                $ssc[$k]['dx']='单';
            }elseif($val%2==0 ){
                $ssc[$k]['dx']='双';
            }
        }
        $total=intval($datas[0])+intval($datas[1]);
        if($total>11 && $total<20 ){
            $ssc['gyds']='大';
        }elseif($total>2 && $total<12){
            $ssc['gyds']='小';
        }
        if($total%2==0){
            $ssc['gydx']='双';
        }else{
            $ssc['gydx']='单';
        }
        // if($total%10>4){
        //     $ssc['zhwsdx']='大';
        // }else{
        //     $ssc['zhwsdx']='小';
        // }
        // $val1=intval($datas[0]);
        // $val5=intval($datas[4]);
        // switch(true)
        // {
        //     case $val1>$val5:
        //         $ssc['lhh']='龙';
        //     break;
        //     case $val1<$val5:
        //         $ssc['lhh']='虎';
        //     break;
        // }
        return $ssc;
	}
}
?>