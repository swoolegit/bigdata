<?php
require __dir__.'/../lib/DBAccess.lib.php';
require __dir__.'/../lib/db.lib.php';
require __dir__.'/../config.php';
require __dir__.'/../vendor_config.php';

define('DB_DSN',$conf['db']['dsn']);
define('DB_USER',$conf['db']['user']);
define('DB_PASSWORD',$conf['db']['password']);
define('DB_PRENAME',$conf['db']['prename']);

class KJ extends DBAccess
{
	public $settings = array();
	public function __construct()
	{
		parent::__construct(DB_DSN, DB_USER, DB_PASSWORD);
		$this->prename = DB_PRENAME;
	}

	public function getParams($name = '') {
		$sql = "SELECT * FROM `{$this->prename}params` WHERE `name`='{$name}'";
		$data = $this->getRow($sql);
		return $data;
	}
	public function getSettings(){
		if ($this->settings) return $this->settings;
		$sql = "SELECT * FROM `{$this->prename}params` ";
		$data = $this->getRows($sql);
		foreach ($data as $rows){
			$this->settings[$rows['name']] = $rows['value'];		
		}
		return $this->settings;
	}

}

require __dir__.'/../lib/Game.lib.php';
