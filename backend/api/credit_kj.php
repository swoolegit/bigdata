<?php 
class credit_kj {
	private $LHC = [
		'zodiacArr' => [ "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪" ]
	];
	private $SX_NUMS = [
		"1" => [ 1, 13, 25, 37, 49 ],
		"2" => [ 2, 14, 26, 38 ],
		"3" => [ 3, 15, 27, 39 ],
		"4" => [ 4, 16, 28, 40 ],
		"5" => [ 5, 17, 29, 41 ],
		"6" => [ 6, 18, 30, 42 ],
		"7" => [ 7, 19, 31, 43 ],
		"8" => [ 8, 20, 32, 44 ],
		"9" => [ 9, 21, 33, 45 ],
		"10" => [ 10, 22, 34, 46 ],
		"11" => [ 11, 23, 35, 47 ],
		"12" => [ 12, 24, 36, 48 ]
	];

	// public $ZODIAC = array('鼠'=>'11,23,35,47'
	// 						,'牛'=>'10,22,34,46'
	// 						,'虎'=>'09,21,33,45'
	// 						,'兔'=>'08,20,32,44'
	// 						,'龍'=>'07,19,31,43'
	// 						,'蛇'=>'06,18,30,42'
	// 						,'馬'=>'05,17,29,41'
	// 						,'羊'=>'04,16,28,40'
	// 						,'猴'=>'03,15,27,39'
	// 						,'雞'=>'02,14,26,38'
	// 						,'狗'=>'01,13,25,37,49'
	// 						,'豬'=>'12,24,36,48');
	
	public $SX_TIAN = array('兔', '马', '猴', '猪', '牛', '龙');
	public $SX_DI = array('蛇', '羊', '鸡', '狗', '鼠', '虎');
	public $SX_QIAN = array('鼠', '牛', '虎', '兔', '龙', '蛇');
	public $SX_HOU = array('马', '羊', '猴', '鸡', '狗', '猪');
	public $SX_JIA = array('羊', '马', '牛', '猪', '狗', '鸡');
	public $SX_YE = array('猴', '蛇', '龙', '兔', '虎', '鼠');
	
	public $HONG_NUMS = array(1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46);
	public $LANG_NUMS = array(3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48);
	public $LV_NUMS = array(5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49);
/*
	public $JIN_NUMS = array(5,6,19,20,27,28,35,36,49);
	public $MU_NUMS = array(1,2,9,10,17,18,31,32,39,40,47,48);
	public $SHUI_NUMS = array(7,8,15,16,23,24,37,38,45,46);
	public $HUO_NUMS = array(3,4,11,12,25,26,33,34,41,42);
	public $TU_NUMS = array(13,14,21,22,29,30,43,44);
*/
public $JIN_NUMS = array(1,6,11,16,21,26,31,36,41,46);
public $MU_NUMS = array(2,7,12,17,22,27,32,37,42,47);
public $SHUI_NUMS = array(3,8,13,18,23,28,33,38,43,48);
public $HUO_NUMS = array(4,9,14,19,24,29,34,39,44,49);
public $TU_NUMS = array(5,10,15,20,25,30,35,40,45);	
	/**基本方法**/
	//判断是否是半顺
	public function isHalfStraight($nums) {
		sort($nums);
		$n = 0;
		if ($nums[0] == 0) {
			$n += 1;
		}
		if ($nums[1] == 1) {
			$n += 1;
		}
		if ($nums[2] == 9) {
			$n += 1;
		}
		if ($n == 2) {
			return true;
		}
		
		for($i=0 ; $i < count($nums) - 1 ; $i++) {
			$num1 = $nums[$i];
			
			$num2 = $nums[$i + 1];
			if($num2 - $num1 == 1) {
				return true;
			}
		}
	}
	//是否顺子
	public function isStraight($nums) {
		sort($nums);
		if ($nums[0] == 0 && $nums[1] == 1 && $nums[2] == 9) {// 019也算顺子
			return true;
		}
		if ($nums[0] == 0 && $nums[1] == 8 && $nums[2] == 9) {// 089也算顺子
			return true;
		}
		
		for($i=0 ; $i < count($nums) - 1 ; $i++) {
			$num1 = $nums[$i];
			$num2 = $nums[$i + 1];
			if($num2 - $num1 != 1) {
				return false;
			}
		}
		return true;
	}
	//是否对子
	public function isPair($nums) {
		for($i=0 ; $i < count($nums) ; $i++) {
			$code = $nums[$i];
			for($j= $i + 1 ; $j < count($nums) ; $j++) {
				$codeTemp = $nums[$j];
				if ($code == $codeTemp) {
					return true;
				}
			}
		}
		return false;
	}
	//是否有重复号
	public function isSameNum($nums) {
		$num1 = $nums[0];
		for($i=0 ; $i < count($nums) ; $i++) {
			if ($num1 != $nums[$i]) {
				return false;
			}
		}
		return true;
	}
	//第N球
	public function sscdnb($actionData, $openCode) {
		$count = 0;
		if(is_numeric($actionData)) {
			if($actionData == $openCode)$count = 1;
		} else {
			$val = '';
			if($openCode >= 5) 
				$val .= '大';
			else 
				$val .= '小';
			
			if($openCode % 2 == 1)
				$val .= "单";
			else
				$val .= "双";
			//print_r("val :".$val.",actionData:".$actionData);
			if(strrpos($val, $actionData) !== false) $count = 1;
		}
		return $count;
	}
	//前三/中三/后三共用方法
	public function sscr3($actionData, $openCode) {
		$count = 0;
		switch ($actionData) {
			case "豹子":
				if($this->isSameNum($openCode)) $count=1;
				break;
			case "对子":
				if($this->isPair($openCode) && !$this->isSameNum($openCode)) $count=1;
				break;
			case "顺子":
				if($this->isStraight($openCode) && 
					!$this->isSameNum($openCode) && !$this->isPair($openCode)) $count=1;
				break;
			case "半顺":
				if($this->isHalfStraight($openCode) && !$this->isStraight($openCode) 
					&& !$this->isSameNum($openCode) && !$this->isPair($openCode)) $count=1;
				break;
			case "杂六":
				if(count($openCode)==3 && !$this->isHalfStraight($openCode) && !$this->isStraight($openCode) 
					&& !$this->isSameNum($openCode) && !$this->isPair($openCode)) $count=1;
				break;
			default:
				break;
		}
		return $count;
	}
	/**时时彩**/
	// 总和大小单双
	public function ssczh($actionData, $openCode){
		$codes = explode(',', $openCode);
		$sum = array_sum($codes);
		$val = "";
		if($sum >= 23)
			$val .= "总和大";
		else
			$val .= "总和小";
		if($sum % 2 == 1)
			$val .= "总和单";
		else
			$val .= "总和双";
		
		$count = 0;
		$datas = explode(',', $actionData);
		foreach($datas as $data){
			if(strrpos($val, $data) !== false) $count++;
		}
		return $count;
	}
	//第一球
	public function sscd1b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[0]);
	}
	//第二球
	public function sscd2b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[1]);
	}
	//第三球
	public function sscd3b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[2]);
	}
	//第四球
	public function sscd4b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[3]);
	}
	//第五球
	public function sscd5b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[4]);
	}
	//前三
	public function sscq3($actionData, $openCode) {
		$codes = explode(',', $openCode);
		array_splice($codes,3,2);
		return $this->sscr3($actionData, $codes,3,2);
	}
	//中三
	public function sscz3($actionData, $openCode) {
		$codes = explode(',', $openCode);
		array_shift($codes);
		array_pop($codes);
		return $this->sscr3($actionData, $codes);
	}
	//后三
	public function ssch3($actionData, $openCode) {
		$codes = explode(',', $openCode);
		array_splice($codes,0,2);
		return $this->sscr3($actionData, $codes);
	}
	
	/** 龙虎和相关(default 1&5) **/
	public function lhh($actionData, $openCode, $first=0, $last=4){
		$codes = explode(',', $openCode);
		$count = 0;
		
		if($codes[$first] == $codes[$last]){
			if(strrpos($actionData, '和') !== false) $count = 1;
		} else if($codes[$first] > $codes[$last]) {
			if(strrpos($actionData, '龙') !== false) $count = 1;
		} else {
			if(strrpos($actionData, '虎') !== false) $count = 1;
		}
		return $count;
	}
	public function lhh12($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 0 ,1);
	}
	public function lhh13($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 0 ,2);
	}
	public function lhh14($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 0 ,3);
	}
	public function lhh16($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 0 ,5);
	}
	public function lhh17($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 0 ,6);
	}
	public function lhh18($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 0 ,7);
	}
	public function lhh23($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 1 ,2);
	}
	public function lhh24($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 1 ,3);
	}
	public function lhh25($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 1 ,4);
	}
	public function lhh26($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 1 ,5);
	}
	public function lhh27($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 1 ,6);
	}
	public function lhh28($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 1 ,7);
	}
	public function lhh34($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 2 ,3);
	}
	public function lhh35($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 2 ,4);
	}
	public function lhh36($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 2 ,5);
	}
	public function lhh37($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 2 ,6);
	}
	public function lhh38($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 2 ,7);
	}
	public function lhh45($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 3 ,4);
	}
	public function lhh46($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 3 ,5);
	}
	public function lhh47($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 3 ,6);
	}
	public function lhh48($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 3 ,7);
	}
	public function lhh56($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 4 ,5);
	}
	public function lhh57($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 4 ,6);
	}
	public function lhh58($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 4 ,7);
	}
	public function lhh67($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 5 ,6);
	}
	public function lhh68($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 5 ,7);
	}
	public function lhh78($actionData, $openCode){
		return $this->lhh($actionData, $openCode, 6 ,7);
	}
	
	/**11选5**/
	//总和大小单双尾大尾小
	public function s115sh($actionData, $openCode){
		$codes = explode(',', $openCode);
		$sum = array_sum($codes);
		$count = 0;
		$val = "";
		if($sum == 30){
			$count = -1;
			return $count;
		} else if($sum > 30){
			$val .= "总和大";
		} else {
			$val .= "总和小";
		}
		
		if($sum % 2 == 1)
			$val .= "总和单";
		else
			$val .= "总和双";
		
		if(intval(substr($sum,-1,1)) > 4)
			$val .= "总和尾大";
		else
			$val .= "总和尾小";

		
		if(strrpos($val, $actionData) !== false) $count++;
		return $count;
	}
	public function sames($a, $b){
		$count = 0;
		for($i = 0;$i < count($a);$i++)
		{   
			for($j = 0;$j < count($b);$j++)
			{
				if($a[$i] == $b[$j]){
					$count++;
					break;
				}
			}
		}
		return $count;
	}
	// 任選一中一
	public function gd11x5R1($actionData, $openCode){
		$datas = explode(',', $actionData);	//01 02 05 07 10 11
		$codes = explode(',', $openCode);
		return $this->sames($datas, $codes);
	}
	//第N球大小单双数字
	public function s115dxds($actionData, $code){
		$count = 0;
		//数字
		if(is_numeric($actionData)) {
			if($actionData == $code){
				$count=1;
				return $count;
			}
		}
		
		if($code != 11){
			$val = "";
			if($code >= 6)
				$val .= "大";
			else
				$val .= "小";
			
			if($code % 2 == 1)
				$val .= "单";
			else
				$val .= "双";
			if(strrpos($val, $actionData) !== false) $count++;
		} else {
			$count = -1;
		}
		return $count;
	}
	//第一球大小单双数字
	public function s115d1b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[0]);
	}
	//第二球大小单双数字
	public function s115d2b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[1]);
	}
	//第三球大小单双数字
	public function s115d3b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[2]);
	}
	//第四球大小单双数字
	public function s115d4b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[3]);
	}
	//第五球大小单双数字
	public function s115d5b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[4]);
	}
	
	/**快三**/
	//三军、大小单双
	public function k3sh($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$sum = array_sum($codes);
		$val = "";
		$count = 0;
		//三军
		if(is_numeric($actionData)) {
			if(strrpos($openCode, $actionData) !== false){
				$count=1;
				return $count;
			}
		}
		//大小单双(排除豹子)
		//if(count(array_unique($codes)) == 1) {
		//	return $count;
		//}

		if($sum >= 11 && $sum <= 18)
			$val .= "大";
		else
			$val .= "小";
		
		if($sum % 2 == 1)
			$val .= "单";
		else
			$val .= "双";
		if(strrpos($val, $actionData) !== false) $count++;
		return $count;
	}
	//围骰、全骰
	public function k3qsws($actionData, $openCode) {
		$count = 0 ;
		$codes = explode(',', $openCode);
		if($actionData =='全骰') {
			if(count(array_unique($codes)) == 1) $count = 1;
		} else {
			$codeAry = array_unique($codes);
			$betAry = array_unique(explode('_', $actionData));
			if(count(array_unique($codes)) == 1 && $codeAry[0] == $betAry[0])$count = 1;
		}
		return $count;
	}
	//和值点数
	public function k3hzds($actionData, $openCode) {
		$count = 0 ;
		$codes = explode(',', $openCode);
		//大小单双(排除豹子)
		//if(count(array_unique($codes)) == 1) {
		//	return $count;
		//}
		$sum = array_sum($codes);
		//if(strrpos($sum.'点', $actionData) !== false) $count++;
		if($sum.'点' === $actionData) $count++;
		return $count;
	}
	//长牌
	public function k3cp($actionData, $openCode) {
		$datas = explode('_',$actionData);
		$codes = explode(',', $openCode);
		$count = 0;
		$sameCnt = 0;
		foreach($datas as $data){
			$bln = false;
			foreach($codes as $code){
				if($data == $code) $bln = true;
			}
			if($bln) $sameCnt++;
		}
		if($sameCnt >= 2)$count = 1;
		return $count;
	}
	//短牌
	public function k3dp($actionData, $openCode) {
		$datas = explode('_',$actionData);
		$codes = explode(',', $openCode);
		$count = 0;
		$sameCnt = 0;
		foreach($codes as $code){
			if($datas[0] == $code){
				$sameCnt++;
			}
		}
		if($sameCnt >= 2)$count = 1;
		return $count;
	}
	
	/**北京PK10**/
	//冠亚军和
	public function pk10gy($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$count = 0;
		$sum = array_sum(array_splice($codes, 0 ,2));
		if(is_numeric($actionData)) {
			if($sum == $actionData)$count = 1;
			return $count;
		}

		$val = "";
		if($sum > 11 && $sum < 20)
			$val .= "冠亚大";
		else
			$val .= "冠亚小";

		if($sum % 2 == 1)
			$val .= "冠亚单";
		else
			$val .= "冠亚双";
		if(strrpos($val, $actionData) !== false) $count++;
		return $count;
	}
	//第N名
	public function pk10dn($actionData, $code) {
		$count = 0;
		if($actionData == $code )$count=1;
		return $count;
	}
	// 冠军
	public function pk10d1($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$openCode = $codes[0];
		return $this->pk10dn($actionData, $openCode);
	}
	// 亚军
	public function pk10d2($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$openCode = $codes[1];
		return $this->pk10dn($actionData, $openCode);
	}
	// 季军
	public function pk10d3($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$openCode = $codes[2];
		return $this->pk10dn($actionData, $openCode);
	}
	// 四名
	public function pk10d4($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$openCode = $codes[3];
		return $this->pk10dn($actionData, $openCode);
	}
	// 五名
	public function pk10d5($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$openCode = $codes[4];
		return $this->pk10dn($actionData, $openCode);
	}
	// 六名
	public function pk10d6($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$openCode = $codes[5];
		return $this->pk10dn($actionData, $openCode);
	}
	// 七名
	public function pk10d7($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$openCode = $codes[6];
		return $this->pk10dn($actionData, $openCode);
	}
	// 八名
	public function pk10d8($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$openCode = $codes[7];
		return $this->pk10dn($actionData, $openCode);
	}
	// 九名
	public function pk10d9($actionData, $openCode) {
		$codes = explode(',', $openCode); 
		$openCode = $codes[8];
		return $this->pk10dn($actionData, $openCode);
	}
	// 十名
	public function pk10d10($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$openCode = $codes[9];
		return $this->pk10dn($actionData, $openCode);
	}
	//大小单双龙虎
	// 冠军
	public function pk10dxdslh1($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[0], $codes[9]);
	}
	// 亚军
	public function pk10dxdslh2($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[1], $codes[8]);
	}
	// 季军
	public function pk10dxdslh3($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[2], $codes[7]);
	}
	// 四名
	public function pk10dxdslh4($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[3], $codes[6]);
	}
	// 五名
	public function pk10dxdslh5($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[4], $codes[5]);
	}
	// 六名
	public function pk10dxdslh6($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[5]);
	}
	// 七名
	public function pk10dxdslh7($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[6]);
	}
	// 八名
	public function pk10dxdslh8($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[7]);
	}
	// 九名
	public function pk10dxdslh9($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[8]);
	}
	// 十名
	public function pk10dxdslh10($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->dxds2($actionData, $codes[9]);
	}
	/**PC蛋蛋**/
	//综合
	public function pcddzh($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$sum = array_sum($codes);
		$count = 0;
		$ball = array(0,13,14,27);
		switch($actionData){
			case "大":
				if($sum >= 14) $count=1;
				break;
			case "小":
				if($sum <= 13) $count=1;
				break;
			case "单":
				if($sum % 2 == 1) $count=1;
				break;
			case "双":
				if($sum % 2 == 0) $count=1;
				break;
			case "大单":
				if($sum >= 14 && $sum % 2 == 1) $count=1;
				break;
			case "小单":
				if($sum <= 13 && $sum % 2 == 1) $count=1;
				break;
			case "大双":
				if($sum >= 14 && $sum % 2 == 0) $count=1;
				break;
			case "小双":
				if($sum <= 13 && $sum % 2 == 0) $count=1;
				break;
			case "极大":
				if($sum >= 23) $count=1;
				break;
			case "极小":
				if($sum <= 4) $count=1;
				break;
			case "豹子":
				if(count(array_unique($codes)) == 1) $count = 1;
				break;
			case "红波":
				if($sum % 3 == 0 && !in_array($sum, $ball)) $count = 1;
				if(in_array($sum, $ball)) $count = -1;
				break;
			case "绿波":
				if($sum % 3 == 1 && !in_array($sum, $ball)) $count = 1;
				if(in_array($sum, $ball)) $count = -1;
				break;
			case "蓝波":
				if($sum % 3 == 2 && !in_array($sum, $ball)) $count = 1;
				if(in_array($sum, $ball)) $count = -1;
				break;
			default:
				if($actionData == $sum) $count=1;
				break;
		}
		return $count;
	}
	
	/**快乐十分**/
	//总和
	public function klsfzh($actionData, $openCode) {
		$codes = explode(',', $openCode);
		$sum = array_sum($codes);
		$last = substr($sum, -1);
		$count = 0;
		switch($actionData){
			case "总和大":
				if($sum > 84 && $sum < 133) $count=1;
				if($sum==84) $count = -1;
				break;
			case "总和小":
				if($sum > 35 && $sum < 84) $count=1;
				if($sum==84) $count = -1;
				break;
			case "总和单":
				if($sum % 2 == 1) $count=1;
				break;
			case "总和双":
				if($sum % 2 == 0) $count=1;
				break;
			case "总和尾大":
				if($last > 4) $count=1;
				break;
			case "总和尾小":
				if($last < 5) $count=1;
				break;
			default:
				break;
		}
		return $count;
	}
	//正码
	public function klsfzm($actionData, $first, $last ='') {
		$count = 0;
		$wei = substr($first, -1);
		$hs = '';
		switch($actionData){
			case "大":
				if($first > 10 && $first < 21) $count=1;
				break;
			case "小":
				if($first > 0 && $first < 11) $count=1;
				break;
			case "单":
				if($first % 2 == 1) $count=1;
				break;
			case "双":
				if($first % 2 == 0) $count=1;
				break;
			case "尾大":
				if($wei > 4) $count=1;
				break;
			case "尾小":
				if($wei < 5) $count=1;
				break;
			case "合数单":
				if($first>0 && $first<10){
					if($first % 2 == 1) $count=1;
				}else if($first>9 && $first<21){
					$first = strval($first);;
					$hs = intval(substr($first,-1) + substr($first,0,1));
					if($hs % 2 == 1) $count=1;
				}
				break;
			case "合数双":
				if($first>0 && $first<10){
					if($first % 2 == 0) $count=1;
				}else if($first>9 && $first<21){
					$first = strval($first);;
					$hs = intval(substr($first,-1) + substr($first,0,1));
					if($hs % 2 == 0) $count=1;
				}
				break;
			case "龙":
				if($first > $last) $count=1;
				break;
			case "虎":
				if($last > $first) $count=1;
				break;
			default:
				if($actionData == $first) $count=1;
				break;
		}
		return $count;
	}
	//正码1
	public function klsfzm1($actionData, $openCode) {
		$codes = explode(",", $openCode);
		return $this->klsfzm($actionData, $codes[0], $codes[7]);
	}
	//正码2
	public function klsfzm2($actionData, $openCode) {
		$codes = explode(",", $openCode);
		return $this->klsfzm($actionData, $codes[1], $codes[6]);
	}
	//正码3
	public function klsfzm3($actionData, $openCode) {
		$codes = explode(",", $openCode);
		return $this->klsfzm($actionData, $codes[2], $codes[5]);
	}
	//正码4
	public function klsfzm4($actionData, $openCode) {
		$codes = explode(",", $openCode);
		return $this->klsfzm($actionData, $codes[3], $codes[4]);
	}
	//正码5
	public function klsfzm5($actionData, $openCode) {
		$codes = explode(",", $openCode);
		return $this->klsfzm($actionData, $codes[4]);
	}
	//正码6
	public function klsfzm6($actionData, $openCode) {
		$codes = explode(",", $openCode);
		return $this->klsfzm($actionData, $codes[5]);
	}
	//正码7
	public function klsfzm7($actionData, $openCode) {
		$codes = explode(",", $openCode);
		return $this->klsfzm($actionData, $codes[6]);
	}
	//正码8
	public function klsfzm8($actionData, $openCode) {
		$codes = explode(",", $openCode);
		return $this->klsfzm($actionData, $codes[7]);
	}
	//8中1
	public function klsfq8z1($actionData, $openCode) {
		$count = 0;
		$codes = explode(",", $openCode);
		if(in_array($actionData, $codes)) $count = 1;
		return $count;
	}
	/**六合彩**/
	// 特码
	public function lhctm($actionData, $openCode) {
		$count = 0;
		$sp = explode(',', $openCode)[6];
		if($actionData == $sp) $count=1;
		return $count;
	}
	// 两面
	public function lhclm($actionData, $openCode, $Groupname, $betInfo, $animalsYear) {
		$codeAry = explode(",", $openCode);
		
		$tm = $codeAry[6];
		$codes = explode(",", $codeAry[0]);
		$sum = array_sum($codes) + $tm;
		$tmAry = str_split($tm);
		$tmh = array_sum($tmAry);
		//$tmZodiac = $this->tm2Zodiacs($tm);
		$tmZodiac = $this->getZodica($tm,$animalsYear);
		
		$count = 0;
		switch($actionData) {
			case "特大":
				if($tm>=25 && $tm<=48) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特小":
				if($tm>0 && $tm<=24) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特单":
				if($tm % 2 == 1 && $tm != 49) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特双":
				if($tm % 2 == 0 && $tm != 49) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特合大":
				if($tmh >= 7 && $tm != 49) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特合小":
				if($tmh <= 6 && $tm != 49) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特合单":
				if($tmh % 2 == 1 && $tm != 49) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特合双":
				if($tmh % 2 == 0 && $tm != 49) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特尾大":
				if($tmAry[1] >= 5 && $tmAry[1] <=9 && $tm != 49) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特尾小":
				if($tmAry[1] >= 0 && $tmAry[1] <=4 && $tm != 49) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特大双":
				if($tm >= 25 && $tm <= 48 && $tm % 2 == 0) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特小双":
				if($tm >= 0 && $tm <= 24 && $tm % 2 == 0) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特大单":
				if($tm >= 25 && $tm <= 48 && $tm % 2 == 1) $count=1;
				if($tm==49) $count = -1;
				break;
			case "特小单":
				if($tm >= 0 && $tm <= 24 && $tm % 2 == 1) $count=1;
				if($tm==49) $count = -1;
				break;
			case "总和大":
				if($sum >= 175) $count=1;
				break;
			case "总和小":
				if($sum <= 174 && $sum >= 15) $count=1;
				break;
			case "总和单":
				if($sum % 2 == 1 && $sum >= 15) $count=1;
				break;
			case "总和双":
				if($sum % 2 == 0 && $sum >= 15) $count=1;
				break;
			case "特天肖":
				if(in_array($tmZodiac, $this->SX_TIAN)) $count=1;
				break;
			case "特地肖":
				if(in_array($tmZodiac, $this->SX_DI)) $count=1;
				break;
			case "特前肖":
				if(in_array($tmZodiac, $this->SX_QIAN)) $count=1;
				break;
			case "特后肖":
				if(in_array($tmZodiac, $this->SX_HOU)) $count=1;
				break;
			case "特家肖":
				if(in_array($tmZodiac, $this->SX_JIA)) $count=1;
				break;
			case "特野肖":
				if(in_array($tmZodiac, $this->SX_YE)) $count=1;
				break;
		}
		return $count;
	}
	// 色波
	public function lhcsb($actionData, $openCode) {
		$tm = explode(",", $openCode)[6];
		$count = 0;
		switch($actionData) {
			case "红波":
				if(in_array($tm, $this->HONG_NUMS)) $count=1;
				break;
			case "蓝波":
				if(in_array($tm, $this->LANG_NUMS)) $count=1;
				break;
			case "绿波":
				if(in_array($tm, $this->LV_NUMS)) $count=1;
				break;
			case "红大":
				if($tm >= 25 && $tm <= 48 && in_array($tm, $this->HONG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "红小":
				if($tm > 0 && $tm <= 24 && in_array($tm, $this->HONG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "红单":
				if($tm % 2 == 1 && in_array($tm, $this->HONG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "红双":
				if($tm % 2 == 0 && in_array($tm, $this->HONG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "蓝大":
				if($tm >= 25 && $tm <= 48 && in_array($tm, $this->LANG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "蓝小":
				if($tm > 0 && $tm <= 24 && in_array($tm, $this->LANG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "蓝单":
				if($tm % 2 == 1 && in_array($tm, $this->LANG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "蓝双":
				if($tm % 2 == 0 && in_array($tm, $this->LANG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "绿大":
				if($tm >= 25 && $tm <= 48 && in_array($tm, $this->LV_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "绿小":
				if($tm > 0 && $tm <= 24 && in_array($tm, $this->LV_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "绿单":
				if($tm % 2 == 1 && in_array($tm, $this->LV_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "绿双":
				if($tm % 2 == 0 && in_array($tm, $this->LV_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "红大双":
				if($tm >= 25 && $tm <= 48 && $tm % 2 == 0 && in_array($tm, $this->HONG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "红小双":
				if($tm > 0 && $tm <= 24 && $tm % 2 == 0 && in_array($tm, $this->HONG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "红大单":
				if($tm >= 25 && $tm <= 48 && $tm % 2 == 1 && in_array($tm, $this->HONG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "红小单":
				if($tm > 0 && $tm <= 24 && $tm % 2 == 1 && in_array($tm, $this->HONG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "蓝大双":
				if($tm >= 25 && $tm <= 48 && $tm % 2 == 0 && in_array($tm, $this->LANG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "蓝小双":
				if($tm > 0 && $tm <= 24 && $tm % 2 == 0 && in_array($tm, $this->LANG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "蓝大单":
				if($tm >= 25 && $tm <= 48 && $tm % 2 == 1 && in_array($tm, $this->LANG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "蓝小单":
				if($tm > 0 && $tm <= 24 && $tm % 2 == 1 && in_array($tm, $this->LANG_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "绿大双":
				if($tm >= 25 && $tm <= 48 && $tm % 2 == 0 && in_array($tm, $this->LV_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "绿小双":
				if($tm > 0 && $tm <= 24 && $tm % 2 == 0 && in_array($tm, $this->LV_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "绿大单":
				if($tm >= 25 && $tm <= 48 && $tm % 2 == 1 && in_array($tm, $this->LV_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
			case "绿小单":
				if($tm > 0 && $tm <= 24 && $tm % 2 == 1 && in_array($tm, $this->LV_NUMS)) $count=1;
				if($tm==49) $count = -1;
				break;
		}
		return $count;
	}
	// 特肖
	public function lhctx($actionData, $openCode, $Groupname, $betInfo, $animalsYear) {
		$tm = explode(",", $openCode)[6];
		$zodiac = $this->getZodica($tm, $animalsYear);
		$count = 0;
		if($actionData == $zodiac) $count=1;
		return $count;
	}
	// 特码头数
	public function lhctmts($actionData, $openCode) {
		$sp = explode(",", $openCode)[6];
		$tou = str_split($sp)[0];
		
		$count = 0;
		if($actionData === $tou."头") $count = 1;
		return  $count;
	}
	// 特码尾数
	public function lhctmws($actionData, $openCode) {
		$sp = explode(",", $openCode)[6];
		$wei = str_split($sp)[1];
		$count = 0;
		if($actionData === $wei."尾")  $count = 1;
		return $count;
	}
	// 正码
	public function lhczm($actionData, $openCode) {
		$codes = explode("+", $openCode)[0];
		$codeAry = explode(",", $codes);
		$count = 0;
		if(in_array($actionData, $codeAry)) $count = 1;
		return $count;
	}
	//正码特
	public function lhczmt($actionData, $code) {
		$count = 0;
		$codeAry = str_split($code);
		$sum = array_sum($codeAry);
		switch($actionData){
			case "大码":
				if($code >= 25 && $code <= 48) $count=1;
				if($code == 49) $count = -1;
				break;
			case "小码":
				if($code> 0 && $code <= 24) $count=1;
				if($code == 49) $count = -1;
				break;
			case "单码":
				if($code % 2 == 1) $count=1;
				if($code==49) $count = -1;
				break;
			case "双码":
				if($code % 2 == 0) $count=1;
				if($code==49) $count = -1;
				break;
			case "合大":
				if($sum >= 7) $count=1;
				if($code==49) $count = -1;
				break;
			case "合小":
				if($sum <= 6) $count=1;
				if($code==49) $count = -1;
				break;
			case "合单":
				if($sum % 2 == 1) $count=1;
				if($code==49) $count = -1;
				break;
			case "合双":
				if($sum % 2 == 0) $count=1;
				if($code==49) $count = -1;
				break;
			case "尾大":
				if($codeAry[1] >= 5 && $codeAry[1] <= 9)$count=1;
				if($code==49) $count = -1;
				break;
			case "尾小":
				if($codeAry[1] >= 0 && $codeAry[1] <= 4)$count=1;
				if($code==49) $count = -1;
				break;
			case "红波":
				if(in_array($code, $this->HONG_NUMS)) $count=1;
				break;
			case "蓝波":
				if(in_array($code, $this->LANG_NUMS)) $count=1;
				break;
			case "绿波":
				if(in_array($code, $this->LV_NUMS)) $count=1;
				break;
			default:
				if($actionData == $code)$count = 1;
				break;
		}
		return $count;
	}
	//正一特/正码一
	public function lhcz1t($actionData, $openCode) {
		$codes = explode("+", $openCode)[0];
		$codeAry = explode(",", $codes);
		return $this->lhczmt($actionData, $codeAry[0]);
	}
	//正二特/正码二
	public function lhcz2t($actionData, $openCode) {
		$codes = explode("+", $openCode)[0];
		$codeAry = explode(",", $codes);
		return $this->lhczmt($actionData, $codeAry[1]);
	}
	//正三特/正码三
	public function lhcz3t($actionData, $openCode) {
		$codes = explode("+", $openCode)[0];
		$codeAry = explode(",", $codes);
		return $this->lhczmt($actionData, $codeAry[2]);
	}
	//正四特/正码四
	public function lhcz4t($actionData, $openCode) {
		$codes = explode("+", $openCode)[0];
		$codeAry = explode(",", $codes);
		return $this->lhczmt($actionData, $codeAry[3]);
	}
	//正五特/正码五
	public function lhcz5t($actionData, $openCode) {
		$codes = explode("+", $openCode)[0];
		$codeAry = explode(",", $codes);
		return $this->lhczmt($actionData, $codeAry[4]);
	}
	//正六特/正码六
	public function lhcz6t($actionData, $openCode) {
		$codes = explode("+", $openCode)[0];
		$codeAry = explode(",", $codes);
		return $this->lhczmt($actionData, $codeAry[5]);
	}
	//五行
	public function lhcwx($actionData, $openCode) {
		$count = 0;
		$tm = explode(",", $openCode)[6];
		switch($actionData){
			case "金":
				if(in_array($tm, $this->JIN_NUMS)) $count=1 ;
				break;
			case "木":
				if(in_array($tm, $this->MU_NUMS)) $count=1 ;
				break;
			case "水":
				if(in_array($tm, $this->SHUI_NUMS)) $count=1 ;
				break;
			case "火":
				if(in_array($tm, $this->HUO_NUMS)) $count=1 ;
				break;
			case "土":
				if(in_array($tm, $this->TU_NUMS)) $count=1 ;
				break;
			default:
				break;
		}
		return $count;
	}
	//平特一肖
	public function lhcptyx($actionData, $openCode, $Groupname, $betInfo, $animalsYear) {
		$count = 0;
		//$tmp = explode('+', $openCode);
		$codes = explode(',', $openCode);
		//array_push($codes, $tmp[1]);
		foreach($codes as $code){
			//$zodiac = $this->tm2Zodiacs($code);
			$zodiac = $this->getZodica($code,$animalsYear);
			if($zodiac == $actionData){
				$count=1;
				break;
			}
		}
		return $count;
	}
	//平特尾数
	public function lhcptws($actionData, $openCode) {
		$count = 0;
		//$tmp = explode('+', $openCode);
		$codes = explode(',', $openCode);
		//array_push($codes, $tmp[1]);
		foreach($codes as $code){
			if(strlen($code)<2)
			{
				$weiSu = str_split($code)[0];
			}else
			{
				$weiSu = str_split($code)[1];
			}
			if($weiSu."尾" == $actionData){
				$count=1;
				break;
			}
		}
		return $count;
	}
	//正肖
	public function lhczx($actionData, $openCode, $Groupname, $betInfo, $animalsYear) {
		$count=0;
		//$tmp = explode('+',$openCode);
		$temp = explode(',',$openCode);
		$codes = [$temp[0],$temp[1],$temp[2],$temp[3],$temp[4],$temp[5]];
		for($i=0; $i<count($codes); $i++){
			if($this->getZodica($codes[$i],$animalsYear)==$actionData){
				$count +=1;
			}
		}
		if ($count>4) $count=4;
		return $count;
	}
	//7色波
	public function lhc7sb($actionData, $openCode) {
		$count=0;
		$HONG_count=0;
		$LANG_count=0;
		$LV_count=0;
		//$tmp = explode('+', $openCode);
		$codes = explode(',', $openCode);
		//array_push($codes, $tmp[1]);
		
		foreach($codes as $val){
		//for($i=0; $i<count($codes); $i++){
			$val=intval($val);
			
			if(in_array($val, $this->HONG_NUMS, true)){
				$HONG_count+=1;
			}else if(in_array($val, $this->LANG_NUMS, true)){
				$LANG_count+=1;
			}else if(in_array($val, $this->LV_NUMS, true)){
				$LV_count+=1;
			}
		}

		if($actionData=='红波'){
			if($HONG_count>$LANG_count && $HONG_count>$LV_count) $count=1;
			if(($HONG_count==3 && $HONG_count==$LANG_count) || ( $LANG_count==3 && $LANG_count==$LV_count) || ($LV_count==3 && $HONG_count==$LV_count)) $count = -1;
		}else if($actionData=='蓝波'){
			if($LANG_count>$HONG_count && $LANG_count>$LV_count) $count=1;
			if(($HONG_count==3 && $HONG_count==$LANG_count) || ( $LANG_count==3 && $LANG_count==$LV_count) || ($LV_count==3 && $HONG_count==$LV_count)) $count = -1;
		}else if($actionData=='绿波'){
			if($LV_count>$HONG_count && $LV_count>$LANG_count) $count=1;
			if(($HONG_count==3 && $HONG_count==$LANG_count) || ( $LANG_count==3 && $LANG_count==$LV_count) || ($LV_count==3 && $HONG_count==$LV_count)) $count = -1;
		}else if($actionData=='和局'){
			if(($HONG_count==3 && $HONG_count==$LANG_count) || ( $LANG_count==3 && $LANG_count==$LV_count) || ($LV_count==3 && $HONG_count==$LV_count)) $count = 1;
		}
		return $count;
	}
	//总肖
	public function lhczhongxiao($actionData, $openCode, $Groupname, $betInfo, $animalsYear){
		$count=0;
		//$tmp = explode('+', $openCode);
		$codes = explode(',', $openCode);
		//array_push($codes, $tmp[1]);
		for($i=0; $i<count($codes); $i++){
			$codes[$i] = $this->getZodica($codes[$i],$animalsYear);
		}
		$codes=array_unique($codes);

		$betarr = preg_split('/(?<!^)(?!$)/u', $actionData );

		if (count($betarr) == 2 && $betarr[1]=='肖'){
			if(intval($betarr[0])==count($codes)) $count=1;
		}else if(count($betarr) == 3 && $actionData=='总肖单'){
			if(count($codes)%2!=0) $count=1;
		}else if(count($betarr)==3 && $actionData=='总肖双'){
			if(count($codes)%2==0) $count=1;
		}
		return $count;	
	}

	public function dxds2($actionData, $first, $last =''){
		$count = 0;
		$val = '';
		if($first >= 6 && $first < 11 )
			$val .= "大";
		else
			$val .= "小";
		
		if($first % 2 == 1)
			$val .= "单";
		else
			$val .= "双";
		
		if($last){
			if($first > $last)
				$val .= "龙";
			else
				$val .= "虎";
		}
		if(strrpos($val, $actionData) !== false) $count++;
		return $count;
	}

	// public function tm2Zodiacs($tmCode){
	// 	$tmCode= strval($tmCode);
	// 	$zodiac = "";
	// 	foreach($this->ZODIAC as $k => $v){
	// 		if(strrpos($v, $tmCode) > -1){
	// 			$zodiac = $k;
	// 			break;
	// 		}
	// 	}
	// 	return $zodiac;
	// }

	//计算起始生肖对应的数组键名
	public function getYearZodicaIndex($year) {
		for($i=0;$i<count($this->LHC['zodiacArr']);$i++){
			if ($year == $this->LHC['zodiacArr'][$i]) {
				return $i;
			}
		}
	}
	//计算投注生肖对应的号码组
	public function getZodicaNum($sxName, $year) {
		$index = $this->getYearZodicaIndex($year);
		$j = 1;
		for ($i = $index; $i >= 0; $i--) {
			if ($this->LHC['zodiacArr'][$i] == $sxName) {
				return $j;
			}
			$j += 1;
		}
		for ($i = count($this->LHC['zodiacArr']) - 1; $i >= $index; $i--) {
			if ($this->LHC['zodiacArr'][$i] == $sxName) {
				return $j;
			}
			$j += 1;
		}
	}
	//计算号码对应的生肖
	public function getZodica($num,$year){
		for($i=0;$i<count($this->LHC['zodiacArr']);$i++){
			$zodiac = $this->LHC['zodiacArr'][$i];
			$sxs = $this->SX_NUMS[$this->getZodicaNum($zodiac,$year)];
			$str = '';
			for($j=0;$j<count($sxs);$j++){
				if (intval($sxs[$j]) == intval($num)) {
					return $zodiac;
				}				
			}
		}
		return '';
	}

	/**福彩3D/排3**/
	//第N球
	//第一球
	public function p3d1b($actionData, $openCode) {
		return $this->sscd1b($actionData, $openCode);
	}
	//第二球
	public function p3d2b($actionData, $openCode) {
		return $this->sscd2b($actionData, $openCode);
	}
	//第三球
	public function p3d3b($actionData, $openCode) {
		return $this->sscd3b($actionData, $openCode);
	}
	// 总和大小单双
	public function p3zh($actionData, $openCode){
		$codes = explode(',', $openCode);
		$sum = array_sum($codes);
		$val = "";
		if($sum >= 14)
			$val .= "总和大";
		else
			$val .= "总和小";
		if($sum % 2 == 1)
			$val .= "总和单";
		else
			$val .= "总和双";
		
		$count = 0;
		if(strrpos($val, $actionData) !== false) $count++;
		return $count;
	}
	//龙虎和
	public function p3lhh($actionData, $openCode, $first=0, $last=2){
		$codes = explode(',', $openCode);
		$count = 0;
		
		if($codes[$first] == $codes[$last]){
			if(strrpos($actionData, '和') !== false) $count = 1;
		} else if($codes[$first] > $codes[$last]) {
			if(strrpos($actionData, '龙') !== false) $count = 1;
		} else {
			if(strrpos($actionData, '虎') !== false) $count = 1;
		}
		return $count;
	}
	//综合:豹子/对子/顺子/半顺/杂六
	public function p3sl($actionData, $openCode) {
		$codes = explode(',', $openCode);
		return $this->sscr3($actionData, $codes);
	}
	//跨度
	public function p3kd($actionData, $openCode) {
		$count = 0;
		$codes = explode(',', $openCode);
		sort($codes);
		$min = array_shift($codes);
		$max = array_pop($codes);
		
		if(($max - $min) == $actionData) $count = 1;
		return $count;
	}	
}
