<?php
//快速管理地址：http://face.apius.cn?token=ff43912c7b61393f&verify=319db670c20d
include_once 'cron.config.php';
include_once 'KaiJiang.php';
include_once 'credit_kj.php';
include_once 'analysis.php';

class Cron extends KJ
{

	private $played = array();
	private $KaiJiang;
	private $credit_kj;
	private $official_kj;
	private $analysis;
	private $playMapping = '';

	public function __construct() {
		parent::__construct();
		$this->setPlayed();
		$this->getAllPlayed();
		//$this->KaiJiang = new KaiJiang();
		$this->credit_kj = new credit_kj();
		$this->official_kj = new KaiJiang();
		$this->analysis = new analysis();
	}
	private function setPlayed() {
		$sql = "
			SELECT
				id,
				ruleFun
			FROM
				`{$this->prename}played`
		";

		$result = $this->getRows($sql);
		foreach ($result as $rows) {
			$this->played[$rows['id']] = $rows['ruleFun'];
		}
		unset($result);
	}
	private function getAllPlayed() {
		if($this->playMapping) return;
		$sql ="
			SELECT
				t.id,
				p.groupId,
				p.id AS playId
			FROM
				{$this->prename}type t,
				{$this->prename}played p
			WHERE
				p.type=t.type
			and
				t.ENABLE = 1
			AND p.ENABLE = 1
			ORDER BY
				t.id,
				p.groupId,
				p.id
		";
		$result = $this->getRows($sql);
		foreach ($result as $rows){
			$this->playMapping .= ",{$rows['id']}-{$rows['groupId']}-{$rows['playId']}";
		}
		$this->playMapping .= ',';
	}


	public function getBets($type,$actionNo) {
		$sql = "
			SELECT
				id,
				playedId,
				actionData,
				weiShu,
				actionName,
				type,
				playedGroup,
				betInfo,
				isGuan,
				actionAmount,
				bonusProp
			FROM
				`{$this->prename}bets`
			WHERE isDelete = 0
			AND check_error = 0
			AND type = :type
			AND actionNo = :actionNo
			AND lotteryNo = ''
			order by id desc
		";
		$params = array(
			'type' => $type,
			'actionNo' => $actionNo
		);
		return $this->getRows($sql, $params);
	}

	public function getCheckBets($type,$actionNo) {
		$sql = "
			SELECT
				id,
				playedId,
				actionData,
				weiShu,
				actionName,
				type,
				playedGroup,
				betInfo,
				isGuan,
				actionAmount,
				bonusProp
			FROM
				`{$this->prename}bets_check`
			WHERE isDelete = 0
			AND check_error = 0
			AND type = :type
			AND actionNo = :actionNo
			AND lotteryNo = ''
			order by id desc
		";
		$params = array(
			'type' => $type,
			'actionNo' => $actionNo
		);

		return $this->getRows($sql, $params);
	}

    public function check_error($id)
    {
        $this->beginTransaction();
        try {
            $this->query("UPDATE `{$this->prename}bets` SET `check_error`=1 WHERE `id`=$id LIMIT 1");
            $this->commit();
			echo "......... 備註成功({$id})  \n";
        }
        catch (Exception $e) {
        	echo $e." \n";
            $this->rollBack();
            echo "......... 備註失敗({$id})  \n";
        }
	}
	
	public function getGuestBets($type,$actionNo) {
		$sql = "
			SELECT
				id,
				playedId,
				actionData,
				weiShu,
				actionName,
				type,
				playedGroup,
				betInfo,
				isGuan
			FROM
				`{$this->prename}guest_bets`
			WHERE isDelete = 0
			AND type = :type
			AND actionNo = :actionNo
			AND lotteryNo = ''
		";
		$params = array(
			'type' => $type,
			'actionNo' => $actionNo
		);
		return $this->getRows($sql, $params);
	}

	// public function doKanJiang($params) {
	// 	return $this->update("call kanJiang(?, ?, ?, ?) ", $params);
	// }
	
	//begin 走勢圖 by robert
	public function trend($type,$actionNo,$kjData) {
		$sql="select data from {$this->prename}data where type={$type} order by id desc  limit 49";
		$get_data =$this->getRows($sql);
		$sql="select type from {$this->prename}type where id={$type}";
		$get_type = $this->getRow($sql);
		$an_data=null;
		$clong=null;
		switch(true)
		{
			case $get_type['type']==1: //時時彩
				$an_data=$this->analysis->ssc($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],5);
				break;
			case $get_type['type']==2: //11選5
				$an_data=$this->analysis->x11x5($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],5);
				break;
			case $get_type['type']==3: //排列3 福彩3D
				$an_data=$this->analysis->p3($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],3);
				break;
			case $get_type['type']==4: //快乐十分
				$an_data=$this->analysis->kl10($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],8);
				break;
			case $get_type['type']==6: //PK10
				$an_data=$this->analysis->pk10($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],10);
				break;
			case $get_type['type']==8: //快樂8
				$an_data=$this->analysis->k8($kjData,$get_data);
				break;
			case $get_type['type']==9: //快3
				$an_data=$this->analysis->k3($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],3);
				break;
			case $get_type['type']==11: //幸運28
				$an_data=$this->analysis->lucky28($kjData,$get_data);
				$clong=$this->analysis->clong($kjData,$get_data,$get_type['type'],1);
				break;
			case $get_type['type']==12: //六合彩
				$an_data=$this->analysis->lhc($kjData,$get_data);
				break;
		}
		if($an_data)
		{
			$sql="delete from {$this->prename}trend where type={$type}";
			$this->query($sql);
			if($get_type['type']==12)
			{
				$sql="insert into {$this->prename}trend(type,number,time,lost,hot,sp_lost,sp_hot,data)
				VALUES({$type},'{$actionNo}',".time().",'". json_encode($an_data['lost'],JSON_FORCE_OBJECT)."','". json_encode($an_data['hot'],JSON_FORCE_OBJECT)."','". json_encode($an_data['sp_lost'],JSON_FORCE_OBJECT)."','". json_encode($an_data['sp_hot'],JSON_FORCE_OBJECT)."','".$kjData."')";
			}else
			{
				$sql="insert into {$this->prename}trend(type,number,time,lost,hot,clong,data)
				VALUES({$type},'{$actionNo}',".time().",'". json_encode($an_data['lost'],JSON_FORCE_OBJECT)."','". json_encode($an_data['hot'],JSON_FORCE_OBJECT)."','". json_encode($clong,JSON_FORCE_OBJECT)."','".$kjData."')";
			}
			$this->query($sql);
		}
	}
	//end 走勢圖 by robert

	public function run($type,$actionNo,$kjData) {
		$bets = $this->getBets($type,$actionNo);
		$checkbets = $this->getCheckBets($type,$actionNo);
		$settings = $this->getSettings();
		$lottery_copy=array();
		$time=time();
		//if($bets) foreach ($bets as $k=>$rows) {
		if($bets) foreach ($bets as $k=>$rows) {

			$md5=md5($rows['id'].$rows['playedId'].$rows['playedGroup'].$rows['actionData'].$rows['type'].$rows['betInfo'].$rows['actionAmount'].$rows['bonusProp']);
			$checkmd5=md5($checkbets[$k]['id'].$checkbets[$k]['playedId'].$checkbets[$k]['playedGroup'].$checkbets[$k]['actionData'].$checkbets[$k]['type'].$checkbets[$k]['betInfo'].$checkbets[$k]['actionAmount'].$checkbets[$k]['bonusProp']);
			if($md5!=$checkmd5)
			{
				//echo $rows['id']." - ".$rows['playedId']." - ".$rows['playedGroup']." - ".$rows['actionData']." - ".$rows['type']." - ".$rows['betInfo']." - ".$rows['actionAmount']." - ".$rows['bonusProp']."\r\n";
				//echo $checkbets[$k]['id']." - ".$checkbets[$k]['playedId']." - ".$checkbets[$k]['playedGroup']." - ".$checkbets[$k]['actionData']." - ".$checkbets[$k]['type']." - ".$checkbets[$k]['betInfo']." - ".$checkbets[$k]['actionAmount']." - ".$checkbets[$k]['bonusProp']."\r\n";
				echo "注單被修改 - ". $rows['id']." - ".$rows['type']." ";
				$this->check_error($rows['id']);
				continue;				
			}

			if (strpos($this->playMapping, ','.$rows['type'].'-'.$rows['playedGroup'].'-'.$rows['playedId'].',') === false) continue;
			$method = $this->played[$rows['playedId']];
			$zjCount = 0;
			//$kjData = '1,3,5,7,9';
			if($rows['isGuan']==0)
			{
				$this->KaiJiang=$this->credit_kj;
			}
			if($rows['isGuan']==1)
			{
				$this->KaiJiang=$this->official_kj;
			}
			if(method_exists($this->KaiJiang, $method)) 
			{
				if(strpos($rows['actionData'], '|') !== false) {
					$betsArray = explode('|',$rows['actionData']);
					foreach ($betsArray as $actionData) {
						switch(true)
						{
							default:
								if ($rows['weiShu'] > 0) {
									$zjCount += $this->KaiJiang->$method($actionData, $kjData, $rows['weiShu']);
								}
								else {
									$zjCount += $this->KaiJiang->$method($actionData, $kjData);
								}
								break;
						}
					}
				}
				else 
				{
					switch(true)
					{
						case $type==90:
							$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData, $rows['actionName'],$rows['betInfo'],$settings['animalsYear'],$settings['lhcWxJin'], $settings['lhcWxMu'], $settings['lhcWxShui'], $settings['lhcWxHuo'], $settings['lhcWxTu']);
							break;
						default:
							if ($rows['weiShu'] > 0) {
								$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData, $rows['weiShu']);
							}
							else {
								$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData);
							}
							break;
					}
				}
				$lottery_copy[]="(".$rows['id'].",".$zjCount.",'".$kjData."'".",".$type.",".$time.")";
				if(count($lottery_copy)>1000)
				{
					$sql="insert into {$this->prename}kanjiang_temp(id,zjCount,kjData,type,actionTime)VALUES".implode(",", $lottery_copy);
					$this->query($sql);
					$this->update("call pre_kanjiang('lottery_running',{$type},0)");
					$lottery_copy=array();		
				}
			}
		}
		//print_r($lottery_copy);
		//exit;
		if(count($lottery_copy)>0)
		{
			$sql="insert into {$this->prename}kanjiang_temp(id,zjCount,kjData,type,actionTime)VALUES".implode(",", $lottery_copy);
			//echo $sql;
			$this->query($sql);
			//var_dump($this->KaiJiang->lucky28hz("13",$kjData));
			$this->update("call pre_kanjiang('lottery_running',{$type},0)");			
		}

		//試玩帳號開獎 begin
		$bets = $this->getGuestBets($type,$actionNo);
		//$settings = $this->getSettings();
		$lottery_copy=array();
		$time=time();
		if($bets) foreach ($bets as $rows) {
			if (strpos($this->playMapping, ','.$rows['type'].'-'.$rows['playedGroup'].'-'.$rows['playedId'].',') === false) continue;
			$method = $this->played[$rows['playedId']];

			$zjCount = 0;
			//$kjData = '1,3,5,7,9';
			if($rows['isGuan']==0)
			{
				$this->KaiJiang=$this->credit_kj;
			}
			if($rows['isGuan']==1)
			{
				$this->KaiJiang=$this->official_kj;
			}

			if(method_exists($this->KaiJiang, $method)) 
			{
				if(strpos($rows['actionData'], '|') !== false) {
					$betsArray = explode('|',$rows['actionData']);
					foreach ($betsArray as $actionData) {
						switch(true)
						{
							default:
								if ($rows['weiShu'] > 0) {
									$zjCount += $this->KaiJiang->$method($actionData, $kjData, $rows['weiShu']);
								}
								else {
									$zjCount += $this->KaiJiang->$method($actionData, $kjData);
								}
								break;
						}
					}
				}
				else 
				{
					switch(true)
					{
						case $type==90:
							$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData, $rows['actionName'],$rows['betInfo'],$settings['animalsYear'],$settings['lhcWxJin'], $settings['lhcWxMu'], $settings['lhcWxShui'], $settings['lhcWxHuo'], $settings['lhcWxTu']);
							break;
						default:
							if ($rows['weiShu'] > 0) {
								$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData, $rows['weiShu']);
							}
							else {
								$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData);
							}
							break;
					}
				}
				$lottery_copy[]="(".$rows['id'].",".$zjCount.",'".$kjData."'".",".$type.",".$time.")";
				if(count($lottery_copy)>1000)
				{
					$sql="insert into {$this->prename}guest_kanjiang_temp(id,zjCount,kjData,type,actionTime)VALUES".implode(",", $lottery_copy);
					$this->query($sql);
					$this->update("call guest_pre_kanjiang('lottery_running',{$type},0)");
					$lottery_copy=array();		
				}
			}
		}
		if(count($lottery_copy)>0)
		{
			$sql="insert into {$this->prename}guest_kanjiang_temp(id,zjCount,kjData,type,actionTime)VALUES".implode(",", $lottery_copy);
			//echo $sql;
			$this->query($sql);
			//var_dump($this->KaiJiang->lucky28hz("13",$kjData));
			$this->update("call guest_pre_kanjiang('lottery_running',{$type},0)");			
		}
		//試玩帳號開獎 end
	}

	public function fetchKaiJiang($lottery){
		foreach($lottery as $k=>$v)
		{
			$type=$v['type'];
			$time=$v['time'];
			$number=$v['number'];
			$data=$v['data'];
			$sql = "
				INSERT INTO {$this->prename}data
				 (type, time, number, data) VALUES (?,?,?,?)
			";
			try{
				$result = $this->insert($sql,[$type, $time, $number, $data]);
				//echo $sql." - ".$type." - ".$time." - ".$number." - ".$data."/r/n";
			} catch (Exception $e) {
				var_dump($e->getMessage());
				//if($e->errorInfo[1] == 1062) {
				//	echo " <== [已寫入] {$dtime}\n";
				//}
			}
			$this->run($type,$number,$data);
		}
		
	}

}

$Cron = new Cron();
//			'merchant_no'=>'icrown',
//			'key'=>'8359aaa5-ad06-11e7-9f73-71f4466',
//{"merchant_no":"hh892","lottery":[{"id":"55548","type":"1","time":"1522222222","number":"20180619069","data":"1,2,3,4,5"},{"id":"55547","type":"73","time":"1525247004","number":"201805020944","data":"4,3,0,0,1"},{"id":"55546","type":"73","time":"1525238049","number":"201805020789","data":"8,8,1,1,3"},{"id":"55545","type":"73","time":"1525077943","number":"201804301006","data":"3,1,6,0,6"},{"id":"55544","type":"73","time":"1524128241","number":"201804191018","data":"0,9,4,9,0"},{"id":"55543","type":"73","time":"1524107210","number":"201804190666","data":"1,3,8,9,0"}],"sign":"e33ce0895dbd18b6495616a155bcdeb7"}
//$_POST = json_decode(file_get_contents("php://input",'r'), true);
$_POST = json_decode(file_get_contents("php://input",'r'), true);
//print_r($_POST);
//exit;
$merchant_no="icrown";							//商户号
$lottery=$_POST['lottery'];									//開獎號碼
$key = "8359aaa5-ad06-11e7-9f73-71f4466";					//商户接口秘钥
$sign=$_POST['sign'];										//md5加密串
//print_r($_POST);
//exit;
//MD5签名
//echo $merchant_no." - ".json_encode($lottery)." - ".$key;
$src = $merchant_no.json_encode($lottery).$key;
//file_put_contents('get_lottery.log',print_r($_POST,true)."\n", FILE_APPEND | LOCK_EX);
//echo $lottery;
//echo $sign." - ".md5($src);
//echo " ********* ";
//exit;
if($sign==md5($src))
{
	$Cron->fetchKaiJiang($lottery);
	echo "true";
}else
{
	/*
	MD5签名错误处理
	*/
	echo "false";
}

?>
