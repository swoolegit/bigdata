<?php
class KaiJiang {
	
	private $LHC = [
		'zodiacArr' => [ "鼠", "牛", "虎", "兔", "龙", "蛇", "马", "羊", "猴", "鸡", "狗", "猪" ]
	];

	private $LHCXX = [
		'HONG_NUMS' => [ 1, 2, 7, 8, 12, 13, 18, 19, 23, 24, 29, 30, 34, 35, 40, 45, 46 ],
		'LANG_NUMS' => [ 3, 4, 9, 10, 14, 15, 20, 25, 26, 31, 36, 37, 41, 42, 47, 48 ],
		'LV_NUMS' => [ 5, 6, 11, 16, 17, 21, 22, 27, 28, 32, 33, 38, 39, 43, 44, 49 ],
		
		'SX_TIAN' => [ '兔', '马', '猴', '猪', '牛', '龙' ],
		'SX_DI' => [ '蛇', '羊', '鸡', '狗', '鼠', '虎' ],
		
		'SX_QIAN' => [ '鼠', '牛', '虎', '兔', '龙', '蛇' ],
		'SX_HOU' => [ '马', '羊', '猴', '鸡', '狗', '猪' ],
		
		'SX_JIA' => [ '羊', '马', '牛', '猪', '狗', '鸡' ],
		'SX_YE' => [ '猴', '蛇', '龙', '兔', '虎', '鼠' ]
	];

	private $SX_NUMS = [
		"1" => [ 1, 13, 25, 37, 49 ],
		"2" => [ 2, 14, 26, 38 ],
		"3" => [ 3, 15, 27, 39 ],
		"4" => [ 4, 16, 28, 40 ],
		"5" => [ 5, 17, 29, 41 ],
		"6" => [ 6, 18, 30, 42 ],
		"7" => [ 7, 19, 31, 43 ],
		"8" => [ 8, 20, 32, 44 ],
		"9" => [ 9, 21, 33, 45 ],
		"10" => [ 10, 22, 34, 46 ],
		"11" => [ 11, 23, 35, 47 ],
		"12" => [ 12, 24, 36, 48 ]
	];

#======================================================================================================================================
# 时时彩
#======================================================================================================================================
	//五星定位胆
	public function dwd5x($bet, $data) {
		$count = 0;
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		array_map(
			function ($i,$val) use ($data, &$count) {
				if(strlen($val) > 1) {
					array_map(
						function ($number) use ($i, $data , &$count) {
							$number == $data[$i] and $count++;
						},
						str_split($val)
					);
				}
				else {
					$val == $data[$i] and $count++;
				}
			},
			array_keys($bet),
			$bet
		);
		return $count;
	}
	//十星定位胆
	public function dwd10x($bet, $data) {
		$count = 0;
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		array_map(
			function ($i,$val) use ($data, &$count) {
				if(strlen($val) > 2) {
					array_map(
						function ($number) use ($i, $data , &$count) {
							$number == $data[$i] and $count++;
						},
						//str_split($val)
						explode(' ', $val)
					);
				}
				else {
					$val == $data[$i] and $count++;
				}
			},
			array_keys($bet),
			$bet
		);
		return $count;
	}

	//五星複式
	public function dxwf5f($bet, $data) {
		return $this->fs($bet, $data);
	}

	// 五星单式
	public function dxwf5d($bet, $data) {
		return $this->ds($bet, $data);
	}

	// 组选120
	public function dxwf5z120($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		sort($data);
		if (count($data) != count(array_unique($data))) return 0;
		return $this->sames($bet, $data) == 5 ? 1 : 0;
	}

	// 组选60
	public function dxwf5z60($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		sort($data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] >= 3) return 0;
		$bet0 = str_split($bet[0]);
		$bet1 = str_split($bet[1]);
		$val = "";
		for($i=0;$i<count($data)-1;$i++) {
			if($data[$i] == $data[$i+1]) {
				$val .= $data[$i];
				array_splice($data,$i,2);
				break;
			}
		}
		if($this->sames($bet0, str_split($val)) == 1) {
			return ($this->sames($bet1,$data) == 3) ? 1 : 0;
		}
		return 0;
	}

	// 组选30
	public function dxwf5z30($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		sort($data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] >= 3) return 0;
		$bet0 = str_split($bet[0]);
		$bet1 = str_split($bet[1]);
		$val = "";
		for($i=0;$i<count($data)-1;$i++) {
			if($data[$i] == $data[$i+1]) {
				$val .= $data[$i];
				array_splice($data,$i,2);
				break;
			}
		}

		for($i=0;$i<count($data)-1;$i++) {
			if($data[$i] == $data[$i+1]) {
				$val .= $data[$i];
				array_splice($data,$i,2);
				break;
			}
		}
		if($this->sames($bet1, $data) == 1) {
			return ($this->sames($bet0, str_split($val)) == 2) ? 1 : 0;
		}
		return 0;
	}

	// 组选20
	public function dxwf5z20($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		sort($data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] > 3) return 0;
		$bet0 = str_split($bet[0]);
		$bet1 = str_split($bet[1]);
		$val = "";
		for($i=0;$i<count($data)-2;$i++) {
			if($data[$i] == $data[$i+1] && $data[$i+1] == $data[$i+2]) {
				$val .= $data[$i];
				array_splice($data,$i,3);
				break;
			}
		}

		if($this->sames($bet0, str_split($val)) == 1) {
			return ($this->sames($bet1,$data) == 2) ? 1 : 0;
		}

		return 0;
	}

	// 组选10
	public function dxwf5z10($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		sort($data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] > 3) return 0;
		$bet0 = str_split($bet[0]);
		$bet1 = str_split($bet[1]);
		$val1 = "";
		$val2 = "";
		for($i=0;$i<count($data)-2;$i++) {
			if($data[$i] == $data[$i+1] && $data[$i+1] == $data[$i+2]) {
				$val1 .= $data[$i];
				array_splice($data,$i,3);
				break;
			}
		}
		for($i=0;$i<count($data)-1;$i++) {
			if($data[$i] == $data[$i+1]) {
				$val2 .= $data[$i];
				break;
			}
		}
		if($this->sames($bet0, str_split($val1)) == 1) {
			return ($this->sames($bet1, str_split($val2)) == 1) ? 1 : 0;
		}
		return 0;
	}

	// 组选5
	public function dxwf5z5($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		sort($data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] > 4) return 0;
		$bet0 = str_split($bet[0]);
		$bet1 = str_split($bet[1]);
		$val = "";
		for($i=0;$i<count($data)-3;$i++) {
			if($data[$i] == $data[$i+1] && $data[$i+1] == $data[$i+2] && $data[$i+2] == $data[$i+3]) {
				$val .= $data[$i];
				array_splice($data,$i,4);
				break;
			}
		}
		if($this->sames($bet0, str_split($val)) == 1) {
			return ($this->sames($bet1, $data) == 1) ? 1 : 0;
		}
		return 0;
	}

	//前4复式
	public function dxwfQ4f($bet, $data) {
		return $this->fs($bet, $this->removeFromList($data,[5]));
	}

	// 前4单式
	public function dxwfQ4d($bet, $data) {
		return $this->ds($bet, $this->removeFromList($data,[5]));
	}

	//后4复式
	public function dxwfH4f($bet, $data) {
		return $this->fs($bet, $this->removeFromList($data,[1]));
	}

	// 后4单式
	public function dxwfH4d($bet, $data) {
		return $this->ds($bet, $this->removeFromList($data,[1]));
	}

	//任选4复式
	public function dxwfR4f($bet, $data) {
		//return $this->fs($bet, $this->replaceList($data, $bet) );
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		$betCount = 0;
		for($i=0; $i<count($bet); $i++)
		{
			if($bet[$i] != '-')
			{
				for($j=$i+1; $j < count($bet); $j++)
				{
					for($k=$j+1; $k < count($bet); $k++)
					{
						for($l=$k+1; $l < count($bet); $l++)
						{
							if($bet[$j] != '-' && $bet[$k] != '-' && $bet[$l] != '-')
							{
								$betCount += $this->fs($bet[$i].",".$bet[$j].",".$bet[$k].",".$bet[$l],$data[$i].",".$data[$j].",".$data[$k].",".$data[$l]);
							}	
						}
					}
				}	
			}
		}
		return $betCount;
	}

	// 任选4单式
	public function dxwfR4d($bet, $data) {
		return $this->ds($bet, $this->replaceList($data, $bet) );
	}

	// 组选24
	public function dxwf4z24($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		unset($data[0]);
		sort($data);
		if (count($data) != count(array_unique($data))) return 0;
		return $this->sames($bet, $data) == 4 ? 1 : 0;
	}

	// 组选12
	public function dxwf4z12($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		unset($data[0]);
		sort($data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] >= 3) return 0;
		$bet0 = str_split($bet[0]);
		$bet1 = str_split($bet[1]);
		$val = "";
		for($i=0;$i<count($data)-1;$i++) {
			if($data[$i] == $data[$i+1]) {
				$val .= $data[$i];
				array_splice($data,$i,2);
				break;
			}
		}
		if($this->sames($bet0, str_split($val)) == 1) {
			return ($this->sames($bet1,$data) == 2) ? 1 : 0;
		}
		return 0;
	}

	// 组选6
	public function dxwf4z6($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		unset($data[0]);
		sort($data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] >= 3) return 0;
		$val1 = "";
		$val2 = "";
		for($i=0;$i<count($data)-1;$i++) {
			if($data[$i] == $data[$i+1]) {
				$val1 .= $data[$i];
				array_splice($data,$i,2);
				break;
			}
		}
		for($i=0;$i<count($data)-1;$i++) {
			if($data[$i] == $data[$i+1]) {
				$val2 .= $data[$i];
				break;
			}
		}
		if($this->sames($bet, str_split($val1)) == 1) {
			return ($this->sames($bet, str_split($val2)) == 1) ? 1 : 0;
		}
		return 0;

	}

	// 组选4
	public function dxwf4z4($bet, $data) {
		$bet = explode(',',$bet);
		$data = explode(',',$data);
		unset($data[0]);
		sort($data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] > 3) return 0;
		$bet0 = str_split($bet[0]);
		$bet1 = str_split($bet[1]);
		$val = "";
		for($i=0;$i<count($data)-2;$i++) {
			if($data[$i] == $data[$i+1] && $data[$i+1] == $data[$i+2]) {
				$val .= $data[$i];
				array_splice($data,$i,3);
				break;
			}
		}

		if($this->sames($bet0, str_split($val)) == 1) {
			return ($this->sames($bet1, $data) == 1) ? 1 : 0;
		}
		return 0;

	}

	//前三复式
	public function sxwfQ3f($bet, $data) {
		return $this->fs($bet, $this->removeFromList($data,[4,5]));
	}

	// 前三单式
	public function sxwfQ3d($bet, $data) {
		return $this->ds($bet, $this->removeFromList($data,[4,5]));
	}

	//中三复式
	public function sxwfz3fs($bet, $data) {
		return $this->fs($bet, $this->removeFromList($data,[1,5]));
	}

	// 中三单式
	public function sxwfz3ds($bet, $data) {
		return $this->ds($bet, $this->removeFromList($data,[1,5]));
	}
	//后三复式
	public function sxwfH3f($bet, $data) {
		return $this->fs($bet, $this->removeFromList($data,[1,2]));
	}

	// 后三单式
	public function sxwfH3d($bet, $data) {
		return $this->ds($bet, $this->removeFromList($data,[1,2]));
	}

	//任选3复式
	public function sxwfR3f($bet, $data) {
		//return $this->fs($bet, $this->replaceList($data, $bet) );
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		$betCount = 0;
		for($i=0; $i<count($bet); $i++)
		{
			if($bet[$i] != '-')
			{
				for($j=$i+1; $j < count($bet); $j++)
				{
					for($k=$j+1; $k < count($bet); $k++)
					{
						if($bet[$j] != '-' && $bet[$k] != '-')
						{
							$betCount += $this->fs($bet[$i].",".$bet[$j].",".$bet[$k],$data[$i].",".$data[$j].",".$data[$k]);
						}	
					}
				}	
			}
		}
		return $betCount;
	}

	// 任选3单式
	public function sxwfR3d($bet, $data) {
		return $this->ds($bet, $this->replaceList($data, $bet) );
	}

	// 后三和值尾数
	public function sxh3hzws($bet, $data) {
		$bet = explode(' ', $bet);
		$data = explode(',', $data);
		$val = array_sum(array_splice($data, 2, 3));
		$val > 10 and $val = $val % 10;
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $val) return 1;
		}
		return 0;
	}

	// 前三组三
	public function sxzxQ3z3($bet, $data) {
		return $this->z3($bet, substr($data,0,5));
	}

	// 前三组六
	public function sxzxQ3z6($bet, $data) {

		return $this->z6($bet, substr($data,0,5));
	}

	// 中三组三
	public function sxzxz3z3($bet, $data) {
		return $this->z3($bet, substr($data,2,5));
	}

	// 中三组六
	public function sxzxz3z6($bet, $data) {
		return $this->z6($bet, substr($data,2,5));
	}

	// 后三组三
	public function sxzxH3z3($bet, $data) {
		return $this->z3($bet, substr($data,4,5));
	}

	// 后三组六
	public function sxzxH3z6($bet, $data) {
		return $this->z6($bet, substr($data,4,5));
	}

	// 中三混合组选(沒做)
	//public function sxzxZ3h($bet, $data) {
	//}
	// 前三混合组选(沒做)
	//public function sxzxQ3h($bet, $data) {
	//}
	// 后三混合组选(沒做)
	//public function sxzxH3h($bet, $data) {
	//}

	// 任三组三
	public function sxzxR3z3($bet, $data, $w = 0) {
		$data = explode(',', $data);
		foreach ([16, 8, 4, 2, 1] as $i => $val){
			if (($w & $val) == 0) unset($data[$i]);
		}
		$data = implode(',', $data);
		return $this->z3($bet, $data);
	}

	// 任三组六
	public function sxzxR3z6($bet, $data, $w = 0) {
		$data = explode(',', $data);
		foreach ([16, 8, 4, 2, 1] as $i => $val){
			if (($w & $val) == 0) unset($data[$i]);
		}
		$data = implode(',', $data);
		return $this->z6($bet, $data);
	}

	// 任三混合组(沒做)
	//public function sxzxR3h($bet, $data, $w = 0) {
	//}

	// 后三组选和值
	public function sxzxH3hz($bet, $data) {
		$bet = explode(',', $bet);
		$data = explode(',', substr($data, 4, 5));
		$group = array_count_values($data);
		rsort($group);
		if($group[0] >= 3) return 0;
		$sum = array_sum($data);
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $sum) {
				return $this->isRepeat($data) ? 2 : 1;
			}
		}
		return 0;
	}

	// 后三特殊号码
	public function sxzxH3ts($bet, $data) {
		$bet = explode(',', $bet);
		$data = explode(',', substr($data, 4, 5));
		sort($data);
		for($i=0;$i<count($data)-1;$i++) {
			if($i < count($data)-2) {
				if($data[$i] == ($data[$i+1]-1) && $data[$i+1] == ($data[$i+2]-1)) {
					return array_search('顺子', $bet) !== false ? 4 : 0;
				}
				if($data[$i] == $data[$i+1] && $data[$i+1] == $data[$i+2]) {
					return array_search('豹子', $bet) !== false ? 27 : 0;
				}
			}
			if($data[$i] == $data[$i+1]) {
				return array_search('对子', $bet) !== false ? 1 : 0;
			}
		}
		return 0;
	}


	// 后三直选跨度
	public function sxzxH3kd($bet, $data) {
		$bet = explode(',', $bet);
		$data = explode(',', substr($data, 4, 5));
		sort($data);
		$val = intval($data[2] - $data[0]);
		return array_search($val,$bet) !== false ? 1 : 0;
	}

	/* 二星直选 */
	// 前二复式
	public function rxwfQ2f($bet, $data) {
		return $this->fs($bet, $this->getFromList($data, [1,2]) );
	}
	// 前二单式
	public function rxwfQ2d($bet, $data) {
		return $this->ds($bet, $this->getFromList($data, [1,2]) );
	}

	// 后二复式
	public function rxwfH2f($bet, $data) {
		return $this->fs($bet, $this->getFromList($data, [4,5]) );
	}
	// 后二单式
	public function rxwfH2d($bet, $data) {
		return $this->ds($bet, $this->getFromList($data, [4,5]) );
	}

	// 前二和值
	public function sscq2zhixhz($bet, $data) {
		$val = 0;
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		$sum = intval($data[0] + $data[1]);
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $sum) $val += 1;
		}
		return $val;
	}

	// 后二和值
	public function ssch2zhixhz($bet, $data) {
		$val = 0;
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		$sum = intval($data[3] + $data[4]);
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $sum) $val += 1;
		}
		return $val;
	}

	// 前二组选和值
	public function sscq2zhuxhz($bet, $data) {
		$val = 0;
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		if($data[0] == $data[1]) return 0;

		$sum = intval($data[0] + $data[1]);
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $sum) $val += 1;
		}
		return $val;
	}

	// 后二组选和值
	public function ssch2zhuxhz($bet, $data) {
		$val = 0;
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		if($data[3] == $data[4]) return 0;
		$sum = intval($data[3] + $data[4]);
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $sum) $val += 1;
		}
		return $val;
	}

	// 任选二复式
	public function rxwfR2f($bet, $data) {
		//return $this->sxwfR3f($bet, $data);
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		$betCount = 0;
		for($i=0; $i<count($bet); $i++)
		{
			if($bet[$i] != '-')
			{
				for($j=$i+1; $j < count($bet); $j++)
				{
					if($bet[$j] != '-')
					{
						$betCount += $this->fs($bet[$i].",".$bet[$j],$data[$i].",".$data[$j]);
					}
				}	
			}
		}
		return $betCount;
	}
	// 任选二单式
	public function rxwfR2d($bet, $data) {
		return $this->sxwfR3d($bet, $data);
	}

	// 前二组复式
	public function rxzxQ2f($bet, $data) {
		return $this->z2f($bet, substr($data,0,3));
	}
	// 前二组单式
	public function rxzxQ2d($bet, $data) {
		return $this->z2d($bet, substr($data,0,3));
	}

	// 后二组复式
	public function rxzxH2f($bet, $data) {
		return $this->z2f($bet, substr($data,6,3));
	}
	// 后二组单式
	public function rxzxH2d($bet, $data) {
		return $this->z2d($bet, substr($data,6,3));
	}

	// 任选二组选复式
	public function rxzxR2f($bet, $data, $w = 0) {
		$data = explode(',', $data);
		foreach ([16, 8, 4, 2, 1] as $i => $val){
			if (($w & $val) == 0) unset($data[$i]);
		}
		$data = implode(',', $data);
		return $this->z2f($bet, $data);
	}

	// 任选二组选单式
	public function rxzxR2d($bet, $data, $w = 0) {
		$data = explode(',', $data);
		foreach ([16, 8, 4, 2, 1] as $i => $val){
			if (($w & $val) == 0) unset($data[$i]);
		}
		$data = implode(',', $data);

		$bet = explode('|', $bet);

		for($i=0;$i<count($bet);$i++) {
			$subBet = explode(',',$bet[$i]);
			foreach ([16, 8, 4, 2, 1] as $j => $val){
				if (($w & $val) == 0) unset($subBet[$j]);
			}
			$bet[$i] = implode(',', $subBet);
		}
		$bet = implode('|', $bet);
		return $this->z2d($bet, $data);
	}

	// 后三不定胆
	public function bddH3($bet, $data) {
		$data = substr($data,4,5);
		$bet = array_filter(
			str_split($bet),
			function ($val) use ($data) {
				return strpos($data, $val) !== false;
			}
		);
		return count($bet);
	}

	// 前三不定胆
	public function bddQ3($bet, $data) {
		$data = substr($data,0,5);
		$bet = array_filter(
			str_split($bet),
			function ($val) use ($data) {
				return strpos($data, $val) !== false;
			}
		);
		return count($bet);
	}

	// 中三一码不定胆
	public function bddZ3($bet, $data) {
		$data = substr($data,2,5);
		$bet = array_filter(
			str_split($bet),
			function ($val) use ($data) {
				return strpos($data, $val) !== false;
			}
		);
		return count($bet);
	}

	// 任选三不定胆
	public function bddR3($bet, $data, $w = 0) {
		$data = explode(',', $data);
		foreach ([16, 8, 4, 2, 1] as $i => $val){
			if (($w & $val) == 0) unset($data[$i]);
		}
		$data = implode(',', $data);
		$bet = array_filter(
			str_split($bet),
			function ($val) use ($data) {
				return strpos($data, $val) !== false;
			}
		);
		return count($bet);
	}

	// 前三二码  二码不定位 (不確定)
	public function bdwQ32($bet, $data) {
		$data = substr($data,0,5);
		$bet = array_filter(
			$this->combine(array_unique(explode(' ',$bet)),2),
			function ($array) use ($data) {
				for($i=0;$i<count($array);$i++) {
					if(strpos($data, $array[$i]) === false) return false;
				}
				return true;
			}
		);
		return count($bet);

		/*
		$data = array_values(array_unique(explode(',', substr($data,0,5))));
		$bet = explode(' ', $bet);
		if(count($bet) < count($data)) {
			if ($this->sames($data,$bet) >= 2) return (int)$this->combination($this->sames($data,$bet),2);
		}
		else {
			if ($this->sames($bet,$data) >= 2) return (int)$this->combination($this->sames($bet,$data),2);
		}
		return 0;*/
	}

	// 后三二码 (不確定)
	public function bdwH32($bet, $data) {
		$data = substr($data,4,5);
		$bet = array_filter(
			$this->combine(array_unique(explode(' ',$bet)),2),
			function ($array) use ($data) {
				for($i=0;$i<count($array);$i++) {
					if(strpos($data, $array[$i]) === false) return false;
				}
				return true;
			}
		);
		return count($bet);

/*
		$data = array_values(array_unique(explode(',', substr($data,4,5))));
		$bet = explode(' ', $bet);
		if(count($bet) < count($data)) {
			if ($this->sames($data,$bet) >= 2) return (int)$this->combination($this->sames($data,$bet),2);
		}
		else {
			if ($this->sames($bet,$data) >= 2) return (int)$this->combination($this->sames($bet,$data),2);
		}
		return 0;
*/
	}

	// 五星三码 (不確定)
	public function bdw5x3m($bet, $data) {
		$bet = array_filter(
			$this->combine(array_unique(explode(' ',$bet)),3),
			function ($array) use ($data) {
				for($i=0;$i<count($array);$i++) {
					if(strpos($data, $array[$i]) === false) return false;
				}
				return true;
			}
		);
		return count($bet);
/*
		$data = array_values(array_unique(explode(',', $data)));
		$bet = explode(' ', $bet);
		if(count($bet) < count($data)) {
			if ($this->sames($data,$bet) >= 3) return (int)$this->combination($this->sames($data,$bet),3);
		}
		else {
			if ($this->sames($bet,$data) >= 3) return (int)$this->combination($this->sames($bet,$data),3);
		}
		return 0;
*/
	}

	// 五星二码 (不確定)
	public function bdw5x2m($bet, $data) {
		$bet = array_filter(
			$this->combine(array_unique(explode(' ',$bet)),2),
			function ($array) use ($data) {
				for($i=0;$i<count($array);$i++) {
					if(strpos($data, $array[$i]) === false) return false;
				}
				return true;
			}
		);
		return count($bet);
/*
		$data = array_values(array_unique(explode(',', $data)));
		$bet = explode(' ', $bet);
		if(count($bet) < count($data)) {
			if ($this->sames($data,$bet) >= 2) return (int)$this->combination($this->sames($data,$bet),2);
		}
		else {
			if ($this->sames($bet,$data) >= 2) return (int)$this->combination($this->sames($bet,$data),2);
		}
		return 0;
*/
	}

	// 四星二码 (不確定)
	public function bdw4x2m($bet, $data) {
		$data = substr($data,2,7);
		$bet = array_filter(
			$this->combine(array_unique(explode(' ',$bet)),2),
			function ($array) use ($data) {
				for($i=0;$i<count($array);$i++) {
					if(strpos($data, $array[$i]) === false) return false;
				}
				return true;
			}
		);
		return count($bet);
		/*
		$data = array_values(array_unique(explode(',', substr($data,2,7))));
		$bet = explode(' ', $bet);
		if(count($bet) < count($data)) {
			if ($this->sames($data,$bet) >= 2) return (int)$this->combination($this->sames($data,$bet),2);
		}
		else {
			if ($this->sames($bet,$data) >= 2) return (int)$this->combination($this->sames($bet,$data),2);
		}
		return 0;*/
	}

	// 四星一码 (不確定)
	public function bdw4x1m($bet, $data) {
		$data = substr($data,2,7);
		$bet = array_filter(
			$this->combine(array_unique(explode(' ',$bet)),1),
			function ($array) use ($data) {
				for($i=0;$i<count($array);$i++) {
					if(strpos($data, $array[$i]) === false) return false;
				}
				return true;
			}
		);
		return count($bet);
		/*
		$data = array_values(array_unique(explode(',', substr($data,2,7))));
		$bet = explode(' ', $bet);
		if(count($bet) < count($data)) {
			if ($this->sames($data,$bet) >= 1) return $this->sames($data,$bet);
		}
		else {
			if ($this->sames($bet,$data) >= 1) return $this->sames($bet,$data);
		}
		return 0;*/
	}

	// 前二大小单双
	public function dsQ2($bet, $data) {
		return $this->dxds($bet, substr($data,0,3));
	}
	// 前三大小单双
	public function dsQ3($bet, $data) {
		return $this->dxds($bet, substr($data,0,5));
	}

	// 后二大小单双
	public function dsH2($bet, $data) {
		return $this->dxds($bet, substr($data,6,3));
	}
	// 后三大小单双
	public function dsH3($bet, $data) {
		return $this->dxds($bet, substr($data,4,5));
	}

	// 任选二大小单双
	public function dsR2($bet, $data, $w=0) {
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == '-') {
				unset($bet[$i],$data[$i]);
			}
		}
		$bet = implode(',',$bet);
		$data = implode(',',$data);
		return $this->dxds($bet, $data);
	}

	// 趣味玩法 一帆风顺 (node應該是寫錯的)
	public function qwwfyffs($bet, $data) {
		$data = array_unique(explode(',', $data));
		sort($data);
		$bet = explode(' ', $bet);
		if(count($data) >= count($bet)) {
			$val = $this->sames($data, $bet);
			return $val >= 1 ? $val : 0;
		}
		else {
			$val = $this->sames($bet, $data);
			return $val >= 1 ? $val : 0;
		}
	}

	// 趣味玩法 好事成双
	public function qwwfhscs($bet, $data) {
		$data = explode(',', $data);
		sort($data);
		$bet = explode(' ', $bet);
		$val = '';
		for($i=0;$i<count($data)-1;$i++) {
			if($data[$i] == $data[$i+1]) {
				$val .= $data[$i];
			}
		}
		return $this->sames($bet, str_split($val)) >= 1 ? 1 : 0;

	}

	// 趣味玩法 三星报喜
	public function qwwfsxbx($bet, $data) {
		$data = explode(',', $data);
		sort($data);
		$bet = explode(' ', $bet);
		$val = '';
		for($i=0;$i<count($data)-2;$i++) {
			if($data[$i] == $data[$i+1] && $data[$i+1] == $data[$i+2]) {
				$val .= $data[$i];
				break;
			}
		}
		return $this->sames($bet, str_split($val)) == 1 ? 1 : 0;
	}

	// 趣味玩法 四季发财
	public function qwwfsjfc($bet, $data) {
		$data = explode(',', $data);
		sort($data);
		$bet = explode(' ', $bet);
		$val = '';
		for($i=0;$i<count($data)-3;$i++) {
			if($data[$i] == $data[$i+1] && $data[$i+1] == $data[$i+2] && $data[$i+2] == $data[$i+3]) {
				$val .= $data[$i];
				break;
			}
		}
		return $this->sames($bet, str_split($val)) == 1 ? 1 : 0;
	}

	// 趣味玩法 前三趣味二星
	public function qwwfq3qw2x($bet, $data) {
		$data = explode(',', substr($data,0,5));
		$bet = explode(',', $bet);

		if(strpos($bet[1], $data[1]) !== false) {
			if(strpos($bet[2], $data[2]) !== false) {
				if($data[0] < 5) {
					return strpos($bet[0], '小') !== false ? 2 : 1;
				}
				else{
					return strpos($bet[0], '大') !== false ? 2 : 1;
				}
			}
		}
		return 0;
	}

	// 趣味玩法 后三趣味二星
	public function qwwfh3qw2x($bet, $data) {
		$data = explode(',', substr($data,4,5));
		$bet = explode(',', $bet);

		if(strpos($bet[1], $data[1]) !== false) {
			if(strpos($bet[2], $data[2]) !== false) {
				if($data[0] < 5) {
					return strpos($bet[0], '小') !== false ? 2 : 1;
				}
				else{
					return strpos($bet[0], '大') !== false ? 2 : 1;
				}
			}
		}
		return 0;
	}

	// 趣味玩法 四码趣味三星
	public function qwwf4mqw3x($bet, $data) {
		$data = explode(',', substr($data,2,7));
		$bet = explode(',', $bet);

		if(strpos($bet[1], $data[1]) !== false) {
			if(strpos($bet[2], $data[2]) !== false) {
				if(strpos($bet[3], $data[3]) !== false) {
					if($data[0] < 5) {
						return strpos($bet[0], '小') !== false ? 2 : 1;
					}
					else{
						return strpos($bet[0], '大') !== false ? 2 : 1;
					}
				}
			}
		}
		return 0;
	}

	// 趣味玩法 五码趣味三星
	public function qwwf5mqw3x($bet, $data) {
		$data = explode(',', $data);
		$bet = explode(',', $bet);

		if(strpos($bet[2], $data[2]) !== false) {
			if(strpos($bet[3], $data[3]) !== false) {
				if(strpos($bet[4], $data[4]) !== false) {
					if($data[0] < 5 && $data[1] < 5) {
						return (strpos($bet[0], '小') && strpos($bet[1], '小')) ? 8 : 1;
					}
					if($data[0] >= 5 && $data[1] >= 5) {
						return (strpos($bet[0], '大') && strpos($bet[1], '大')) ? 8 : 1;
					}
					if($data[0] >= 5 && $data[1] < 5) {
						return (strpos($bet[0], '大') && strpos($bet[1], '小')) ? 8 : 1;
					}
					if($data[0] < 5 && $data[1] >= 5) {
						return (strpos($bet[0], '小') && strpos($bet[1], '大')) ? 8 : 1;
					}
				}
			}
		}
		return 0;
	}
/*
	// 时时彩整合
	public function ssczh($bet, $data, $w = 0) {		
		 // 萬:16,千:8,百:4,十:2,個:1
		$count = 0;
		$data = explode(',', $data);
		$val='';
		$valarr = array();
		switch(true)
		{
			case $w==16:
				$val=intval($data[0]);
				break;
			case $w==8:
				$val=intval($data[1]);
				break;
			case $w==4:
				$val=intval($data[2]);
				break;
			case $w==2:
				$val=intval($data[3]);
				break;
			case $w==1:
				$val=intval($data[4]);
				break;
			case $w==28:
				$valarr = [intval($data[0]), intval($data[1]), intval($data[2])];
				break;
			case $w==14:
				$valarr = [intval($data[1]), intval($data[2]), intval($data[3])];
				break;
			case $w==7:
				$valarr = [intval($data[2]), intval($data[3]), intval($data[4])];
				break;
		}
		switch(true)
		{
			case $bet==strval($val):
				$count=1;
				break;
			case $bet=='大':
				if($val>=5 && $val<=9) $count=1;
				break;
			case $bet=='小':
				if($val>=0 && $val<=4) $count=1;
				break;
			case $bet=='单':
				if($val%2!=0) $count=1;
				break;
			case $bet=='双':
				if($val%2==0) $count=1;
				break;
			case $bet=='豹子':
				if($this->isSameNum($valarr)) $count=1;
				break;
			case $bet=='对子':
				if($this->isPair($valarr) && !$this->isSameNum($valarr)) $count=1;
				break;
			case $bet=='顺子':
				if($this->isStraight($valarr) && !$this->isSameNum($valarr) && !$this->isPair($valarr)) $count=1;
				break;
			case $bet=='半顺':
				if($this->isHalfStraight($valarr) && !$this->isStraight($valarr) && !$this->isSameNum($valarr) && !$this->isPair($valarr)) $count=1;
				break;
			case $bet=='杂六':
				if(count($valarr)==3 && !$this->isHalfStraight($valarr) && !$this->isStraight($valarr) && !$this->isSameNum($valarr) && !$this->isPair($valarr)) $count=1;
				break;
		}
		return $count;
	}
*/

	// 总和大小单双
	public function ssczh($actionData, $openCode){
		$codes = explode(',', $openCode);
		$sum = array_sum($codes);
		$val = "";
		if($sum >= 23)
			$val .= "总和大";
		else
			$val .= "总和小";
		if($sum % 2 == 1)
			$val .= "总和单";
		else
			$val .= "总和双";
		
		$count = 0;
		$datas = explode(',', $actionData);
		foreach($datas as $data){
			if(strrpos($val, $data) !== false) $count++;
		}
		return $count;
	}
	//第一球
	public function sscd1b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[0]);
	}
	//第二球
	public function sscd2b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[1]);
	}
	//第三球
	public function sscd3b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[2]);
	}
	//第四球
	public function sscd4b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[3]);
	}
	//第五球
	public function sscd5b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->sscdnb($actionData, $code[4]);
	}
	//前三
	public function sscq3($actionData, $openCode) {
		$codes = explode(',', $openCode);
		array_splice($codes,3,2);
		return $this->sscr3($actionData, $codes,3,2);
	}
	//中三
	public function sscz3($actionData, $openCode) {
		$codes = explode(',', $openCode);
		array_shift($codes);
		array_pop($codes);
		return $this->sscr3($actionData, $codes);
	}
	//后三
	public function ssch3($actionData, $openCode) {
		$codes = explode(',', $openCode);
		array_splice($codes,0,2);
		return $this->sscr3($actionData, $codes);
	}

	//第N球
	public function sscdnb($actionData, $openCode) {
		$count = 0;
		if(is_numeric($actionData)) {
			if($actionData == $openCode)$count = 1;
		} else {
			$val = '';
			if($openCode >= 5) 
				$val .= '大';
			else 
				$val .= '小';
			
			if($openCode % 2 == 1)
				$val .= "單";
			else
				$val .= "雙";
			//print_r("val :".$val.",actionData:".$actionData);
			if(strrpos($val, $actionData) !== false) $count = 1;
		}
		return $count;
	}
	//前三/中三/后三共用方法
	public function sscr3($actionData, $openCode) {
		$count = 0;
		switch ($actionData) {
			case "豹子":
				if($this->isSameNum($openCode)) $count=1;
				break;
			case "對子":
				if($this->isPair($openCode) && !$this->isSameNum($openCode)) $count=1;
				break;
			case "順子":
				if($this->isStraight($openCode) && 
					!$this->isSameNum($openCode) && !$this->isPair($openCode)) $count=1;
				break;
			case "半順":
				if($this->isHalfStraight($openCode) && !$this->isStraight($openCode) 
					&& !$this->isSameNum($openCode) && !$this->isPair($openCode)) $count=1;
				break;
			case "雜六":
				if(count($openCode)==3 && !$this->isHalfStraight($openCode) && !$this->isStraight($openCode) 
					&& !$this->isSameNum($openCode) && !$this->isPair($openCode)) $count=1;
				break;
			default:
				break;
		}
		return $count;
	}
		
	/**
	 * 是否为豹子
	 * 
	 * @param nums
	 * @returns {Boolean}
	 */
	public function isSameNum($nums) {
		$num1 = $nums[0];
		for ($i = 1; $i < count($nums); $i++) {
			if ($num1 != $nums[$i]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 是否对子
	 * 
	 * @param nums
	 * @returns {Boolean}
	 */
	public function isPair($nums) {
		for ($i = 0; $i < count($nums); $i++) {
			$code = $nums[$i];
			for ($j = $i + 1; $j < count($nums); $j++) {
				$codeTemp = $nums[$j];
				if ($code == $codeTemp) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * 判断是否顺子
	 * 
	 * @param nums
	 * @return
	 */
	public function isStraight($nums) {
		sort($nums);
		if ($nums[0] == 0 && $nums[1] == 1 && $nums[2] == 9) {// 019也算顺子
			return true;
		}
		if ($nums[0] == 0 && $nums[1] == 8 && $nums[2] == 9) {// 089也算顺子
			return true;
		}
		for ($i = 0; $i < count($nums) - 1; $i++) {
			$num1 = $nums[$i];
			$num2 = $nums[$i + 1];
			if ($num2 - $num1 != 1) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 判断是否是半顺
	 */
	public function isHalfStraight($nums) {
		sort($nums);
		$n = 0;
		if ($nums[0] == 0) {
			$n += 1;
		}
		if ($nums[1] == 1) {
			$n += 1;
		}
		if ($nums[2] == 9) {
			$n += 1;
		}
		if ($n == 2) {
			return true;
		}

		for ($i = 0; $i < count($nums) - 1; $i++) {
			$num1 = $nums[$i];
			$num2 = $nums[$i + 1];
			if ($num2 - $num1 == 1) {
				return true;
			}
		}
		return false;
	}

	// 总和值大小单双
	public function ssczhdxds($bet, $data) {
		$data = explode(',', $data);
		$val=$data[0]+$data[1]+$data[2]+$data[3]+$data[4];
		switch(true)
		{
			case $bet==="大" && $val>=23:
				return 1;
				break;
			case $bet==="小" && $val<=22:
				return 1;
				break;
			case $bet==="双" && $val%2==0:
				return 1;
				break;
			case $bet==="单" && $val%2==1:
				return 1;
				break;
			default:
				return 0;
				break;
		}
		return 0;
	}
	/*
	// 龙虎和
	public function ssclhhz($bet, $data) {
		$data = explode(',', $data);
		$val=$data[0]+$data[1]+$data[2]+$data[3]+$data[4];
		switch(true)
		{
			case $bet==="龙" && $data[0]>$data[4]:
				return 1;
				break;
			case $bet==="虎" && $data[0]<$data[4]:
				return 1;
				break;
			case $bet==="和" && $data[0]==$data[4]:
				return 1;
				break;
			default:
				return 0;
				break;
		}
		return 0;
	}
	*/
	// 牛1-9
	public function nini($bet, $data) {
		$val = explode(',', $data);
		$c=$this->check_nini($val);
		if(count($c)==0) return 0;
		$v=$this->re2num($val,$c);
		$v2=($v[0]+$v[1])%10; 
		switch(true)
		{
			case $bet==="牛1" && $v2==1:
				return 1;
				break;
			case $bet==="牛2" && $v2==2:
				return 1;
				break;
			case $bet==="牛3" && $v2==3:
				return 1;
				break;
			case $bet==="牛4" && $v2==4:
				return 1;
				break;
			case $bet==="牛5" && $v2==5:
				return 1;
				break;
			case $bet==="牛6" && $v2==6:
				return 1;
				break;
			case $bet==="牛7" && $v2==7:
				return 1;
				break;
			case $bet==="牛8" && $v2==8:
				return 1;
				break;
			case $bet==="牛9" && $v2==9:
				return 1;
				break;
			default:
				return 0;
				break;
		}
		return 0;
	}
	// 没牛
	public function nini_not($bet, $data) {
		$val = explode(',', $data);
		$c=$this->check_nini($val);
		if(count($c)==0 && $bet==="没牛")
		{
			return 1;
		}
		return 0;
	}	
	// 牛牛
	public function nini_is($bet, $data) {
		$val = explode(',', $data);
		$c=$this->check_nini($val);
		if(count($c)==0) return 0;
		$v=$this->re2num($val,$c);
		$v2=($v[0]+$v[1])%10; 
		if($v2==0 && $bet==="牛牛")
		{
			return 1;
		}
		return 0;
	}	
	// 牛大小单双
	public function nidxds($bet, $data) {
		$val = explode(',', $data);
		$c=$this->check_nini($val);
		if(count($c)==0) return 0;
		$v=$this->re2num($val,$c);
		$v2=($v[0]+$v[1])%10; 
		switch(true)
		{
			case $bet==="牛大" && in_array($v2,array(6,7,8,9,0)):
				return 1;
				break;
			case $bet==="牛小" && in_array($v2,array(1,2,3,4,5)):
				return 1;
				break;
			case $bet==="牛单" && in_array($v2,array(1,3,5,7,9)):
				return 1;
				break;
			case $bet==="牛双" && in_array($v2,array(2,4,6,8,0)):
				return 1;
				break;
			default:
				return 0;
				break;
		}
		return 0;
	}	
	public function check_nini($val) {
		foreach ($val as $k => $v) {
			array_shift($val);
			if(count($val)<=1)break;
			$val2=$val;
			foreach ($val as $k1 => $v1) {
				array_shift($val2);
				foreach($val2 as $k2=>$v2)
				{
					if(($v+$v1+$v2)%10===0)
					{
						return array($v,$v1,$v2);
					}
				}
			}
		}
		return array();
	}
	public function re2num($a,$b) {
		$key = array_search($b[0], $a);
		array_splice($a, $key, 1);
		$key = array_search($b[1], $a);
		array_splice($a, $key, 1);
		$key = array_search($b[2], $a);
		array_splice($a, $key, 1);
		return $a;
	}	

#======================================================================================================================================
# 福彩3D
#======================================================================================================================================
	// 三星直选－复式
	public function fc3dFs($bet, $data) {
		return $this->fs($bet, $data);
	}

	// 三星直选－单式
	public function fc3dDs($bet, $data) {
		return $this->ds($bet, $data);
	}

	// 三星直选和值
	public function fc3dhz($bet, $data) {
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		$val = $data[0] + $data[1] + $data[2];
		return array_search($val, $bet) !== false ? 1 : 0;
	}

	// 三星组选－组三
	public function fc3dZ3($bet, $data) {
		return $this->z3($bet, $data);
	}
	// 三星组选－组六
	public function fc3dZ6($bet, $data) {
		return $this->z6($bet, $data);
	}

	// 三星组选和值
	public function fc3d_zxhz($bet, $data) {
		$bet = explode(',', $bet);
		if(preg_match('/(\d),\\1,\\1/',$data)) return 0;
		$data = explode(',', $data);
		$val = $data[0] + $data[1] + $data[2];
		if (array_search($val, $bet) !== false) {
			return $this->isRepeat($data) ? 2 : 1;
		}
		return 0;
	}

	// 二星直选－前二单式
	public function fc3dQ2d($bet, $data) {
		return $this->rxwfQ2d($bet, $data);
	}

	// 二星直选－前二复式
	public function fc3dQ2f($bet, $data) {
		return $this->rxwfQ2f($bet, $data);
	}

	// 二星直选－后二单式
	public function fc3dH2d($bet, $data) {
		return $this->ds($bet, substr($data,2,5));
	}

	// 二星直选－后二复式
	public function fc3dH2f($bet, $data) {
		return $this->fs($bet, substr($data,2,5));
	}

	// 二星组选－前二组选单式
	public function fc3dZQ2d($bet, $data) {
		return $this->rxzxQ2d($bet, $data);
	}

	// 二星组选－前二组选复式
	public function fc3dZQ2f($bet, $data) {
		return $this->rxzxQ2f($bet, $data);
	}

	// 二星组选－后二组选单式
	public function fc3dZH2d($bet, $data) {
		return $this->z2d($bet, substr($data,2,5));
	}

	// 二星组选－后二组选复式
	public function fc3dZH2f($bet, $data) {
		return $this->z2f($bet, substr($data,2,5));
	}

	// 三星定位胆
	public function fc3d3xdw($bet, $data) {
		return $this->dwd5x($bet, $data);
	}

	// 不定胆
	public function fc3dbdd($bet, $data) {
		return $this->bddQ3($bet, $data);
	}

	// 后二大小单双
	public function fc3dH2dxds($bet, $data) {
		return $this->dxds($bet, substr($data,2,3));
	}

	// 任选二大小单双
	public function fc3dR2dxds($bet, $data, $w = 0) {
		// print_r("c");
		// $data = explode(',', $data);
		// foreach ([4, 2, 1] as $i => $val){
		// 	if (($w & $val) == 0) unset($data[$i]);
		// }
		// $data = implode(',', $data);
		//return $this->dxds($bet, $data);
		//return $this->dxds("双,单双", "6,6");
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		$betCount = 0;
		for($i=0; $i<count($bet); $i++)
		{
			if($bet[$i] != '-')
			{
				for($j=$i+1; $j < count($bet); $j++)
				{
					if($bet[$j] != '-')
					{
						$betCount += $this->dxds($bet[$i].",".$bet[$j],$data[$i].",".$data[$j]);
					}
				}	
			}
		}
		return $betCount;
	}

#======================================================================================================================================
# 11選5
#======================================================================================================================================
	// 任选一
	public function gd11x5R1($bet, $data) {
		$data = explode(',', $data);
		$bet = explode(' ', $bet);
		return count($bet) > count($data) ? $this->sames($bet, $data) : $this->sames($data, $bet);
	}

	//任选一单式
	public function gd11x5R1ds($bet, $data) {
		$data = explode(',', $data);
		return array_search($bet,$data) !== false ? 1 : 0;
	}

	//任选二復式
	public function gd11x5R2($bet, $data) {
		return $this->rx($bet, $data, 2);
	}

	//任选二单式
	public function gd11x5R2ds($bet, $data) {
		$data = explode(',', $data);
		$bet = $this->strCut($bet, 2);
		return $this->sames($data, $bet) == 2 ? 1 : 0;
	}

	//任选三復式
	public function gd11x5R3($bet, $data) {
		return $this->rx($bet, $data, 3);
	}

	//任选三单式
	public function gd11x5R3ds($bet, $data) {
		$data = explode(',', $data);
		$bet = $this->strCut($bet, 2);
		return $this->sames($data, $bet) == 3 ? 1 : 0;
	}

	//任选四復式
	public function gd11x5R4($bet, $data) {
		return $this->rx($bet, $data, 4);
	}

	//任选四单式
	public function gd11x5R4ds($bet, $data) {
		$data = explode(',', $data);
		$bet = $this->strCut($bet, 2);
		return $this->sames($data, $bet) == 4 ? 1 : 0;
	}

	//任选五復式
	public function gd11x5R5($bet, $data) {
		return $this->rx($bet, $data, 5);
	}

	//任选五单式
	public function gd11x5R5ds($bet, $data) {
		$data = explode(',', $data);
		$bet = $this->strCut($bet, 2);
		return $this->sames($data, $bet) == 5 ? 1 : 0;
	}

	//任选六復式
	public function gd11x5R6($bet, $data) {
		return $this->rx($bet, $data, 6);
	}

	//任选六单式
	public function gd11x5R6ds($bet, $data) {
		$data = explode(',', $data);
		$bet = $this->strCut($bet, 2);
		return $this->sames($data, $bet) == 5 ? 1 : 0;
	}

	//任选七復式
	public function gd11x5R7($bet, $data) {
		return $this->rx($bet, $data, 7);
	}

	//任选七单式
	public function gd11x5R7ds($bet, $data) {
		$data = explode(',', $data);
		$bet = $this->strCut($bet, 2);
		return $this->sames($data, $bet) == 5 ? 1 : 0;
	}

	//任选八復式
	public function gd11x5R8($bet, $data) {
		return $this->rx($bet, $data, 8);
	}

	//任选八单式
	public function gd11x5R8ds($bet, $data) {
		$data = explode(',', $data);
		$bet = $this->strCut($bet, 2);
		return $this->sames($data, $bet) == 5 ? 1 : 0;
	}


	//任选九復式
	public function gd11x5R9($bet, $data) {
		return $this->rx($bet, $data, 9);
	}
	//任选十復式
	public function gd11x5R10($bet, $data) {
		return $this->rx($bet, $data, 10);
	}

	//前一直选
	public function gd11x5Q1($bet, $data) {
		$bet = explode(' ', $bet);
		$data = explode(',', $data);

		for($i=0;$i<count($bet);$i++) {
			if($data[0] == $bet[$i]) return 1;
		}
		return 0;
	}


	//前二直选
	public function gd11x5Q2($bet, $data) {
		return $this->qs($bet, $data, 2);
	}
	//前二组选
	public function gd11x5Q2z($bet, $data) {
		return $this->zx($bet, substr($data, 0, 5));
	}
	//前三直选
	public function gd11x5Q3($bet, $data) {
		return $this->qs($bet, $data, 3);
	}
	//前三组选
	public function gd11x5Q3z($bet, $data) {
		return $this->zx($bet, substr($data, 0, 8));
	}

	//前四组选
	public function gd11x5Q4z($bet, $data) {
		return $this->zx($bet, substr($data, 0, 11));
	}

	//定位胆
	public function gd11x5dwd($bet, $data) {
		$bet = explode(',', $bet);
		$data = explode(',', substr($data,0 ,8));
		$val = array();
		if(array_search($data[0], explode(' ', $bet[0])) !== false) $val[] = $data[0];
		if(array_search($data[1], explode(' ', $bet[1])) !== false) $val[] = $data[1];
		if(array_search($data[2], explode(' ', $bet[2])) !== false) $val[] = $data[2];
		return count($val);
	}

	//不定位
	public function gd11x5bdw($bet, $data) {
		$bet = explode(' ', $bet);
		$data = explode(',', substr($data, 0, 8));
		return $this->sames($bet, $data);
	}

	//趣味_猜中位
	public function qwwfczw($bet, $data) {
		$bet = explode(' ', $bet);
		$data = explode(',', $data);
		sort($data);
		$count = 0;;
		for($i=0;$i<count($bet);$i++) {
			if($data[2] == $bet[$i]) {
				$count++;
			}
		}
		return $count;
	}

	//趣味_定单双
	public function qwwfdds($bet, $data) {
		$odd = 0;
		$even = 0;
		$count = 0;
		$data = explode(',', $data);
		$number = preg_replace("/[^0-9]/i","",$bet);
		$bet = $this->strCut($number,2);

		for($i=0;$i<count($data);$i++) {
			$data[$i] % 2 == 0 ? $even++ : $odd++;
		}
		$data = $odd.$even;

		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $data) {
				$count++;
			}
		}
		return $count;
	}

	//总和大小单双尾大尾小 信用玩法begin
	public function s115sh($actionData, $openCode){
		$codes = explode(',', $openCode);
		$sum = array_sum($codes);
		$count = 0;
		$val = "";
		if($sum == 30){
			$count = -1;
			return $count;
		} else if($sum > 30){
			$val .= "总和大";
		} else {
			$val .= "总和小";
		}
		
		if($sum % 2 == 1)
			$val .= "总和单";
		else
			$val .= "总和双";
		
		if(intval(substr($sum,-1,1)) > 4)
			$val .= "总和尾大";
		else
			$val .= "总和尾小";

		
		if(strrpos($val, $actionData) !== false) $count++;
		return $count;
	}
	//第N球大小单双数字
	public function s115dxds($actionData, $code){
		$count = 0;
		//数字
		if(is_numeric($actionData)) {
			if($actionData == $code){
				$count=1;
			}
			return $count;
		}
		
		if($code != 11){
			$val = "";
			if($code >= 6)
				$val .= "大";
			else
				$val .= "小";
			
			if($code % 2 == 1)
				$val .= "单";
			else
				$val .= "双";
			if(strrpos($val, $actionData) !== false) $count++;
		} else {
			$count = -1;
		}
		return $count;
	}
	//第一球大小单双数字
	public function s115d1b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[0]);
	}
	//第二球大小单双数字
	public function s115d2b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[1]);
	}
	//第三球大小单双数字
	public function s115d3b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[2]);
	}
	//第四球大小单双数字
	public function s115d4b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[3]);
	}
	//第五球大小单双数字
	public function s115d5b($actionData, $openCode) {
		$code = explode(',', $openCode);
		return $this->s115dxds($actionData, $code[4]);
	}

	// 信用玩法 end

#======================================================================================================================================
# 快樂十分
#======================================================================================================================================

	//任选一 选一数投
	public function klsfR1B($bet, $data) {
		$result = array_filter(
			explode(' ', $bet),
			function ($val) use ($data){
				return strpos(substr($data,0,2),$val) !== false;
			}
		);
		return count($result);
	}
	//任选一 选一红投
	public function klsfR1R($bet, $data) {
		$result = array_filter(
			explode(' ', $bet),
			function ($val) use ($data){
				return strpos(substr($data,0,2),$val) !== false;
			}
		);
		return count($result);
	}

	public function klsfR2($bet, $data) {
		return $this->rx($bet, $data, 2);
	}

	public function klsfR3($bet, $data) {
		return $this->rx($bet, $data, 3);
	}

	public function klsfR4($bet, $data) {
		return $this->rx($bet, $data, 4);
	}

	public function klsfR5($bet, $data) {
		return $this->rx($bet, $data, 5);
	}

	//前二直选
	public function klsfQ2($bet, $data) {
		return $this->qs($bet, $data, 2);
	}

	//前二组选
	public function klsfQ2z($bet, $data) {
		return $this->zx($bet, substr($data,0,5));
	}

	//前三直选
	public function klsfQ3($bet, $data) {
		return $this->qs($bet, $data, 3);
	}

	//前三组选
	public function klsfQ3z($bet, $data) {
		return $this->zx($bet, substr($data,0,8));
	}

#======================================================================================================================================
# 北京PK10
#======================================================================================================================================
	//冠军
	public function kjq1($bet, $data) {
		return $this->qs_addW($bet, $data, 1);
	}

	//冠亚军
	public function kjq2($bet, $data) {
		return $this->qs_addW($bet, $data, 2);
	}
	//前三
	public function kjq3($bet, $data) {
		return $this->qs_addW($bet, $data, 3);
	}
	//两面
	//冠军 亚军 季军 五名 六名 七名 八名 九名 十名 冠亚 冠亚季
	public function pk10lmdxds1($bet, $data) {
		return $this->dxds2($bet, substr($data,0,2));
	}
	public function pk10lmdxds2($bet, $data) {
		return $this->dxds2($bet, substr($data,3,2));
	}
	public function pk10lmdxds3($bet, $data) {
		return $this->dxds2($bet, substr($data,6,2));
	}
	public function pk10lmdxds4($bet, $data) {
		return $this->dxds2($bet, substr($data,9,2));
	}
	public function pk10lmdxds5($bet, $data) {
		return $this->dxds2($bet, substr($data,12,2));
	}
	public function pk10lmdxds6($bet, $data) {
		return $this->dxds2($bet, substr($data,15,2));
	}
	public function pk10lmdxds7($bet, $data) {
		return $this->dxds2($bet, substr($data,18,2));
	}
	public function pk10lmdxds8($bet, $data) {
		return $this->dxds2($bet, substr($data,21,2));
	}
	public function pk10lmdxds9($bet, $data) {
		return $this->dxds2($bet, substr($data,24,2));
	}
	public function pk10lmdxds10($bet, $data) {
		return $this->dxds2($bet, substr($data,27,2));
	}

	public function pk10lmdxds22($bet, $data) {
		$data = explode(',', $data);
		$val = sprintf('%02d',$data[0]+$data[1]);
		$bet = $this->strCut($bet,1);
		$count = 0;
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == '大') {
				if($val > 11 && $val <20) $count++;
			}
			elseif ($bet[$i] == '小') {
				if($val > 2 && $val <12) $count++;
			}
			elseif ($bet[$i] == '单') {
				if($val % 2 != 0) $count++;
			}
			elseif ($bet[$i] == '双') {
				if($val % 2 == 0) $count++;
			}
		}
		return $count;
	}

	public function pk10lmdxds33($bet, $data) {
		$data = explode(',', $data);
		$val = sprintf('%02d',$data[0]+$data[1]+$data[2]);
		$bet = $this->strCut($bet,1);
		$count = 0;
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == '大') {
				if($val >= 17 && $val <=27) $count++;
			}
			elseif ($bet[$i] == '小') {
				if($val >= 6 && $val <=16) $count++;
			}
			elseif ($bet[$i] == '单') {
				if($val % 2 != 0) $count++;
			}
			elseif ($bet[$i] == '双') {
				if($val % 2 == 0) $count++;
			}
		}
		return $count;
	}

	//冠亚季选一
	public function pk10r123($bet, $data) {
		return $this->rx($bet, substr($data,0,8), 1);
	}

	//冠亚总和
	public function pk10gy2($bet, $data) {
		$data = explode(',', $data);
		$val = sprintf('%02d',$data[0]+$data[1]);
		$bet = explode(' ', $bet);
		$count = 0;
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $val) $count++;
		}
		return $count;
	}

	//冠亚组合
	public function pk10gyzh($bet, $data) {
		$data = explode(',', $data);
		$str1 = sprintf('%02d-%02d',$data[0],$data[1]);
		$str2 = sprintf('%02d-%02d',$data[1],$data[0]);
		$bet = explode(' ', $bet);
		$count = 0;
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $str1 || $bet[$i] == $str2) $count++;
		}
		return $count;
	}

	//龙虎
	public function pk10lh1($bet, $data) {
		return $this->pk10lh($bet, $data, 1);
	}
	public function pk10lh2($bet, $data) {
		return $this->pk10lh($bet, $data, 2);
	}
	public function pk10lh3($bet, $data) {
		return $this->pk10lh($bet, $data, 3);
	}
	public function pk10lh4($bet, $data) {
		return $this->pk10lh($bet, $data, 4);
	}
	public function pk10lh5($bet, $data) {
		return $this->pk10lh($bet, $data, 5);
	}

	public function pk10lh12($bet, $data){
		$data = explode(',', $data);
		$val1 = intval($data[0] + $data[1]);
		$val2 = intval($data[9] + $data[8]);
		$bet = mb_split($bet);
		$count = 0;
		for($i=0;$i<count($bet);$i++) {
			if ($bet[$i] == '龙') {
				if ($val1 > $val2) $count++;
			}
			elseif ($bet[$i] == '虎') {
				if ($val1 < $val2) $count++;
			}
		}
		return $count;
	}

	public function pk10lh123($bet, $data){
		$data = explode(',', $data);
		$val1 = intval($data[0] + $data[1] + $data[2]);
		$val2 = intval($data[9] + $data[8] + $data[7]);
		$bet = mb_split($bet);
		$count = 0;
		for($i=0;$i<count($bet);$i++) {
			if ($bet[$i] == '龙') {
				if ($val1 > $val2) $count++;
			}
			elseif ($bet[$i] == '虎') {
				if ($val1 < $val2) $count++;
			}
		}
		return $count;
	}

	// 前二组选
	public function kjzx2($bet, $data) {
		return $this->zx($bet, substr($data,0,5));
	}

	// 前三组选
	public function kjzx3($bet, $data) {
		return $this->zx($bet, substr($data,0,8));
	}
#======================================================================================================================================
# 快乐8
#======================================================================================================================================

	public function k8R1($bet, $data) {
		$data = explode('|', $data);
		return $this->rx($bet, $data[0], 1);
	}

	public function k8R2($bet, $data) {
		$data = explode('|', $data);
		return $this->rx($bet, $data[0], 2);
	}

	public function k8R3($bet, $data) {
		$data = explode('|', $data);
		return $this->rx($bet, $data[0], 3);
	}

	public function k8R4($bet, $data) {
		$data = explode('|', $data);
		return $this->rx($bet, $data[0], 4);
	}

	public function k8R5($bet, $data) {
		$data = explode('|', $data);
		return $this->rx($bet, $data[0], 5);
	}

	public function k8R6($bet, $data) {
		$data = explode('|', $data);
		return $this->rx($bet, $data[0], 6);
	}

	public function k8R7($bet, $data) {
		$data = explode('|', $data);
		return $this->rx($bet, $data[0], 7);
	}
#======================================================================================================================================
# 快3
#======================================================================================================================================
	//和值
	public function k3hz($bet, $data){
		$data = explode(',', $data);
		$bet = explode(' ', $bet);
		$val = intval($data[0] + $data[1] + $data[2]);
		$count = 0;
		for($i=0;$i<count($bet);$i++) {
			if ($bet[$i] == $val) $count++;
		}
		return $count;
	}

	//三同号通选 確定
	public function k33tx($bet, $data){
		$data = str_replace(',','',$data);
		return strpos($bet, $data) !== false ? 1 : 0;
	}

	//三连号通选 確定
	public function k33ltx($bet, $data){
		return $this->k33tx($bet, $data);
	}

	//三同号单选 確定
	public function k33dx($bet, $data){
		$data = str_replace(',','',$data);
		$bet = explode(' ', $bet);
		for($i=0;$i<count($bet);$i++) {
			if($bet[$i] == $data) return 1;
		}
		return 0;
	}

	//三不同号
	public function k33x($bet, $data){
		return $this->zx($bet, $data);
	}

	//二不同号
	public function k32x($bet, $data){
		$data = explode(',', $data);
		$result = array_filter(
			$this->combine(explode(' ', $bet), 2),
			function ($array) use ($data) {
				for($i=0;$i<count($array);$i++) {
					if(array_search($array[$i], $data) === false) return false;
				}
				return true;
			}
		);
		return count($result);
	}

	// 二同号单选
	public function k32dx($bet, $data){
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		$a=array_unique($data);
		if(count($a)!=2)
		{
			return 0;
		}
		$c= array_count_values($data);
		asort($c);
		$cT='';
		$cF='';
		foreach($c as $k=>$v)
		{
			if($v==1)
			{
				$cF = $k;
			}
			if($v==2)
			{
				$cT = $k;
			}
		}
		$betT = explode(' ', $bet[0]);
		$betF = explode(' ', $bet[1]);
		$check=[];
		for($i=0;$i<count($betT);$i++) {
			if($betT[$i] == $cT.$cT)
			{
				$check[]=$cT;
			}
		}
		for($i=0;$i<count($betF);$i++) {
			if($betF[$i] == $cF)
			{
				$check[]=$cF;
			}
		}
		if(count($check)==2)
		{
			return 1;
		}else {
			return 0;
		}
	}

	// 二同号复选 確定
	public function k32fx($bet, $data){
		$bet = explode(' ', str_replace('*','',$bet));
		$data = explode(',', $data);
		$a=array_unique($data);
		if(count($a)==3)
		{
			return 0;
		}
		$c= array_count_values($data);
		asort($c);
		$cT='';
		$cF='';
		foreach($c as $k=>$v)
		{
			if($v==1)
			{
				$cF = $k;
			}
			if($v==2)
			{
				$cT = $k;
			}
			if($v==3)
			{
				$cT = $k;
			}
		}

		$val = $cT.$cT;

		for($i=0;$i<count($bet);$i++) {
			if(strpos($bet[$i], $val) !== false) return 1;
		}
		return 0;
	}
#======================================================================================================================================
# 幸運28
#======================================================================================================================================
	//單選和值
	public function lucky28hz($bet, $data){
		$data = explode(',', $data);
		if($bet===$data[3])
		{
			return 1;
		}
		return 0;
	}
	public function lucky28dxds($bet, $data){
		$data = explode(',', $data);
		return $this->dxds28($bet, $data[3]);
	}
	public function lucky28dxds2($bet, $data){
		$data = explode(',', $data);
		return $this->dxds282($bet, $data[3]);
	}
	public function lucky28zdxds($bet, $data){
		$data = explode(',', $data);
		return $this->zdxds28($bet, $data[3]);
	}
	public function lucky28color($bet, $data){
		$data = explode(',', $data);
		return $this->color28($bet, $data[3]);
	}
	public function lucky28bz($bet, $data){
		$data = explode(',', $data);
		if($data[0] == $data[1] && $data[1] == $data[2]) {
			return ($bet==='豹子')? 1 : 0;
		}
		return 0;
	}
	//特码包三
	public function lucky28r3($bet, $data){
		$check=explode(' ', $bet);
		$data = explode(',', $data);
		$count=array_unique($check);
		if(count($check)!=count($count) || count($check)<>3) return 0;
		if(in_array($data[3],$check)) return 1;
		return 0;
	}

#======================================================================================================================================
# 龍虎和相關
#======================================================================================================================================

	/** 龍虎和相關(default 1&5) **/
	public function lhh($bet, $data, $first=0, $last=4){
		$codes = explode(',', $data);
		$count = 0;
		if($codes[$first] == $codes[$last]){
			if(strrpos($bet, '和') !== false) $count = 1;
		} else if($codes[$first] > $codes[$last]) {
			if(strrpos($bet, '龙') !== false) $count = 1;
		} else {
			if(strrpos($bet, '虎') !== false) $count = 1;
		}
		return $count;
	}
	public function lhh12($bet, $data){
		return $this->lhh($bet, $data, 0 ,1);
	}
	public function lhh13($bet, $data){
		return $this->lhh($bet, $data, 0 ,2);
	}
	public function lhh14($bet, $data){
		return $this->lhh($bet, $data, 0 ,3);
	}
	public function lhh16($bet, $data){
		return $this->lhh($bet, $data, 0 ,5);
	}
	public function lhh17($bet, $data){
		return $this->lhh($bet, $data, 0 ,6);
	}
	public function lhh18($bet, $data){
		return $this->lhh($bet, $data, 0 ,7);
	}
	public function lhh23($bet, $data){
		return $this->lhh($bet, $data, 1 ,2);
	}
	public function lhh24($bet, $data){
		return $this->lhh($bet, $data, 1 ,3);
	}
	public function lhh25($bet, $data){
		return $this->lhh($bet, $data, 1 ,4);
	}
	public function lhh26($bet, $data){
		return $this->lhh($bet, $data, 1 ,5);
	}
	public function lhh27($bet, $data){
		return $this->lhh($bet, $data, 1 ,6);
	}
	public function lhh28($bet, $data){
		return $this->lhh($bet, $data, 1 ,7);
	}
	public function lhh34($bet, $data){
		return $this->lhh($bet, $data, 2 ,3);
	}
	public function lhh35($bet, $data){
		return $this->lhh($bet, $data, 2 ,4);
	}
	public function lhh36($bet, $data){
		return $this->lhh($bet, $data, 2 ,5);
	}
	public function lhh37($bet, $data){
		return $this->lhh($bet, $data, 2 ,6);
	}
	public function lhh38($bet, $data){
		return $this->lhh($bet, $data, 2 ,7);
	}
	public function lhh45($bet, $data){
		return $this->lhh($bet, $data, 3 ,4);
	}
	public function lhh46($bet, $data){
		return $this->lhh($bet, $data, 3 ,5);
	}
	public function lhh47($bet, $data){
		return $this->lhh($bet, $data, 3 ,6);
	}
	public function lhh48($bet, $data){
		return $this->lhh($bet, $data, 3 ,7);
	}
	public function lhh56($bet, $data){
		return $this->lhh($bet, $data, 4 ,5);
	}
	public function lhh57($bet, $data){
		return $this->lhh($bet, $data, 4 ,6);
	}
	public function lhh58($bet, $data){
		return $this->lhh($bet, $data, 4 ,7);
	}
	public function lhh67($bet, $data){
		return $this->lhh($bet, $data, 5 ,6);
	}
	public function lhh68($bet, $data){
		return $this->lhh($bet, $data, 5 ,7);
	}
	public function lhh78($bet, $data){
		return $this->lhh($bet, $data, 6 ,7);
	}


#======================================================================================================================================
# 六合彩
#======================================================================================================================================

	public function lhczh() {
		//list($bet, $data, $Groupname, $betInfo, $animalsYear, $alias, $lhcWxJin, $lhcWxMu, $lhcWxShui, $lhcWxHuo, $lhcWxTu) = func_get_args();
		list($bet, $data, $Groupname, $betInfo, $animalsYear, $lhcWxJin, $lhcWxMu, $lhcWxShui, $lhcWxHuo, $lhcWxTu) = func_get_args();
		$count = 0;
		if (count(explode(',',$data)) != 7) return $count;

		if($Groupname=='特码'){
			return $this->lhctm($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='特肖头尾'){
			return $this->lhctxws($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='五行'){
			return $this->lhcwx($bet, $data, $lhcWxJin, $lhcWxMu, $lhcWxShui, $lhcWxHuo, $lhcWxTu);
		}else if($Groupname=='合肖'){
			return $this->lhchx($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='头尾数'){
			return $this->lhctws($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='平特肖尾'){
			return $this->lhcptyxws($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='正肖'){
			return $this->lhczx($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='7色波'){
			return $this->lhc7sb($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='总肖'){
			return $this->lhczhongxiao($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='自选不中'){
			return $this->lhczxbz($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='连肖'){
			return $this->lhclx($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='连尾'){
			return $this->lhclw($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='连肖连尾'){
			return $this->lhclxlw($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='连码'){
			return $this->lhclianma($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='正码'){
			return $this->lhczm($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='正码特'){
			return $this->lhczmt($bet, $data, $Groupname, $betInfo, $animalsYear, $alias);
		}else if($Groupname=='色波'){
			return $this->lhcsb($bet, $data, $Groupname, $betInfo, $animalsYear);
		}else if($Groupname=='两面'){
			return $this->lhclm($bet, $data, $Groupname, $betInfo, $animalsYear);
		}		 
		return $count;
	}
	public function lhctm($bet, $data, $Groupname, $betInfo, $animalsYear){
		$count=0;
		$valhe='';
		$kjhe=0;
		$data = explode(',',$data);
		$val = intval($data[6]);
		$val2 = $data[6];
		switch(true)
		{
			case $bet==strval($val2):
				$count=1;
				break;
			case $bet=='特大':
				if($val>=25 && $val<=48) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特小':
				if($val>0 && $val<=24) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特单':
				if($val%2!=0 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特双':
				if($val%2==0 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特合大':
				$valhe=intval(substr($val2,0,1)) + intval(substr($val2,1,1));
				if($valhe>=7 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特合小':
				$valhe=intval(substr($val2,0,1)) + intval(substr($val2,1,1));
				if($valhe<=6 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特合单':
				$valhe=intval(substr($val2,0,1)) + intval(substr($val2,1,1));
				if($valhe%2!=0 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特合双':
				$valhe=intval(substr($val2,0,1)) + intval(substr($val2,1,1));
				if($valhe%2==0 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特尾大':
				$valwx = intval(substr($val2,1,1));
				if($valwx>=5 && $valwx<=9 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特尾小':
				$valwx = intval(substr($val2,1,1));
				if($valwx>=0 && $valwx<=4 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特大双':
				if($val>=25 && $val<=48 && $val%2==0) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特小双':
				if($val>0 && $val<=24 && $val%2==0) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特大单':
				if($val>=25 && $val<=48 && $val%2!=0) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特小单':
				if($val>0 && $val<=24 && $val%2!=0) $count=1;
				if($val==49) $count = -1;
				break;
		}
		return $count;
	}

	public function lhclm($bet, $data, $Groupname, $betInfo, $animalsYear){
		$count=0;
		$valhe='';
		$kjhe=0;
		$data = explode(',',$data);
		$val = intval($data[6]);
		$val2 = $data[6];
		for($i=0; $i<count($data); $i++){
			$kjhe += intval($data[$i]);
		}
		switch(true)
		{
			case $bet=='特大':
				if($val>=25 && $val<=48) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特小':
				if($val>0 && $val<=24) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特单':
				if($val%2!=0 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特双':
				if($val%2==0 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特合大':
				$valhe=intval(substr($val2,0,1)) + intval(substr($val2,1,1));
				if($valhe>=7 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特合小':
				$valhe=intval(substr($val2,0,1)) + intval(substr($val2,1,1));
				if($valhe<=6 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特合单':
				$valhe=intval(substr($val2,0,1)) + intval(substr($val2,1,1));
				if($valhe%2!=0 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特合双':
				$valhe=intval(substr($val2,0,1)) + intval(substr($val2,1,1));
				if($valhe%2==0 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特尾大':
				$valwx = intval(substr($val2,1,1));
				if($valwx>=5 && $valwx<=9 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特尾小':
				$valwx = intval(substr($val2,1,1));
				if($valwx>=0 && $valwx<=4 && $val != 49) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特大双':
				if($val>=25 && $val<=48 && $val%2==0) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特小双':
				if($val>0 && $val<=24 && $val%2==0) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特大单':
				if($val>=25 && $val<=48 && $val%2!=0) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='特小单':
				if($val>0 && $val<=24 && $val%2!=0) $count=1;
				if($val==49) $count = -1;
				break;
			case $bet=='总和大':
				if($kjhe>=175) $count=1;
				break;
			case $bet=='总和小':
				if($kjhe<=174 && $kjhe>=15) $count=1;
				break;
			case $bet=='总和单':
				if($kjhe%2!=0 && $kjhe>=15) $count=1;
				break;
			case $bet=='总和双':
				if($kjhe%2==0 && $kjhe>=15) $count=1;
				break;
			case $bet=='特天肖':
				if($this->ArrayContains($this->LHCXX['SX_TIAN'], $this->getZodica($val, $animalsYear))) $count=1;
				break;
			case $bet=='特地肖':
				if($this->ArrayContains($this->LHCXX['SX_DI'], $this->getZodica($val, $animalsYear))) $count=1;
				break;
			case $bet=='特前肖':
				if($this->ArrayContains($this->LHCXX['SX_QIAN'], $this->getZodica($val, $animalsYear))) $count=1;
				break;
			case $bet=='特后肖':
				if($this->ArrayContains($this->LHCXX['SX_HOU'], $this->getZodica($val, $animalsYear))) $count=1;
				break;
			case $bet=='特家肖':
				if($this->ArrayContains($this->LHCXX['SX_JIA'], $this->getZodica($val, $animalsYear))) $count=1;
				break;
			case $bet=='特野肖':
				if($this->ArrayContains($this->LHCXX['SX_YE'], $this->getZodica($val, $animalsYear))) $count=1;
				break;
		}
		return $count;
	}

	public function lhctxws($bet, $data, $Groupname, $betInfo, $animalsYear){
		$count=0;
		$data = explode(',',$data);
		$val = intval($data[6]);		
		$val=$this->getZodica($val, $animalsYear);
		if($val==$bet)
		{
			$count=1;
			return $count;
		}
		$val = $data[6];
		$valtou = intval(substr((string)$val,0,1));
		$valwei = intval(substr((string)$val,1,1));
		$bet = preg_split('/(?<!^)(?!$)/u', $bet );
		if($valtou==intval($bet[0]) && $bet[1]=='头'){
			$count=1;
		}else if($valwei==intval($bet[0]) && $bet[1]=='尾'){
			$count=1;
		}		
		return $count;
	}

	//计算起始生肖对应的数组键名
	public function getYearZodicaIndex($year) {
		for($i=0;$i<count($this->LHC['zodiacArr']);$i++){
			if ($year == $this->LHC['zodiacArr'][$i]) {
				return $i;
			}
		}
	}
	//计算投注生肖对应的号码组
	public function getZodicaNum($sxName, $year) {
		$index = $this->getYearZodicaIndex($year);
		$j = 1;
		for ($i = $index; $i >= 0; $i--) {
			if ($this->LHC['zodiacArr'][$i] == $sxName) {
				return $j;
			}
			$j += 1;
		}
		for ($i = count($this->LHC['zodiacArr']) - 1; $i >= $index; $i--) {
			if ($this->LHC['zodiacArr'][$i] == $sxName) {
				return $j;
			}
			$j += 1;
		}
	}
	//计算号码对应的生肖
	public function getZodica($num,$year){
		for($i=0;$i<count($this->LHC['zodiacArr']);$i++){
			$zodiac = $this->LHC['zodiacArr'][$i];
			$sxs = $this->SX_NUMS[$this->getZodicaNum($zodiac,$year)];
			$str = '';
			for($j=0;$j<count($sxs);$j++){
				if (intval($sxs[$j]) == intval($num)) {
					return $zodiac;
				}				
			}
		}
		return '';
	}

	public function ArrayContains($array, $num) {
		for($i=0; $i<count($array); $i++){
			if ($array[$i] == $num) return true;
		}
		return false;
	}

	public function lhcsb($bet, $data, $Groupname, $betInfo, $animalsYear){
		$count=0;
		$data = explode(',',$data);
		$val = intval($data[6]);	
		if($bet=='红波'){
			if($this->ArrayContains($this->LHCXX['HONG_NUMS'], $val)) $count=1;
		}else if($bet=='蓝波'){
			if($this->ArrayContains($this->LHCXX['LANG_NUMS'], $val)) $count=1;
			
		}else if($bet=='绿波'){
			if($this->ArrayContains($this->LHCXX['LV_NUMS'], $val)) $count=1;
			
		}else if($bet=='红大'){
			if($val>=25 && $val<=48 && $this->ArrayContains($this->LHCXX['HONG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='红小'){
			if($val>0 && $val<=24 && $this->ArrayContains($this->LHCXX['HONG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='红单'){
			if($val%2!=0 && $val != 49 && $this->ArrayContains($this->LHCXX['HONG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='红双'){
			if($val%2==0 && $val != 49 && $this->ArrayContains($this->LHCXX['HONG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='蓝大'){
			if($val>=25 && $val<=48 && $this->ArrayContains($this->LHCXX['LANG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='蓝小'){
			if($val>0 && $val<=24 && $this->ArrayContains($this->LHCXX['LANG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='蓝单'){
			if($val%2!=0 && $val != 49 && $this->ArrayContains($this->LHCXX['LANG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='蓝双'){
			if($val%2==0 && $val != 49 && $this->ArrayContains($this->LHCXX['LANG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='绿大'){
			if($val>=25 && $val<=48 && $this->ArrayContains($this->LHCXX['LV_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='绿小'){
			if($val>0 && $val<=24 && $this->ArrayContains($this->LHCXX['LV_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='绿单'){
			if($val%2!=0 && $val != 49 && $this->ArrayContains($this->LHCXX['LV_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='绿双'){
			if($val%2==0 && $val != 49 && $this->ArrayContains($this->LHCXX['LV_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;		
			
		}else if($bet=='红大双'){
			if($val>=25 && $val<=48 && $val%2==0  && $this->ArrayContains($this->LHCXX['HONG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='红小双'){
			if($val>0 && $val<=24 && $val%2==0  && $this->ArrayContains($this->LHCXX['HONG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='红大单'){
			if($val>=25 && $val<=48 && $val%2!=0  && $this->ArrayContains($this->LHCXX['HONG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='红小单'){
			if($val>0 && $val<=24 && $val%2!=0  && $this->ArrayContains($this->LHCXX['HONG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='蓝大双'){
			if($val>=25 && $val<=48 && $val%2==0  && $this->ArrayContains($this->LHCXX['LANG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='蓝小双'){
			if($val>0 && $val<=24 && $val%2==0  && $this->ArrayContains($this->LHCXX['LANG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='蓝大单'){
			if($val>=25 && $val<=48 && $val%2!=0  && $this->ArrayContains($this->LHCXX['LANG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='蓝小单'){
			if($val>0 && $val<=24 && $val%2!=0  && $this->ArrayContains($this->LHCXX['LANG_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='绿大双'){
			if($val>=25 && $val<=48 && $val%2==0  && $this->ArrayContains($this->LHCXX['LV_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='绿小双'){
			if($val>0 && $val<=24 && $val%2==0  && $this->ArrayContains($this->LHCXX['LV_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='绿大单'){
			if($val>=25 && $val<=48 && $val%2!=0  && $this->ArrayContains($this->LHCXX['LV_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
			
		}else if($bet=='绿小单'){
			if($val>0 && $val<=24 && $val%2!=0  && $this->ArrayContains($this->LHCXX['LV_NUMS'], $val)) $count=1;
			if($val==49) $count = -1;
		}
		return $count;
	}

	public function lhczm($bet, $data, $Groupname, $betInfo, $animalsYear){
		$count=0;
		$kjhe=0;
		$data = explode(',',$data);
		for($i=0; $i<count($data); $i++){
			$kjhe += intval($data[$i]);
		}
		array_pop($data);
		switch(true)
		{
			case $bet=='总和大':
				if($kjhe>=175) $count=1;
				break;
			case $bet=='总和小':
				if($kjhe<=174 && $kjhe>=15) $count=1;
				break;
			case $bet=='总和单':
				if($kjhe%2!=0 && $kjhe>=15) $count=1;
				break;
			case $bet=='总和双':
				if($kjhe%2==0 && $kjhe>=15) $count=1;
				break;
			default:
				if($this->ArrayContains($data, $bet)){
					$count=1;
				}			
				break;
		}
		return $count;
	}
	public function lhcptyxws($bet, $data, $Groupname, $betInfo, $animalsYear){
		$count=0;
		$data = explode(',',$data);
		$betarr = preg_split('/(?<!^)(?!$)/u', $bet );
		if(count($betarr)==2 && $betarr[1]=='尾'){
			$bet=intval($betarr[0]);
			for($i=0; $i<count($data); $i++){
				$kjwei = preg_split('/(?<!^)(?!$)/u', $data[$i] );
				$valwei = intval($kjwei[1]);
				if($valwei==$bet){
					$count=1;
					break;
				}
			}
		}else if(count($betarr)==1){
			for($i=0; $i<count($data); $i++){
				$valwei=$this->getZodica(intval($data[$i]), $animalsYear);
				if($valwei==$bet){
					$count=1;
					break;
				}
			}
		}
		return $count;
	}
	public function lhclx($bet, $data, $Groupname, $betInfo, $animalsYear){
		$count=0;
		$ii=0;
		$data = explode(',',$data);
		$betInfo = explode(',',$betInfo);
		$betarr = preg_split('/(?<!^)(?!$)/u', $bet );
		$match_arr=array();
		if(count($betarr)==3 && $betarr[2]=='肖'){
			for($i=0; $i<count($data); $i++){
				$val=$this->getZodica(intval($data[$i]), $animalsYear);
				$match_arr[]=$val;
			}
			for($j=0; $j<count($betInfo); $j++){
				if(in_array($betInfo[$j], $match_arr)){
					$ii+=1;
					//unset($betInfo[$j]);
				}
			}
			if($ii==count($betInfo) && $ii) $count=1;
		}
		return $count;	
	}
	public function lhclw($bet, $data, $Groupname, $betInfo, $animalsYear){
		$count=0;
		$ii=0;
		$data = explode(',',$data);
		$betInfo = explode(',',$betInfo);
		$betarr = preg_split('/(?<!^)(?!$)/u', $bet );
		$match_arr=array();
		if(count($betarr)==3 && $betarr[2]=='尾'){
			$valwei='';
			for($i=0; $i<count($data); $i++){
				$valwei=$data[$i];
				$match_arr[]=intval($valwei[1]);
			}
			for($j=0; $j<count($betInfo); $j++){
				if(in_array(intval($betInfo[$j]), $match_arr)){
					$ii+=1;
					//unset($betInfo[$j]);
				}
			}			
			if($ii==count($betInfo) && $ii) $count=1;
		}
		return $count;	
	}
	public function lhclianma($bet, $data, $Groupname, $betInfo, $animalsYear){
		$count=0;
		$data = explode(',',$data);
		$betInfo = explode(',',$betInfo);
		$mapzm=array($data[0],$data[1],$data[2],$data[3],$data[4],$data[5]);
		if($bet=='三中二'){
			$betInfo=$this->combine($betInfo, 3);
			for($i=0; $i<count($betInfo); $i++){
				$betarr=$betInfo[$i];
				$xoi=0;
				for($j=0; $j<count($betarr); $j++){
					if(in_array($betarr[$j],$mapzm))
					{
						$xoi++;
					}
				}
				if($xoi==3){
					//三个全中按5倍算
					$count+=5;
				}else if($xoi==2){
					//中两个按1倍算
					$count+=1;
				}
			}
		}else if($bet=='三全中'){
			$betInfo=$this->combine($betInfo, 3);
			for($i=0; $i<count($betInfo); $i++){
				$betarr=$betInfo[$i];
				$xoi=0;
				for($j=0; $j<count($betarr); $j++){
					if(in_array($betarr[$j],$mapzm))
					{
						$xoi++;
					}
				}
				if($xoi==3){
					$count+=1;
				}
			}

		}else if($bet=='二全中'){
			$betInfo=$this->combine($betInfo, 2);
			for($i=0; $i<count($betInfo); $i++){
				$betarr=$betInfo[$i];
				$xoi=0;
				for($j=0; $j<count($betarr); $j++){
					if(in_array($betarr[$j],$mapzm))
					{
						$xoi++;
					}
				}
				if($xoi==2){
					$count+=1;
				}
			}

		}else if($bet=='四全中'){
			$betInfo=$this->combine($betInfo, 4);
			for($i=0; $i<count($betInfo); $i++){
				$betarr=$betInfo[$i];
				$xoi=0;
				for($j=0; $j<count($betarr); $j++){
					if(in_array($betarr[$j],$mapzm))
					{
						$xoi++;
					}
				}
				if($xoi==4){
					$count+=1;
				}
			}
	
		}else if($bet=='二中特'){
			$betInfo=$this->combine($betInfo, 2);
			$val=intval($data[6]);
			for($i=0; $i<count($betInfo); $i++){
				$betarr=$betInfo[$i];
				$xoi=0;
				for($j=0; $j<count($betarr); $j++){
					if(in_array($betarr[$j],$data))
					{
						$xoi++;
					}
					/* 邏輯錯誤,註解掉 by robert
					for($ii=0; $ii<count($data); $ii++){
						if($ii<6 && $betarr[$j]==$data[$ii]){
							$xoi++;
						}
					}
					*/
				}
				if($xoi==2 && $this->ArrayContains($betarr, $val)){
					// 中特
					$count+=1;
				}else if($xoi==2 && !$this->ArrayContains($betarr, $val)){
					// 中二
					$count+=2;
				}
			}
		}else if($bet=='特串'){
			//特串即一个号码为正码另一号码为特码
			$betInfo=$this->combine($betInfo, 2);
			$val=intval($data[6]);
			for($i=0; $i<count($betInfo); $i++){
				$betarr=$betInfo[$i];
				$xoi=0;
				for($j=0; $j<count($betarr); $j++){
					if(in_array($betarr[$j],$data))
					{
						$xoi++;
					}
				}
				if($xoi==2 && $this->ArrayContains($betarr, $val)){
					$count+=1;
				}
			}
		}
		return $count;
	}
#======================================================================================================================================
# 組合公式
#======================================================================================================================================
	//龙虎
	public function pk10lh($bet, $data, $num){
		$data = explode(',', $data);
		$val1 = intval($data[$num-1]);
		$val2 = intval($data[10-$num]);
		$bet = mb_split($bet);
		$count = 0;
		for($i=0;$i<count($bet);$i++) {
			if ($bet[$i] == '龙') {
				if ($val1 > $val2) $count++;
			}
			elseif ($bet[$i] == '虎') {
				if ($val1 < $val2) $count++;
			}
		}
		return $count;
	}

	public function rx($bet, $data, $num) {
		preg_match('/^\(([\d ]+)\)([\d ]+)$/', $bet, $match);
		if($match) {
			$arr = explode(' ', $match[1]);
			for($i=0;$i<count($arr);$i++) {
				if(strpos($data, $arr[$i]) === false) return 0;
			}
			$result = array_filter(
				$this->combine(explode(' ', $match[2]), $num - count($arr)),
				function ($array) use ($data, $num, $arr) {
					if($num < 5) {
						for($i=0;$i<count($array);$i++) {
							if(strpos($data, $array[$i]) === false) return false;
						}
					}
					else {
						$array = array_merge($array, $arr);
						$data = explode(',', $data);
						for($i=0;$i<count($data);$i++) {
							if(array_search($data[$i], $array) === false) return false;
						}
					}
					return true;
				}
			);
		}
		else {
			$result = array_filter(
				$this->combine(explode(' ', $bet), $num),
				function ($array) use ($data, $num) {
					if($num < 5) {
						for($i=0;$i<count($array);$i++) {
							if(strpos($data, $array[$i]) === false) return false;
						}
					}
					else {
						$data = explode(',', $data);
						for($i=0;$i<count($data);$i++) {
							if(array_search($data[$i], $array) === false) return false;
						}
					}
					return true;
				}
			);
		}
		return count($result);

	}


	public function zx($bet, $data) {
		preg_match('/^\(([\d ]+)\)([\d ]+)$/', $bet, $match);
		$data = explode(',', $data);
		if($match) {
			$arr = explode(' ', $match[1]);
			for($i=0;$i<count($arr);$i++) {
				if(array_search($arr[$i], $data) === false) return 0;
			}
			$result = array_filter(
				$this->combine(explode(' ', $match[2]), count($data) - count($arr)),
				function ($array) use ($data) {
					for($i=0;$i<count($array);$i++) {
						if(array_search($array[$i], $data) === false) return false;
					}
					return true;
				}
			);
		}
		else {
			$result = array_filter(
				$this->combine(explode(' ', $bet), count($data)),
				function ($array) use ($data) {
					for($i=0;$i<count($array);$i++) {
						if(array_search($array[$i], $data) === false) return false;
					}
					return true;
				}
			);
		}
		
		return count($result);

	}


	/**
	 * 28色波
	 *
	 * @params bet		投注列表：大单,小单
	 * @params data		开奖所需的那几位号码：4,5
	 *
	 * @return			返回中奖注数
	 */
	public function color28($bet, $data){
		$map = array(
			'红波'=>'3,6,9,12,15,18,21,24',
			'绿波'=>'1,4,7,10,16,19,22,25',
			'蓝波'=>'2,5,8,11,17,20,23,26'
		);
		$bet_array = explode(',',$map[$bet]);
		if(!in_array($data,$bet_array)) return 0;
		return 1;
	}
	/**
	 * 28大小单双
	 *
	 * @params bet		投注列表：大单,小单
	 * @params data		开奖所需的那几位号码：4,5
	 *
	 * @return			返回中奖注数
	 */
	public function dxds282($bet, $data){
		$map = array(
			'大单'=>'15,17,19,21,23,25,27',
			'小单'=>'1,3,5,7,9,11,13',
			'大双'=>'14,16,18,20,22,24,26',
			'小双'=>'0,2,4,6,8,10,12'
		);
		$bet_array = explode(',',$map[$bet]);
		if(!in_array($data,$bet_array)) return 0;
		return 1;
	}
	/**
	 * 28大小单双
	 *
	 * @params bet		投注列表：大单,小单
	 * @params data		开奖所需的那几位号码：4,5
	 *
	 * @return			返回中奖注数
	 */
	public function dxds28($bet, $data){
		$map = array(
			'大'=>'14,15,16,17,18,19,20,21,22,23,24,25,26,27',
			'小'=>'0,1,2,3,4,5,6,7,8,9,10,11,12,13',
			'单'=>'1,3,5,7,9,11,13,15,17,19,21,23,25,27',
			'双'=>'0,2,4,6,8,10,12,14,16,18,20,22,24,26'
		);
		$bet_array = explode(',',$map[$bet]);
		if(!in_array($data,$bet_array)) return 0;
		return 1;
	}
	/**
	 * 28极小值极大值
	 *
	 * @params bet		投注列表：大单,小单
	 * @params data		开奖所需的那几位号码：4,5
	 *
	 * @return			返回中奖注数
	 */
	public function zdxds28($bet, $data){
		$map = array(
			'极大'=>'22,23,24,25,26,27',
			'极小'=>'0,1,2,3,4,5'
		);
		$bet_array = explode(',',$map[$bet]);
		if(!in_array($data,$bet_array)) return 0;
		return 1;
	}	
	/**
	 * 大小单双
	 *
	 * @params bet		投注列表：大单,小单
	 * @params data		开奖所需的那几位号码：4,5
	 *
	 * @return			返回中奖注数
	 */
	public function dxds($bet, $data){

		$data = explode(',',$data);
		$map = array(
			'大'=>'56789',
			'小'=>'01234',
			'单'=>'13579',
			'双'=>'02468'
		);
		$result = array_filter(
			call_user_func_array( array(__CLASS__, 'descartesAlgorithm' ), array_map( array($this,'mbStrSplit'), explode(',', $bet) ) ),
			function ($value) use ($data, $map){
				for($i=0;$i<count($value);$i++) {
					if(strpos($map[$value[$i]], $data[$i]) === false) return false;
				}
				return true;
			}
		);
		return count($result);
	}

	public function dxds2($bet, $data){

//		$data = explode(',',$data);
		$map = array(
			'大'=>'06,07,08,09,10',
			'小'=>'01,02,03,04,05',
			'单'=>'01,03,05,07,09',
			'双'=>'02,04,06,08,10'
		);
		$x = array_map( array($this,'mbStrSplit'), explode(',', $bet) );
		$result = array_filter(
			call_user_func_array( array(__CLASS__, 'descartesAlgorithm' ), array_map( array($this,'mbStrSplit'), explode(',', $bet) ) ),
			function ($value) use ($data, $map){
				for($i=0;$i<count($value);$i++) {
					if(strpos($map[$value], $data) === false) return false;
				}
				return true;
			}
		);
		return count($result);
	}

	public function mbStrSplit ($string, $len=1) {
		$start = 0;
		$strlen = mb_strlen($string);
		while ($strlen) {
			$array[] = mb_substr($string,$start,$len,"utf8");
			$string = mb_substr($string, $len, $strlen,"utf8");
			$strlen = mb_strlen($string);
		}
		return $array;
	}


	/**
	 * 组二复式
	 *
	 * @params bet		投注列表：135687
	 * @params data		开奖所需的那几位号码：4,5
	 *
	 * @return			返回中奖注数
	 */
	public function z2f($bet, $data) {
		if(preg_match('/(\d),\\1/',$data)) return 0;
		$bet = str_split($bet);
		$data = explode(',', $data);
		$dataString = implode('',$data);
		$data = implode('', array_reverse($data));
		$bet = array_map(
			function($array){
				return implode('',$array);
			},
			$this->combine($bet, 2)
		);
		$result = array_filter(
			$bet,
			function ($val) use ($data, $dataString) {
				return $val == $data || $val == $dataString;
			}
		);
		return count($result);
	}


	/**
	 * 组二单式
	 *
	 * @params bet		投注列表：1,3|5,6|8,7
	 * @params data		开奖所需的那几位号码：4,5
	 *
	 * @return			返回中奖注数
	 */
	public function z2d($bet, $data) {
		if(preg_match('/(\d),\\1/',$data)) return 0;
		$bet = explode('|', $bet);
		$dataString = implode(',', array_reverse(explode(',', $data)));
		$result = array_filter(
			$bet,
			function ($val) use ($data, $dataString) {
				return $val == $data || $val == $dataString;
			}
		);
		return count($result);
	}
	/**
	 * 常用前选算法
	 *
	 * @params bet		投注列表：01 02 03,04 05
	 * @params data		开奖所需的那几个：04,05
	 * @params weizhu   开奖前几位数
	 *
	 * @return 			返回中奖注数
	 */
	public function qs($bet, $data, $weizhu = 0){
		$bet = explode(',', $bet);
		$data = explode(',', substr($data,0,$weizhu*3-1));

		for($i=0;$i<count($data);$i++) {
			if(strpos($bet[$i], $data[$i]) === false) return 0;
		}
		return 1;
	}

	public function qs_addW($bet, $data, $weizhu = 0){
		$bet = explode(',', $bet);
		$data = explode(',', $data);
		print_r($bet);
		print_r($data);
		for($i=0;$i<$weizhu;$i++) {
			if(intval($data[$i]) < 10)
			{
				$data[$i] = "0".$data[$i];
			}
			
			if(strpos($bet[$i], $data[$i]) === false) return 0;
		}
		return 1;
	}
	/**
	 * 组三
	 *
	 * @params bet		投注列表：135687或112,334
	 * @params data		开奖所需的那几位号码：4,5,3
	 *
	 * @return			返回中奖注数
	 */
	public function z3($bet, $data) {
		$data = explode(',', $data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] >= 3) return 0;
		if(strpos($bet, ',') !== false || preg_match('/(\d)\d?\\1/', $bet)) {
			$bet = explode(',', $bet);
			$data = implode('', $data);
			preg_match('/(\d)\d?\\1/',$data,$match);
			if(!count($match)) return 0;
			$matchString = $match[1];
			$data = preg_replace('/'.$matchString.'/', '', $data);
			$result = array_filter(
				$bet,
				function ($val) use($data, $matchString) {
					return preg_replace('/'.$matchString.'/', '', $val) == $data;
				}
			);
			return count($result);
		}
		else {

			
/*
		$data = explode(',', $data);
		foreach ([16, 8, 4, 2, 1] as $i => $val){
			if (($w & $val) == 0) unset($data[$i]);
		}
		$data = implode(',', $data);
		$bet = array_filter(
			str_split($bet),
			function ($val) use ($data) {
				return strpos($data, $val) !== false;
			}
		);
		return count($bet);
*/
			$bet = array_map(
				function($array){
					return implode(',',$array);
				},
				$this->combine(str_split($bet), 2)
			);
			$result = array_filter(
				$bet,
				function ($val) use ($data) {
					for($i=0;$i<count($data);$i++) {
						if (strpos($val, $data[$i]) === false) return false;
					}
					return true;
				}
			);
			return count($result);
		}
	}

	/**
	 * 组六
	 *
	 * @params bet		投注列表：135687
	 * @params data		开奖所需的那几位号码：4,5,3
	 *
	 * @return			返回中奖注数
	 */
	public function z6($bet, $data) {
		$data = explode(',', $data);
		$group = array_count_values($data);
		rsort($group);
		if($group[0] >= 3) return 0;
		$data = array_map(
			function($array){
				return implode('', $array);
			},
			$this->permutation($data, 3)
		);

		if(strpos($bet, ',') !== false) {
			$bet = explode(',', $bet);
		}
		else{
			$bet = array_map(
				function($array){
					return implode('', $array);
				},
				$this->combine(str_split($bet), 3)
			);
		}

		$result = array_filter(
			$bet,
			function ($val) use ($data) {
				return array_search($val, $data) !== false;
			}
		);
		return count($result);
	}

	/**
	 * 排列算法
	 */
	public function permutation($arr, $num) {
		$r = [];
		$f = function ($t, $a, $n, $f) use (&$r) {
			if($n == 0) return array_push($r, $t);
			for ($i = 0, $l = count($a); $i < $l; $i++) {
				$f(array_merge($t,[$a[$i]]), array_merge(array_slice($a, 0, $i), array_slice($a, $i+1)), $n - 1, $f);
			}
		};
		$f([], $arr, $num, $f);
		return $r;
	}

	/**
	 * 组合算法
	 *
	 * @params Array arr		备选数组
	 * @params Int num
	 *
	 * @return Array			组合
	 *
	 * useage:  combine([1,2,3,4,5,6,7,8,9], 3);
	 */
	public function combine($arr, $num) {
		$r = [];
		$f = function ($t, $a, $n, $f) use (&$r) {
			if($n == 0) return array_push($r, $t);
			for ($i = 0, $l = count($a); $i <= $l - $n; $i++) {
				$f(array_merge($t,[$a[$i]]), array_slice($a,$i+1), $n - 1, $f);
			}
		};
		$f([], $arr, $num, $f);
		return $r;
	}

	//两个数组，返回包含相同数字的个数
	public function sames($a, $b) {
		$number = 0;
		if (strlen($b[0]) == 0) return $number;
		for ($i=0; $i<count($a); $i++) {
			$flag = 0;
			for ($j=0; $j<count($b); $j++) {
				if($a[$i] - $b[$j] == 0) {
					$flag = 1;
					break;
				}
			}
			if ($flag == 1) $number += 1;
		}
		return $number;
	}

	public function replaceList($data, $bets) {
		$array = explode(',', $data);
		$betsArray = explode(',', $bets);
		for($i=0;$i<count($betsArray);$i++) {
			if($betsArray[$i] == '-') {
				$array[$i] = '-';
			}
		}
		return implode(',',$array);
	}

	public function removeFromList($data, $position) {
		$array = explode(',', $data);
		for($i=0;$i<count($position);$i++) {
			unset($array[$position[$i]-1]);
		}
		return implode(',',$array);
	}

	public function getFromList($array, $position) {
		$array = explode(',', $array);
		$r = array();
		for($i=0;$i<count($position);$i++) {
			$r[] = $array[$position[$i]-1];
		}
		return implode(',', $r);
	}

	/**
	 * 常用复式算法
	 *
	 * @params bet		投注列表：123,45,2,59
	 * @params data		开奖所需的那几个：4,5,0,8
	 *
	 * @return 			返回中奖注数
	 */
	public function fs($bet, $data){
		$result = array_filter(
			array_map(
				function ($array) {
					return implode(',',$array);
				},
				call_user_func_array( array(__CLASS__, 'descartesAlgorithm' ), array_map( 'str_split', explode(',', $bet) ) )
			),
			function ($value) use ($data){
				return $value == $data;
			}
		);
		return count($result);
	}

	/**
	 * 单式算法
	 *
	 * @params bet		投注列表：1,5,2,9|3,2,4,6
	 * @params data		开奖所需的那几位号码：4,5,3,6
	 *
	 * @return			返回中奖注数
	 */
	public function ds($bet, $data){
		return count(array_filter(explode('|', $bet), function ($value) use ($data) {
			return $value == $data;
		}));
	}

	/**
	 * 笛卡爾乘積演算法
	 *
	 * @params 一个可变参数，原则上每个都是数组，但如果数组只有一个值是直接用这个值
	 *
	 * useage:
	 * console.log(DescartesAlgorithm(2, [4,5], [6,0],[7,8,9]));
	 */
	public function descartesAlgorithm() {

		$a = array();
		$b = array();
		$c = array();

		$args = func_get_args();
		if(count($args) == 1) return is_array($args[0]) ? $args[0] : array($args[0]);
		if(count($args) > 2) {
			for($i=0; $i<count($args)-1; $i++) {
				$a[$i] = $args[$i];
			}
			$b = $args[$i];
			return $this->descartesAlgorithm(call_user_func_array(array(__CLASS__, __METHOD__), $a), $b);
		}

		$a = is_array($args[0]) ? $args[0] : array($args[0]);
		$b = is_array($args[1]) ? $args[1] : array($args[1]);
		for($i=0; $i<count($a); $i++) {
			for($j=0; $j<count($b); $j++) {
				$c[] = (is_array($a[$i])) ? array_merge($a[$i], is_array($b[$j]) ? $b[$j] : array($b[$j])) : array($a[$i], $b[$j]);
			}
		}
		return $c;
	}

	/**
	 * 分割字符串
	 *
	 * @params str		字符串
	 * @params len      长度
	 */
	public function strCut($str, $len){
		$strlen = mb_strlen($str);
		if($strlen == 0) return false;
		$j = ceil($strlen / $len);
		$arr = array();
		for($i = 0; $i < $j; $i++)
			$arr[] = mb_substr($str, $i*$len, $len);
		return $arr;
	}


	public function combination($c, $b) {
		$b = intval($b);
		$c = intval($c);
		if($b < 0 || $c < 0) return 0;
		if($b == 0 || $c == 0) return 1;
		if($b > $c) return 0;
		if($b > $c / 2) $b = $c - $b;
		$a = 0;
		for($i=$c; $i>=($c-$b+1); $i--) {
			$a -= log($i);
		}
		for($i=$b; $i>=1; $i--) {
			$a -= log($i);
		}
		return round(exp($a));
	}

	//过滤重复的数组
	public function filterArray($arrs) {
		$n = count($arrs);
		$arr = array();
		for($i=0; $i < $n; $i++) {
			for($j=$i+1; $j < $n; $j++) {
				if($arrs[$i] == $arrs[$j]) {
					$arrs[$i] = null;
					break;
				}
			}
		}
		for($i=0; $i < $n; $i++) {
			if($arrs[$i] != null) $arr[] = $arrs[$i];
		}
		return $arr;
	}

	//是否有重复值
	public function isRepeat($array) {
		return (count($array) != count(array_unique($array))) ? true : false;
	}


}