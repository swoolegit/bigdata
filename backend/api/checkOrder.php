<?php
//快速管理地址：http://face.apius.cn?token=ff43912c7b61393f&verify=319db670c20d
include_once 'cron.config.php';
include_once 'KaiJiang.php';
include_once 'credit_kj.php';
set_time_limit(300);
class Cron extends KJ
{

	private $played = array();
	private $KaiJiang;
	private $credit_kj;
	private $official_kj;
	private $playMapping = '';
	private $check_bet = array();
	private $check_type;
	private $check_actionNO;

	public function __construct() {
		parent::__construct();
		$this->setPlayed();
		$this->getAllPlayed();
		//$this->KaiJiang = new KaiJiang();
		$this->credit_kj = new credit_kj();
		$this->official_kj = new KaiJiang();
	}
	private function setPlayed() {
		$sql = "
			SELECT
				id,
				ruleFun
			FROM
				`{$this->prename}played`
		";

		$result = $this->getRows($sql);
		foreach ($result as $rows) {
			$this->played[$rows['id']] = $rows['ruleFun'];
		}
		unset($result);
	}
	private function getAllPlayed() {
		if($this->playMapping) return;
		$sql ="
			SELECT
				t.id,
				p.groupId,
				p.id AS playId
			FROM
				{$this->prename}type t,
				{$this->prename}played p
			WHERE
				t.ENABLE = 1
			AND p.ENABLE = 1
			ORDER BY
				t.id,
				p.groupId,
				p.id
		";
		$result = $this->getRows($sql);
		foreach ($result as $rows){
			$this->playMapping .= ",{$rows['id']}-{$rows['groupId']}-{$rows['playId']}";
		}
		$this->playMapping .= ',';
	}
	public function getBets() {
		$sql = "
			SELECT
				id,
				playedId,
				actionData,
				weiShu,
				actionName,
				type,
				actionNo,
				kjTime,
				playedGroup,
				betInfo,
				isGuan,
				actionAmount,
				bonusProp
			FROM
				`{$this->prename}bets`
			WHERE isDelete = 0
			AND check_error = 0
			AND kjTime < ".(time()-15)."
			AND lotteryNo = '' 
			order by id desc
			limit 3000
		";
		return $this->getRows($sql);
	}

	public function getCheckBets_type($type,$actionNo) {
		if($this->check_bet && $this->check_type==$type && $this->actionNo==$actionNo)
		{
			return $this->check_bet;
		}
		$sql = "
			SELECT
				id,
				playedId,
				actionData,
				type,
				actionNo,
				kjTime,
				playedGroup,
				betInfo,
				isGuan,
				actionAmount,
				bonusProp
			FROM
				`{$this->prename}bets_check`
			WHERE isDelete = 0
			AND check_error = 0
			AND kjTime < ".(time()-15)."
			AND lotteryNo = '' 
			and type={$type}
			and actionNo='{$actionNo}'
			order by id desc
		";
		$result = $this->getRows($sql);
		$r=array();
		foreach ($result as $k=>$v) {
			$r[$v['id']]=array(
				'id'=>$v['id'],
				'playedId'=>$v['playedId'],
				'actionData'=>$v['actionData'],
				'type'=>$v['type'],
				'actionNo'=>$v['actionNo'],
				'kjTime'=>$v['kjTime'],
				'playedGroup'=>$v['playedGroup'],
				'betInfo'=>$v['betInfo'],
				'actionAmount'=>$v['actionAmount'],
				'bonusProp'=>$v['bonusProp']
			);
		}
		$this->check_type=$type;
		$this->actionNo=$actionNo;
		$this->check_bet=$r;
		return $r;
	}
	
    public function check_error($id)
    {
        $this->beginTransaction();
        try {
            $this->query("UPDATE `{$this->prename}bets` SET `check_error`=1 WHERE `id`=$id LIMIT 1");
            $this->commit();
			echo "......... 備註成功({$id})  \n";
        }
        catch (Exception $e) {
        	echo $e." \n";
            $this->rollBack();
            echo "......... 備註失敗({$id})  \n";
        }
	}
	
	public function getGuestBets() {
		$sql = "
			SELECT
				id,
				playedId,
				actionData,
				type,
				actionNo,
				kjTime,
				weiShu,
				actionName,
				playedGroup,
				betInfo,
				isGuan,
				bonusProp
			FROM
				`{$this->prename}guest_bets`
			WHERE isDelete = 0
			AND kjTime < ".(time()-15)."
			AND lotteryNo = ''
			order by id desc
			limit 3000
		";
		return $this->getRows($sql);
	}

	public function getNo($type,$number) {
		$sql = "
			SELECT
				type,
				number,
				data
			FROM
				`{$this->prename}data`
			WHERE type = {$type}
			AND number = '".$number."'
		";
		return $this->getRow($sql);
	}

    public function remove($id)
    {
        $this->beginTransaction();
        try {
            $data = $this->getRow("SELECT * FROM `{$this->prename}bets` WHERE `id`=$id LIMIT 1");
			//$amount = $data['beiShu'] * $data['mode'] * $data['actionNum'];
			$amount = $data['actionAmount'];
            $amount = abs($amount);
            $this->set_coin(array(
                'uid' => $data['uid'],
                'type' => $data['type'],
                'playedId' => $data['playedId'],
                'liqType' => 7,
                'info' => '撤单',
                'extfield0' => $id,
                'coin' => $amount
            ));
            $this->query("UPDATE `{$this->prename}bets` SET `isDelete`=1 WHERE `id`=$id LIMIT 1");
            $this->commit();
			echo "......... 撤单成功({$id})  \n";
        }
        catch (Exception $e) {
        	echo $e." \n";
            $this->rollBack();
            echo "......... 撤单失敗({$id})  \n";
        }
	}
	
    public function guest_remove($id)
    {
        $this->beginTransaction();
        try {
            $data = $this->getRow("SELECT * FROM `{$this->prename}guest_bets` WHERE `id`=$id LIMIT 1");
			//$amount = $data['beiShu'] * $data['mode'] * $data['actionNum'];
			$amount = $data['actionAmount'];
            $amount = abs($amount);
            $this->guest_set_coin(array(
                'uid' => $data['uid'],
                'type' => $data['type'],
                'playedId' => $data['playedId'],
                'liqType' => 7,
                'info' => '撤单',
                'extfield0' => $id,
                'coin' => $amount
            ));
            $this->query("UPDATE `{$this->prename}guest_bets` SET `isDelete`=1 WHERE `id`=$id LIMIT 1");
            $this->commit();
			echo "......... 撤单成功({$id})  \n";
        }
        catch (Exception $e) {
        	echo $e." \n";
            $this->rollBack();
            echo "......... 撤单失敗({$id})  \n";
        }
	}
	
	// 用户资金变动(请在一个事务里使用)
	public function set_coin($log) {
		$default = array(
			'coin' => 0,
			'fcoin' => 0,
			'uid' => $this->user['uid'],
			'liqType' => 0,
			'type' => 0,
			'info' => '',
			'extfield0' => 0,
			'extfield1' => '',
			'extfield2' => '',
		);
		$sql = 'call setCoin(';
		foreach ($default as $k => $v) {
			$val = (array_key_exists($k, $log) && $log[$k]) ? $log[$k] : $v;
			if ($v !== 0) $val = "'$val'";
			$sql .= $val.',';
		}
		$sql = substr($sql, 0, -1).')';
		$this->update($sql);
	}

	public function guest_set_coin($log) {
		$default = array(
			'coin' => 0,
			'fcoin' => 0,
			'uid' => $this->user['uid'],
			'liqType' => 0,
			'type' => 0,
			'info' => '',
			'extfield0' => 0,
			'extfield1' => '',
			'extfield2' => '',
		);
		$sql = 'call guest_setCoin(';
		foreach ($default as $k => $v) {
			$val = (array_key_exists($k, $log) && $log[$k]) ? $log[$k] : $v;
			if ($v !== 0) $val = "'$val'";
			$sql .= $val.',';
		}
		$sql = substr($sql, 0, -1).')';
		$this->update($sql);
	}
	public function run() {
		$p=rand(1,10000);
		$time=time();
		echo "開獎開始 - ". date('Y-m-d H:i:s',$time)."(p={$p}) \n";
		// $sql="TRUNCATE TABLE `lottery_kanjiang_temp` ";
		// $this->query($sql);
		$sql="delete from {$this->prename}guest_kanjiang_temp where status=1";
		$this->query($sql);
		$sql="delete from {$this->prename}kanjiang_temp where status=1";
		$this->query($sql);
		$del_time=($time-86400); // 2小时7200秒
		$bets = $this->getBets();
		//print_r($bets);
		$no=null;
		$lottery_copy=array();
		$settings = $this->getSettings();
		$i=0;
		if($bets) foreach ($bets as $rows) {
			$i++;
			//if($i > 300) exit;
			if(!isset($no))
			{
				$no=$this->getNo($rows['type'],$rows['actionNo']);
			}else
			{
				if($no['type']!=$rows['type'] || $no['number']!=$rows['actionNo'])
				{
					$no=$this->getNo($rows['type'],$rows['actionNo']);
				}
			}
			if(!$no)
			{
				if($rows['kjTime']<$del_time)
				{
					echo "撤单(超過24小时未開) - ". $rows['id']."(".$rows['actionNo'].") - ".$rows['type']." \n";
					$this->remove($rows['id']);
				}
				continue;
			}else
			{
				$kjData=$no['data'];
				//echo "開獎(".$rows['id'].") - ".$kjData ." - ". $no['number']. " - ". $no['type']." \n";					
			}			
			if (strpos($this->playMapping, ','.$rows['type'].'-'.$rows['playedGroup'].'-'.$rows['playedId'].',') === false)
			{
				echo "撤单(彩種或玩法關閉) - ". $rows['id']."(".$rows['actionNo'].") - ".$rows['type']." ";
				$this->remove($rows['id']);
				continue;
			}
			$checkbets=$this->getCheckBets_type($rows['type'],$rows['actionNo']);
			$md5=md5($rows['id'].$rows['playedId'].$rows['actionData'].$rows['type'].$rows['actionNo'].$rows['kjTime'].$rows['playedGroup'].$rows['betInfo'].$rows['actionAmount'].$rows['bonusProp']);
			$checkmd5=md5($checkbets[$rows['id']]['id'].$checkbets[$rows['id']]['playedId'].$checkbets[$rows['id']]['actionData'].$checkbets[$rows['id']]['type'].$checkbets[$rows['id']]['actionNo'].$checkbets[$rows['id']]['kjTime'].$checkbets[$rows['id']]['playedGroup'].$checkbets[$rows['id']]['betInfo'].$checkbets[$rows['id']]['actionAmount'].$checkbets[$rows['id']]['bonusProp']);
			if($md5!=$checkmd5)
			{
				echo "注單被修改 - ". $rows['id']."(".$rows['actionNo'].") - ".$rows['type']." == ".$md5." - ".$checkmd5." \n";
				echo $rows['id']." - ".$rows['playedId']." -  ".$rows['actionData']." - ".$rows['type']." - ".$rows['actionNo']." - ".$rows['kjTime']." - ".$rows['playedGroup']." - ".$rows['betInfo']." == ".$checkbets[$rows['id']]['id']." - ".$checkbets[$rows['id']]['playedId']." - "." - ".$checkbets[$rows['id']]['actionData']." - ".$checkbets[$rows['id']]['type']." - ".$checkbets[$rows['id']]['actionNo']." - ".$checkbets[$rows['id']]['kjTime']." - ".$checkbets[$rows['id']]['playedGroup']." - ".$checkbets[$rows['id']]['betInfo']." \n";
				$this->check_error($rows['id']);
				continue;				
			}
			$method = $this->played[$rows['playedId']];
			
			echo "注單補開 - ". $rows['id']."(".$rows['actionNo'].") - ".$rows['type']." \n";
			
			$zjCount = 0;
			if($rows['isGuan']==0)
			{
				$this->KaiJiang=$this->credit_kj;
			}
			if($rows['isGuan']==1)
			{
				$this->KaiJiang=$this->official_kj;
			}		
			if(method_exists($this->KaiJiang, $method)) {

				if(strpos($rows['actionData'], '|') !== false) {
					$betsArray = explode('|',$rows['actionData']);
					foreach ($betsArray as $actionData) {
						if ($rows['weiShu'] > 0) {
							$zjCount += $this->KaiJiang->$method($actionData, $kjData, $rows['weiShu']);
						}
						else {
							$zjCount += $this->KaiJiang->$method($actionData, $kjData);
						}
					}
				}
				else {
					
					switch(true)
					{
						case $rows['type']==90 || $rows['type']==94:
							$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData, $rows['actionName'],$rows['betInfo'],$settings['animalsYear'],$settings['lhcWxJin'], $settings['lhcWxMu'], $settings['lhcWxShui'], $settings['lhcWxHuo'], $settings['lhcWxTu']);
							break;
						default:
							if ($rows['weiShu'] > 0) {
								$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData, $rows['weiShu']);
							}
							else {
								$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData);
							}
							break;
					}
				}
				$lottery_copy[]="(".$rows['id'].",".$zjCount.",'".$kjData."',".$rows['type'].",".$time.",1)";
				if(count($lottery_copy)>1000)
				{
					$sql="insert into {$this->prename}kanjiang_temp(id,zjCount,kjData,type,actionTime,status)VALUES".implode(",", $lottery_copy);
					$this->query($sql);
					$this->update("call pre_kanjiang_notype('lottery_running',".$rows['type'].",1)");
					$lottery_copy=array();
					$etime=time();
					echo " , 處理1000筆 - 目前總計 :" . $i ."筆 , 已使用 ".($etime - $time)."秒(".date('Y-m-d H:i:s',$etime).")(p={$p}) \n";
					/*
					try{
						$sql="insert into {$this->prename}kanjiang_temp(id,zjCount,kjData)VALUES".implode(",", $lottery_copy);
						$this->query($sql);
						$this->update("call pre_kanjiang('lottery_running')");
						$lottery_copy=array();
						echo " , 已處理1000筆 - 目前總計 :" . $i ."筆 , 已使用 ".(time() - $time)."秒  \n";
					}catch (Exception $e) 
					{
						echo "資料庫錯誤  -- ".$e." \n";
						$sql="TRUNCATE TABLE `lottery_kanjiang_temp` ";
						$this->query($sql);
						echo "開獎過程出錯,清空  lottery_kanjiang_temp  -- \n";
					}
					 *
					 */	
				}
			}
		}
		if(count($lottery_copy)>0)
		{
			$sql="insert into {$this->prename}kanjiang_temp(id,zjCount,kjData,type,actionTime,status)VALUES".implode(",", $lottery_copy);
			$this->query($sql);
			$this->update("call pre_kanjiang_notype('lottery_running',".$rows['type'].",1)");
			$lottery_copy=array();
		}

		$del_time=($time-86400); // 2小时7200秒
		$bets = $this->getGuestBets();
		$no=null;
		$lottery_copy=array();
		$ii=0;
		if($bets) foreach ($bets as $rows) {
			$ii++;
			//if($i > 300) exit;
			if(!isset($no))
			{
				$no=$this->getNo($rows['type'],$rows['actionNo']);
			}else
			{
				if($no['type']!=$rows['type'] || $no['number']!=$rows['actionNo'])
				{
					$no=$this->getNo($rows['type'],$rows['actionNo']);
				}
			}
			if(!$no)
			{
				if($rows['kjTime']<$del_time)
				{
					echo "撤单(超過24小时未開) - ". $rows['id']."(".$rows['actionNo'].") - ".$rows['type']." \n";
					$this->guest_remove($rows['id']);
				}
				continue;
			}else
			{
				$kjData=$no['data'];
				//echo "開獎(".$rows['id'].") - ".$kjData ." - ". $no['number']. " - ". $no['type']." \n";					
			}			
			if (strpos($this->playMapping, ','.$rows['type'].'-'.$rows['playedGroup'].'-'.$rows['playedId'].',') === false)
			{
				echo "撤单(彩種或玩法關閉) - ". $rows['id']."(".$rows['actionNo'].") - ".$rows['type']." ";
				$this->guest_remove($rows['id']);
				continue;
			}
			
			$method = $this->played[$rows['playedId']];

			$zjCount = 0;
			if($rows['isGuan']==0)
			{
				$this->KaiJiang=$this->credit_kj;
			}
			if($rows['isGuan']==1)
			{
				$this->KaiJiang=$this->official_kj;
			}			
			if(method_exists($this->KaiJiang, $method)) {

				if(strpos($rows['actionData'], '|') !== false) {
					$betsArray = explode('|',$rows['actionData']);
					foreach ($betsArray as $actionData) {
						if ($rows['weiShu'] > 0) {
							$zjCount += $this->KaiJiang->$method($actionData, $kjData, $rows['weiShu']);
						}
						else {
							$zjCount += $this->KaiJiang->$method($actionData, $kjData);
						}
					}
				}
				else {
					switch(true)
					{
						case $rows['type']==90 || $rows['type']==94:
							$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData, $rows['actionName'],$rows['betInfo'],$settings['animalsYear'],$settings['lhcWxJin'], $settings['lhcWxMu'], $settings['lhcWxShui'], $settings['lhcWxHuo'], $settings['lhcWxTu']);
							break;
						default:
							if ($rows['weiShu'] > 0) {
								$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData, $rows['weiShu']);
							}
							else {
								$zjCount = $this->KaiJiang->$method($rows['actionData'], $kjData);
							}
							break;
					}
				}
				$lottery_copy[]="(".$rows['id'].",".$zjCount.",'".$kjData."',".$rows['type'].",".$time.",1)";
				if(count($lottery_copy)>1000)
				{
					$sql="insert into {$this->prename}guest_kanjiang_temp(id,zjCount,kjData,type,actionTime,status)VALUES".implode(",", $lottery_copy);
					$this->query($sql);
					$this->update("call guest_pre_kanjiang_notype('lottery_running',".$rows['type'].",1)");
					$lottery_copy=array();
					$etime=time();
					echo " , 處理1000筆 - 目前總計 :" . $i ."筆 , 已使用 ".($etime - $time)."秒(".date('Y-m-d H:i:s',$etime).")(p={$p}) \n";
					/*
					try{
						$sql="insert into {$this->prename}kanjiang_temp(id,zjCount,kjData)VALUES".implode(",", $lottery_copy);
						$this->query($sql);
						$this->update("call pre_kanjiang('lottery_running')");
						$lottery_copy=array();
						echo " , 已處理1000筆 - 目前總計 :" . $i ."筆 , 已使用 ".(time() - $time)."秒  \n";
					}catch (Exception $e) 
					{
						echo "資料庫錯誤  -- ".$e." \n";
						$sql="TRUNCATE TABLE `lottery_kanjiang_temp` ";
						$this->query($sql);
						echo "開獎過程出錯,清空  lottery_kanjiang_temp  -- \n";
					}
					 *
					 */	
				}
			}
		}		
		if(count($lottery_copy)>0)
		{
			$sql="insert into {$this->prename}guest_kanjiang_temp(id,zjCount,kjData,type,actionTime,status)VALUES".implode(",", $lottery_copy);
			$this->query($sql);
			$this->update("call guest_pre_kanjiang_notype('lottery_running',".$rows['type'].",1)");
			$lottery_copy=array();
		}
		$etime=time();
		echo ".............. 開獎結束 ,總計 {$i} 筆正式單, {$ii} 筆測試單 , 使用  ".($etime-$time)."秒(".date('Y-m-d H:i:s',$etime).")(p={$p}) \n";
		$sql="delete from {$this->prename}guest_kanjiang_temp where actionTime < ".($etime-600)." and status=0";
		$this->query($sql);
	}
}
$Cron = new Cron();
$Cron->run();
//$Cron->run(53, '2017141', '0,4,4,8,3');
//$Cron->trend(25,'20170605051','3,5,6');