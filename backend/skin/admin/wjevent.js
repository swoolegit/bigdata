$(function(){
	var tipTask = function(url) {
		$.getJSON(url, function(tip) {
			if(tip){
				if(!tip.flag) return;
				playVoice('/skin/sound/backcash.wav', 'cash-voice');
				var buttons=[];
				tip.buttons.split('|').forEach(function(button){
					button=button.split(':');
					buttons.push({text:button[0], click:window[button[1]]});
				});
				/*
                function explode(){
                  $(tip).dialog('destroy');
                }
                setTimeout(explode, 3000);
                */
                var unix = Math.round(+new Date()/1000);
                var R = Math.floor(Math.random()*(10000-1+1)+1);
                var divID = 'dialog'+unix+R;
				$('<div id='+divID+'>').append(tip.message).dialog({
					position:['right','bottom'],
					minHeight:40,
					title:'系统提示',
					buttons:buttons,
                    open: function(event, ui) {
                        setTimeout(function(){
                            $("#"+divID).dialog('close');                
                        }, 20000);
                    }
				});
                //setTimeout("defaultCloseModal()", 2000);
			}
		});
	};
	if(typeof(TIP)!='undefined' && TIP) {
		//setInterval(tipTask, 10000, '/business/getTip'); //系统提示
		//setInterval(tipTask, 10000, '/business/getRecharge'); //充值提示
		//setInterval(tipTask, 10000, '/business/getCash'); //提现提示
	}
});