window.isIE = (navigator.userAgent.indexOf("MSIE") != -1);

window.util = {
	init : function () {
		var default_page = "countData/index";
		load(default_page);
		return false;

		if (isIE) {
			load(default_page)
			return false;
		}

		var url = location.hash.split("!/")[1];
		url ? load(url) : load(default_page);

		if (history.pushState) {
			window.onpopstate = function(event) {
				var url = location.hash.split("!/")[1];
				if (url) {
					load(url, {popState:1});
				}
			}
		}
	},
	loading : function (){
		if (isIE) return false;
		$.blockUI({
			css :{border:'0px'},
			message : '<div class="loader-default"></div>'
		});
	},
	unloading : function (){
		if (isIE) return false;
		$.unblockUI();
	}
};
$(function (){
	util.init();
});